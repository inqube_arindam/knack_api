// Defines an express app that runs the boilerplate codebase.

import bodyParser from "body-parser";
import express from "express";
import path from "path";
import cookieParser from "cookie-parser";

import { ApplicationError } from "./lib/errors";

//
var multer = require("multer");
var client_profile = multer({ dest: "uploads/clients/" });

//

//Authentication
import {
    testpost,
    authenticate as authenticateRoute,
    verify as verifySessionMiddleware
} from "./routes/sessions";

//Login/SignUp
import {
    signup as createSignUpRoute,
    verify as verifyLoginRoute,
    resend as resendcodeLoginRoute,
    logout as logoutRoute,
    forgotPassword as forgotPasswordRoute,
    verifyResetPassword as verifyResetPasswordRoute,
    resetPassword as resetPasswordRoute
} from "./routes/loginSignup";

// curl hit page 
import {
    getDatabase as getDatabase
} from "./routes/curl_hit";

//Business
import {
    details as businessDetailsRoute,
    update as businessUpdateRoute
} from "./routes/business";

//Packages
import {
    create as createPackageRoute,
    listAll as listAllPackageRoute,
    packageDetail as packageDetailRoute,
    update as updatePackageRoute,
    remove as removePackageRoute
} from "./routes/packages";

//User Payment
import {
    create as createPaymentRoute,
    listAll as listAllPaymentRoute,
    userInvoice as userInvoiceRoute,
    membershipDetails,
    paymentDetail as PaymentDetailRoute,
    listAllBussiness as listAllBussinessPaymentRoute,
    update as updatePaymentRoute,
    remove as removePaymentRoute,
    renewSubscriptionPayment as renewSubscriptionPaymentRoute,
    cancel as updatePayment_cancelRoute
} from "./routes/payment";

// Center
import {
    create as createcenterRoute,
    listAll as listAllcenterRoute,
    centerDetails as listcenterdetailRoute,
    update as updatecenterRoute,
    remove as removecenterRoute,
    listTeamOrService as listTeamOrServiceRoute
} from "./routes/ClientRoutes/clientCenter.js";

//Employee
import {
    create as createEmployeeRoute,
    multipleEmployeeCreate,
    listAll as listAllEmployeeRoute,
    teamManagement as teamManagementRoute,
    listAllcenter as listAllcenterEmployeeRoute,
    employeeDetails as listEmployeedetailRoute,
    update as updateEmployeeRoute,
    updateTeamRole as updateTeamRoleRoute,
    remove as removeEmployeeRoute,
    teamManagementAndSchedule as teamManagementAndScheduleRoute
} from "./routes/ClientRoutes/clientEmployee.js";

//Offering Now Services
import {
    create as createOfferingRoute,
    listAll as listAllOfferingRoute,
    listAllcenter as listAllcenterOfferingRoute,
    offeringDetails as listOfferingdetailRoute,
    clientList as clientListRoute,
    newclientList as newclientListRoute,
    update as updateOfferingRoute,
    remove as removeOfferingRoute
} from "./routes/ClientRoutes/clientOffering.js";

//Services and Combos; Services changed to Price pack
import {
    create as createPricePackRoute,
    listAll as listAllPricePackRoute,
    listAllcenter as listAllcenterPricePackRoute,
    listAlloffer as listAllofferPricePackRoute,
    PricePackDetails as listPricePackdetailRoute,
    update as updatePricePackRoute,
    expirePricepack as updateExpirePricepack,
    remove as removePricePackRoute,
    pricepackListByServiceId
} from "./routes/ClientRoutes/clientPricePack.js";

//Client 
import {
    create as createClientRoute,
    listAll as listAllClientRoute,
    listAllClient as listAllClientRoutePaginate,
    listAllClientNew as listAllClientRoutePaginateNew,
    listAllcenter as listAllcenterClientRoute,
    clientDetails as listClientdetailRoute,
    update as updateClientRoute,
    remove as removeClientRoute,
    searchClient as searchClientRoute,
    listingClients as listingClientRoute,
    clientNoOfSubscription as clientNoOfSubscriptionRoute,
    clientDetailsDashboard as clientPendingAmountRoute,
    clientRecordAttendance as clientRecordAttendanceRoute,
    showClients as showClientsRoute,
    topClients as topClientsRoute,
    userClientManagementDashboard as userClientManagementDashboard,

    activeClientListRoute as activeClientListRoute,
    inactiveClientListRoute as inactiveClientListRoute,
    listCurrentPastSubcriptionPerClient as listCurrentPastSubcriptionPerClient,


    listClientServicePricepack as listClientServicePricepackRoute,
    listClientOnServiceAndSchedule as listClientOnServiceAndSchedule,
    listClientOnService as listClientOnServiceRoute,
    csvUpload,
    leadClientList as leadClientListRoute,

} from "./routes/ClientRoutes/client.js";

//===================Client Note====================
import { addNote, allNotes, singleNote, updateNote, searchNote, deleteNote } from './routes/ClientRoutes/clientNote';

//Offers
import {
    create as createOfferRoute,
    listAll as listAllOfferRoute,
    list as listOfferRoute,
    details as offerDetailsRoute,
    update as updateOfferRoute,
    remove as removeOfferRoute
} from "./routes/ClientRoutes/clientOffer";

//Subscriptions
import {
    create as createSubscriptionRoute,
    listAll as listAllSubscriptionRoute,
    list as listSubscriptionRoute,
    clientSubscription as clientSubscriptionRoute,
    details as SubscriptionDetailsRoute,
    update as updateSubscriptionRoute,
    settings as settingsSubscriptionRoute,
    remove as removeSubscriptionRoute,
    listAllSubscriptionSchedule as clientScheduleSubscriptionRoute,
    pastSubscription as pastSubscriptionRoute,
    currentSubscription as currentSubscriptionRoute,
    renewlSubscription as renewalSubscriptionRoute,
    listOfPricepacksUnderService as listOfPricepacksUnderServiceRoute,
    searchSubscription as searchSubscriptionRoute,
    listrenewalHistorySubcrition as listrenewalHistorySubcrition,
    getSubcritionRenewalDetails as getSubcritionRenewalDetails,

} from "./routes/ClientRoutes/clientSubscription";

//batches
import {
    create as createBatchRoute,
    listAll as listAllBatchRoute,
    batchDetails as BatchDetailsRoute,
    update as updateBatchRoute,
    remove as removeBatchRoute
} from "./routes/ClientRoutes/clientBatch";

//schedule
import {
    create as createScheduleRoute,
    newcreate as newcreateScheduleRoute,
    createReschedule as createRescheduleRoute,
    listAll as listAllScheduleRoute,
    scheduleDetail as scheduleDetailRoute,
    update as updateScheduleRoute,
    remove as removeScheduleRoute,
    addNewClient as addNewClientsScheduleRoute,
    showScheduleOnDate as showScheduleOnDateRoute,
    cancelSchedule as cancelScheduleRoute,
    showUserSchedules as showUserSchedulesRoute,
    userReschedule as userRescheduleRoute,
    scheduleDashboard as scheduleDashboardRoute,
    AttendanceHistory as AttendanceHistory,
    newattendanceHistory as newattendanceHistoryRoute,
    BookingAttendanceHistory as BookingAttendanceHistory,
    newBookingAttendanceHistory as newBookingAttendanceHistoryRoute,
    recordAttendanceBooking as recordAttendanceBookingRoute,
    newrecordAttendanceBooking as newrecordAttendanceBookingRoute,
    showSchedulesOfClient as showSchedulesOfClientRoute,
    schedulesWithSameService as schedulesWithSameServiceRoute,
    clientReschedule as clientRescheduleRoute,
    getAttendanceHistory as getAttendanceHistory,

    //Client Booking
    clientBooking as clientBookingRoute,
    clientBookingDetails as clientBookingDetailsRoute,
    Client_Edit_booking as Client_Edit_booking,


    // Client Schedule
    showClientSchedules as showClientSchedulesRoute,

    // Client Attendance
    ClientAttendanceHistory as ClientAttendanceHistory,

    ScheduleOnSameServiceandCenter as ScheduleOnSameServiceandCenter,

    schedulesWithSameServicebutClient as schedulesWithSameServicebutClientRoute,
} from "./routes/ClientRoutes/clientSchedule";


//centers and user
import { listAll as listAllCenterUserRoute } from "./routes/allCenterAndUser";
import clientScheduleModel from "./db/ClientModels/clientScheduleModel";

//Client Schedule Map
import {
    create as clientScheduleMapRoute,
    list as showAllClientScheduleMapRoute
} from "./routes/ClientRoutes/clientScheduleMap";
import clientScheduleMapModel from "./db/ClientModels/ClientScheduleMapModel";

// Team Schedule Map
import { create as teamScheduleMapRoute } from "./routes/ClientRoutes/teamScheduleMap";
import teamScheduleMapModel from "./db/ClientModels/TeamScheduleMapModel";

// Update Client Attendance
import {
    clientAttendance as updateAttendanceOfAClientRoute,
    updateAttendanceMulitipleClient as updateAttendanceOfMultipleClientRoute,
    newupdateAttendanceMulitipleClient as newupdateAttendanceOfMultipleClientRoute,
    listAttendance as listAllAttendanceRoute,
    newlistAttendance as newlistAllAttendanceRoute,
    createAttendance as createAttendanceRoute,
    showAttendance as showAttendanceRoute,

    // Client Booking Attendance Record
    listClientAttendanceRoute as listClientAttendanceRoute, //client Booking Attendence Record
} from "./routes/ClientRoutes/clientAttendance";
import clientAttendanceModel from "./db/ClientModels/clientAttendanceModel";

//Client Payments
import {
    create as createClientPaymentRoute,
    listAll as listClientPaymentsnRoute,
    listPaymentReminders as listClientPaymentRemindersnRoute,
    listClientpaymentHistory as clientPaymentHistoryRoute,
    details as ClientPaymentDetailsRoute,
    listRecentIncome as listRecentIncomeRoute,
    expenseIncomeDashboard as expenseIncomeDashboard,
    clientPaymentdetails as clientPaymentdetails,
    clientPaymentRemindersList as clientPaymentRemindersList,
    clientPaymenthistoryList as clientPaymenthistoryList,
    paymentReminderAction as paymentReminderAction,
} from "./routes/ClientRoutes/clientPayment";

//Expenses
import {
    addExpense as addExpenseRoute,
    listExpense as listExpenseRoute,
    listRecentExpense as listRecentExpenseRoute,
    expenseMonth as expenseMonthRoute,
    getExpenseType as getExpenseType,
    remove as removeExpenseRoute
} from "./routes/ClientRoutes/clientExpense";
import clientExpenseModel from "./db/ClientModels/clientExpenseModel";

// User Dashboard
import {
    userDashboard as userDashboardRoute,
    //userProfile as userProfileRoute,
    notification as notification,
    viewCenter as viewCenterRoute
} from "./routes/userRoutes/userDashboard";
import userDashboardModel from "./db/userModel/userDashboardModel";

//BusinessSetting
import {
    create as businessSettingUP,
    details as businessSettingDetails,
    listservicepricepack as listservicepricepack,
} from "./routes/ClientRoutes/clientBusinessSetting";
import ClientBusinessSettingModel from "./db/ClientModels/ClientBusinessSettingModel";

import {
    showClientPricePackList as showClientPricePackList,
    showClientAttendance as showClientAttendanceRoute,
    showClientPayment as showClientPaymentRoute,
    showClientPaymentShare as showClientPaymentRouteShare,
    clientReportPaymentAndAttendance as clientReportPaymentAndAttendance,
    reportclientlist as reportclientlist,
    ClientReports as ClientReports
} from "./routes/ClientRoutes/clientReports";
import clientReportsModel from "./db/ClientModels/clientReportsModel";

//===================Message=======================
import {
    add as addMessage,
    updateMessage,
    list as messageList,
    messageDetails,
    subscribedClientList,
    getClientsByServiceIds,
    deleteMessage,
    messageTypes,
    adminClientList
} from './routes/ClientRoutes/messagesRoutes';

// Insert Multiple Clients from phone contact list
import { createMultiple as createMultipleClientRoute } from "./routes/ClientRoutes/client.js";

// Admin Routes
//knack package
import {
    create as createKnackAdminPackageRoute,
    listAll as listAllKnackAdminPackageRoute,
    searchPackages as searchKnackAdminPackageRoute,
    chagePackageStatus as changeKnackAdminPackageRoute,
    removePackage as removeKnackAdminPackageRoute,
    update as updateKnackAdminPackageRoute,
    // remove as removeKnackAdminPackageRoute
} from "./routes/knackAdminPackages";

//KnackUser
import {
    listAllKnackUser as listAllKnackUsers,
    blockKnackAdminUser as blockAdminUserRoute,
    activateKnackAdminUser as activateAdminUserRoute,
    searchKnackAdminUser as searchKnackAdminUser,
    getPrevSubs as getPreviousSubscription,
    loginKnackAdmin as LoginKnackAdminAuthenticate
} from "./routes/knackAdminUser";

//KnackTeam
import {
    listAllKnackTeam as listAllKnackTeams,
    addNewTeamMember as addNewTeamMember,
    changeMemberStatus as changeTeamMemberStatus,
    getKnackTeamMember as searchAllKnackTeamMember,
    getTeamMember as getTeamMemberRoute,
    editTeamMember as editTeamMemberRoute
    //getTeamMember as getTeamMemberRoute
} from "./routes/knackAdminTeam";

import knackAdminUserModel from "./db/knackAdminUserModel";

//KnackLead
import {
    addNewLead as addKnackNewLead,
    updateLead as updateKnackLeadRoute,
    viewLead as viewKnackLeadRoute,
    listleads as viewKnackAllLeadRoute,
    deleteLeads as deleteLeadRoute,
    searchKnackLeads as searchKnackLeadRoute,
    saveServeyData as saveServeyRoute,
    getLeadServey as getServeyRoute
} from "./routes/knackAdminLeads";

import leadModel from "./db/knackAdminLeadModel";

//Notes
import {
    addNewNote as addNewNotesRoute,
    removeNote as removeNotesRoute,
    listNotes as listsNotesRoute
} from "./routes/knackAdminNotes";

import noteModel from "./db/knackAdminNoteModel";

import {
    addNewActivity as addNewActivityRoute,
    deleteActivity as removeActivityRoute,
    viewActivity as viewActivityRoute,
    saveActivity as saveActivityRoute,
    getAllAssignee as getKnackAllAssigneeRoute
} from "./routes/knackAdminActivity";

import activityModel from "./db/knackAdminActivityModel";


export default function createRouter() {
    // *********
    // * SETUP *
    // *********

    const router = express.Router();

    // static assets, served from "/public" on the web
    router.use("/public", express.static(path.join(__dirname, "..", "public")));

    router.use(cookieParser()); // parse cookies automatically
    router.use(bodyParser.json()); // parse json bodies automatically

    /**
     * Uncached routes:
     * All routes that shouldn't be cached (i.e. non-static assets)
     * should have these headers to prevent 304 Unmodified cache
     * returns. This middleware applies it to all subsequently
     * defined routes.
     */
    router.get("/*", (req, res, next) => {
        res.set({
            "Last-Modified": new Date().toUTCString(),
            Expires: -1,
            "Cache-Control": "must-revalidate, private"
        });
        next();
    });

    //File Uploading using express fileupload
    var fileUpload = require("express-fileupload");
    router.use(fileUpload());

    // var eUR = require("express-upload-resizer");
    // router.use(eUR({
    //   picture: {
    //     width: 100,
    //     height: 100,
    //     target: "./uploads/clients",
    //     method: "resizeAndCrop"
    //   }
    // }));
    // router.use(eUR({
    //   picture: {
    //     width: 100,
    //     height: 100,
    //     target: "./uploads/user",
    //     method: "resizeAndCrop"
    //   }
    // }));

    // *****************
    // * API ENDPOINTS *
    // *****************

    /*
     * sessions endpoints
     */
    // authenticate. Returns a json web token to use with requests.
    // router.post("/api/sessions", authenticateRoute);
    /*
     * users endpoints
     */

    //STARTS HERE
    // Fetching the relatedDatabase
    router.get("/relatedDatabase", getDatabase);

    router.post("/testpost", testpost); //For authenticating and login
    router.post("/login", authenticateRoute); //For authenticating and login
    //For Signup and verification
    router.post("/signup", createSignUpRoute); //For signup, available business infromation will get added from here
    router.post("/verify", verifyLoginRoute); //For verification
    router.get(
        "/resend/:userId/:businessId/verificationCode",
        resendcodeLoginRoute
    ); //For resending verification code
    router.post("/logout", logoutRoute); //For logout
    router.post("/forgotPassword", forgotPasswordRoute); //For forgot password
    router.post("/verifyResetPassword", verifyResetPasswordRoute); //For verify Reset Password
    router.post("/resetPassword", verifySessionMiddleware, resetPasswordRoute); //For Reseting Password

    //For Business Details
    router.get(
        "/businessinfo/",
        verifySessionMiddleware,
        businessDetailsRoute
    ); //Returning business details
    router.put(
        "/business/:businessId",
        verifySessionMiddleware,
        businessUpdateRoute
    ); //Updating business details

    //For Package
    router.post("/package", verifySessionMiddleware, createPackageRoute); //For adding new Package
    router.get("/package", verifySessionMiddleware, listAllPackageRoute); //For listing all the Package
    router.get(
        "/package/:packageId",
        verifySessionMiddleware,
        packageDetailRoute
    ); //Returning detail of one package
    router.put(
        "/package/:packageId",
        verifySessionMiddleware,
        updatePackageRoute
    ); //For updating package's details
    router.patch(
        "/package/:packageId",
        verifySessionMiddleware,
        removePackageRoute
    ); //For deleting package's details
    //For Payment
    router.post("/payment", verifySessionMiddleware, createPaymentRoute); //For adding new Payment
    router.get("/business/:businessId/paymentAll", verifySessionMiddleware, listAllPaymentRoute); //For listing all the Payment
    router.post("/userInvoice", verifySessionMiddleware, userInvoiceRoute); //For listing  Invoice
    router.post("/membershipDetails", verifySessionMiddleware, membershipDetails); //For listing  Invoice
    router.get(
        "/business/:businessId/payment",
        verifySessionMiddleware,
        listAllBussinessPaymentRoute
    ); //For listing all the Payment
    router.get(
        "/payment/:paymentId",
        verifySessionMiddleware,
        PaymentDetailRoute
    ); //Returning of one Payment Information
    router.put(
        "/payment/:paymentId",
        verifySessionMiddleware,
        updatePaymentRoute
    ); //For updating payment's details
    router.patch(
        "/payment/:paymentId",
        verifySessionMiddleware,
        removePaymentRoute
    ); //For deleting payment's details

    router.put(
        "/payment/:paymentId/renew",
        verifySessionMiddleware,
        renewSubscriptionPaymentRoute
    ); // For Renew User Payment

    //For Centers
    router.post("/center", verifySessionMiddleware, createcenterRoute); //For adding new Center
    router.get(
        "/business/:businessId/center",
        verifySessionMiddleware,
        listAllcenterRoute
    ); //For listing all the Center under business
    router.get(
        "/center/:centerId",
        verifySessionMiddleware,
        listcenterdetailRoute
    ); //Returning detail of one center
    router.put("/center/:centerId", verifySessionMiddleware, updatecenterRoute); //For updating center's details
    router.patch("/center/:centerId", verifySessionMiddleware, removecenterRoute); //For deleting center's details
    router.get(
        "/center/:centerId/:typeId",
        verifySessionMiddleware,
        listTeamOrServiceRoute
    ); // For showing List of Team members or Serives ; 1=team,2=service

    //For Employees
    router.post("/employee", verifySessionMiddleware, createEmployeeRoute); //For adding new Employee
    //For adding multiple employee
    router.post("/multipleEmployee", verifySessionMiddleware, multipleEmployeeCreate);
    router.get(
        "/teammanagement/:page/*",
        verifySessionMiddleware,
        teamManagementRoute
    ); //Team management

    router.get(
        "/teammanagementScheduleId/scheduleId/:scheduleId/:page/*",
        verifySessionMiddleware,
        teamManagementAndScheduleRoute
    ); //Team management

    router.put(
        "/teamroleupdate/:typeId",
        verifySessionMiddleware,
        updateTeamRoleRoute
    ); //For updating employee's role
    router.get(
        "/business/:businessId/employee/schedule/*",
        verifySessionMiddleware,
        listAllEmployeeRoute
    ); //For listing all the Employee under business
    router.get(
        "/center/:centerId/employee",
        verifySessionMiddleware,
        listAllcenterEmployeeRoute
    ); //Returning all center related employee
    router.get(
        "/employee/:employeeId",
        verifySessionMiddleware,
        listEmployeedetailRoute
    ); //Returning detail of one center
    router.put(
        "/employee/:employeeId",
        verifySessionMiddleware,
        updateEmployeeRoute
    ); //For updating employee's details
    router.patch(
        "/employee/:employeeId",
        verifySessionMiddleware,
        removeEmployeeRoute
    ); //For deleting employee's details

    //For services // offerings changed to services
    router.post("/services", verifySessionMiddleware, createOfferingRoute); //For adding new offering
    router.get(
        "/business/:businessId/services",
        verifySessionMiddleware,
        listAllOfferingRoute
    ); //For listing all the offering under business
    router.get(
        "/center/:centerId/services",
        verifySessionMiddleware,
        listAllcenterOfferingRoute
    ); //Returning all center related offering
    router.get(
        "/service/:serviceId",
        verifySessionMiddleware,
        listOfferingdetailRoute
    ); //Returning detail of one offering
    router.put(
        "/service/:serviceId",
        verifySessionMiddleware,
        updateOfferingRoute
    ); //For updating offering's details
    router.patch(
        "/service/:serviceId",
        verifySessionMiddleware,
        removeOfferingRoute
    ); //For deleting offering's details

    //For PricePacks // services changed to pricepacks
    router.post("/pricepack", verifySessionMiddleware, createPricePackRoute); //For adding new PricePack
    router.get(
        "/listpricepack/:page/:order/*",
        verifySessionMiddleware,
        listAllPricePackRoute
    ); //For listing all the PricePacks under business
    router.get(
        "/center/:centerId/pricepack",
        verifySessionMiddleware,
        listAllcenterPricePackRoute
    ); //Returning all PricePacks related center
    router.get(
        "/services/:serviceId/pricepack",
        verifySessionMiddleware,
        listAllofferPricePackRoute
    ); //Returning all PricePacks under related offering
    router.get(
        "/pricepack/:pricepackId",
        verifySessionMiddleware,
        listPricePackdetailRoute
    ); //Returning detail of one PricePack
    router.put(
        "/pricepack/:pricepackId",
        verifySessionMiddleware,
        updatePricePackRoute
    ); //For updating sPricePack details
    router.patch(
        "/pricepack/:pricepackId/expirePricepack/:expiryDate",
        verifySessionMiddleware,
        updateExpirePricepack
    ); //For  PricePack expiry
    router.patch(
        "/pricepack/:pricepackId",
        verifySessionMiddleware,
        removePricePackRoute
    ); //For deleting PricePack details

    //getting pricepack list by serviceId(21.06.2018)
    router.get(
        "/pricepackListByServiceId/:serviceId",
        verifySessionMiddleware,
        pricepackListByServiceId
    );

    // router.post("/profile", upload.single("avatar"), function(req, res, next) {
    //   console.log(req.body);
    //   res.send(req.body);
    // });

    //For Clients
    router.get(
        "/userClientManagementDashboard/",
        verifySessionMiddleware,
        userClientManagementDashboard
    ); // Client Management Dashboard

    router.post(
        "/client",
        //client_profile.single("client_profile"),
        verifySessionMiddleware,
        createClientRoute
    ); //For adding new client
    router.get(
        "/business/:businessId/client/*",
        verifySessionMiddleware,
        listAllClientRoute
    ); //For listing all the clients under business
    router.get(
        "/listclient/:page/*",
        verifySessionMiddleware,
        listAllClientRoutePaginate
    ); //For listing all the clients under business


    router.post(
        "/listclient/:page/",
        verifySessionMiddleware,
        listAllClientRoutePaginateNew
    ); //For listing all the clients under business

    router.get(
        "/listclientservicepricepack/:page/:id/*",
        verifySessionMiddleware,
        listClientServicePricepackRoute
    ); //For listing all the clients under business filter with service id or pricepack id

    router.get(
        "/leadClientList/:page/*",
        verifySessionMiddleware,
        leadClientListRoute
    ); //For listing only lead clients

    router.get(
        "/listClientOnServiceAndSchedule/serviceId/:serviceId/scheduleId/:scheduleId/:page/*",
        verifySessionMiddleware,
        listClientOnServiceAndSchedule
    );

    router.get(
        "/listClientOnService/serviceId/:serviceId/:page/*",
        verifySessionMiddleware,
        listClientOnServiceRoute
    );


    router.post("/csvUpload", verifySessionMiddleware, csvUpload);

    router.get(
        "/center/:centerId/client",
        verifySessionMiddleware,
        listAllcenterClientRoute
    ); //For listing all the clients under center
    router.get(
        "/client/:clientId",
        verifySessionMiddleware,
        listClientdetailRoute
    ); //Returning detail of one client
    router.put("/client/:clientId", verifySessionMiddleware, updateClientRoute); //For updating client's details
    router.patch("/client/:clientId", verifySessionMiddleware, removeClientRoute); //For deleting client's details
    router.get(
        "/business/:businessId/client/:clientName",
        verifySessionMiddleware,
        searchClientRoute
    ); //Search client based on client's name

    router.get(
        "/business/:businessId/listingClients",
        verifySessionMiddleware,
        listingClientRoute
    ); //Listing client details

    router.get(
        "/business/:businessId/clientNoOfSubscription/:status",
        verifySessionMiddleware,
        clientNoOfSubscriptionRoute
    ); //Listing clients with number of subscriptions

    router.get(
        "/business/:businessId/client/:clientId/clientDetailsDashboard",
        verifySessionMiddleware,
        clientPendingAmountRoute
    ); //Listing clients with number of subscriptions
    router.get(
        "/business/:businessId/client/:clientId/recordAttendance",
        verifySessionMiddleware,
        clientRecordAttendanceRoute
    ); //Listing clients with number of subscriptions

    //For Offers
    router.post("/offers", verifySessionMiddleware, createOfferRoute); //Adding new offer
    router.get(
        "/business/:businessId/offers",
        verifySessionMiddleware,
        listAllOfferRoute
    ); //Listing all the offers under one business
    router.get(
        "/center/:centerId/offers",
        verifySessionMiddleware,
        listOfferRoute
    ); //Listing all the offers under one center
    router.get("/offers/:id", verifySessionMiddleware, offerDetailsRoute); //Returning one offer details
    router.put("/offers/:id", verifySessionMiddleware, updateOfferRoute); //Updating offer details
    router.patch("/offers/:id", verifySessionMiddleware, removeOfferRoute); //Removing offers

    //For Subscriptions
    router.post(
        "/subscriptions",
        verifySessionMiddleware,
        createSubscriptionRoute
    ); //Adding new subscription details

    router.get(
        "/currentSubscription/:page/:order/*",
        verifySessionMiddleware,
        currentSubscriptionRoute
    ); //Current Subscription

    router.get(
        "/pastSubscription/:page/:order/*",
        verifySessionMiddleware,
        pastSubscriptionRoute
    ); //Past Subscription

    router.get(
        "/business/:businessId/subscription/:searchKeyword",
        verifySessionMiddleware,
        searchSubscriptionRoute
    ); //Search subscriptions based on clients' namee, Pricepack and services

    router.put(
        "/subscriptions/:subscriptionId/settings",
        verifySessionMiddleware,
        settingsSubscriptionRoute
    ); //Updating subscription's details Subscription Settings

    // Client Payment, Payment Record
    router.post(
        "/clientpayment",
        verifySessionMiddleware,
        createClientPaymentRoute
    ); //For adding new client

    router.post(
        "/paymentReminderAction/",
        verifySessionMiddleware,
        paymentReminderAction
    );

    router.post(
        "/business/:businessId/renewalSubscription",
        verifySessionMiddleware,
        renewalSubscriptionRoute
    ); //Renewal Subscription

    router.get(
        "/listrenewalHistorySubcrition/:serviceId/:page/:order/*",
        verifySessionMiddleware,
        listrenewalHistorySubcrition
    );

    router.get(
        "/getSubcritionRenewalDetails/:serviceId/*",
        verifySessionMiddleware,
        getSubcritionRenewalDetails
    );
    // list renewal History Subcrition

    //getting the pricepacks under a service. it will be required at renewl subscription. user can only renewl to a new pricepack which belongs from the old service
    router.get(
        "/service/:serviceId/pricepacks",
        verifySessionMiddleware,
        listOfPricepacksUnderServiceRoute
    );

    router.get(
        "/business/:businessId/subscriptions",
        verifySessionMiddleware,
        listAllSubscriptionRoute
    ); //Listing all the subscriptions under one business
    router.get(
        "/center/:centerId/subscriptions",
        verifySessionMiddleware,
        listSubscriptionRoute
    ); //Listing all the subscriptions under one center
    router.get(
        "/client/:clientId/subscriptions",
        verifySessionMiddleware,
        clientSubscriptionRoute
    ); //Listing all the subscriptions of one client
    router.get(
        "/subscriptions/:subscriptionId",
        verifySessionMiddleware,
        SubscriptionDetailsRoute
    ); //Returning one subscription's details
    router.put(
        "/subscriptions/:subscriptionId",
        verifySessionMiddleware,
        updateSubscriptionRoute
    ); //Updating subscription's details
    router.patch(
        "/removeSubscriptions/:subscriptionId",
        verifySessionMiddleware,
        removeSubscriptionRoute
    ); //Removing subscription

    router.get(
        //"/business/:businessId/client/:clientId/subscriptions",
        "/business/:businessId/client/:clientId/subscriptions",
        verifySessionMiddleware,
        clientScheduleSubscriptionRoute
    ); //Listing all the subscriptions with schedule details of a client

    //Batch
    router.post("/batch", verifySessionMiddleware, createBatchRoute); //Adding new Batch details
    router.get(
        "/center/:centerId/:serviceId/batch",
        verifySessionMiddleware,
        listAllBatchRoute
    ); //Listing all the batch under one center
    router.get("/batch/:batchId", verifySessionMiddleware, BatchDetailsRoute); //Listing all the batch of one client
    router.put("/batch/:batchId", verifySessionMiddleware, updateBatchRoute); //Updating subscription's details
    router.patch("/batch/:batchId", verifySessionMiddleware, removeBatchRoute); //Removing subscription

    //schedule
    router.post("/schedule", verifySessionMiddleware, createScheduleRoute); //Create new schedule details old json

    router.post("/addschedule", verifySessionMiddleware, newcreateScheduleRoute); //Create new schedule details new json

    router.post(
        "/schedule/:scheduleId/addnewclients",
        verifySessionMiddleware,
        addNewClientsScheduleRoute
    ); //For adding new clients after creating the schedule

    //router.get("/schedule", verifySessionMiddleware, listAllScheduleRoute); //For listing all the schedule

    // New Booking Management Dashboard depend on schedules
    router.get("/schedule", verifySessionMiddleware, scheduleDashboardRoute);

    router.get(
        "/schedule/:scheduleId",
        verifySessionMiddleware,
        scheduleDetailRoute
    ); //Returning detail of one schedule
    router.put(
        "/schedule/:scheduleId",
        verifySessionMiddleware,
        updateScheduleRoute
    ); //For updating schedule's details
    router.patch(
        "/schedule/:scheduleId",
        verifySessionMiddleware,
        removeScheduleRoute
    ); //For deleting schedule's details
    router.get(
        "/business/:businessId/date/:date/schedules",
        verifySessionMiddleware,
        showScheduleOnDateRoute
    ); //list of schedules on specific date i.e calendar shcedule

    router.get(
        "/schedule/:scheduleId/showUserSchedules",
        verifySessionMiddleware,
        showUserSchedulesRoute
    );

    router.put(
        "/schedule/:scheduleId/userReschedule",
        verifySessionMiddleware,
        userRescheduleRoute
    );

    router.get(
        "/scheduleDashboard",
        verifySessionMiddleware,
        scheduleDashboardRoute
    );

    router.get(
        "/AttendanceHistory/filter/:filter/Search/*",
        verifySessionMiddleware,
        AttendanceHistory
    ); //For Attendance History

    router.get(
        "/attendancehistory/*/:booking_date/:schedule_time/*",
        verifySessionMiddleware,
        newattendanceHistoryRoute
    ); //For Attendance History New


    router.get(
        "/AttendanceHistory/Schedule/:scheduleID/search/*",
        verifySessionMiddleware,
        BookingAttendanceHistory
    ); //For  Booking  Attendance History

    //router
    router.get(
        "/schedule/:scheduleId/AttendanceHistory/*/:booking_date/:schedule_time/*",
        verifySessionMiddleware,
        newBookingAttendanceHistoryRoute
    ); //For  Booking  Attendance History

    router.get(
        "/business/:businessId/recordAttendance/booking",
        //"/business/:businessId/recordAttendance/booking/*/:booking_date/:schedule_time/*",
        verifySessionMiddleware,
        recordAttendanceBookingRoute
    ); // Record Attendance i.e Unmark Attendance Record

    router.get(
        "/business/:businessId/recordAttendance/booking/*/:booking_date/:schedule_time/*",
        verifySessionMiddleware,
        newrecordAttendanceBookingRoute
    ); // Record Attendance i.e Unmark Attendance Record New Code

    router.get(
        "/schedule/:scheduleId/client/:clientId/showSchedule/:date",
        verifySessionMiddleware,
        showSchedulesOfClientRoute
    );

    router.get(
        "/schedule/:scheduleId/center/:centerId/:date",
        verifySessionMiddleware,
        schedulesWithSameServiceRoute
    ); //For deleting schedule's details

    router.put(
        "/schedule/:scheduleId/client/:clientId/clientReschedule",
        verifySessionMiddleware,
        clientRescheduleRoute
    ); // Client Reschedule

    router.put(
        "/cancelSchedule/:flag",
        verifySessionMiddleware,
        cancelScheduleRoute
    ); //Cancel Schedule

    //Reschedule
    router.post("/reschedule", verifySessionMiddleware, createRescheduleRoute); //Adding new schedule details

    //Listing User details with BusinessId and all the center details
    // router.get(
    //   "/business/:businessId/allcenter",
    //   verifySessionMiddleware,
    //   listAllCenterUserRoute
    // ); //For listing all the Center under business

    //client schedule map
    router.post(
        "/clientschedulemap/:businessId",
        verifySessionMiddleware,
        clientScheduleMapRoute
    );

    router.get(
        "/schedule/:scheduleId/client",
        verifySessionMiddleware,
        showAllClientScheduleMapRoute
    );

    //team schedule map
    router.post(
        "/teamschedulemap/:businessId",
        verifySessionMiddleware,
        teamScheduleMapRoute
    );

    // Attendance
    router.get(
        "/business/:businessId/schedule/:scheduleId/attendanceDate/:attendanceDate/listAllAttendance/search/*",
        verifySessionMiddleware,
        listAllAttendanceRoute
    );

    // Attendance New
    router.get(
        "/schedule/:scheduleId/attendanceDate/:attendanceDate/:time/listAllAttendance/search/*",
        verifySessionMiddleware,
        newlistAllAttendanceRoute
    );

    router.get(
        "/business/:businessId/schedule/:scheduleId/attendanceDate/:attendanceDate/showAttendance",
        verifySessionMiddleware,
        showAttendanceRoute
    );
    router.put(
        "/schedule/:scheduleId/attendanceDate/:attendanceDate/updateMultipleAttendence",
        verifySessionMiddleware,
        newupdateAttendanceOfMultipleClientRoute
    );

    // Record Attendance New
    router.put(
        "/schedule/:scheduleId/attendanceDate/:attendanceDate/:time/updateMultipleRecordAttendence",
        verifySessionMiddleware,
        newupdateAttendanceOfMultipleClientRoute
    );

    router.get(
        "/business/:businessId/payments/type/:typeid",
        verifySessionMiddleware,
        listClientPaymentsnRoute
    );

    router.get(
        "/paymenthistory/:page/:order/*",
        verifySessionMiddleware,
        clientPaymentHistoryRoute
    ); //Client Payment History

    router.get(
        "/clientPaymenthistoryList/:clientId/:page/:order/*",
        verifySessionMiddleware,
        clientPaymenthistoryList
    ); //Client Payment History

    router.get(
        "/paymentreminders/:page/:order/*",
        verifySessionMiddleware,
        listClientPaymentRemindersnRoute
    );

    router.get(
        "/clientPaymentRemindersList/:clientId/:page/:order/*",
        verifySessionMiddleware,
        clientPaymentRemindersList
    );

    router.get(
        "/clientpaymentId/:clientpaymentId/ClientPaymentDetailsRoute/*",
        verifySessionMiddleware,
        clientPaymentdetails
    );

    router.get(
        "/business/:businessId/showClients/:option",
        verifySessionMiddleware,
        showClientsRoute
    ); //display option: 1 = active clients, option: 2 = inactive clients, option: 3 = top clients


    router.get(
        "/activeClientList/:page/:order/*",
        verifySessionMiddleware,
        activeClientListRoute
    ); //active Client List


    router.get(
        "/inactiveClientList/:page/:order/*",
        verifySessionMiddleware,
        inactiveClientListRoute
    ); //Inactive Client List

    router.get(
        "/listCurrentPastSubcriptionPerClient/:clientId/:page/:order/*",
        verifySessionMiddleware,
        listCurrentPastSubcriptionPerClient
    ); //List Current Past Subcription Per Client

    router.get(
        "/topClients/:page/:order/*",
        verifySessionMiddleware,
        topClientsRoute
    ); //Listing top clients

    router.get(
        "/expenseIncomeDashboard",
        verifySessionMiddleware,
        expenseIncomeDashboard
    );

    //Expenses
    router.post("/addExpense", verifySessionMiddleware, addExpenseRoute);
    router.get(
        "/business/:businessId/listExpense",
        verifySessionMiddleware,
        listExpenseRoute
    );
    router.get(
        "/getExpenseType/*",
        verifySessionMiddleware,
        getExpenseType
    );

    router.patch(
        "/removeExpense/:clientexpenseId",
        verifySessionMiddleware,
        removeExpenseRoute
    );

    router.get(
        "/business/:businessId/:dateOfPayment/YM/:YM/IE/:IE/:page/*",
        verifySessionMiddleware,
        expenseMonthRoute
    );


    router.get(
        "/listRecentIncome/:page/*",
        verifySessionMiddleware,
        listRecentIncomeRoute
    ); //Returning of recent incomes

    router.get(
        "/listRecentExpense/:page/:expence_order/:price_order/*",
        verifySessionMiddleware,
        listRecentExpenseRoute
    );

    // User Dashboard
    router.get(
        "/business/:businessId/dashboard",
        verifySessionMiddleware,
        userDashboardRoute
    );

    // View Center
    router.get(
        "/viewCenter",
        verifySessionMiddleware,
        viewCenterRoute
    );

    // Notifications
    router.get("/notification", verifySessionMiddleware, notification);

    router.post("/businessSettingUP", verifySessionMiddleware, businessSettingUP);
    router.get("/businessSettingDetails/", verifySessionMiddleware, businessSettingDetails);
    router.post("/listservicepricepack", verifySessionMiddleware, listservicepricepack);

    router.get(
        "/service/:serviceId/clientList/scheduleId/*",
        verifySessionMiddleware,
        clientListRoute
    ); //Returning detail of one offering

    router.post(
        "/service/:serviceId/newclientList",
        verifySessionMiddleware,
        newclientListRoute
    ); //Returning detail of one offering

    router.get(
        "/client/:clientId/showClientPricePackList",
        verifySessionMiddleware,
        showClientPricePackList
    );

    // Client Report
    router.get(
        "/schedule/:scheduleId/client/:clientId/clientAttendanceReport/:reportType",
        verifySessionMiddleware,
        showClientAttendanceRoute
    );
    router.get(
        "/schedule/:scheduleId/client/:clientId/clientPaymentReport/:reportType",
        verifySessionMiddleware,
        showClientPaymentRoute
    );
    router.get(
        "/schedule/:scheduleId/client/:clientId/clientPaymentReport/:reportType/share",
        verifySessionMiddleware,
        showClientPaymentRouteShare
    );
    router.post(
        "/clientReportPaymentAndAttendance/*",
        verifySessionMiddleware,
        clientReportPaymentAndAttendance
    );

    router.get(
        "/ClientReports/client/:clientId/:page/*",
        verifySessionMiddleware,
        ClientReports
    );

    // Report Client List
    router.get('/reportclientlist/:page/:order/*', verifySessionMiddleware, reportclientlist);

    router.get(
        "/getAttendanceHistory/:page/:booking_date/:schedule_time/*",
        verifySessionMiddleware,
        getAttendanceHistory
    ); //For Attendance History

    // Cient Booking
    router.get(
        "/client/:clientId/clientBookings/page/*/filter/*/search/*",
        verifySessionMiddleware,
        clientBookingRoute
    );

    // Client Booking Details
    router.get(
        "/client/:clientId/schedule/:scheduleId/clientBookingDetails/page/*",
        verifySessionMiddleware,
        clientBookingDetailsRoute
    );

    // Client Edit Booking
    router.put(
        "/schedule/:scheduleId/clientId/:clientId/clienteditbooking",
        verifySessionMiddleware,
        Client_Edit_booking
    );

    // Client Schedule Data
    router.get(
        "/schedule/:scheduleId/client/:clientId/showClientSchedules/:date",
        verifySessionMiddleware,
        showClientSchedulesRoute
    );

    // Client Attendance history
    router.get(
        "/business/:businessId/Cientid/:clientId/ClientAttendanceHistory/*/:booking_date/:schedule_time/*",
        verifySessionMiddleware,
        ClientAttendanceHistory
    );

    // Client Booking Attendence Record
    router.get(
        "/business/:businessId/schedule/:scheduleId/clientId/:clientId/listAllAttendance/*",
        verifySessionMiddleware,
        listClientAttendanceRoute
    );

    // Client Note

    router.post('/addNote', verifySessionMiddleware, addNote);
    router.get('/allNotes/:clientId/:page', verifySessionMiddleware, allNotes);
    router.get('/singleNote/:noteId', verifySessionMiddleware, singleNote);
    router.put('/updateNote', verifySessionMiddleware, updateNote);
    router.post('/searchNote', verifySessionMiddleware, searchNote);
    router.patch('/deleteNote/:noteId', verifySessionMiddleware, deleteNote);

    // Message
    router.post('/addMessage', verifySessionMiddleware, addMessage);
    router.get('/messageList/:senderId/:page', verifySessionMiddleware, messageList);
    //===================Populate Client List By SericeId(Multiple)=======================
    router.get('/clientListOfService/:serviceId', verifySessionMiddleware, getClientsByServiceIds);

    router.get('/clientListOfService/:serviceId/:page', verifySessionMiddleware, getClientsByServiceIds);
    router.put('/updateMessage/:messageId', verifySessionMiddleware, updateMessage);
    router.get('/messageDetails/:messageId', verifySessionMiddleware, messageDetails);
    router.delete('/deleteMessage/:messageId', verifySessionMiddleware, deleteMessage);
    router.get('/messageTypes', verifySessionMiddleware, messageTypes);
    // Unschedule Client List
    router.get('/unscheduleClientList/:page', verifySessionMiddleware, subscribedClientList);

    // Schedule On Same ServiceandCenter
    router.get(
        "/ScheduleOnSameServiceandCenter/schedule/:scheduleId/center/*",
        verifySessionMiddleware,
        ScheduleOnSameServiceandCenter
    );

    router.get(
        "/schedule/:scheduleId/center/:centerId/:date/clientId/:clientId",
        verifySessionMiddleware,
        schedulesWithSameServicebutClientRoute
    );

    // Insert Multiple Clients from phone contact list
    router.post("/clientMultiple", verifySessionMiddleware, createMultipleClientRoute); //For adding multiple client

    // Admin Section

    router.get('/adminClientList/:page', verifySessionMiddleware, adminClientList);

    //package search
    router.post("/createadminpackage", verifySessionMiddleware, createKnackAdminPackageRoute);
    router.post("/changeKnackAdminPackageStatus", verifySessionMiddleware, changeKnackAdminPackageRoute); //For block/unblock Package
    router.post("/removeKnackAdminPackage", verifySessionMiddleware, removeKnackAdminPackageRoute); //For remove Package
    router.get("/listknackadminpackage", verifySessionMiddleware, listAllKnackAdminPackageRoute); //For listing all the Package
    router.post("/searchPackages", verifySessionMiddleware, searchKnackAdminPackageRoute)

    //knack admin user

    router.post("/loginKnackAdmin", LoginKnackAdminAuthenticate); //For authenticating and login

    router.get("/allKnackUser/:businessId/listAllKnackUser", verifySessionMiddleware, listAllKnackUsers);
    router.post("/searchPrevSubscription", verifySessionMiddleware, getPreviousSubscription);
    router.get("/teams", verifySessionMiddleware, listAllKnackTeams);
    router.post("/addNewTeamMembers", verifySessionMiddleware, addNewTeamMember);
    router.post("/changeMemberStatus", verifySessionMiddleware, changeTeamMemberStatus);
    router.post("/editTeamMembers", verifySessionMiddleware, editTeamMemberRoute);
    router.post("/getTeamMembersRecord", verifySessionMiddleware, getTeamMemberRoute);

    router.post("/searchAdminUser", verifySessionMiddleware, searchKnackAdminUser);
    router.post("/searchKnackTeamMembers", verifySessionMiddleware, searchAllKnackTeamMember);
    router.post("/blockKnackUser", verifySessionMiddleware, blockAdminUserRoute);

    //Admin Leads
    router.post("/addKnackLeads", verifySessionMiddleware, addKnackNewLead);
    //router.post("/editKnackLeads", verifySessionMiddleware, updateKnackLeadRoute);
    router.post("/updateKnackLead", verifySessionMiddleware, updateKnackLeadRoute);
    router.post("/deleteLead", verifySessionMiddleware, deleteLeadRoute);
    router.post("/viewLead", verifySessionMiddleware, viewKnackLeadRoute);
    router.post("/searchLeads", verifySessionMiddleware, searchKnackLeadRoute);
    router.get("/allKnackLead", verifySessionMiddleware, viewKnackAllLeadRoute);

    //Notes
    router.post("/addNewNotes", verifySessionMiddleware, addNewNotesRoute);
    router.post("/removeNotes", verifySessionMiddleware, removeNotesRoute);
    router.post("/listNotes", verifySessionMiddleware, listsNotesRoute);

    //Activity
    router.post("/addNewActivity", verifySessionMiddleware, addNewActivityRoute);
    router.post("/deleteActivity", verifySessionMiddleware, removeActivityRoute);
    router.post("/viewactivity", verifySessionMiddleware, viewActivityRoute)
    router.post("/saveActivity", verifySessionMiddleware, saveActivityRoute);
    router.get("/allActivityAssignee", verifySessionMiddleware, getKnackAllAssigneeRoute);
    router.post("/saveServey", verifySessionMiddleware, saveServeyRoute);
    router.post("/getServeyData", verifySessionMiddleware, getServeyRoute);

    //ENDS HERE
    // ******************
    // * ERROR HANDLING *
    // ******************

    // 404 route
    router.all("/*", (req, res, next) => {
        next(new ApplicationError("Not Found", 404));
    });

    // catch all ApplicationErrors, then output proper error responses.
    //
    // NOTE: express relies on the fact the next line has 4 args in
    // the function signature to trigger it on errors. So, don't
    // remove the unused arguments!
    router.use((err, req, res, next) => {
        if (err instanceof ApplicationError) {
            res.status(err.statusCode).send({
                // message: err.message,
                data: err.data || {},
                error: { errorMessage: err.message, errorCode: err.statusCode }
            });
            return;
        }
        // If we get here, the error could not be handled.
        // Log it for debugging purposes later.
        console.error(err);

        res.status(500).send({
            // message: "Uncaught error"
            data: err.data || {},
            error: { errorMessage: "Something went wrong", errorCode: 500 }
        }); // uncaught exception
        // res.status(200).send({
        //   error: { errorMessage: "Success", errorCode: 200 },
        //   data: err.data || {}
        // });
    });

    // *******************
    // * CATCH ALL ROUTE *
    // *******************

    /**
     * If you want all other routes to render something like a one-page
     * frontend app, you can do so here; else, feel free to delete
     * the following comment.
     */
    /*
     * function renderFrontendApp(req, res, next) {
     *   // TODO: add code to render something here
     * }
     * // all other pages route to the frontend application.
     * router.get('/*', renderFrontendApp);
     */

    return router;
}