import request from "request";
const nodemail = require('nodemailer');
import { KNACKDB, EMAIL, KNACK_UPLOAD_URL, TRIAL_PERIOD, HELPLINE } from "../lib/constants.js";

//message
export async function textNotification(textData, cellNumber) {
    try {
        //sending password on mobile phone
        var mobileNumber = cellNumber;
        var sms_api =
            " http://api-alerts.solutionsinfini.com/v3/?method=sms&api_key=A31cf85c3cc3b65100bf9bd7fbe30cd90&to=+91_MOBILE&sender=SIDEMO&message=_MESSAGE";
        var url = "";

        let text = textData;
        url = sms_api.replace("_MOBILE", mobileNumber);
        url = url.replace("_MESSAGE", encodeURIComponent(text));
        request(url, function (err) {
            if (err) {
                throw err;
            }
        });
    } catch (error) {
        throw error;
    }

}

//email
export async function mailNotification(subject, textData, emailId) {
    try {
        //........Mailing............

        let transporter = nodemail.createTransport({
            //service: "gmail",
            host: 'smtp.gmail.com',
            port: 465,
            auth: {
                user: EMAIL.userEmail,
                pass: EMAIL.password
            }
        });

        //..Mailing Contents...
        let mailOptions = {
            from: '"KNACK" <' + EMAIL.userEmail + '>',
            to: emailId, // list of receivers
            subject: subject, // Subject line
            html: textData

        };

        //...Sending Mail...
        transporter.sendMail(mailOptions, (err, info) => {
            if (err) {
                return console.log(err);
            }
            console.log("Payment Reminder Email Sent!");
        });

    } catch (error) {
        throw error;
    }

}