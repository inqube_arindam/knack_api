import Sequelize from "sequelize";
import lodash from "lodash";
import nodemailer from "nodemailer";
import { KNACKDB, EMAIL, KNACK_UPLOAD_URL } from "../lib/constants.js";
import request from "request";
import { ApplicationError } from "../lib/errors";
import loginSignUpHelperDB from "../db/HelperModels/LoginSignUpHelperModel";

var fs = require("fs");
var path = require('path');
// var os = require('os');

export function filterFields(toFilter, allowedFields, showCreate = false) {
    let data = [];
    if (showCreate) {
        toFilter = Array(toFilter);
    }
    toFilter.forEach(function(element) {
        data.push(
            allowedFields.reduce((memo, field) => {
                if (field.indexOf(".") != -1) {
                    var segments = field.split(".");
                    return {
                        ...memo,
                        [segments[1]]: element[field]
                    };
                } else {
                    return {
                        ...memo,
                        [field]: element[field]
                    };
                }
            }, {})
        );
    });
    return data;
}

export async function sendEmail__(emailId, subject, message, attachments = {}) {
    let mailOptions = {};
    var transporter = nodemailer.createTransport({
        service: "gmail",
        auth: {
            user: EMAIL.userEmail,
            pass: EMAIL.password
        }
    });
    if (Object.keys(attachments).length > 0) {
        mailOptions = {
            from: EMAIL.userEmail,
            to: emailId,
            subject: subject,
            text: message,
            attachments: [attachments]
        };
    } else {
        mailOptions = {
            from: EMAIL.userEmail,
            to: emailId,
            subject: subject,
            text: message
        };
    }
    return await transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        return console.log("Mail Send");
    });
}

export async function sendEmail(emailId, subject, message, attachments = {}) {
    let mailOptions = {};
    var transporter = nodemailer.createTransport({
        service: "gmail",
        auth: {
            user: EMAIL.userEmail,
            pass: EMAIL.password
        }
    });
    if (Object.keys(attachments).length > 0) {
        mailOptions = {
            from: '"KNACK" <' + EMAIL.userEmail + '>',
            to: emailId,
            subject: subject,
            html: message,
            attachments: [attachments]
        };
    } else {
        mailOptions = {
            from: '"KNACK" <' + EMAIL.userEmail + '>',
            to: emailId,
            subject: subject,
            html: message
        };
    }
    return await transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        return console.log("Mail Send");
    });
}

export function sendSmsMobile(mobileNumber, mseeage) {
    var sms_api =
        " http://api-alerts.solutionsinfini.com/v3/?method=sms&api_key=A31cf85c3cc3b65100bf9bd7fbe30cd90&to=+91_MOBILE&sender=SIDEMO&message=_MESSAGE";
    var url = "";
    url = sms_api.replace("_MOBILE", mobileNumber);
    url = url.replace("_MESSAGE", encodeURIComponent(mseeage));
    request(url, function(err, res) {
        if (err) {
            throw err;
        }
    });
}
export function sendSms(mobileNumber, vcode, name) {
    var sms_api =
        " http://api-alerts.solutionsinfini.com/v3/?method=sms&api_key=A31cf85c3cc3b65100bf9bd7fbe30cd90&to=+91_MOBILE&sender=SIDEMO&message=_MESSAGE";
    var url = "";
    let text = 'Hi ' + name + ', welcome to Knack. Your OTP is ' + vcode + '. Enter this in the app to verify your account.';
    url = sms_api.replace("_MOBILE", mobileNumber);
    url = url.replace("_MESSAGE", encodeURIComponent(text));
    request(url, function(err) {
        if (err) {
            throw err;
        }
    });
}

export function forgotPasswordSms(user, vcode) {
    var sms_api =
        " http://api-alerts.solutionsinfini.com/v3/?method=sms&api_key=A31cf85c3cc3b65100bf9bd7fbe30cd90&to=+91_MOBILE&sender=SIDEMO&message=_MESSAGE";
    var url = "";
    let text = 'Hi ' + user.name + ', to reset your password, an OTP will be sent to your registered email ID. Enter the OTP in the app to proceed and reset the password.';
    url = sms_api.replace("_MOBILE", user.contactNumber);
    url = url.replace("_MESSAGE", encodeURIComponent(text));
    request(url, function(err) {
        if (err) {
            throw err;
        }
    });
}

export function sendSms__(mobileNumber, vcode) {
    var sms_api =
        " http://api-alerts.solutionsinfini.com/v3/?method=sms&api_key=A31cf85c3cc3b65100bf9bd7fbe30cd90&to=+91_MOBILE&sender=SIDEMO&message=_MESSAGE";
    var url = "";
    var text = vcode + " is your One Time Password (OTP) for Knack";
    url = sms_api.replace("_MOBILE", mobileNumber);
    url = url.replace("_MESSAGE", encodeURIComponent(text));
    request(url, function(err) {
        if (err) {
            throw err;
        }
    });
}
export function createInstance(schemas, databasename) {
    var arrayOfStrings = databasename.split(",");
    // if(os.homedir() == '/home/ubuntu'){
    //     arrayOfStrings[0] = KNACKDB.host;
    // }
    const Op = Sequelize.Op;
    var sequelize = new Sequelize(
            arrayOfStrings[1],
            KNACKDB.user,
            KNACKDB.password, {
                host: arrayOfStrings[0],
                port: 3306,
                dialect: "mysql",
                operatorsAliases: Op,
                pool: {
                    max: 100,
                    min: 0,
                    idle: 10000
                }
            }
        ),
        db = {};
    var model;
    schemas.forEach(function(element) {
        model = sequelize.import(
            "../schemas/ClientSchemas/" + element + ".schema.js"
        );
        db[model.name] = model;
    });
    return lodash.extend({
            sequelize: sequelize
        },
        db
    );
}

export function dumpTables(data) {
    const promise = require("bluebird");
    const fs = require("fs");
    const path = require("path");
    const assert = require("assert");
    const debug = require("debug")("my_new_api");
    const Aring = new RegExp(
        String.fromCharCode(65533) + "\\" + String.fromCharCode(46) + "{1,3}",
        "g"
    );
    const Auml = new RegExp(
        String.fromCharCode(65533) + String.fromCharCode(44) + "{1,3}",
        "g"
    );
    const Ouml = new RegExp(
        String.fromCharCode(65533) + String.fromCharCode(45) + "{1,3}",
        "g"
    );
    var last_sql;
    var filename = __dirname + "/knack_users.sql";
    var actions = [];
    var queries = fs
        .readFileSync(filename)
        .toString()
        .split(/;\n/);

    for (let i in queries) {
        try {
            if (queries[i]) {
                let trimVal = queries[i].trim();
                if (((trimVal.length) == 0) || (queries[i].match(new RegExp("/\\*!40101 .+ \\*/")))) {
                    continue;
                }
            }
        } catch (err) {
            console.log(err)
        }
        // if (
        //     queries[i].trim().length == 0 ||
        //     queries[i].match(new RegExp("/\\*!40101 .+ \\*/"))
        // ) {
        //     continue;
        // }
        let clean_query = queries[i]
            .replace(Aring, "Å")
            .replace(Ouml, "Ö")
            .replace(Auml, "Ä");
        actions.push({
            query: clean_query.substring(0, 200),
            exec: () => data.query(clean_query)
        });
    }
    return promise.mapSeries(
        actions,
        function(item) {
            debug(item.query);
            return item.exec();
        }, { concurrency: 1 }
    );
}

export async function clientInfo(information, getobj) {
    try {
        var db = createInstance(["client"], information);
        let getInfo = information.split(",");
        let dbBusnessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusnessId };
        const setCond = Object.assign(getobj, setDbid);
        var clients = await db.clients.findAll({
            where: setCond,
            raw: true
        });
        db.sequelize.close();
        return clients;
    } catch (error) {
        throw error;
    }
}


export async function sessionInfo_old(information, getObj) { //returns session details
    try {
        let getInfo = information.split(",");
        let dbBusnessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusnessId };
        const setCond = Object.assign(getObj, setDbid);

        //getting all the subscriptions under the business
        let dbsub = createInstance(["clientSubscription"], information);
        let subscriptions = await dbsub.subscriptions.findAll({
            where: setCond,
            raw: true
        }); //code ends here
        console.log(subscriptions);
        var info = [];
        for (var i = 0; i < subscriptions.length; i++) {
            let dbCSMap = createInstance(["clientScheduleMap"], information);
            //getting the scheduleId from the clientScheduleMap table
            let getSchedule = await dbCSMap.clientschedulemap.findAll({
                where: {
                    status: 1,
                    subscriptionId: subscriptions[i].subscriptionId
                },
                raw: true
            }); //code ends here

            var sessionAtd = subscriptions[i].totalSessionsAttended;
            if (getSchedule[0]) { //if the subscription id exists in clientScheduleMap table

                //getting pricepack name
                let dbPP = createInstance(["clientPricepack"], information);
                let pricepacks = await dbPP.pricepacks.findAll({
                    where: {
                        status: 1,
                        pricepackId: subscriptions[i].pricepacks
                    },
                    raw: true
                });
                console.log(pricepacks);
                var sessionTotal = pricepacks[0].serviceDuration;
                var sessionRemain = sessionTotal - sessionAtd; //client will get remaining "sessionRemain"

                //calculating attendance values of a particular client
                var attendanceKeys = [];
                var attendanceValues = [];
                let x = getSchedule[0].attendanceType;
                for (let i = 0; i < x.length; i++) {
                    attendanceKeys.push(x[i]["scdate"]);
                    attendanceValues.push(x[i]);
                }

                var countPresent = 0,
                    countAbsent = 0,
                    countExcused = 0,
                    countUnmarked = 0;
                var countTotal = attendanceKeys.length;

                for (var k = 0; k < attendanceKeys.length; k++) {

                    if (attendanceValues[k]["atd"] == 1) countPresent++;
                    else if (attendanceValues[k]["atd"] == 2) countAbsent++;
                    else if (attendanceValues[k]["atd"] == 3) countExcused++;
                    else if (attendanceValues[k]["atd"] == 4) countUnmarked++;
                }
                //calculation ends

                var markedAtd = countPresent + countAbsent;
                //add exception markedAtd can't be more than sessionRemain

                var totalAtd = markedAtd + sessionAtd;
                var remainAtd = sessionTotal - totalAtd;
                var details = [];
                Array.prototype.push.apply(details, [{ businessId: getSchedule[0].businessId }]);
                Array.prototype.push.apply(details, [{ scheduleId: getSchedule[0].scheduleId }]);
                Array.prototype.push.apply(details, [{ clientId: getSchedule[0].clientId }]);
                Array.prototype.push.apply(details, [{ subscriptionId: getSchedule[0].subscriptionId }]);
                Array.prototype.push.apply(details, [{ pricepackId: pricepacks[0].pricepackId }]);
                Array.prototype.push.apply(details, [{ totalSession: sessionTotal }]);
                Array.prototype.push.apply(details, [{ attendedSession: totalAtd }]);
                var merged = {};
                merged = Object.assign.apply(Object, details);
                info = info.concat(merged);
            }
        }
        return await info;
    } catch (error) {
        throw error;
    }
}

export async function sessionInfo__(information, getObj) { //returns session details
    try {
        let dbSchedule = createInstance(["clientSchedule"], information);
        let dbCSM = createInstance(["clientScheduleMap"], information);
        let dbSPM = createInstance(["servicePricepackMap"], information);
        let dbSubscription = createInstance(["clientSubscription"], information);
        let dbPricepack = createInstance(["clientPricepack"], information);
        let dbCenter = createInstance(["clientCenter"], information);
        dbCSM.clientschedulemap.belongsTo(dbSchedule.schedule, {
            foreignKey: "scheduleId",
            targetKey: "scheduleId"
        });
        dbSchedule.schedule.belongsTo(dbCenter.centers, {
            foreignKey: "centerId",
            targetKey: "centerId"
        });

        let getInfo = information.split(",");
        let dbBusnessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusnessId };
        const setCond = Object.assign(getObj, setDbid);

        let subscriptions = await dbSubscription.subscriptions.findAll({
            where: setCond,
            raw: true
        });

        let info = [];
        for (let subscription of subscriptions) {
            let getSchedule = await dbCSM.clientschedulemap.findAll({
                where: {
                    status: 1,
                    subscriptionId: subscription.subscriptionId
                },
                include: [{
                    model: dbSchedule.schedule,
                    attributes: ['serviceId', 'centerId'],
                    include: [{
                        model: dbCenter.centers,
                        attributes: ['centerId', 'centerName']
                    }]
                }],
                raw: true
            });
            let sessionAttended = subscription.totalSessionsAttended;
            if (getSchedule[0]) { //if the subscription id exists in clientScheduleMap table
                let spm = await dbSPM.servicepricepackmap.find({
                    where: {
                        status: 1,
                        pricepackId: subscription.pricepacks,
                        serviceId: getSchedule[0]['schedule.serviceId']
                    },
                    raw: true
                });
                let serviceDuration = 0;
                if (spm != null) {
                    serviceDuration = spm.serviceDuration;
                }
                let pricepack = await dbPricepack.pricepacks.find({
                    where: {
                        status: 1,
                        pricepackId: subscription.pricepacks
                    },
                    raw: true
                });
                let countPresent = 0,
                    countAbsent = 0;
                for (let clientAttendance of getSchedule[0].attendanceType) {
                    if (clientAttendance.atd == 1) countPresent++;
                    else if (clientAttendance.atd == 2) countAbsent++;
                }
                let markedAttendance = countPresent + countAbsent;
                var totalSessionAttended = markedAttendance + sessionAttended;
                var details = [];
                Array.prototype.push.apply(details, [{ businessId: getSchedule[0].businessId }]);
                Array.prototype.push.apply(details, [{ scheduleId: getSchedule[0].scheduleId }]);
                Array.prototype.push.apply(details, [{ clientId: getSchedule[0].clientId }]);
                Array.prototype.push.apply(details, [{ subscriptionId: getSchedule[0].subscriptionId }]);
                Array.prototype.push.apply(details, [{ pricepackId: pricepack.pricepackId }]);
                Array.prototype.push.apply(details, [{ serviceId: getSchedule[0]['schedule.serviceId'] }]);
                Array.prototype.push.apply(details, [{ centerId: getSchedule[0]['schedule.center.centerId'] }]);
                Array.prototype.push.apply(details, [{ centerName: getSchedule[0]['schedule.center.centerName'] }]);
                Array.prototype.push.apply(details, [{ totalSession: serviceDuration }]);
                Array.prototype.push.apply(details, [{ attendedSession: totalSessionAttended }]);
                Array.prototype.push.apply(details, [{ durationType: pricepack.durationType }]);
                Array.prototype.push.apply(details, [{
                    message: pricepack.durationType == 1 ? totalSessionAttended + "/" + serviceDuration + " Sessions Completed" : pricepack.durationType == 2 ? totalSessionAttended + " Days Completed" : pricepack.durationType == 3 ? totalSessionAttended + " Weeks Completed" : pricepack.durationType == 4 ? totalSessionAttended + " Months Completed" : pricepack.durationType == 5 ? totalSessionAttended + " Years Completed" : pricepack.durationType == 6 ? totalSessionAttended + " Items Completed" : pricepack.durationType == 7 ? totalSessionAttended + " Hours Completed" : ""
                }]);
                var merged = {};
                merged = Object.assign.apply(Object, details);
                info = info.concat(merged);
            }
        }
        dbSchedule.sequelize.close();
        dbCSM.sequelize.close();
        dbSPM.sequelize.close();
        dbSubscription.sequelize.close();
        dbPricepack.sequelize.close();
        dbCenter.sequelize.close();
        return await info;
    } catch (error) {
        throw error;
    }
}

export async function sessionInfo(information, getObj) { //returns session details
    try {
        let dbSchedule = createInstance(["clientSchedule"], information);
        let dbCSM = createInstance(["clientScheduleMap"], information);
        let dbSPM = createInstance(["servicePricepackMap"], information);
        let dbSubscription = createInstance(["clientSubscription"], information);
        let dbPricepack = createInstance(["clientPricepack"], information);
        let dbCenter = createInstance(["clientCenter"], information);
        dbCSM.clientschedulemap.belongsTo(dbSchedule.schedule, {
            foreignKey: "scheduleId",
            targetKey: "scheduleId"
        });
        dbSchedule.schedule.belongsTo(dbCenter.centers, {
            foreignKey: "centerId",
            targetKey: "centerId"
        });

        let getInfo = information.split(",");
        let dbBusnessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusnessId };
        const setCond = Object.assign(getObj, setDbid);

        let subscriptions = await dbSubscription.subscriptions.findAll({
            where: setCond,
            raw: true
        });

        let info = [];
        for (let subscription of subscriptions) {
            let pricepack = await dbPricepack.pricepacks.find({
                where: {
                    status: 1,
                    pricepackId: subscription.pricepacks
                },
                raw: true
            });

            let getSchedule = await dbCSM.clientschedulemap.findAll({
                where: {
                    status: 1,
                    subscriptionId: subscription.subscriptionId
                },
                include: [{
                    model: dbSchedule.schedule,
                    attributes: ['serviceId', 'centerId'],
                    include: [{
                        model: dbCenter.centers,
                        attributes: ['centerId', 'centerName']
                    }]
                }],
                raw: true
            });

            let sessionAttended = subscription.totalSessionsAttended;
            if (getSchedule[0]) { //if the subscription id exists in clientScheduleMap table
                let spm = await dbSPM.servicepricepackmap.find({
                    where: {
                        status: 1,
                        pricepackId: subscription.pricepacks,
                        serviceId: getSchedule[0]['schedule.serviceId']
                    },
                    raw: true
                });
                let serviceDuration = 0;
                if (spm != null) {
                    serviceDuration = spm.serviceDuration;
                }
                let countPresent = 0,
                    countAbsent = 0;
                for (let clientAttendance of getSchedule[0].attendanceType) {
                    if (clientAttendance.atd == 1) countPresent++;
                    else if (clientAttendance.atd == 2) countAbsent++;
                }
                let markedAttendance = countPresent + countAbsent;
                var totalSessionAttended = markedAttendance + sessionAttended;
                let remainingSession=pricepack.serviceDuration-totalSessionAttended;
                var details = [];
                Array.prototype.push.apply(details, [{ businessId: getSchedule[0].businessId }]);
                Array.prototype.push.apply(details, [{ scheduleId: getSchedule[0].scheduleId }]);
                Array.prototype.push.apply(details, [{ clientId: getSchedule[0].clientId }]);
                Array.prototype.push.apply(details, [{ subscriptionId: getSchedule[0].subscriptionId }]);
                Array.prototype.push.apply(details, [{ pricepackId: pricepack.pricepackId }]);
                Array.prototype.push.apply(details, [{ pricepackName: pricepack.pricepackName }]);
                Array.prototype.push.apply(details, [{ serviceId: getSchedule[0]['schedule.serviceId'] }]);
                Array.prototype.push.apply(details, [{ centerId: getSchedule[0]['schedule.center.centerId'] }]);
                Array.prototype.push.apply(details, [{ centerName: getSchedule[0]['schedule.center.centerName'] }]);
                Array.prototype.push.apply(details, [{ totalSession: pricepack.serviceDuration }]);
                Array.prototype.push.apply(details, [{ serviceSession: serviceDuration }]);
                Array.prototype.push.apply(details, [{ attendedSession: totalSessionAttended }]);
                Array.prototype.push.apply(details, [{ remainingSession: remainingSession }]);
                Array.prototype.push.apply(details, [{ durationType: pricepack.durationType }]);
                // Array.prototype.push.apply(details, [{
                //     message: pricepack.durationType == 1 ? totalSessionAttended + "/" + pricepack.serviceDuration + " Sessions Completed" : pricepack.durationType == 2 ? totalSessionAttended + " Days Completed" : pricepack.durationType == 3 ? totalSessionAttended + " Weeks Completed" : pricepack.durationType == 4 ? totalSessionAttended + " Months Completed" : pricepack.durationType == 5 ? totalSessionAttended + " Years Completed" : pricepack.durationType == 6 ? totalSessionAttended + " Items Completed" : pricepack.durationType == 7 ? totalSessionAttended + " Hours Completed" : ""
                // }]);
                Array.prototype.push.apply(details, [{
                    message: pricepack.durationType == 1 ? totalSessionAttended + "/" + pricepack.serviceDuration + " Sessions Completed" : totalSessionAttended + " Days Completed"
                }]);
                var merged = {};
                merged = Object.assign.apply(Object, details);
                info = info.concat(merged);
            } else {
                var details = [];
                Array.prototype.push.apply(details, [{ businessId: "" }]);
                Array.prototype.push.apply(details, [{ scheduleId: "" }]);
                Array.prototype.push.apply(details, [{ clientId: "" }]);
                Array.prototype.push.apply(details, [{ subscriptionId: subscription.subscriptionId }]);
                Array.prototype.push.apply(details, [{ pricepackId: pricepack.pricepackId }]);
                Array.prototype.push.apply(details, [{ pricepackName: pricepack.pricepackName }]);
                Array.prototype.push.apply(details, [{ serviceId: "" }]);
                Array.prototype.push.apply(details, [{ centerId: "" }]);
                Array.prototype.push.apply(details, [{ centerName: "" }]);
                Array.prototype.push.apply(details, [{ totalSession: pricepack.serviceDuration }]);
                Array.prototype.push.apply(details, [{ attendedSession: 0 }]);
                Array.prototype.push.apply(details, [{ remainingSession: 0 }]);
                Array.prototype.push.apply(details, [{ durationType: pricepack.durationType }]);
                // Array.prototype.push.apply(details, [{
                //     message: pricepack.durationType == 1 ? "0/" + pricepack.serviceDuration + " Sessions Completed" : pricepack.durationType == 2 ? "0 Days Completed" : pricepack.durationType == 3 ? "0 Weeks Completed" : pricepack.durationType == 4 ? "0 Months Completed" : pricepack.durationType == 5 ? "0 Years Completed" : pricepack.durationType == 6 ? "0 Items Completed" : pricepack.durationType == 7 ? "0 Hours Completed" : ""
                // }]);
                Array.prototype.push.apply(details, [{
                    message: pricepack.durationType == 1 ? "0/" + pricepack.serviceDuration + " Sessions Completed" : "0 Days Completed"
                }]);
                var merged = {};
                merged = Object.assign.apply(Object, details);
                info = info.concat(merged);
            }
        }
        dbSchedule.sequelize.close();
        dbCSM.sequelize.close();
        dbSPM.sequelize.close();
        dbSubscription.sequelize.close();
        dbPricepack.sequelize.close();
        dbCenter.sequelize.close();
        return await info;
    } catch (error) {
        throw error;
    }
}

export async function bookingInfo(information, getobj, date) { //returns number of schedule of a business
    try {

        let getInfo = information.split(",");
        let dbBusnessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusnessId };
        const setCond = Object.assign(getobj, setDbid);

        var d = date.getDate();
        if (d >= 0 && d <= 9) d = "0" + d;
        var m = date.getMonth() + 1;
        if (m >= 0 && m <= 9) m = "0" + m;
        var y = date.getFullYear();
        var getDate = y + '-' + m + '-' + d;

        var sequelize = require("sequelize");
        let db = createInstance(["clientSchedule"], information);
        let schedules = await db.schedule.findAll({
            // attributes:
            //   ['businessId', [sequelize.fn('count', sequelize.col('scheduleId')), 'totalBooking']],
            // group: ["businessId"],
            where: setCond,
            raw: true
        });

        var countSchedule = 0;

        let dbCSM = createInstance(["clientScheduleMap"], information);
        for (var i = 0; i < schedules.length; i++) {
            let clients = await dbCSM.clientschedulemap.findAll({
                where: {
                    scheduleId: schedules[i].scheduleId
                },
                raw: true
            });

            if (clients[0]) {
                // var attendanceKeys = [];
                // Object.keys(clients[0].attendanceType).forEach(function (key) {
                //     attendanceKeys.push(key);
                // });

                // for (var k = 0; k < attendanceKeys.length; k++) {
                //     if (attendanceKeys[k] == getDate) {
                //         countSchedule++;
                //     }
                // }
                for (let k of clients[0].attendanceType) {
                    if (k.scdate == getDate) {
                        countSchedule++;
                    }
                }
            }
        }

        db.sequelize.close();
        return countSchedule;
    } catch (error) {
        throw error;
    }
}

export async function subscriptionInfo(information, setObj) { //returns past subscription details. pending work current subscription should be implemented here by using operator overloading
    try {

        let businessIdinformation = information.split(",")[1];
        let dbclientpayments = createInstance(["clientPayments"], information);
        let dbsubscriptions = createInstance(["clientSubscription"], information);
        let dbClients = createInstance(["client"], information);
        let dbpricepacks = createInstance(["clientPricepack"], information);
        let dbpaymentTransactions = createInstance(["paymentTransactions"], information);
        let dbservicepricepackmap = createInstance(["servicePricepackMap"], information);
        let dbservices = createInstance(["clientService"], information);
        let dbclientschedulemap = createInstance(["clientScheduleMap"], information);

        dbclientpayments.clientpayments.belongsTo(dbsubscriptions.subscriptions, {
            foreignKey: "subscriptionsId",
            targetKey: "subscriptionId"
        });
        dbsubscriptions.subscriptions.belongsTo(dbClients.clients, {
            foreignKey: "clientId",
            targetKey: "clientId"
        });
        dbClients.clients.belongsTo(dbsubscriptions.subscriptions, {
            foreignKey: "clientId",
            targetKey: "clientId"
        });
        dbsubscriptions.subscriptions.belongsTo(dbpricepacks.pricepacks, {
            foreignKey: "pricepacks",
            targetKey: "pricepackId"
        });
        dbclientpayments.clientpayments.belongsTo(dbsubscriptions.subscriptions, {
            foreignKey: "subscriptionsId",
            targetKey: "subscriptionId"
        });
        dbsubscriptions.subscriptions.belongsTo(dbClients.clients, {
            foreignKey: "clientId",
            targetKey: "clientId"
        });
        dbsubscriptions.subscriptions.belongsTo(dbpricepacks.pricepacks, {
            foreignKey: "pricepacks",
            targetKey: "pricepackId"
        });
        dbclientpayments.clientpayments.belongsTo(dbpaymentTransactions.paymentTransactions, {
            foreignKey: "clientpaymentId",
            targetKey: "clientpaymentId"
        });
        dbservicepricepackmap.servicepricepackmap.belongsTo(dbservices.services, {
            foreignKey: "serviceId",
            targetKey: "serviceId"
        });

        dbsubscriptions.subscriptions.belongsTo(dbclientschedulemap.clientschedulemap, {
            foreignKey: "subscriptionId",
            targetKey: "subscriptionId"
        });

        let itemConditionsclientpayments = {
            status: 1,
            is_last_payment: 1,
            businessId: businessIdinformation,
            // [dbClients.sequelize.Op.and]: [Sequelize.literal("( ( `subscription->clientschedulemap`.`clientScheduleMapId` IS NOT NULL OR `subscription->clientschedulemap`.`sessionCount` IS NOT NULL ) OR ( `subscription->clientschedulemap`.`sessionCount` = `subscription->pricepack`.`serviceDuration` ) )")],
            [dbClients.sequelize.Op.and]: [Sequelize.literal("(`subscription->clientschedulemap`.`sessionCount` = `subscription->pricepack`.`serviceDuration` )")],
        };
        let itemGroupConditions = {
            status: 1,
            businessId: businessIdinformation,
        };
        let itemGroupConditionsPricePack = {};
        let itemConditions = {
            renewl_status: 1
        };

        let paymentDataCount = await dbclientpayments.clientpayments.findAndCount({
            attributes: [
                "clientpaymentId",
                "subscriptionsId",
                "due_amount",
                "payble_amount",
                "payment_due_date",
                "payment_end_date", ["UpdateDateTime", "updated_at"],
                [
                    dbClients.clients.sequelize.literal(
                        'Date_add( `subscription`.`subscriptiondatetime`,INTERVAL `subscription->pricepack`.`pricepackvalidity` day)'
                    ),
                    "subcription_end_Date"
                ],
                [
                    dbclientpayments.sequelize.literal("( SELECT IFNULL( SUM(`payble_amount`), 0 ) FROM `clientpayments` INNER join `paymentTransactions` AS `paymentTransactions` ON `clientpayments`.`clientpaymentId` = `paymentTransactions`.`clientpaymentId` WHERE `clientpayments`.`subscriptionsId` = `subscription`.`subscriptionId` )"),
                    "total_subcription_payemt"
                ]
            ],
            include: [{
                model: dbsubscriptions.subscriptions,
                attributes: ["clientId", "status_amountPaidSoFar", "amountPaidSoFar"],
                include: [{
                        model: dbClients.clients,
                        attributes: {
                            include: [
                                "clientName", [
                                    dbClients.clients.sequelize.literal(
                                        'CASE WHEN photoUrl  is NULL THEN CONCAT("' +
                                        KNACK_UPLOAD_URL.localUrl +
                                        '" , "noimage.jpg" ) ELSE CONCAT("' +
                                        KNACK_UPLOAD_URL.localUrl +
                                        '" , photoUrl ) END'
                                    ),
                                    "photoUrl"
                                ],
                            ],
                            exclude: []
                        },
                        where: itemGroupConditions
                    },
                    {
                        model: dbpricepacks.pricepacks,
                        attributes: ["pricepackName", "amount"],
                        where: itemGroupConditionsPricePack,
                    },
                    {
                        model: dbclientschedulemap.clientschedulemap,
                        attributes: ["scheduleId"],
                        require: false,
                        where: {
                            // [dbclientschedulemap.sequelize.Op.and]: [Sequelize.literal("`subscription->clientschedulemap`.`scheduleId`= `subscription->clientschedulemap`.`parentScheduleId`")],
                            [dbclientschedulemap.sequelize.Op.and]: [Sequelize.literal("( `subscription->clientschedulemap`.`scheduleId` = `subscription->clientschedulemap`.`parentScheduleId` ) AND ( `subscription->clientschedulemap`.`scheduleId` = (select min(`scheduleId`) from `clientschedulemap` `inSched` where `inSched`.`subscriptionId` = `subscription`.`subscriptionId` ) )")],
                        }
                    }
                
                ],
                where: itemConditions
            }],
            order: [
                ["CreateDateTime", "DESC"]
            ],
            where: itemConditionsclientpayments,
            subQuery: false,
            raw: true
        });

        let number = 0;

        if (paymentDataCount.length > 0) {
            number = paymentDataCount.rows.length
        } else {
            number = paymentDataCount.rows.length
        }

        dbclientpayments.sequelize.close();
        dbsubscriptions.sequelize.close();
        dbClients.sequelize.close();
        dbpricepacks.sequelize.close();
        dbpaymentTransactions.sequelize.close();
        dbservicepricepackmap.sequelize.close();
        dbservices.sequelize.close();
        dbclientschedulemap.sequelize.close();

        return number;



    } catch (error) {
        throw error;
    }
};


export async function attendanceInfo(information, setObj, date) { //returns number of attendance for a particular date
    try { //change the name to dateAttendance
        let getInfo = information.split(",");
        let dbBusnessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusnessId };
        const setCond = Object.assign(setObj, setDbid);

        var d = date.getDate();
        if (d >= 0 && d <= 9) d = "0" + d;
        var m = date.getMonth() + 1;
        if (m >= 0 && m <= 9) m = "0" + m;
        var y = date.getFullYear();
        var getDate = y + '-' + m + '-' + d;

        let db = createInstance(["clientScheduleMap"], information);
        let clients = await db.clientschedulemap.findAll({
            where: setCond,
            raw: true
        });

        var countPresent = 0,
            countAbsent = 0,
            countExcused = 0,
            countUnmarked = 0;
        for (var i = 0; i < clients.length; i++) {
            var attendanceKeys = [];
            var attendanceValues = [];
            Object.keys(clients[i].attendanceType).forEach(function(key) {
                attendanceKeys.push(key);
                attendanceValues.push(clients[i].attendanceType[key]);
            });

            for (var j = 0; j < attendanceKeys.length; j++) {
                if (attendanceKeys[j] == getDate) {
                    if (attendanceValues[j] == 1) countPresent++;
                    else if (attendanceValues[j] == 2) countAbsent++;
                    else if (attendanceValues[j] == 3) countExcused++;
                    else if (attendanceValues[j] == 4) countUnmarked++;
                }
            }
        }

        var sub = [];
        Array.prototype.push.apply(sub, [{ present: countPresent }]);
        Array.prototype.push.apply(sub, [{ absent: countAbsent }]);
        Array.prototype.push.apply(sub, [{ excused: countExcused }]);
        Array.prototype.push.apply(sub, [{ unmarked: countUnmarked }]);
        var info = Object.assign.apply(Object, sub);

        db.sequelize.close();
        return info;
    } catch (error) {
        throw error;
    }
}


export async function paymentInfo(information, businessId, date) { //returns payment details of a client with client details
    try {
        let clientTotalPaymentdb = createInstance(["clientPayments"], information);
        let clientTotalPayment = await clientTotalPaymentdb.clientpayments.findAll({
            where: {
                status: 1,
                businessId: businessId,
                payment_date: date
            },
            raw: true
        });
        var info = [];
        let dbClients = createInstance(["client"], information);
        for (var i = 0; i < clientTotalPayment.length; i++) {
            let dbSub = createInstance(["clientSubscription"], information);
            let subscriptions = await dbSub.subscriptions.findAll({
                where: {
                    status: 1,
                    businessId: businessId,
                    subscriptionId: clientTotalPayment[i].subscriptionsId
                },
                raw: true
            });

            let clients = await dbClients.clients.findAll({
                where: {
                    businessId: businessId,
                    clientId: subscriptions[0].clientId,
                    status: 1
                },
                raw: true
            });


            var client_id = [{ clientId: clients[0].clientId }];
            var client_name = [{ clientName: clients[0].clientName }];
            var payment_date = [{ paymentDate: clientTotalPayment[i].payment_date }];
            var total_amount_paid = [{ totalAmountPaid: clientTotalPayment[i].totalAmountPaid }];

            var arr = [];
            Array.prototype.push.apply(arr, client_id);
            Array.prototype.push.apply(arr, client_name);
            Array.prototype.push.apply(arr, payment_date);
            Array.prototype.push.apply(arr, total_amount_paid);
            var merged = {};
            merged = Object.assign.apply(Object, arr);
            info = info.concat(merged);
        }

        return info;
    } catch (error) {
        throw error;
    }
}


export async function scheduleInfo(information, businessId, date) { //returns payment details of a client with client details
    try {

        let db = createInstance(["clientSchedule"], information);
        let schedules = await db.schedule.findAll({
            where: {
                status: 1,
                businessId: businessId
            },
            raw: true
        });

        var info = [];
        for (var i = 0; i < schedules.length; i++) {
            var setCond = {
                scheduleId: schedules[i].scheduleId,
                status: 1
            };
            var getAttendance = await attendanceInfo(information, setCond, date);

            var arr = [];
            Array.prototype.push.apply(arr, [{ scheduleId: schedules[i].scheduleId }]);
            Array.prototype.push.apply(arr, [{ attendance: getAttendance }]);
            var merged = Object.assign.apply(Object, arr);
            info = info.concat(merged);
        }

        return info;
    } catch (error) {
        throw error;
    }
}

export async function commaSeparatedValues(str) {
    try {
        var str_array4 = new Array();
        var str_array3 = str.split(",");
        for (var i = 0; i < str_array3.length; i++) {
            str_array3[i] = str_array3[i].replace(/^\s*/, "").replace(/\s*$/, ""); // Trim the excess whitespace.
            if (
                str_array3[i] === "" ||
                str_array3[i] === " " ||
                str_array3[i] === null
            ) {} else {
                str_array4[i] = str_array3[i];
            }
        }

        return str_array4;

    } catch (error) {
        throw error;
    }
}

export async function intersect_arrays(a, b) {
    try {
        var sorted_a = a.concat().sort();
        var sorted_b = b.concat().sort();
        var common = [];
        var a_i = 0;
        var b_i = 0;

        while (a_i < a.length &&
            b_i < b.length) {
            if (sorted_a[a_i] === sorted_b[b_i]) {
                common.push(sorted_a[a_i]);
                a_i++;
                b_i++;
            } else if (sorted_a[a_i] < sorted_b[b_i]) {
                a_i++;
            } else {
                b_i++;
            }
        }
        return common;

    } catch (error) {
        throw error;
    }
}

export async function diff_arrays(array1, array2) { //array1 should be the superset of array2 (uncommon values of array2 will not be shown)
    try {
        var array3 = array1.filter(function(obj) { return array2.indexOf(obj) == -1; });
        return array3;
    } catch (error) {
        throw error;
    }
}

// Returns an array of dates between the two dates
export async function getDates(startDate, endDate) {
    try {
        var dates = [],
            currentDate = startDate,
            addDays = function(days) {
                var date = new Date(this.valueOf());
                date.setDate(date.getDate() + days);
                return date;
            };
        while (currentDate <= endDate) {
            dates.push(currentDate);
            currentDate = addDays.call(currentDate, 1);
        }
        return dates;
    } catch (error) {
        throw error;
    }
}

// New code
export async function getAttendance(
    scheduleId,
    centerId,
    startTime,
    endTime,
    scheduleDate,
    is_repeat, //checking if repeat or not
    repeatation_type, //daily, weekly, monthly, yearly
    monthly_type,
    repeat_number, //if daily then repeat every *day. if weekly repeat upto this number of weeks
    repeat_days, //if weekly then repeat days sun:0 to sat:6
    repeat_ends,
    repeat_ends_date,
    bdays) {
    try {
        var std = scheduleDate;
        var sdate = std.split("-");
        var stYear = parseInt(sdate[0]);
        var stMonth = parseInt(sdate[1]) - 1;
        var stDate = parseInt(sdate[2]);

        var end = repeat_ends_date;
        var edate = end.split("-");
        var enYear = parseInt(edate[0]);
        var enMonth = parseInt(edate[1]) - 1;
        var enDate = parseInt(edate[2]);

        //getting the dates between the scheduleDate and endDate
        var dates = await getDates(new Date(stYear, stMonth, stDate), new Date(enYear, enMonth, enDate));
        var i = 0;
        var newYear, newMonth, newDate;
        var arrayOfDate = new Array();
        dates.forEach(function(date) {
            newYear = dates[i].getFullYear();
            newMonth = dates[i].getMonth() + 1;
            newDate = dates[i].getDate();
            if (newMonth >= 0 && newMonth < 10) newMonth = "0" + newMonth;
            if (newDate >= 0 && newDate < 10) newDate = "0" + newDate;
            arrayOfDate[i] = newYear + "-" + newMonth + "-" + newDate;
            i++;
        });

        //no repeat
        if (is_repeat == 0) {
            var output = [];
            //output[scheduleDate] = "4";
            let s_date = scheduleDate.split("-"),
                s_time = startTime.split(":");
            let id = "" + s_date[0] + s_date[1] + s_date[2] + s_time[0] + s_time[1];

            let rescheduleDate = scheduleDate;
            output.push({
                id: id,
                scdate: scheduleDate,
                atd: 4,
                atdBy: "",
                atdDate: "",
                atdTime: "",
                reschedule: 1,
                rescheduleId: scheduleId,
                rescheduleDate: rescheduleDate,
                scheduleStartTime: startTime,
                scheduleEndTime: endTime,
                scheduleFrom: "",
                centerId: centerId,
                rescheduleBy: "",
                comment: "",
            });
            var finalAttendance = JSON.stringify(output);
            if (finalAttendance.includes('\"')) { var attendanceType = JSON.parse(finalAttendance); }
        }
        //repeat
        else if (is_repeat == 1) {
            if (is_repeat == 1 && parseInt(repeat_number) > 0)
                var interval = parseInt(repeat_number);
            else throw new ApplicationError(
                "Repeat number shouldn't be less than 1",
                401
            );
            //daily repeat
            if (repeatation_type == 1) {
                var output = [];
                let b_days = bdays.map(function(value) {
                    return (value - 1).toString();
                });
                for (var it = 0; it < arrayOfDate.length; it = it + interval) {
                    // output[arrayOfDate[it]] = "4";
                    let rescheduleDate = arrayOfDate[it];
                    // output[arrayOfDate[it]] = { attendance: 4, reschedule: 1, rescheduleDate: rescheduleDate, rescheduleId: scheduleId, scheduleStartTime: startTime, scheduleEndTime: endTime, scheduleFrom: "", centerId: centerId, comment: "" };
                    let thisDay = new Date(arrayOfDate[it]);

                    if (b_days.includes(thisDay.getDay().toString())) {
                        let s_date = arrayOfDate[it].split("-"),
                            s_time = startTime.split(":");
                        let id = "" + s_date[0] + s_date[1] + s_date[2] + s_time[0] + s_time[1];
                        output.push({
                            id: id,
                            scdate: arrayOfDate[it],
                            atd: 4,
                            atdBy: "",
                            atdDate: "",
                            atdTime: "",
                            reschedule: 1,
                            rescheduleId: scheduleId,
                            rescheduleDate: rescheduleDate,
                            scheduleStartTime: startTime,
                            scheduleEndTime: endTime,
                            scheduleFrom: "",
                            centerId: centerId,
                            rescheduleBy: "",
                            comment: "",
                        });
                    }
                }
                var finalAttendance = JSON.stringify(output);
                if (finalAttendance.includes('\"')) { var attendanceType = JSON.parse(finalAttendance); }
            }
            //weekly repeat
            else if (repeatation_type == 2) {
                var str_repeatDaysFrontEnd = await commaSeparatedValues(repeat_days);
                var str_repeatDays = str_repeatDaysFrontEnd.map(function(value) {
                    return value - 1;
                });

                var dateArray = [];
                for (var k = 0, c = 0; k < arrayOfDate.length; k = k + (interval * 7)) {

                    for (var weekday = k; weekday < k + 7; weekday++) {
                        if (arrayOfDate[weekday] != null) {
                            var day = new Date(arrayOfDate[weekday]);
                            newYear = day.getFullYear();
                            newMonth = day.getMonth() + 1;
                            newDate = day.getDate();
                            if (newMonth >= 0 && newMonth < 10) newMonth = '0' + newMonth;
                            if (newDate >= 0 && newDate < 10) newDate = '0' + newDate;
                            dateArray[c++] = newYear + '-' + newMonth + '-' + newDate;

                        }

                    }
                }

                var output = [];
                var count = 1;
                for (var a = 0; a < dateArray.length; a++) {
                    var d = new Date(dateArray[a]);
                    for (var givenDays = 0; givenDays < str_repeatDays.length; givenDays++) {
                        if (d.getDay() == str_repeatDays[givenDays]) {
                            // output[arrayOfDate[a]] = "4";
                            let rescheduleDate = dateArray[a];
                            // output[arrayOfDate[a]] = { attendance: 4, reschedule: 1, rescheduleDate: rescheduleDate, rescheduleId: scheduleId, scheduleStartTime: startTime, scheduleEndTime: endTime, scheduleFrom: "", centerId: centerId, comment: "" };
                            let s_date = dateArray[a].split("-"),
                                s_time = startTime.split(":");
                            let id = "" + s_date[0] + s_date[1] + s_date[2] + s_time[0] + s_time[1];
                            output.push({
                                id: id,
                                scdate: dateArray[a],
                                atd: 4,
                                atdBy: "",
                                atdDate: "",
                                atdTime: "",
                                reschedule: 1,
                                rescheduleId: scheduleId,
                                rescheduleDate: rescheduleDate,
                                scheduleStartTime: startTime,
                                scheduleEndTime: endTime,
                                scheduleFrom: "",
                                centerId: centerId,
                                rescheduleBy: "",
                                comment: "",
                            });
                        }
                    }
                }
                var finalAttendance = JSON.stringify(output);
                if (finalAttendance.includes('\"')) { var attendanceType = JSON.parse(finalAttendance); }
            }
            //monthly repeat
            else if (repeatation_type == 3) {
                if (monthly_type == 1) { //monthly on the same date
                    var output = [];
                    var date = new Date(scheduleDate);
                    while (new Date(date) <= new Date(repeat_ends_date)) {
                        var xYear = date.getFullYear();
                        var xMonth = date.getMonth() + 1;
                        var xDate = date.getDate();
                        if (xMonth >= 0 && xMonth < 10)
                            xMonth = '0' + xMonth;
                        if (xDate >= 0 && xDate < 10)
                            xDate = '0' + xDate;
                        var xDay = xYear + '-' + xMonth + '-' + xDate;

                        // output[xDay] = "4";
                        let rescheduleDate = xDay;
                        // output[xDay] = { attendance: 4, reschedule: 1, rescheduleDate: rescheduleDate, rescheduleId: scheduleId, scheduleStartTime: startTime, scheduleEndTime: endTime, scheduleFrom: "", centerId: centerId, comment: "" };
                        let s_date = xDay.split("-"),
                            s_time = startTime.split(":");
                        let id = "" + s_date[0] + s_date[1] + s_date[2] + s_time[0] + s_time[1];
                        output.push({
                            id: id,
                            scdate: xDay,
                            atd: 4,
                            atdBy: "",
                            atdDate: "",
                            atdTime: "",
                            reschedule: 1,
                            rescheduleId: scheduleId,
                            rescheduleDate: rescheduleDate,
                            scheduleStartTime: startTime,
                            scheduleEndTime: endTime,
                            scheduleFrom: "",
                            centerId: centerId,
                            rescheduleBy: "",
                            comment: "",
                        });
                        date.setMonth(date.getMonth() + interval);
                    }
                    var finalAttendance = JSON.stringify(output);
                    if (finalAttendance.includes('\"')) { var attendanceType = JSON.parse(finalAttendance); }
                } else if (monthly_type == 2) { //monthly on the same day of week
                    var output = [];
                    var date = new Date(scheduleDate);
                    while (new Date(date) <= new Date(repeat_ends_date)) {
                        var xYear = date.getFullYear();
                        var xMonth = date.getMonth() + 1;
                        var xDate = date.getDate();
                        if (xMonth >= 0 && xMonth < 10)
                            xMonth = '0' + xMonth;
                        if (xDate >= 0 && xDate < 10)
                            xDate = '0' + xDate;
                        var xDay = xYear + '-' + xMonth + '-' + xDate;

                        // output[xDay] = "4";
                        let rescheduleDate = xDay;
                        // output[xDay] = { attendance: 4, reschedule: 1, rescheduleDate: rescheduleDate, rescheduleId: scheduleId, scheduleStartTime: startTime, scheduleEndTime: endTime, scheduleFrom: "", centerId: centerId, comment: "" };
                        let s_date = xDay.split("-"),
                            s_time = startTime.split(":");
                        let id = "" + s_date[0] + s_date[1] + s_date[2] + s_time[0] + s_time[1];
                        output.push({
                            id: id,
                            scdate: xDay,
                            atd: 4,
                            atdBy: "",
                            atdDate: "",
                            atdTime: "",
                            reschedule: 1,
                            rescheduleId: scheduleId,
                            rescheduleDate: rescheduleDate,
                            scheduleStartTime: startTime,
                            scheduleEndTime: endTime,
                            scheduleFrom: "",
                            centerId: centerId,
                            rescheduleBy: "",
                            comment: "",
                        });

                        let datesaved = new Date(scheduleDate);
                        date.setMonth(date.getMonth() + interval);
                        date = await getDayOfMonth(datesaved, date);
                        date = new Date(date);
                    }
                    var finalAttendance = JSON.stringify(output);
                    if (finalAttendance.includes('\"')) { var attendanceType = JSON.parse(finalAttendance); }
                }

            }
            //yearly repeat
            else if (repeatation_type == 4) {
                var output = [];
                var date = new Date(scheduleDate);
                while (new Date(date) <= new Date(repeat_ends_date)) {
                    var xYear = date.getFullYear();
                    var xMonth = date.getMonth() + 1;
                    var xDate = date.getDate();
                    if (xMonth >= 0 && xMonth < 10)
                        xMonth = '0' + xMonth;
                    if (xDate >= 0 && xDate < 10)
                        xDate = '0' + xDate;
                    var xDay = xYear + '-' + xMonth + '-' + xDate;

                    // output[xDay] = "4";
                    let rescheduleDate = xDay;
                    // output[xDay] = { attendance: 4, reschedule: 1, rescheduleDate: rescheduleDate, rescheduleId: scheduleId, scheduleStartTime: startTime, scheduleEndTime: endTime, scheduleFrom: "", centerId: centerId, comment: "" };
                    let s_date = xDay.split("-"),
                        s_time = startTime.split(":");
                    let id = "" + s_date[0] + s_date[1] + s_date[2] + s_time[0] + s_time[1];
                    output.push({
                        id: id,
                        scdate: xDay,
                        atd: 4,
                        atdBy: "",
                        atdDate: "",
                        atdTime: "",
                        reschedule: 1,
                        rescheduleId: scheduleId,
                        rescheduleDate: rescheduleDate,
                        scheduleStartTime: startTime,
                        scheduleEndTime: endTime,
                        scheduleFrom: "",
                        centerId: centerId,
                        rescheduleBy: "",
                        comment: "",
                    });

                    date.setFullYear(date.getFullYear() + interval);
                }
                var finalAttendance = JSON.stringify(output);
                if (finalAttendance.includes('\"')) { var attendanceType = JSON.parse(finalAttendance); }
            } else {
                throw new ApplicationError(
                    "provide correct value for repeatation_type. 1 for daily. 2 for weekly. 3 for monthly. 4 for yearly.",
                    409
                );
            }
        }
        //repeat ends here
        else {
            throw new ApplicationError(
                "provide correct value for is_repeat. 1 for repeat. 0 for not.",
                409
            );
        }
        return await attendanceType;
    } catch (error) {
        throw error;
    }
}

export async function getScheduleInfo(information, businessId) {
    try {
        let db = createInstance(["clientSchedule"], information);
        let info = await db.schedule.findAll({
            where: {
                businessId: businessId,
                status: 1
            },
            raw: true
        });
        return await info;
    } catch (err) {
        throw err;
    }
}

export async function getClientScheduleMapInfo(information, scheduleId) {
    try {
        let db = createInstance(["clientScheduleMap"], information);
        let info = await db.clientschedulemap.findAll({
            where: {
                status: 1,
                scheduleId: scheduleId
            },
            raw: true
        });
        return await info;
    } catch (err) {
        throw err;
    }
}

export async function getClientInfo(information, clientScheduleMap) {
    try {
        var info = [];
        var db = createInstance(["client"], information);
        for (var i = 0; i < clientScheduleMap.length; i++) {
            var clientDetails = await db.clients.findAll({
                where: {
                    clientId: clientScheduleMap[i].clientId,
                    status: 1
                },
                raw: true
            });
            info = info.concat(clientDetails);
        }
        return await info;
    } catch (err) {
        throw err;
    }
}

export async function makeid() {
    try {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 5; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
    } catch (err) {
        throw err;
    }
}

export async function hyphenDateFormat(dateString) { //this function returns a date string in YYYY-MM-DD format
    try {
        let getDate = new Date(dateString);
        let year = getDate.getFullYear(),
            month = getDate.getMonth() + 1,
            date = getDate.getDate();
        if (month >= 0 && month < 10)
            month = '0' + month;
        if (date >= 0 && date < 10)
            date = '0' + date;
        return year + '-' + month + '-' + date;
    } catch (err) {
        throw err;
    }
}

// New Code
export async function getScheduleTimeTable(
    startTime,
    endTime,
    scheduleDate,
    centerId,
    is_repeat, //checking if repeat or not
    repeatation_type, //daily, weekly, monthly, yearly
    monthly_type,
    repeat_number, //if daily then repeat every *day. if weekly repeat upto this number of weeks
    repeat_days, //if weekly then repeat days sun:0 to sat:6
    repeat_ends,
    repeat_ends_date,
    bdays) {
    try {
        var std = scheduleDate;
        var sdate = std.split("-");
        var stYear = parseInt(sdate[0]);
        var stMonth = parseInt(sdate[1]) - 1;
        var stDate = parseInt(sdate[2]);

        var end = repeat_ends_date;
        var edate = end.split("-");
        var enYear = parseInt(edate[0]);
        var enMonth = parseInt(edate[1]) - 1;
        var enDate = parseInt(edate[2]);
        //getting the dates between the scheduleDate and endDate
        var dates = await getDates(new Date(stYear, stMonth, stDate), new Date(enYear, enMonth, enDate));
        var i = 0;
        var newYear, newMonth, newDate;
        var arrayOfDate = new Array();
        dates.forEach(function(date) {
            newYear = dates[i].getFullYear();
            newMonth = dates[i].getMonth() + 1;
            newDate = dates[i].getDate();
            if (newMonth >= 0 && newMonth < 10) newMonth = "0" + newMonth;
            if (newDate >= 0 && newDate < 10) newDate = "0" + newDate;
            arrayOfDate[i] = newYear + "-" + newMonth + "-" + newDate;
            i++;
        });

        //no repeat
        if (is_repeat == 0) {
            var output = [];
            let rescheduleDate = scheduleDate;
            // output[scheduleDate] = { reschedule: 1, rescheduleDate: rescheduleDate, scheduleStartTime: startTime, scheduleEndTime: endTime, comment: "" };
            let s_date = scheduleDate.split("-"),
                s_time = startTime.split(":");
            let id = "" + s_date[0] + s_date[1] + s_date[2] + s_time[0] + s_time[1];

            output.push({
                id: id,
                scdate: scheduleDate,
                reschedule: 1,
                rescheduleDate: rescheduleDate,
                scheduleStartTime: startTime,
                scheduleEndTime: endTime,
                scheduleFrom: "",
                centerId: centerId,
                rescheduleBy: "",
                comment: "",
            });
            var finalAttendance = JSON.stringify(output);
            if (finalAttendance.includes('\"')) { var attendanceType = JSON.parse(finalAttendance); }
        }
        //repeat
        else if (is_repeat == 1) {
            if (parseInt(repeat_number) > 0)
                var interval = parseInt(repeat_number);
            else throw new ApplicationError(
                "Repeat number shouldn't be less than 1",
                401
            );
            //daily repeat
            if (repeatation_type == 1) {
                var output = [];
                let b_days = bdays.map(function(value) {
                    return (value - 1).toString();
                });
                for (var it = 0; it < arrayOfDate.length; it = it + interval) {
                    let rescheduleDate = arrayOfDate[it];
                    // output[arrayOfDate[it]] = { reschedule: 1, rescheduleDate: rescheduleDate, scheduleStartTime: startTime, scheduleEndTime: endTime, comment: "" };
                    let thisDay = new Date(arrayOfDate[it]);
                    if (b_days.includes(thisDay.getDay().toString())) {
                        let s_date = arrayOfDate[it].split("-"),
                            s_time = startTime.split(":");
                        let id = "" + s_date[0] + s_date[1] + s_date[2] + s_time[0] + s_time[1];
                        output.push({
                            id: id,
                            scdate: arrayOfDate[it],
                            reschedule: 1,
                            rescheduleDate: rescheduleDate,
                            scheduleStartTime: startTime,
                            scheduleEndTime: endTime,
                            scheduleFrom: "",
                            centerId: centerId,
                            rescheduleBy: "",
                            comment: "",
                        });
                    }
                }
                var finalAttendance = JSON.stringify(output);
                if (finalAttendance.includes('\"')) { var attendanceType = JSON.parse(finalAttendance); }
            }
            //weekly repeat
            else if (repeatation_type == 2) {
                var str_repeatDaysFrontEnd = await commaSeparatedValues(repeat_days);
                var str_repeatDays = str_repeatDaysFrontEnd.map(function(value) {
                    return value - 1;
                });

                var dateArray = [];
                for (var k = 0, c = 0; k < arrayOfDate.length; k = k + (interval * 7)) {
                    for (var weekday = k; weekday < k + 7; weekday++) {
                        if (arrayOfDate[weekday] != null) {
                            var day = new Date(arrayOfDate[weekday]);
                            newYear = day.getFullYear();
                            newMonth = day.getMonth() + 1;
                            newDate = day.getDate();
                            if (newMonth >= 0 && newMonth < 10) newMonth = '0' + newMonth;
                            if (newDate >= 0 && newDate < 10) newDate = '0' + newDate;
                            dateArray[c++] = newYear + '-' + newMonth + '-' + newDate;
                        }
                    }
                }

                var output = [];
                var count = 1;
                for (var a = 0; a < dateArray.length; a++) {
                    var d = new Date(dateArray[a]);
                    for (var givenDays = 0; givenDays < str_repeatDays.length; givenDays++) {
                        if (d.getDay() == str_repeatDays[givenDays]) {
                            let rescheduleDate = dateArray[a];
                            // output[dateArray[a]] = { reschedule: 1, rescheduleDate: rescheduleDate, scheduleStartTime: startTime, scheduleEndTime: endTime, comment: "" };
                            let s_date = dateArray[a].split("-"),
                                s_time = startTime.split(":");
                            let id = "" + s_date[0] + s_date[1] + s_date[2] + s_time[0] + s_time[1];
                            output.push({
                                id: id,
                                scdate: dateArray[a],
                                reschedule: 1,
                                rescheduleDate: rescheduleDate,
                                scheduleStartTime: startTime,
                                scheduleEndTime: endTime,
                                scheduleFrom: "",
                                centerId: centerId,
                                rescheduleBy: "",
                                comment: "",
                            });
                        }
                    }
                }


                var finalAttendance = JSON.stringify(output);
                if (finalAttendance.includes('\"')) { var attendanceType = JSON.parse(finalAttendance); }
            }
            //monthly repeat
            else if (repeatation_type == 3) {
                if (monthly_type == 1) { //monthly on the same date
                    var output = [];
                    var date = new Date(scheduleDate);
                    while (new Date(date) <= new Date(repeat_ends_date)) {
                        var xYear = date.getFullYear();
                        var xMonth = date.getMonth() + 1;
                        var xDate = date.getDate();
                        if (xMonth >= 0 && xMonth < 10)
                            xMonth = '0' + xMonth;
                        if (xDate >= 0 && xDate < 10)
                            xDate = '0' + xDate;
                        var xDay = xYear + '-' + xMonth + '-' + xDate;

                        let rescheduleDate = xDay;
                        // output[xDay] = { reschedule: 1, rescheduleDate: rescheduleDate, scheduleStartTime: startTime, scheduleEndTime: endTime, comment: "" };
                        let s_date = xDay.split("-"),
                            s_time = startTime.split(":");
                        let id = "" + s_date[0] + s_date[1] + s_date[2] + s_time[0] + s_time[1];
                        output.push({
                            id: id,
                            scdate: xDay,
                            reschedule: 1,
                            rescheduleDate: rescheduleDate,
                            scheduleStartTime: startTime,
                            scheduleEndTime: endTime,
                            scheduleFrom: "",
                            centerId: centerId,
                            rescheduleBy: "",
                            comment: "",
                        });
                        date.setMonth(date.getMonth() + interval);
                    }

                    var finalAttendance = JSON.stringify(output);
                    if (finalAttendance.includes('\"')) { var attendanceType = JSON.parse(finalAttendance); }
                } else if (monthly_type == 2) { //monthly on the same day of week
                    var output = [];
                    var date = new Date(scheduleDate);
                    while (new Date(date) <= new Date(repeat_ends_date)) {
                        var xYear = date.getFullYear();
                        var xMonth = date.getMonth() + 1;
                        var xDate = date.getDate();
                        if (xMonth >= 0 && xMonth < 10)
                            xMonth = '0' + xMonth;
                        if (xDate >= 0 && xDate < 10)
                            xDate = '0' + xDate;
                        var xDay = xYear + '-' + xMonth + '-' + xDate;

                        let rescheduleDate = xDay;
                        // output[xDay] = { reschedule: 1, rescheduleDate: rescheduleDate, scheduleStartTime: startTime, scheduleEndTime: endTime, comment: "" };
                        let s_date = xDay.split("-"),
                            s_time = startTime.split(":");
                        let id = "" + s_date[0] + s_date[1] + s_date[2] + s_time[0] + s_time[1];
                        output.push({
                            id: id,
                            scdate: xDay,
                            reschedule: 1,
                            rescheduleDate: rescheduleDate,
                            scheduleStartTime: startTime,
                            scheduleEndTime: endTime,
                            scheduleFrom: "",
                            centerId: centerId,
                            rescheduleBy: "",
                            comment: "",
                        });

                        let datesaved = new Date(scheduleDate);
                        date.setMonth(date.getMonth() + interval);
                        date = await getDayOfMonth(datesaved, date);
                        date = new Date(date);
                    }
                    console.log(output);
                    var finalAttendance = JSON.stringify(output);
                    if (finalAttendance.includes('\"')) { var attendanceType = JSON.parse(finalAttendance); }
                }
            }
            //yearly repeat
            else if (repeatation_type == 4) {
                var output = [];
                var date = new Date(scheduleDate);
                while (new Date(date) <= new Date(repeat_ends_date)) {
                    var xYear = date.getFullYear();
                    var xMonth = date.getMonth() + 1;
                    var xDate = date.getDate();
                    if (xMonth >= 0 && xMonth < 10)
                        xMonth = '0' + xMonth;
                    if (xDate >= 0 && xDate < 10)
                        xDate = '0' + xDate;
                    var xDay = xYear + '-' + xMonth + '-' + xDate;

                    let rescheduleDate = xDay;
                    // output[xDay] = { reschedule: 1, rescheduleDate: rescheduleDate, scheduleStartTime: startTime, scheduleEndTime: endTime, comment: "" };
                    let s_date = xDay.split("-"),
                        s_time = startTime.split(":");
                    let id = "" + s_date[0] + s_date[1] + s_date[2] + s_time[0] + s_time[1];
                    output.push({
                        id: id,
                        scdate: xDay,
                        reschedule: 1,
                        rescheduleDate: rescheduleDate,
                        scheduleStartTime: startTime,
                        scheduleEndTime: endTime,
                        scheduleFrom: "",
                        centerId: centerId,
                        rescheduleBy: "",
                        comment: "",
                    });
                    date.setFullYear(date.getFullYear() + interval);
                }

                var finalAttendance = JSON.stringify(output);
                if (finalAttendance.includes('\"')) { var attendanceType = JSON.parse(finalAttendance); }
            } else {
                throw new ApplicationError(
                    "provide correct value for repeatation_type. 1 for daily. 2 for weekly. 3 for monthly. 4 for yearly.",
                    409
                );
            }
        }
        //repeat ends here
        else {
            throw new ApplicationError(
                "provide correct value for is_repeat. 1 for repeat. 0 for not.",
                409
            );
        }
        return await attendanceType;
    } catch (err) {
        throw err;
    }
}

export async function weeklyDates(dateString) { //this function returns an array of some objects each holds weekly dates
    try {
        let date = new Date(dateString);
        let firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
        let lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
        let diff = Math.round((lastDay - firstDay) / (1000 * 60 * 60 * 24)) + 1;
        let dates = await getDates(firstDay, lastDay);
        let arr = [];
        for (let i = 0; i < diff;) {
            let week = [];
            let value = dates[i].getDay();
            for (let j = i == 0 ? value : 0; j <= 6 && i < diff; j++, i++) {
                week.push({
                    [dates[i].getDate()]: dates[i].getDay()
                });
            }
            let weekObj = [];
            weekObj = Object.assign.apply(Object, week);
            arr.push(weekObj);
        }
        return arr;
    } catch (err) {
        throw err;
    }
}

export async function getDayOfMonth(date_1, date_2) { //this function return day of date_1 for finding it in date_2
    try {
        //working with 1st date
        let date = new Date(date_1);
        let weekDay = date.getDay();
        let arr = await weeklyDates(date);
        let pos = null;
        for (let i = 0; i < arr.length; i++) {
            let obj = arr[i];
            Object.keys(obj).forEach((key) => {
                if (key == date.getDate()) {
                    pos = i;
                }
            })
        }
        let weekNumber = null;
        if (pos != null)
            weekNumber = pos;
        else
            weekNumber = null;


        //working with 2nd date
        let dateNew = new Date(date_2);
        let arrNew = await weeklyDates(dateNew);
        let objNew = arrNew[weekNumber];
        if (objNew == null) {
            if (weekNumber == 0) objNew = arrNew[weekNumber + 1];
            else if (weekNumber == 5) objNew = arrNew[weekNumber - 1];
        }
        let dayNew = null;
        Object.keys(objNew).forEach((key) => {
            if (objNew[key] == weekDay) {
                dayNew = key;
            }
        });


        if (dayNew == null) {
            if (weekNumber == 3 || weekNumber == 4 || weekNumber == 5)
                objNew = arrNew[weekNumber - 1];
            else if (weekNumber == 0 || weekNumber == 1)
                objNew = arrNew[weekNumber + 1];
        }
        Object.keys(objNew).forEach((key) => {
            if (objNew[key] == weekDay) {
                dayNew = key;
            }
        });


        let monthNew = dateNew.getMonth() + 1;
        let yearNew = dateNew.getFullYear();
        let newlyCreatedDate = null;
        if (dayNew != null)
            newlyCreatedDate = yearNew + '-' + monthNew + '-' + dayNew;
        else
            newlyCreatedDate = null;
        return newlyCreatedDate;
    } catch (err) {
        throw err;
    }
}

export function makeEmailTemplate(filename = "", data) {
    try {
        var emailPath = path.resolve(filename);
        var tools = require(emailPath);
        var content = tools.makeEmailTemplate(data);
        return content;
    } catch (err) {
        throw err;
    }
}

export function numberWithCommas(formatedPrice = "") {
    return formatedPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}


//Covert 24 Hours Time Format
export function tConv24(time24) {
    var ts = time24;
    var H = +ts.substr(0, 2);
    var h = (H % 12) || 12;
    h = (h < 10) ? ("0" + h) : h; // leading 0 at the left for 1 digit hours
    var ampm = H < 12 ? "am" : "pm";
    ts = h + ts.substr(2, 3) + ampm;
    return ts;
};
// Week Initial
export function convartday(stringval) {
    var getarray = JSON.parse("[" + stringval + "]");
    let weekin = '';
    for (let wk = 0; wk < getarray.length; wk++) {
        if (getarray[wk] == 0) {
            weekin = 'S' + ',';
        } else if (getarray[wk] == 1) {
            weekin = weekin + 'M' + ',';
        } else if (getarray[wk] == 2) {
            weekin = weekin + 'T' + ',';
        } else if (getarray[wk] == 3) {
            weekin = weekin + 'W' + ',';
        } else if (getarray[wk] == 4) {
            weekin = weekin + 'Th' + ',';
        } else if (getarray[wk] == 5) {
            weekin = weekin + 'F' + ',';
        } else if (getarray[wk] == 6) {
            weekin = weekin + 'Sa';
        }
    }
    weekin = weekin.replace(/,\s*$/, "");
    return weekin;
}

export async function attendanceInfoOfClientAndDate(information, setObj, date) { //returns number of attendance for a particular date
    try { //change the name to dateAttendance
        let getInfo = information.split(",");
        let dbBusnessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusnessId };
        const setCond = Object.assign(setObj, setDbid);

        var d = date.getDate();
        if (d >= 0 && d <= 9) d = "0" + d;
        var m = date.getMonth() + 1;
        if (m >= 0 && m <= 9) m = "0" + m;
        var y = date.getFullYear();
        var getDate = y + '-' + m + '-' + d;

        let db = createInstance(["clientScheduleMap"], information);
        let clients = await db.clientschedulemap.findAll({
            where: setCond,
            raw: true
        });

        let countClientOnDate = 0;
        var countPresent = 0,
            countAbsent = 0,
            countExcused = 0,
            countUnmarked = 0,
            countTotal = 0;
        for (var i = 0; i < clients.length; i++) {
            var attendanceKeys = [];
            var attendanceValues = [];
            // Object.keys(clients[i].attendanceType).forEach(function (key) {
            //   attendanceKeys.push(key);
            //   attendanceValues.push(clients[i].attendanceType[key]);
            // });
            for (let keyClient = 0; keyClient < clients[i].attendanceType.length; keyClient++) {
                attendanceKeys.push(clients[i].attendanceType[keyClient].scdate);
                attendanceValues.push(clients[i].attendanceType[keyClient].atd);
            }

            for (var j = 0; j < attendanceKeys.length; j++) {
                if (attendanceKeys[j] == getDate) {
                    if (attendanceValues[j] == 1) countPresent++;
                    else if (attendanceValues[j] == 2) countAbsent++;
                    else if (attendanceValues[j] == 3 || attendanceValues[j] == 6 || attendanceValues[j] == 7) countExcused++;
                    else if (attendanceValues[j] == 4) countUnmarked++;
                    countClientOnDate++;
                    break;
                }
                countTotal++;
            }
        }

        let totalAtd = countPresent + countAbsent + countExcused + countUnmarked;
        let perPresent = (countPresent == 0) ? 0 : (100 / totalAtd) * countPresent;
        let perAbsent = (countAbsent == 0) ? 0 : (100 / totalAtd) * countAbsent;
        let perExcused = (countExcused == 0) ? 0 : (100 / totalAtd) * countExcused;
        let perUnmarked = (countUnmarked == 0) ? 0 : (100 / totalAtd) * countUnmarked;

        let totalClients = countClientOnDate;
        let markedClients = countPresent + countAbsent + countExcused;


        var sub = [];
        Array.prototype.push.apply(sub, [{ present: (perPresent.toFixed(0)) }]);
        Array.prototype.push.apply(sub, [{ absent: (perAbsent.toFixed(0)) }]);
        Array.prototype.push.apply(sub, [{ excused: (perExcused.toFixed(0)) }]);
        Array.prototype.push.apply(sub, [{ unmarked: (perUnmarked.toFixed(0)) }]);
        Array.prototype.push.apply(sub, [{ totalClients: totalClients }]);
        Array.prototype.push.apply(sub, [{ markedClients: markedClients }]);
        var info = Object.assign.apply(Object, sub);

        db.sequelize.close();
        return info;
    } catch (error) {
        throw error;
    }
}

export async function attendanceInfoCalculate(information, setObj, date) { //returns number of attendance for a particular date

    try { //change the name to dateAttendance
        let getInfo = information.split(",");
        let dbBusnessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusnessId };
        const setCond = Object.assign(setObj, setDbid);
        let dbSchedule = createInstance(["clientSchedule"], information);
        let dbCSM = createInstance(["clientScheduleMap"], information);

        var countUnmarked = 0;
        let schedules = await dbSchedule.schedule.findAll({
            where: setCond,
            raw: true
        });
        for (let schedule of schedules) { //loop through all the bookings
            // for (let key of Object.keys(schedule.scheduleTime)) { //loop through the schedules
            for (let key of schedule.scheduleTime) {
                if (+(new Date(key.scdate)) < date) { //checking if the date is past
                    let clients = await dbCSM.clientschedulemap.findAll({
                        where: {
                            scheduleId: schedule.scheduleId,
                            status: 1
                        },
                        raw: true
                    });
                    let foundSchedule = 0;
                    for (let client of clients) { //loop through all the clients
                        // for (let atd of Object.keys(client.attendanceType)) { //loop through attendances
                        for (let atd of client.attendanceType) {
                            if (+(new Date(atd.scdate)) == +(new Date(key.scdate)) && atd.atd == 4) { //checking if attendanceDate and scheduleDate is same.

                                let schedule_date = new Date(key);
                                countUnmarked++;
                                foundSchedule = 1;
                                break;
                            }
                        }
                        if (foundSchedule == 1) break;
                    }

                }
            }

        }
        var sub = [];
        Array.prototype.push.apply(sub, [{ unmarked: countUnmarked }]);
        var info = Object.assign.apply(Object, sub);

        dbSchedule.sequelize.close();
        dbCSM.sequelize.close();

        return info;
    } catch (error) {
        throw error;
    }
}

export async function getPaymentCalculation(information, payble_amount, serviceId, pricepackId) {
    try {
        let sgstData = 0.0;
        let cgstData = 0.0;
        let taxableAmount = 0;
        let payCalculation = {
            basicAmout: 0,
            sgstPercentage: 0,
            cgstPercentage: 0,
            discountAmount: 0,
            discountPercentage: 0,
            taxableAmount: 0,
            payableAmount: 0
        };
        let dbbusinessSettings = createInstance(["businessSettings"], information);
        let businessIdinformation = information.split(",")[1];
        let businessSettingData = await dbbusinessSettings.businessSettings.findAll({
            attributes: ["businessSettingsData"],
            where: {
                status: 1,
                businessId: businessIdinformation
            },
            raw: true
        });
        businessSettingData = JSON.parse(
            businessSettingData[0].businessSettingsData
        );
        let gstDetails = businessSettingData[2].gstDetails;
        if (gstDetails.gstApplicable) {
            sgstData = Number((Math.round(gstDetails.sgst * 100) / 100).toFixed(2));
            cgstData = Number((Math.round(gstDetails.cgst * 100) / 100).toFixed(2));
        }
        taxableAmount = (Math.round((Number(sgstData) + Number(cgstData)) * 100) / 100).toFixed(2);
        let discountsOffersDetails = businessSettingData[4].discountsOffers;
        if (discountsOffersDetails.allServices == 0) {
            if (discountsOffersDetails.selectedServices.length > 0) {
                for (var x = 0; x < discountsOffersDetails.selectedServices.length; x++) {
                    if ((discountsOffersDetails.selectedServices[x].serviceId == serviceId) && (discountsOffersDetails.selectedServices[x].pricePackId == pricepackId) && (discountsOffersDetails.selectedServices[x].status == 1)) {
                        let payble_amount_format = Number(
                            (Math.round(payble_amount * 100) / 100).toFixed(2)
                        );
                        let persectageRow = Number(
                            (discountsOffersDetails.selectedServices.length > 0) ? (discountsOffersDetails.selectedServices[x].percentage) : 0
                        );
                        let amountRow = Number(
                            (discountsOffersDetails.selectedServices.length > 0) ? (discountsOffersDetails.selectedServices[x].amount) : 0
                        );
                        if (persectageRow > 0) {
                            payCalculation.taxableAmount = payble_amount_format - persectageRow * payble_amount_format / 100;
                        } else {
                            payCalculation.taxableAmount = payble_amount_format - amountRow;
                        }
                        payCalculation.payableAmount = (Math.round(payCalculation.taxableAmount + payCalculation.taxableAmount * (sgstData + cgstData) / 100)).toFixed(2);
                        payCalculation.taxableAmount = (Math.round(payCalculation.taxableAmount)).toFixed(2);
                        payCalculation.basicAmout = payble_amount;
                        payCalculation.sgstPercentage = sgstData;
                        payCalculation.cgstPercentage = cgstData;
                        payCalculation.discountAmount = amountRow;
                        payCalculation.discountPercentage = persectageRow;
                    } else {
                        let payble_amount_format = Number(
                            (Math.round(payble_amount * 100) / 100).toFixed(2)
                        );
                        let persectageRow = Number(0);
                        let amountRow = Number(0);

                        if (persectageRow > 0) {
                            payCalculation.taxableAmount = payble_amount_format - persectageRow * payble_amount_format / 100;
                        } else {
                            payCalculation.taxableAmount = payble_amount_format - amountRow;
                        }
                        payCalculation.payableAmount = (Math.round(payCalculation.taxableAmount + payCalculation.taxableAmount * (sgstData + cgstData) / 100)).toFixed(2);
                        payCalculation.taxableAmount = (Math.round(payCalculation.taxableAmount)).toFixed(2);
                        payCalculation.basicAmout = payble_amount;
                        payCalculation.sgstPercentage = sgstData;
                        payCalculation.cgstPercentage = cgstData;
                        payCalculation.discountAmount = amountRow;
                        payCalculation.discountPercentage = persectageRow;
                    }
                }
            } else {
                let payble_amount_format = Number(
                    (Math.round(payble_amount * 100) / 100).toFixed(2)
                );
                let persectageRow = Number(
                    (discountsOffersDetails.selectedServices.length > 0) ? (discountsOffersDetails.selectedServices[x].percentage) : 0
                );
                let amountRow = Number(
                    (discountsOffersDetails.selectedServices.length > 0) ? (discountsOffersDetails.selectedServices[x].amount) : 0
                );
                if (persectageRow > 0) {
                    payCalculation.taxableAmount = payble_amount_format - persectageRow * payble_amount_format / 100;
                } else {
                    payCalculation.taxableAmount = payble_amount_format - amountRow;
                }
                payCalculation.payableAmount = (Math.round(payCalculation.taxableAmount + payCalculation.taxableAmount * (sgstData + cgstData) / 100)).toFixed(2);
                payCalculation.taxableAmount = (Math.round(payCalculation.taxableAmount)).toFixed(2);
                payCalculation.basicAmout = payble_amount;
                payCalculation.sgstPercentage = sgstData;
                payCalculation.cgstPercentage = cgstData;
                payCalculation.discountAmount = amountRow;
                payCalculation.discountPercentage = persectageRow;
            }
        } else {
            let payble_amount_format = Number(
                (Math.round(payble_amount * 100) / 100).toFixed(2)
            );
            let persectageRow = Number(
                (discountsOffersDetails.selectedServices.length > 0) ? (discountsOffersDetails.selectedServices[x].percentage) : 0
            );
            let amountRow = Number(
                (discountsOffersDetails.selectedServices.length > 0) ? (discountsOffersDetails.selectedServices[x].amount) : 0
            );
            if (persectageRow > 0) {
                payCalculation.taxableAmount = payble_amount_format - persectageRow * payble_amount_format / 100;
            } else {
                payCalculation.taxableAmount = payble_amount_format - amountRow;
            }
            payCalculation.payableAmount = (Math.round(payCalculation.taxableAmount + payCalculation.taxableAmount * (sgstData + cgstData) / 100)).toFixed(2);
            payCalculation.taxableAmount = (Math.round(payCalculation.taxableAmount)).toFixed(2);
            payCalculation.basicAmout = payble_amount;
            payCalculation.sgstPercentage = sgstData;
            payCalculation.cgstPercentage = cgstData;
            payCalculation.discountAmount = amountRow;
            payCalculation.discountPercentage = persectageRow;
        }
        dbbusinessSettings.sequelize.close();
        return payCalculation;
    } catch (error) {
        throw error;
    }
};

// export async function getPaymentDateCalculation(information, payble_amount_list, pricepackId, clientId, installment_frequency) {
//     try {
//         return 1;

//     } catch (error) {
//         throw error;
//     }
// };

export async function make_date(x, y) {
    try {
        var z = {
            M: x.getMonth() + 1,
            d: x.getDate(),
            h: x.getHours(),
            m: x.getMinutes(),
            s: x.getSeconds()
        };
        y = y.replace(/(M+|d+|h+|m+|s+)/g, function(v) {
            return ((v.length > 1 ? "0" : "") + eval('z.' + v.slice(-1))).slice(-2)
        });

        return y.replace(/(y+)/g, function(v) {
            return x.getFullYear().toString().slice(-v.length)
        });
    } catch (error) {
        throw error;
    }
}

export async function rsPaginate(recordSet, pageNo, showLimit) {
    try {
        let resultSet = [];
        let offSet = (parseInt(pageNo) - 1) * showLimit;
        for (let i = offSet; i < offSet + showLimit; i++) {
            if (recordSet[i] != null)
                resultSet = resultSet.concat(recordSet[i]);
        }
        return resultSet;
    } catch (err) {
        throw err;

    }
}

export async function date_ISO_Convert(str) {
    try {
        var date = new Date(str),
            mnth = ("0" + (date.getMonth() + 1)).slice(-2),
            day = ("0" + date.getDate()).slice(-2);
        return [date.getFullYear(), mnth, day].join("-");
    } catch (error) {
        throw error;

    }
}


export async function hyphenDateFormatByDate(dateString) {
    try {
        let getDate = new Date(dateString);
        let year = getDate.getFullYear(),
            month = getDate.getMonth(),
            date = getDate.getDate() + 1;
        if (month >= 0 && month < 10)
            month = '0' + month;
        if (date >= 0 && date < 10)
            date = '0' + date;
        return year + '-' + month + '-' + date;
    } catch (err) {
        throw err;
    }
}

export async function getDateNextAndPrevious(array, val, dir) {
    try {
        if ((array[array.length - 1] != val) && (val < array[array.length - 1])) {
            if (array.indexOf("'" + val + "'")) {
                let date = new Date(val + " 00:00:00");
                let n = date.setDate(date.getDate() + 1);
                val = await formatDate(n);
            }
        } else {
            val = array[array.length - 1];
        }
        for (var i = 0; i < array.length; i++) {
            if (dir == true) {
                if (new Date(array[i]) > new Date(val)) {
                    return array[i - 1] || 0;
                }
            } else {
                if (new Date(array[i]) >= new Date(val)) {
                    return array[i];
                }
            }
        }
    } catch (err) {
        throw err;
    }
}
export async function formatDate(date) {
    try {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    } catch (err) {
        throw err;
    }
}
export async function getPlusDays(date, plusNumber = 1) {
    try {
        let newDate = await formatDate(date);
        let modifyDate = new Date(newDate);
        let dateNew = modifyDate.setDate(modifyDate.getDate() + plusNumber);
        return await formatDate(dateNew);
    } catch (err) {
        throw err;
    }
}

export async function getPlusMonths(date, plusNumber = 1) {
    try {
        let newDate = await formatDate(date);
        let modifyDate = new Date(newDate);
        let dateNew = modifyDate.setMonth(modifyDate.getMonth() + plusNumber);
        return await formatDate(dateNew);
    } catch (err) {
        throw err;
    }
}

export async function countAttendance(x) {
    try {
        console.log(x);
        let Present = 0;
        let Abscent = 0;
        let Execused = 0;
        for (let i = 0; i < x.length; i++) {
            if (x[i]["atd"] == 1) { Present++; } else if (x[i]["atd"] == 2) { Abscent++; } else if (x[i]["atd"] == 3) { Execused++; }
        }
        return [{ Present: Present, Abscent: Abscent, Execused: Execused, Total: x.length }];
    } catch (error) {
        throw error;
    }
}

export async function getRepeatDays(repeat_days) {
    try {
        let days_arr = [];
        let res = repeat_days.split(",");
        for (let i = 0; i < res.length; i++) {
            if (res[i] != 0) {
                days_arr.push(res[i]);
            }
        }
        let final_repeat_days = days_arr.toString();
        return final_repeat_days;
    } catch (err) {
        throw err;
    }
}

export async function attendanceCountOfSchedule(setObj, information) {
    let db = createInstance(["clientScheduleMap"], information);
    let clients = await db.clientschedulemap.findAll({
        where: setObj,
        raw: true
    });
    db.sequelize.close();
    var countPresent = 0,
        countAbsent = 0,
        countExcused = 0,
        countUnmarked = 0,
        countTotal = 0;
    let remainingClients = 0;
    let completedClients = 0;
    let TotalClients = clients.length;
    for (var i = 0; i < clients.length; i++) {
        let attendanceKeys = [];
        let attendanceValues = [];
        let Client = 0;
        for (let keyClient = 0; keyClient < clients[i].attendanceType.length; keyClient++) {
            attendanceKeys.push(clients[i].attendanceType[keyClient].scdate);
            attendanceValues.push(clients[i].attendanceType[keyClient].atd);
        }
        for (var j = 0; j < attendanceKeys.length; j++) {
            if (new Date(attendanceKeys[j]).getTime() < new Date().getTime()) {
                if (attendanceValues[j] == 1) countPresent++;
                else if (attendanceValues[j] == 2) countAbsent++;
                else if (attendanceValues[j] == 3 || attendanceValues[j] == 6 || attendanceValues[j] == 7) countExcused++;
                else if (attendanceValues[j] == 4) {
                    countUnmarked++;
                    Client = 1;
                }
            }
            countTotal++;
        }
        if (Client == 1) {
            remainingClients++;
        } else {
            completedClients++;
        }
    }


    var sub = [];
    Array.prototype.push.apply(sub, [{ present: countPresent }]);
    Array.prototype.push.apply(sub, [{ absent: countAbsent }]);
    Array.prototype.push.apply(sub, [{ excused: countExcused }]);
    Array.prototype.push.apply(sub, [{ unmarked: countUnmarked }]);

    Array.prototype.push.apply(sub, [{ RemainingClients: remainingClients }]);
    Array.prototype.push.apply(sub, [{ CompletedClients: completedClients }]);
    Array.prototype.push.apply(sub, [{ TotalClients: TotalClients }]);


    var info = Object.assign.apply(Object, sub);
    return info;
}

export async function sendPushNotification(businessId, data = {}) {
    try {
        let getData = await loginSignUpHelperDB.getBusinessDetails(businessId);
        if (getData) {
            var FCM = require('fcm-node');
            var SERVER_API_KEY = 'AAAAqd8l2yw:APA91bGKAYHPhWRPiga5qirCjlb7rII8h1F8fiS3p3qYbfqByPeapLXs3gPDZqY4bo-zRxlgytNxkGcaRXyljWwCUJv8foA-qe9ydBuBZxCAT7Mg15ie2FdxUTAGltDoA1liyQwAV627'; //put your api key here
            var validDeviceRegistrationToken = getData[0].FcmUserToken; //put a valid device token here
            var fcmCli = new FCM(SERVER_API_KEY);
            var payloadOK = {
                to: validDeviceRegistrationToken,
                data: data,
                priority: 'high',
                content_available: true,
                notification: { //notification object
                    title: 'Knack',
                    body: '',
                    sound: "default",
                    badge: "1"
                }
            };

            var payloadError = {
                to: "336896098496", //invalid registration token
                data: {
                    url: "news"
                },
                priority: 'high',
                content_available: true,
                notification: {
                    title: 'TEST HELLO',
                    body: '123',
                    sound: "default",
                    badge: "1"
                }
            };

            var payloadMulticast = {
                registration_ids: ["336896098496",
                    '123123123',
                    validDeviceRegistrationToken, //valid token among invalid tokens to see the error and ok response
                    '123133213123123'
                ],
                data: {
                    url: "news"
                },
                priority: 'high',
                content_available: true,
                notification: {
                    title: 'Hello',
                    body: 'Multicast',
                    sound: "default",
                    badge: "1"
                }
            };

            var callbackLog = function(sender, err, res) {
                console.log("\n__________________________________")
                console.log("\t" + sender);
                console.log("----------------------------------")
                console.log("err=" + err);
                console.log("res=" + res);
                console.log("----------------------------------\n>>>");
            };

            function sendOK() {
                fcmCli.send(payloadOK, function(err, res) {
                    callbackLog('sendOK', err, res);
                });
            }

            function sendError() {
                fcmCli.send(payloadError, function(err, res) {
                    callbackLog('sendError', err, res);
                });
            }

            function sendMulticast() {
                fcmCli.send(payloadMulticast, function(err, res) {
                    callbackLog('sendMulticast', err, res);
                });
            }

            return sendOK();

        } else {
            throw new ApplicationError(
                "Please provide valid businessId",
                409
            )
        }


    } catch (err) {
        throw err;
    }
}

export async function CheckMainOrCenterAdmin(information) {
    try {

        let userId = information.split(',')[2];
        let businessId = information.split(',')[1];
        let centerId = [];

        for (let i = 3; i < information.split(',').length; i++) {
            centerId.push(information.split(',')[i]);
        }


        let permittedSchedules = createInstance(["teamScheduleMap"], information);

        let dbSchedule = createInstance(["clientSchedule"], information);
        permittedSchedules.teamschedulemap.belongsTo(dbSchedule.schedule, {
            foreignKey: "scheduleId",
            targetKey: "scheduleId"
        });



        let CheckUser = await loginSignUpHelperDB.viewLogin({ businessId: businessId, userId: userId });


        let UserStatus = 0;
        if (CheckUser.length == 0) throw new ApplicationError("Invalid User", 401);

        if (CheckUser[0].userType == 2) {

            if (centerId.length == 0) {

                var checkSchedule = await permittedSchedules.teamschedulemap.findAll({
                    attributes: ['scheduleId'],
                    where: {
                        teamId: userId,
                        status: 1,
                        businessId: businessId
                    },
                    raw: true
                });
            } else if (centerId.length > 0) {

                var checkSchedule = await permittedSchedules.teamschedulemap.findAll({
                    attributes: ['scheduleId'],
                    where: {
                        teamId: userId,
                        status: 1,
                        businessId: businessId
                    },
                    include: [{
                            model: dbSchedule.schedule,

                            attributes: ['centerId'],

                            where: {


                                centerId: {

                                    [Sequelize.Op.in]: centerId

                                }


                            }


                        }


                    ],
                    raw: true
                });
            } else {
                throw new ApplicationError("Error in Center Id", 401);
            }


            if (checkSchedule.length == 0) {
                throw new ApplicationError("User is not permitted to give attendance", 401);
            }

            UserStatus = 2;

        } else {
            UserStatus = 1;
        }
        let Obj = {};
        if (UserStatus == 1) {
            console.log("MAin Admin");

            if (centerId.length == 0) {
                Obj = {
                    status: 1,
                    businessId: businessId,
                };

            } else {
                Obj = {
                    status: 1,
                    businessId: businessId,
                    centerId: {
                        [Sequelize.Op.in]: centerId
                    }
                };
            }

        } else {


            let scheArr = [];
            for (let i = 0; i < checkSchedule.length; i++) {
                scheArr.push(checkSchedule[i].scheduleId);
            }



            if (centerId.length == 0) {
                Obj = {
                    status: 1,
                    businessId: businessId,
                    scheduleId: {
                        [Sequelize.Op.in]: scheArr
                    }
                };
            } else {

                Obj = {
                    status: 1,
                    businessId: businessId,
                    scheduleId: {
                        [Sequelize.Op.in]: scheArr
                    },
                    centerId: {
                        [Sequelize.Op.in]: centerId
                    }
                };

            }


        }
        return [{ Obj: Obj }, { UserStatus: UserStatus }];
    } catch (err) {
        throw err;
    }
}

export async function toLogEntry(Obj, information) {
    let dbLog = createInstance(["log"], information);
    let log = await dbLog.log.create(Obj);
    dbLog.sequelize.close();
    console.log();
}



exports.RemainingClientsOfService__ = async(
    information
) => {
    try {

        let dbclientSubscription = createInstance(["clientSubscription"], information);
        let dbPricepack = createInstance(["clientPricepack"], information);
        let dbservicepricepackmap = createInstance(["servicePricepackMap"], information);
        let dbservices = createInstance(["clientService"], information);
        let dbLog = createInstance(["log"], information);
        let dbClientSchedule = createInstance(["clientScheduleMap"], information);

        dbclientSubscription.subscriptions.belongsTo(dbClientSchedule.clientschedulemap, {
            foreignKey: "subscriptionId",
            targetKey: "subscriptionId"
        });

        let data = await dbservices.services.findAll({
            where: {
                businessId: information.split(",")[1],
                status: 1
            },
            group: ['serviceId'],
            raw: true
        });


        for (let i = 0; i < data.length; i++) {
            let Count = 0;
            let pricepackData = await dbservicepricepackmap.servicepricepackmap.findAll({
                where: {
                    serviceId: data[i].serviceId,
                    status: 1
                },
                raw: true
            })
            console.log(pricepackData);
            for (let j = 0; j < pricepackData.length; j++) {
                let SubscriptionSchedule = await dbclientSubscription.subscriptions.findAll({
                    where: {
                        pricepacks: pricepackData[j].pricepackId
                    },
                    include: [{ model: dbClientSchedule.clientschedulemap }],
                    raw: true
                });

                console.log(SubscriptionSchedule);

                for (let i = 0; i < SubscriptionSchedule.length; i++) {
                    if (SubscriptionSchedule[i]["clientschedulemap.clientScheduleMapId"] == null) {
                        console.log(SubscriptionSchedule);
                        Count++;
                        console.log(Count);
                    }
                }

            }
            let NotificationText = "";
            console.log("---------------" + Count);

            if (Count > 0) {
                NotificationText = Count + ' clients remain unscheduled for ' + data[i].serviceName + ' Schedule them to manage their services & accept payments from them';

                //Log Entry......
                const newLog = {
                    businessId: information.split(",")[1],
                    activity: {
                        Status: 2,
                        header: 'Unscheduled clients',
                        NotificationText: NotificationText,
                        UserStatus: 0,
                        activityDate: new Date(),
                        activityTime: new Date(),
                        message: 'Unscheduled clients',
                    },
                    referenceTable: 'clientScheduleMap,ClientSubscription',
                    CreateDateTime: new Date(),
                    UpdateDateTime: new Date(),
                    status: 1
                };

                // await toLogEntry(newLog, information);
                console.log(newLog);
                return [newLog, Count];


            }
        }

    } catch (error) {
        throw error;
    }
};

exports.RemainingClientsOfService = async(
    information
) => {
    try {

        let info = [];

        let dbclientSubscription = createInstance(["clientSubscription"], information);
        let dbPricepack = createInstance(["clientPricepack"], information);
        let dbservicepricepackmap = createInstance(["servicePricepackMap"], information);
        let dbservices = createInstance(["clientService"], information);
        let dbLog = createInstance(["log"], information);
        let dbClientSchedule = createInstance(["clientScheduleMap"], information);

        dbclientSubscription.subscriptions.belongsTo(dbClientSchedule.clientschedulemap, {
            foreignKey: "subscriptionId",
            targetKey: "subscriptionId"
        });

        let data = await dbservices.services.findAll({
            where: {
                businessId: information.split(",")[1],
                status: 1
            },
            group: ['serviceId'],
            raw: true
        });


        for (let i = 0; i < data.length; i++) {
            let Count = 0;
            let pricepackData = await dbservicepricepackmap.servicepricepackmap.findAll({
                where: {
                    serviceId: data[i].serviceId,
                    status: 1
                },
                raw: true
            })
            for (let j = 0; j < pricepackData.length; j++) {
                let SubscriptionSchedule = await dbclientSubscription.subscriptions.findAll({
                    where: {
                        pricepacks: pricepackData[j].pricepackId
                    },
                    include: [{ model: dbClientSchedule.clientschedulemap }],
                    raw: true
                });


                for (let i = 0; i < SubscriptionSchedule.length; i++) {
                    if (SubscriptionSchedule[i]["clientschedulemap.clientScheduleMapId"] == null) {
                        console.log(SubscriptionSchedule);
                        Count++;
                        console.log(Count);
                    }
                }

            }
            let NotificationText = "";
            let newLog = {};

            if (Count > 0) {
                NotificationText = Count + ' clients remain unscheduled for ' + data[i].serviceName + ' Schedule them to manage their services & accept payments from them';

                //Log Entry......
                newLog = {
                    businessId: information.split(",")[1],
                    activity: {
                        Status: 2,
                        header: 'Unscheduled clients',
                        NotificationText: NotificationText,
                        UserStatus: 0,
                        activityDate: new Date(),
                        activityTime: new Date(),
                        message: 'Unscheduled clients',
                    },
                    referenceTable: 'clientScheduleMap,ClientSubscription',
                    CreateDateTime: new Date(),
                    UpdateDateTime: new Date(),
                    status: 1
                };

                // await toLogEntry(newLog, information);
                //console.log(newLog);


                //return [newLog,Count];
                info.push(newLog);

            }


        }
        return info;

    } catch (error) {
        throw error;
    }
};

export async function sessionInfoBasedOnService(information, getObj) {
    try { //getObj must contain subscriptionId and serviceId both
        let dbSchedule = createInstance(["clientSchedule"], information);
        let dbCSM = createInstance(["clientScheduleMap"], information);
        let dbSPM = createInstance(["servicePricepackMap"], information);
        let dbSubscription = createInstance(["clientSubscription"], information);
        let dbPricepack = createInstance(["clientPricepack"], information);
        let dbCenter = createInstance(["clientCenter"], information);
        dbCSM.clientschedulemap.belongsTo(dbSchedule.schedule, {
            foreignKey: "scheduleId",
            targetKey: "scheduleId"
        });
        dbSchedule.schedule.belongsTo(dbCenter.centers, {
            foreignKey: "centerId",
            targetKey: "centerId"
        });
        dbCSM.clientschedulemap.belongsTo(dbSubscription.subscriptions, {
            foreignKey: "subscriptionId",
            targetKey: "subscriptionId"
        });
        dbSubscription.subscriptions.belongsTo(dbPricepack.pricepacks, {
            foreignKey: "pricepacks",
            targetKey: "pricepackId"
        });
        // dbPricepack.pricepacks.hasMany(dbSPM.servicepricepackmap, {
        //     foreignKey: "pricepackId",
        //     targetKey: "pricepackId"
        // });

        let getInfo = information.split(",");
        let dbBusnessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusnessId };
        const setCond = Object.assign(getObj, setDbid);

        let getSchedule = await dbCSM.clientschedulemap.find({
            where: {
                subscriptionId: setCond.subscriptionId,
                status: 1
            },
            include: [{
                    model: dbSchedule.schedule,
                    attributes: ['serviceId', 'centerId'],
                    where: {
                        serviceId: setCond.serviceId,
                        status: 1
                    },
                    include: [{
                        model: dbCenter.centers,
                        attributes: ['centerId', 'centerName']
                    }]
                },
                {
                    model: dbSubscription.subscriptions,
                    include: [{
                        model: dbPricepack.pricepacks,
                        // include: [
                        //     {
                        //         model: dbSPM.servicepricepackmap,
                        //         where: {
                        //             serviceId: setCond.serviceId
                        //         }
                        //     }
                        // ]
                    }]
                }
            ],
            raw: true
        });

        let getSPM = await dbSPM.servicepricepackmap.find({
            where: {
                serviceId: setCond.serviceId,
                pricepackId: getSchedule['subscription.pricepack.pricepackId'],
                status: 1
            }
        });

        let info = {};
        if (getSchedule != null && getSPM != null) { //if the subscription id exists in clientScheduleMap table
            // let serviceDuration = getSchedule['subscription.pricepack.servicepricepackmaps.serviceDuration'];
            let serviceDuration = getSPM.serviceDuration;
            let sessionAttended = getSchedule['subscription.totalSessionsAttended'] == null ? 0 :
                getSchedule['subscription.totalSessionsAttended'];
            let countPresent = 0,
                countAbsent = 0;
            for (let clientAttendance of getSchedule.attendanceType) {
                if (clientAttendance.atd == 1) countPresent++;
                else if (clientAttendance.atd == 2) countAbsent++;
            }
            let markedAttendance = countPresent + countAbsent;
            var totalSessionAttended = markedAttendance + sessionAttended;
            let durationType = getSchedule['subscription.pricepack.durationType'];
            info = {
                businessId: getSchedule.businessId,
                scheduleId: getSchedule.scheduleId,
                clientId: getSchedule.clientId,
                subscriptionId: getSchedule['subscription.subscriptionId'],
                pricepackId: getSchedule['subscription.pricepack.pricepackId'],
                serviceId: getSchedule['schedule.serviceId'],
                centerId: getSchedule['schedule.center.centerId'],
                centerName: getSchedule['schedule.center.centerName'],
                totalSession: serviceDuration,
                attendedSession: totalSessionAttended,
                durationType: durationType,
                message: durationType == 1 ? totalSessionAttended + "/" + serviceDuration + " Sessions Completed" : durationType == 2 ? totalSessionAttended + " Days Completed" : durationType == 3 ? totalSessionAttended + " Weeks Completed" : durationType == 4 ? totalSessionAttended + " Months Completed" : durationType == 5 ? totalSessionAttended + " Years Completed" : durationType == 6 ? totalSessionAttended + " Items Completed" : durationType == 7 ? totalSessionAttended + " Hours Completed" : ""
            };
        }
        dbSchedule.sequelize.close();
        dbCSM.sequelize.close();
        dbSPM.sequelize.close();
        dbSubscription.sequelize.close();
        dbPricepack.sequelize.close();
        dbCenter.sequelize.close();
        return await info;
    } catch (error) {
        throw error;
    }
}
//end of Calculation


export async function dateAttendance(information, setObj, date, time) { //returns number of attendance for a particular date
    try { //set obj can contain either a scheduleId or a clientId or both of them
        let getInfo = information.split(",");
        let dbBusnessId = getInfo[1];
        let setDbid = { businessId: dbBusnessId };
        const setCond = Object.assign(setObj, setDbid);
        let givenDatetime = new Date(date + ' ' + time);
        let dbCSM = createInstance(["clientScheduleMap"], information);
        let clients = await dbCSM.clientschedulemap.findAll({
            where: setCond,
            raw: true
        });
        let countPresent = 0,
            countAbsent = 0,
            countExcused = 0,
            countUnmarked = 0,
            countTotal = 0;
        for (let client of clients) {
            for (let clientAtd of client.attendanceType) {
                let clientDatetime = new Date(clientAtd.scdate + ' ' + clientAtd.scheduleStartTime)
                if (+clientDatetime === +givenDatetime) {
                    if (clientAtd.atd == 1) countPresent++;
                    else if (clientAtd.atd == 2) countAbsent++;
                    else if (clientAtd.atd == 3 || clientAtd.atd == 7) countExcused++;
                    else if (clientAtd.atd == 4) countUnmarked++;
                }
            }
        }
        let totalAtd = countPresent + countAbsent + countExcused + countUnmarked;
        let perPresent = (countPresent == 0) ? 0 : (100 / totalAtd) * countPresent;
        let perAbsent = (countAbsent == 0) ? 0 : (100 / totalAtd) * countAbsent;
        let perExcused = (countExcused == 0) ? 0 : (100 / totalAtd) * countExcused;
        let perUnmarked = (countUnmarked == 0) ? 0 : (100 / totalAtd) * countUnmarked;
        let totalClients = countPresent + countAbsent + countExcused + countUnmarked;
        let markedClients = countPresent + countAbsent + countExcused;
        let info = {
            present: (perPresent.toFixed(0)),
            absent: (perAbsent.toFixed(0)),
            excused: (perExcused.toFixed(0)),
            unmarked: (perUnmarked.toFixed(0)),
            totalClients: totalClients,
            markedClients: markedClients
        };

        dbCSM.sequelize.close();
        return info;
    } catch (error) {
        throw error;
    }
}

// Client Schedules
export async function getAttendanceJSON(scheduleArr, scdate, pricepackValidity) {
    try {
        let atdObj = [];
        let pval = pricepackValidity;
        scheduleArr.sort((a, b) => new Date(a.scdate) - new Date(b.scdate));
        for (let scdl of scheduleArr) {
            if (new Date(scdl.scdate) >= new Date(scdate) && pricepackValidity > 0) {
                let s_date = scdl.scdate.split("-"),
                    s_time = scdl.scheduleStartTime.split(":");
                let id = "" + s_date[0] + s_date[1] + s_date[2] + s_time[0] + s_time[1];
                atdObj.push({
                    id: id,
                    scdate: scdl.scdate,
                    atd: 4,
                    atdBy: "",
                    atdDate: "",
                    atdTime: "",
                    reschedule: 1,
                    rescheduleId: "",
                    rescheduleDate: scdl.scdate,
                    scheduleStartTime: scdl.scheduleStartTime,
                    scheduleEndTime: scdl.scheduleEndTime,
                    scheduleFrom: "",
                    centerId: scdl.centerId,
                    rescheduleBy: "",
                    comment: "",
                });
                pricepackValidity--;
            }
        }
        // if (atdObj.length < pval) {
        //     throw new ApplicationError(
        //         "number of schedules are less than pricepack validity after the given date !",
        //         401
        //     );
        // } else 
        return await atdObj;
    } catch (error) {
        throw error;
    }
}

// session percentage
export async function getSessionPercentage(information, subscriptionId) {
    try {
        let dbCSM = createInstance(["clientScheduleMap"], information);
        let dbSPM = createInstance(["servicePricepackMap"], information);
        let dbSubscription = createInstance(["clientSubscription"], information);
        let dbPricepack = createInstance(["clientPricepack"], information);
        let dbCenter = createInstance(["clientCenter"], information);
        dbCSM.clientschedulemap.belongsTo(dbSubscription.subscriptions, {
            foreignKey: "subscriptionId",
            targetKey: "subscriptionId"
        });
        dbSubscription.subscriptions.belongsTo(dbPricepack.pricepacks, {
            foreignKey: "pricepacks",
            targetKey: "pricepackId"
        });
        let subscriptions = await dbSubscription.subscriptions.find({
            where: {
                subscriptionId: subscriptionId,
                status: 1
            },
            include: [{
                model: dbPricepack.pricepacks
            }],
            raw: true
        });
        if (subscriptions == null) {
            throw new ApplicationError(
                "Invalid Subscription",
                401
            );
        }
        let serviceDuration = subscriptions['pricepack.serviceDuration'];
        let totalSessionAttended = subscriptions.totalSessionsAttended == null ? 0 :
            subscriptions.totalSessionsAttended;

        let getCSMS = await dbCSM.clientschedulemap.findAll({
            where: {
                subscriptionId: subscriptionId,
                status: 1
            },
            raw: true
        });
        if (getCSMS[0]) {
            for (let CSM of getCSMS) {
                let countPresent = 0,
                    countAbsent = 0;
                for (let clientAttendance of CSM.attendanceType) {
                    if (clientAttendance.atd == 1) countPresent++;
                    else if (clientAttendance.atd == 2) countAbsent++;
                }
                totalSessionAttended += countPresent + countAbsent;
            }
        }
        let info = (parseInt(totalSessionAttended) / parseInt(serviceDuration)) * 100;
        dbCSM.sequelize.close();
        dbSPM.sequelize.close();
        dbSubscription.sequelize.close();
        dbPricepack.sequelize.close();
        dbCenter.sequelize.close();
        return await {
            percentage: parseInt(info.toFixed(0))
        };
    } catch (error) {
        throw error;
    }
}


export async function getAmountPercentage(information, subTotal, total) {
    try {
        let cal = 0;
        if (parseInt(subTotal) <= parseInt(total)) {
            cal = ((parseInt(subTotal) / parseInt(total)) * 100).toFixed(0);
        }
        return cal;
    } catch (error) {
        throw error;
    }
}

export async function bookingCountInfo(information, getobj, date) { //returns number of schedule of a business
    try {

        let getInfo = information.split(",");
        let dbBusnessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusnessId };
        const setCond = Object.assign(getobj, setDbid);
        let today = await hyphenDateFormat(date);

        let db = createInstance(["clientSchedule"], information);
        let schedules = await db.schedule.findAll({
            where: setCond,
            raw: true
        });
        var countSchedule = 0;
        for (let schedule of schedules) {
            for (let stime of schedule.scheduleTime) {
                if (+new Date(stime.scdate) === +new Date(today) && stime.reschedule == 1) {
                    countSchedule++;
                }
            }
        }

        db.sequelize.close();
        return countSchedule;
    } catch (error) {
        throw error;
    }
}

export async function sessionCount(information, getObj) {
    try { //getObj must contain subscriptionId and serviceId both
        let dbSchedule = createInstance(["clientSchedule"], information);
        let dbCSM = createInstance(["clientScheduleMap"], information);
        let dbSPM = createInstance(["servicePricepackMap"], information);
        let dbSubscription = createInstance(["clientSubscription"], information);
        let dbPricepack = createInstance(["clientPricepack"], information);
        let dbCenter = createInstance(["clientCenter"], information);
        dbCSM.clientschedulemap.belongsTo(dbSchedule.schedule, {
            foreignKey: "scheduleId",
            targetKey: "scheduleId"
        });
        dbSchedule.schedule.belongsTo(dbCenter.centers, {
            foreignKey: "centerId",
            targetKey: "centerId"
        });
        dbCSM.clientschedulemap.belongsTo(dbSubscription.subscriptions, {
            foreignKey: "subscriptionId",
            targetKey: "subscriptionId"
        });
        dbSubscription.subscriptions.belongsTo(dbPricepack.pricepacks, {
            foreignKey: "pricepacks",
            targetKey: "pricepackId"
        });


        let getInfo = information.split(",");
        let dbBusnessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusnessId };
        const setCond = Object.assign(getObj, setDbid);


        let getMasterScheduleId = await dbCSM.clientschedulemap.findAll({
            attributes: ['masterParentScheduleId'],
            where: {
                status: 1,
                scheduleId: getObj.scheduleId
            },
            raw: true
        });


        let getSchedule = await dbCSM.clientschedulemap.findAll({


            where: {
                subscriptionId: setCond.subscriptionId,
                status: 1,
                scheduleId: getMasterScheduleId[0].masterParentScheduleId
            },
            include: [{
                    model: dbSchedule.schedule,
                    attributes: ['serviceId', 'centerId'],
                    where: {
                        serviceId: setCond.serviceId,
                        status: 1
                    },
                    include: [{
                        model: dbCenter.centers,
                        attributes: ['centerId', 'centerName']
                    }]
                },
                {
                    model: dbSubscription.subscriptions,
                    include: [{
                        model: dbPricepack.pricepacks,

                    }]
                }
            ],
            raw: true
        });


        let getSPM = await dbSPM.servicepricepackmap.find({
            where: {
                serviceId: setCond.serviceId,
                pricepackId: getSchedule[0]['subscription.pricepack.pricepackId'],
                status: 1
            },
            raw: true
        });

        let info = {};
        if (getSchedule[0] != null && getSPM != null) { //if the subscription id exists in clientScheduleMap table
            // let serviceDuration = getSchedule['subscription.pricepack.servicepricepackmaps.serviceDuration'];
            let serviceDuration = getSPM.serviceDuration;
            let sessionAttended = getSchedule[0]['subscription.totalSessionsAttended'] == null ? 0 :
                getSchedule[0]['subscription.totalSessionsAttended'];


            var totalSessionAttended = getSchedule[0].sessionCount;
            let durationType = getSchedule[0]['subscription.pricepack.durationType'];
            info = {
                businessId: getSchedule.businessId,
                scheduleId: getSchedule[0].scheduleId,
                clientId: getSchedule[0].clientId,
                subscriptionId: getSchedule[0]['subscription.subscriptionId'],
                pricepackId: getSchedule[0]['subscription.pricepack.pricepackId'],
                serviceId: getSchedule[0]['schedule.serviceId'],
                centerId: getSchedule[0]['schedule.center.centerId'],
                centerName: getSchedule[0]['schedule.center.centerName'],
                totalSession: serviceDuration,
                attendedSession: totalSessionAttended,
                durationType: durationType,
                message: durationType == 1 ? totalSessionAttended + "/" + serviceDuration + " Sessions Completed" : durationType == 2 ? totalSessionAttended + " Days Completed" : durationType == 3 ? totalSessionAttended + " Weeks Completed" : durationType == 4 ? totalSessionAttended + " Months Completed" : durationType == 5 ? totalSessionAttended + " Years Completed" : durationType == 6 ? totalSessionAttended + " Items Completed" : durationType == 7 ? totalSessionAttended + " Hours Completed" : ""
            };
        }


        dbSchedule.sequelize.close();
        dbCSM.sequelize.close();
        dbSPM.sequelize.close();
        dbSubscription.sequelize.close();
        dbPricepack.sequelize.close();
        dbCenter.sequelize.close();
        return await info;
    } catch (error) {
        throw error;
    }
}
//end of Calculation

// Session calculation using service id
export async function sessionCalc(information, subscriptionId, serviceId = "") { //returns session details
    try {
        let dbSchedule = createInstance(["clientSchedule"], information);
        let dbCSM = createInstance(["clientScheduleMap"], information);
        let dbSPM = createInstance(["servicePricepackMap"], information);
        let dbSubscription = createInstance(["clientSubscription"], information);
        let dbPricepack = createInstance(["clientPricepack"], information);
        let dbCenter = createInstance(["clientCenter"], information);
        dbCSM.clientschedulemap.belongsTo(dbSchedule.schedule, {
            foreignKey: "scheduleId",
            targetKey: "scheduleId"
        });
        dbSchedule.schedule.belongsTo(dbCenter.centers, {
            foreignKey: "centerId",
            targetKey: "centerId"
        });
        dbCSM.clientschedulemap.belongsTo(dbSubscription.subscriptions, {
            foreignKey: "subscriptionId",
            targetKey: "subscriptionId"
        });
        dbSubscription.subscriptions.belongsTo(dbPricepack.pricepacks, {
            foreignKey: "pricepacks",
            targetKey: "pricepackId"
        });

        let businessId = information.split(",")[1];

        let info = {};
        if (serviceId.trim() == "") { //serviceId not found. show data directly from pricepack and subscription
            let subscription = await dbSubscription.subscriptions.find({
                where: {
                    subscriptionId: subscriptionId
                },
                include: [{
                    model: dbPricepack.pricepacks
                }],
                raw: true
            });
            if (subscription != null) {
                let serviceDuration = subscription['pricepack.serviceDuration'],
                    durationType = subscription['pricepack.durationType'],
                    totalSessionAttended = subscription.totalSessionsAttended;
                info = [{
                    businessId: businessId,
                    scheduleId: "",
                    clientId: subscription.clientId,
                    subscriptionId: subscriptionId,
                    pricepackId: subscription['pricepack.pricepackId'],
                    serviceId: "",
                    centerId: "",
                    centerName: "",
                    totalSession: serviceDuration,
                    attendedSession: totalSessionAttended,
                    durationType: durationType,
                    message: durationType == 1 ? totalSessionAttended + "/" + serviceDuration + " Sessions Completed" : durationType == 2 ? totalSessionAttended + " Days Completed" : durationType == 3 ? totalSessionAttended + " Weeks Completed" : durationType == 4 ? totalSessionAttended + " Months Completed" : durationType == 5 ? totalSessionAttended + " Years Completed" : durationType == 6 ? totalSessionAttended + " Items Completed" : durationType == 7 ? totalSessionAttended + " Hours Completed" : ""
                }];
            } else {
                throw new ApplicationError(
                    "SubscriptionId is not found",
                    401
                );
            }
        } else { //if serviceId exists then the subscription must has been scheduled
            let getSchedule = await dbCSM.clientschedulemap.find({
                where: {
                    subscriptionId: subscriptionId,
                    status: 1
                },
                include: [{
                        model: dbSchedule.schedule,
                        attributes: ['serviceId', 'centerId'],
                        where: {
                            serviceId: serviceId,
                            status: 1
                        },
                        include: [{
                            model: dbCenter.centers,
                            attributes: ['centerId', 'centerName']
                        }]
                    },
                    {
                        model: dbSubscription.subscriptions,
                        include: [{
                            model: dbPricepack.pricepacks
                        }]
                    }
                ],
                raw: true
            });
            if (getSchedule == null) {
                throw new ApplicationError(
                    "Invalid input. Schedule is not found !",
                    401
                );
            } else {
                let getSPM = await dbSPM.servicepricepackmap.find({
                    where: {
                        serviceId: serviceId,
                        pricepackId: getSchedule['subscription.pricepack.pricepackId'],
                        status: 1
                    }
                });
                if (getSchedule != null && getSPM != null) { //if the subscription id exists in clientScheduleMap table
                    let serviceDuration = getSPM.serviceDuration;
                    let sessionAttended = getSchedule['subscription.totalSessionsAttended'] == null ? 0 :
                        getSchedule['subscription.totalSessionsAttended'];
                    let allSchedules = await dbCSM.clientschedulemap.findAll({
                        where: {
                            clientId: getSchedule.clientId,
                            masterParentScheduleId: getSchedule.masterParentScheduleId
                        },
                        raw: true
                    });
                    let countPresent = 0,
                        countAbsent = 0;
                    for (let oneSchedule of allSchedules) {
                        for (let atd of oneSchedule.attendanceType) {
                            if (atd.atd == 1) countPresent++;
                            else if (atd.atd == 2) countAbsent++;
                        }
                    }
                    let markedAttendance = countPresent + countAbsent;
                    var totalSessionAttended = markedAttendance + sessionAttended;
                    let durationType = getSchedule['subscription.pricepack.durationType'];
                    info = [{
                        businessId: getSchedule.businessId,
                        scheduleId: getSchedule.scheduleId,
                        clientId: getSchedule.clientId,
                        subscriptionId: getSchedule['subscription.subscriptionId'],
                        pricepackId: getSchedule['subscription.pricepack.pricepackId'],
                        serviceId: getSchedule['schedule.serviceId'],
                        centerId: getSchedule['schedule.center.centerId'],
                        centerName: getSchedule['schedule.center.centerName'],
                        totalSession: serviceDuration,
                        attendedSession: totalSessionAttended,
                        durationType: durationType,
                        message: durationType == 1 ? totalSessionAttended + "/" + serviceDuration + " Sessions Completed" : durationType == 2 ? totalSessionAttended + " Days Completed" : durationType == 3 ? totalSessionAttended + " Weeks Completed" : durationType == 4 ? totalSessionAttended + " Months Completed" : durationType == 5 ? totalSessionAttended + " Years Completed" : durationType == 6 ? totalSessionAttended + " Items Completed" : durationType == 7 ? totalSessionAttended + " Hours Completed" : ""
                    }];
                } else {
                    throw new ApplicationError(
                        "given service isn't available in this schedule",
                        401
                    );
                }
            }
        }

        dbSchedule.sequelize.close();
        dbCSM.sequelize.close();
        dbSPM.sequelize.close();
        dbSubscription.sequelize.close();
        dbPricepack.sequelize.close();
        dbCenter.sequelize.close();
        return await info;
    } catch (error) {
        throw error;
    }


}

///// New Attendance Calculation

export async function getAttendanceSheet(scheduleArr, scdate, expiryDate) {
    try {
        let atdObj = [];
        scheduleArr.sort((a, b) => new Date(a.scdate) - new Date(b.scdate));
        for (let scdl of scheduleArr) {
            if ((new Date(scdl.scdate) >= new Date(scdate)) && (new Date(scdl.scdate) <= new Date(expiryDate))) {
                let s_date = scdl.scdate.split("-"),
                    s_time = scdl.scheduleStartTime.split(":");
                let id = "" + s_date[0] + s_date[1] + s_date[2] + s_time[0] + s_time[1];
                atdObj.push({
                    id: id,
                    scdate: scdl.scdate,
                    atd: 4,
                    atdBy: "",
                    atdDate: "",
                    atdTime: "",
                    reschedule: 1,
                    rescheduleId: "",
                    rescheduleDate: scdl.scdate,
                    scheduleStartTime: scdl.scheduleStartTime,
                    scheduleEndTime: scdl.scheduleEndTime,
                    scheduleFrom: "",
                    centerId: scdl.centerId,
                    rescheduleBy: "",
                    comment: "",
                });
            }
        }
        return await atdObj;
    } catch (error) {
        throw error;
    }
}

// show booking info
export async function getBookingGraph(information) {
    try {
        //connection
        let seqdb = new Sequelize(information.split(',')[1], KNACKDB.user, KNACKDB.password, {
            host: KNACKDB.host,
            port: 3306,
            dialect: "mysql",
            pool: {
                max: 100,
                min: 0,
                idle: 10000
            }
        });

        //array of 7 dates
        let dateArr = [];
        let tDay = new Date();
        let pdate1 = tDay.getTime() - 1 * 24 * 60 * 60 * 1000;
        let pdate2 = tDay.getTime() - 2 * 24 * 60 * 60 * 1000;
        let pdate3 = tDay.getTime() - 3 * 24 * 60 * 60 * 1000;
        let tdate1 = tDay.getTime() + 1 * 24 * 60 * 60 * 1000;
        let tdate2 = tDay.getTime() + 2 * 24 * 60 * 60 * 1000;
        let tdate3 = tDay.getTime() + 3 * 24 * 60 * 60 * 1000;
        dateArr.push(await hyphenDateFormat(pdate3));
        dateArr.push(await hyphenDateFormat(pdate2));
        dateArr.push(await hyphenDateFormat(pdate1));
        dateArr.push(await hyphenDateFormat(tDay));
        dateArr.push(await hyphenDateFormat(tdate1));
        dateArr.push(await hyphenDateFormat(tdate2));
        dateArr.push(await hyphenDateFormat(tdate3));

        let graphInfo = [];
        for (let thisDate of dateArr) {
            let booking = await seqdb.query("select count(*) as bookingCount from schedule where json_contains(`scheduleTime`, '{\"scdate\" : \"" + thisDate + "\"}');", {
                type: Sequelize.QueryTypes.SELECT
            });
            let present = await seqdb.query("SELECT COUNT(*) as presentCount FROM clientschedulemap WHERE JSON_CONTAINS(`attendanceType`, JSON_OBJECT(\'scdate\', \"" + thisDate + "\", \'atd\', 1))", {
                type: Sequelize.QueryTypes.SELECT
            });
            let absent = await seqdb.query("SELECT COUNT(*) as absentCount FROM clientschedulemap WHERE JSON_CONTAINS(`attendanceType`, JSON_OBJECT(\'scdate\', \"" + thisDate + "\", \'atd\', 2))", {
                type: Sequelize.QueryTypes.SELECT
            });
            let excused = await seqdb.query("SELECT COUNT(*) as excusedCount FROM clientschedulemap WHERE JSON_CONTAINS(`attendanceType`, JSON_OBJECT(\'scdate\', \"" + thisDate + "\", \'atd\', 3))", {
                type: Sequelize.QueryTypes.SELECT
            });
            let unmarked = await seqdb.query("SELECT COUNT(*) as unmarkedCount FROM clientschedulemap WHERE JSON_CONTAINS(`attendanceType`, JSON_OBJECT(\'scdate\', \"" + thisDate + "\", \'atd\', 4))", {
                type: Sequelize.QueryTypes.SELECT
            });
            // SELECT JSON_CONTAINS(
            //     '[{"id": "e0c2","name": "she"},{"id": "e0c2", "name": "another_she"}]', 
            //     JSON_OBJECT('id', "e0c2", 'name', "a")
            //     );

            //percentage calculation
            let totalCal = present[0]["presentCount"] + absent[0]["absentCount"] + excused[0]["excusedCount"] + unmarked[0]["unmarkedCount"];
            let presentPercentage = totalCal == 0 ? 0 : (present[0]["presentCount"] / totalCal) * 100;
            let absentPercentage = totalCal == 0 ? 0 : (absent[0]["absentCount"] / totalCal) * 100;
            let excusedPercentage = totalCal == 0 ? 0 : (excused[0]["excusedCount"] / totalCal) * 100;
            let unmarkedPercentage = totalCal == 0 ? 0 : (unmarked[0]["unmarkedCount"] / totalCal) * 100;

            //object pushed into array
            graphInfo.push({
                scdate: thisDate,
                bookingCount: parseInt(booking[0]["bookingCount"]),
                present: parseInt(presentPercentage.toFixed(0)),
                absent: parseInt(absentPercentage.toFixed(0)),
                excused: parseInt(excusedPercentage.toFixed(0)),
                unmarked: parseInt(unmarkedPercentage.toFixed(0))
            });
        }
        return await graphInfo;
    } catch (error) {
        throw error;
    }
}

export async function getAtdSheet(scheduleArr, scdate, expiryDate) {
    try {
        let atdObj = [];
        scheduleArr.sort((a, b) => new Date(a.scdate) - new Date(b.scdate));
        for (let scdl of scheduleArr) {
            if ((new Date(scdl.scdate) >= new Date(scdate)) && (new Date(scdl.scdate) <= new Date(expiryDate))) {
                let s_date = scdl.scdate.split("-"),
                    s_time = scdl.scheduleStartTime.split(":");
                let id = "" + s_date[0] + s_date[1] + s_date[2] + s_time[0] + s_time[1];
                atdObj.push({
                    id: id,
                    scdate: scdl.scdate,
                    atd: scdl.reschedule == 2 ? 6 : scdl.reschedule == 3 ? 5 : 4,
                    atdBy: "",
                    atdDate: "",
                    atdTime: "",
                    reschedule: scdl.reschedule,
                    rescheduleId: scdl.rescheduleId,
                    rescheduleDate: scdl.rescheduleDate,
                    scheduleStartTime: scdl.scheduleStartTime,
                    scheduleEndTime: scdl.scheduleEndTime,
                    scheduleFrom: scdl.scheduleFrom,
                    centerId: scdl.centerId,
                    rescheduleBy: scdl.rescheduleBy,
                    comment: scdl.comment,
                });
            }
        }
        return await atdObj;
    } catch (error) {
        throw error;
    }
}

export async function mysql_real_escape_string(str) {
    return str.replace(/[\0\x08\x09\x1a\n\r"'\\\%]/g, function(char) {
        switch (char) {
            case "\0":
                return "\\0";
            case "\x08":
                return "\\b";
            case "\x09":
                return "\\t";
            case "\x1a":
                return "\\z";
            case "\n":
                return "\\n";
            case "\r":
                return "\\r";
            case "\"":
            case "'":
            case "\\":
            case "%":
                return "\\" + char; // prepends a backslash to backslash, percent,
                // and double/single quotes
        }
    });
}


export async function expiryDate(information, subscriptionId) {
    let dbclientSchedule = createInstance(["clientScheduleMap"], information);
    let dbSub = createInstance(["clientSubscription"], information);
    let dbpricepack = createInstance(["clientPricepack"], information);
    let dbservicePricepackMap = createInstance(["servicePricepackMap"], information);

    dbSub.subscriptions.belongsTo(dbpricepack.pricepacks, {
        foreignKey: "pricepacks",
        targetKey: "pricepackId"
    });
    let data = await dbSub.subscriptions.findAll(
        {
            where: {
                subscriptionId: subscriptionId
            },

            include: [{
                model: dbpricepack.pricepacks
            }],
            raw: true
        }

    );
    let day = new Date(data[0].subscriptionDateTime);
    let expirydate = new Date(data[0].subscriptionDateTime).setDate(day.getDate() + data[0]["pricepack.pricepackValidity"]);
    let newdate = new Date(expirydate).toLocaleDateString();
    return newdate;
    dbclientSchedule.sequelize.close();
    dbSub.sequelize.close();
    dbpricepack.sequelize.close();
    dbservicePricepackMap.sequelize.close();
}