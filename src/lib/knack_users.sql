-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 05, 2018 at 06:51 AM
-- Server version: 5.7.19
-- PHP Version: 7.0.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `knack_users`
--

-- --------------------------------------------------------

--
-- Table structure for table `businessinfo`
--

DROP TABLE IF EXISTS `businessinfo`;
CREATE TABLE IF NOT EXISTS `businessinfo` (
  `businessId` varchar(200) NOT NULL COMMENT 'Business ID',
  `businessName` varchar(200) DEFAULT NULL COMMENT 'Business Name',
  `businessType` varchar(200) DEFAULT NULL COMMENT 'Business Type',
  `numberOfClients` int(11) DEFAULT NULL COMMENT 'Number of Clients',
  `numberOfCenters` int(11) DEFAULT NULL COMMENT 'Number of Centers',
  `numberOfTeamMembers` int(11) DEFAULT NULL COMMENT 'Number of Team Members',
  `contactNumber` varchar(200) NOT NULL COMMENT 'Contact Numbers',
  `emailId` varchar(200) NOT NULL COMMENT 'Email ID',
  `area` varchar(200) DEFAULT NULL COMMENT 'Area',
  `pin` varchar(100) DEFAULT NULL COMMENT 'Pincode',
  `city` varchar(200) DEFAULT NULL COMMENT 'City',
  `landmark` varchar(200) DEFAULT NULL COMMENT 'Landmark',
  `photoUrl` varchar(200) DEFAULT NULL COMMENT 'Business Logo',  
  `business_days` varchar(200) DEFAULT NULL COMMENT 'Business Days',  
  `createDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Create Date and Time',
  `updateDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Update Date and Time',
  `status` int(2) NOT NULL COMMENT '0-Inactive, 1-Active',
  PRIMARY KEY (`businessId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `centers`
--

DROP TABLE IF EXISTS `centers`;
CREATE TABLE IF NOT EXISTS `centers` (
  `centerId` varchar(150) NOT NULL COMMENT 'Center ID',
  `businessId` varchar(150) NOT NULL COMMENT 'Business ID',
  `centerName` varchar(150) NOT NULL COMMENT 'Center Name',
  `area` varchar(200) NOT NULL COMMENT 'Area',
  `landmark` varchar(200) NOT NULL COMMENT 'Landmark',
  `pin` int(10) NOT NULL COMMENT 'Pincode',
  `city` varchar(150) NOT NULL COMMENT 'City Name',
  `contactNumber` varchar(100) NOT NULL COMMENT 'Contact Number',
  `updateFlag` int(11) DEFAULT NULL COMMENT 'Update Flag for checking if the record is updated while updating business',
  `createdBy` varchar(150) DEFAULT NULL COMMENT 'Created By',
  `createDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Create Date Time',
  `updateDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Update Date and Time',
  `status` int(2) NOT NULL COMMENT '0-Closed, 1-Active',
  PRIMARY KEY (`centerId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
CREATE TABLE IF NOT EXISTS `clients` (
  `clientId` varchar(150) NOT NULL COMMENT 'Client ID',
  `businessId` varchar(150) NOT NULL COMMENT 'Business ID',
  `centerId` varchar(150) DEFAULT NULL COMMENT 'Center ID',
  `clientName` varchar(200) NOT NULL COMMENT 'Client Name',
  `contactNumber` varchar(100) NOT NULL COMMENT 'Contact Number',
  `alternateNumber` varchar(100) DEFAULT NULL COMMENT 'Alternate Number',
  `emailId` varchar(250) DEFAULT NULL COMMENT 'Email ID',
  `alternateEmail` varchar(250) DEFAULT NULL COMMENT 'alternate Email ID',
  `dateOfBirth` date DEFAULT NULL COMMENT 'Date of Birth',
  `area` varchar(200) DEFAULT NULL COMMENT 'Address',
  `pin` varchar(200) DEFAULT NULL COMMENT 'Pincode',
  `city` varchar(150) DEFAULT NULL COMMENT 'City',
  `photoUrl` varchar(200) DEFAULT NULL COMMENT 'Photo',
  `createdBy` varchar(150) DEFAULT NULL COMMENT 'Created By',
  `createDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Create Date Time',
  `updateDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Update Date and Time',
  `status` int(2) NOT NULL COMMENT '0-Inactive, 1-Active',
  PRIMARY KEY (`clientId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

DROP TABLE IF EXISTS `employees`;
CREATE TABLE IF NOT EXISTS `employees` (
  `employeeId` varchar(150) NOT NULL COMMENT 'Employee ID',
  `roleId` int(11) DEFAULT NULL COMMENT 'User Role ID from role.roleId',
  `businessId` varchar(150) NOT NULL COMMENT 'Bussiness ID',
  `centerId` varchar(150) DEFAULT NULL COMMENT 'Center ID',
  `employeeName` varchar(150) DEFAULT NULL COMMENT 'Employee Name',
  `contactNumber` varchar(100) NOT NULL COMMENT 'Employee Number',
  `emailId` varchar(250) DEFAULT NULL COMMENT 'Employee Email',
  `alternateNumber` varchar(100) DEFAULT NULL COMMENT 'Alternate Number',
  `alternateEmail` varchar(250) DEFAULT NULL COMMENT 'Alternate Email',
  `dateOfBirth` date DEFAULT NULL COMMENT 'Date of Birth',
  `area` varchar(200) DEFAULT NULL COMMENT 'Area',
  `pin` varchar(10) DEFAULT NULL COMMENT 'Pincode',
  `city` varchar(150) DEFAULT NULL COMMENT 'City',
  `photoUrl` varchar(200) DEFAULT NULL,
  `createdBy` varchar(150) DEFAULT NULL COMMENT 'Created By',
  `status` int(2) NOT NULL COMMENT '0-Inactive,1-Active',
  `createDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Create Date Time',
  `updateDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Update Date and Time',
  PRIMARY KEY (`employeeId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `offerings`
--

-- DROP TABLE IF EXISTS `offerings`;
-- CREATE TABLE IF NOT EXISTS `offerings` (
--   `offeringId` varchar(150) NOT NULL COMMENT 'Offering ID',
--   `businessId` varchar(150) NOT NULL COMMENT 'Business ID',
--   `centerId` varchar(150) NOT NULL COMMENT 'Center ID',
--   `offeringName` varchar(200) NOT NULL COMMENT 'offering Name',
--   `offeringDetails` varchar(600) NOT NULL COMMENT 'Offering Details',
--   `createDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Create Date and Time',
--   `updateDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Update Date and Time',
--   `status` int(2) NOT NULL COMMENT '0-Inactive, 1-Active',
--   PRIMARY KEY (`offeringId`)
-- ) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `services` // rename offerings table into services
--

DROP TABLE IF EXISTS `services`;
CREATE TABLE IF NOT EXISTS `services` (
  `serviceId` varchar(150) NOT NULL COMMENT 'Service ID',
  `businessId` varchar(150) NOT NULL COMMENT 'Business ID',  
  `serviceName` varchar(200) NOT NULL COMMENT 'Service Name',
  `serviceDetails` varchar(600) DEFAULT NULL COMMENT 'Service Details',
  `createdBy` varchar(600) DEFAULT NULL COMMENT 'Created By',
  `createDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Create Date and Time',
  `updateDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Update Date and Time',
  `status` int(2) NOT NULL COMMENT '0-Inactive, 1-Active',
  PRIMARY KEY (`serviceId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

DROP TABLE IF EXISTS `offers`;
CREATE TABLE IF NOT EXISTS `offers` (
  `id` varchar(150) NOT NULL COMMENT 'Offer ID',
  `businessId` varchar(150) NOT NULL COMMENT 'Business ID',
  `centerId` varchar(150) DEFAULT NULL COMMENT 'Center ID',
  `offerName` varchar(200) NOT NULL COMMENT 'Offer Name',
  `offerType` int(11) NOT NULL COMMENT '1-Amount, 2-Percentage',
  `offerValue` float NOT NULL COMMENT 'Offer Value in percentage or amount',
  `createDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Create Date and Time',
  `updateDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Update Date and Time',
  `status` int(2) NOT NULL COMMENT '1-Active, 0-InActive',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `schedule`
--

DROP TABLE IF EXISTS `schedule`;
CREATE TABLE IF NOT EXISTS `schedule` (
  `scheduleId` varchar(150) NOT NULL COMMENT 'schedule ID',
  `businessId` varchar(150) NOT NULL COMMENT 'Business ID',
  `centerId` varchar(150) DEFAULT NULL COMMENT 'Center ID',  
  `rescheduleId` varchar(150) DEFAULT NULL COMMENT 'Store Schedule ID',  
  `serviceId` varchar(150) NOT NULL COMMENT 'Service ID',  
  `scheduleName` varchar(150) DEFAULT NULL COMMENT 'Booking Name',  
  `scheduleDate` date NOT NULL COMMENT 'Scheduled Date',
  `startTime` time DEFAULT NULL COMMENT 'Start Time',
  `endTime` time NOT NULL COMMENT 'End Time',
  `scheduleType` int(11) NOT NULL COMMENT '1-Scheduled, 2-Rescheduled',
  `parent_scheduleId` varchar(150) DEFAULT NULL COMMENT 'Store Parent Schedule ID',
  `schedule_status` int(11) NOT NULL COMMENT '1-Active Reschedule, 2-Inactive Rescheduled',  
  `scheduleTime` json DEFAULT NULL COMMENT 'Schedule Date Time',  
  `numberOfClients` int(11) DEFAULT NULL COMMENT 'Number of Clients',  
  `batchId` varchar(150) DEFAULT NULL COMMENT 'Batch ID',
  `comments` varchar(300) DEFAULT NULL COMMENT 'Comments if any',
  `is_repeat` int(11) DEFAULT NULL COMMENT '1-Yes, 0-No',
  `repeatation_type` int(11) DEFAULT NULL COMMENT '1-Day, 2-Week, 3-Month, 4-Year',
  `repeat_number` int(11) DEFAULT NULL COMMENT 'Repetition number for day,weeks,months and year',
  `repeat_days` varchar(100) DEFAULT NULL COMMENT 'If you select week option;7 days in a week,i.e 1-7(Select multiple days)',
  `monthly_type` varchar(100) DEFAULT NULL COMMENT 'If you select Monthly option;1=On date,2=On day i.e last month of the monday',
  `repeat_ends` int(11) DEFAULT NULL COMMENT '1-On Date, 2-Never',
  `repeat_ends_date` date DEFAULT NULL COMMENT 'Repeat End Date with date value',
  `createdBy` varchar(150) DEFAULT NULL COMMENT 'Created By',
  `createDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Create Date Time and',
  `updateDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Update Date and Time',
  `status` int(11) NOT NULL COMMENT '1-Active, 0-Inactive',
  PRIMARY KEY (`scheduleId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `clientschedulemap`
--

DROP TABLE IF EXISTS `clientschedulemap`;
CREATE TABLE IF NOT EXISTS `clientschedulemap` (
  `clientScheduleMapId` varchar(150) NOT NULL COMMENT 'Client Schedule Map ID',
  `businessId` varchar(150) NOT NULL COMMENT 'Business ID',
  `scheduleId` varchar(150) NOT NULL COMMENT 'Schedule ID',
  `parentScheduleId` varchar(150) DEFAULT NULL COMMENT 'Parent Schedule ID',
  `masterParentScheduleId` varchar(150) DEFAULT NULL COMMENT 'Master Parent Schedule ID',
  `sessionCount` int(11) DEFAULT NULL COMMENT 'Addition of totalsession and present or absent session',
  `isActive` int(11) NOT NULL COMMENT 'Is Active',
  `subscriptionId` varchar(150) NOT NULL COMMENT 'Subscription ID',
  `clientId` varchar(150) NOT NULL COMMENT 'Client ID',
  `client_comments` varchar(300) DEFAULT NULL COMMENT 'Comments if any',
  `attendanceType` json NOT NULL COMMENT '1-Present, 2-Absent, 3-Excused, 4-Unmarked',
  `createDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Create Date Time and',
  `updateDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Update Date and Time',
  `status` int(11) NOT NULL COMMENT '1-Active, 0-Inactive',
  PRIMARY KEY (`clientScheduleMapId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `teamschedulemap`
--

DROP TABLE IF EXISTS `teamschedulemap`;
CREATE TABLE IF NOT EXISTS `teamschedulemap` (
  `teamScheduleMapId` varchar(150) NOT NULL COMMENT 'Team Schedule Map ID',
  `businessId` varchar(150) NOT NULL COMMENT 'Business ID',
  `scheduleId` varchar(150) DEFAULT NULL COMMENT 'Schedule ID',
  `teamId` varchar(150) NOT NULL COMMENT 'Team ID',
  `createDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Create Date Time and',
  `updateDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Update Date and Time',
  `status` int(11) NOT NULL COMMENT '1-Active, 0-Inactive',
  PRIMARY KEY (`teamScheduleMapId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Table structure for table `PricePack` // rename services table into PricePack
--

DROP TABLE IF EXISTS `pricepacks`;
CREATE TABLE IF NOT EXISTS `pricepacks` (
  `pricepackId` varchar(150) NOT NULL COMMENT 'Pricepack ID',
  `businessId` varchar(150) NOT NULL COMMENT 'Business ID',    
  `pricepackName` varchar(200) NOT NULL COMMENT 'Price Pack Name',
  `amount` float NOT NULL COMMENT 'Amount',
  `serviceDuration` int(11) NOT NULL COMMENT 'Number Of Sessions',
  `serviceDuration_number` int(11) DEFAULT NULL COMMENT 'Service Duration of sessions, days, months etc',
  `durationType` int(200) NOT NULL COMMENT 'Duration type 1=Session, 2=Day, 3=Week, 4=Month, 5=Year, 6= Item, 7= Consultancy(Hours) etc',
  `pricepackValidity` int(22) NULL COMMENT 'Price Pack Validity',
  `pricepackType` varchar(150) NOT NULL COMMENT '1-Single Service ,2-Multi Services, 3-Appointment.4-Events/Workshop,5-Membership,6-Trial Packs',
  `details` varchar(250) DEFAULT NULL COMMENT 'Service details',
  `type` int(2) NOT NULL COMMENT '1-Normal,2- Combo,3-Membership Service',
  `isExpired` int(2) NOT NULL COMMENT '1-Yes,0-No',
  `expiryDate` date DEFAULT NULL COMMENT 'expiry date of pricepack',
  `createdBy` varchar(250) DEFAULT NULL COMMENT 'created By',  
  `createDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Create Date Time',
  `updateDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Update Date and Time',
  `status` int(2) NOT NULL COMMENT '0-Inactive, 1-Active',
  PRIMARY KEY (`pricepackId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- -------------------------------------------------------
--
-- Table structure for table `servicepricepackmap`
--

DROP TABLE IF EXISTS `servicepricepackmap`;
CREATE TABLE IF NOT EXISTS `servicepricepackmap` (
  `servicepricepackmapId` varchar(150) NOT NULL COMMENT 'Service Pricepack Map ID',
  `pricepackId` varchar(150) NOT NULL COMMENT 'Pricepack ID',
  `serviceId` varchar(150) NOT NULL COMMENT 'Service ID',
  `businessId` varchar(150) NOT NULL COMMENT 'Business ID',
  `serviceDuration` int(11) NOT NULL COMMENT 'Individual Service Duration',
  `serviceDuration_number` int(11) DEFAULT NULL COMMENT 'Service Duration of sessions, days, months etc',
  `createDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Create Date Time',
  `updateDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Update Date and Time',
  `status` int(2) NOT NULL COMMENT '0-Inactive, 1-Active',
  PRIMARY KEY (`servicepricepackmapId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subscriptions`
--

DROP TABLE IF EXISTS `subscriptions`;
CREATE TABLE IF NOT EXISTS `subscriptions` (
   `subscriptionId` varchar(150) NOT NULL COMMENT 'Subscription ID',
  `parentSubscriptionId` varchar(150) DEFAULT NULL COMMENT 'Parent Subscription ID',
  `clientId` varchar(150) NOT NULL COMMENT 'Client ID',
  `businessId` varchar(150) NOT NULL COMMENT 'Business ID',
  `pricepacks` varchar(400) NOT NULL COMMENT 'Selected Pricepacks',
  `offerId` varchar(400) DEFAULT NULL COMMENT 'Selected Offers',  
  `installment_frequency` varchar(150) DEFAULT NULL COMMENT '1=Day,2=Week, 3=Month, 4=Quarter, 5= year, 6=Others',
  `frequency_number` varchar(150) DEFAULT NULL COMMENT 'Installment Intervals',
  `numberOfInstallments` int(11) DEFAULT NULL COMMENT 'Number of Installments',
  `subscriptionDateTime` datetime DEFAULT NULL COMMENT 'Subscription Date Time',
  `clientJoiningDate` datetime DEFAULT NULL COMMENT 'Client Joint Date Time',
  `totalSessionsAttended` int(11) DEFAULT NULL COMMENT 'Total Sessions Attended',
  `amountPaidSoFar` float DEFAULT NULL COMMENT 'Amount Paid So Far',
  `status_amountPaidSoFar` int(2) DEFAULT NULL COMMENT 'Status Amount Paid So Far',
  `renewl_status` int(11) NOT NULL COMMENT 'Renewl Status of A Subscription',
  `CreateDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Create Date and Time',
  `UpdateDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Update Date and Time',
  `Notification` varchar(20) DEFAULT NULL COMMENT '1-1day, 5-5day',
  `status` int(2) NOT NULL COMMENT '1-Active, 0-Inactive',
  PRIMARY KEY (`subscriptionId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
COMMIT;

-- --------------------------------------------------------

--
-- Table structure for table `clientpayments`
--


DROP TABLE IF EXISTS `clientpayments`;
CREATE TABLE `clientpayments` (
  `clientpaymentId` varchar(150) NOT NULL COMMENT 'Client Payment ID',
  `invoice_number` varchar(150) NOT NULL COMMENT 'Generate Invoice Number',
  `subscriptionsId` varchar(150) NOT NULL COMMENT 'Subscriptions ID',
  `businessId` varchar(150) NOT NULL COMMENT 'Business ID',
  `payment_due_date` date DEFAULT NULL COMMENT 'Payment Due Date',
  `payment_end_date` date DEFAULT NULL COMMENT 'Payment End Date',
  `curent_subcription_number` int(2) DEFAULT NULL COMMENT 'Current Subscription Number',
  `due_amount` float DEFAULT NULL COMMENT 'Due Amount',
  `payble_amount` float DEFAULT NULL COMMENT 'Total Amount Paid',
  `is_last_payment` int(11) DEFAULT '0' COMMENT 'Last Payment',
  `payment_type` int(11) DEFAULT NULL COMMENT '1=One Time Payment, 2=Installment',
  `paymentStatus` int(11) DEFAULT NULL COMMENT '1=Pending, 2=Complete, 3=Advance Paid, 4=Partial',
  `paymentReminder_status` int(11) DEFAULT NULL COMMENT 'Payment Reminder Status',
  `paymentReminder_date` date DEFAULT NULL COMMENT 'Payment Reminder date',
  `CreateDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Create Date and Time',
  `UpdateDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Update Date and Time',
  `status` int(2) NOT NULL COMMENT '1-Active, 0-Inactive',
  PRIMARY KEY (`clientpaymentId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
COMMIT;

-- --------------------------------------------------------
--
-- Table structure for table `clientpayments`
--

CREATE TABLE IF NOT EXISTS `clientexpense` (
`clientexpenseId` varchar(150) NOT NULL COMMENT 'Client Expense ID',
`businessId` varchar(150) NOT NULL COMMENT 'Business ID',
`name` varchar(200) NOT NULL COMMENT 'Name of Person',
`amount` float NOT NULL COMMENT 'Amount',
`expenseType` varchar(50) NOT NULL COMMENT 'Expense Type',
`centerId`  varchar(150) NOT NULL COMMENT 'Client Center ID',
`dateOfPayment` datetime NOT NULL COMMENT 'Date of Payment', 
`modeOfPayment` int(20) NOT NULL COMMENT 'Mode of Payment i.e 1=Cash,2=Cheque', 
`chequeNumber` varchar(50) DEFAULT NULL COMMENT 'Cheque Number',
`CreateDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Create Date and Time',
`UpdateDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Update Date and Time',
`status` int(2) NOT NULL COMMENT '1-Active, 0-Inactive',
PRIMARY KEY (`clientexpenseId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
COMMIT;

-- --------------------------------------------------------
--
-- Table structure for table `log`
--

CREATE TABLE IF NOT EXISTS `log` (
`logId` varchar(150) NOT NULL COMMENT 'Log ID',
`businessId` varchar(150) NOT NULL COMMENT 'Business ID',
`activity` json NOT NULL COMMENT 'Activity Log. {activityId,activityName,activityDate,activityTime,message,attendance,payment}',
`referenceTable` varchar(150) NOT NULL COMMENT 'Reference Table Name',
`CreateDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Create Date and Time',
`UpdateDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Update Date and Time',
`status` int(2) NOT NULL COMMENT '1-Active, 0-Inactive',
PRIMARY KEY (`logId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
COMMIT;

-- --------------------------------------------------------

--
-- Table structure for table `businessSetting`
--

DROP TABLE IF EXISTS `businessSettings`;
CREATE TABLE IF NOT EXISTS `businessSettings` (
  `businessSettingsId` varchar(150) NOT NULL COMMENT 'businessSetting ID',
  `businessId` varchar(150) NOT NULL COMMENT 'Bussiness ID',
  `businessSettingsData` TEXT DEFAULT NULL COMMENT 'businessSetting Data',
  `status` int(2) NOT NULL COMMENT '0-Inactive,1-Active',
  `createDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Create Date Time',
  `updateDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Update Date and Time',
  PRIMARY KEY (`businessSettingsId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Table structure for table `paymentTransaction`
--

DROP TABLE IF EXISTS `paymentTransactions`;
CREATE TABLE `paymentTransactions` (
  `paymentTransactionId` varchar(150) NOT NULL COMMENT 'Payment Transaction Id',
  `clientpaymentId` varchar(150) NOT NULL COMMENT 'Client Payment Id',
  `subscriptionsId` varchar(150) DEFAULT NULL COMMENT 'Subscription Id',
  `payment_user_id` varchar(150) DEFAULT NULL COMMENT 'User Id for record payment',
  `payment_date` date NOT NULL COMMENT 'Client Payment Date',
  `payment_mode` int(11) NOT NULL COMMENT '1=Cash, 2=Cheque',
  `paid_amount` float NOT NULL COMMENT 'Client Paid Amount',
  `cheque_number` varchar(150) DEFAULT NULL COMMENT 'Cheque Number',
  `payment_content` json DEFAULT NULL COMMENT 'Payment Report Content',
  `createDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Create Date Time',
  `updateDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Update Date and Time',
  `status` int(2) NOT NULL COMMENT '1-Active, 0-Inactive',
  PRIMARY KEY (`paymentTransactionId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Table structure for table `notes`
--

DROP TABLE IF EXISTS `notes`;
CREATE TABLE IF NOT EXISTS `notes` (
  `noteId` varchar(150) NOT NULL ,
  `businessId` varchar(150) NOT NULL ,
  `clientId` varchar(150) NOT NULL ,
  `heading` varchar(150) NOT NULL ,
  `description` longtext NOT NULL ,
  `reminder_date` date NOT NULL ,
  `reminder_time` varchar(200) NOT NULL ,
  `createDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Create Date Time',
  `updateDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Update Date and Time',
  `status` int(2) NOT NULL COMMENT '0-Closed, 1-Active',
  PRIMARY KEY (`noteId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
CREATE TABLE IF NOT EXISTS `messages` (
  `messageId` varchar(150) NOT NULL ,
  `businessId` varchar(150) NOT NULL ,
  `senderId` varchar(100) NOT NULL COMMENT 'User',
  `messageTypeId` varchar(150) NOT NULL ,
  `messageBody` text NOT NULL ,
  `message_date_time` datetime NOT NULL ,
  `createDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Create Date Time',
  `updateDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Update Date and Time',
  `status` int(2) NOT NULL COMMENT '0-Closed, 1-Active',
  PRIMARY KEY (`messageId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


-- --------------------------------------------------------

--
-- Table structure for table `messages_recipients`
--

DROP TABLE IF EXISTS `messageRecipients`;
CREATE TABLE IF NOT EXISTS `messageRecipients` (
  `message_recipients_id` varchar(150) NOT NULL ,
  `messageId` varchar(150) NOT NULL ,
  `businessId` varchar(150) NOT NULL ,
  `senderId` varchar(100) NOT NULL COMMENT 'User',
  `recipientId` varchar(100) NOT NULL COMMENT 'ClientId',
  PRIMARY KEY (`message_recipients_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `messageServices`
--

DROP TABLE IF EXISTS `messageServices`;
CREATE TABLE IF NOT EXISTS `messageServices` (
  `message_services_id` varchar(150) NOT NULL ,
  `messageId` varchar(150) NOT NULL ,
  `businessId` varchar(150) NOT NULL ,
  `serviceId` varchar(100) NOT NULL,
  PRIMARY KEY (`message_services_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `messageTypes`
--

DROP TABLE IF EXISTS `messageTypes`;
CREATE TABLE IF NOT EXISTS `messageTypes` (
  `messageTypeId` varchar(150) NOT NULL ,
  `messageTypeName` varchar(200) NOT NULL ,
  PRIMARY KEY (`messageTypeId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;