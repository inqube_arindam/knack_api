var multer = require('multer')
var gm = require('gm');

module.exports.image_upload = function(upload_dest) {
    if (upload_dest) {
        var storage = multer.diskStorage({
            destination: function(req, file, cb) {
                cb(null, upload_dest)
            },
            filename: function(req, file, cb) {
                cb(null, file.originalname + '-' + Date.now())
            }
        })
        var upload = module.exports.upload = multer({ storage: storage })
    }

}

module.exports.image_processing = function(file, resize = false, resize_dest = '', resize_width = '', resize_height = '',
    crop = '', crop_dest = '', crop_width = '', crop_height = '') {
    if (resize == true) {
        gm(file.path)
            .resize(resize_width, resize_height)
            .noProfile()
            .write(resize_dest + file.originalname + '-' + Date.now(), function(err) {
                if (!err) {
                    console.log('image resized')
                }
            })
    }
    if (crop == true) {
        gm(file.path)
            .crop(crop_width, crop_height)
            .write(crop_dest + file.originalname + '-' + Date.now(), function(err) {
                if (!err) console.log('image cropped');
            })
    }
}