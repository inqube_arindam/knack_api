import BaseModel from "../db/BaseModel";
import Sequelize from "sequelize";
export class LeadsSchema extends BaseModel {
  constructor(connection) {
    super(connection);
  }
  Leadinfo = () => {
    const leadinfo = this.connection.define(
      "leadinfo",
      {
        leadId: {
          type: Sequelize.UUIDV1,
          primaryKey: true,
          defaultValue: Sequelize.UUIDV1
        },
        leadName: { type: Sequelize.STRING, defaultValue: null },
        leadEmailId: { type: Sequelize.STRING, defaultValue: null },
        contactNumber: Sequelize.STRING,
        leadAddress: { type: Sequelize.STRING, defaultValue: null },
        pincode: { type:Sequelize.INTEGER, defaultValue: null },
        businessName: { type: Sequelize.STRING, defaultValue: null },
        businessExperience: { type: Sequelize.INTEGER, defaultValue: 0 },
        numberOfClients: { type: Sequelize.INTEGER, defaultValue: 0 },
        numberOfBranches: { type: Sequelize.INTEGER, defaultValue: 0 },
        numberOfTeamMembers: { type: Sequelize.INTEGER, defaultValue: 0 },
        alternateContactNumber: { type: Sequelize.STRING, defaultValue: null },
        alternateAddress: { type: Sequelize.STRING, defaultValue: null },
        businessWebsite: { type: Sequelize.STRING, defaultValue: null },
        centers: { type: Sequelize.INTEGER, defaultValue: 0 },
        managedBy: { type: Sequelize.STRING, defaultValue: null },
        createdBy: { type: Sequelize.STRING, defaultValue: null },
        referalDetails: { type: Sequelize.STRING, defaultValue: null },
        leadType: { type: Sequelize.INTEGER, defaultValue: 0 },
        leadStatus: { type: Sequelize.INTEGER, defaultValue: 0 },
        assignedTo: { type: Sequelize.STRING, defaultValue: null },
        priority: Sequelize.INTEGER,
        leadScore: { type: Sequelize.INTEGER, defaultValue: 0 },
        createDateTime: Sequelize.DATE,
        updateDateTime: Sequelize.DATE,
        isServeyComplete: { type: Sequelize.INTEGER, defaultValue: 0 },
        leadCategory: { type: Sequelize.INTEGER, defaultValue: 0 },
        gender: { type: Sequelize.INTEGER, defaultValue: 0 },
        isDelete:Sequelize.INTEGER
      },
      {
        engine: "MYISAM",
        charset: "latin1",
        timestamps: false,
        paranoid: true,
        underscored: true,
        freezeTableName: true,
        tableName: "leadinfo"
      }
    );
    //packages.belongsTo(packages, { foreignKey: "packageId" });
    leadinfo.removeAttribute("id");
    return leadinfo;
  };
}
export default new LeadsSchema();

/*import BaseModel from "../db/BaseModel";
import Sequelize from "sequelize";
export class leadSchema extends BaseModel {
  constructor(connection) {
    super(connection);
  }
  Leadinfo = () => {
    const leadinfo = this.connection.define(
      "leadinfo",
      {
        leadId: {
          type: Sequelize.UUIDV1,
          primaryKey: true,
          defaultValue: Sequelize.UUIDV1
        },
        leadName: { type: Sequelize.STRING, defaultValue: null },
        leadEmailId: { type: Sequelize.STRING, defaultValue: null },
        contactNumber: Sequelize.STRING,
        leadAddress: { type: Sequelize.STRING, defaultValue: null },
        pincode: { type:Sequelize.INTEGER, defaultValue: null },
        businessName: { type: Sequelize.STRING, defaultValue: null },
        businessExperience: { type: Sequelize.INTEGER, defaultValue: null },
        numberOfClients: { type: Sequelize.INTEGER, defaultValue: null },
        numberOfCenters: { type: Sequelize.INTEGER, defaultValue: null },
        numberOfTeamMembers: { type: Sequelize.INTEGER, defaultValue: null },
        alternateContactNumber: Sequelize.STRING,
        alternateAddress: { type: Sequelize.STRING, defaultValue: null },
        businessWebsite: { type: Sequelize.STRING, defaultValue: null },
        centers: Sequelize.STRING,
        leadType: Sequelize.INTEGER,
        leadStatus: Sequelize.INTEGER,
        assignedTo: { type: Sequelize.STRING, defaultValue: null },
        priority: Sequelize.INTEGER,
        leadScore: { type: Sequelize.INTEGER, defaultValue: 0 },
        createDateTime: Sequelize.DATE,
        updateDateTime: Sequelize.DATE,
        status: { type: Sequelize.INTEGER, defaultValue: 1 }
      },
      {
        engine: "MYISAM",
        charset: "latin1",
        timestamps: false,
        paranoid: true,
        underscored: true,
        freezeTableName: true,
        tableName: "leadinfo"
      }
    );
    //leadinfo.removeAttribute("id");
    return leadinfo;
  };
}
export default new leadSchema();*/
