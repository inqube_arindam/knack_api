import BaseModel from "../db/BaseModel";
import Sequelize from "sequelize";
export class ActivitySchema extends BaseModel {
  constructor(connection) {
    super(connection);
  }
  Activityinfo = () => {
    const activityinfo = this.connection.define(
      "activityinfo",
      {
        activityId: {
          type: Sequelize.UUIDV1,
          primaryKey: true,
          defaultValue: Sequelize.UUIDV1
        },
        leadId: { type: Sequelize.STRING, defaultValue: null },
        activityTypeLead: { type: Sequelize.INTEGER, defaultValue: 0 },
        activityStatus: { type: Sequelize.INTEGER, defaultValue: 0 },
        activityReason: Sequelize.STRING,
        otherReason: { type: Sequelize.STRING, defaultValue: null },
        activityComments: { type:Sequelize.INTEGER, defaultValue: null },
        activityDate: { type: Sequelize.STRING, defaultValue: null },
        activityTime: { type: Sequelize.INTEGER, defaultValue: 0 },
        activityFinalStatus: { type: Sequelize.INTEGER, defaultValue: 0 },
        activityStep: { type: Sequelize.INTEGER, defaultValue: 0 },
        activityAssignedTo: { type: Sequelize.STRING, defaultValue: null }, 
        isPending: { type: Sequelize.INTEGER, defaultValue: 1 },
        isCompleted: { type: Sequelize.INTEGER, defaultValue: 0 },
        activityCreatedDateTime: Sequelize.DATE,
        activityModifiedDateTime: Sequelize.DATE,
        activityLastDateTime: Sequelize.DATE,
        isDelete: { type: Sequelize.INTEGER, defaultValue: 0 }
      },
      {
        engine: "MYISAM",
        charset: "latin1",
        timestamps: false,
        paranoid: true,
        underscored: true,
        freezeTableName: true,
        tableName: "activityinfo"
      }
    );
    //packages.belongsTo(packages, { foreignKey: "packageId" });
    activityinfo.removeAttribute("id");
    return activityinfo;
  };
}
export default new ActivitySchema();

