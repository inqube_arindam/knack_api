import BaseModel from "../db/BaseModel";
import Sequelize from "sequelize";
export class NotesSchema extends BaseModel {
  constructor(connection) {
    super(connection);
  }
  Notesinfo = () => {
    const notesinfo = this.connection.define(
      "notesinfo",
      {
        noteId: {
          type: Sequelize.UUIDV1,
          primaryKey: true,
          defaultValue: Sequelize.UUIDV1
        },
        leadId: { type: Sequelize.STRING, defaultValue: null },
        comment: { type: Sequelize.STRING, defaultValue: null },
        created_by: { type: Sequelize.STRING, defaultValue: null },
        createdDateTime: Sequelize.DATE,
        isDelete: { type: Sequelize.INTEGER, defaultValue: 0 }
      },
      {
        engine: "MYISAM",
        charset: "latin1",
        timestamps: false,
        paranoid: true,
        underscored: true,
        freezeTableName: true,
        tableName: "notesinfo"
      }
    );
    //packages.belongsTo(packages, { foreignKey: "packageId" });
    notesinfo.removeAttribute("id");
    return notesinfo;
  };
}
export default new NotesSchema();
