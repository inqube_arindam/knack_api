import Sequelize from "sequelize";
module.exports = function(sequelize, DataTypes) {
  var teamschedulemap = sequelize.define(
    "teamschedulemap",
    {
      teamScheduleMapId: {
        type: Sequelize.UUIDV1,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV1
      },
      businessId: Sequelize.STRING,
      scheduleId: Sequelize.STRING,
      teamId: Sequelize.STRING,
      createDateTime: Sequelize.DATE,
      updateDateTime: Sequelize.DATE,
      status: {
        type: Sequelize.STRING,
        defaultValue: 1
      }
    },
    {
      timestamps: false,
      paranoid: true,
      underscored: true,
      freezeTableName: true,
      tableName: "teamschedulemap"
    }
  );
  teamschedulemap.removeAttribute("id");
  return teamschedulemap;
};
