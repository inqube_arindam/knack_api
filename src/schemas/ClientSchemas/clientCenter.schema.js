import Sequelize from "sequelize";
module.exports = function(sequelize, DataTypes) {
  var centers = sequelize.define(
    "centers",
    {
      centerId: {
        type: Sequelize.UUIDV1,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV1
      },
      businessId: Sequelize.STRING,
      centerName: Sequelize.STRING,
      area: Sequelize.STRING,
      landmark: {
        type: Sequelize.STRING,
        defaultValue: null
      },
      pin: Sequelize.INTEGER,
      city: Sequelize.STRING,
      contactNumber: Sequelize.STRING,
      updateFlag: {
        type: Sequelize.INTEGER,
        defaultValue: null
      },
      createdBy: {
        type: Sequelize.STRING,
        defaultValue: null
      },
      createDateTime: Sequelize.DATE,
      updateDateTime: Sequelize.DATE,
      status: {
        type: Sequelize.INTEGER,
        defaultValue: 1
      }
    },
    {
      timestamps: false,
      paranoid: true,
      underscored: true,
      freezeTableName: true,
      tableName: "centers"
    }
  );
  centers.removeAttribute("id");
  return centers;
};
