import Sequelize from "sequelize";
module.exports = function (sequelize, DataTypes) {
  var log = sequelize.define(
    "log",
    {
      logId: {
        type: Sequelize.UUIDV1,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV1
      },
      businessId: Sequelize.STRING,
      activity: Sequelize.JSON,
      referenceTable: Sequelize.STRING,
      createDateTime: Sequelize.DATE,
      updateDateTime: Sequelize.DATE,
      status: {
        type: Sequelize.STRING,
        defaultValue: 1
      }
    },
    {
      timestamps: false,
      paranoid: true,
      underscored: true,
      freezeTableName: true,
      tableName: "log"
    }
  );
  log.removeAttribute("id");
  return log;
};