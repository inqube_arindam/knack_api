import Sequelize from "sequelize";
module.exports = function(sequelize, DataTypes) {
  var offers = sequelize.define(
    "offers",
    {
      id: {
        type: Sequelize.UUIDV1,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV1
      },
      businessId: Sequelize.STRING,
      centerId: Sequelize.STRING,
      offerName: Sequelize.STRING,
      offerType: Sequelize.STRING,
      offerValue: Sequelize.INTEGER,
      createDateTime: Sequelize.DATE,
      updateDateTime: Sequelize.DATE,
      status: { type: Sequelize.INTEGER, defaultValue: 1 }
    },
    {
      engine: "MYISAM",
      charset: "latin1",
      timestamps: false,
      paranoid: true,
      underscored: true,
      freezeTableName: true,
      tableName: "offers"
    }
  );
  return offers;
};
