import Sequelize from "sequelize";
module.exports = function(sequelize, DataTypes) {
  var batches = sequelize.define(
    "batches",
    {
      batchId: {
        type: Sequelize.UUIDV1,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV1
      },
      businessId: Sequelize.STRING,
      centerId: {
        type: Sequelize.STRING,
        defaultValue: null
      },
      serviceId: Sequelize.STRING,
      batchName: Sequelize.STRING,
      batchSize: Sequelize.INTEGER,
      startTime: Sequelize.STRING,
      endTime: Sequelize.STRING,
      createDateTime: Sequelize.DATE,
      updateDateTime: Sequelize.DATE,
      status: {
        type: Sequelize.STRING,
        defaultValue: 1
      }
    },
    {
      timestamps: false,
      paranoid: true,
      underscored: true,
      freezeTableName: true,
      tableName: "batches"
    }
  );
  batches.removeAttribute("id");
  return batches;
};
