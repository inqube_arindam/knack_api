import Sequelize from "sequelize";
module.exports = function(sequelize, DataTypes) {
    var businessSettings = sequelize.define(
        "businessSettings", {
            businessSettingsId: {
                type: Sequelize.UUIDV1,
                primaryKey: true,
                defaultValue: Sequelize.UUIDV1
            },
            businessId: Sequelize.STRING,
            businessSettingsData: {
                type: Sequelize.JSON,
                defaultValue: null
            },
            createDateTime: Sequelize.DATE,
            updateDateTime: Sequelize.DATE,
            status: {
                type: Sequelize.STRING,
                defaultValue: 1
            }
        }, {
            timestamps: false,
            paranoid: true,
            underscored: true,
            freezeTableName: true,
            tableName: "businessSettings"
        }
    )
    businessSettings.removeAttribute("id");
    return businessSettings;
}