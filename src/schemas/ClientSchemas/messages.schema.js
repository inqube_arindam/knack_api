import Sequelize from "sequelize";
module.exports = function (sequelize, DataTypes) {
    var messages = sequelize.define('messages', {
        messageId: {
            type: Sequelize.UUIDV4,
            primaryKey: true,
            defaultValue: Sequelize.UUIDV4
        },
        businessId: Sequelize.STRING,
        senderId: Sequelize.STRING,
        messageTypeId: Sequelize.STRING,
        messageBody: Sequelize.STRING,
        message_date_time: Sequelize.TIME,
        createDateTime: Sequelize.DATE,
        updateDateTime: Sequelize.TIME,
        status: {
            type: Sequelize.INTEGER,
            defaultValue: 1
        }
    },
        {
            timestamps: false,
            paranoid: true,
            underscored: true,
            freezeTableName: true,
            tableName: "messages"
        }
    );
    messages.removeAttribute("id");
    return messages;
};