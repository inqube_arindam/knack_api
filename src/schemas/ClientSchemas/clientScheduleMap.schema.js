import Sequelize from "sequelize";
module.exports = function(sequelize, DataTypes) {
  var clientschedulemap = sequelize.define(
    "clientschedulemap",
    {
      clientScheduleMapId: {
        type: Sequelize.UUIDV1,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV1
      },
      businessId: Sequelize.STRING,
      scheduleId: Sequelize.STRING,
      parentScheduleId: {
        type :Sequelize.STRING,
        defaultValue: null
      },
      masterParentScheduleId: {
        type :Sequelize.STRING,
        defaultValue: null
      },
      isActive: {
        type: Sequelize.INTEGER,
        defaultValue: 1
      },
      subscriptionId: Sequelize.STRING,
      clientId: Sequelize.STRING,
      client_comments: { type: Sequelize.STRING, defaultValue: null },
      attendanceType: {
        type: Sequelize.JSON
      },
      sessionCount:{
        type: Sequelize.INTEGER,
        defaultValue: 0
      },
      createDateTime: Sequelize.DATE,
      updateDateTime: Sequelize.DATE,
      status: {
        type: Sequelize.STRING,
        defaultValue: 1
      }
    },
    {
      timestamps: false,
      paranoid: true,
      underscored: true,
      freezeTableName: true,
      tableName: "clientschedulemap"
    }
  );
  clientschedulemap.removeAttribute("id");
  return clientschedulemap;
};
