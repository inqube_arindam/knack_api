import Sequelize from "sequelize";
module.exports = function (sequelize, DataTypes) {
  var messageRecipients = sequelize.define(
    "messageRecipients",
    {
      message_recipients_id: {
        type: Sequelize.UUIDV4,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV4
      },
      messageId: Sequelize.STRING,
      businessId: Sequelize.STRING,
      senderId: Sequelize.STRING,
      recipientId: Sequelize.STRING,
    },
    {
      timestamps: false,
      paranoid: true,
      underscored: true,
      freezeTableName: true,
      tableName: "messageRecipients"
    }
  );
  messageRecipients.removeAttribute("id");
  return messageRecipients;
};