import Sequelize from "sequelize";
module.exports = function(sequelize, DataTypes) {
    var subscriptions = sequelize.define(
        "subscriptions", {
            subscriptionId: {
                type: Sequelize.UUIDV1,
                primaryKey: true,
                defaultValue: Sequelize.UUIDV1
            },
            parentSubscriptionId: {
                type: Sequelize.STRING,
                defaultValue: null
            },
            clientId: {
                type: Sequelize.STRING,
                references: "clients",
                referencesKey: "clientId"
            },
            businessId: Sequelize.STRING,
            pricepacks: {
                type: Sequelize.STRING,
                references: "pricepacks",
                referencesKey: "pricepackId"
            },
            offerId: {
                type: Sequelize.STRING,
                defaultValue: null
            },
            installment_frequency: {
                type: Sequelize.STRING,
                defaultValue: null
            },
            frequency_number: {
                type: Sequelize.STRING,
                defaultValue: null
            },
            numberOfInstallments: {
                type: Sequelize.INTEGER,
                defaultValue: null
            },
            subscriptionDateTime: {
                type: Sequelize.DATEONLY,
                defaultValue: null
            },
            clientJoiningDate: {
                type: Sequelize.DATEONLY,
                defaultValue: null
            },
            totalSessionsAttended: {
                type: Sequelize.INTEGER,
                defaultValue: null
            },
            amountPaidSoFar: {
                type: Sequelize.FLOAT,
                defaultValue: null
            },
            status_amountPaidSoFar: {
                type: Sequelize.INTEGER,
                defaultValue: null
            },
            renewl_status: {
                type: Sequelize.INTEGER,
                defaultValue: 1
            },
            Notification: {
                type: Sequelize.STRING,
                defaultValue: null
            },
            createDateTime: Sequelize.DATE,
            updateDateTime: Sequelize.DATE,
            status: {
                type: Sequelize.STRING,
                defaultValue: 1
            }
        }, {
            timestamps: false,
            paranoid: true,
            underscored: true,
            freezeTableName: true,
            tableName: "subscriptions"
        }
    );
    subscriptions.removeAttribute("id");
    return subscriptions;
};