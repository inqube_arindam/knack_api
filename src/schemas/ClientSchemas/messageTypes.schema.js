import Sequelize from "sequelize";
module.exports = function (sequelize, DataTypes) {
    var messageTypes = sequelize.define('messageTypes', {
        messageTypeId: {
            type: Sequelize.UUIDV4,
            primaryKey: true,
            defaultValue: Sequelize.UUIDV4
        },
        messageTypeName: Sequelize.STRING
    },
        {
            timestamps: false,
            paranoid: true,
            underscored: true,
            freezeTableName: true,
            tableName: "messageTypes"
        }
    );
    messageTypes.removeAttribute("id");
    return messageTypes;
};