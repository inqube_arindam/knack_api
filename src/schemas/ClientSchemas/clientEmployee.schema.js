import Sequelize from "sequelize";
module.exports = function (sequelize, DataTypes) {
  var employees = sequelize.define(
    "employees",
    {
      employeeId: {
        type: Sequelize.UUIDV1,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV1
      },
      businessId: Sequelize.STRING,
      roleId: {
        type: Sequelize.INTEGER,
        defaultValue: null
      },
      centerId: {
        type: Sequelize.STRING,
        defaultValue: null
      },
      employeeName: {
        type: Sequelize.STRING,
        defaultValue: null
      },
      contactNumber: Sequelize.STRING,
      emailId: {
        type: Sequelize.STRING,
        defaultValue: null
      },
      alternateNumber: {
        type: Sequelize.STRING,
        defaultValue: null
      },
      alternateEmail: {
        type: Sequelize.STRING,
        defaultValue: null
      },
      dateOfBirth: {
        type: Sequelize.DATEONLY,
        defaultValue: null
      },
      area: {
        type: Sequelize.STRING,
        defaultValue: null
      },
      pin: {
        type: Sequelize.STRING,
        defaultValue: null
      },
      city: {
        type: Sequelize.STRING,
        defaultValue: null
      },
      photoUrl: {
        type: Sequelize.STRING,
        defaultValue: null
      },
      createdBy: {
        type: Sequelize.STRING,
        defaultValue: null
      },
      createDateTime: Sequelize.DATE,
      updateDateTime: Sequelize.DATE,
      status: {
        type: Sequelize.STRING,
        defaultValue: 1
      }
    },
    {
      timestamps: false,
      paranoid: true,
      underscored: true,
      freezeTableName: true,
      tableName: "employees"
    },
    {
      indexes: [
        {
          unique: true,
          fields: ['emailId']
        }
      ]
    }
  );
  employees.removeAttribute("id");
  return employees;
};