import Sequelize from "sequelize";
module.exports = function(sequelize, DataTypes) {
    var clientpayments = sequelize.define(
        "clientpayments", {
            clientpaymentId: {
                type: Sequelize.UUIDV1,
                primaryKey: true,
                defaultValue: Sequelize.UUIDV1
            },
            invoice_number: Sequelize.STRING,
            subscriptionsId: Sequelize.STRING,
            businessId: Sequelize.STRING,
            payment_due_date: {
                type: Sequelize.DATE,
                defaultValue: null,
            },
            payment_end_date: {
                type: Sequelize.DATE,
                defaultValue: null,
            },
            curent_subcription_number: {
                type: Sequelize.INTEGER,
                defaultValue: null,
            },
            due_amount: {
                type: Sequelize.FLOAT,
                defaultValue: null
            },
            payble_amount: {
                type: Sequelize.FLOAT,
                defaultValue: null
            },
            is_last_payment: {
                type: Sequelize.INTEGER,
                defaultValue: null
            },
            payment_type: {
                type: Sequelize.INTEGER,
                defaultValue: null
            },
            paymentStatus: {
                type: Sequelize.INTEGER,
                defaultValue: 1
            },
            paymentReminder_status: {
                type: Sequelize.INTEGER,
                defaultValue: null
            },
            paymentReminder_date: {
                type: Sequelize.DATE,
            },
            createDateTime: Sequelize.DATE,
            updateDateTime: Sequelize.DATE,
            status: {
                type: Sequelize.STRING,
                defaultValue: 1
            }
        }, {
            timestamps: false,
            paranoid: true,
            underscored: true,
            freezeTableName: true,
            tableName: "clientpayments"
        }
    );
    clientpayments.removeAttribute("id");
    return clientpayments;
};