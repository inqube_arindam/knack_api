import Sequelize from "sequelize";
module.exports = function(sequelize, DataTypes) {
  var clientexpense = sequelize.define(
    "clientexpense",
    {
      clientexpenseId: {
        type: Sequelize.UUIDV1,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV1
      },
      businessId: Sequelize.STRING,
      name: Sequelize.STRING,
      amount: Sequelize.STRING,
      expenseType: Sequelize.STRING,
      centerId: Sequelize.STRING,
      dateOfPayment: Sequelize.DATE,
      modeOfPayment: Sequelize.INTEGER,
      chequeNumber: Sequelize.STRING,
      createDateTime: Sequelize.DATE,
      updateDateTime: Sequelize.DATE,
      status: {
        type: Sequelize.STRING,
        defaultValue: 1
      }
    },
    {
      timestamps: false,
      paranoid: true,
      underscored: true,
      freezeTableName: true,
      tableName: "clientexpense"
    }
  );
  clientexpense.removeAttribute("id");
  return clientexpense;
};
