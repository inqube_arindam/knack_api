import Sequelize from "sequelize";
module.exports = function (sequelize, DataTypes) {
  var pricepacks = sequelize.define(
    "pricepacks",
    {
      pricepackId: {
        type: Sequelize.UUIDV1,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV1
      },
      businessId: Sequelize.STRING,
      pricepackName: Sequelize.STRING,
      amount: Sequelize.STRING,
      serviceDuration: Sequelize.INTEGER,
      serviceDuration_number: {
        type: Sequelize.INTEGER,
        defaultValue: 0
      },
      durationType: Sequelize.INTEGER,
      pricepackValidity: {
        type: Sequelize.INTEGER,
        defaultValue: null
      },
      pricepackType: Sequelize.STRING,
      details: {
        type: Sequelize.STRING,
        defaultValue: null
      },
      type: {
        type: Sequelize.STRING,
        defaultValue: 1
      },
      isExpired: {
        type: Sequelize.INTEGER,
        defaultValue: 0
      },
      expiryDate: {
        type: Sequelize.DATE,
        defaultValue: null
      },
      createdBy: {
        type: Sequelize.STRING,
      },
      createDateTime: Sequelize.DATE,
      updateDateTime: Sequelize.DATE,
      status: {
        type: Sequelize.STRING,
        defaultValue: 1
      }
    },
    {
      timestamps: false,
      paranoid: true,
      underscored: true,
      freezeTableName: true,
      tableName: "pricepacks"
    }
  );
  pricepacks.removeAttribute("id");
  return pricepacks;
};
