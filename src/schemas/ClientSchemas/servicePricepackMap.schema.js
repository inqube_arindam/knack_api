import Sequelize from 'sequelize';
module.exports = function (sequelize, DataTypes) {
  var servicepricepackmap = sequelize.define (
    'servicepricepackmap',
    {
      servicepricepackmapId: {
        type: Sequelize.UUIDV1,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV1,
      },
      pricepackId: Sequelize.STRING,
      serviceId: Sequelize.STRING,
      businessId: Sequelize.STRING,
      serviceDuration: Sequelize.INTEGER,
      serviceDuration_number: {
        type: Sequelize.INTEGER,
        defaultValue: 0
      },
      createDateTime: Sequelize.DATE,
      updateDateTime: Sequelize.DATE,
      status: {
        type: Sequelize.STRING,
        defaultValue: 1,
      },
    },
    {
      timestamps: false,
      paranoid: true,
      underscored: true,
      freezeTableName: true,
      tableName: 'servicepricepackmap',
    }
  );
  servicepricepackmap.removeAttribute ('id');
  return servicepricepackmap;
};
