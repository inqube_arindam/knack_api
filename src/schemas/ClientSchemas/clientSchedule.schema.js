import Sequelize from "sequelize";
module.exports = function(sequelize, DataTypes) {
  var schedule = sequelize.define(
    "schedule",
    {
      scheduleId: {
        type: Sequelize.UUIDV1,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV1
      },
      businessId: Sequelize.STRING,
      centerId: {
        type: Sequelize.STRING,
        defaultValue: null
      },
      rescheduleId: {
        type: Sequelize.STRING,
        defaultValue: null
      },
      serviceId: Sequelize.STRING,
      scheduleName: {
        type: Sequelize.STRING,
        defaultValue: null
      },
      scheduleDate: Sequelize.DATEONLY,
      startTime: Sequelize.TIME,
      endTime: Sequelize.TIME,
      scheduleType: {
        type: Sequelize.INTEGER,
        defaultValue: 1
      },
      parent_scheduleId: {
        type: Sequelize.STRING,
        defaultValue: null
      },
      schedule_status: {
        type: Sequelize.INTEGER,
        defaultValue: 1
      },
      scheduleTime: {
        type: Sequelize.JSON,
        defaultValue: null
      },
      numberOfClients: {
        type: Sequelize.INTEGER,
        defaultValue: null
      },
      batchId: {
        type: Sequelize.STRING,
        defaultValue: null
      },
      createdBy: {
        type: Sequelize.STRING,
        defaultValue: null
      },
      comments: { type: Sequelize.STRING, defaultValue: null },
      is_repeat: Sequelize.INTEGER,
      repeatation_type: Sequelize.INTEGER,
      repeat_number: Sequelize.INTEGER,
      repeat_days: Sequelize.STRING,
      monthly_type:{ type: Sequelize.STRING, defaultValue: null },
      repeat_ends: Sequelize.INTEGER,
      repeat_ends_date: Sequelize.DATE,
      createDateTime: Sequelize.DATE,
      updateDateTime: Sequelize.DATE,
      status: {
        type: Sequelize.STRING,
        defaultValue: 1
      }
    },
    {
      timestamps: false,
      paranoid: true,
      underscored: true,
      freezeTableName: true,
      tableName: "schedule"
    }
  );
  schedule.removeAttribute("id");
  return schedule;
};
