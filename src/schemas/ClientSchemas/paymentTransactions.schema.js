import Sequelize from "sequelize";
module.exports = function(sequelize, DataTypes) {
    var paymentTransactions = sequelize.define(
        "paymentTransactions", {
            paymentTransactionId: {
                type: Sequelize.UUIDV1,
                primaryKey: true,
                defaultValue: Sequelize.UUIDV1
            },
            clientpaymentId: Sequelize.STRING,
            subscriptionsId: {
                type: Sequelize.STRING,
                defaultValue: null
            },
            payment_user_id: {
                type: Sequelize.STRING,
                defaultValue: null
            },
            payment_date: Sequelize.DATE,
            payment_mode: Sequelize.INTEGER,
            paid_amount: Sequelize.FLOAT,
            cheque_number: {
                type: Sequelize.STRING,
                defaultValue: null
            },
            payment_content: {
                type: Sequelize.JSON,
                defaultValue: null
            },
            createDateTime: Sequelize.DATE,
            updateDateTime: Sequelize.DATE,
            status: {
                type: Sequelize.STRING,
                defaultValue: 1
            }
        }, {
            timestamps: false,
            paranoid: true,
            underscored: true,
            freezeTableName: true,
            tableName: "paymentTransactions"
        }
    );
    paymentTransactions.removeAttribute("id");
    return paymentTransactions;
};