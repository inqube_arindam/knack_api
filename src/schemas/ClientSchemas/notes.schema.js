import Sequelize from "sequelize";
module.exports = function(sequelize, DataTypes) {
  var notes = sequelize.define(
    "notes",
    {
      noteId: {
        type: Sequelize.UUIDV4,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV4
      },
      businessId: Sequelize.STRING,
      clientId: Sequelize.STRING,
      heading: Sequelize.STRING,
      description: Sequelize.STRING,
      reminder_date: Sequelize.DATE,
      reminder_time: Sequelize.TIME,
      createDateTime: Sequelize.DATE,
      updateDateTime: Sequelize.TIME,
      status: {
        type: Sequelize.INTEGER,
        defaultValue: 1
      }
    },
    {
      timestamps: false,
      paranoid: true,
      underscored: true,
      freezeTableName: true,
      tableName: "notes"
    }
  );
  notes.removeAttribute("id");
  return notes;
};
