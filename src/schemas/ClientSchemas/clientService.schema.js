import Sequelize from "sequelize";
module.exports = function(sequelize, DataTypes) {
  var services = sequelize.define(
    "services",
    {
      serviceId: {
        type: Sequelize.UUIDV1,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV1
      },
      businessId: Sequelize.STRING,
      serviceName: Sequelize.STRING,      
      serviceDetails: {
        type: Sequelize.STRING,
        defaultValue: null
      },
      createdBy: {
        type: Sequelize.STRING,
      },
      createDateTime: Sequelize.DATE,
      updateDateTime: Sequelize.DATE,
      status: {
        type: Sequelize.STRING,
        defaultValue: 1
      }
    },
    {
      timestamps: false,
      paranoid: true,
      underscored: true,
      freezeTableName: true,
      tableName: "services"
    }
  );
  services.removeAttribute("id");
  return services;
};
