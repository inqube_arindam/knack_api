import Sequelize from "sequelize";
module.exports = function (sequelize, DataTypes) {
    var messageServices = sequelize.define(
        "messageServices",
        {
            message_services_id: {
                type: Sequelize.UUIDV4,
                primaryKey: true,
                defaultValue: Sequelize.UUIDV4
            },
            messageId: Sequelize.STRING,
            businessId: Sequelize.STRING,
            serviceId: Sequelize.STRING,
        },
        {
            timestamps: false,
            paranoid: true,
            underscored: true,
            freezeTableName: true,
            tableName: "messageServices"
        }
    );
    messageServices.removeAttribute("id");
    return messageServices;
};