import Sequelize from "sequelize";
module.exports = function(sequelize, DataTypes) {
    var role = sequelize.define(
      "role",
      {
        roleId: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          primaryKey: true,
          // defaultValue: Sequelize.INTEGER
        },
        roleName: Sequelize.STRING,
        roleDescription: Sequelize.STRING,
        modulePermissions: {
          type: Sequelize.JSON,
          defaultValue: null
        },
        createDateTime: Sequelize.DATE,
        updateDateTime: Sequelize.DATE,
        status: {
          type: Sequelize.STRING,
          defaultValue: 1
        }
      },
      {
        timestamps: false,
        paranoid: true,
        underscored: true,
        freezeTableName: true,
        tableName: "role"
      }
    );
    role.removeAttribute("roleId");
    return role;
};
