import BaseModel from "../db/BaseModel";
import Sequelize from "sequelize";
export class BusinessSchema extends BaseModel {
  constructor(connection) {
    super(connection);
  }
  Business = () => {
    const businessinfo = this.connection.define(
      "businessinfo",
      {
        businessId: {
          type: Sequelize.UUIDV1,
          primaryKey: true,
          defaultValue: Sequelize.UUIDV1
        },
        businessName: { type: Sequelize.STRING, defaultValue: null },
        businessType: { type: Sequelize.STRING, defaultValue: null },
        numberOfClients: { type: Sequelize.INTEGER, defaultValue: null },
        numberOfCenters: { type: Sequelize.INTEGER, defaultValue: null },
        numberOfTeamMembers: { type: Sequelize.INTEGER, defaultValue: null },
        contactNumber: Sequelize.STRING,
        emailId: Sequelize.STRING,
        area: { type: Sequelize.STRING, defaultValue: null },
        pin: { type: Sequelize.INTEGER, defaultValue: null },
        city: { type: Sequelize.STRING, defaultValue: null },
        landmark: { type: Sequelize.STRING, defaultValue: null },
        photoUrl: { type: Sequelize.STRING, defaultValue: null },
        business_days: { type: Sequelize.STRING, defaultValue: null },
        createDateTime: Sequelize.DATE,
        updateDateTime: Sequelize.DATE,
        status: { type: Sequelize.INTEGER, defaultValue: 1 }
      },
      {
        engine: "MYISAM",
        charset: "latin1",
        timestamps: false,
        paranoid: true,
        underscored: true,
        freezeTableName: true,
        tableName: "businessinfo"
      }
    );
    businessinfo.removeAttribute("id");
    return businessinfo;
  };
}
export default new BusinessSchema();
