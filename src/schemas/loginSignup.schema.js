import BaseModel from "../db/BaseModel";
import Sequelize from "sequelize";
export class LoginSignupSchema extends BaseModel {
  constructor(connection) {
    super(connection);
  }
  LoginSignUp = () => {
    const logininfo = this.connection.define(
      "logininfo",
      {
        userId: {
          type: Sequelize.UUIDV1,
          primaryKey: true,
          defaultValue: Sequelize.UUIDV1
        },
        businessId: {
          type: Sequelize.UUIDV1,
          defaultValue: Sequelize.UUIDV1
        },
        centerId: Sequelize.STRING,
        name: Sequelize.STRING,
        businessName: Sequelize.STRING,
        emailId: Sequelize.STRING,
        contactNumber: Sequelize.STRING,
        password: Sequelize.STRING,
        userType: Sequelize.STRING,
        createdBy: { type: Sequelize.STRING, defaultValue: null },
        modulePermissions: { type: Sequelize.STRING, defaultValue: null },
        dateOfBirth: { type: Sequelize.DATEONLY, defaultValue: null },
        photoUrl: { type: Sequelize.STRING, defaultValue: null },
        lastLogin: Sequelize.DATE,
        verificationCode: { type: Sequelize.STRING, defaultValue: null },
        emailverificationCode: { type: Sequelize.STRING, defaultValue: null },
        relatedDatabase: { type: Sequelize.STRING, defaultValue: null },
        relatedIP: { type: Sequelize.STRING, defaultValue: null },
        FcmUserToken: { type: Sequelize.STRING, defaultValue: null },
        IMEINumber: { type: Sequelize.STRING, defaultValue: null },
        status: Sequelize.INTEGER,
        isSuperAdmin: { type: Sequelize.INTEGER, defaultValue: 0 },
        role: { type: Sequelize.INTEGER, defaultValue: 0 },
        isSuperAdminTeam: { type: Sequelize.INTEGER, defaultValue: 0 }
      },
      {
        engine: "MYISAM", // default: 'InnoDB'
        charset: "latin1", // default: null
        timestamps: false,
        paranoid: true,
        underscored: true,
        freezeTableName: true,
        tableName: "logininfo"
      }
    );
    logininfo.removeAttribute("id");
    return logininfo;
  };
}
export default new LoginSignupSchema();
