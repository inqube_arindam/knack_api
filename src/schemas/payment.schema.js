import BaseModel from "../db/BaseModel";
import Sequelize from "sequelize";
export class PaymentSchema extends BaseModel {
  constructor(connection) {
    super(connection);
  }
  Payment = () => {
    const payment = this.connection.define(
      "payment",
      {
        paymentId: {
          type: Sequelize.UUIDV1,
          primaryKey: true,
          defaultValue: Sequelize.UUIDV1
          //   scopes: ["create", "private"],
          //   scopes: ["self"]
          //   access: { read: false }
          //   scopes: ["private"],
          //   access: ["create", "edit"]
        },
        packageId: Sequelize.STRING,
        userId: Sequelize.STRING,
        businessId: Sequelize.STRING,
        invoiceId: Sequelize.STRING,
        paymentMode: Sequelize.STRING,
        amount: Sequelize.STRING,
        details: Sequelize.STRING,
        centerNumber: Sequelize.INTEGER,
        paymentStatus: { type: Sequelize.INTEGER, defaultValue: 1 },
        subscriptionDate: { type: Sequelize.DATE },
        isRenewl: { type: Sequelize.INTEGER },
        status: { type: Sequelize.INTEGER, defaultValue: 1 },
        amountDate: { type: Sequelize.DATE },
        createDateTime: Sequelize.DATE,
        updateDateTime: Sequelize.DATE
      },
      {
        engine: "MYISAM", // default: 'InnoDB'
        charset: "latin1", // default: null
        timestamps: false,
        paranoid: true,
        underscored: true,
        freezeTableName: true,
        // scopes: ["self"],
        // hidden: ["paymentId", "packageId"],
        // privateColumns: ["userId", "details"],
        tableName: "payment"
        // instanceMethods: {
        //   toJSON: function() {
        //     var privateAttributes = ["businessId"];
        //     delete this.dataValues.businessId;
        //     console.log(this.dataValues);
        //     return _.omit(this.dataValues, privateAttributes);
        //   }
        // }
      }
    );
    payment.hasMany(payment, { foreignKey: "packageId" });
    payment.removeAttribute("id");
    return payment;
  };
}

export default new PaymentSchema();
