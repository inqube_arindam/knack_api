import BaseModel from "../db/BaseModel";
import Sequelize from "sequelize";
export class RoleSchema extends BaseModel {
  constructor(connection) {
    super(connection);
  }
  role = () => {
    const role = this.connection.define(
      "role",
      {
        roleId: {
          type: Sequelize.INTEGER,
          autoIncrement: true,
          primaryKey: true,
          // defaultValue: Sequelize.INTEGER
        },
        roleName: Sequelize.STRING,
        roleDescription: Sequelize.STRING,
        modulePermissions: {
          type: Sequelize.JSON,
          defaultValue: null
        },
        createDateTime: Sequelize.DATE,
        updateDateTime: Sequelize.DATE,
        status: {
          type: Sequelize.STRING,
          defaultValue: 1
        }
      },
      {
        timestamps: false,
        paranoid: true,
        underscored: true,
        freezeTableName: true,
        tableName: "role"
      }
    );
    role.belongsTo(role, { foreignKey: "roleId" });
    role.removeAttribute("id");
    return role;
  };
}
export default new RoleSchema();