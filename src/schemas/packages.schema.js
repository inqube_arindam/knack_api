import BaseModel from "../db/BaseModel";
import Sequelize from "sequelize";
export class PackagesSchema extends BaseModel {
  constructor(connection) {
    super(connection);
  }
  Packages = () => {
    const packages = this.connection.define(
      "packages",
      {
        packageId: {
          type: Sequelize.UUIDV1,
          primaryKey: true,
          defaultValue: Sequelize.UUIDV1
        },
        name: Sequelize.STRING,
        details: Sequelize.STRING,
        duration: Sequelize.STRING,
        amount: Sequelize.INTEGER,
        referralDiscount: {
          type: Sequelize.INTEGER,
          defaultValue: 0
        },
        isGstApplicable: {
          type: Sequelize.INTEGER,
          defaultValue: 0
        },
        cgst: {
          type: Sequelize.INTEGER,
          defaultValue: 0
        },
        sgst: {
          type: Sequelize.INTEGER,
          defaultValue: 0
        },
        totalAmount: {
          type: Sequelize.INTEGER,
          defaultValue: 0
        },
        numberOfCenters: Sequelize.INTEGER,
        numberOfTeamMembers: Sequelize.INTEGER,
        createDateTime: Sequelize.DATE,
        updateDateTime: Sequelize.DATE,
        status: { type: Sequelize.INTEGER, defaultValue: 1 },
        isDelete: {
          type: Sequelize.INTEGER,
          defaultValue: 0
        }
      },
      {
        engine: "MYISAM", // default: 'InnoDB'
        charset: "latin1", // default: null
        timestamps: false,
        paranoid: true,
        underscored: true,
        freezeTableName: true,
        tableName: "packages"
      }
    );
    packages.belongsTo(packages, { foreignKey: "packageId" });
    packages.removeAttribute("id");
    return packages;
  };
}
export default new PackagesSchema();