import BaseModel from "../db/BaseModel";
import Sequelize from "sequelize";
export class LoginMappingSchema extends BaseModel {
  constructor(connection) {
    super(connection);
  }
  LoginMapping = () => {
    const loginmapping = this.connection.define(
      "loginmapping",
      {
        id: {
          type: Sequelize.UUIDV1,
          primaryKey: true,
          defaultValue: Sequelize.UUIDV1
        },
        userId: Sequelize.STRING,
        businessId: Sequelize.STRING,
        centerId: Sequelize.STRING,
        lastLogin: Sequelize.DATE,
        FcmUserToken: Sequelize.STRING,
        IMEINumber: Sequelize.STRING
      },
      {
        engine: "MYISAM",
        charset: "latin1",
        timestamps: false,
        paranoid: true,
        underscored: true,
        freezeTableName: true,
        tableName: "loginmapping"
      }
    );
    return loginmapping;
  };
}
export default new LoginMappingSchema();
