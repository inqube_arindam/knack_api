exports.makeEmailTemplate = function(data) {
    try {


        var setRow = "";
        var color = "";
        var statusText = "";
        if (data.getSubcriptionPaymentFormat) {
            if (data.getSubcriptionPaymentFormat.length > 0) {
                for (var i = 0; i < data.getSubcriptionPaymentFormat.length; i++) {
                    let statusText = "Pending";
                    let color = "color : red";
                    let cal_Amount = Number(data.getSubcriptionPaymentFormat[i].due_amount) - Number(data.getSubcriptionPaymentFormat[i].payble_amount);
                    setRow += ` <tr>
                                    <td style=" border: 2px solid  #757575; border-collapse: collapse;color:#1f497d;padding:15px 0;box-shadow: 0px 3px #b1b1b1; font-size: 14px; ` + color + `">` + data.getSubcriptionPaymentFormat[i].payment_due_date + `</td>
                                    <td style=" border: 2px solid  #757575; border-collapse: collapse;color:#1f497d;padding:15px 0;box-shadow: 0px 3px #b1b1b1; font-size: 14px; ` + color + `"> ` + cal_Amount + `</td>
                                    <td style=" border: 2px solid  #757575; border-collapse: collapse;color:#1f497d;padding:15px 0;box-shadow: 0px 3px #b1b1b1; font-size: 14px; ` + color + `">` + statusText + `</td>
                    </tr>`;
                }
            }
            if (data.allPaymentListData.length > 0) {
                for (var n = 0; n < data.allPaymentListData.length; n++) {
                    let statusText = "Paid";
                    let color = "color : green";
                    let cal_Amount = Number(data.allPaymentListData[n].paid_amount);
                    setRow += ` <tr>
                                    <td style=" border: 2px solid  #757575; border-collapse: collapse;color:#1f497d;padding:15px 0;box-shadow: 0px 3px #b1b1b1; font-size: 14px; ` + color + `">` + data.allPaymentListData[n].payment_date + `</td>
                                    <td style=" border: 2px solid  #757575; border-collapse: collapse;color:#1f497d;padding:15px 0;box-shadow: 0px 3px #b1b1b1; font-size: 14px; ` + color + `"> ` + cal_Amount + `</td>
                                    <td style=" border: 2px solid  #757575; border-collapse: collapse;color:#1f497d;padding:15px 0;box-shadow: 0px 3px #b1b1b1; font-size: 14px; ` + color + `">` + statusText + `</td>
                    </tr>`;
                }
            }
        }


        var content = `<!DOCTYPE html>
        <html lang="en">
        
        <head>
            <title>Knack</title>
        </head>
        
        <body>
            <table style="padding:15px; background: #fff; width: 480px; margin: 0 auto; -webkit-box-shadow: 0 0 7px #d6d6d6;margin-top:4%; box-shadow: 0 0 7px #d6d6d6;font-family: sans-serif; font-size: 14px;">
                <tr>
                    <td>
                        <table style="width:480px; padding-bottom: 10px;">
                            <tr>
                                <td style="text-align:center">
                                    <span style="text-align:center; width:90px;height:90px;background:#4bacc6; color:#fff; font-size:15px;display:block; border-radius:50%;line-height: 6;box-shadow: 0px 3px #b1b1b1; line-height: 6.3;">Knack </span>
                                </td>
                                <td>
                                    <table style="padding:20px; text-align:center;text-align:center;background:#10243e; color:#fff;  width: 100%; box-shadow: 0px 3px #b1b1b1;">
                                        <tr>
                                            <td style="opacity: 0.9; font-size: 14px;">
                                            ` + data["businessinfo"][0]['businessName'] + ` <br> ` + data["businessinfo"][0]['contactNumber'] + ` | ` + data["businessinfo"][0]['emailId'] + `
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
        
                        <table style="width: 480px;padding-bottom: 10px;">
                            <tr>
                                <td style="background:#1f497d;height:4px;box-shadow: 0px 3px #b1b1b1;"></td>
                            </tr>
                        </table>
                        <table style="width:480px;padding-bottom: 10px;">
                            <tr>
                                <td style="color:#1f497d;">
                                    <b>` + data["client.clientName"] + `</b>
                                    <p style="margin:2px; color:#1f497d; font-size:14px">` + data["client.contactNumber"] + `</p>`;

        if (data["serviceNameList"]) {
            content += `<p style="margin:2px; color:#1f497d; font-size:14px">` + data["sessionInfo"][0]['centerName'] + `</p>`;
        }
        content += `
                                    
                                </td>
                                <td style="color:#1f497d;">
                                    <p style="margin:2px; color:#1f497d; font-size:14px">` + data["pricepack.pricepackName"] + `</p>
                                    <p style="margin:2px; color:#1f497d; font-size:14px">` + data["serviceNameList"][0]["service.serviceNameList"] + `</p>`;
        if (data["serviceNameList"]) {
            content += `<p style="margin:2px; color:#1f497d; font-size:14px">` + data["sessionInfo"][0]['message'] + `</p>`;
        }
        content += `
                                </td>
                            </tr>
                        </table>
                        <table style="width: 480px;">
                            <tr>
                                <td style="background:#1f497d;height:4px;box-shadow: 0px 3px #b1b1b1;"></td>
                            </tr>
                        </table>
                        <table style="width:480px;background:#4bacc6;margin-top: 8px;">
                            <tr>
                                <td style="background:#4bacc6; padding:10px 0; text-align:center; color:#fff;font-size: 14px; font-weight:normal;box-shadow: 0px 3px #b1b1b1;font-size: 14px;">
                                    PAYMENT REPORT
                                </td>
                            </tr>
                        </table>
                        <table style="width:480px;border: 2px solid #757575; border-collapse: collapse; text-align:center">
                            <tr>
                                <th style=" border: 2px solid  #757575; border-collapse: collapse; color:#4bacc6;padding:15px 0;box-shadow: 0px 3px #b1b1b1;font-size: 15px;"> DATE</th>
                                <th style=" border: 2px solid  #757575; border-collapse: collapse;color:#4bacc6;padding:15px 0;box-shadow: 0px 3px #b1b1b1;font-size: 15px;">Amount (INR)</th>
                                <th style=" border: 2px solid  #757575; border-collapse: collapse; color:#4bacc6;padding:15px 0;box-shadow: 0px 3px #b1b1b1;font-size: 15px;">Status</th>
                            </tr>
                            ` + setRow + `
        
                        </table>
                        <table style="width: 480px;background:#1f497d; height:3px;margin-top: 10px;box-shadow: 0px 3px #b1b1b1;">
                            <tr>
                                <td style="background:#1f497d;"></td>
                            </tr>
                        </table>
                        <table style="width: 480px; padding:16px 0 0">
                            <tr>
                                <td style="text-align:center">
                                    <img src="images/Knack-Logo.svg" alt="knack">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </body>
        
        </html>`;


        return content;
    } catch (err) {
        throw err;
    }
}