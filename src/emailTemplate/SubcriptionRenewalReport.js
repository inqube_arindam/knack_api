function formatDate(date) {
    try {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [day, month, year].join('-');
    } catch (err) {
        throw err;
    }
}

exports.makeEmailTemplate = function(data) {
    try {

        data[0]["businessinfo.businessName_initials"] = (data[0]["businessinfo.businessName_initials"]) ? data[0]["businessinfo.businessName_initials"] : '';
        data[0]["businessinfo.businessName"] = (data[0]["businessinfo.businessName"]) ? data[0]["businessinfo.businessName"] : '';
        data[0].subscriptionDateTime = formatDate(data[0].subscriptionDateTime);
        data[0].getPaymentCalculation.cgstPercentageAmount = (parseInt(data[0].getPaymentCalculation.taxableAmount) / 100) * data[0].getPaymentCalculation.cgstPercentage;
        data[0].getPaymentCalculation.sgstPercentageAmount = (parseInt(data[0].getPaymentCalculation.taxableAmount) / 100) * data[0].getPaymentCalculation.sgstPercentage;

        let businessSettingsData = data[0]["businessinfo.businessSetting.businessSettingsData"];
        let businessSettingsDataArrayData = JSON.parse(businessSettingsData);
        let terms = businessSettingsDataArrayData[5].termsAndConditionsDetails.content;


        var content =
            `<html>
			<head>
			  <title>Knack</title>
			</head>
			<body>
			  <table style="padding:15px; background: #fff; width: 400px; margin: 0 auto; -webkit-box-shadow: 0 0 7px #d6d6d6;margin-top:4%; box-shadow: 0 0 7px #d6d6d6;">
				<tr>
				  <td>
					<!-- logo table top -->
					<table style="width:100%; padding-bottom: 10px;text-align:center">
					  <tbody>
						<tr>
						  <td>
							<span style="margin:5px 0;font-size:42px;padding: 0px 8px;border:1px solid #212e43;">` + data[0]["businessinfo.businessName_initials"] + `</span>
							<label style="display: inline-block;text-align:left;margin-left:15px;">
							  <span style="margin:5px 0;font-size:25px;"> ` + data[0]["businessinfo.businessName"] + ` </span>
							  <br>
														  Dated:` + data[0].subscriptionDateTime + `
													  
							  </label>
							</td>
						  </tr>
						</tbody>
					  </table>
					  <table style="width:100%; padding-bottom: 10px;">
						<tbody>
						  <tr>
							<td style="">
							  <p style="margin:2px 0; font-size:16px;">` + data[0]["client.clientName"] + `</p>
							  <p style="margin:2px 0; color:#666">` + data[0]["client.area"] + `
								<br>` + data[0]["client.city"] + ` ` + data[0]["client.pin"] + `
								</p>
							  </td>
							  <td style="text-align:right">
								<p style="margin:2px 0; color:#666; font-size:14px;">Receipt No</p>
								<p style="margin:2px 0; color: #212e43; font-size:16px;">123456</p>
							  </td>
							</tr>
						  </tbody>
						</table>
						<hr style=" border-top: 1px dotted #9199a1;  box-shadow: none; background: #fff;">
						  <table style="width: 400px;">
							<tr>
							  <td style="font-size:14px">
							  ` + data[0]["pricepack.pricepackName"] + `
													  </td>
							  <td style="text-align:right">
														   &#x20B9 ` + data[0]["pricepack.amount"] + `
													  </td>
							</tr>
							<tr>
							  <td style="font-size:14px">
														  Discounts
													  </td>
							  <td style="text-align:right">
							  ` + data[0].getPaymentCalculation.discountAmount + `
													  </td>
							</tr>
						  </table>
						  <hr style="border-top: 1px dotted #9199a1;  box-shadow: none; background: #fff;">
							<table style="text-align:right; width: 400px; padding:15px;">
							  <tr>
								<td  collspan="2" style="color: #9199a1; font-size:13px; padding-bottom: 10px;">
															   Total
														  </td>
								<td  style="text-align:right; padding-bottom: 10px;">
															   &#x20B9 ` + data[0].getPaymentCalculation.taxableAmount + `
														  </td>
							  </tr>
							  <tr>
								<td collspan="2" style="color: #9199a1; font-size:13px; padding-bottom: 10px;">
															  CGST (` + data[0].getPaymentCalculation.cgstPercentage + `%)
														  </td>
								<td style="text-align:right; padding-bottom: 10px;">
															   &#x20B9 ` + data[0].getPaymentCalculation.cgstPercentageAmount + `
														  </td>
							  </tr>
							  <tr>
								<td collspan="2" style="color: #9199a1; font-size:13px; padding-bottom: 10px;">
																	  SCGST (` + data[0].getPaymentCalculation.sgstPercentage + `%)
															  </td>
								<td style="text-align:right; padding-bottom: 3px;">
																  &#x20B9 ` + data[0].getPaymentCalculation.sgstPercentageAmount + `
															  </td>
							  </tr>
							</table>
							<table style="width: 400px;">
							  <tr>
								<td ></td>
								<td  collspan="2" style="">
								  <hr style="    border-top: 1px dotted #9199a1;  box-shadow: none; background: #fff;">
								  </td>
								</tr>
							  </table>
							  <table style="width: 400px; padding:15px;">
								<tr>
								  <td style="text-align:right">
									<table style="width:100%">
									  <tr>
										<td  collspan="3" style="text-align:right">
																				  Grand Total
																			  </td>
									  </tr>
									  <tr>
										<td  collspan="3" style="text-align:right; font-size:40px;">
																			   &#x20B9 ` + data[0].getPaymentCalculation.payableAmount + `
																		  </td>
									  </table>
									</td>
								  </tr>
								</table>
								<hr style="border-top: 1px dotted #9199a1;  box-shadow: none; background: #fff;">
								  <table style="width: 400px;">
									<tr>
									  <td  style="">
										<h4>Terms and Conditions</h4>
										<p style="color: #666;">` + terms + `</p>
									  </td>
									</tr>
								  </table>
								</td>
							  </tr>
							</table>
						  </body>
						</html>`;

        return content;
    } catch (err) {
        throw err;
    }
};