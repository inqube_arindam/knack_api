exports.makeEmailTemplate = function(data) {
    try {
        console.log(data);
        var content =
            `<!DOCTYPE html>
      <html lang="en">
         <head>
            <title>Knack</title>
         </head>
         <body>
            <table style="font-size: 14px;width:480px;margin:auto; color: #212e43;border-radius: 2px; padding:25px; border: solid 1px rgba(33, 46, 67, 0.1);">
               <tr>
                  <td>
                     <table style="width:100%; padding-bottom: 10px;">
                        <tr>
                           <td style="text-align:right;">
                              <span><img src="https://app.zeplin.io/img/icZeplin.svg" alt="knack" style="margin-right:10px"></span>
                           </td>
                           <td>
                              <h3 style="margin:5px 0">` + data[0]['businessinfo'][0]["businessName"] + `</h3>
                              Dated:` + formatDate(new Date()) + `
                           </td>
                        </tr>
                     </table>
                     <table style="width:100%; padding-bottom: 10px;">
                        <tr>
                           <td style="">
                              <p style="margin:2px 0; font-size:16px;">` + data[0]["clientData"][0]["clientName"] + `</p> <p style="margin:2px 0; color:#666">`;
        if (data[0]["clientData"][0]["area"]) {
            content += data[0]["clientData"][0]["area"] + `<br>`;
        }
        if (data[0]["clientData"][0]["city"]) {
            content += data[0]["clientData"][0]["city"] + `<br>`;
        }
        if (data[0]["clientData"][0]["pin"]) {
            content += data[0]["clientData"][0]["pin"];
        }
        content += `
                              </p>
                           </td>
                           <td style="text-align:right">
                              <p style="margin:2px 0; color:#666; font-size:14px;">Receipt No</p>
                              <p style="margin:2px 0; color: #212e43; font-size:16px;">` + data[0]['getSubcriptionPaymentFormat'][0]["invoice_number"] + `</p>
                           </td>
                        </tr>
                     </table>
                     <hr style="border: 1px solid #ebeced">
                     <table style="width:100%; padding-bottom: 10px;">
                        <tr>
                           <td style="border-left: 5px solid #35c07c;    padding-left: 10px;">
                              <p style="margin:2px 0; font-size:16px;">` + data[0]["pricepack.pricepackName"] + `</p>
                              <p style="margin:2px 0 ">` + data[0]["serviceNameList"][0]["service.serviceNameList"] + `</p>`;

        if (data[0]["sessionInfo"].length > 0) {
            content += `<p style="margin:2px 0;color:#666 ">` + data[0]["sessionInfo"][0]['message'] + `</p>`;
        }
        content += `
                           </td>
                           <td style="text-align:right">
                              <p style="margin:2px 0; color: #35c07c; font-size:16px;font-weight:bold">&#x20B9 ` + data[0]['totalAmountPaid'] + `</p>`;
        if (data[0]['payment_mode'] == '2') {
            content += `<p style="margin:2px 0"><span style="width:80px; height:25px;border:1px solid #212e43;text-align:center; border-radius: 2px; line-height: 25px; display: inline-block; text-align: center;">Cheque</span> </p>`;
        } else {
            content += `<p style="margin:2px 0"><span style="width:80px; height:25px;border:1px solid #212e43;text-align:center; border-radius: 2px; line-height: 25px; display: inline-block; text-align: center;">Cash</span> </p>`;
        }
        if (data[0]['payment_mode'] == '2') {
            content += `<p style="margin:2px 0; color:#666">Cheque No: ` + data[0]['cheque_number'] + `</p>`;
        }
        content += `</td>
                        </tr>
                     </table>
                     <table style="width:100%; padding:25px 0 10px;">
                        <tr>
                           <td style="text-align:right"> <p style="margin:2px 0; color: #ff6366; font-size:16px;font-weight:bold">Pending Amount: &#x20B9 `;


        if (Number(data[0]['getTotalDue'][0]['total_current_due']) - Number(data[0]['getTotalPaid'][0]['total_paid_amount']) != NaN) {
            content += Number(data[0]['getTotalDue'][0]['total_current_due']) - Number(data[0]['getTotalPaid'][0]['total_paid_amount']);
        } else {
            content += 0;
        }


        if (data[0]['next_payment_due_date']) {
            content += `</p> <p style="margin:2px 0; color:#666">Next Due: ` + formatDate(data[0]['clientTotalPaymentLastDate']) + `</p>`;
        }
        content += `</td>
                        </tr>
                     </table>
                  </td>
               </tr>
            </table>
         </body>
      </html>`;

        return content;
    } catch (err) {
        throw err;
    }
};

function formatDate(date) {
    try {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [day, month, year].join('-');
    } catch (err) {
        throw err;
    }
}