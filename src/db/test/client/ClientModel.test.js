import { ClientDB } from "../../ClientModels/ClientModel.js";
var fs = require("fs");
var content = fs.readFileSync("src/db/test/client/client.json");
var jsonContent = JSON.parse(content);
var Clientid;
describe("create", () => {
  test("Client are added", async () => {
    const db = new ClientDB();
    const Client = await db.create(
      jsonContent.businessId,
      jsonContent.centerId,
      jsonContent.clientName,
      jsonContent.contactNumber,
      jsonContent.alternateNumber,
      jsonContent.emailId,
      jsonContent.dateOfBirth,
      jsonContent.area,
      jsonContent.pin,
      jsonContent.city,
      jsonContent.photoUrl
    );
    Clientid = await Client["dataValues"]["clientId"];
    expect(Client["dataValues"]["businessId"]).toBe("1");
  });
});
describe("list", () => {
  test("lists of Client in Business ID when we added", async () => {
    const db = new ClientDB();
    const Client = await db.listAll(jsonContent.businessId);
    expect(Client[0]["clientName"]).toBeDefined();
  });
});
describe("listclientcenterid", () => {
  test("lists of Client in Business id and center id when we added", async () => {
    const db = new ClientDB();
    const Client = await db.listOne(jsonContent.businessId, jsonContent.centerid);
    expect(Client[0]["clientName"]).toBeDefined();
  });
});
describe("listclientid", () => {
  test("lists of Client in Client id when we added", async () => {
    const db = new ClientDB();
    const Client = await db.cldetails(Clientid);
    expect(Client[0]["clientName"]).toBeDefined();
  });
});
describe("update", () => {
  test("Client are updated when we added", async () => {
    const db = new ClientDB();
    const Client = await db.update(
      Clientid,
      jsonContent.businessId,
      jsonContent.centerId,
      "Akshay-update",
      jsonContent.contactNumber,
      jsonContent.alternateNumber,
      jsonContent.emailId,
      jsonContent.dateOfBirth,
      jsonContent.area,
      jsonContent.pin,
      jsonContent.city,
      jsonContent.photoUrl
    );
    expect(Client[0]).toBe(1);
  });
});
describe("DeleteClient", () => {
  test("Remove Client when we added", async () => {
    const db = new ClientDB();
    const Client = await db.remove(Clientid);
    expect(Client[0]).toBe(1);
  });
});
