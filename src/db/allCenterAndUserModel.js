import BaseModel from "./BaseModel";
import Sequelize from "sequelize";
import { filterFields, createInstance } from "../lib/common";
import { ApplicationError } from "../lib/errors";
import { listAll } from "../routes/ClientRoutes/clientSchedule";
import LoginSignupSchema from "../schemas/loginSignup.schema.js";

import { KNACKDB, KNACK_DBSETTINGS } from "../lib/constants.js";

const PUBLIC_FIELDS_USER = ["userId", "businessId"];

const PUBLIC_FIELDS_CENTER = ["businessId", "centerId"];

export class AllCenterAndUserDB extends BaseModel {
    constructor(connection) {
        super("logininfo", connection);
        this.schema = LoginSignupSchema.LoginSignUp();
        this.name = "logininfo";
        this.db = this.connection;
        this.model = this.connection.model(this.name, this.schema);
    }

    listAll = async(information, businessId, filterPrivateFields = true) => {
        var show_user = {};
        var show_center = {};
        var user = [show_user, show_center];
        try {
            let user = await this.model.findAll({
                where: {
                    businessId: businessId
                },
                raw: true
            }); //displays user data from KCACK database

            if (user[0]) {
                var seqdb = new Sequelize(businessId, KNACKDB.user, KNACKDB.password, {
                    host: KNACKDB.host,
                    port: 3306,
                    dialect: "mysql",
                    pool: {
                        max: 100,
                        min: 0,
                        idle: 10000
                    }
                });
                show_center = await seqdb.query(
                    "select * from centers where businessId='" + businessId + "'", {
                        type: Sequelize.QueryTypes.SELECT //this code will remove the duplicate data (meta data)
                    }
                );
            }
            seqdb.close();
            show_user = await user; //displays user data
            return await [
                { business: filterFields(show_user, PUBLIC_FIELDS_USER) },
                { center: filterFields(show_center, PUBLIC_FIELDS_CENTER) }
            ];

            // if (filterPrivateFields) {
            //   return await filterFields(user, PUBLIC_FIELDS_USER);
            // }
        } catch (error) {
            throw error;
        }
    };
}
export default new AllCenterAndUserDB();