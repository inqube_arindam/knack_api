import { CLIENTS, KNACK_DBSETTINGS } from "../lib/constants.js";
import Sequelize from "sequelize";
const Op = Sequelize.Op;
var conn = KNACK_DBSETTINGS;
conn["operatorsAliases"] = Op;
const sequelize = new Sequelize(
  CLIENTS.database,
  CLIENTS.user,
  CLIENTS.password,
  conn
);
sequelize
  .authenticate()
  .then(() => {
    console.log("Clients DB Connection has been established successfully.");
  })
  .catch(err => {
    console.error("Unable to connect to the database clients database:", err);
  });
export default class BaseModelClients {
  constructor(name, connection) {
    this.name = name;
    if (sequelize) {
      this.connection = sequelize;
    }
  }

  async _getModel() {
    this.model = await this.connection.model(this.name, this.schema);
  }
}
