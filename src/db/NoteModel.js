import { filterFields, createInstance, clientInfo } from "../lib/common";
import {
    KNACKDB,
    KNACK_DBSETTINGS,
    KNACK_UPLOAD_URL,
    PEGINATION_LIMIT
} from "../lib/constants.js";
import Sequelize from "sequelize";
const Op = Sequelize.Op;


const PUBLIC_FIELDS = [
    "noteId",
    "businessId",
    "clientId",
    "heading",
    "description",
    "reminder_date",
    "reminder_time"
];

export class NoteDB {


    //===================Add New Note=============================
    create = async (information, businessId, clientId, heading, description, reminder_date, reminder_time) => {
        const noteData = {
            businessId,
            clientId,
            heading,
            description,
            reminder_date,
            reminder_time
        };
        try {
            let db = createInstance(["notes"], information);

            let note = await db.notes.create(noteData);
            db.sequelize.close();
            return await filterFields(note, PUBLIC_FIELDS, true);
        } catch (error) {
            throw error;
        }
    };

    // ====================List All Notes===========================

    listAllNotes = async (information, page, clientId, filterPrivateFields = true) => {
        try {
            var pageNo = (page) ? page : 1;
            var limitPerPage = 20;
            var offsetLimit = (pageNo - 1) * limitPerPage;

            let db = createInstance(["notes"], information);

            let notes = await db.notes.findAll({
                where: {
                    clientId: clientId,
                    status: 1
                },
                offset: offsetLimit,
                limit: limitPerPage,
                raw: true
            });
            db.sequelize.close();
            if (filterPrivateFields) {
                return await filterFields(notes, PUBLIC_FIELDS);
            }
        } catch (error) {
            throw error;
        }
    };

    // ================findOne Note=================================

    singleNote = async (information, notetId, filterPrivateFields = true) => {
        try {
            let db = createInstance(["notes"], information);

            let note = await db.notes.findAll({
                where: {
                    noteId: notetId,
                    status: 1
                },
                raw: true
            });
            db.sequelize.close();
            if (filterPrivateFields) {
                return await filterFields(note, PUBLIC_FIELDS);
            }
        } catch (error) {
            throw error;
        }
    };

    //==================update Note===================================

    updateNote = async (
        information,
        noteId,
        heading,
        description,
        reminder_date,
        reminder_time
    ) => {
        try {
            let db = createInstance(["notes"], information);

            let note = await db.notes.update({
                heading: heading,
                description: description,
                reminder_date: reminder_date,
                reminder_time: reminder_time
            }, {
                    where: {
                        noteId: noteId
                    }
                });
            db.sequelize.close();
            return await note;
        } catch (error) {
            throw error;
        }
    };

    //================Search Notes======================

    searchNote = async (information, data) => {
        try {
            let db = createInstance(["notes"], information);
            let result = await db.notes.findAll({
                where: {
                    clientId: data.clientId,
                    [Op.or]: [
                        {
                            heading: {
                                [Op.like]: '%' + data.searchKey + '%'
                            }
                        },
                        {
                            description: {
                                [Op.like]: '%' + data.searchKey + '%'
                            }
                        }
                    ]
                },
                raw: true
            })

            db.sequelize.close();
            return await result;
        } catch (error) {
            throw error;
        }
    };

    //=================Delete Note=================

    deleteNote = async (information, noteId) => {
        try {
            let db = createInstance(["notes"], information);
            let note = await db.notes.update(
                {
                    status: 0,
                    updateDateTime: Sequelize.fn('NOW')
                },
                {
                    where: {
                        noteId: noteId
                    }
                }
            );
            db.sequelize.close();
            return note;

        } catch (error) {
            throw error;
        }
    };




}

export default new NoteDB();