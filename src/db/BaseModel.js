import { KNACKDB, KNACK_DBSETTINGS } from "../lib/constants.js";
import Sequelize from "sequelize";
const Op = Sequelize.Op;
var conn = KNACK_DBSETTINGS;
conn["operatorsAliases"] = Op;
const sequelize = new Sequelize(
  KNACKDB.database,
  KNACKDB.user,
  KNACKDB.password,
  conn
);
sequelize
  .authenticate()
  .then(() => {
    console.log("Connection has been established successfully.");
  })
  .catch(err => {
    console.error("Unable to connect to the database:", err);
  });
export default class BaseModel {
  constructor(name, connection) {
    this.name = name;
    if (sequelize) {
      this.connection = sequelize;
    }
  }

  async _getModel() {
    this.model = await this.connection.model(this.name, this.schema);
  }
}
