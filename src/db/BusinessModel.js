import BaseModel from "./BaseModel";
import Sequelize from "sequelize";
import BusinessSchema from "../schemas/business.schema.js";
import { createInstance, getRepeatDays, mysql_real_escape_string } from "../lib/common";
import { KNACKDB, KNACK_DBSETTINGS, KNACK_UPLOAD_URL, KNACK_TEAM_UPLOAD_URL, PEGINATION_LIMIT } from "../lib/constants.js";

const PUBLIC_FIELDS = [
    "businessName",
    "businessType",
    "numberOfClients",
    "numberOfCenters",
    "contactNumber",
    "emailId",
    "area",
    "pin",
    "city"
];

const filterFields = (toFilter, allowedFields, showCreate = false) => {
    let data = [];
    if (showCreate) {
        toFilter = Array(toFilter);
    }
    toFilter.forEach(function(element) {
        data.push(
            allowedFields.reduce((memo, field) => {
                return {
                    ...memo,
                    [field]: element[field]
                };
            }, {})
        );
    });
    return data;
};

export class BusinessDB extends BaseModel {
    constructor(connection) {
        super("businessinfo", connection);
        this.schema = BusinessSchema.Business();
        this.name = "businessinfo";
        this.db = this.connection;
        this.model = this.connection.model(this.name, this.schema);
    }

    business = [];

    //Return business's information based on passed businessId
    details = async(information) => {
        try {
            let businessIdinformation = information.split(",")[1];
            var seqdb = new Sequelize(businessIdinformation, KNACKDB.user, KNACKDB.password, {
                host: KNACKDB.host,
                port: 3306,
                dialect: "mysql",
                pool: {
                    max: 100,
                    min: 0,
                    idle: 10000
                }
            });
            let business = await seqdb.query('SELECT `businessinfo`.*, (SELECT COUNT(*) FROM `clients`) AS `clientCount`, (SELECT COUNT(*) FROM `centers`) AS `centersCount`, (SELECT COUNT(*) FROM `pricepacks`) AS `pricepacksCount`, CASE WHEN photoUrl is NULL THEN CONCAT("' + KNACK_TEAM_UPLOAD_URL.localUrl + '" , "noimage.jpg" ) ELSE CONCAT("' + KNACK_TEAM_UPLOAD_URL.localUrl + '" , photoUrl ) END AS `photoFullUrl`, SUBSTRING(SOUNDEX(`businessName`), 1, 1) AS `name_initials` FROM `businessinfo` AS `businessinfo` WHERE `businessinfo`.`businessId` = "' + businessIdinformation + '" AND `businessinfo`.`status` = 1', {
                type: seqdb.Sequelize.QueryTypes.SELECT
            });
            return await business;
        } catch (error) {
            throw error;
        }
    };

    //Update specific business's information
    update = async(
        information,
        businessId,
        businessName,
        businessType,
        numberOfClients,
        numberOfCenters,
        numberOfTeamMembers,
        contactNumber,
        emailId,
        area,
        pin,
        city,
        landmark,
        photoUrl,
        business_days
    ) => {
        try {
            if (photoUrl != null) {
                var filename = Date.now() + photoUrl.name;

                // Use the mv() method to place the file somewhere on your server
                await photoUrl.mv("./uploads/users/" + filename, function(err) {});
                var photoUrl = filename;
            } else {
                var photoUrl = '';
            }

            let final_business_days = await getRepeatDays(business_days);

            let business = await this.model.update({
                businessName: businessName,
                businessType: businessType,
                numberOfClients: numberOfClients,
                numberOfCenters: numberOfCenters,
                numberOfTeamMembers: numberOfTeamMembers,
                contactNumber: contactNumber,
                emailId: emailId,
                area: area,
                pin: pin,
                city: city,
                landmark: landmark,
                photoUrl: photoUrl,
                business_days: final_business_days,
                updateDateTime: Date.now()
            }, {
                where: {
                    businessId: businessId
                }
            });
            if (business[0]) {
                var seqdb = new Sequelize(businessId, KNACKDB.user, KNACKDB.password, {
                    host: KNACKDB.host,
                    port: 3306,
                    dialect: "mysql",
                    pool: {
                        max: 100,
                        min: 0,
                        idle: 10000
                    }
                });
                await seqdb.query(
                    "update businessinfo set businessName='" + await mysql_real_escape_string(businessName) +
                    "', businessType='" + businessType +
                    "',numberOfClients='" + numberOfClients +
                    "', numberOfCenters='" + numberOfCenters +
                    "', numberOfTeamMembers='" + numberOfTeamMembers +
                    "', contactNumber='" + contactNumber +
                    "', emailId='" + emailId +
                    "', area='" + area +
                    "', pin='" + pin +
                    "', city='" + city +
                    "', landmark='" + landmark +
                    "', photoUrl='" + photoUrl + "', business_days='" + final_business_days +
                    "' where businessId='" + businessId + "'"
                );

                let db = createInstance(["clientCenter"], information);
                let centers_number = await db.centers.findAndCountAll({
                    where: {
                        status: 1,
                        businessId: businessId,
                        updateFlag: 1
                    },
                    raw: true
                });
                var flag = centers_number.count;

                if (flag > 0) {
                    //update
                    var centerName = area + " center";
                    let centers = await db.centers.update({
                        centerName: centerName,
                        area: area,
                        landmark: landmark,
                        pin: pin,
                        city: city,
                        contactNumber: contactNumber,
                        updateDateTime: Date.now()
                    }, {
                        where: {
                            businessId: businessId,
                            updateFlag: 1
                        }
                    });
                } else {
                    //insert
                    var centerName = area + " center";
                    var updateFlag = 1;
                    const centerdetail = {
                        businessId,
                        centerName,
                        area,
                        landmark,
                        pin,
                        city,
                        contactNumber,
                        updateFlag
                    };
                    let centers = await db.centers.create(centerdetail);
                    db.sequelize.close();
                }
                seqdb.close();
            }
            return await business;
        } catch (error) {
            throw error;
        }
    };

    //Removing business detail, i.e. setting status=0, multiple businesses can be deleted by passing multiple ids using ,(comma) separator
    remove = async businessId => {
        try {
            let business = await this.model.update({
                status: 0,
                updateDateTime: Date.now()
            }, {
                where: {
                    businessId: businessId
                }
            });
            return await business;
        } catch (error) {
            throw error;
        }
    };

    getAllBusinessIds = async() => {
        try {
            let businesses = await this.model.findAndCountAll();
            return businesses;
        } catch (error) {
            throw error;
        }
    }
}
export default new BusinessDB();