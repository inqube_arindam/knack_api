import BaseModel from "./BaseModel";
import Sequelize from "sequelize";
import LeadsSchema from "../schemas/knackadmin.lead.schema.js";
import NotesSchema from "../schemas/knackadmin.note.schema.js";
import ActivitySchema from "../schemas/knackadmin.activity.schema.js";
import LoginSignupSchema from "../schemas/loginSignup.schema.js";
import { createInstance, formatDate } from "../lib/common";
import { KNACKDB, KNACK_DBSETTINGS } from "../lib/constants.js";
import { ApplicationError } from "../lib/errors";

const PUBLIC_FIELDS = [
  "activityId",
  "leadId",
  "activityTypeLead",
  "activityStatus",
  "activityReason",
  "otherReason",
  "activityComments",
  "activityDate",
  "activityTime",
  "activityFinalStatus",
  "activityStep",
  "activityAssignedTo",
  "referalDetail",
  "isPending",
  "isCompleted",
  "activityCreatedDateTime",
  "activityModifiedDateTime",
  "activityLastDateTime"
];

const Op = Sequelize.Op;

const filterFields = (toFilter, allowedFields, showCreate = false) => {
  let data = [];
  if (showCreate) {
    toFilter = Array(toFilter);
  }
  toFilter.forEach(function(element) {
    data.push(
      allowedFields.reduce((memo, field) => {
        return {
          ...memo,
          [field]: element[field]
        };
      }, {})
    );
  });
  return data;
};

export class ActivityDB extends BaseModel {
  constructor(connection) {
    super("activityinfo", connection);
    this.schema = ActivitySchema.Activityinfo();
    this.name = "activityinfo";
    this.db = this.connection;
    this.model = this.connection.model(this.name, this.schema);
   
    //leads Model
    this.Leadsschema          = LeadsSchema.Leadinfo();
    this.Leadsname            = "leadinfo";
    this.Leadsmodel           = this.connection.model(this.Leadsname,this.Leadsschema);

    //Activity Model
    this.Notesschema          = NotesSchema.Notesinfo();
    this.Notesname            = "notesinfo";
    this.Notesmodel           = this.connection.model(this.Notesname,this.Notesschema);

    this.Loginschema          = LoginSignupSchema.LoginSignUp();
    this.Loginname            = "logininfo";
    this.Loginmodel           = this.connection.model(this.Loginname,this.Loginschema);
  }

  lead = []; activity = [];

  //
  deleActivity = async(
    activityId,
    leadId
  ) => {  
    try {
      let activityUpdate = await this.model.update(
        {
          isDelete:1
        },
        {
          where: {
            activityId: activityId
          }
        }
      );
    } catch (error) {
      throw error;
    }

    var leads = {};
    try {     
      var leads = await this.Leadsmodel.find({
        attributes: ['leadId', 'leadName', 'leadEmailId', 'contactNumber','leadAddress','pincode','createDateTime','updateDateTime','businessName','priority'],  
        order:[['createDateTime','DESC']],
        where: {
          leadId:leadId
        },
        raw: true
      });
      if(leads) {
        var notes   = await this.Notesmodel.findAll({
          where: {
            leadId:leadId,
            isDelete:0
          },
          raw: true
        });
        if(notes) {
          for (var i = 0; i < notes.length; i++) {
            var notesAddedBy = await this.Loginmodel.find({
              where: {
                userId: notes[i].created_by
              },
              raw:true
            });
            if(notesAddedBy) {
              notes[i].created_by_name  = notesAddedBy.name;
            }
          }      
        }
        //console.log(notes);
        leads.notesarr  = notes;  
        var activities  = await this.model.findAll({
          attributes: ['activityId', 'leadId', 'activityTypeLead', 'activityStatus','activityReason','otherReason','activityComments','activityDate','activityTime','activityFinalStatus','activityStep','activityAssignedTo','isCompleted','activityCreatedDateTime','activityModifiedDateTime','activityLastDateTime'],  
          order:[['activityStep','DESC']],
          where: {
           leadId: leads.leadId,
           isDelete:0
          },
          raw: true        
        }); 
        if(activities.length>0) {          
          for (var i = 0; i < activities.length; i++) {
            var assignedToName = await this.Loginmodel.find({
            attributes: ['name'],
            where: {
              userId: activities[i].activityAssignedTo
            }
            });
            if(assignedToName){
              activities[i].assignedToName = assignedToName.name;
            }
          }
        }
        leads.activityarr  = activities;
      }    
    } catch (error) {
        throw error;
    }
    return await leads;  
  }

  //Add New Leads
  createNewActivity = async(
    leadId,
    comment,
    created_by,
    creationDateTime
  ) => {
    const noteinfodetails = {
      leadId,
      comment,
      created_by,
      creationDateTime
    };
    //console.log(leadId);
    await this.model.create(noteinfodetails);   
    var leads = {};
    try {     
      var leads = await this.Leadsmodel.find({
        attributes: ['leadId', 'leadName', 'leadEmailId', 'contactNumber','leadAddress','pincode','createDateTime','updateDateTime','businessName','priority'],  
        order:[['createDateTime','DESC']],
        where: {
          leadId:leadId
        },
        raw: true
      });
      if(leads) {
        var notes   = await this.model.findAll({
          where: {
            leadId: leads.leadId,
            isDelete:0
          },
          raw: true
        });
        if(notes) {
          for (var i = 0; i < notes.length; i++) {
            var notesAddedBy = await this.Loginmodel.find({
              where: {
                userId: notes[i].created_by
              }
            });
            if(notesAddedBy) {
              notes[i].created_by_name  = notesAddedBy.name;
            }
          }      
        }
        leads.notesarr  = notes;  
        var activities  = await this.model.findAll({
          attributes: ['activityId', 'leadId', 'activityTypeLead', 'activityStatus','activityReason','otherReason','activityComments','activityDate','activityTime','activityFinalStatus','activityStep','activityAssignedTo','isCompleted','activityCreatedDateTime','activityModifiedDateTime','activityLastDateTime'],  
          order:[['activityStep','DESC']],
          where: {
           leadId: leads.leadId,
           isDelete:0
          },
          raw: true        
        }); 
        if(activities.length>0) {          
          for (var i = 0; i < activities.length; i++) {
            var assignedToName = await this.Loginmodel.find({
            attributes: ['name'],
            where: {
              userId: activities[i].activityAssignedTo
            }
            });
            if(assignedToName){
              activities[i].assignedToName = assignedToName.name;
            }
          }
        }
        leads.activityarr  = activities;
      }    
    } catch (error) {
        throw error;
    }
    return await leads;  
  }

  viewActivity = async(leadId, filterPrivateFields = true) => {
    //activity = [];
    try {
      let activity = await this.model.findAll({
        order:[['activityStep','DESC']],
        limit:1,
        where: {
          leadId:leadId,
          isPending:1
        },
        raw: true
      });
      if (filterPrivateFields) {
        return await filterFields(activity, PUBLIC_FIELDS);
      }
    } catch (error) {
      throw error;
    }
  };

  getAllAssignee = async (filterPrivateFields = true) => {
    var assignedToName = [];
    try {
      var assignedToName = await this.Loginmodel.findAll({
        attributes: ['name','userId'],
        where: {
          isSuperAdminTeam:1,
          status:1,
          role: {
            [Op.ne]: 5
          }
        }
      });
      return await assignedToName;
    } catch (error) {
      throw error;
    }
  };

  saveActivity = async(activityId=null, leadId, activityTypeLead, activityStatus, activityReason, otherReason=null, activityComments, activityAssignedTo, activityDate, activityTime, activityStep=null, nextActivityType=null, nextActivityReason=null, nextActivityDate=null, nextActivityTime=null,nextActivityReferal=null, nextActivityAssignedTo=null, nextActivityComment=null, nextActivityStatus) => {
    //current activity
    
    var current_activity = {}; var next_activity = {}; var insert_date_year = ''; var insert_date_month = ''; var insert_date_date = '';
    //console.log(leadId+' '+nextActivityType);
    //console.log(activityAssignedTo+' '+nextActivityAssignedTo);
    //console.log(activityDate+' '+nextActivityDate);
    if(activityId !='') {  //activity added for this leads
      //console.log("-------------------test1233puja test puja----------------------");    
      //update current activity
      //var insert_date = await formatDate(activityDate);
      //var date = new Date(activityDate);
      //console.log(leadId+' '+nextActivityType);
      /*insert_date_year = date.getUTCFullYear();
      insert_date_month = date.getUTCMonth()+1;
      insert_date_date = date.getUTCDate();
      console.log("insert_date_year");
      console.log(insert_date_year);*/
      //console.log(activityDate+' '+nextActivityDate);

      //var insert_date = await formatDate(activityDate);

      //console.log(insert_date);
      var nextActivityStep  = activityStep+1;
      try {
        let updateActivity = await this.model.update(
          {
            
            activityTypeLead: activityTypeLead,
            activityStatus: activityStatus,
            activityReason: activityReason,
            otherReason: otherReason,
            activityComments: activityComments,
            activityAssignedTo: activityAssignedTo,
            activityDate:  activityDate,
            activityTime: activityTime,
            isPending:0
          },
          {
            where: {
              activityId: activityId
            }
          }
        );
        return updateActivity;
      } catch (error) {
        throw error;
      }
    } else {
      //console.log('activity id is blank');
      //insert current activity
      var insert_date = await formatDate(activityDate);
      console.log(insert_date);
      //var date = new Date(activityDate);
      //console.log(leadId+' '+nextActivityType);
      /*var insert_date_year = date.getUTCFullYear();
      var insert_date_month = date.getUTCMonth()+1;
      var insert_date_date = date.getUTCDate();
      console.log("insert_date_year");
      console.log(insert_date_year);*/
      const activitydetail = {
        leadId,
        activityTypeLead: activityTypeLead,
        activityStatus,
        activityReason,
        otherReason,
        isPending:0,
        isComplete:1,
        activityComments,
        activityStep:1,
        activityAssignedTo: activityAssignedTo,
        activityDate:activityDate,
        activityTime
      };
      //console.log(activityDate);
      let createActivity = await this.model.create(activitydetail);
      var nextActivityStep  = 2;
    }
    //console.log('nextActivityStatus '+nextActivityStatus);
    //Next activity inserted   
    var next_activity_date  = await formatDate(nextActivityDate); 
    console.log(next_activity_date);
    if(nextActivityStatus>0) {
      if(activityStatus==3) {        
        const activitydetail = {
          leadId:leadId,
          activityTypeLead:activityTypeLead,
          activityReason:activityReason,
          activityComments:nextActivityComment,
          activityAssignedTo:activityAssignedTo,
          activityStep:nextActivityStep,
          activityDate:nextActivityDate,
          activityTime:nextActivityTime,
          isPending:1,
          activityFinalStatus:nextActivityStatus
        };
        let createActivity = await this.model.create(activitydetail);
      } else {
        const activitydetail = {
          leadId:leadId,
          activityTypeLead:nextActivityType,
          activityReason:nextActivityReason,
          activityComments:nextActivityComment,
          activityStep:nextActivityStep,
          activityDate:nextActivityDate,
          activityTime:nextActivityTime,
          isPending:1,
          activityAssignedTo:nextActivityAssignedTo,
          activityFinalStatus:nextActivityStatus
        };
        let createActivity = await this.model.create(activitydetail);
      }
    }
    //console.log('leads');
    var leads = await this.Leadsmodel.find({
      attributes: ['leadId', 'leadName', 'leadEmailId', 'contactNumber','leadAddress','pincode','createDateTime','updateDateTime','businessName','priority'],  
      order:[['createDateTime','DESC']],
      where: {
        leadId:leadId
      },
      raw: true
    });
    if(leads) {
      //console.log(leads.leadId);
      var notes   = await this.Notesmodel.findAll({
        where: {
          leadId: leads.leadId,
          isDelete:0
        },
        raw: true
      });
      if(notes) {
        for (var i = 0; i < notes.length; i++) {
          //console.log(notes[i].created_by);
          var notesAddedBy = await this.Loginmodel.find({
            where: {
              userId: notes[i].created_by
            }
          });
          //console.log(notesAddedBy);
          if(notesAddedBy) {
            //console.log("notesAddedBy sera value name"+notesAddedBy);
            //console.log(notesAddedBy.name);
            notes[i].created_by_name  = notesAddedBy.name;
            //console.log('here '+notes[i].created_by_name);
          }
          //console.log(notes[i].noteinfo);
        }
        //console.log(notes);        
      }
      leads.notesarr  = notes;
      var activities  = await this.model.findAll({
        attributes: ['activityId', 'leadId', 'activityTypeLead', 'activityStatus','activityReason','otherReason','activityComments','activityDate','activityTime','activityFinalStatus','activityStep','activityAssignedTo','isCompleted','activityCreatedDateTime','activityModifiedDateTime','activityLastDateTime'],  
        order:[['activityStep','DESC']],
        limit: 1,
        where: {
         leadId: leads.leadId,
         isDelete:0
        },
        raw: true        
      });  
      //console.log(activities.length);
      if(activities.length>0) {
        //console.log('Activity');
        for (var i = 0; i < activities.length; i++) {
          var assignedToName = await this.Loginmodel.find({
          attributes: ['name'],
          where: {
            userId: activities[i].activityAssignedTo
          }
          });
          if(assignedToName){
            activities[i].assignedToName = assignedToName.name;
          }
        }
        //console.log(assignedToName.name);
      }
      leads.activityarr  = activities;
    }
    return await leads;
  };

  // removeActivity = async(activityId, leadId, filterPrivateFields = true) => {   
  //   //activity = [];
  //   try {
  //     let activity = await this.model.findAll({
  //       order:[['activityStep','DESC']],
  //       limit:1,
  //       where: {
  //         leadId:leadId,
  //         isPending:1
  //       },
  //       raw: true
  //     });
  //     if (filterPrivateFields) {
  //       return await filterFields(activity, PUBLIC_FIELDS);
  //     }
  //   } catch (error) {
  //     throw error;
  //   }
  // };
}
export default new ActivityDB();
