import BaseModel from "./BaseModel";
import Sequelize from "sequelize";
import LeadsSchema from "../schemas/knackadmin.lead.schema.js";
import NotesSchema from "../schemas/knackadmin.note.schema.js";
import ActivitySchema from "../schemas/knackadmin.activity.schema.js";
import LoginSignupSchema from "../schemas/loginSignup.schema.js";
import { createInstance } from "../lib/common";
import { KNACKDB, KNACK_DBSETTINGS } from "../lib/constants.js";
import { ApplicationError } from "../lib/errors";

const PUBLIC_FIELDS = [
  "noteId",
  "leadId",
  "comment",
  "created_by",
  "creationDateTime"
];

const Op = Sequelize.Op;

const filterFields = (toFilter, allowedFields, showCreate = false) => {
  let data = [];
  if (showCreate) {
    toFilter = Array(toFilter);
  }
  toFilter.forEach(function(element) {
    data.push(
      allowedFields.reduce((memo, field) => {
        return {
          ...memo,
          [field]: element[field]
        };
      }, {})
    );
  });
  return data;
};

export class NotesDB extends BaseModel {
  constructor(connection) {
    super("notesinfo", connection);
    this.schema = NotesSchema.Notesinfo();
    this.name = "notesinfo";
    this.db = this.connection;
    this.model = this.connection.model(this.name, this.schema);
    //leads Model
    this.Leadsschema          = LeadsSchema.Leadinfo();
    this.Leadsname            = "leadinfo";
    this.Leadsmodel           = this.connection.model(this.Leadsname,this.Leadsschema);

    //Activity Model
    this.Activityschema       = ActivitySchema.Activityinfo();
    this.Activityname         = "activityinfo";
    this.Activitymodel        = this.connection.model(this.Activityname,this.ActivitySchema);

    this.Loginschema          = LoginSignupSchema.LoginSignUp();
    this.Loginname            = "logininfo";
    this.Loginmodel           = this.connection.model(this.Loginname,this.Loginschema);
  }

  lead = [];

  //list all notes
  listNotes = async(leadId) => {
    //console.log(leadId);
    var leads = {};
    try {     
      var leads = await this.Leadsmodel.find({
        attributes: ['leadId', 'leadName', 'leadEmailId', 'contactNumber','leadAddress','pincode','createDateTime','updateDateTime','businessName','priority'],  
        order:[['createDateTime','DESC']],
        where: {
          leadId:leadId
        },
        raw: true
      });
      if(leads) {
        var notes   = await this.model.findAll({
          where: {
            leadId: leads.leadId,
            isDelete:0
          },
          raw: true
        });
        if(notes) {
          for (var i = 0; i < notes.length; i++) {
            var notesAddedBy = await this.Loginmodel.find({
              where: {
                userId: notes[i].created_by
              }
            });
            if(notesAddedBy) {
              notes[i].created_by_name  = notesAddedBy.name;
            }
          }      
        }
        leads.notesarr  = notes;  
        var activities  = await this.Activitymodel.findAll({
          attributes: ['activityId', 'leadId', 'activityTypeLead', 'activityStatus','activityReason','otherReason','activityComments','activityDate','activityTime','activityFinalStatus','activityStep','activityAssignedTo','isCompleted','activityCreatedDateTime','activityModifiedDateTime','activityLastDateTime'],  
          order:[['activityStep','DESC']],
          where: {
           leadId: leads.leadId
          },
          raw: true        
        }); 
        if(activities.length>0) {          
          for (var i = 0; i < activities.length; i++) {
            var assignedToName = await this.Loginmodel.find({
            attributes: ['name'],
            where: {
              userId: activities[i].activityAssignedTo
            }
            });
            if(assignedToName){
              activities[i].assignedToName = assignedToName.name;
            }
          }
        }
        leads.activityarr  = activities;
      }    
    } catch (error) {
        throw error;
    }
    return await leads;
  };
  //Add New Leads
  createNewNote = async(
    leadId,
    comment,
    created_by
  ) => {
    const noteinfodetails = {
      leadId,
      comment,
      created_by,
      creationDateTime:Date.now()
    };
    console.log(leadId);
    await this.model.create(noteinfodetails);   
    var leads = {}; var notesarr  = {};
    try {     
      var leads = await this.Leadsmodel.find({
        attributes: ['leadId', 'leadName', 'leadEmailId', 'contactNumber','leadAddress','pincode','createDateTime','updateDateTime','businessName','priority'],  
        order:[['createDateTime','DESC']],
        where: {
          leadId:leadId
        },
        raw: true
      });
      if(leads) {
        var notes   = await this.model.findAll({
          where: {
            leadId: leads.leadId,
            isDelete:0
          },
          raw: true
        });
        console.log(notes)
        if(notes) {
          for (var i = 0; i < notes.length; i++) {
            var notesAddedBy = await this.Loginmodel.find({
              where: {
                userId: notes[i].created_by
              }
            });
            if(notesAddedBy) {
              notes[i].created_by_name  = notesAddedBy.name;
            }
          }      
        }
        
        leads.notesarr  = notes;
  
        var activities  = await this.Activitymodel.findAll({
          attributes: ['activityId', 'leadId', 'activityTypeLead', 'activityStatus','activityReason','otherReason','activityComments','activityDate','activityTime','activityFinalStatus','activityStep','activityAssignedTo','isCompleted','activityCreatedDateTime','activityModifiedDateTime','activityLastDateTime'],  
          order:[['activityStep','DESC']],
          where: {
           leadId: leads.leadId
          },
          raw: true        
        }); 
        
        if(activities.length>0) {   
          //for() 
          
          for (var i = 0; i < activities.length; i++) {
            var assignedToName = await this.Loginmodel.find({
            attributes: ['name'],
            where: {
              userId: activities[i].activityAssignedTo
            }
            });
            if(assignedToName){
              activities[i].assignedToName = assignedToName.name;
            }
          }
        }
        leads.activityarr  = activities;
      }    
    } catch (error) {
        throw error;
    }
    return await leads;  
  }

  deleteNote = async(noteId, leadId, filterPrivateFields = true) => {
    try {
      let notes = await this.model.update(
        {
          isDelete:1
        },
        {
          where: {
            noteid: noteId
          }
        }
      );
      //return updateActivity;
    } catch (error) {
      throw error;
    }
    /*var notes =  await this.model.update({
      where: {
        noteId: noteId
      }
    });*/
    var leads = {}; notes = {}; activities = {};
    try {     
      var leads = await this.Leadsmodel.find({
        attributes: ['leadId', 'leadName', 'leadEmailId', 'contactNumber','leadAddress','pincode','createDateTime','updateDateTime','businessName','priority'],  
        order:[['createDateTime','DESC']],
        where: {
          leadId:leadId
        },
        raw: true
      });
      if(leads) {
        var notes   = await this.model.findAll({
          where: {
            leadId: leads.leadId,
            isDelete:0
          }
        });
        if(notes) {
          for (var i = 0; i < notes.length; i++) {
            var notesAddedBy = await this.Loginmodel.find({
              where: {
                userId: notes[i].created_by
              }
            });
            if(notesAddedBy) {              
              notes[i].created_by_name  = notesAddedBy.name;              
            }
          }        
        }
        leads.notesarr  = notes;
  
        var activities  = await this.Activitymodel.findAll({
          attributes: ['activityId', 'leadId', 'activityTypeLead', 'activityStatus','activityReason','otherReason','activityComments','activityDate','activityTime','activityFinalStatus','activityStep','activityAssignedTo','isCompleted','activityCreatedDateTime','activityModifiedDateTime','activityLastDateTime'],  
          order:[['activityStep','DESC']],
          where: {
           leadId: leads.leadId
          },
          raw: true        
        });
        if(activities.length>0) {
          for (var i = 0; i < activities.length; i++) {
            var assignedToName = await this.Loginmodel.find({
            attributes: ['name'],
            where: {
              userId: activities[i].activityAssignedTo
            }
            });
            if(assignedToName){
              activities[i].assignedToName = assignedToName.name;
            }
          }
        }
        leads.activityarr  = activities;
      }  
    } catch (error) {
        throw error;
    }
    return await leads;
  };
}
export default new NotesDB();
