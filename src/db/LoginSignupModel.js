import { ApplicationError } from "../lib/errors";
import { hashPassword, comparePassword } from "../lib/crypto";
import Sequelize from "sequelize";
import BaseModel from "./BaseModel";
import { dumpTables, sendEmail, sendSms, createInstance, forgotPasswordSms } from "../lib/common";
import { generateToken } from "../lib/token";
import LoginSignupSchema from "../schemas/loginSignup.schema.js";
import BusinessSchema from "../schemas/business.schema.js";
import LoginMappingSchema from "../schemas/loginMapping.schema.js";
import PaymentSchema from "../schemas/payment.schema.js";
import PackagesSchema from "../schemas/packages.schema.js";

import { KNACKDB, KNACK_DBSETTINGS } from "../lib/constants.js";

const PUBLIC_FIELDS = [
    "userId",
    "name",
    "businessId",
    "emailId",
    "contactNumber",
    "userType",
    "modulePermissions",
    "lastLogin",
    "relatedDatabase",
    "relatedIP",
    "IMEINumber",
    "FcmUserToken",
    "status",
    "token",
    "isSuperAdmin",
    "isSuperAdminTeam"
];

const filterFields = (toFilter, allowedFields, showCreate = false) => {
    let data = [];
    if (showCreate) {
        toFilter = Array(toFilter);
    }
    toFilter.forEach(function(element) {
        data.push(
            allowedFields.reduce((memo, field) => {
                return {
                    ...memo,
                    [field]: element[field]
                };
            }, {})
        );
    });
    return data;
};

export class loginSignUpDB extends BaseModel {
    constructor(connection) {
        super("logininfo", connection);
        this.schema = LoginSignupSchema.LoginSignUp();
        this.name = "logininfo";
        this.db = this.connection;
        this.model = this.connection.model(this.name, this.schema);
        //Creating business model
        this.Businesschema = BusinessSchema.Business();
        this.Businessname = "businessinfo";
        this.Businessmodel = this.connection.model(
            this.Businessname,
            this.Businesschema
        );
        //Creating Login Mapping model
        this.Mappingschema = LoginMappingSchema.LoginMapping();
        this.Mappingname = "loginmapping";
        this.Mappingmodel = this.connection.model(
            this.Mappingname,
            this.Mappingschema
        );

        //Payment Model
        this.Paymentschema = PaymentSchema.Payment();
        this.Paymentname = "payment";
        this.paymentModel = this.connection.model(
            this.Paymentname,
            this.Paymentschema
        );

        //Package Model
        this.packageschema = PackagesSchema.Packages();
        this.packagename = "packages";
        this.packagedb = this.connection;
        this.packageModel = this.connection.model(this.packagename, this.packageschema);
    }

    loginsignup = [];

    //Adding new signup details
    create_old = async(
        name,
        emailId,
        contactNumber,
        password,
        FcmUserToken,
        IMEINumber
    ) => {
        if (!this.model) {
            await this._getModel();
        }
        var vcode = Math.floor(Math.random() * 8999 + 1000);
        var evcode = Math.floor(Math.random() * 7999 + 1000);
        const newLoginUser = {
            name,
            emailId,
            contactNumber,
            password: await hashPassword(password),
            userType: 1,
            modulePermissions: "",
            verificationCode: vcode,
            emailverificationCode: evcode,
            FcmUserToken,
            IMEINumber,
            status: 3
        };
        try {
            let LoginUser = await this.model
                .create(newLoginUser)
                .catch(this.db.ValidationError, function(error) {
                    var showError = error["errors"];
                    //throw error["errors"];
                    throw new ApplicationError(showError[0].message, 409);
                });
            const NewBusinessInfo = {
                businessId: LoginUser["dataValues"]["businessId"],
                contactNumber,
                emailId
            };
            let BusinessInfo = await this.Businessmodel.create(NewBusinessInfo);
            sendEmail(
                emailId,
                "Verification code from Knack",
                "Your verification code is " + evcode
            );
            sendSms(contactNumber, vcode);
            return await filterFields(LoginUser, PUBLIC_FIELDS, true);
        } catch (error) {
            // throw new ApplicationError(error.name, error.parent.errno);
            throw error;
        }
    };

    create = async(
        name,
        emailId,
        contactNumber,
        password,
        FcmUserToken,
        IMEINumber
    ) => {
        if (!this.model) {
            await this._getModel();
        }
        var vcode = Math.floor(Math.random() * 8999 + 1000);
        var evcode = Math.floor(Math.random() * 7999 + 1000);
        const newLoginUser = {
            name,
            emailId,
            contactNumber,
            password: await hashPassword(password),
            userType: 1,
            modulePermissions: "",
            verificationCode: vcode,
            emailverificationCode: evcode,
            FcmUserToken,
            IMEINumber,
            status: 3
        };
        try {
            let LoginUser = await this.model
                .create(newLoginUser)
                .catch(this.db.ValidationError, function(error) {
                    var showError = error["errors"];
                    //throw error["errors"];
                    throw new ApplicationError(showError[0].message, 409);
                });
            const NewBusinessInfo = {
                businessId: LoginUser["dataValues"]["businessId"],
                contactNumber,
                emailId
            };
            let BusinessInfo = await this.Businessmodel.create(NewBusinessInfo);
            let emailBody = "Hi " + name + ",<br><br>Welcome to Knack!<br><br>Your OTP for verifiying your email ID is " + evcode + ".<br><br>For any help or assistance, reach out to us anytime at support@justknackit.com";
            sendEmail(
                emailId,
                "Verify your email",
                emailBody
            );
            sendSms(contactNumber, vcode, name);
            return await filterFields(LoginUser, PUBLIC_FIELDS, true);
        } catch (error) {
            // throw new ApplicationError(error.name, error.parent.errno);
            throw error;
        }
    };

    verify = async(
        information,
        businessId,
        verificationCode,
        emailverificationCode,
        FcmUserToken,
        IMEINumber
    ) => {
        let verifylogin = await this.model.find({
            where: {
                status: 3,
                businessId: businessId,
                verificationCode: verificationCode,
                emailverificationCode: emailverificationCode
            },
            raw: true
        });
        if (!verifylogin) {
            return null;
        } else {
            //For Updating status
            await this.model.update({
                status: 4,
                verificationCode: "",
                emailverificationCode: "",
                relatedDatabase: businessId,
                relatedIP: KNACKDB.host,
                lastLogin: Date.now()
            }, { where: { businessId: businessId } });
            let chechkLogin = await this.Mappingmodel.find({
                where: {
                    IMEINumber: IMEINumber
                },
                raw: true
            });
            if (!chechkLogin) {
                const newLogin = {
                    userId: verifylogin.userId,
                    businessId: verifylogin.businessId,
                    centerId: verifylogin.centerId,
                    FcmUserToken: FcmUserToken,
                    IMEINumber: IMEINumber
                };
                await this.Mappingmodel.create(newLogin);
            } else {
                await this.Mappingmodel.update({
                    userId: verifylogin.userId,
                    businessId: verifylogin.businessId,
                    centerId: verifylogin.centerId,
                    lastLogin: Date.now(),
                    FcmUserToken: FcmUserToken
                }, { where: { IMEINumber: IMEINumber } });
            }
            await this.db.query("create database `" + businessId + "`");
            //Trying to create new connection for dumping tables and data in created db
            var seqdb = new Sequelize(businessId, KNACKDB.user, KNACKDB.password, {
                host: KNACKDB.host,
                port: 3306,
                dialect: "mysql",
                pool: {
                    max: 100,
                    min: 0,
                    idle: 10000
                }
            });
            await dumpTables(seqdb);
            await seqdb.query(
                "insert into businessinfo (businessId,contactNumber,emailId,status) values('" +
                verifylogin.businessId +
                "','" +
                verifylogin.contactNumber +
                "','" +
                verifylogin.emailId +
                "','1')"
            );

            //inserting data into employees table as well
            let loginInfo = await this.model.findAll({
                where: {
                    businessId: businessId
                },
                raw: true
            });

            // Role 1 for main admin
            const employeedetals = {
                roleId: 1,
                businessId: loginInfo[0].businessId,
                employeeName: loginInfo[0].name,
                emailId: verifylogin.emailId,
                contactNumber: verifylogin.contactNumber,
                // photoUrl: loginInfo[0].photoUrl,
                status: 1
            };
            let db = createInstance(["clientEmployee"], information);
            let employees = await db.employees.create(employeedetals);
            db.sequelize.close();
        }
        verifylogin.relatedDatabase = businessId;
        verifylogin.relatedIP = KNACKDB.host;
        verifylogin.token = await generateToken(verifylogin.businessId);
        seqdb.close();

        // Business settings start
        let businessSettingsData = [{
                "notification": {
                    "attendance": 1,
                    "paymentReceipts": 1,
                    "dailyInternalUpdate": 1,
                    "reScheaduling": 1,
                    "cancellation": 1,
                    "renewalOfService": 1,
                    "paymentInvoceies": 1
                }
            },
            {
                "neftDetails": {
                    "bankName": null,
                    "accountNumber": null,
                    "accountHolderName": null,
                    "ifscCode": null,
                    "acceptOnlinePayment": 0
                }
            },
            {
                "gstDetails": {
                    "gstApplicable": 0,
                    "sgst": null,
                    "cgst": null,
                    "gstnNumber": null
                }
            },
            {
                "paymentReminder": {
                    "sessionBased": {
                        "70%sessionBased": 0,
                        "0%sessionBased": 0,
                        "5daysBeforeValidity": 0,
                        "1daysBeforeValidity": 0
                    },
                    "anothersessionBased": {
                        "1daysBeforeDueDate": 0,
                        "3daysBeforeDueDate": 0,
                        "5daysBeforeDueDate": 0
                    }
                }
            },
            {
                "discountsOffers": {
                    "allServices": 0,
                    "selectedServices": []
                }
            },
            {
                "termsAndConditionsDetails": {
                    "content": null
                }
            }
        ];
        const newBusinessSetting = {
            businessId,
            businessSettingsData,
            businessSettingType: 'new'
        };
        let dbbusinessSettings = createInstance(["businessSettings"], information);
        let businessSettingsRowData = await dbbusinessSettings.businessSettings.create(newBusinessSetting);
        dbbusinessSettings.sequelize.close();
        // Business settings end

        return await filterFields(verifylogin, PUBLIC_FIELDS, true);
    };

    resend = async(userId, businessId) => {
        let login = await this.model.find({
            attributes: [
                "userId",
                "businessId",
                "name",
                "verificationCode",
                "emailverificationCode",
                "emailId",
                "contactNumber"
            ],
            where: {
                status: 3,
                userId: userId,
                businessId: businessId
            },
            raw: true
        });

        if (!login) {
            throw new ApplicationError("User is already verified", 401);
        } else {
            let evcode = login.emailverificationCode;
            let emailBody = "Hi " + login.name + ",<br><br>Welcome to Knack!<br><br>Your OTP for verifiying your email ID is " + evcode + ".<br><br>For any help or assistance, reach out to us anytime at support@justknackit.com";
            sendEmail(
                login.emailId,
                "Verify your email (OTP resent)",
                emailBody
            );
            sendSms(login.contactNumber, login.verificationCode, login.name);
            return filterFields(login, PUBLIC_FIELDS, true);

        }
    };

    verify_old = async(
        information,
        businessId,
        verificationCode,
        emailverificationCode,
        FcmUserToken,
        IMEINumber
    ) => {
        let verifylogin = await this.model.find({
            where: {
                status: 3,
                businessId: businessId,
                verificationCode: verificationCode,
                emailverificationCode: emailverificationCode
            },
            raw: true
        });
        if (!verifylogin) {
            return null;
        } else {
            //For Updating status
            await this.model.update({
                status: 4,
                verificationCode: "",
                emailverificationCode: "",
                relatedDatabase: businessId,
                relatedIP: KNACKDB.host,
                lastLogin: Date.now()
            }, { where: { businessId: businessId } });
            let chechkLogin = await this.Mappingmodel.find({
                where: {
                    IMEINumber: IMEINumber
                },
                raw: true
            });
            if (!chechkLogin) {
                const newLogin = {
                    userId: verifylogin.userId,
                    businessId: verifylogin.businessId,
                    centerId: verifylogin.centerId,
                    FcmUserToken: FcmUserToken,
                    IMEINumber: IMEINumber
                };
                await this.Mappingmodel.create(newLogin);
            } else {
                await this.Mappingmodel.update({
                    userId: verifylogin.userId,
                    businessId: verifylogin.businessId,
                    centerId: verifylogin.centerId,
                    lastLogin: Date.now(),
                    FcmUserToken: FcmUserToken
                }, { where: { IMEINumber: IMEINumber } });
            }
            await this.db.query("create database `" + businessId + "`");
            //Trying to create new connection for dumping tables and data in created db
            var seqdb = new Sequelize(businessId, KNACKDB.user, KNACKDB.password, {
                host: KNACKDB.host,
                port: 3306,
                dialect: "mysql",
                pool: {
                    max: 100,
                    min: 0,
                    idle: 10000
                }
            });
            await dumpTables(seqdb);
            await seqdb.query(
                "insert into businessinfo (businessId,contactNumber,emailId,status) values('" +
                verifylogin.businessId +
                "','" +
                verifylogin.contactNumber +
                "','" +
                verifylogin.emailId +
                "','1')"
            );

            //inserting data into employees table as well
            let loginInfo = await this.model.findAll({
                where: {
                    businessId: businessId
                },
                raw: true
            });

            // Role 1 for main admin
            const employeedetals = {
                roleId: 1,
                businessId: loginInfo[0].businessId,
                employeeName: loginInfo[0].name,
                emailId: verifylogin.emailId,
                contactNumber: verifylogin.contactNumber,
                photoUrl: loginInfo[0].photoUrl,
                status: 1
            };
            let db = createInstance(["clientEmployee"], information);
            let employees = await db.employees.create(employeedetals);
            db.sequelize.close();
        }
        verifylogin.relatedDatabase = businessId;
        verifylogin.relatedIP = KNACKDB.host;
        verifylogin.token = await generateToken(verifylogin.businessId);
        seqdb.close();

        // Business settings start
        let businessSettingsData = [{
                "notification": {
                    "attendance": 1,
                    "paymentReceipts": 1,
                    "dailyInternalUpdate": 1,
                    "reScheaduling": 1,
                    "cancellation": 1,
                    "renewalOfService": 1,
                    "paymentInvoceies": 1
                }
            },
            {
                "neftDetails": {
                    "bankName": null,
                    "accountNumber": null,
                    "accountHolderName": null,
                    "ifscCode": null,
                    "acceptOnlinePayment": 0
                }
            },
            {
                "gstDetails": {
                    "gstApplicable": 0,
                    "sgst": null,
                    "cgst": null,
                    "gstnNumber": null
                }
            },
            {
                "paymentReminder": {
                    "sessionBased": {
                        "70%sessionBased": 0,
                        "0%sessionBased": 0,
                        "5daysBeforeValidity": 0,
                        "1daysBeforeValidity": 0
                    },
                    "anothersessionBased": {
                        "1daysBeforeDueDate": 0,
                        "3daysBeforeDueDate": 0,
                        "5daysBeforeDueDate": 0
                    }
                }
            },
            {
                "discountsOffers": {
                    "allServices": 0,
                    "selectedServices": []
                }
            },
            {
                "termsAndConditionsDetails": {
                    "content": null
                }
            }
        ];
        const newBusinessSetting = {
            businessId,
            businessSettingsData,
            businessSettingType: 'new'
        };
        let dbbusinessSettings = createInstance(["businessSettings"], information);
        let businessSettingsRowData = await dbbusinessSettings.businessSettings.create(newBusinessSetting);
        dbbusinessSettings.sequelize.close();
        // Business settings end

        return await filterFields(verifylogin, PUBLIC_FIELDS, true);
    };

    resend_old = async(userId, businessId) => {
        let login = await this.model.find({
            attributes: [
                "userId",
                "businessId",
                "verificationCode",
                "emailverificationCode",
                "emailId",
                "contactNumber"
            ],
            where: {
                status: 3,
                userId: userId,
                businessId: businessId
            },
            raw: true
        });
        if (!login) {
            return null;
        } else {
            sendEmail(
                login.emailId,
                "Verification code from Knack",
                "Your verification code is " + login.emailverificationCode
            );
            sendSms(login.contactNumber, login.verificationCode);
            return filterFields(login, PUBLIC_FIELDS, true);
        }
    };

    //Fetching login details based on passed username and password
    getLoginByCredentials = async(
        emailId,
        password,
        FcmUserToken,
        IMEINumber
    ) => {
        const login = await this.getLoginById(emailId, false);
        if (!login) {
            throw new ApplicationError("Something doesn’t look right. Please check your email ID and password again.", 401);
        }

        // check user subscription status
        let payments = await this.paymentModel.findAll({
            where: {
                businessId: login[0].businessId,
                isRenewl: 1
            },
            raw: true
        });

        if (payments[0]) {
            if (payments[0].status == 0) {
                throw new ApplicationError("The subscription is cancelled. Please contact Knack.", 401);
            }
            var package_info = await this.packageModel.findAll({
                where: {
                    packageId: payments[0].packageId,
                    status: 1
                },
                raw: true
            });
            var expiryDate = new Date(payments[0].subscriptionDate);
            expiryDate.setMonth(expiryDate.getMonth() + parseInt(package_info[0].duration));
            var today = new Date();
            if (payments[0].paymentStatus != 2) {
                if (new Date(today) > new Date(payments[0].subscriptionDate)) {
                    throw new ApplicationError("Trial period is Expired. Please do payment for further transactions.", 401);
                }
            }
            if (new Date(today) > new Date(expiryDate)) {
                throw new ApplicationError("The subscription is Expired. Please contact Knack.", 401);
            }
        } else {
            throw new ApplicationError("Payment Is Not Found.", 401);
        }

        if (await comparePassword(password, login[0]["password"])) {
            let chechkLogin = await this.Mappingmodel.find({
                where: {
                    IMEINumber: IMEINumber
                },
                raw: true
            });
            if (!chechkLogin) {
                const newLogin = {
                    userId: login[0].userId,
                    businessId: login[0].businessId,
                    centerId: login[0].centerId,
                    FcmUserToken: FcmUserToken,
                    IMEINumber: IMEINumber
                };
                await this.Mappingmodel.create(newLogin);
            } else {
                await this.Mappingmodel.update({
                    userId: login[0].userId,
                    businessId: login[0].businessId,
                    centerId: login[0].centerId,
                    lastLogin: Date.now(),
                    FcmUserToken: FcmUserToken
                }, { where: { IMEINumber: IMEINumber } });

                await this.model.update({
                    FcmUserToken: FcmUserToken
                }, { where: { businessId: login[0].businessId, userId: login[0].userId } });
            }

            let finduser = await this.model.find({
                where: {
                    emailId: emailId
                },
                raw: true
            });

            var seqdb = new Sequelize(finduser.businessId, KNACKDB.user, KNACKDB.password, {
                host: KNACKDB.host,
                port: 3306,
                dialect: "mysql",
                pool: {
                    max: 100,
                    min: 0,
                    idle: 10000
                }
            });
            //for centre admin
            if (finduser.userType == 2 || finduser.userType == 1) {
                var centerArr = [];
                var emp = await seqdb.query("select scheduleId from teamschedulemap where teamId ='" + finduser.userId + "'", {
                    type: Sequelize.QueryTypes.SELECT
                });
                if (emp.length > 0) {
                    for (let i = 0; i < emp.length; i++) {
                        var scheduledetails = await seqdb.query("select * from schedule where scheduleId ='" + emp[i].scheduleId + "'", {
                            type: Sequelize.QueryTypes.SELECT
                        });
                        console.log(scheduledetails.length);

                        if (scheduledetails.length > 0) {

                            var centre = await seqdb.query("select centerId,centerName from centers where centerId ='" + scheduledetails[0].centerId + "'", {
                                type: Sequelize.QueryTypes.SELECT //this code will remove the duplicate data (meta data)
                            });
                            centerArr.push(centre[0]);

                        }
                    }
                } else {
                    let tempce = { centerId: '1', centerName: '' };
                    centerArr.push(tempce)
                }
                let p_schedule = centerArr.filter((center, index, self) =>
                    index === self.findIndex((t) => (
                        t.scheduleId == center.scheduleId

                    ))
                )

                //end of centre admin section

                var info = [];
                Array.prototype.push.apply(info, login);
                if (finduser.userType == 1) {
                    Array.prototype.push.apply(info, [{ User: "Main Admin" }]);
                } else {
                    Array.prototype.push.apply(info, [{ User: "Center Admin" }]);
                }
                Array.prototype.push.apply(info, [{ centerDetails: p_schedule }]);
                Object.assign.apply(Object, info);
                return login;
            }

            return filterFields(login, PUBLIC_FIELDS);
        }
        // return null;
        throw new ApplicationError("Invalid Password", 401);
    };

    getLoginById = async emailId => {
        let login = await this.model.findAll({
            where: {
                status: {
                    [this.db.Op.ne]: 0
                },
                emailId: emailId
            },
            raw: true
        });
        if (!login[0]) {
            return null;
        }
        return login;
    };

    getUserById = async(businessId, filterPrivateFields = true) => {
        let login = await this.model.findAll({
            where: {
                status: {
                    [this.db.Op.ne]: 0
                },
                businessId: businessId
            },
            raw: true
        });
        if (!login[0]) {
            return null;
        }
        if (filterPrivateFields) {
            return filterFields(login, PUBLIC_FIELDS);
        }
    };

    logout = async(userId, businessId, FcmUserToken, IMEINumber) => {
        let login = await this.Mappingmodel.destroy({
            where: {
                IMEINumber: IMEINumber
            }
        });
    };

    forgotPassword__ = async emailId => {
        let login = await this.model.find({
            where: {
                status: {
                    [this.db.Op.ne]: 0
                },
                emailId: emailId
            },
            raw: true
        });
        if (!login) {
            throw new ApplicationError("Email ID not Present", 401);
        } else {
            var vcode = Math.floor(Math.random() * 8999 + 1000);
            var evcode = Math.floor(Math.random() * 7999 + 1000);
            await this.model.update({
                //verificationCode: vcode,
                emailverificationCode: evcode
            }, { where: { emailId: emailId } });
            sendEmail(
                emailId,
                "Verification code from Knack",
                "Your verification code for reseting your password is " + evcode
            );
            //sendSms(login.contactNumber, vcode);
            return filterFields(login, PUBLIC_FIELDS, true);
        }
    };

    forgotPassword = async emailId => {
        let login = await this.model.find({
            where: {
                status: {
                    [this.db.Op.ne]: 0
                },
                emailId: emailId
            },
            raw: true
        });
        if (!login) {
            throw new ApplicationError("Email ID not Present", 401);
        } else {
            var vcode = Math.floor(Math.random() * 8999 + 1000);
            var evcode = Math.floor(Math.random() * 7999 + 1000);
            await this.model.update({
                //verificationCode: vcode,
                emailverificationCode: evcode
            }, { where: { emailId: emailId } });

            let emailBody = "Hi " + login.name + ",<br><br> To reset your password.<br><br>Your OTP is " + evcode + " - enter this in the app and proceed to reset your password.<br><br>For any help or assistance, reach out to us anytime at support@justknackit.com";
            sendEmail(
                emailId,
                "Reset your password",
                emailBody
            );
            forgotPasswordSms(login, vcode);
            return filterFields(login, PUBLIC_FIELDS, true);
        }
    };

    verifyResetPassword = async(
        emailId,
        //verificationCode,
        emailverificationCode
    ) => {
        let login = await this.model.find({
            where: {
                status: {
                    [this.db.Op.ne]: 0
                },
                emailId: emailId,
                //verificationCode: verificationCode,
                emailverificationCode: emailverificationCode
            },
            raw: true
        });
        if (!login) {
            throw new ApplicationError("Invalid inputs", 401);
        } else {
            await this.model.update({
                //verificationCode: "",
                emailverificationCode: ""
            }, { where: { emailId: emailId } });
            login.token = await generateToken(login.businessId);
            return await filterFields(login, PUBLIC_FIELDS, true);
        }
    };

    resetPassword = async(emailId, password, FcmUserToken, IMEINumber) => {
        let login = await this.model.update({
            password: await hashPassword(password)
        }, { where: { emailId: emailId } });
        if (!login) {
            throw new ApplicationError("Invalid inputs", 401);
        } else {
            let login = await this.model.findAll({
                where: {
                    status: {
                        [this.db.Op.ne]: 0
                    },
                    emailId: emailId
                },
                raw: true
            });
            let chechkLogin = await this.Mappingmodel.find({
                where: {
                    IMEINumber: IMEINumber
                },
                raw: true
            });
            if (!chechkLogin) {
                const newLogin = {
                    userId: login[0].userId,
                    businessId: login[0].businessId,
                    centerId: login[0].centerId,
                    FcmUserToken: FcmUserToken,
                    IMEINumber: IMEINumber
                };
                await this.Mappingmodel.create(newLogin);
            } else {
                await this.Mappingmodel.update({
                    userId: login[0].userId,
                    businessId: login[0].businessId,
                    centerId: login[0].centerId,
                    lastLogin: Date.now(),
                    FcmUserToken: FcmUserToken
                }, { where: { IMEINumber: IMEINumber } });
            }
            return filterFields(login, PUBLIC_FIELDS);
        }
    };

    findAll = async() => {
        //console.log("cron page find all");
        if (!this.model) {
            await this._getModel();
        }
        try {
            let getDatabase = await this.model.findAll({
                attributes: [
                    [Sequelize.fn('DISTINCT', Sequelize.col('businessId')), 'businessId']
                ],
                where: {
                    status: [1, 4]
                }
            });
            //console.log(JSON.stringify(getDatabase));
            // return await filterFields(getDatabase, PUBLIC_FIELDS, true);
            return await (JSON.stringify(getDatabase));

        } catch (e) {
            throw e;
        }
    }

}

export default new loginSignUpDB();