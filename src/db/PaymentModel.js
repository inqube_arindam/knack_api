import BaseModel from "./BaseModel";
import {
  sendEmail,
  sendSms,
  createInstance,mysql_real_escape_string
} from "../lib/common";
import Sequelize from "sequelize";
import PaymentSchema from "../schemas/payment.schema.js";
import LoginSignupSchema from "../schemas/loginSignup.schema.js";
import PackagesSchema from "../schemas/packages.schema.js";
import BusinessSchema from "../schemas/business.schema.js";
import {
  ApplicationError
} from "../lib/errors";
import {
  KNACKDB,
  KNACK_DBSETTINGS,
  EMAIL,
  UPLOAD_DIR,
  TRIAL_PERIOD,
  HELPLINE,
  SUPPORT_EMAIL
} from "../lib/constants.js";
import {
  textNotification,
  mailNotification
} from "../lib/notification";

var fs = require("fs");
var pdf = require("pdfkit");
var pdf1 = require('html-pdf');
const nodemail = require("nodemailer");
import request from "request";
const PUBLIC_FIELDS = [
  "paymentId",
  "packageId",
  "paymentMode",
  "userId",
  "businessId",
  "details",
  "centerNumber"
];
const filterFields = (toFilter, allowedFields, showCreate = false) => {
  let data = [];
  if (showCreate) {
    toFilter = Array(toFilter);
  }
  toFilter.forEach(function (element) {
    data.push(
      allowedFields.reduce((memo, field) => {
        return {
          ...memo,
          [field]: element[field]
        };
      }, {})
    );
  });
  return data;
};
export class PaymentDB extends BaseModel {
  constructor(connection) {
    super("payment", connection);
    this.schema = PaymentSchema.Payment();
    this.name = "payment";
    this.db = this.connection;
    this.model = this.connection.model(this.name, this.schema);
    //Login model for updating user's status
    this.Loginschema = LoginSignupSchema.LoginSignUp();
    this.LoginName = "logininfo";
    this.Loginmodel = this.connection.model(this.LoginName, this.Loginschema);

    //Get Package Model Data
    this.packageSchema = PackagesSchema.Packages();
    this.packageName = "packages";
    this.packageModel = this.connection.model(this.packageName, this.packageSchema);

    //Get Business Model Data
    this.businessSchema = BusinessSchema.Business();
    this.businessName = "businessinfo";
    this.businessModel = this.connection.model(this.businessName, this.businessSchema);
  }
  payments = [];

  create = async (
    packageId,
    userId,
    businessId,
    invoiceId,
    paymentMode,
    details,
    centerNumber,
    amountDate,
    amount
  ) => {

    var subscriptionDate = new Date();

    var dd = subscriptionDate.getDate();
    var mm = subscriptionDate.getMonth() + 1; //January is 0!

    var yyyy = subscriptionDate.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    var today = dd + '/' + mm + '/' + yyyy;

    var year = subscriptionDate.getFullYear();

    var invCount = await this.model.findAndCountAll({
      where: {
        status: 1
      },
      raw: true
    });
    let totalInv = invCount.count.toString().length;
    if (totalInv <= 1) {
      var invID = year + '0000' + (invCount.count + 1);
    } else if (totalInv === 2) {
      var invID = year + '000' + (invCount.count + 1);
    } else if (totalInv === 3) {
      var invID = year + '00' + (invCount.count + 1);
    } else if (totalInv === 4) {
      var invID = year + '0' + (invCount.count + 1);
    } else if (totalInv === 5) {
      var invID = year + (invCount.count + 1);
    }

    //var invID = Date.now() + Math.floor(Math.random() * 8999 + 1000);

    //get package info 

    subscriptionDate.setDate(subscriptionDate.getDate() + TRIAL_PERIOD); //trial duration
    let packages = await this.packageModel.findAll({
      where: {
        packageId: packageId,
        status: 1
      },
      raw: true
    });
    if (packages.length <= 0) {
      throw new ApplicationError(
        "This packageId is not valid",
        401
      );
    }
    var package_centers = packages[0].numberOfCenters;
    // get user info 

    let userinfo = await this.Loginmodel.findAll({
      where: {
        userId: userId,
        status: [1, 4]
      },
      raw: true
    });

    // get business info 
    let businessInfo = await this.businessModel.findAll({
      where: {
        businessId: businessId,
        status: 1
      },
      raw: true
    });


    var cgst = parseFloat(packages[0].cgst);
    var sgst = parseFloat(packages[0].sgst);
    var total_center_amount = parseFloat(parseFloat(packages[0].amount) * centerNumber).toFixed(2);
    var cgst_value = parseFloat(cgst * parseFloat(total_center_amount) / 100).toFixed(2);
    var sgst_value = parseFloat(sgst * parseFloat(total_center_amount) / 100).toFixed(2);
    var gst_value = parseFloat(cgst_value) + parseFloat(sgst_value);
    var total_amount = parseFloat(parseFloat(total_center_amount) + parseFloat(gst_value)).toFixed(2);



    const paymentdetail = {
      packageId,
      userId,
      businessId,
      invoiceId: invID,
      paymentMode,
      details,
      centerNumber,
      amountDate,
      amount: total_amount,
      subscriptionDate,
      isRenewl: 1
    };

    if (centerNumber <= package_centers) {
      try {

        let payments = await this.model.create(paymentdetail);
        await this.Loginmodel.update({
          status: 1
        }, {
            where: {
              businessId: businessId,
              userId: userId
            }
          });

        let get_package_date = await this.model.findAll({
          where: {
            businessId: businessId,
            invoiceId: invID
          },
          raw: true
        });

        let paymentData = await this.model.sequelize.query("SELECT logininfo.name, emailid, contactnumber, subscriptiondate, packages.name as PackageName, duration as PackageDuration, packages.amount as PricePackAmount, ( SELECT Date_add( subscriptiondate, INTERVAL duration day ) ) as expiryDate, ( SELECT Datediff( Date(( SELECT Date_add( subscriptiondate, INTERVAL duration day ) )), Date(Now()) ) ) as Diff, ( SELECT Date_add( Date(Now()), INTERVAL " + TRIAL_PERIOD + " day ) ) AS trialExpireDate, ( SELECT Datediff( Date(( SELECT Date_add( Date(Now()), INTERVAL " + TRIAL_PERIOD + " day ) )), Date(Now()) ) ) as trialEndsIn FROM `payment` JOIN `logininfo` ON payment.userid = logininfo.userid JOIN `packages` ON payment.packageid = packages.packageid WHERE logininfo.userid = '" + userId + "' AND payment.isrenewl = '1'", {
          type: this.model.sequelize.QueryTypes.SELECT
        });

        console.log("paymentData :-------> ", paymentData);

        //----**....Pdf creation.....**

        let business_name =  await mysql_real_escape_string(businessInfo[0].businessName);

        var content = `<html>
        <head>
            <title>Knack</title>
            <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
        </head>
        <body>
            <table style="letter-spacing: 0.1px; color: #212e43;padding:15px;font-family: 'Lato', sans-serif; background: #fff; width: 400px; margin: 0 auto; -webkit-box-shadow: 0 0 7px #d6d6d6;margin-top:4%; box-shadow: 0 0 7px #d6d6d6;">
                <tr>
                    <td>
                        <table style="width: 100%; padding:10px 15px 20px;">
                            <tr>
                                <td>
                                    <h3 style="color: #838383; font-weight: normal; text-align: center;font-family: serif;font-size: 22px;margin-bottom: 0;">INVOICE</h3>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:center"><img src="` + UPLOAD_DIR.localUrl + `images/Knack-Logo.svg" alt="knack" style="width: 150px;"></td>
                            </tr>
                            <tr>
                                <td style="text-align: center;margin-top: 8px;"><p><span style="color:#666">Date: </span >` + today + `</p></td>
                            </tr>
                        </table>
                        <table style="width: 100%; padding:15px 15px 0; color: #939393">
                            <tr>
                                <td style=" font-size:15px">` +businessInfo[0].businessName+ `</td>
                            </tr>
                            <tr>
                                <td style="color: #939393; font-size:13px">` + businessInfo[0].area + `</td>
                            </tr>
                            <tr>
                                <td style="color: #939393; font-size:13px">` + businessInfo[0].city + `</td>
                                <td style="color: #939393; font-size:13px; text-align:right">+91 9820789803</td>
                            </tr>
                            <tr>
                                <td style="color: #939393; font-size:13px">` + businessInfo[0].pin + `</td>
                                <td style="color: #939393; font-size:13px; text-align:right">support@justknackit.com</td>
                            </tr>
                        </table>
                        <hr style="border-top: 1px dotted #e0dfe4;  box-shadow: none; background: #fff;">
                        <table style="width: 100%; padding:15px;">
                            <tr>
                                <td style=" font-size:16px; color: #42464e">` + businessInfo[0].city + `</td>
                                <td style="text-align:right; color: #9199a1; font-size:14px">Invoice No</td>
                            </tr>
                            <tr>
                                <td style="color: #666; font-size:13px">` + businessInfo[0].businessType + `</td>
                                <td style="text-align:right">` + invID + `</td>
                            </tr>
                        </table>
                        <hr style=" border-top: 1px dotted #e0dfe4;  box-shadow: none; background: #fff;">
                        <table style="width: 100%; padding:15px;">
                            <tr>
                                <td style="font-size:15px; padding-bottom: 15px; color: #42464e">` + packages[0].name + `</td>
                                <td style="text-align:right; padding-bottom: 15px; color: #42464e">&#x20B9 ` + packages[0].amount + `</td>
                            </tr>
                            <tr>
                                <td style="font-size:15px;font-size:15px; color: #42464e">No. Of Centers</td>
                                <td style="text-align:right;font-size:15px; color: #42464e">` + centerNumber + `</td>
                            </tr>
                        </table>
                        <hr style="border-top: 1px dotted #e0dfe4;  box-shadow: none; background: #fff;">
                        <table style="text-align:right; width: 100%; padding:15px 15px 0;">
                            <tr>
                                <td collspan="2" style="color: #9199a1; font-size:13px; padding-bottom: 10px;">Total</td>
                                <td style="text-align:right; padding-bottom: 10px;">&#x20B9 ` + total_center_amount + `</td>
                            </tr>
                            <tr>
                                <td collspan="2" style="color: #9199a1; font-size:13px; padding-bottom: 10px;">CGST (` + cgst + `%)</td>
                                <td style="text-align:right; padding-bottom: 10px;"> &#x20B9 ` + cgst_value + `</td>
                            </tr>
                            <tr>
                                <td collspan="2" style="color: #9199a1; font-size:13px; padding-bottom: 10px;">SGST (` + sgst + `%)</td>
                                <td style="text-align:right; padding-bottom: 3px;">&#x20B9 ` + sgst_value + `</td>
                            </tr>
                        </table>
                        <table style="width: 100%;">
                            <tr>
                                <td></td>
                                <td collspan="2" style="">
                                    <hr style="border-top: 1px dotted #e0dfe4;  box-shadow: none; background: #fff;">
                                </td>
                            </tr>
                        </table>
                        <table style="width: 100%; padding:15px;">
                            <tr>
                                <td collspan="3" style="text-align:right; color: #afb4bc">Grand Total</td>
                            </tr>
                            <tr>
                                <td collspan="3" style="text-align:right; font-size:38px;font-weight: normal; color: #3a4559">&#x20B9 ` + total_amount + `</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </body>
        </html>`;

        var srv = [];
        var pricepack_service = [];
        Array.prototype.push.apply(srv, [payments]);
        //Array.prototype.push.apply(srv, [{'userName' : userinfo[0].name}]);
        //Array.prototype.push.apply(srv, [{ 'businessInfo' : business_name}]);
        Array.prototype.push.apply(srv, [{
          'amount': [{
            'userName': userinfo[0].name,
            'businessInfo': business_name,
            'cgst': cgst,
            'sgst': sgst,
            'cgst_value': cgst_value,
            'sgst_value': sgst_value,
            'centerAmount': total_center_amount,
            'totalAmount': total_amount
          }]
        }]);
        var merged = [];
        merged = Object.assign.apply(Object, srv);
        pricepack_service = pricepack_service.concat(merged);

        //... **fetching the email address**---
        var receiptant = await this.Loginmodel.findAll({
          where: {
            businessId: payments.businessId
          }
        });

        //........Mailing............
        let transporter = nodemail.createTransport({
          //service: "gmail",
          host: 'smtp.gmail.com',
          port: 465,
          auth: {
            user: EMAIL.userEmail,
            pass: EMAIL.password
          }
        });

        let emailBody = "Hi " + paymentData[0].name + ",<br><br>Thank you for registering with us and we're delighted to have you on board!<br><br> Operate your business now in just a few clicks, anytime, anywhere!<br><br>Your account information is as follows:<br>Your offer: Free trial (ends on " + paymentData[0].trialExpireDate + ")<br>Plan: " + paymentData[0].PackageName + "<br> Price: INR " + paymentData[0].PricePackAmount + " for " + paymentData[0].PackageDuration + " days<br>Plan start date: " + paymentData[0].trialExpireDate + "<br> Plan expiry date: " + paymentData[0].expiryDate + "<br><br> Please find the detailed invoice attached for your reference. For any help or assistance, reach out to us anytime at support@justknackit.com";

        //console.log(content);
        var options = {
          format: 'Letter'
        };
        var user_id = payments.userId + "_payment";
        fs.writeFile("uploads/html/" + user_id + ".html", content, function (err) {
          if (err) {
            console.log("Error!html File not created!");
            return console.log(err);
          } else {
            console.log("Success!html File created!");
            var html = fs.readFileSync('uploads/html/' + user_id + '.html', 'utf8');
            pdf1.create(html, options).toFile('uploads/knack_users/' + user_id + '.pdf', function (err, res) {
              if (err) {
                console.log(err);
              } else {
                let mailOptions = {
                  from: '"KNACK" <' + EMAIL.userEmail + '>',
                  to: receiptant[0].emailId,
                  subject: paymentData[0].name + ", Welcome! Enjoy Knack for free for 15 days.",
                  html: emailBody,
                  attachments: [{
                    filename: user_id + ".pdf",
                    path: "./uploads/knack_users/" + user_id + ".pdf",
                    contentType: "application/pdf"
                  }] // plain text body
                };

                //...Sending Mail...

                transporter.sendMail(mailOptions, (err, info) => {
                  if (err) {
                    return console.log(err);
                  } else {
                    console.log("Message sent");
                    //fs.unlinkSync('./uploads/html/' + user_id + '.html');
                    //fs.unlinkSync('uploads/knack_users/' + user_id + '.pdf');
                  }
                });
              }

              //console.log(res); // { filename: '/app/businesscard.pdf' }
            });
          }
        })


        var mobileNumber = receiptant[0].contactNumber;
        let pdfLink = UPLOAD_DIR.localUrl + 'knack_users/' + user_id + ".pdf";
        var sms_api =
          " http://api-alerts.solutionsinfini.com/v3/?method=sms&api_key=A31cf85c3cc3b65100bf9bd7fbe30cd90&to=+91_MOBILE&sender=SIDEMO&message=_MESSAGE";
        var url = "";

        var text = "Thank you for registering with Knack and subscribing to our " + paymentData[0].PackageName + " plan for " + paymentData[0].PackageDuration + " days. Your plan will only begin on " + paymentData[0].trialExpireDate + ", post your FREE trial period. To view your invoice, click on " + pdfLink + ". For any help, reach out to us anytime at +91 9820789803";

        url = sms_api.replace("_MOBILE", mobileNumber);
        url = url.replace("_MESSAGE", encodeURIComponent(text));
        request(url, function (err) {
          if (err) {
            throw err;
          }
        });

        // end pdf     

        return await pricepack_service; //27.06.2018
        //return await filterFields(payments, PUBLIC_FIELDS, true);
      } catch (error) {
        throw error;
      }
    } else {
      throw new ApplicationError(
        "You can add upto " + package_centers + " centers.",
        401
      );
    }
  };

  userInvoice =
    async (
      packageId,
      userId,
      businessId,
      centerNumber,
    ) => {
      try {
        var invID = Date.now() + Math.floor(Math.random() * 8999 + 1000);
        // get package info 
        let packages = await this.packageModel.findAll({
          where: {
            packageId: packageId,
            status: 1
          },
          raw: true
        });
        var package_centers = packages[0].numberOfCenters;
        if (centerNumber > package_centers) {
          throw new ApplicationError(
            "You can add upto " + package_centers + " centers.",
            401
          );
        }
        // get user info 
        let userinfo = await this.Loginmodel.findAll({
          where: {
            userId: userId,
            status: [1, 4]
          },
          raw: true
        });

        // get business info 
        let businessInfo = await this.businessModel.findAll({
          where: {
            businessId: businessId,
            status: 1
          },
          raw: true
        });

        var cgst = parseFloat(packages[0].cgst);
        var sgst = parseFloat(packages[0].sgst);
        var total_center_amount = parseFloat(parseFloat(packages[0].amount) * centerNumber).toFixed(2);
        var cgst_value = parseFloat(cgst * parseFloat(total_center_amount) / 100).toFixed(2);
        var sgst_value = parseFloat(sgst * parseFloat(total_center_amount) / 100).toFixed(2);
        var gst_value = parseFloat(cgst_value) + parseFloat(sgst_value);
        var total_amount = parseFloat(parseFloat(total_center_amount) + parseFloat(gst_value)).toFixed(2);

        var srv = [];
        var pricepack_service = [];
        console.log(packages);
        Array.prototype.push.apply(srv, [{
          'inVoiceID': invID,
          'userName': userinfo[0].name,
          'businessInfo': businessInfo[0].businessName,
          'packageInfo': packages,
          'packageName': packages[0].name,
          'centerNumber': centerNumber,
          'centerAmount': packages[0].amount,
          'cgst': cgst,
          'sgst': sgst,
          'cgst_value': cgst_value,
          'sgst_value': sgst_value,
          'total': total_center_amount,
          'totalAmount': total_amount
        }]);
        var merged = [];
        merged = Object.assign.apply(Object, srv);
        pricepack_service = pricepack_service.concat(merged);
        return await pricepack_service;
      } catch (error) {
        throw error;
      }
    };

  //user subscription renewal
  renewSubscriptionPayment = async (
    paymentId,
    packageId,
    userId,
    businessId,
    invoiceId,
    paymentMode,
    amount,
    centerNumber,
    amountDate,
    details
  ) => {
    try {
      var payments = await this.model.find({
        where: {
          paymentId: paymentId,
          isRenewl: 1
        },
        raw: true
      });

      if (payments) {
        var package_info = await this.packageModel.find({
          where: {
            status: 1,
            packageId: payments.packageId
          },
          raw: true
        });

        var invoiceId = Date.now() + Math.floor(Math.random() * 8999 + 1000);
        var expiryDate = new Date(payments.subscriptionDate);
        expiryDate.setMonth(expiryDate.getMonth() + parseInt(package_info.duration));
        var today = new Date();
      } else {
        throw new ApplicationError(
          "payment status isn't found",
          401
        );
      }

      if (new Date(today) <= new Date(expiryDate)) {
        var paymentUpdate = await this.model.update({
          isRenewl: 0,
          updateDateTime: Date.now()
        }, {
            where: {
              paymentId: paymentId
            }
          });

        //upgrade or keep the same package
        var cgst = parseFloat(9);
        var sgst = parseFloat(9);
        var total_center_amount = parseFloat(parseFloat(package_info.amount) * centerNumber).toFixed(2);
        var cgst_value = parseFloat(cgst * parseFloat(total_center_amount) / 100).toFixed(2);
        var sgst_value = parseFloat(sgst * parseFloat(total_center_amount) / 100).toFixed(2);
        var total_amount = parseFloat(parseFloat(total_center_amount) + parseFloat(cgst_value)).toFixed(2);

        var subscriptionDate = expiryDate; //new subscription will start after the plan expires
        var isRenewl = 1;
        const paymentdetail = {
          packageId,
          userId,
          businessId,
          invoiceId,
          paymentMode,
          amount: total_amount,
          centerNumber,
          amountDate,
          details,
          subscriptionDate,
          isRenewl
        };
        var paymentNew = await this.model.create(paymentdetail);
      } else {
        throw new ApplicationError(
          "subscription period is expired. You need to re-subscribe. Please contact Knack.",
          401
        );
      }
      return paymentNew;
    } catch (error) {
      throw error;
    }
  };

  listAll = async (businessId, filterPrivateFields = true) => {
    try {
      let payments = await this.model.findAll({
        where: {
          status: 1,
          businessId: businessId,
          isRenewl: 1
        },
        raw: true
      });

      var invID = Date.now() + Math.floor(Math.random() * 8999 + 1000);

      // get package info 
      var subscriptionDate = new Date();
      subscriptionDate.setDate(subscriptionDate.getDate() + TRIAL_PERIOD); //trial duration
      let packages = await this.packageModel.findAll({
        where: {
          packageId: payments[0].packageId,
          status: 1
        },
        raw: true
      });
      var package_centers = packages[0].numberOfCenters;
      // get user info 
      let userinfo = await this.Loginmodel.findAll({
        where: {
          userId: payments[0].userId,
          status: 1
        },
        raw: true
      });
      // get business info 
      let businessInfo = await this.businessModel.findAll({
        where: {
          businessId: payments[0].businessId,
          status: 1
        },
        raw: true
      });

      var cgst = parseFloat(packages[0].cgst);
      var sgst = parseFloat(packages[0].sgst);
      var srv = [];
      var pricepack_service = [];
      //Array.prototype.push.apply(srv, [payments]);
      let business_name =  await mysql_real_escape_string(businessInfo[0].businessName);
      Array.prototype.push.apply(srv, [{
        'payments': payments
      }, {
        'userinfo': [{
          'userName': userinfo[0].name,
          'businessInfo': business_name,
          'cgst': cgst,
          'sgst': sgst,
          'totalAmount': payments[0].amount
        }]
      }]);
      var merged = [];
      merged = Object.assign.apply(Object, srv);
      pricepack_service = pricepack_service.concat(merged);
      return await pricepack_service;
      // if (filterPrivateFields) {
      //   return await filterFields(payments, PUBLIC_FIELDS);
      // }
    } catch (error) {
      throw error;
    }
  };

  listAllBussiness = async (businessId, filterPrivateFields = true) => {
    try {
      let payments = await this.model.findAll({
        where: {
          status: 1,
          businessId: businessId
        },
        raw: true
      });
      if (filterPrivateFields) {
        return await filterFields(payments, PUBLIC_FIELDS);
      }
    } catch (error) {
      throw error;
    }
  };

  paymentDetail = async (paymentId, filterPrivateFields = true) => {
    try {
      let payments = await this.model.findAll({
        where: {
          status: 1,
          paymentId: paymentId
        },
        raw: true
      });
      if (filterPrivateFields) {
        return await filterFields(payments, PUBLIC_FIELDS);
      }
    } catch (error) {
      throw error;
    }
  };

  update = async (
    paymentId,
    packageId,
    userId,
    businessId,
    paymentMode,
    amount,
    details
  ) => {
    try {
      let payments = await this.model.update({
        packageId: packageId,
        userId: userId,
        businessId: businessId,
        paymentMode: paymentMode,
        amount: amount,
        details: details,
        updateDateTime: Date.now()
      }, {
          where: {
            paymentId: paymentId
          }
        });
      return payments;
    } catch (error) {
      throw error;
    }
  };
  remove = async paymentId => {
    try {
      let payments = await this.model.update({
        status: 0,
        updateDateTime: Date.now()
      }, {
          where: {
            paymentId: paymentId
          }
        });
      return payments;
    } catch (error) {
      throw error;
    }
  };

  allPayments = async () => {
    try {

      let payments = await this.model.sequelize.query("SELECT logininfo.name, emailid, contactnumber, subscriptiondate, packages.name as PackageName, duration as PackageDuration, packages.amount as PricePackAmount, ( SELECT Date_add( subscriptiondate, INTERVAL duration day ) ) as expiryDate, ( SELECT Datediff( Date(( SELECT Date_add( subscriptiondate, INTERVAL duration day ) )), Date(Now()) ) ) as Diff, ( SELECT Date_add( Date(Now()), INTERVAL " + TRIAL_PERIOD + " day ) ) AS trialExpireDate, ( SELECT Datediff( Date(( SELECT Date_add( Date(Now()), INTERVAL " + TRIAL_PERIOD + " day ) )), Date(Now()) ) ) as trialEndsIn FROM `payment` JOIN `logininfo` ON payment.userid = logininfo.userid JOIN `packages` ON payment.packageid = packages.packageid WHERE payment.isrenewl = '1'", {
        type: this.model.sequelize.QueryTypes.SELECT
      });

      return payments;
    } catch (error) {
      throw error;
    }
  }

  sendMessage = async (data, type) => {

    let str = (type == 'PLAN') ? 'Knack membership ends in ' + data.Diff + ' days on ' + data.expiryDate : 'Trial period on Knack ends in ' + data.trialEndsIn + ' days on ' + data.trialExpireDate;

    let emailSub = (type == 'PLAN') ? ' Reminder: Your Knack account is going to expire in ' + data.Diff + ' days' : 'Reminder: Your Free Trial is going to expire in ' + data.trialEndsIn + ' days';

    let msgBody = "Hi " + data.name + ", this is a gentle reminder that your " + str + ". Kindly re-subscribe on the app to enjoy any un-interrupted services. For any help or assistance, reach out to us anytime at " + HELPLINE;

    let emailBody = '';

    if (type == 'PLAN') {
      emailBody = "Hi " + data.name + ",<br><br> Thank you for using Knack. We hope you're enjoying our services.<br><br>As a reminder, your membership ends in " + data.Diff + " days on " + data.expiryDate + " . Kindly re-subscribe on the app to enjoy any un-interrupted services. <br><br>Your account information is as follows:<br>Plan: Silver<br>Plan Price: INR " + data.PricePackAmount + " for " + data.PackageDuration + " days<br>Plan start date: " + data.subscriptiondate + " <br>Plan expiry date: " + data.expiryDate + "<br><br>For any help or assistance, reach out to us anytime at " + SUPPORT_EMAIL;
    } else {
      emailBody = "Hi " + data.name + ",<br><br> Thank you for trying Knack. We hope you're finding it useful for your business..<br><br>As a reminder, your trial period ends in " + data.trialEndsIn + " days on " + data.trialExpireDate + " after which you will be able to use our services under your subscribed membership plan. If you wish to discontinue, remember to cancel before the end of the trial period to get your full money back.<br><br>Your account information is as follows:<br>Your offer: Free trial (ends on " + data.trialExpireDate + ")<br>Plan: Silver<br>Plan Price: INR " + data.PricePackAmount + " for " + data.PackageDuration + " days<br>Plan start date: " + data.subscriptiondate + " <br>Plan expiry date: " + data.expiryDate + "<br><br>For any help or assistance, reach out to us anytime at " + SUPPORT_EMAIL;
    }

    textNotification(msgBody, data.contactnumber);
    mailNotification(emailSub, emailBody, data.emailid);

  }

  membershipDetails = async (userId, businessId) => {
    let membershipData = await this.model.sequelize.query("SELECT centerNumber,paymentId,payment.amount as paymentAmount,amountDate,paymentMode,payment.packageId,name as PackageName,duration,subscriptionDate,( SELECT Date_add( Date(subscriptionDate), INTERVAL " + TRIAL_PERIOD + " day ) ) AS planStartDate,( SELECT Date_add((SELECT Date_add( Date(subscriptionDate), INTERVAL " + TRIAL_PERIOD + " day )), INTERVAL duration day ) ) AS planExpireDate,packages.amount as amountPerCenter,cgst,sgst FROM `payment` JOIN `packages` ON payment.packageId = packages.packageId WHERE payment.userId = '" + userId + "' AND payment.businessId = '" + businessId + "' AND isRenewl='1'", {
      type: this.model.sequelize.QueryTypes.SELECT
    });
    return membershipData;
  }

}
export default new PaymentDB();