import {
    filterFields,
    createInstance,
    commaSeparatedValues,
    getPaymentCalculation
} from "../../lib/common";
import {
    KNACKDB,
    KNACK_DBSETTINGS,
    KNACK_UPLOAD_URL,
    PEGINATION_LIMIT
} from "../../lib/constants.js";
var cron = require('node-cron');
import { ApplicationError } from "../../lib/errors";
const PUBLIC_FIELDS = [
    "pricepackId",
    "serviceId",
    "businessId",
    "pricepackName",
    "details",
    "pricepackType",
    "amount",
    "serviceDuration",
    "durationType",
    "pricepackValidity"
];

export class PricepackDB {
    pricepacks = [];



    create = async (
        information,
        businessId,
        serviceId,
        pricepackName,
        amount,
        serviceDuration,
        durationType,
        pricepackValidity,
        pricepackType,
        details
    ) => {
        var service_duration = await commaSeparatedValues(serviceDuration);
        var serviceIds = await commaSeparatedValues(serviceId);
        let dbSPM = createInstance(["servicePricepackMap"], information);

        let createdBy = information.split(',')[2];
        if (createdBy == null) {
            throw new ApplicationError("Please provide userId", 401);
        }

        if (service_duration.length != serviceIds.length) {
            throw new ApplicationError(
                "serviceId and duration_type doesn't match",
                401
            );
        }

        var type = 1;
        if (serviceId.includes(",")) {
            type = 2;
        }
        if ((pricepackType == 1 || pricepackType == 6) && (await type) == 2) {
            throw new ApplicationError(
                "Multiple service is not allowed for trial pack " +
                "and single service pricepack",
                401
            );
        }

        
        let ServiceDurationOfDays_MapTable = [];
        let serviceDuration_days = 0;
        let total = 0;
        for (var i = 0; i < service_duration.length; i++) {
            let ServiceDurationOfDays;
            serviceDuration_days = parseInt(serviceDuration_days) + parseInt(service_duration[i]);
            
            switch (durationType) {

                case "1":
                    ServiceDurationOfDays = service_duration[i] * 1;
                    break;
                case "2":
                    ServiceDurationOfDays = service_duration[i] * 1;
                    break;
                case "3":
                    ServiceDurationOfDays = service_duration[i] * 7;
                    break;
                case "4":
                    ServiceDurationOfDays = service_duration[i] * 30;
                    break;
                case "5":
                    ServiceDurationOfDays = service_duration[i] * 12 * 30;
                    break;
                case "6":
                    ServiceDurationOfDays = service_duration[i] * 1;
                    break;
                case "7":
                    ServiceDurationOfDays = service_duration[i] * 1;
                    break;
                default:
                    throw new ApplicationError("Error in Duration Type", 401);
            }

            total = total + ServiceDurationOfDays;
            ServiceDurationOfDays_MapTable[i] = ServiceDurationOfDays;

        }
       
        var serviceDuration = total;

        const newPricepack = {
            businessId,
            serviceId,
            pricepackName,
            amount,
            serviceDuration,
            serviceDuration_number : serviceDuration_days,
            durationType,
            pricepackValidity,
            pricepackType,
            details,
            type,
            createdBy
        };
        try {
            let db = createInstance(["clientPricepack"], information);
            let pricepacks = await db.pricepacks.create(newPricepack);
            var pricepackId = pricepacks.pricepackId;

            //inserting data into log table
            // const newLog = {
            //     businessId: businessId,
            //     activity: {
            //         header: 'create new pricepack',
            //         pricepackName: pricepackName,
            //         activityDate: new Date(),
            //         activityTime: new Date(),
            //         message: 'create new pricepack',
            //     },
            //     referenceTable: 'clientPricepack',
            // };
            // let dbLog = createInstance(["log"], information);
            // let log = await dbLog.log.create(newLog);
            //end of inserting data into log table

            for (var i = 0; i < serviceIds.length; i++) {
                var serviceId = serviceIds[i];
                var serviceDuration = service_duration[i];
                let serviceDuration_days = ServiceDurationOfDays_MapTable[i];
                let spm = await dbSPM.servicepricepackmap.create({
                    businessId,
                    pricepackId,
                    serviceId,
                    serviceDuration,
                    serviceDuration_number : serviceDuration_days,
                });
            }

            db.sequelize.close();
            return await filterFields(pricepacks, PUBLIC_FIELDS, true);
        } catch (error) {
            throw error;
        }
    };

    create_conver = async (
        information,
        businessId,
        serviceId,
        pricepackName,
        amount,
        serviceDuration,
        durationType,
        pricepackValidity,
        pricepackType,
        details
    ) => {
        var service_duration = await commaSeparatedValues(serviceDuration);
        var serviceIds = await commaSeparatedValues(serviceId);

        let createdBy = information.split(',')[2];

        if (createdBy == null) {
            throw new ApplicationError("Please provide userId", 401);
        }

        if (service_duration.length != serviceIds.length) {
            throw new ApplicationError(
                "serviceId and duration_type doesn't match",
                401
            );
        }

        var type = 1;
        if (serviceId.includes(",")) {
            type = 2;
        }
        if ((pricepackType == 1 || pricepackType == 6) && (await type) == 2) {
            throw new ApplicationError(
                "Multiple service is not allowed for trial pack " +
                "and single service pricepack",
                401
            );
        }

        let ServiceDurationOfDays;
        let ServiceDurationOfDays_MapTable = [];

        let total = 0;
        for (var i = 0; i < service_duration.length; i++) {

            let ServiceDurationOfDays;
            switch (durationType) {

                case "1":
                    ServiceDurationOfDays = service_duration[i];
                    break;
                case "2":
                    ServiceDurationOfDays = service_duration[i];
                    break;
                case "3":
                    ServiceDurationOfDays = service_duration[i] * 7;
                    break;
                case "4":
                    ServiceDurationOfDays = service_duration[i] * 30;
                    break;
                case "5":
                    ServiceDurationOfDays = service_duration[i] * 12 * 30;
                    break;
                case "6":
                    ServiceDurationOfDays = service_duration[i];
                    break;
                case "7":
                    ServiceDurationOfDays = service_duration[i] * 1;
                    break;
                default:
                    throw new ApplicationError("Error in Duration Type", 401);
            }

            total = total + ServiceDurationOfDays;

            ServiceDurationOfDays_MapTable[i] = ServiceDurationOfDays;

        }
        var serviceDuration = total;

        let pricepackValidity_days = 0;
        switch (durationType) {
            case "1":
                pricepackValidity_days = pricepackValidity;
                if (parseInt(pricepackValidity_days) < parseInt(serviceDuration)) {
                    throw new ApplicationError("PricePack Vality Session should be greater than ServiceDuration Session", 401);
                }
                break;


            case "2":


                pricepackValidity_days = pricepackValidity;

                if (parseInt(pricepackValidity_days) < parseInt(serviceDuration)) {


                    throw new ApplicationError("PricePack Vality Days should be greater than ServiceDuration Days", 401);
                }
                break;


            case "3":
                pricepackValidity_days = pricepackValidity * 7;

                if (parseInt(pricepackValidity_days) < parseInt(serviceDuration)) {


                    throw new ApplicationError("PricePack Vality Weeks should be greater than ServiceDuration Weeks", 401);
                }
                break;
            case "4":

                pricepackValidity_days = pricepackValidity * 30;
                if (parseInt(pricepackValidity_days) < parseInt(serviceDuration)) {


                    throw new ApplicationError("PricePack Vality Months should be greater than ServiceDuration Months", 401);
                }
                break;
            case "5":

                pricepackValidity_days = pricepackValidity * 12 * 30;
                if (parseInt(pricepackValidity_days) < parseInt(serviceDuration)) {


                    throw new ApplicationError("PricePack Vality Years should be greater than ServiceDuration Years", 401);
                }
                break;
            case "6":

                pricepackValidity_days = pricepackValidity;

                if (parseInt(pricepackValidity_days) < parseInt(serviceDuration)) {


                    throw new ApplicationError("PricePack Vality Items should be greater than ServiceDuration Items", 401);
                }
                break;
            case "7":
                pricepackValidity_days = pricepackValidity;


                if (parseInt(pricepackValidity_days) < parseInt(parseInt(pricepackValidity_days) < parseInt(serviceDuration))) {


                    throw new ApplicationError("PricePack Vality Consultancy should be greater than ServiceDuration Consultancy", 401);
                }
                break;
            default:
                throw new ApplicationError("Error in Duration Type", 401);
        }
        pricepackValidity = pricepackValidity_days;
        const newPricepack = {
            businessId,
            serviceId,
            pricepackName,
            amount,
            serviceDuration,
            durationType,
            pricepackValidity,
            pricepackType,
            details,
            type,
            createdBy
        };
        try {
            let db = createInstance(["clientPricepack"], information);
            let pricepacks = await db.pricepacks.create(newPricepack);
            var pricepackId = pricepacks.pricepackId;



            for (var i = 0; i < serviceIds.length; i++) {
                let dbSPM = createInstance(["servicePricepackMap"], information);
                var serviceId = serviceIds[i];
                let serviceDuration = ServiceDurationOfDays_MapTable[i];
                let spm = await dbSPM.servicepricepackmap.create({
                    businessId,
                    pricepackId,
                    serviceId,
                    serviceDuration
                });
            }

            db.sequelize.close();
            return await filterFields(pricepacks, PUBLIC_FIELDS, true);
        } catch (error) {
            throw error;
        }
    };


    listAll = async (information, params, filterPrivateFields = true) => {
        try {
            let orderBy;
            let businessIdinformation = information.split(",")[1];
            let filter = (params[0]) ? params[0] : '';
            var pageNo = (params['page']) ? params['page'] : 1;
            var limitPerPage = PEGINATION_LIMIT;
            var offsetLimit = (pageNo - 1) * limitPerPage;
            if (params["order"] == 1) {
                orderBy = [
                    ["updateDateTime", "DESC"]
                ];
            } else {
                orderBy = [
                    ["updateDateTime", "ASC"]
                ];
            }
            let dbpricepacks = createInstance(["clientPricepack"], information);
            let dbservices = createInstance(["clientService"], information);
            let dbservicepricepackmap = createInstance(["servicePricepackMap"], information);
            dbservicepricepackmap.servicepricepackmap.belongsTo(dbpricepacks.pricepacks, {
                foreignKey: "pricepackId",
                targetKey: "pricepackId"
            });
            dbservicepricepackmap.servicepricepackmap.belongsTo(dbservices.services, {
                foreignKey: "serviceId",
                targetKey: "serviceId"
            });
            let pricepackslist = await dbpricepacks.pricepacks.findAndCount({
                attributes:{
                    include:[[`serviceDuration_number`,`serviceDuration`]],
                    exclude:[`serviceDuration_number`]
                    },
                order: orderBy,
                where: {
                    status: 1,
                    businessId: businessIdinformation,
                    pricepackName: {
                        like: ["%" + filter + "%"]
                    }
                },
                offset: offsetLimit,
                limit: limitPerPage,
                raw: true
            });

            if (pricepackslist.rows) {
                for (var i = 0; i < pricepackslist.rows.length; i++) {
                    let serviceAll = await dbservicepricepackmap.servicepricepackmap.findAll({
                        attributes: {
                            include: [],
                            exclude: [`servicepricepackmapId`, `pricepackId`, `serviceId`, `businessId`, `serviceDuration_number`, `serviceDuration`, `createDateTime`, `updateDateTime`, `status`]
                        },
                        where: {
                            status: 1,
                            pricepackId: pricepackslist.rows[i].pricepackId
                        },
                        include: [{
                            model: dbservices.services,
                            attributes: [
                                [dbservicepricepackmap.sequelize.fn('GROUP_CONCAT', dbservicepricepackmap.sequelize.col('serviceName')), 'serviceNameList']
                            ],
                        }],
                        raw: true
                    });
                    let serviceAllID = await dbservicepricepackmap.servicepricepackmap.findAll({
                        attributes: {
                            include: [],
                            exclude: [`servicepricepackmapId`, `pricepackId`, `serviceId`, `businessId`, `createDateTime`, `updateDateTime`, `status`]
                        },
                        where: {
                            status: 1,
                            pricepackId: pricepackslist.rows[i].pricepackId
                        },
                        include: [{
                            model: dbservices.services,
                            attributes: [
                                'serviceId'
                            ],
                        }],
                        raw: true
                    });
                    pricepackslist.rows[i]["serviceIDList"] = serviceAllID;
                    pricepackslist.rows[i]["serviceNameList"] = serviceAll;
                    if (serviceAllID) {
                        for (var x = 0; x < serviceAllID.length; x++) {
                            let payble_amount_perPackage = pricepackslist.rows[i]["amount"];
                            let serviceId_perPackage = serviceAllID[x]["service.serviceId"];
                            let serviceId_serviceduration = serviceAllID[x]["service.serviceDuration"];
                            let pricepackId_perPackage = pricepackslist.rows[i].pricepackId;
                            pricepackslist.rows[i]["getPaymentCalculation"] = await getPaymentCalculation(information, payble_amount_perPackage, serviceId_perPackage, pricepackId_perPackage);
                        }
                    }
                }
            }
            dbpricepacks.sequelize.close();
            dbservices.sequelize.close();
            dbservicepricepackmap.sequelize.close();
            return pricepackslist;
        } catch (error) {
            throw error;
        }
    };

    listAllcenter = async (information, centerId, filterPrivateFields = true) => {
        try {
            let db = createInstance(["clientPricepack"], information);
            let pricepacks = await db.pricepacks.findAll({
                where: {
                    status: 1,
                    centerId: centerId
                },
                raw: true
            });
            db.sequelize.close();
            if (filterPrivateFields) {
                return await filterFields(pricepacks, PUBLIC_FIELDS);
            }
        } catch (error) {
            throw error;
        }
    };

    listAlloffer = async (information, serviceId, filterPrivateFields = true) => {
        try {
            let db = createInstance(["clientPricepack"], information);
            let pricepacks = await db.pricepacks.findAll({
                where: {
                    status: 1,
                    serviceId: {
                        like: "%" + serviceId + "%"
                    }
                },
                raw: true
            });
            db.sequelize.close();
            if (filterPrivateFields) {
                return await filterFields(pricepacks, PUBLIC_FIELDS);
            }
        } catch (error) {
            throw error;
        }
    };

    PricePackDetails = async (
        information,
        pricepackId,
        filterPrivateFields = true
    ) => {
        try {
            let db = createInstance(["clientPricepack"], information);
            let pricepacks = await db.pricepacks.findAll({
                where: {
                    status: 1,
                    pricepackId: pricepackId
                },
                raw: true
            });

            let service_pricepack_db = createInstance(["servicePricepackMap"], information);
            let service_pricepacks = await service_pricepack_db.servicepricepackmap.findAll({
                where: {
                    status: 1,
                    pricepackId: pricepacks[0].pricepackId
                },
                raw: true
            });

            var servicesData = Array();
            let service_db = createInstance(["clientService"], information);
            for (var i = 0; i < service_pricepacks.length; i++) {
                let services = await service_db.services.findAll({
                    where: {
                        status: 1,
                        serviceId: service_pricepacks[i].serviceId
                    },
                    raw: true
                });
                var totalServices = parseInt(pricepacks[0].serviceDuration);
                servicesData[i] = {

                    serviceName: services[0].serviceName,
                    serviceDuration: service_pricepacks[i].serviceDuration
                };
            }

            db.sequelize.close();
            return await { 'pricepackInfo': pricepacks, totalSerives: totalServices, 'services': servicesData };
            // if (filterPrivateFields) {
            //   return await filterFields(pricepacks, PUBLIC_FIELDS);
            // }
        } catch (error) {
            throw error;
        }
    };

    update = async (
        information,
        pricepackId,
        businessId,
        pricepackName,
        details
    ) => {
        try {
            let db = createInstance(["clientPricepack"], information);
            let pricepacks = await db.pricepacks.update({
                pricepackName: pricepackName,
                details: details,
                updateDateTime: Date.now()
            }, {
                    where: {
                        pricepackId: pricepackId,
                        status: 1
                    }
                });
            db.sequelize.close();
            return pricepacks;
        } catch (error) {
            throw error;
        }
    };

    expirePricepack = async (
        information,
        pricepackId,
        expiryDate,
        filterPrivateFields = true
    ) => {
        try {
            let db = createInstance(["clientPricepack"], information);
            var date = new Date();
            let expire_Date = new Date(expiryDate);

            let timediff = Math.abs(date.getTime() - expire_Date.getTime());
            // console.log((timediff/(1000*3600*24)));
            let days = Math.floor(timediff / (1000 * 3600 * 24));
            var is_expired = (days === 0) ? 1 : 0;
            var is_status = (days === 0) ? 0 : 1;

            let pricepacks = await db.pricepacks.update({
                // isExpired: is_expired,
                isExpired: 1,
                //status: is_status,
                expiryDate: expiryDate,
                updateDateTime: Date.now()
            }, {
                    where: {
                        pricepackId: pricepackId
                    }
                });
            db.sequelize.close();


            // Cron checking
            // cron.schedule('* * * * *', function(){           
            //   console.log("in cron");
            //   if(days===0)
            //   {
            //     let pricepacks = db.pricepacks.update({
            //       isExpired: 1,
            //       status: 0,
            //       },
            //     {
            //       where: {
            //       pricepackId: pricepackId
            //     }
            //     });
            //     db.sequelize.close();  
            //     console.log('after cron');
            //   }
            // }) 
            // Cron check end
            return pricepacks;
        } catch (error) {
            throw error;
        }
    };

    remove = async (information, pricepackId, filterPrivateFields = true) => {
        try {
            let db = createInstance(["clientSubscription"], information);
            let subscriptions = await db.subscriptions.findAll({
                where: {
                    status: 1,
                    pricepacks: pricepackId,
                    renewl_status: 1
                },
                raw: true
            }); //code ends here
            var subCount = subscriptions.length;
            if (subCount > 0) {
                throw new ApplicationError(
                    "Pricepack is assigned to one or more subcriptions. Hence it can't be deleted.",
                    401
                );
            } else {

                let db = createInstance(["clientPricepack"], information);
                let remove_pricepacks = await db.pricepacks.update({
                    status: 0,
                    updateDateTime: Date.now()
                }, {
                        where: {
                            pricepackId: pricepackId
                        }
                    });
                // show the remaining data..  

                let pricepacks = await db.pricepacks.findAll({
                    order: [
                        ["updateDateTime", "DESC"]
                    ],
                    where: {
                        status: 1,
                    },
                    raw: true
                });

                var pricepack_service = [];
                for (var i = 0; i < pricepacks.length; i++) {
                    let dbSPM = createInstance(["servicePricepackMap"], information);
                    let spm = await dbSPM.servicepricepackmap.findAll({
                        where: {
                            status: 1,
                            pricepackId: pricepacks[i].pricepackId
                        },
                        raw: true
                    });

                    var service_name = [];
                    for (var j = 0; j < spm.length; j++) {
                        let db = createInstance(["clientService"], information);
                        let services = await db.services.findAll({
                            where: {
                                status: 1,
                                serviceId: spm[j].serviceId
                            },
                            raw: true
                        });
                        service_name += services[0].serviceName;
                        if ((spm.length - 1) != j) service_name += ', ';
                    }

                    var srv = [];
                    Array.prototype.push.apply(srv, [pricepacks[i]]);
                    Array.prototype.push.apply(srv, [{ services: service_name }]);
                    var merged = [];
                    merged = Object.assign.apply(Object, srv);
                    pricepack_service = pricepack_service.concat(merged);
                }
                db.sequelize.close();
                return await pricepack_service;
            }

        } catch (error) {
            throw error;
        }
    };

    pricepackListByServiceId = async (information, params, filterPrivateFields = true) => {
        try {
            let orderBy;
            let businessIdinformation = information.split(",")[1];
            let serviceId = params.serviceId;

            let dbpricepacks = createInstance(["clientPricepack"], information);
            //let dbservices = createInstance(["clientService"], information);
            let dbservicepricepackmap = createInstance(["servicePricepackMap"], information);

            dbservicepricepackmap.servicepricepackmap.belongsTo(dbpricepacks.pricepacks, {
                foreignKey: "pricepackId",
                targetKey: "pricepackId"
            });
            // dbservicepricepackmap.servicepricepackmap.belongsTo(dbservices.services, {
            //     foreignKey: "serviceId",
            //     targetKey: "serviceId"
            // });
            let pricepackslist = await dbservicepricepackmap.servicepricepackmap.findAndCount({
                attributes: [],
                where: {
                    status: 1,
                    businessId: businessIdinformation,
                    serviceId: serviceId
                },
                include: [{
                    model: dbpricepacks.pricepacks
                }],

                raw: true
            });


            dbpricepacks.sequelize.close();
            //dbservices.sequelize.close();
            dbservicepricepackmap.sequelize.close();
            return pricepackslist;
        } catch (error) {
            throw error;
        }
    };

    cronFunction = async (information) => {
        try {
            console.log("Hello cron");
            let db = createInstance(["clientPricepack"], information);
            let dateStr = new Date().toISOString().split('T')[0];
            var date = new Date(dateStr);
            let pricepacks = await db.pricepacks.update({
                isExpired: 1,
                status: 0,
                updateDateTime: new Date()
            }, {
                    where: {
                        expiryDate: date
                    }
                });
            db.sequelize.close();

        } catch (error) {
            throw error;
        }
    };
}

export default new PricepackDB();