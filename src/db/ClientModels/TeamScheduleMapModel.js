import { filterFields, createInstance } from "../../lib/common";
import { ApplicationError } from "../../lib/errors";
import { start } from "repl";
import Sequelize from "sequelize";
import BaseModel from "../../db/BaseModel";

const PUBLIC_FIELDS = [
  "teamScheduleMapId",
  "businessId",
  "scheduleId",
  "teamId"
];

export class TeamScheduleMapDB extends BaseModel {
  teamschedulesmap = [];

  //Adding new schedule details
  create = async (information, businessId, scheduleId, teamId) => {
    const newTeamScheduleMap = {
      businessId,
      scheduleId,
      teamId
    };
    try {
      let db = createInstance(["teamScheduleMap"], information);
      let checkSchedule = await db.teamschedulemap.findAll({
        where: {
          teamId: teamId,
          status: 1
        },
        raw: true
      });
      if (!checkSchedule[0]) {
        let teamschedulesmap = await db.teamschedulemap.create(
          newTeamScheduleMap
        );
        db.sequelize.close();
        return await filterFields(teamschedulesmap, PUBLIC_FIELDS, true);
      } else {
        db.sequelize.close();
        throw new ApplicationError(
          "Selected team is already scheduled for selected date and time",
          409
        );
      }
    } catch (error) {
      throw error;
    }
  };
}
export default new TeamScheduleMapDB();
