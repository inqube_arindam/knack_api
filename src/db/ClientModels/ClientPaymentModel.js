import {
    ApplicationError
} from "../../lib/errors";
import {
    filterFields,
    createInstance,
    clientInfo,
    sendEmail,
    sendSms,
    makeEmailTemplate,
    numberWithCommas,
    hyphenDateFormat,
    getDateNextAndPrevious,
    make_date,
    date_ISO_Convert,
    getPaymentCalculation,
    sessionInfo,
    sendSmsMobile,
    sendPushNotification,
    getSessionPercentage,
    getAmountPercentage
} from "../../lib/common";
import Sequelize from "sequelize";
import BaseModel from "../../db/BaseModel";
import {
    KNACKDB,
    KNACK_UPLOAD_URL,
    PEGINATION_LIMIT,
    HELPLINE
} from "../../lib/constants.js";
import BusinessSchema from "../../schemas/business.schema.js";
import LoginSignupSchema from "../../schemas/loginSignup.schema";

var fs = require("fs");
var path = require("path");
var pdf1 = require("html-pdf");
import SubscriptionDB from "../ClientModels/ClientSubscriptionModel.js";

const PUBLIC_FIELDS = [
    "subscriptionId",
    "clientId",
    "businessId",
    "centerId",
    "services",
    "pricepacks",
    "service.serviceName",
    "totalSessionUnit",
    "totalSessionType",
    "totalAmount",
    "paymentType",
    "numberOfInstallments",
    "paymentDueDate",
    "subscriptionDateTime",
    "totalSessionsAttended",
    "totalAmountPaid",
    "payment_mode",
    "installment_frequency"
];

export class PendingpaymentDB extends BaseModel {
    subscriptions = [];

    constructor(connection) {
        //Get Business Model Data
        super("businessinfo", connection);
        this.businessSchema = BusinessSchema.Business();
        this.businessName = "businessinfo";
        this.businessModel = this.connection.model(
            this.businessName,
            this.businessSchema
        );
    }

    // create = async(
    //     information,
    //     clientpaymentId,
    //     businessId,
    //     subscriptionId,
    //     pricepacks,
    //     totalAmountPaid,
    //     payment_mode,
    //     cheque_number,
    //     payment_date
    // ) => {
    //     try {
    //         if (isNaN(totalAmountPaid)) {
    //             throw new ApplicationError(
    //                 "Please provide an integer value for totalAmountPaid",
    //                 401
    //             );
    //         }

    //         var dbpayment = createInstance(["clientPayments"], information);
    //         let clientpayments = await dbpayment.clientpayments.findAll({
    //             where: {
    //                 status: 1,
    //                 businessId: businessId,
    //                 subscriptionsId: subscriptionId,
    //             },
    //             order: [
    //                 ['payment_due_date', 'ASC'],
    //             ],
    //             raw: true
    //         });

    //         var total_paid = 0;
    //         for (var i = 0; i < clientpayments.length; i++) {
    //             if (clientpayments[i].paid_amount != null) {
    //                 total_paid += clientpayments[i].paid_amount;
    //             }
    //         }

    //         let dbSub = createInstance(["clientSubscription"], information);
    //         let subscriptions = await dbSub.subscriptions.findAll({
    //             where: {
    //                 status: 1,
    //                 businessId: businessId,
    //                 subscriptionId: subscriptionId
    //             },
    //             raw: true
    //         });
    //         dbSub.sequelize.close();

    //         var amtPaidSoFar = 0;
    //         if (subscriptions[0].amountPaidSoFar != null) {
    //             amtPaidSoFar = subscriptions[0].amountPaidSoFar;
    //         }

    //         total_paid = parseInt(total_paid) + parseInt(totalAmountPaid) + parseInt(amtPaidSoFar);
    //         let dbPricepack = createInstance(["clientPricepack"], information);
    //         let pricepack = await dbPricepack.pricepacks.findAll({
    //             where: {
    //                 pricepackId: pricepacks,
    //                 status: 1
    //             }
    //         });
    //         dbPricepack.sequelize.close();

    //         if (total_paid > pricepack[0].amount) {
    //             throw new ApplicationError(
    //                 "Payment Amount Should Be Less Than Pricepack Amount. Check All Installment Amounts.",
    //                 401
    //             );
    //         }

    //         if (clientpaymentId == clientpayments[clientpayments.length - 1].clientpaymentId) { //if it is last paymentId
    //             if ((pricepack[0].amount - total_paid) > 0) {
    //                 throw new ApplicationError(
    //                     "Please provide the total pending amount since it is the last installment",
    //                     401
    //                 );
    //             }
    //         }

    //         if (total_paid == pricepack[0].amount) {
    //             //make all the paymentStatus of remaining installments to 3
    //             var posFound;
    //             for (var i = 0; i < clientpayments.length; i++) {
    //                 if (clientpayments[i].clientpaymentId == clientpaymentId) {
    //                     posFound = i;
    //                     break;
    //                 }
    //             }

    //             for (var i = posFound + 1; i < clientpayments.length; i++) {
    //                 let update_client_payment = await dbpayment.clientpayments.update({
    //                     paymentStatus: 3
    //                 }, {
    //                     where: {
    //                         businessId: businessId,
    //                         subscriptionsId: subscriptionId,
    //                         clientpaymentId: clientpayments[i].clientpaymentId
    //                     }
    //                 });
    //             }
    //         }

    //         let update_client_payment = await dbpayment.clientpayments.update({
    //             paid_amount: totalAmountPaid,
    //             payment_date: payment_date,
    //             payment_mode: payment_mode,
    //             cheque_number: cheque_number,
    //             paymentStatus: 2
    //         }, {
    //             where: {
    //                 businessId: businessId,
    //                 subscriptionsId: subscriptionId,
    //                 clientpaymentId: clientpaymentId
    //             }
    //         });

    //         let find_client_payment = await dbpayment.clientpayments.find({
    //             where: {
    //                 businessId: businessId,
    //                 subscriptionsId: subscriptionId,
    //                 clientpaymentId: clientpaymentId
    //             }
    //         });
    //         dbpayment.sequelize.close();

    //         //let clientData = await clientInfo(information, getobj);
    //         let db = createInstance(["clientSubscription"], information);
    //         let subs = await db.subscriptions.findAll({
    //             where: {
    //                 businessId: businessId,
    //                 subscriptionId: subscriptionId
    //             },
    //             raw: true
    //         });
    //         db.sequelize.close();

    //         var getobj = {
    //             businessId: businessId,
    //             clientId: subs[0].clientId
    //         };
    //         let clientData = await clientInfo(information, getobj);
    //         var name = clientData[0].clientName;

    //         //inserting data into log table
    //         const newLog = {
    //             businessId: businessId,
    //             activity: {
    //                 header: 'payment received',
    //                 activityId: find_client_payment.clientpaymentId,
    //                 activityName: 'update',
    //                 activityDate: find_client_payment.updateDateTime.toLocaleDateString("en-US"),
    //                 activityTime: find_client_payment.updateDateTime.toLocaleTimeString("en-US"),
    //                 message: 'payment received from ' + name,
    //                 attendance: '',
    //                 payment: find_client_payment.paid_amount
    //             },
    //             referenceTable: 'clientPayment',
    //         };
    //         let dbLog = createInstance(["log"], information);
    //         let log = await dbLog.log.create(newLog);
    //         //end of inserting data into log table

    //         // Business settings start
    //         let dbBusinessSettings = createInstance(["businessSettings"], information);
    //         var businessSettingsRow = await dbBusinessSettings.businessSettings.find({
    //             where: {
    //                 businessId: businessId
    //             },
    //             raw: true
    //         });
    //         dbBusinessSettings.sequelize.close();
    //         if (businessSettingsRow) {
    //             let rowbusinessSettingsData = JSON.parse(businessSettingsRow.businessSettingsData);
    //             if (rowbusinessSettingsData) {
    //                 if (rowbusinessSettingsData[0].notification.paymentReceipts == 1) {
    //                     let business_info = await this.businessModel.findAll({
    //                         where: {
    //                             status: 1,
    //                             businessId: businessId
    //                         },
    //                         raw: true
    //                     });
    //                     var msg = "Your total payment received " + totalAmountPaid;
    //                     msg = msg.replace("is your One Time Password (OTP) for Knack", "");
    //                     var mailStatus = sendEmail(
    //                         business_info[0].emailId,
    //                         "Payment receipts from Knack",
    //                         msg
    //                     );
    //                     sendSms(business_info[0].contactNumber, msg);
    //                 }
    //             }
    //         }
    //         // Business settings end

    //         return await update_client_payment;
    //     } catch (error) {
    //         throw error;
    //     }
    // };

    create = async(
        information,
        clientpaymentId,
        businessId,
        subscriptionId,
        pricepacks,
        totalAmountPaid,
        payment_mode,
        cheque_number,
        payment_date
    ) => {
        try {
            let clientpaymentsListArray = [];
            let allClientpayments = [];
            let nextClientpayments = [];
            let todayFormat = await date_ISO_Convert(new Date());
            let businessIdinformation = information.split(",")[1];
            let userIdinformation = information.split(",")[2];
            let dbpaymentTransactions = createInstance(["paymentTransactions"], information);
            let dbclientpayments = createInstance(["clientPayments"], information);
            let dbservicepricepackmap = createInstance(["servicePricepackMap"], information);
            let dbservices = createInstance(["clientService"], information);
            let dbpricepacks = createInstance(["clientPricepack"], information);
            let dbsubscriptions = createInstance(["clientSubscription"], information);
            let dbLog = createInstance(["log"], information);
            let dbemployees = createInstance(["clientEmployee"], information);
            let amountPaidSoFar = 0;

            dbpaymentTransactions.paymentTransactions.belongsTo(dbclientpayments.clientpayments, {
                foreignKey: "clientpaymentId",
                targetKey: "clientpaymentId"
            });
            dbservicepricepackmap.servicepricepackmap.belongsTo(dbpricepacks.pricepacks, {
                foreignKey: "pricepackId",
                targetKey: "pricepackId"
            });
            dbservicepricepackmap.servicepricepackmap.belongsTo(dbservices.services, {
                foreignKey: "serviceId",
                targetKey: "serviceId"
            });
            // ====================== Start employees Details ======================================
            console.log("==========================employees===============================================");
            let employees = await LoginSignupSchema.LoginSignUp().findAll({
                where: {
                    status: 1,
                    userId: userIdinformation,
                },
                raw: true
            });
            console.log("==========================employees===============================================");
            // ====================== Start employees Details ======================================

            // ====================== Start Subcription Details ======================================
            let subscriptions = await dbsubscriptions.subscriptions.findAll({
                where: {
                    status: 1,
                    subscriptionId: subscriptionId,
                },
                raw: true
            });
            // ====================== End Subcription Details ======================================

            if (subscriptions[0].status_amountPaidSoFar == '1') {
                amountPaidSoFar = amountPaidSoFar + Number(subscriptions[0].amountPaidSoFar);
            }

            let alloverTotalPayment = await dbclientpayments.clientpayments.findAll({
                attributes: [
                    [dbservicepricepackmap.sequelize.fn('SUM', dbservicepricepackmap.sequelize.col('payble_amount')), 'payble_amount']
                ],
                where: {
                    status: 1,
                    [dbclientpayments.sequelize.Op.and]: [{
                        $paymentStatus$: { in: [1, 2]
                        }
                    }],
                    subscriptionsId: subscriptionId,
                    businessId: businessIdinformation
                },
                raw: true
            });

            let calTotalPaid = (Number(alloverTotalPayment[0].payble_amount)) ? Number(alloverTotalPayment[0].payble_amount) : 0;
            // ====================== start price pack total price calculation ======================================
            let pricepackslist = await dbpricepacks.pricepacks.findAndCount({
                order: [
                    ["updateDateTime", "DESC"]
                ],
                where: {
                    status: 1,
                    pricepackId: pricepacks,
                    businessId: businessIdinformation
                },
                raw: true
            });

            if (pricepackslist.rows) {
                for (var i = 0; i < pricepackslist.rows.length; i++) {
                    let serviceAll = await dbservicepricepackmap.servicepricepackmap.findAll({
                        attributes: {
                            include: [],
                            exclude: [`servicepricepackmapId`, `serviceDuration_number`, `pricepackId`, `serviceId`, `businessId`, `serviceDuration`, `createDateTime`, `updateDateTime`, `status`]
                        },
                        where: {
                            status: 1,
                            pricepackId: pricepackslist.rows[i].pricepackId
                        },
                        include: [{
                            model: dbservices.services,
                            attributes: [
                                [dbservicepricepackmap.sequelize.fn('GROUP_CONCAT', dbservicepricepackmap.sequelize.col('serviceName')), 'serviceNameList']
                            ],
                        }],
                        raw: true
                    });
                    let serviceAllID = await dbservicepricepackmap.servicepricepackmap.findAll({
                        attributes: {
                            include: [],
                            exclude: [`servicepricepackmapId`, `pricepackId`, `serviceId`, `businessId`, `serviceDuration`, `createDateTime`, `updateDateTime`, `status`]
                        },
                        where: {
                            status: 1,
                            pricepackId: pricepackslist.rows[i].pricepackId
                        },
                        include: [{
                            model: dbservices.services,
                            attributes: [
                                'serviceId'
                            ],
                        }],
                        raw: true
                    });
                    pricepackslist.rows[i]["serviceIDList"] = serviceAllID;
                    pricepackslist.rows[i]["serviceNameList"] = serviceAll;
                    if (serviceAllID) {
                        for (var x = 0; x < serviceAllID.length; x++) {
                            let payble_amount_perPackage = pricepackslist.rows[i]["amount"];
                            let serviceId_perPackage = serviceAllID[x]["service.serviceId"];
                            let pricepackId_perPackage = pricepackslist.rows[i].pricepackId;
                            pricepackslist.rows[i]["getPaymentCalculation"] = await getPaymentCalculation(information, payble_amount_perPackage, serviceId_perPackage, pricepackId_perPackage);
                        }
                    }
                }
            }




            let pricepacksTotatalPrice = pricepackslist.rows[0].getPaymentCalculation.payableAmount;

            // ====================== End price pack total price calculation ======================================
            let singlePaymentClientpayments = await dbclientpayments.clientpayments.findAll({
                where: {
                    status: 1,
                    clientpaymentId: clientpaymentId,
                    subscriptionsId: subscriptionId,
                    businessId: businessIdinformation
                },
                raw: true
            });

            let singleDueAmount = Number(singlePaymentClientpayments[0].due_amount);
            let singlePaidAmount = Number(singlePaymentClientpayments[0].payble_amount);
            let allClientpaymentsTransaction = await dbpaymentTransactions.paymentTransactions.findAll({
                attributes: [
                    [dbservicepricepackmap.sequelize.fn('SUM', dbservicepricepackmap.sequelize.col('paid_amount')), 'payble_amount_sum']
                ],
                where: {
                    status: 1,
                    [dbpaymentTransactions.sequelize.Op.and]: [{
                        $clientpaymentId$: { in: dbpaymentTransactions.sequelize.literal("(SELECT `clientpaymentId` FROM `clientpayments` WHERE `subscriptionsId` = '" + subscriptionId + "')")
                        }
                    }]

                },
                raw: true
            });
            let SumcalTotalPaid = (Number(allClientpaymentsTransaction[0].payble_amount_sum)) ? Number(allClientpaymentsTransaction[0].payble_amount_sum) : 0;
            if ((Number(pricepacksTotatalPrice) < (Number(SumcalTotalPaid) + Number(amountPaidSoFar) + Number(totalAmountPaid)))) {
                throw new ApplicationError(
                    "You can not payment more then " + pricepacksTotatalPrice + " payble amount,you can pay " + (Number(pricepacksTotatalPrice) - (Number(SumcalTotalPaid) + Number(amountPaidSoFar))),
                    401
                );
            }
            allClientpayments = await dbclientpayments.clientpayments.findAll({
                where: {
                    status: 1,
                    [dbclientpayments.sequelize.Op.and]: [{
                        $paymentStatus$: { in: [1]
                        }
                    }],
                    subscriptionsId: subscriptionId,
                    businessId: businessIdinformation
                },
                order: [
                    ["payment_end_date", "ASC"]
                ],
                raw: true
            });
            let insertPaymentTransactions = await dbpaymentTransactions.paymentTransactions.create({
                clientpaymentId: clientpaymentId,
                subscriptionsId: subscriptionId,
                payment_user_id: userIdinformation,
                payment_date: payment_date,
                payment_mode: payment_mode,
                paid_amount: totalAmountPaid,
                cheque_number: cheque_number,
                raw: true
            });
            // ================================= start ============================



            console.log(insertPaymentTransactions.dataValues.paymentTransactionId);

            if (Number(singlePaidAmount) == 0) {
                var total_paid_amount_db = Number(totalAmountPaid) + Number(amountPaidSoFar) + Number(singlePaidAmount) + calTotalPaid;
            } else {
                var total_paid_amount_db = Number(totalAmountPaid) + Number(amountPaidSoFar) + calTotalPaid;
            }


            console.log("======" + Number(totalAmountPaid) + "======" + Number(amountPaidSoFar) + "======" + Number(singlePaidAmount) + "======" + calTotalPaid + "======");

            // return total_paid_amount_db;

            if ((singleDueAmount) >= Number(total_paid_amount_db)) {
                let paymentStatusSec = 1;
                if ((singleDueAmount) == total_paid_amount_db) {
                    paymentStatusSec = 2;
                }
                let update_client_payment = await dbclientpayments.clientpayments.update({
                    payble_amount: Number(totalAmountPaid) + Number(amountPaidSoFar) + Number(singlePaidAmount),
                    paymentStatus: paymentStatusSec
                }, {
                    where: {
                        status: 1,
                        businessId: businessId,
                        subscriptionsId: subscriptionId,
                        clientpaymentId: clientpaymentId
                    }
                });
            } else {
                nextClientpayments = await dbclientpayments.clientpayments.findAll({
                    where: {
                        status: 1,
                        // [dbclientpayments.sequelize.Op.and]: [{
                        //     $paymentStatus$: { in: [1]
                        //     }
                        // }],
                        subscriptionsId: subscriptionId,
                        businessId: businessIdinformation
                    },
                    order: [
                        ["payment_end_date", "ASC"]
                    ],
                    raw: true
                });
                if (subscriptions[0].installment_frequency == '6') {

                    if (nextClientpayments) {
                        let temp_cal_data = 0;
                        for (var i = 0; i < nextClientpayments.length; i++) {

                            let updatenextClientpayments = await dbclientpayments.clientpayments.findAll({
                                where: {
                                    status: 1,
                                    [dbclientpayments.sequelize.Op.and]: [{
                                        $paymentStatus$: { in: [1]
                                        }
                                    }],
                                    subscriptionsId: subscriptionId,
                                    businessId: businessIdinformation,
                                    clientpaymentId: nextClientpayments[i].clientpaymentId
                                },
                                order: [
                                    ["payment_end_date", "ASC"]
                                ],
                                raw: true
                            });

                            if (i == 0) {
                                temp_cal_data = total_paid_amount_db + Number(singlePaidAmount);
                            }
                            if ((Number(updatenextClientpayments[0].due_amount) < temp_cal_data)) {
                                let update_client_payment = await dbclientpayments.clientpayments.update({
                                    payble_amount: Number(updatenextClientpayments[0].due_amount),
                                    paymentStatus: 2,
                                }, {
                                    where: {
                                        status: 1,
                                        businessId: businessId,
                                        subscriptionsId: subscriptionId,
                                        clientpaymentId: updatenextClientpayments[0].clientpaymentId
                                    }
                                });
                            }
                            if ((Number(updatenextClientpayments[0].due_amount) > temp_cal_data) && (temp_cal_data > 0)) {
                                let update_client_payment = await dbclientpayments.clientpayments.update({
                                    payble_amount: Number(temp_cal_data),
                                    paymentStatus: 1,
                                }, {
                                    where: {
                                        status: 1,
                                        businessId: businessId,
                                        subscriptionsId: subscriptionId,
                                        clientpaymentId: updatenextClientpayments[0].clientpaymentId
                                    }
                                });
                            }
                            if (Number(updatenextClientpayments[0].due_amount) == temp_cal_data) {
                                let update_client_payment = await dbclientpayments.clientpayments.update({
                                    payble_amount: Number(updatenextClientpayments[0].due_amount),
                                    paymentStatus: 2,
                                }, {
                                    where: {
                                        status: 1,
                                        businessId: businessId,
                                        subscriptionsId: subscriptionId,
                                        clientpaymentId: updatenextClientpayments[0].clientpaymentId
                                    }
                                });
                            }
                            temp_cal_data = temp_cal_data - Number(updatenextClientpayments[0].due_amount);
                        }
                    }

                } else {
                    if (calTotalPaid == 0) {
                        console.log("====================================================================");
                        console.log("====================================================================" + nextClientpayments.length);
                        console.log("====================================================================" + (Math.floor(total_paid_amount_db / singleDueAmount) - 1));
                        if ((singlePaidAmount != null) && (singlePaidAmount < singleDueAmount)) {
                            for (var i = 0; i < Math.floor(total_paid_amount_db / singleDueAmount); i++) {
                                if (i == (Math.floor(total_paid_amount_db / singleDueAmount) - 1)) {
                                    if (nextClientpayments[i + 1]) {
                                        let update_client_payment_last = await dbclientpayments.clientpayments.update({
                                            payble_amount: Number(total_paid_amount_db % singleDueAmount),
                                        }, {
                                            where: {
                                                status: 1,
                                                businessId: businessId,
                                                subscriptionsId: subscriptionId,
                                                clientpaymentId: nextClientpayments[i + 1].clientpaymentId
                                            }
                                        });
                                    } else {
                                        let update_client_payment_last = await dbclientpayments.clientpayments.update({
                                            payble_amount: Number(total_paid_amount_db % singleDueAmount),
                                        }, {
                                            where: {
                                                status: 1,
                                                businessId: businessId,
                                                subscriptionsId: subscriptionId,
                                                clientpaymentId: nextClientpayments[i].clientpaymentId
                                            }
                                        });
                                    }
                                }
                                let update_client_payment = await dbclientpayments.clientpayments.update({
                                    payble_amount: Number(singleDueAmount),
                                    paymentStatus: 2,
                                }, {
                                    where: {
                                        status: 1,
                                        businessId: businessId,
                                        subscriptionsId: subscriptionId,
                                        clientpaymentId: nextClientpayments[i].clientpaymentId
                                    }
                                });
                            }

                        }

                    } else {
                        console.log("====================================================================else" + nextClientpayments.length);
                        console.log("====================================================================elseLast" + Math.floor(total_paid_amount_db / singleDueAmount));
                        for (let i = 0; i < Math.floor(total_paid_amount_db / (singleDueAmount)); i++) {
                            console.log("================================================== i count" + i);
                            if (i == (Math.floor(total_paid_amount_db / (singleDueAmount)) - 1)) {
                                if (nextClientpayments[i + 1]) {
                                    let update_client_payment_last = await dbclientpayments.clientpayments.update({
                                        payble_amount: Number(total_paid_amount_db % (singleDueAmount)),
                                    }, {
                                        where: {
                                            status: 1,
                                            businessId: businessId,
                                            subscriptionsId: subscriptionId,
                                            clientpaymentId: nextClientpayments[i + 1].clientpaymentId
                                        }
                                    });
                                }
                            }

                            if (nextClientpayments[i]) {
                                let update_client_payment = await dbclientpayments.clientpayments.update({
                                    payble_amount: Number(singleDueAmount),
                                    paymentStatus: 2,
                                }, {
                                    where: {
                                        status: 1,
                                        businessId: businessId,
                                        subscriptionsId: subscriptionId,
                                        clientpaymentId: nextClientpayments[i].clientpaymentId
                                    }
                                });
                            }
                        }
                    }

                }
            }
            console.log("================================================================");
            let update_subscriptions = await dbsubscriptions.subscriptions.update({
                status_amountPaidSoFar: 0,
            }, {
                where: {
                    status: 1,
                    parentSubscriptionId: subscriptionId,
                }
            });
            console.log("================================================================");
            //inserting data into log table
            var getobj = {
                businessId: businessId,
                clientId: subscriptions[0].clientId
            };
            let clientData = await clientInfo(information, getobj);
            if (clientData[0].clientName) {
                var name = clientData[0].clientName;
            } else {
                var name = '';
            }
            var message = employees[0].name + ' just recorded a payment for ' + name;
            let header = 'Payment Received';

            const newLog = {
                businessId: businessId,
                activity: {
                    Status: 1,
                    header: header,
                    activityId: singlePaymentClientpayments[0].clientpaymentId,
                    activityName: 'update',
                    activityDate: singlePaymentClientpayments[0].updateDateTime.toLocaleDateString("en-US"),
                    activityTime: singlePaymentClientpayments[0].updateDateTime.toLocaleTimeString("en-US"),
                    message: message,
                    attendance: '',
                    payment: totalAmountPaid
                },
                referenceTable: 'clientPayment',
            };
            let log = await dbLog.log.create(newLog);
            //end of inserting data into log table

            let subscriptionDetails = await SubscriptionDB.details(information, subscriptionId);

            if (subscriptionDetails) {
                if (subscriptionDetails[0].getSubcriptionPaymentFormat.length > 0) {
                    let someArray = [];
                    for (let k = 0; k < subscriptionDetails[0].getSubcriptionPaymentFormat.length; k++) {
                        if (subscriptionDetails[0].getSubcriptionPaymentFormat[k].clientpaymentId === clientpaymentId) {
                            someArray.push(subscriptionDetails[0].getSubcriptionPaymentFormat[k]);
                        }
                    }
                    subscriptionDetails[0].getSubcriptionPaymentFormat = someArray;
                }
            }
            subscriptionDetails[0]['clientData'] = clientData;
            subscriptionDetails[0]['totalAmountPaid'] = totalAmountPaid;
            subscriptionDetails[0]['cheque_number'] = cheque_number;
            let new_data = '';
            let cal_new_amount = '';
            if (subscriptionDetails[0]["status_amountPaidSoFar"] == 1) {
                let date_check = (subscriptionDetails[0]['clientJoiningDate'] != null) ? subscriptionDetails[0]['clientJoiningDate'] : subscriptionDetails[0]['subscriptionDateTime'];
                cal_new_amount = await this.getTotalShowData(information, subscriptionDetails[0]['subscriptionId'], date_check);
                new_data = Number(cal_new_amount[0].new_balance) - Number(subscriptionDetails[0]["amountPaidSoFar"]);
            } else {
                let date_check = (subscriptionDetails[0]['clientJoiningDate'] != null) ? subscriptionDetails[0]['clientJoiningDate'] : subscriptionDetails[0]['subscriptionDateTime'];
                cal_new_amount = await this.getTotalShowData(information, subscriptionDetails[0]['subscriptionId'], date_check);
                new_data = Number(cal_new_amount[0].new_balance);
            }
            subscriptionDetails[0]["getTotalShowData"] = [{
                new_balance: new_data
            }];
            subscriptionDetails[0]["getTotalDue"] = await this.getTotalDue(information, subscriptionDetails[0]['subscriptionId']);
            let getTotalPaid = await this.getTotalPaid(information, subscriptionDetails[0]['subscriptionId']);
            if (subscriptionDetails[0]["status_amountPaidSoFar"] == '1') {
                getTotalPaid[0].total_paid_amount = Number(getTotalPaid[0].total_paid_amount);
            } else {
                getTotalPaid[0].total_paid_amount = Number(getTotalPaid[0].total_paid_amount) - Number(subscriptionDetails[0]["amountPaidSoFar"]);
            }
            subscriptionDetails[0]["getTotalPaid"] = getTotalPaid;
            var arr = [];
            var todayDate = new Date().toISOString().slice(0, 10);
            let next_payment_due_date = null;
            let pendingPaymentDate;
            let getsubscriptionsId = await dbclientpayments.clientpayments.findAll({
                attributes: ["subscriptionsId"],
                group: ["subscriptionsId"],
                where: {
                    status: 1,
                    clientpaymentId: clientpaymentId
                },
                raw: true
            });
            if (getsubscriptionsId.length > 0) {
                pendingPaymentDate = await dbclientpayments.clientpayments.findAll({
                    attributes: ["payment_due_date"],
                    order: [
                        ["payment_due_date", "ASC"]
                    ],
                    where: {
                        paymentStatus: 1,
                        subscriptionsId: getsubscriptionsId[0].subscriptionsId
                    },
                    raw: true
                });
            }
            if (pendingPaymentDate) {
                for (var prop in pendingPaymentDate) {
                    if (pendingPaymentDate.hasOwnProperty(prop)) {
                        let innerObj = {};
                        innerObj = pendingPaymentDate[prop]["payment_due_date"];
                        arr.push(innerObj);
                    }
                }
            }

            function getDateNextAndPrevious(array, val, dir) {
                for (var i = 0; i < array.length; i++) {
                    if (dir == true) {
                        if (new Date(array[i]) > new Date(val)) {
                            return array[i - 1] || 0;
                        }
                    } else {
                        if (new Date(array[i]) >= new Date(val)) {
                            return array[i];
                        }
                    }
                }
            }
            subscriptionDetails[0]["next_payment_due_date"] = (getDateNextAndPrevious(arr, todayDate)) ? getDateNextAndPrevious(arr, todayDate) : next_payment_due_date;


            console.log(JSON.stringify(subscriptionDetails));

            let update_paymentTransactions = await dbpaymentTransactions.paymentTransactions.update({
                payment_content: subscriptionDetails,
            }, {
                where: {
                    status: 1,
                    paymentTransactionId: insertPaymentTransactions.dataValues.paymentTransactionId,
                }
            });

            if (clientData) {
                if (clientData[0].contactNumber) {
                    await sendSmsMobile(clientData[0].contactNumber, message);
                }

                if (clientData[0].emailId) {
                    var content = makeEmailTemplate(
                        "src/emailTemplate/ClientreportTemplate.js",
                        subscriptionDetails
                    );
                    const filenameRender = subscriptionDetails[0].getSubcriptionPaymentFormat[0].clientpaymentId + '__' + Date.now();
                    fs.writeFile(
                        "uploads/paymentReceived/html/" + filenameRender + ".html",
                        content,
                        function(err) {
                            if (err) {
                                return console.log(err);
                            } else {
                                var html = fs.readFileSync(
                                    "uploads/paymentReceived/html/" + filenameRender + ".html",
                                    "utf8"
                                );
                                pdf1
                                    .create(html, {
                                        format: "Letter"
                                    })
                                    .toFile(
                                        "uploads/paymentReceived/content/" + filenameRender + ".pdf",
                                        function(err, res) {
                                            if (err) return console.log(err);
                                        }
                                    );
                            }
                        }
                    );
                    await sendEmail(
                        clientData[0].emailId,
                        "PAYMENT INVOICE ",
                        "INVOICE", {
                            filename: filenameRender + ".pdf",
                            path: "uploads/paymentReceived/content/" + filenameRender + ".pdf",
                            contentType: "application/pdf"
                        }
                    );
                }
            }
            let dataForPushNotification = {
                title: header,
                body: message,
                url: ''
            };
            await sendPushNotification(businessId, dataForPushNotification);
            dbpaymentTransactions.sequelize.close();
            dbclientpayments.sequelize.close();
            dbservicepricepackmap.sequelize.close();
            dbservices.sequelize.close();
            dbpricepacks.sequelize.close();
            dbsubscriptions.sequelize.close();
            dbLog.sequelize.close();
            dbemployees.sequelize.close();
            return true;
        } catch (error) {
            throw error;
        }
    };


    getTotalShowData = async(
        information,
        subscriptionsId,
        date
    ) => {
        try {
            let businessIdinformation = information.split(",")[1];
            let dbsubscriptions = createInstance(["clientSubscription"], information);
            let todayDate = await hyphenDateFormat(date);
            // let todayDate = '2018-07-04';
            if (subscriptionsId) {
                console.log("============================================================")
            }
            let data = await dbsubscriptions.sequelize.query(
                "SELECT (SUM(`due_amount`) - CASE WHEN SUM(`payble_amount`) is NULL THEN 0 ELSE SUM(`payble_amount`) END) AS `new_balance` FROM `clientpayments` WHERE `payment_due_date` <= '" + todayDate + "' AND `status` = '1' AND `subscriptionsId` = '" + subscriptionsId + "'", {
                    type: dbsubscriptions.sequelize.QueryTypes.SELECT
                }
            );
            if (subscriptionsId) {
                console.log("============================================================")
            }
            dbsubscriptions.sequelize.close();
            return data;
        } catch (error) {
            throw error;
        }
    };

    //Dashboard
    listAll = async(
        information,
        businessId,
        typeid,
        filterPrivateFields = true
    ) => {
        if (typeid != 1 && typeid != 2) {
            throw new ApplicationError("Invalid Parmas.", 422);
        } else {
            try {
                let clienttotalpricedb = createInstance(
                    ["clientPayments"],
                    information
                );
                var ts;
                let clienttotalprice = await clienttotalpricedb.clientpayments
                    .sum("totalAmountPaid")
                    .then(sum => {
                        ts = sum;
                    });

                // Total Expenses
                let db = createInstance(["clientExpense"], information);
                let expense = await db.clientexpense.findAll({
                    where: {
                        status: 1,
                        businessId: businessId
                    },
                    raw: true
                });

                var totalexpense = 0;
                for (var i = 0; i < expense.length; i++) {
                    totalexpense += expense[i].amount;
                }

                var new_data1 = Array();
                var new_data2 = Array();

                var show_payments = [];
                var seqdb = new Sequelize(businessId, KNACKDB.user, KNACKDB.password, {
                    host: KNACKDB.host,
                    port: 3306,
                    dialect: "mysql",
                    pool: {
                        max: 100,
                        min: 0,
                        idle: 10000
                    }
                });
                var paymentStatusText = typeid == 2 ? "Complete" : "Pending";
                show_payments = await seqdb.query(
                    "select subscriptionsId,SUM(totalAmountPaid) AS Amount from clientpayments where businessId='" +
                    businessId +
                    // "' and paymentStatus='" +
                    // typeid +
                    "' GROUP BY subscriptionsId", {
                        type: Sequelize.QueryTypes.SELECT
                    }
                );
                // Client Subscriptions Payment Info

                let clientTotalPaymentdb = createInstance(
                    ["clientPayments"],
                    information
                );

                var days = 45;
                var daysPast = 45;
                var today = new Date();
                var futureDate = new Date(today.getTime() + days * 24 * 60 * 60 * 1000);
                var pastDate = new Date(
                    today.getTime() - daysPast * 24 * 60 * 60 * 1000
                );

                for (var a = 0; a < show_payments.length; a++) {
                    let clientTotalPayment = await clientTotalPaymentdb.clientpayments.findAll({
                        where: {
                            status: 1,
                            paymentStatus: 1,
                            businessId: businessId,
                            subscriptionsId: show_payments[a].subscriptionsId,
                            paymentDueDate: {
                                between: [today, futureDate]
                            }
                        },
                        raw: true
                    });

                    if (clientTotalPayment.length > 0) {
                        // Pricepack Info
                        let pricepack_db = createInstance(["clientPricepack"], information);
                        let service_pack_price = await pricepack_db.pricepacks.findAll({
                            where: {
                                status: 1,
                                businessId: businessId,
                                pricepackId: clientTotalPayment[0].pricepacks
                            },
                            raw: true
                        });
                        //Subscription Info
                        let db = createInstance(["clientSubscription"], information);
                        let subscriptions = await db.subscriptions.findAll({
                            where: {
                                status: 1,
                                //businessId: businessId
                                subscriptionId: clientTotalPayment[0].subscriptionsId
                            },
                            raw: true
                        });
                        // Client Info
                        let clientdb = createInstance(["client"], information);
                        let clientInfo = await clientdb.clients.findAll({
                            where: {
                                status: 1,
                                businessId: businessId,
                                clientId: subscriptions[0].clientId
                            },
                            raw: true
                        });

                        var pricepack_amount = service_pack_price[0].amount;
                        var pending_amount = parseFloat(
                            pricepack_amount - show_payments[a].Amount
                        ).toFixed(2);
                        var clientphoto =
                            KNACK_UPLOAD_URL.localUrl + clientInfo[0].photoUrl;
                        new_data1[a] = {
                            clientpaymentId: clientTotalPayment[0].clientpaymentId,
                            subscriptionsId: clientTotalPayment[0].subscriptionsId,

                            clientId: clientInfo[0].clientId,
                            clientName: clientInfo[0].clientName,
                            clientPhotoUrl: clientphoto,

                            pricepackId: service_pack_price[0].pricepackId,
                            pricepackName: service_pack_price[0].pricepackName,
                            pricePackAmount: pricepack_amount,
                            totalSessionUnit: subscriptions[0].totalSessionUnit,
                            totalSessionType: subscriptions[0].totalSessionType,
                            subscriptionDateTime: subscriptions[0].subscriptionDateTime,
                            totalSessionsAttended: subscriptions[0].totalSessionsAttended,

                            totalAmountPaid: show_payments[a].Amount.toFixed(2),
                            pendingAmount: pending_amount,
                            numberOfInstallments: clientTotalPayment[0].numberOfInstallments,
                            installmentFrequency: clientTotalPayment[0].installment_frequency,
                            paymentStatusType: typeid,
                            paymentStatus: paymentStatusText
                                //listallsub: list_sub
                        };
                    }
                }

                var lastPayment = [];
                for (var a = 0; a < show_payments.length; a++) {
                    var clientTotalPayment = await clientTotalPaymentdb.clientpayments.findAll({
                        order: [
                            ["payment_date", "DESC"]
                        ],
                        where: {
                            status: 1,
                            businessId: businessId,
                            subscriptionsId: show_payments[a].subscriptionsId
                                // payment_date: {
                                //   between: [pastDate, today]
                                // }
                        },
                        raw: true
                    });

                    if (clientTotalPayment.length) {
                        lastPayment.push(clientTotalPayment[0].totalAmountPaid);
                        // var list_sub =
                        //   show_payments[a].subscriptionsId ==
                        //     clientTotalPayment[0].subscriptionsId
                        //     ? clientTotalPayment
                        //     : "";

                        // Pricepack Info
                        let pricepack_db = createInstance(["clientPricepack"], information);
                        let service_pack_price = await pricepack_db.pricepacks.findAll({
                            where: {
                                status: 1,
                                businessId: businessId,
                                pricepackId: clientTotalPayment[0].pricepacks
                            },
                            raw: true
                        });
                        //Subscription Info
                        let db = createInstance(["clientSubscription"], information);
                        let subscriptions = await db.subscriptions.findAll({
                            where: {
                                status: 1,
                                //businessId: businessId
                                subscriptionId: show_payments[a].subscriptionsId
                            },
                            raw: true
                        });
                        // Client Info
                        let clientdb = createInstance(["client"], information);
                        let clientInfo = await clientdb.clients.findAll({
                            where: {
                                status: 1,
                                businessId: businessId,
                                clientId: subscriptions[0].clientId
                            },
                            raw: true
                        });

                        var pricepack_amount = service_pack_price[0].amount;
                        var pending_amount = parseFloat(
                            pricepack_amount - show_payments[a].Amount
                        ).toFixed(2);
                        var clientphoto =
                            KNACK_UPLOAD_URL.localUrl + clientInfo[0].photoUrl;
                        new_data2[a] = {
                            clientpaymentId: clientTotalPayment[0].clientpaymentId,
                            subscriptionsId: clientTotalPayment[0].subscriptionsId,

                            clientId: clientInfo[0].clientId,
                            clientName: clientInfo[0].clientName,
                            clientPhotoUrl: clientphoto,

                            pricepackId: service_pack_price[0].pricepackId,
                            pricepackName: service_pack_price[0].pricepackName,
                            pricePackAmount: pricepack_amount,
                            totalSessionUnit: subscriptions[0].totalSessionUnit,
                            totalSessionType: subscriptions[0].totalSessionType,
                            subscriptionDateTime: subscriptions[0].subscriptionDateTime,
                            totalSessionsAttended: subscriptions[0].totalSessionsAttended,

                            totalAmountPaid: show_payments[a].Amount.toFixed(2),
                            pendingAmount: pending_amount,
                            numberOfInstallments: clientTotalPayment[0].numberOfInstallments,
                            installmentFrequency: clientTotalPayment[0].installment_frequency,
                            paymentStatusType: typeid,
                            paymentStatus: paymentStatusText
                                //listallsub: list_sub
                        };
                    }
                }

                seqdb.close();
                return [{
                        Transcation: [{
                            Incomes: ts,
                            Expenses: totalexpense
                        }]
                    },
                    {
                        Payments: {
                            paymentReminders: new_data1,
                            paymentHistory: new_data2
                        }
                    }
                ];
                // return new_data;
                // return await new_data;
                // if (filterPrivateFields) {
                //   return await filterFields(subscriptions, PUBLIC_FIELDS);
                // }
            } catch (error) {
                throw error;
            }
        }
    };

    //Payment Reminders
    // listPaymentReminders = async(
    //     information,
    //     params,
    //     filterPrivateFields = true
    // ) => {
    //     try {
    //         var businessIdinformation = information.split(",")[1];
    //         let dbclientpayments = createInstance(["clientPayments"], information);
    //         let dbClients = createInstance(["client"], information);
    //         let dbsubscriptions = createInstance(["clientSubscription"], information);
    //         let dbpricepack = createInstance(["clientPricepack"], information);
    //         // let todayDate = await hyphenDateFormat(new Date);
    //         let todayDate = '2018-05-20';

    //         dbclientpayments.clientpayments.belongsTo(dbsubscriptions.subscriptions, {
    //             foreignKey: "subscriptionsId",
    //             targetKey: "subscriptionId"
    //         });
    //         dbsubscriptions.subscriptions.belongsTo(dbClients.clients, {
    //             foreignKey: "clientId",
    //             targetKey: "clientId"
    //         });
    //         dbsubscriptions.subscriptions.belongsTo(dbpricepack.pricepacks, {
    //             foreignKey: "pricepacks",
    //             targetKey: "pricepackId"
    //         });
    //         let filter = (params[0]) ? params[0] : '';
    //         var pageNo = (params['page']) ? params['page'] : 1;
    //         var limitPerPage = PEGINATION_LIMIT;
    //         var offsetLimit = (pageNo - 1) * limitPerPage;
    //         let orderBy;
    //         if ((params["order"] == 1)) {
    //             orderBy = [
    //                 ["CreateDateTime", "DESC"]
    //             ];
    //         } else {
    //             orderBy = [
    //                 ["CreateDateTime", "ASC"]
    //             ];
    //         }
    //         let itemConditionsclientpayments = {
    //             status: 1,
    //             paymentStatus: 1,
    //             businessId: businessIdinformation,
    //             [dbClients.sequelize.Op.or]: [{
    //                     "$subscription.client.clientName$": {
    //                         like: "%" + filter + "%"
    //                     }
    //                 },
    //                 {
    //                     "$subscription.pricepack.pricepackName$": {
    //                         like: "%" + filter + "%"
    //                     }
    //                 }
    //             ],
    //             [dbClients.sequelize.Op.or]: [{
    //                     "$payment_due_date$": {
    //                         eq: todayDate,
    //                     },
    //                     "$payment_end_date$": {
    //                         eq: todayDate,
    //                     }
    //                 },
    //                 {
    //                     "$payment_due_date$": {
    //                         lte: todayDate,
    //                     },
    //                     "$payment_end_date$": {
    //                         gte: todayDate,
    //                     }
    //                 }
    //             ]
    //         };
    //         let itemGroupConditions = {
    //             status: 1,
    //             businessId: businessIdinformation
    //         };
    //         let itemGroupConditionsPricePack = {};
    //         let itemConditions = {};
    //         let paymentData = await dbclientpayments.clientpayments.findAndCount({
    //             attributes: ["payment_due_date"],
    //             include: [{
    //                 model: dbsubscriptions.subscriptions,
    //                 attributes: ["installment_frequency", "subscriptionId", "subscriptionDateTime", "clientJoiningDate"],
    //                 include: [{
    //                         model: dbClients.clients,
    //                         attributes: ["clientId"],
    //                         where: itemGroupConditions,
    //                     },
    //                     {
    //                         model: dbpricepack.pricepacks,
    //                         attributes: ["pricepackName"],
    //                         where: itemGroupConditionsPricePack,
    //                     }
    //                 ],
    //                 where: itemConditions,
    //             }],
    //             order: orderBy,
    //             where: itemConditionsclientpayments,
    //             subQuery: false,
    //             offset: offsetLimit,
    //             limit: limitPerPage,
    //             raw: true
    //         });
    //         dbclientpayments.sequelize.close();
    //         dbClients.sequelize.close();
    //         dbsubscriptions.sequelize.close();
    //         dbpricepack.sequelize.close();
    //         let final = paymentData;
    //         return final;
    //     } catch (error) {
    //         throw error;
    //     }
    // };

    //Payment Reminders
    listPaymentReminders = async(
        information,
        params,
        filterPrivateFields = true
    ) => {
        try {
            var mergedAllActiveClients = [];
            let todayDate = await hyphenDateFormat(new Date);
            // let todayDate = '2018-07-04';
            var businessIdinformation = information.split(",")[1];
            let dbclientpayments = createInstance(["clientPayments"], information);
            let dbsubscriptions = createInstance(["clientSubscription"], information);
            let dbClients = createInstance(["client"], information);
            let dbpricepack = createInstance(["clientPricepack"], information);
            let dbclientexpense = createInstance(["clientExpense"], information);
            let dbpaymentTransactions = createInstance(["paymentTransactions"], information);
            let dbclientScheduleMap = createInstance(["clientScheduleMap"], information);
            dbclientpayments.clientpayments.belongsTo(dbsubscriptions.subscriptions, {
                foreignKey: "subscriptionsId",
                targetKey: "subscriptionId"
            });
            dbsubscriptions.subscriptions.belongsTo(dbClients.clients, {
                foreignKey: "clientId",
                targetKey: "clientId"
            });
            dbsubscriptions.subscriptions.belongsTo(dbpricepack.pricepacks, {
                foreignKey: "pricepacks",
                targetKey: "pricepackId"
            });
            dbclientpayments.clientpayments.belongsTo(dbpaymentTransactions.paymentTransactions, {
                foreignKey: "clientpaymentId",
                targetKey: "clientpaymentId"
            });
            dbsubscriptions.subscriptions.belongsTo(dbclientScheduleMap.clientschedulemap, {
                foreignKey: "subscriptionId",
                targetKey: "subscriptionId"
            });

            let filter = params[0] ? params[0] : "";
            var pageNo = params["page"] ? params["page"] : 1;
            var limitPerPage = PEGINATION_LIMIT;
            var offsetLimit = (pageNo - 1) * limitPerPage;

            let clientpaymentsRiminder = {};
            let recentIncomeclientpayments = {};
            let recentExpenseclientExpense = {};

            let itemclientpaymentsRiminderData = {
                status: 1,
                businessId: businessIdinformation,
                clientpaymentId: {
                    [dbclientpayments.sequelize.Op.in]:
                    // [Sequelize.literal("(SELECT MIN(`clientpaymentId`) FROM `clientpayments` WHERE `payment_due_date` >= '" + todayDate + "' AND `payment_end_date` >= '" + todayDate + "' AND `status` = '1' GROUP BY `subscriptionsId`)")]
                        [Sequelize.literal("(SELECT MIN(`clientpaymentId`) as `clientpaymentId` FROM `clientpayments` INNER JOIN `subscriptions` AS `subscription` ON `clientpayments`.`subscriptionsId` = `subscription`.`subscriptionId` INNER JOIN `pricepacks` AS `subscription->pricepack` ON `subscription`.`pricepacks` = `subscription->pricepack`.`pricepackId` WHERE Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day ) <= '" + todayDate + "' OR ( ( `clientpayments`.`payment_due_date` <= '" + todayDate + "' AND `clientpayments`.`payment_end_date` >= '" + todayDate + "' ) OR ( `clientpayments`.`payment_due_date` >= '" + todayDate + "' AND `clientpayments`.`payment_end_date` >= '" + todayDate + "' ) ) AND `clientpayments`.`status` = '1' GROUP BY `clientpayments`.`subscriptionsId` UNION select c.clientpaymentId from clientpayments as c, ( SELECT a.clientpaymentId, a.curent_subcription_number, a.payment_end_date, COUNT(a.subscriptionsId) FROM clientpayments a LEFT JOIN clientpayments b ON a.subscriptionsId = b.subscriptionsId AND b.status = 1 WHERE b.status = 1 AND a.status = 1 GROUP BY a.subscriptionsId, a.clientpaymentId having a.curent_subcription_number = COUNT(a.subscriptionsId) AND a.payment_end_date < CURDATE() ) as total WHERE c.clientpaymentId IN (total.clientpaymentId))")]
                },
                [dbClients.sequelize.Op.or]: [{
                        "$subscription.client.clientName$": {
                            like: "%" + filter + "%"
                        }
                    },
                    {
                        "$subscription.pricepack.pricepackName$": {
                            like: "%" + filter + "%"
                        }
                    }
                ],
                [dbclientScheduleMap.sequelize.Op.and]: [Sequelize.literal("( ( `subscription->clientschedulemap`.`clientScheduleMapId` IS NULL OR `subscription->clientschedulemap`.`sessionCount` IS NULL ) OR ( `subscription->clientschedulemap`.`sessionCount` < `subscription->pricepack`.`serviceDuration` ) )")],
            };
            let itemGroupConditions = {
                status: 1,
                businessId: businessIdinformation,
            };
            let clientpaymentsRiminderData = await dbclientpayments.clientpayments.findAndCount({
                attributes: [
                    "clientpaymentId",
                    "subscriptionsId",
                    "due_amount",
                    "payble_amount",
                    "payment_due_date",
                    "payment_end_date", ["UpdateDateTime", "updated_at"],
                    "paymentStatus", [dbclientpayments.sequelize.literal("DATE(Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day ))"), 'subcription_end_Date'],
                    "paymentReminder_status",
                    "paymentReminder_date",
                ],
                include: [{
                    model: dbsubscriptions.subscriptions,
                    attributes: [
                        "clientId",
                        "amountPaidSoFar",
                        "status_amountPaidSoFar",
                        "clientJoiningDate",
                        "subscriptionDateTime",
                        "installment_frequency",
                    ],
                    include: [{
                            model: dbClients.clients,
                            attributes: {
                                include: [
                                    "clientName", [
                                        dbClients.clients.sequelize.literal(
                                            'CASE WHEN photoUrl  is NULL THEN CONCAT("' +
                                            KNACK_UPLOAD_URL.localUrl +
                                            '" , "noimage.jpg" ) ELSE CONCAT("' +
                                            KNACK_UPLOAD_URL.localUrl +
                                            '" , photoUrl ) END'
                                        ),
                                        "photoUrl"
                                    ],
                                ],
                                exclude: []
                            },
                            where: itemGroupConditions
                        },
                        {
                            model: dbpricepack.pricepacks,
                            attributes: ["pricepackName"],
                            where: {}
                        },
                        {
                            model: dbclientScheduleMap.clientschedulemap,
                            required: false,
                            attributes: ["scheduleId", "sessionCount"],
                            where: {
                                // [dbclientScheduleMap.sequelize.Op.and]: [Sequelize.literal("`subscription->clientschedulemap`.`scheduleId`= `subscription->clientschedulemap`.`parentScheduleId`")],
                                [dbclientScheduleMap.sequelize.Op.and]: [Sequelize.literal("( `subscription->clientschedulemap`.`scheduleId` = `subscription->clientschedulemap`.`parentScheduleId` ) AND ( `subscription->clientschedulemap`.`scheduleId` = (select min(`scheduleId`) from `clientschedulemap` `inSched` where `inSched`.`subscriptionId` = `subscription`.`subscriptionId` ) )")],
                            }
                        }
                    ],
                    where: {}
                }],
                order: [
                    ["CreateDateTime", "DESC"]
                ],
                where: itemclientpaymentsRiminderData,
                subQuery: false,
                offset: offsetLimit,
                limit: limitPerPage,
                raw: true
            });
            if (clientpaymentsRiminderData.rows.length > 0) {
                for (let x = 0; x < clientpaymentsRiminderData.rows.length; x++) {
                    clientpaymentsRiminderData.rows[x]["sessionInfo"] = await sessionInfo(information, {
                        status: 1,
                        subscriptionId: clientpaymentsRiminderData.rows[x]['subscriptionsId']
                    });
                    if (clientpaymentsRiminderData.rows[x]["subscription.installment_frequency"] == '6') {
                        let new_data = '';
                        let cal_new_amount = '';
                        if (clientpaymentsRiminderData.rows[x]["subscription.status_amountPaidSoFar"] == 1) {
                            let date_check = (clientpaymentsRiminderData.rows[x]['subscription.clientJoiningDate'] != null) ? clientpaymentsRiminderData.rows[x]['subscription.clientJoiningDate'] : clientpaymentsRiminderData.rows[x]['subscription.subscriptionDateTime'];
                            cal_new_amount = await this.getTotalShowDataOthers(information, clientpaymentsRiminderData.rows[x]['subscriptionsId'], date_check);
                            new_data = Number(cal_new_amount[0].new_balance) - Number(clientpaymentsRiminderData.rows[x]["subscription.amountPaidSoFar"]);
                        } else {
                            let date_check = await hyphenDateFormat(new Date());
                            cal_new_amount = await this.getTotalShowDataOthers(information, clientpaymentsRiminderData.rows[x]['subscriptionsId'], date_check);
                            new_data = Number(cal_new_amount[0].new_balance);
                        }
                        clientpaymentsRiminderData.rows[x]["getTotalShowData"] = [{
                            new_balance: new_data
                        }];
                        clientpaymentsRiminderData.rows[x]["getTotalDue"] = await this.getTotalDue(information, clientpaymentsRiminderData.rows[x]['subscriptionsId']);
                        let getTotalPaid = await this.getTotalPaid(information, clientpaymentsRiminderData.rows[x]['subscriptionsId']);
                        if (clientpaymentsRiminderData.rows[x]["subscription.status_amountPaidSoFar"] == '1') {
                            getTotalPaid[0].total_paid_amount = Number(getTotalPaid[0].total_paid_amount);
                        } else {
                            getTotalPaid[0].total_paid_amount = Number(getTotalPaid[0].total_paid_amount) - Number(clientpaymentsRiminderData.rows[x]["subscription.amountPaidSoFar"]);
                        }
                        clientpaymentsRiminderData.rows[x]["getTotalPaid"] = getTotalPaid;
                    } else {
                        let new_data = '';
                        let cal_new_amount = '';
                        if (clientpaymentsRiminderData.rows[x]["subscription.status_amountPaidSoFar"] == 1) {
                            let date_check = (clientpaymentsRiminderData.rows[x]['subscription.clientJoiningDate'] != null) ? clientpaymentsRiminderData.rows[x]['subscription.clientJoiningDate'] : clientpaymentsRiminderData.rows[x]['subscription.subscriptionDateTime'];
                            cal_new_amount = await this.getTotalShowData(information, clientpaymentsRiminderData.rows[x]['subscriptionsId'], date_check);
                            new_data = Number(cal_new_amount[0].new_balance) - Number(clientpaymentsRiminderData.rows[x]["subscription.amountPaidSoFar"]);
                        } else {
                            // let date_check = (clientpaymentsRiminderData.rows[x]['subscription.clientJoiningDate'] != null) ? clientpaymentsRiminderData.rows[x]['subscription.clientJoiningDate'] : clientpaymentsRiminderData.rows[x]['subscription.subscriptionDateTime'];
                            let date_check = clientpaymentsRiminderData.rows[x]['payment_end_date'];
                            cal_new_amount = await this.getTotalShowData(information, clientpaymentsRiminderData.rows[x]['subscriptionsId'], date_check);
                            new_data = Number(cal_new_amount[0].new_balance);
                        }
                        clientpaymentsRiminderData.rows[x]["getTotalShowData"] = [{
                            new_balance: new_data
                        }];
                        clientpaymentsRiminderData.rows[x]["getTotalDue"] = await this.getTotalDue(information, clientpaymentsRiminderData.rows[x]['subscriptionsId']);
                        let getTotalPaid = await this.getTotalPaid(information, clientpaymentsRiminderData.rows[x]['subscriptionsId']);
                        if (clientpaymentsRiminderData.rows[x]["subscription.status_amountPaidSoFar"] == '1') {
                            getTotalPaid[0].total_paid_amount = Number(getTotalPaid[0].total_paid_amount);
                        } else {
                            getTotalPaid[0].total_paid_amount = Number(getTotalPaid[0].total_paid_amount) - Number(clientpaymentsRiminderData.rows[x]["subscription.amountPaidSoFar"]);
                        }
                        clientpaymentsRiminderData.rows[x]["getTotalPaid"] = getTotalPaid;

                    }
                    let listclientPayments = await dbclientpayments.clientpayments.findAll({
                        where: {
                            businessId: businessIdinformation,
                            subscriptionsId: clientpaymentsRiminderData.rows[x]['subscriptionsId'],
                            status: 1,
                            "$payble_amount$": {
                                ne: null
                            }
                        },
                        raw: true
                    });
                    clientpaymentsRiminderData.rows[x]["subcription_payment_status"] = listclientPayments.length;
                }
            }
            clientpaymentsRiminder.count = clientpaymentsRiminderData.count;
            clientpaymentsRiminder.rows = clientpaymentsRiminderData.rows;
            return await clientpaymentsRiminder;
        } catch (error) {
            throw error;
        }
    };





    getTotalDue = async(
        information,
        subscriptionsId
    ) => {
        try {

            let businessIdinformation = information.split(",")[1];
            let dbsubscriptions = createInstance(["clientSubscription"], information);
            let todayDate = await hyphenDateFormat(new Date);
            let data = await dbsubscriptions.sequelize.query(
                "SELECT SUM(`due_amount`) as `total_current_due` FROM `clientpayments` WHERE `subscriptionsId` = '" + subscriptionsId + "' AND `paymentStatus` = '1' AND ( ( `payment_due_date` = '" + todayDate + "' AND `payment_end_date` = '" + todayDate + "' ) OR ( `payment_due_date` <= '" + todayDate + "' ) )", {
                    type: dbsubscriptions.sequelize.QueryTypes.SELECT
                }
            );
            dbsubscriptions.sequelize.close();
            return data;
        } catch (error) {
            throw error;
        }
    };


    getTotalPaid = async(
        information,
        subscriptionsId
    ) => {
        try {
            let businessIdinformation = information.split(",")[1];
            let dbsubscriptions = createInstance(["clientSubscription"], information);
            let data = await dbsubscriptions.sequelize.query(
                "SELECT SUM(`payble_amount`) as `total_paid_amount` FROM `clientpayments` WHERE `subscriptionsId` = '" + subscriptionsId + "'", {
                    type: dbsubscriptions.sequelize.QueryTypes.SELECT
                }
            );
            dbsubscriptions.sequelize.close();
            return data;
        } catch (error) {
            throw error;
        }
    };

    /////
    // clientPaymentRemindersList = async(
    //     information,
    //     clientId,
    //     params,
    //     filterPrivateFields = true
    // ) => {
    //     try {

    //         var businessIdinformation = information.split(",")[1];
    //         let dbpaymentTransactions = createInstance(["paymentTransactions"], information);
    //         let dbclientpayments = createInstance(["clientPayments"], information);
    //         let dbClients = createInstance(["client"], information);
    //         let dbsubscriptions = createInstance(["clientSubscription"], information);
    //         let dbpricepack = createInstance(["clientPricepack"], information);
    //         let todayDate = await hyphenDateFormat(new Date);
    //         // let todayDate = "2018-05-23";
    //         dbclientpayments.clientpayments.belongsTo(dbsubscriptions.subscriptions, {
    //             foreignKey: "subscriptionsId",
    //             targetKey: "subscriptionId"
    //         });
    //         dbsubscriptions.subscriptions.belongsTo(dbClients.clients, {
    //             foreignKey: "clientId",
    //             targetKey: "clientId"
    //         });
    //         dbsubscriptions.subscriptions.belongsTo(dbpricepack.pricepacks, {
    //             foreignKey: "pricepacks",
    //             targetKey: "pricepackId"
    //         });
    //         dbclientpayments.clientpayments.belongsTo(dbpaymentTransactions.paymentTransactions, {
    //             foreignKey: "clientpaymentId",
    //             targetKey: "clientpaymentId"
    //         });
    //         let filter = params[0] ? params[0] : "";
    //         var pageNo = params["page"] ? params["page"] : 1;
    //         var limitPerPage = PEGINATION_LIMIT;
    //         var offsetLimit = (pageNo - 1) * limitPerPage;
    //         let orderBy;
    //         if (params["order"] == 1) {
    //             orderBy = [
    //                 ["CreateDateTime", "DESC"]
    //             ];
    //         } else {
    //             orderBy = [
    //                 ["CreateDateTime", "ASC"]
    //             ];
    //         }
    //         let itemConditionsclientpayments = {
    //             status: 1,
    //             paymentStatus: 1,
    //             businessId: businessIdinformation,
    //             clientpaymentId: {
    //                 [dbclientpayments.sequelize.Op.in]: [Sequelize.literal("(SELECT MIN(`clientpaymentId`) FROM `clientpayments` WHERE `payment_due_date` >= '" + todayDate + "' AND `payment_end_date` <= '" + todayDate + "' AND `status` = '1' AND `paymentStatus` = '1' GROUP BY `subscriptionsId`)")]
    //             },
    //             [dbClients.sequelize.Op.and]: [{
    //                     "$subscription.client.clientName$": {
    //                         like: "%" + filter + "%"
    //                     }
    //                 },
    //                 {
    //                     "$subscription.pricepack.pricepackName$": {
    //                         like: "%" + filter + "%"
    //                     }
    //                 }
    //             ],
    //             // [dbClients.sequelize.Op.or]: [{
    //             //         $payment_due_date$: {
    //             //             eq: todayDate
    //             //         },
    //             //         $payment_end_date$: {
    //             //             eq: todayDate
    //             //         }
    //             //     },
    //             //     {
    //             //         $payment_due_date$: {
    //             //             lte: todayDate
    //             //         },
    //             //         $payment_end_date$: {
    //             //             gte: todayDate
    //             //         }
    //             //     },
    //             //     {
    //             //         $payment_type$: {
    //             //             eq: 1
    //             //         }
    //             //     }
    //             // ],
    //         };
    //         let itemGroupConditions = {
    //             status: 1,
    //             businessId: businessIdinformation
    //         };
    //         let itemGroupConditionsPricePack = {};
    //         let itemConditions = {
    //             clientId: clientId
    //         };
    //         let paymentData = await dbclientpayments.clientpayments.findAndCount({
    //             attributes: [
    //                 "clientpaymentId",
    //                 "subscriptionsId",
    //                 "due_amount",
    //                 "payble_amount",
    //                 "payment_due_date",
    //                 "paymentReminder_status",
    //                 "payment_end_date", ["UpdateDateTime", "updated_at"],
    //                 [dbclientpayments.sequelize.literal("Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day )"), 'subcription_end_Date']
    //             ],
    //             include: [{
    //                 model: dbsubscriptions.subscriptions,
    //                 attributes: ["clientId"],
    //                 include: [{
    //                         model: dbClients.clients,
    //                         attributes: {
    //                             include: [
    //                                 "clientName", [
    //                                     dbClients.clients.sequelize.literal(
    //                                         'CASE WHEN photoUrl  is NULL THEN CONCAT("' +
    //                                         KNACK_UPLOAD_URL.localUrl +
    //                                         '" , "noimage.jpg" ) ELSE CONCAT("' +
    //                                         KNACK_UPLOAD_URL.localUrl +
    //                                         '" , photoUrl ) END'
    //                                     ),
    //                                     "photoUrl"
    //                                 ],
    //                             ],
    //                             exclude: []
    //                         },
    //                         where: itemGroupConditions
    //                     },
    //                     {
    //                         model: dbpricepack.pricepacks,
    //                         attributes: ["pricepackName"],
    //                         where: itemGroupConditionsPricePack
    //                     }
    //                 ],
    //                 where: itemConditions
    //             }],
    //             order: orderBy,
    //             where: itemConditionsclientpayments,
    //             subQuery: false,
    //             offset: offsetLimit,
    //             limit: limitPerPage,
    //             raw: true
    //         });
    //         if (paymentData.rows.length > 0) {
    //             for (let x = 0; x < paymentData.rows.length; x++) {
    //                 paymentData.rows[x]["sessionInfo"] = await sessionInfo(information, {
    //                     status: 1,
    //                     subscriptionId: paymentData.rows[x]['subscriptionsId']
    //                 });
    //                 paymentData.rows[x]["getTotalDue"] = await this.getTotalDue(information, paymentData.rows[x]['subscriptionsId']);
    //                 paymentData.rows[x]["getTotalPaid"] = await this.getTotalPaid(information, paymentData.rows[x]['subscriptionsId']);
    //             }
    //         }
    //         dbclientpayments.sequelize.close();
    //         dbClients.sequelize.close();
    //         dbsubscriptions.sequelize.close();
    //         dbpricepack.sequelize.close();
    //         dbpaymentTransactions.sequelize.close();
    //         dbclientpayments.sequelize.close();
    //         return paymentData;
    //     } catch (error) {
    //         throw error;
    //     }
    // };


    // clientPaymentRemindersList = async(
    //     information,
    //     clientId,
    //     params,
    //     filterPrivateFields = true
    // ) => {
    //     try {


    //         var mergedAllActiveClients = [];
    //         let todayDate = await hyphenDateFormat(new Date);
    //         // let todayDate = '2018-07-04';
    //         var businessIdinformation = information.split(",")[1];
    //         let dbclientpayments = createInstance(["clientPayments"], information);
    //         let dbsubscriptions = createInstance(["clientSubscription"], information);
    //         let dbClients = createInstance(["client"], information);
    //         let dbpricepack = createInstance(["clientPricepack"], information);
    //         let dbclientexpense = createInstance(["clientExpense"], information);
    //         let dbpaymentTransactions = createInstance(["paymentTransactions"], information);
    //         let dbclientScheduleMap = createInstance(["clientScheduleMap"], information);
    //         dbclientpayments.clientpayments.belongsTo(dbsubscriptions.subscriptions, {
    //             foreignKey: "subscriptionsId",
    //             targetKey: "subscriptionId"
    //         });
    //         dbsubscriptions.subscriptions.belongsTo(dbClients.clients, {
    //             foreignKey: "clientId",
    //             targetKey: "clientId"
    //         });
    //         dbsubscriptions.subscriptions.belongsTo(dbpricepack.pricepacks, {
    //             foreignKey: "pricepacks",
    //             targetKey: "pricepackId"
    //         });
    //         dbclientpayments.clientpayments.belongsTo(dbpaymentTransactions.paymentTransactions, {
    //             foreignKey: "clientpaymentId",
    //             targetKey: "clientpaymentId"
    //         });
    //         dbsubscriptions.subscriptions.belongsTo(dbclientScheduleMap.clientschedulemap, {
    //             foreignKey: "subscriptionId",
    //             targetKey: "subscriptionId"
    //         });

    //         let filter = params[0] ? params[0] : "";
    //         var pageNo = params["page"] ? params["page"] : 1;
    //         var limitPerPage = PEGINATION_LIMIT;
    //         var offsetLimit = (pageNo - 1) * limitPerPage;

    //         let clientpaymentsRiminder = {};
    //         let recentIncomeclientpayments = {};
    //         let recentExpenseclientExpense = {};

    //         let itemclientpaymentsRiminderData = {
    //             status: 1,
    //             businessId: businessIdinformation,
    //             clientpaymentId: {
    //                 [dbclientpayments.sequelize.Op.in]:
    //                 // [Sequelize.literal("(SELECT MIN(`clientpaymentId`) FROM `clientpayments` WHERE `payment_due_date` >= '" + todayDate + "' AND `payment_end_date` >= '" + todayDate + "' AND `status` = '1' GROUP BY `subscriptionsId`)")]
    //                 // [Sequelize.literal("(SELECT MIN(`clientpaymentId`) FROM `clientpayments` WHERE (`payment_due_date` <= '" + todayDate + "' AND `payment_end_date` >= '" + todayDate + "' AND `status` = '1' ) OR ( `payment_due_date` >= '" + todayDate + "' AND `payment_end_date` >= '" + todayDate + "' AND `status` = '1' ) AND `status` = '1' GROUP BY `subscriptionsId`)")]
    //                 // [Sequelize.literal("(SELECT MIN(`clientpaymentId`) FROM `clientpayments` INNER JOIN `subscriptions` AS `subscription` ON `clientpayments`.`subscriptionsId` = `subscription`.`subscriptionId` INNER JOIN `pricepacks` AS `subscription->pricepack` ON `subscription`.`pricepacks` = `subscription->pricepack`.`pricepackId` WHERE Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day ) <= '" + todayDate + "' OR ( ( `clientpayments`.`payment_due_date` <= '" + todayDate + "' AND `clientpayments`.`payment_end_date` >= '" + todayDate + "' ) OR ( `clientpayments`.`payment_due_date` >= '" + todayDate + "' AND `clientpayments`.`payment_end_date` >= '" + todayDate + "' ) AND `clientpayments`.`status` = '1' ) GROUP BY `clientpayments`.`subscriptionsId` ORDER BY `payment_end_date` ASC )")]
    //                     [Sequelize.literal("(SELECT MIN(`clientpaymentId`) as `clientpaymentId` FROM `clientpayments` INNER JOIN `subscriptions` AS `subscription` ON `clientpayments`.`subscriptionsId` = `subscription`.`subscriptionId` INNER JOIN `pricepacks` AS `subscription->pricepack` ON `subscription`.`pricepacks` = `subscription->pricepack`.`pricepackId` WHERE Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day ) <= '" + todayDate + "' OR ( ( `clientpayments`.`payment_due_date` <= '" + todayDate + "' AND `clientpayments`.`payment_end_date` >= '" + todayDate + "' ) OR ( `clientpayments`.`payment_due_date` >= '" + todayDate + "' AND `clientpayments`.`payment_end_date` >= '" + todayDate + "' ) ) AND `clientpayments`.`status` = '1' GROUP BY `clientpayments`.`subscriptionsId` UNION select c.clientpaymentId from clientpayments as c, ( SELECT a.clientpaymentId, a.curent_subcription_number, a.payment_end_date, COUNT(a.subscriptionsId) FROM clientpayments a LEFT JOIN clientpayments b ON a.subscriptionsId = b.subscriptionsId AND b.status = 1 WHERE b.status = 1 AND a.status = 1 GROUP BY a.subscriptionsId, a.clientpaymentId having a.curent_subcription_number = COUNT(a.subscriptionsId) AND a.payment_end_date < CURDATE() ) as total WHERE c.clientpaymentId IN (total.clientpaymentId))")]
    //             },
    //             [dbClients.sequelize.Op.or]: [{
    //                     "$subscription.client.clientName$": {
    //                         like: "%" + filter + "%"
    //                     }
    //                 },
    //                 {
    //                     "$subscription.pricepack.pricepackName$": {
    //                         like: "%" + filter + "%"
    //                     }
    //                 }
    //             ],
    //             [dbclientScheduleMap.sequelize.Op.and]: [Sequelize.literal("( ( `subscription->clientschedulemap`.`clientScheduleMapId` IS NULL OR `subscription->clientschedulemap`.`sessionCount` IS NULL ) OR ( `subscription->clientschedulemap`.`sessionCount` < `subscription->pricepack`.`serviceDuration` ) )")],
    //         };
    //         let itemGroupConditions = {
    //             status: 1,
    //             businessId: businessIdinformation,
    //             clientId: clientId
    //         };
    //         let clientpaymentsRiminderData = await dbclientpayments.clientpayments.findAndCount({
    //             attributes: [
    //                 "clientpaymentId",
    //                 "subscriptionsId",
    //                 "due_amount",
    //                 "payble_amount",
    //                 "payment_due_date",
    //                 "payment_end_date", ["UpdateDateTime", "updated_at"],
    //                 "paymentStatus", [dbclientpayments.sequelize.literal("Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day )"), 'subcription_end_Date']
    //             ],
    //             include: [{
    //                 model: dbsubscriptions.subscriptions,
    //                 attributes: [
    //                     "clientId",
    //                     "amountPaidSoFar",
    //                     "status_amountPaidSoFar",
    //                     "clientJoiningDate",
    //                     "subscriptionDateTime",
    //                     "installment_frequency",
    //                 ],
    //                 include: [{
    //                         model: dbClients.clients,
    //                         attributes: {
    //                             include: [
    //                                 "clientName", [
    //                                     dbClients.clients.sequelize.literal(
    //                                         'CASE WHEN photoUrl  is NULL THEN CONCAT("' +
    //                                         KNACK_UPLOAD_URL.localUrl +
    //                                         '" , "noimage.jpg" ) ELSE CONCAT("' +
    //                                         KNACK_UPLOAD_URL.localUrl +
    //                                         '" , photoUrl ) END'
    //                                     ),
    //                                     "photoUrl"
    //                                 ],
    //                             ],
    //                             exclude: []
    //                         },
    //                         where: itemGroupConditions
    //                     },
    //                     {
    //                         model: dbpricepack.pricepacks,
    //                         attributes: ["pricepackName"],
    //                         where: {}
    //                     },
    //                     {
    //                         model: dbclientScheduleMap.clientschedulemap,
    //                         required: false,
    //                         attributes: ["sessionCount"],
    //                         where: {
    //                             // [dbclientScheduleMap.sequelize.Op.and]: [Sequelize.literal("`subscription->clientschedulemap`.`scheduleId`= `subscription->clientschedulemap`.`parentScheduleId`")],
    //                             [dbclientScheduleMap.sequelize.Op.and]: [Sequelize.literal("( `subscription->clientschedulemap`.`scheduleId` = `subscription->clientschedulemap`.`parentScheduleId` ) AND ( `subscription->clientschedulemap`.`scheduleId` = (select min(`scheduleId`) from `clientschedulemap` `inSched` where `inSched`.`subscriptionId` = `subscription`.`subscriptionId` ) )")],
    //                         }
    //                     }
    //                 ],
    //                 where: {}
    //             }],
    //             order: [
    //                 ["CreateDateTime", "DESC"]
    //             ],
    //             where: itemclientpaymentsRiminderData,
    //             subQuery: false,
    //             offset: offsetLimit,
    //             limit: limitPerPage,
    //             raw: true
    //         });
    //         if (clientpaymentsRiminderData.rows.length > 0) {
    //             for (let x = 0; x < clientpaymentsRiminderData.rows.length; x++) {
    //                 clientpaymentsRiminderData.rows[x]["sessionInfo"] = await sessionInfo(information, {
    //                     status: 1,
    //                     subscriptionId: clientpaymentsRiminderData.rows[x]['subscriptionsId']
    //                 });
    //                 if (clientpaymentsRiminderData.rows[x]["subscription.installment_frequency"] == '6') {
    //                     let new_data = '';
    //                     let cal_new_amount = '';
    //                     if (clientpaymentsRiminderData.rows[x]["subscription.status_amountPaidSoFar"] == 1) {
    //                         let date_check = (clientpaymentsRiminderData.rows[x]['subscription.clientJoiningDate'] != null) ? clientpaymentsRiminderData.rows[x]['subscription.clientJoiningDate'] : clientpaymentsRiminderData.rows[x]['subscription.subscriptionDateTime'];
    //                         cal_new_amount = await this.getTotalShowDataOthers(information, clientpaymentsRiminderData.rows[x]['subscriptionsId'], date_check);
    //                         new_data = Number(cal_new_amount[0].new_balance) - Number(clientpaymentsRiminderData.rows[x]["subscription.amountPaidSoFar"]);
    //                     } else {
    //                         let date_check = await hyphenDateFormat(new Date());
    //                         cal_new_amount = await this.getTotalShowDataOthers(information, clientpaymentsRiminderData.rows[x]['subscriptionsId'], date_check);
    //                         new_data = Number(cal_new_amount[0].new_balance);
    //                     }
    //                     clientpaymentsRiminderData.rows[x]["getTotalShowData"] = [{
    //                         new_balance: new_data
    //                     }];
    //                     clientpaymentsRiminderData.rows[x]["getTotalDue"] = await this.getTotalDue(information, clientpaymentsRiminderData.rows[x]['subscriptionsId']);
    //                     let getTotalPaid = await this.getTotalPaid(information, clientpaymentsRiminderData.rows[x]['subscriptionsId']);
    //                     if (clientpaymentsRiminderData.rows[x]["subscription.status_amountPaidSoFar"] == '1') {
    //                         getTotalPaid[0].total_paid_amount = Number(getTotalPaid[0].total_paid_amount);
    //                     } else {
    //                         getTotalPaid[0].total_paid_amount = Number(getTotalPaid[0].total_paid_amount) - Number(clientpaymentsRiminderData.rows[x]["subscription.amountPaidSoFar"]);
    //                     }
    //                     clientpaymentsRiminderData.rows[x]["getTotalPaid"] = getTotalPaid;
    //                 } else {
    //                     let new_data = '';
    //                     let cal_new_amount = '';
    //                     if (clientpaymentsRiminderData.rows[x]["subscription.status_amountPaidSoFar"] == 1) {
    //                         let date_check = (clientpaymentsRiminderData.rows[x]['subscription.clientJoiningDate'] != null) ? clientpaymentsRiminderData.rows[x]['subscription.clientJoiningDate'] : clientpaymentsRiminderData.rows[x]['subscription.subscriptionDateTime'];
    //                         cal_new_amount = await this.getTotalShowData(information, clientpaymentsRiminderData.rows[x]['subscriptionsId'], date_check);
    //                         new_data = Number(cal_new_amount[0].new_balance) - Number(clientpaymentsRiminderData.rows[x]["subscription.amountPaidSoFar"]);
    //                     } else {
    //                         // let date_check = (clientpaymentsRiminderData.rows[x]['subscription.clientJoiningDate'] != null) ? clientpaymentsRiminderData.rows[x]['subscription.clientJoiningDate'] : clientpaymentsRiminderData.rows[x]['subscription.subscriptionDateTime'];
    //                         let date_check = clientpaymentsRiminderData.rows[x]['payment_end_date'];
    //                         cal_new_amount = await this.getTotalShowData(information, clientpaymentsRiminderData.rows[x]['subscriptionsId'], date_check);
    //                         new_data = Number(cal_new_amount[0].new_balance);
    //                     }
    //                     clientpaymentsRiminderData.rows[x]["getTotalShowData"] = [{
    //                         new_balance: new_data
    //                     }];
    //                     clientpaymentsRiminderData.rows[x]["getTotalDue"] = await this.getTotalDue(information, clientpaymentsRiminderData.rows[x]['subscriptionsId']);
    //                     let getTotalPaid = await this.getTotalPaid(information, clientpaymentsRiminderData.rows[x]['subscriptionsId']);
    //                     if (clientpaymentsRiminderData.rows[x]["subscription.status_amountPaidSoFar"] == '1') {
    //                         getTotalPaid[0].total_paid_amount = Number(getTotalPaid[0].total_paid_amount);
    //                     } else {
    //                         getTotalPaid[0].total_paid_amount = Number(getTotalPaid[0].total_paid_amount) - Number(clientpaymentsRiminderData.rows[x]["subscription.amountPaidSoFar"]);
    //                     }
    //                     clientpaymentsRiminderData.rows[x]["getTotalPaid"] = getTotalPaid;

    //                 }
    //                 let listclientPayments = await dbclientpayments.clientpayments.findAll({
    //                     where: {
    //                         businessId: businessIdinformation,
    //                         subscriptionsId: clientpaymentsRiminderData.rows[x]['subscriptionsId'],
    //                         status: 1,
    //                         "$payble_amount$": {
    //                             ne: null
    //                         }
    //                     },
    //                     raw: true
    //                 });
    //                 clientpaymentsRiminderData.rows[x]["subcription_payment_status"] = listclientPayments.length;
    //             }
    //         }
    //         clientpaymentsRiminder.count = clientpaymentsRiminderData.count;
    //         clientpaymentsRiminder.rows = clientpaymentsRiminderData.rows;
    //         return await clientpaymentsRiminder;

    //     } catch (error) {
    //         throw error;
    //     }
    // };


    clientPaymentRemindersList = async(
        information,
        clientId,
        params,
        filterPrivateFields = true
    ) => {
        try {
            let todayFormat = await date_ISO_Convert(new Date());
            // let todayFormat = "2018-08-23";
            let orderBy;
            let total_record = {};
            let clientId = params['clientId'];
            let businessIdinformation = information.split(",")[1];
            let dbpaymentTransactions = createInstance(["paymentTransactions"], information);
            let dbclientpayments = createInstance(["clientPayments"], information);
            let dbClients = createInstance(["client"], information);
            let dbsubscriptions = createInstance(["clientSubscription"], information);
            let dbpricepack = createInstance(["clientPricepack"], information);
            let dbservicepricepackmap = createInstance(["servicePricepackMap"], information);
            let dbservices = createInstance(["clientService"], information);
            dbclientpayments.clientpayments.belongsTo(dbsubscriptions.subscriptions, {
                foreignKey: "subscriptionsId",
                targetKey: "subscriptionId"
            });
            dbsubscriptions.subscriptions.belongsTo(dbClients.clients, {
                foreignKey: "clientId",
                targetKey: "clientId"
            });
            dbsubscriptions.subscriptions.belongsTo(dbpricepack.pricepacks, {
                foreignKey: "pricepacks",
                targetKey: "pricepackId"
            });
            dbclientpayments.clientpayments.belongsTo(dbpaymentTransactions.paymentTransactions, {
                foreignKey: "clientpaymentId",
                targetKey: "clientpaymentId"
            });
            dbservicepricepackmap.servicepricepackmap.belongsTo(dbservices.services, {
                foreignKey: "serviceId",
                targetKey: "serviceId"
            });
            let filter = params[0] ? params[0] : "";
            var pageNo = params["page"] ? params["page"] : 1;
            var limitPerPage = PEGINATION_LIMIT;
            var offsetLimit = (pageNo - 1) * limitPerPage;
            if (params["order"] == 1) {
                orderBy = "ORDER BY `subcription_type` DESC";
            } else {
                orderBy = "ORDER BY `subcription_type` ASC";
            }
            // let paymentData = await dbsubscriptions.sequelize.query("( SELECT `clientpayments`.`clientpaymentId`, `clientpayments`.`subscriptionsId`, `clientpayments`.`due_amount`, `clientpayments`.`payble_amount`, `clientpayments`.`payment_due_date`, `clientpayments`.`payment_end_date`, `clientpayments`.`UpdateDateTime` AS `updated_at`, `clientpayments`.`paymentStatus`, Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day ) AS `subcription_end_Date`, `subscription`.`clientId` AS `subscription.clientId`, `subscription`.`amountPaidSoFar` AS `subscription.amountPaidSoFar`, `subscription`.`status_amountPaidSoFar` AS `subscription.status_amountPaidSoFar`, `subscription->client`.`clientId` AS `subscription.client.clientId`, `subscription->client`.`businessId` AS `subscription.client.businessId`, `subscription->client`.`centerId` AS `subscription.client.centerId`, `subscription->client`.`clientName` AS `subscription.client.clientName`, `subscription->client`.`contactNumber` AS `subscription.client.contactNumber`, `subscription->client`.`alternateNumber` AS `subscription.client.alternateNumber`, `subscription->client`.`emailId` AS `subscription.client.emailId`, `subscription->client`.`alternateEmail` AS `subscription.client.alternateEmail`, `subscription->client`.`dateOfBirth` AS `subscription.client.dateOfBirth`, `subscription->client`.`area` AS `subscription.client.area`, `subscription->client`.`pin` AS `subscription.client.pin`, `subscription->client`.`city` AS `subscription.client.city`, `subscription->client`.`photoUrl` AS `subscription.client.photoUrl`, `subscription->client`.`createDateTime` AS `subscription.client.createDateTime`, `subscription->client`.`updateDateTime` AS `subscription.client.updateDateTime`, `subscription->client`.`status` AS `subscription.client.status`, `subscription->client`.`clientName` AS `subscription.client.clientName`, CASE WHEN photoUrl is NULL THEN CONCAT( '" + KNACK_UPLOAD_URL.localUrl + "', 'noimage.jpg' ) ELSE CONCAT( '" + KNACK_UPLOAD_URL.localUrl + "', photoUrl ) END AS `subscription.client.photoUrl`, `subscription->pricepack`.`pricepackId` AS `subscription.pricepack.pricepackId`, `subscription->pricepack`.`pricepackName` AS `subscription.pricepack.pricepackName`, 1 as `subcription_type` FROM `clientpayments` AS `clientpayments` INNER JOIN `subscriptions` AS `subscription` ON `clientpayments`.`subscriptionsId` = `subscription`.`subscriptionId` INNER JOIN `clients` AS `subscription->client` ON `subscription`.`clientId` = `subscription->client`.`clientId` AND `subscription->client`.`status` = 1 AND `subscription->client`.`businessId` = '" + businessIdinformation + "' INNER JOIN `pricepacks` AS `subscription->pricepack` ON `subscription`.`pricepacks` = `subscription->pricepack`.`pricepackId` LEFT OUTER JOIN `clientschedulemap` AS `subscription->clientschedulemap` ON `subscription`.`subscriptionId` = `subscription->clientschedulemap`.`subscriptionId` AND `subscription->clientschedulemap`.`scheduleId`= `subscription->clientschedulemap`.`parentScheduleId` WHERE ( `subscription->client`.`clientName` LIKE '%" + filter + "%' OR `subscription->pricepack`.`pricepackName` LIKE '%" + filter + "%' ) AND ( ( `subscription->clientschedulemap`.`clientScheduleMapId` IS NULL OR `subscription->clientschedulemap`.`sessionCount` IS NULL ) OR ( `subscription->clientschedulemap`.`sessionCount` < `subscription->pricepack`.`serviceDuration` ) ) AND `clientpayments`.`status` = 1 AND `clientpayments`.`paymentStatus` = 1 AND `clientpayments`.`businessId` = '" + businessIdinformation + "' AND `subscription->client`.`clientId` = '" + clientId + "' AND `clientpayments`.`clientpaymentId` IN ( ( SELECT MIN(`clientpaymentId`) FROM `clientpayments` WHERE `payment_due_date` >= '" + todayFormat + "' AND `payment_end_date` >= '" + todayFormat + "' AND `status` = '1' AND `paymentStatus` = '1' GROUP BY `subscriptionsId` ) ) ) union all ( SELECT `clientpayments`.`clientpaymentId`, `clientpayments`.`subscriptionsId`, `clientpayments`.`due_amount`, `clientpayments`.`payble_amount`, `clientpayments`.`payment_due_date`, `clientpayments`.`payment_end_date`, `clientpayments`.`UpdateDateTime` AS `updated_at`, `clientpayments`.`paymentStatus`, Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day ) AS `subcription_end_Date`, `subscription`.`clientId` AS `subscription.clientId`, `subscription`.`amountPaidSoFar` AS `subscription.amountPaidSoFar`, `subscription`.`status_amountPaidSoFar` AS `subscription.status_amountPaidSoFar`, `subscription->client`.`clientId` AS `subscription.client.clientId`, `subscription->client`.`businessId` AS `subscription.client.businessId`, `subscription->client`.`centerId` AS `subscription.client.centerId`, `subscription->client`.`clientName` AS `subscription.client.clientName`, `subscription->client`.`contactNumber` AS `subscription.client.contactNumber`, `subscription->client`.`alternateNumber` AS `subscription.client.alternateNumber`, `subscription->client`.`emailId` AS `subscription.client.emailId`, `subscription->client`.`alternateEmail` AS `subscription.client.alternateEmail`, `subscription->client`.`dateOfBirth` AS `subscription.client.dateOfBirth`, `subscription->client`.`area` AS `subscription.client.area`, `subscription->client`.`pin` AS `subscription.client.pin`, `subscription->client`.`city` AS `subscription.client.city`, `subscription->client`.`photoUrl` AS `subscription.client.photoUrl`, `subscription->client`.`createDateTime` AS `subscription.client.createDateTime`, `subscription->client`.`updateDateTime` AS `subscription.client.updateDateTime`, `subscription->client`.`status` AS `subscription.client.status`, `subscription->client`.`clientName` AS `subscription.client.clientName`, CASE WHEN photoUrl is NULL THEN CONCAT( '" + KNACK_UPLOAD_URL.localUrl + "', 'noimage.jpg' ) ELSE CONCAT( '" + KNACK_UPLOAD_URL.localUrl + "', photoUrl ) END AS `subscription.client.photoUrl`, `subscription->pricepack`.`pricepackId` AS `subscription.pricepack.pricepackId`, `subscription->pricepack`.`pricepackName` AS `subscription.pricepack.pricepackName`, 0 as `subcription_type` FROM `clientpayments` AS `clientpayments` INNER JOIN `subscriptions` AS `subscription` ON `clientpayments`.`subscriptionsId` = `subscription`.`subscriptionId` AND `subscription`.`renewl_status` = 1 INNER JOIN `clients` AS `subscription->client` ON `subscription`.`clientId` = `subscription->client`.`clientId` AND `subscription->client`.`status` = 1 AND `subscription->client`.`businessId` = '" + businessIdinformation + "' AND `subscription->client`.`clientId` = '" + clientId + "' INNER JOIN `pricepacks` AS `subscription->pricepack` ON `subscription`.`pricepacks` = `subscription->pricepack`.`pricepackId` LEFT OUTER JOIN `clientschedulemap` AS `subscription->clientschedulemap` ON `subscription`.`subscriptionId` = `subscription->clientschedulemap`.`subscriptionId` AND `subscription->clientschedulemap`.`scheduleId`= `subscription->clientschedulemap`.`parentScheduleId` WHERE (`subscription->client`.`clientName` LIKE '%" + filter + "%' OR `subscription->pricepack`.`pricepackName` LIKE '%" + filter + "%') AND ( Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day ) < '" + todayFormat + "' OR ( `subscription->clientschedulemap`.`sessionCount` = `subscription->pricepack`.`serviceDuration` ) ) AND `clientpayments`.`status` = 1 AND `clientpayments`.`is_last_payment` = 1 AND `clientpayments`.`businessId` = '" + businessIdinformation + "' ) " + orderBy + " LIMIT " + limitPerPage + " OFFSET " + offsetLimit, {
            //     type: dbsubscriptions.sequelize.QueryTypes.SELECT
            // });
            let paymentData = await dbsubscriptions.sequelize.query("( SELECT `clientpayments`.`clientpaymentId`, `clientpayments`.`subscriptionsId`, `clientpayments`.`due_amount`, `clientpayments`.`payble_amount`, `clientpayments`.`payment_due_date`, `clientpayments`.`payment_end_date`, `clientpayments`.`UpdateDateTime` AS `updated_at`, `clientpayments`.`paymentStatus`,`clientpayments`.`paymentReminder_status` AS `paymentReminder_status`, `clientpayments`.`paymentReminder_date` AS `paymentReminder_date`, DATE(DATE( Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day ) )) AS `subcription_end_Date`, `subscription`.`clientId` AS `subscription.clientId`, `subscription`.`amountPaidSoFar` AS `subscription.amountPaidSoFar`, `subscription`.`status_amountPaidSoFar` AS `subscription.status_amountPaidSoFar`, `subscription`.`clientJoiningDate` AS `subscription.clientJoiningDate`, `subscription`.`subscriptionDateTime` AS `subscription.subscriptionDateTime`, `subscription`.`installment_frequency` AS `subscription.installment_frequency`, `subscription->client`.`clientId` AS `subscription.client.clientId`, `subscription->client`.`businessId` AS `subscription.client.businessId`, `subscription->client`.`centerId` AS `subscription.client.centerId`, `subscription->client`.`clientName` AS `subscription.client.clientName`, `subscription->client`.`contactNumber` AS `subscription.client.contactNumber`, `subscription->client`.`alternateNumber` AS `subscription.client.alternateNumber`, `subscription->client`.`emailId` AS `subscription.client.emailId`, `subscription->client`.`alternateEmail` AS `subscription.client.alternateEmail`, `subscription->client`.`dateOfBirth` AS `subscription.client.dateOfBirth`, `subscription->client`.`area` AS `subscription.client.area`, `subscription->client`.`pin` AS `subscription.client.pin`, `subscription->client`.`city` AS `subscription.client.city`, `subscription->client`.`photoUrl` AS `subscription.client.photoUrl`, `subscription->client`.`createDateTime` AS `subscription.client.createDateTime`, `subscription->client`.`updateDateTime` AS `subscription.client.updateDateTime`, `subscription->client`.`status` AS `subscription.client.status`, `subscription->client`.`clientName` AS `subscription.client.clientName`, CASE WHEN photoUrl is NULL THEN CONCAT( '" + KNACK_UPLOAD_URL.localUrl + "', 'noimage.jpg' ) ELSE CONCAT( '" + KNACK_UPLOAD_URL.localUrl + "', photoUrl ) END AS `subscription.client.photoUrl`, `subscription->pricepack`.`pricepackId` AS `subscription.pricepack.pricepackId`, `subscription->pricepack`.`pricepackName` AS `subscription.pricepack.pricepackName`, `subscription->clientschedulemap`.`clientScheduleMapId` AS `subscription.clientschedulemap.clientScheduleMapId`, `subscription->clientschedulemap`.`sessionCount` AS `subscription.clientschedulemap.sessionCount`, 1 as `subcription_type` FROM `clientpayments` AS `clientpayments` INNER JOIN `subscriptions` AS `subscription` ON `clientpayments`.`subscriptionsId` = `subscription`.`subscriptionId` INNER JOIN `clients` AS `subscription->client` ON `subscription`.`clientId` = `subscription->client`.`clientId` AND `subscription->client`.`status` = 1 AND `subscription->client`.`businessId` = '" + businessIdinformation + "' AND `subscription->client`.`clientId` = '" + clientId + "' INNER JOIN `pricepacks` AS `subscription->pricepack` ON `subscription`.`pricepacks` = `subscription->pricepack`.`pricepackId` LEFT OUTER JOIN `clientschedulemap` AS `subscription->clientschedulemap` ON `subscription`.`subscriptionId` = `subscription->clientschedulemap`.`subscriptionId` AND ( ( `subscription->clientschedulemap`.`scheduleId` = `subscription->clientschedulemap`.`parentScheduleId` ) AND ( `subscription->clientschedulemap`.`scheduleId` = ( select min(`scheduleId`) from `clientschedulemap` `inSched` where `inSched`.`subscriptionId` = `subscription`.`subscriptionId` ) ) ) WHERE ( `subscription->client`.`clientName` LIKE '%" + filter + "%' OR `subscription->pricepack`.`pricepackName` LIKE '%" + filter + "%' ) AND ( ( ( `subscription->clientschedulemap`.`clientScheduleMapId` IS NULL OR `subscription->clientschedulemap`.`sessionCount` IS NULL ) OR ( `subscription->clientschedulemap`.`sessionCount` < `subscription->pricepack`.`serviceDuration` ) ) ) AND `clientpayments`.`status` = 1 AND `clientpayments`.`businessId` = '" + businessIdinformation + "' AND `clientpayments`.`clientpaymentId` IN ( ( SELECT MIN(`clientpaymentId`) as `clientpaymentId` FROM `clientpayments` INNER JOIN `subscriptions` AS `subscription` ON `clientpayments`.`subscriptionsId` = `subscription`.`subscriptionId` INNER JOIN `pricepacks` AS `subscription->pricepack` ON `subscription`.`pricepacks` = `subscription->pricepack`.`pricepackId` WHERE ( DATE( Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day ) ) >= '" + todayFormat + "' ) AND ( ( `clientpayments`.`payment_due_date` <= '" + todayFormat + "' AND `clientpayments`.`payment_end_date` >= '" + todayFormat + "' ) OR ( `clientpayments`.`payment_due_date` >= '" + todayFormat + "' AND `clientpayments`.`payment_end_date` >= '" + todayFormat + "' ) ) AND `clientpayments`.`status` = '1' GROUP BY `clientpayments`.`subscriptionsId` UNION select c.clientpaymentId from clientpayments as c, ( SELECT a.clientpaymentId, a.curent_subcription_number, a.payment_end_date, COUNT(a.subscriptionsId) FROM clientpayments a LEFT JOIN clientpayments b ON a.subscriptionsId = b.subscriptionsId AND b.status = 1 WHERE b.status = 1 AND a.status = 1 GROUP BY a.subscriptionsId, a.clientpaymentId having a.curent_subcription_number = COUNT(a.subscriptionsId) AND a.payment_end_date < CURDATE() ) as total WHERE ( DATE( Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day ) ) >= '" + todayFormat + "' ) AND c.clientpaymentId IN (total.clientpaymentId) ) ) ) union ( SELECT `clientpayments`.`clientpaymentId`, `clientpayments`.`subscriptionsId`, `clientpayments`.`due_amount`, `clientpayments`.`payble_amount`, `clientpayments`.`payment_due_date`, `clientpayments`.`payment_end_date`, `clientpayments`.`UpdateDateTime` AS `updated_at`, `clientpayments`.`paymentStatus`, `clientpayments`.`paymentReminder_status` AS `paymentReminder_status`, `clientpayments`.`paymentReminder_date` AS `paymentReminder_date`, DATE(DATE( Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day ) )) AS `subcription_end_Date`, `subscription`.`clientId` AS `subscription.clientId`, `subscription`.`amountPaidSoFar` AS `subscription.amountPaidSoFar`, `subscription`.`status_amountPaidSoFar` AS `subscription.status_amountPaidSoFar`, `subscription`.`clientJoiningDate` AS `subscription.clientJoiningDate`, `subscription`.`subscriptionDateTime` AS `subscription.subscriptionDateTime`, `subscription`.`installment_frequency` AS `subscription.installment_frequency`, `subscription->client`.`clientId` AS `subscription.client.clientId`, `subscription->client`.`businessId` AS `subscription.client.businessId`, `subscription->client`.`centerId` AS `subscription.client.centerId`, `subscription->client`.`clientName` AS `subscription.client.clientName`, `subscription->client`.`contactNumber` AS `subscription.client.contactNumber`, `subscription->client`.`alternateNumber` AS `subscription.client.alternateNumber`, `subscription->client`.`emailId` AS `subscription.client.emailId`, `subscription->client`.`alternateEmail` AS `subscription.client.alternateEmail`, `subscription->client`.`dateOfBirth` AS `subscription.client.dateOfBirth`, `subscription->client`.`area` AS `subscription.client.area`, `subscription->client`.`pin` AS `subscription.client.pin`, `subscription->client`.`city` AS `subscription.client.city`, `subscription->client`.`photoUrl` AS `subscription.client.photoUrl`, `subscription->client`.`createDateTime` AS `subscription.client.createDateTime`, `subscription->client`.`updateDateTime` AS `subscription.client.updateDateTime`, `subscription->client`.`status` AS `subscription.client.status`, `subscription->client`.`clientName` AS `subscription.client.clientName`, CASE WHEN photoUrl is NULL THEN CONCAT( '" + KNACK_UPLOAD_URL.localUrl + "', 'noimage.jpg' ) ELSE CONCAT( '" + KNACK_UPLOAD_URL.localUrl + "', photoUrl ) END AS `subscription.client.photoUrl`, `subscription->pricepack`.`pricepackId` AS `subscription.pricepack.pricepackId`, `subscription->pricepack`.`pricepackName` AS `subscription.pricepack.pricepackName`, `subscription->clientschedulemap`.`clientScheduleMapId` AS `subscription.clientschedulemap.clientScheduleMapId`, `subscription->clientschedulemap`.`sessionCount` AS `subscription.clientschedulemap.sessionCount`, 0 as `subcription_type` FROM `clientpayments` AS `clientpayments` INNER JOIN `subscriptions` AS `subscription` ON `clientpayments`.`subscriptionsId` = `subscription`.`subscriptionId` AND `subscription`.`renewl_status` = 1 INNER JOIN `clients` AS `subscription->client` ON `subscription`.`clientId` = `subscription->client`.`clientId` AND `subscription->client`.`status` = 1 AND `subscription->client`.`businessId` = '" + businessIdinformation + "' AND `subscription->client`.`clientId` = '" + clientId + "' INNER JOIN `pricepacks` AS `subscription->pricepack` ON `subscription`.`pricepacks` = `subscription->pricepack`.`pricepackId` INNER JOIN `clientschedulemap` AS `subscription->clientschedulemap` ON `subscription`.`subscriptionId` = `subscription->clientschedulemap`.`subscriptionId` AND ( ( `subscription->clientschedulemap`.`scheduleId` = `subscription->clientschedulemap`.`parentScheduleId` ) AND ( `subscription->clientschedulemap`.`scheduleId` = ( select min(`scheduleId`) from `clientschedulemap` `inSched` where `inSched`.`subscriptionId` = `subscription`.`subscriptionId` ) ) ) WHERE ( `subscription->client`.`clientName` LIKE '%" + filter + "%' OR `subscription->pricepack`.`pricepackName` LIKE '%" + filter + "%' ) AND ( ( DATE( Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day ) ) < '" + todayFormat + "' ) OR ( `subscription->clientschedulemap`.`sessionCount` = `subscription->pricepack`.`serviceDuration` ) ) AND `clientpayments`.`status` = 1 AND `clientpayments`.`is_last_payment` = 1 AND `clientpayments`.`businessId` = '" + businessIdinformation + "' )" + orderBy + " LIMIT " + limitPerPage + " OFFSET " + offsetLimit, {
                type: dbsubscriptions.sequelize.QueryTypes.SELECT
            });
            if (paymentData) {
                for (var i = 0; i < paymentData.length; i++) {
                    let serviceAll = await dbservicepricepackmap.servicepricepackmap.findAll({
                        attributes: {
                            include: [],
                            exclude: [`servicepricepackmapId`, `serviceDuration_number`, `pricepackId`, `serviceId`, `businessId`, `serviceDuration`, `createDateTime`, `updateDateTime`, `status`]
                        },
                        where: {
                            status: 1,
                            pricepackId: paymentData[i]["subscription.pricepack.pricepackId"]
                        },
                        include: [{
                            model: dbservices.services,
                            attributes: [
                                [dbservicepricepackmap.sequelize.fn('GROUP_CONCAT', dbservicepricepackmap.sequelize.col('serviceName')), 'serviceNameList']
                            ],
                        }],
                        raw: true
                    });
                    let serviceAllID = await dbservicepricepackmap.servicepricepackmap.findAll({
                        attributes: {
                            include: [],
                            exclude: [`servicepricepackmapId`, `pricepackId`, `serviceId`, `businessId`, `serviceDuration`, `createDateTime`, `updateDateTime`, `status`]
                        },
                        where: {
                            status: 1,
                            pricepackId: paymentData[i]["subscription.pricepack.pricepackId"]
                        },
                        include: [{
                            model: dbservices.services,
                            attributes: [
                                'serviceId'
                            ],
                        }],
                        raw: true
                    });
                    paymentData[i]["serviceIDList"] = serviceAllID;
                    paymentData[i]["serviceNameList"] = serviceAll;
                }
            }
            if (paymentData.length > 0) {
                for (let x = 0; x < paymentData.length; x++) {
                    paymentData[x]["sessionInfo"] = await sessionInfo(information, {
                        status: 1,
                        subscriptionId: paymentData[x]['subscriptionsId']
                    });
                    if (paymentData[x]["subscription.installment_frequency"] == '6') {
                        let new_data = '';
                        let cal_new_amount = '';
                        if (paymentData[x]["subscription.status_amountPaidSoFar"] == 1) {
                            let date_check = (paymentData[x]['subscription.clientJoiningDate'] != null) ? paymentData[x]['subscription.clientJoiningDate'] : paymentData[x]['subscription.subscriptionDateTime'];
                            cal_new_amount = await this.getTotalShowDataOthers(information, paymentData[x]['subscriptionsId'], date_check);
                            new_data = Number(cal_new_amount[0].new_balance) - Number(paymentData[x]["subscription.amountPaidSoFar"]);
                        } else {
                            let date_check = await hyphenDateFormat(new Date());
                            cal_new_amount = await this.getTotalShowDataOthers(information, paymentData[x]['subscriptionsId'], date_check);
                            new_data = Number(cal_new_amount[0].new_balance);
                        }
                        paymentData[x]["getTotalShowData"] = [{
                            new_balance: new_data
                        }];
                        paymentData[x]["getTotalDue"] = await this.getTotalDue(information, paymentData[x]['subscriptionsId']);
                        let getTotalPaid = await this.getTotalPaid(information, paymentData[x]['subscriptionsId']);
                        if (paymentData[x]["subscription.status_amountPaidSoFar"] == '1') {
                            getTotalPaid[0].total_paid_amount = Number(getTotalPaid[0].total_paid_amount);
                        } else {
                            getTotalPaid[0].total_paid_amount = Number(getTotalPaid[0].total_paid_amount) - Number(paymentData[x]["subscription.amountPaidSoFar"]);
                        }
                        paymentData[x]["getTotalPaid"] = getTotalPaid;
                    } else {
                        let new_data = '';
                        let cal_new_amount = '';
                        if (paymentData[x]["subscription.status_amountPaidSoFar"] == 1) {
                            let date_check = (paymentData[x]['subscription.clientJoiningDate'] != null) ? paymentData[x]['subscription.clientJoiningDate'] : paymentData[x]['subscription.subscriptionDateTime'];
                            cal_new_amount = await this.getTotalShowData(information, paymentData[x]['subscriptionsId'], date_check);
                            new_data = Number(cal_new_amount[0].new_balance) - Number(paymentData[x]["subscription.amountPaidSoFar"]);
                        } else {
                            // let date_check = (paymentData[x]['subscription.clientJoiningDate'] != null) ? paymentData[x]['subscription.clientJoiningDate'] : paymentData[x]['subscription.subscriptionDateTime'];
                            let date_check = paymentData[x]['payment_end_date'];
                            cal_new_amount = await this.getTotalShowData(information, paymentData[x]['subscriptionsId'], date_check);
                            new_data = Number(cal_new_amount[0].new_balance);
                        }
                        paymentData[x]["getTotalShowData"] = [{
                            new_balance: new_data
                        }];
                        paymentData[x]["getTotalDue"] = await this.getTotalDue(information, paymentData[x]['subscriptionsId']);
                        let getTotalPaid = await this.getTotalPaid(information, paymentData[x]['subscriptionsId']);
                        if (paymentData[x]["subscription.status_amountPaidSoFar"] == '1') {
                            getTotalPaid[0].total_paid_amount = Number(getTotalPaid[0].total_paid_amount);
                        } else {
                            getTotalPaid[0].total_paid_amount = Number(getTotalPaid[0].total_paid_amount) - Number(paymentData[x]["subscription.amountPaidSoFar"]);
                        }
                        paymentData[x]["getTotalPaid"] = getTotalPaid;

                    }
                    let listclientPayments = await dbclientpayments.clientpayments.findAll({
                        where: {
                            businessId: businessIdinformation,
                            subscriptionsId: paymentData[x]['subscriptionsId'],
                            status: 1,
                            "$payble_amount$": {
                                ne: null
                            }
                        },
                        raw: true
                    });
                    paymentData[x]["subcription_payment_status"] = listclientPayments.length;
                }
            }
            dbclientpayments.sequelize.close();
            dbClients.sequelize.close();
            dbsubscriptions.sequelize.close();
            dbpricepack.sequelize.close();
            dbpaymentTransactions.sequelize.close();
            dbclientpayments.sequelize.close();
            dbservicepricepackmap.sequelize.close();
            dbservices.sequelize.close();
            total_record.count = paymentData.length;
            total_record.rows = paymentData;
            return total_record;


        } catch (error) {
            throw error;
        }
    };

    // listClientpaymentHistory = async(
    //     information,
    //     params,
    //     filterPrivateFields = true
    // ) => {
    //     try {
    //         var mergedAllActiveClients = [];
    //         let todayDate = await hyphenDateFormat(new Date);
    //         // let todayDate = '2018-07-04';
    //         var businessIdinformation = information.split(",")[1];
    //         let dbclientpayments = createInstance(["clientPayments"], information);
    //         let dbsubscriptions = createInstance(["clientSubscription"], information);
    //         let dbClients = createInstance(["client"], information);
    //         let dbpricepack = createInstance(["clientPricepack"], information);
    //         let dbclientexpense = createInstance(["clientExpense"], information);
    //         let dbpaymentTransactions = createInstance(["paymentTransactions"], information);
    //         let dbclientScheduleMap = createInstance(["clientScheduleMap"], information);
    //         dbclientpayments.clientpayments.belongsTo(dbsubscriptions.subscriptions, {
    //             foreignKey: "subscriptionsId",
    //             targetKey: "subscriptionId"
    //         });
    //         dbsubscriptions.subscriptions.belongsTo(dbClients.clients, {
    //             foreignKey: "clientId",
    //             targetKey: "clientId"
    //         });
    //         dbsubscriptions.subscriptions.belongsTo(dbpricepack.pricepacks, {
    //             foreignKey: "pricepacks",
    //             targetKey: "pricepackId"
    //         });
    //         dbclientpayments.clientpayments.belongsTo(dbpaymentTransactions.paymentTransactions, {
    //             foreignKey: "clientpaymentId",
    //             targetKey: "clientpaymentId"
    //         });
    //         dbsubscriptions.subscriptions.belongsTo(dbclientScheduleMap.clientschedulemap, {
    //             foreignKey: "parentSubscriptionId",
    //             targetKey: "subscriptionId"
    //         });

    //         let filter = params[0] ? params[0] : "";
    //         var pageNo = params["page"] ? params["page"] : 1;
    //         var limitPerPage = PEGINATION_LIMIT;
    //         var offsetLimit = (pageNo - 1) * limitPerPage;

    //         let clientpaymentsRiminder = {};
    //         let recentIncomeclientpayments = {};
    //         let recentExpenseclientExpense = {};

    //         let itemclientpaymentsRiminderData = {
    //             status: 1,
    //             businessId: businessIdinformation,
    //             clientpaymentId: {
    //                 [dbclientpayments.sequelize.Op.in]: [Sequelize.literal("(SELECT MIN(`clientpaymentId`) FROM `clientpayments` WHERE `payment_due_date` >= '" + todayDate + "' AND `payment_end_date` >= '" + todayDate + "' AND `status` = '1' GROUP BY `subscriptionsId`)")]
    //             },
    //             [dbClients.sequelize.Op.or]: [{
    //                     "$subscription.client.clientName$": {
    //                         like: "%" + filter + "%"
    //                     }
    //                 },
    //                 {
    //                     "$subscription.pricepack.pricepackName$": {
    //                         like: "%" + filter + "%"
    //                     }
    //                 }
    //             ],
    //             [dbclientScheduleMap.sequelize.Op.and]: [Sequelize.literal("( ( `subscription->clientschedulemap`.`clientScheduleMapId` IS NULL OR `subscription->clientschedulemap`.`sessionCount` IS NULL ) OR ( `subscription->clientschedulemap`.`sessionCount` <= `subscription->pricepack`.`serviceDuration` ) )")],
    //         };
    //         let itemGroupConditions = {
    //             status: 1,
    //             businessId: businessIdinformation,
    //         };
    //         let clientpaymentsRiminderData = await dbclientpayments.clientpayments.findAndCount({
    //             attributes: [
    //                 "clientpaymentId",
    //                 "subscriptionsId",
    //                 "due_amount",
    //                 "payment_due_date",
    //                 "payment_end_date", ["UpdateDateTime", "updated_at"],
    //                 "paymentStatus", [dbclientpayments.sequelize.literal("Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day )"), 'subcription_end_Date']
    //             ],
    //             include: [{
    //                 model: dbsubscriptions.subscriptions,
    //                 attributes: [
    //                     "clientId",
    //                     "amountPaidSoFar",
    //                     "status_amountPaidSoFar",
    //                     "clientJoiningDate",
    //                     "subscriptionDateTime",
    //                     "installment_frequency",
    //                 ],
    //                 include: [{
    //                         model: dbClients.clients,
    //                         attributes: {
    //                             include: [
    //                                 "clientName", [
    //                                     dbClients.clients.sequelize.literal(
    //                                         'CASE WHEN photoUrl  is NULL THEN CONCAT("' +
    //                                         KNACK_UPLOAD_URL.localUrl +
    //                                         '" , "noimage.jpg" ) ELSE CONCAT("' +
    //                                         KNACK_UPLOAD_URL.localUrl +
    //                                         '" , photoUrl ) END'
    //                                     ),
    //                                     "photoUrl"
    //                                 ],
    //                             ],
    //                             exclude: []
    //                         },
    //                         where: itemGroupConditions
    //                     },
    //                     {
    //                         model: dbpricepack.pricepacks,
    //                         attributes: ["pricepackName"],
    //                         where: {}
    //                     },
    //                     {
    //                         model: dbclientScheduleMap.clientschedulemap,
    //                         required: false,
    //                         attributes: ["sessionCount"],
    //                         where: {}
    //                     }
    //                 ],
    //                 where: {}
    //             }, {
    //                 model: dbpaymentTransactions.paymentTransactions,
    //                 required: true,
    //                 attributes: [
    //                     "payment_date", ['paid_amount', 'payble_amount'],
    //                 ],
    //             }],
    //             order: [
    //                 ["CreateDateTime", "DESC"]
    //             ],
    //             where: itemclientpaymentsRiminderData,
    //             subQuery: false,
    //             offset: offsetLimit,
    //             limit: limitPerPage,
    //             raw: true
    //         });

    //         if (clientpaymentsRiminderData.rows.length > 0) {
    //             for (let x = 0; x < clientpaymentsRiminderData.rows.length; x++) {
    //                 clientpaymentsRiminderData.rows[x]["sessionInfo"] = await sessionInfo(information, {
    //                     status: 1,
    //                     subscriptionId: clientpaymentsRiminderData.rows[x]['subscriptionsId']
    //                 });
    //             }
    //         }

    //         return clientpaymentsRiminderData;

    //         // if (clientpaymentsRiminderData.rows.length > 0) {
    //         //     for (let x = 0; x < clientpaymentsRiminderData.rows.length; x++) {
    //         //         clientpaymentsRiminderData.rows[x]["sessionInfo"] = await sessionInfo(information, {
    //         //             status: 1,
    //         //             subscriptionId: clientpaymentsRiminderData.rows[x]['subscriptionsId']
    //         //         });
    //         //         if (clientpaymentsRiminderData.rows[x]["subscription.installment_frequency"] == '6') {
    //         //             // let new_data = '';
    //         //             // let cal_new_amount = '';
    //         //             // if (clientpaymentsRiminderData.rows[x]["subscription.status_amountPaidSoFar"] == 1) {
    //         //             //     let date_check = (clientpaymentsRiminderData.rows[x]['subscription.clientJoiningDate'] != null) ? clientpaymentsRiminderData.rows[x]['subscription.clientJoiningDate'] : clientpaymentsRiminderData.rows[x]['subscription.subscriptionDateTime'];
    //         //             //     cal_new_amount = await this.getTotalShowDataOthers(information, clientpaymentsRiminderData.rows[x]['subscriptionsId'], date_check);
    //         //             //     new_data = Number(cal_new_amount[0].new_balance) - Number(clientpaymentsRiminderData.rows[x]["subscription.amountPaidSoFar"]);
    //         //             // } else {
    //         //             //     let date_check = (clientpaymentsRiminderData.rows[x]['subscription.clientJoiningDate'] != null) ? clientpaymentsRiminderData.rows[x]['subscription.clientJoiningDate'] : clientpaymentsRiminderData.rows[x]['subscription.subscriptionDateTime'];
    //         //             //     cal_new_amount = await this.getTotalShowDataOthers(information, clientpaymentsRiminderData.rows[x]['subscriptionsId'], date_check);
    //         //             //     new_data = Number(cal_new_amount[0].new_balance);
    //         //             // }
    //         //             // clientpaymentsRiminderData.rows[x]["getTotalShowData"] = [{
    //         //             //     new_balance: new_data
    //         //             // }];
    //         //             // clientpaymentsRiminderData.rows[x]["getTotalDue"] = await this.getTotalDue(information, clientpaymentsRiminderData.rows[x]['subscriptionsId']);
    //         //             // let getTotalPaid = await this.getTotalPaid(information, clientpaymentsRiminderData.rows[x]['subscriptionsId']);
    //         //             // if (clientpaymentsRiminderData.rows[x]["subscription.status_amountPaidSoFar"] == '1') {
    //         //             //     getTotalPaid[0].total_paid_amount = Number(getTotalPaid[0].total_paid_amount);
    //         //             // } else {
    //         //             //     getTotalPaid[0].total_paid_amount = Number(getTotalPaid[0].total_paid_amount) - Number(clientpaymentsRiminderData.rows[x]["subscription.amountPaidSoFar"]);
    //         //             // }
    //         //             clientpaymentsRiminderData.rows[x]["getTotalPaid"] = await this.getAmountPaid(information, clientpaymentsRiminderData.rows[x]['subscriptionsId']);
    //         //         } else {
    //         //             // let new_data = '';
    //         //             // let cal_new_amount = '';
    //         //             // if (clientpaymentsRiminderData.rows[x]["subscription.status_amountPaidSoFar"] == 1) {
    //         //             //     let date_check = (clientpaymentsRiminderData.rows[x]['subscription.clientJoiningDate'] != null) ? clientpaymentsRiminderData.rows[x]['subscription.clientJoiningDate'] : clientpaymentsRiminderData.rows[x]['subscription.subscriptionDateTime'];
    //         //             //     cal_new_amount = await this.getTotalShowData(information, clientpaymentsRiminderData.rows[x]['subscriptionsId'], date_check);
    //         //             //     new_data = Number(cal_new_amount[0].new_balance) - Number(clientpaymentsRiminderData.rows[x]["subscription.amountPaidSoFar"]);
    //         //             // } else {
    //         //             //     let date_check = (clientpaymentsRiminderData.rows[x]['subscription.clientJoiningDate'] != null) ? clientpaymentsRiminderData.rows[x]['subscription.clientJoiningDate'] : clientpaymentsRiminderData.rows[x]['subscription.subscriptionDateTime'];
    //         //             //     cal_new_amount = await this.getTotalShowData(information, clientpaymentsRiminderData.rows[x]['subscriptionsId'], date_check);
    //         //             //     new_data = Number(cal_new_amount[0].new_balance);
    //         //             // }
    //         //             // clientpaymentsRiminderData.rows[x]["getTotalShowData"] = [{
    //         //             //     new_balance: new_data
    //         //             // }];
    //         //             // clientpaymentsRiminderData.rows[x]["getTotalDue"] = await this.getTotalDue(information, clientpaymentsRiminderData.rows[x]['subscriptionsId']);
    //         //             // let getTotalPaid = await this.getTotalPaid(information, clientpaymentsRiminderData.rows[x]['subscriptionsId']);
    //         //             // if (clientpaymentsRiminderData.rows[x]["subscription.status_amountPaidSoFar"] == '1') {
    //         //             //     getTotalPaid[0].total_paid_amount = Number(getTotalPaid[0].total_paid_amount);
    //         //             // } else {
    //         //             //     getTotalPaid[0].total_paid_amount = Number(getTotalPaid[0].total_paid_amount) - Number(clientpaymentsRiminderData.rows[x]["subscription.amountPaidSoFar"]);
    //         //             // }
    //         //             clientpaymentsRiminderData.rows[x]["getTotalPaid"] = await this.getAmountPaid(information, clientpaymentsRiminderData.rows[x]['subscriptionsId']);

    //         //         }
    //         //         let listclientPayments = await dbclientpayments.clientpayments.findAll({
    //         //             where: {
    //         //                 businessId: businessIdinformation,
    //         //                 subscriptionsId: clientpaymentsRiminderData.rows[x]['subscriptionsId'],
    //         //                 status: 1,
    //         //                 "$payble_amount$": {
    //         //                     ne: null
    //         //                 }
    //         //             },
    //         //             raw: true
    //         //         });
    //         //         clientpaymentsRiminderData.rows[x]["subcription_payment_status"] = listclientPayments.length;
    //         //     }
    //         // }
    //         // clientpaymentsRiminder.count = clientpaymentsRiminderData.count;
    //         // clientpaymentsRiminder.rows = clientpaymentsRiminderData.rows;
    //         // return await clientpaymentsRiminder;
    //     } catch (error) {
    //         throw error;
    //     }
    // };


    listClientpaymentHistory = async(
        information,
        params,
        filterPrivateFields = true
    ) => {
        try {
            var mergedAllActiveClients = [];
            let todayDate = await hyphenDateFormat(new Date);
            // let todayDate = '2018-07-04';
            var businessIdinformation = information.split(",")[1];
            let dbclientpayments = createInstance(["clientPayments"], information);
            let dbsubscriptions = createInstance(["clientSubscription"], information);
            let dbClients = createInstance(["client"], information);
            let dbpricepack = createInstance(["clientPricepack"], information);
            let dbclientexpense = createInstance(["clientExpense"], information);
            let dbpaymentTransactions = createInstance(["paymentTransactions"], information);
            let dbclientScheduleMap = createInstance(["clientScheduleMap"], information);
            dbclientpayments.clientpayments.belongsTo(dbsubscriptions.subscriptions, {
                foreignKey: "subscriptionsId",
                targetKey: "subscriptionId"
            });
            dbsubscriptions.subscriptions.belongsTo(dbClients.clients, {
                foreignKey: "clientId",
                targetKey: "clientId"
            });
            dbsubscriptions.subscriptions.belongsTo(dbpricepack.pricepacks, {
                foreignKey: "pricepacks",
                targetKey: "pricepackId"
            });
            dbclientpayments.clientpayments.belongsTo(dbpaymentTransactions.paymentTransactions, {
                foreignKey: "clientpaymentId",
                targetKey: "clientpaymentId"
            });
            dbsubscriptions.subscriptions.belongsTo(dbclientScheduleMap.clientschedulemap, {
                foreignKey: "parentSubscriptionId",
                targetKey: "subscriptionId"
            });
            dbpaymentTransactions.paymentTransactions.belongsTo(dbsubscriptions.subscriptions, {
                foreignKey: "subscriptionsId",
                targetKey: "subscriptionId"
            });

            let filter = params[0] ? params[0] : "";
            var pageNo = params["page"] ? params["page"] : 1;
            var limitPerPage = PEGINATION_LIMIT;
            var offsetLimit = (pageNo - 1) * limitPerPage;


            let itemclientpaymentsRiminderData = {
                status: 1,
                [dbpaymentTransactions.sequelize.Op.or]: [{
                        "$subscription.client.clientName$": {
                            like: "%" + filter + "%"
                        }
                    },
                    {
                        "$subscription.pricepack.pricepackName$": {
                            like: "%" + filter + "%"
                        }
                    }
                ],
            };
            let clientpaymentsRiminderData = await dbpaymentTransactions.paymentTransactions.findAndCount({
                attributes: {
                    include: [],
                    exclude: []
                },
                include: [{
                    model: dbsubscriptions.subscriptions,
                    attributes: {
                        include: [],
                        exclude: ["createDateTime", "updateDateTime"]
                    },
                    include: [{
                            model: dbClients.clients,
                            attributes: {
                                include: [
                                    [
                                        dbClients.clients.sequelize.literal(
                                            'CASE WHEN photoUrl  is NULL THEN CONCAT("' +
                                            KNACK_UPLOAD_URL.localUrl +
                                            '" , "noimage.jpg" ) ELSE CONCAT("' +
                                            KNACK_UPLOAD_URL.localUrl +
                                            '" , photoUrl ) END'
                                        ),
                                        "photoUrl"
                                    ],
                                ],
                                exclude: ["createDateTime", "updateDateTime"]
                            },
                            where: {
                                status: 1,
                                businessId: businessIdinformation,
                            }
                        },
                        {
                            model: dbpricepack.pricepacks,
                            attributes: ["pricepackName"],
                            where: {},
                        }
                    ],
                    where: {},
                }],
                order: [
                    ["CreateDateTime", "DESC"]
                ],
                where: itemclientpaymentsRiminderData,
                offset: offsetLimit,
                limit: limitPerPage,
                raw: true
            });

            if (clientpaymentsRiminderData.rows.length > 0) {
                for (let x = 0; x < clientpaymentsRiminderData.rows.length; x++) {
                    clientpaymentsRiminderData.rows[x]["sessionInfo"] = await sessionInfo(information, {
                        status: 1,
                        subscriptionId: clientpaymentsRiminderData.rows[x]['subscriptionsId']
                    });
                }
            }


            dbclientpayments.sequelize.close();
            dbsubscriptions.sequelize.close();
            dbClients.sequelize.close();
            dbpricepack.sequelize.close();
            dbclientexpense.sequelize.close();
            dbpaymentTransactions.sequelize.close();
            dbclientScheduleMap.sequelize.close();

            return clientpaymentsRiminderData;
        } catch (error) {
            throw error;
        }
    };



    clientPaymenthistoryList = async(
        information,
        clientId,
        params,
        filterPrivateFields = true
    ) => {
        try {
            var mergedAllActiveClients = [];
            let todayDate = await hyphenDateFormat(new Date);
            // let todayDate = '2018-07-04';
            var businessIdinformation = information.split(",")[1];
            let dbclientpayments = createInstance(["clientPayments"], information);
            let dbsubscriptions = createInstance(["clientSubscription"], information);
            let dbClients = createInstance(["client"], information);
            let dbpricepack = createInstance(["clientPricepack"], information);
            let dbclientexpense = createInstance(["clientExpense"], information);
            let dbpaymentTransactions = createInstance(["paymentTransactions"], information);
            let dbclientScheduleMap = createInstance(["clientScheduleMap"], information);
            dbclientpayments.clientpayments.belongsTo(dbsubscriptions.subscriptions, {
                foreignKey: "subscriptionsId",
                targetKey: "subscriptionId"
            });
            dbsubscriptions.subscriptions.belongsTo(dbClients.clients, {
                foreignKey: "clientId",
                targetKey: "clientId"
            });
            dbsubscriptions.subscriptions.belongsTo(dbpricepack.pricepacks, {
                foreignKey: "pricepacks",
                targetKey: "pricepackId"
            });
            dbclientpayments.clientpayments.belongsTo(dbpaymentTransactions.paymentTransactions, {
                foreignKey: "clientpaymentId",
                targetKey: "clientpaymentId"
            });
            dbsubscriptions.subscriptions.belongsTo(dbclientScheduleMap.clientschedulemap, {
                foreignKey: "parentSubscriptionId",
                targetKey: "subscriptionId"
            });
            dbpaymentTransactions.paymentTransactions.belongsTo(dbsubscriptions.subscriptions, {
                foreignKey: "subscriptionsId",
                targetKey: "subscriptionId"
            });

            let filter = params[0] ? params[0] : "";
            var pageNo = params["page"] ? params["page"] : 1;
            var limitPerPage = PEGINATION_LIMIT;
            var offsetLimit = (pageNo - 1) * limitPerPage;


            let itemclientpaymentsRiminderData = {
                status: 1,
                [dbpaymentTransactions.sequelize.Op.or]: [{
                        "$subscription.client.clientName$": {
                            like: "%" + filter + "%"
                        }
                    },
                    {
                        "$subscription.pricepack.pricepackName$": {
                            like: "%" + filter + "%"
                        }
                    }
                ],
            };
            let clientpaymentsRiminderData = await dbpaymentTransactions.paymentTransactions.findAndCount({
                attributes: {
                    include: [],
                    exclude: []
                },
                include: [{
                    model: dbsubscriptions.subscriptions,
                    attributes: {
                        include: [],
                        exclude: ["createDateTime", "updateDateTime"]
                    },
                    include: [{
                            model: dbClients.clients,
                            attributes: {
                                include: [
                                    [
                                        dbClients.clients.sequelize.literal(
                                            'CASE WHEN photoUrl  is NULL THEN CONCAT("' +
                                            KNACK_UPLOAD_URL.localUrl +
                                            '" , "noimage.jpg" ) ELSE CONCAT("' +
                                            KNACK_UPLOAD_URL.localUrl +
                                            '" , photoUrl ) END'
                                        ),
                                        "photoUrl"
                                    ],
                                ],
                                exclude: ["createDateTime", "updateDateTime"]
                            },
                            where: {
                                status: 1,
                                clientId: clientId,
                                businessId: businessIdinformation,
                            }
                        },
                        {
                            model: dbpricepack.pricepacks,
                            attributes: ["pricepackName"],
                            where: {},
                        }
                    ],
                    where: {},
                }],
                order: [
                    ["CreateDateTime", "DESC"]
                ],
                where: itemclientpaymentsRiminderData,
                offset: offsetLimit,
                limit: limitPerPage,
                raw: true
            });

            if (clientpaymentsRiminderData.rows.length > 0) {
                for (let x = 0; x < clientpaymentsRiminderData.rows.length; x++) {
                    clientpaymentsRiminderData.rows[x]["sessionInfo"] = await sessionInfo(information, {
                        status: 1,
                        subscriptionId: clientpaymentsRiminderData.rows[x]['subscriptionsId']
                    });
                }
            }


            dbclientpayments.sequelize.close();
            dbsubscriptions.sequelize.close();
            dbClients.sequelize.close();
            dbpricepack.sequelize.close();
            dbclientexpense.sequelize.close();
            dbpaymentTransactions.sequelize.close();
            dbclientScheduleMap.sequelize.close();

            return clientpaymentsRiminderData;


        } catch (error) {
            throw error;
        }
    };

    listRecentIncome = async(
        information,
        params,
        filterPrivateFields = true
    ) => {
        try {

            var mergedAllActiveClients = [];
            let todayDate = await hyphenDateFormat(new Date);
            // let todayDate = '2018-07-04';
            var businessIdinformation = information.split(",")[1];
            let dbclientpayments = createInstance(["clientPayments"], information);
            let dbsubscriptions = createInstance(["clientSubscription"], information);
            let dbClients = createInstance(["client"], information);
            let dbpricepack = createInstance(["clientPricepack"], information);
            let dbclientexpense = createInstance(["clientExpense"], information);
            let dbpaymentTransactions = createInstance(["paymentTransactions"], information);
            let dbclientScheduleMap = createInstance(["clientScheduleMap"], information);
            dbclientpayments.clientpayments.belongsTo(dbsubscriptions.subscriptions, {
                foreignKey: "subscriptionsId",
                targetKey: "subscriptionId"
            });
            dbsubscriptions.subscriptions.belongsTo(dbClients.clients, {
                foreignKey: "clientId",
                targetKey: "clientId"
            });
            dbsubscriptions.subscriptions.belongsTo(dbpricepack.pricepacks, {
                foreignKey: "pricepacks",
                targetKey: "pricepackId"
            });
            dbclientpayments.clientpayments.belongsTo(dbpaymentTransactions.paymentTransactions, {
                foreignKey: "clientpaymentId",
                targetKey: "clientpaymentId"
            });
            dbsubscriptions.subscriptions.belongsTo(dbclientScheduleMap.clientschedulemap, {
                foreignKey: "parentSubscriptionId",
                targetKey: "subscriptionId"
            });
            dbpaymentTransactions.paymentTransactions.belongsTo(dbsubscriptions.subscriptions, {
                foreignKey: "subscriptionsId",
                targetKey: "subscriptionId"
            });

            let filter = params[0] ? params[0] : "";
            var pageNo = params["page"] ? params["page"] : 1;
            var limitPerPage = PEGINATION_LIMIT;
            var offsetLimit = (pageNo - 1) * limitPerPage;


            let itemclientpaymentsRiminderData = {
                status: 1,
                [dbpaymentTransactions.sequelize.Op.or]: [{
                        "$subscription.client.clientName$": {
                            like: "%" + filter + "%"
                        }
                    },
                    {
                        "$subscription.pricepack.pricepackName$": {
                            like: "%" + filter + "%"
                        }
                    }
                ],
            };
            let clientpaymentsRiminderData = await dbpaymentTransactions.paymentTransactions.findAndCount({
                attributes: {
                    include: [],
                    exclude: []
                },
                include: [{
                    model: dbsubscriptions.subscriptions,
                    attributes: {
                        include: [],
                        exclude: ["createDateTime", "updateDateTime"]
                    },
                    include: [{
                            model: dbClients.clients,
                            attributes: {
                                include: [
                                    [
                                        dbClients.clients.sequelize.literal(
                                            'CASE WHEN photoUrl  is NULL THEN CONCAT("' +
                                            KNACK_UPLOAD_URL.localUrl +
                                            '" , "noimage.jpg" ) ELSE CONCAT("' +
                                            KNACK_UPLOAD_URL.localUrl +
                                            '" , photoUrl ) END'
                                        ),
                                        "photoUrl"
                                    ],
                                ],
                                exclude: ["createDateTime", "updateDateTime"]
                            },
                            where: {
                                status: 1,
                                businessId: businessIdinformation,
                            }
                        },
                        {
                            model: dbpricepack.pricepacks,
                            attributes: ["pricepackName"],
                            where: {},
                        }
                    ],
                    where: {},
                }],
                order: [
                    ["CreateDateTime", "DESC"]
                ],
                where: itemclientpaymentsRiminderData,
                offset: offsetLimit,
                limit: limitPerPage,
                raw: true
            });

            if (clientpaymentsRiminderData.rows.length > 0) {
                for (let x = 0; x < clientpaymentsRiminderData.rows.length; x++) {
                    clientpaymentsRiminderData.rows[x]["sessionInfo"] = await sessionInfo(information, {
                        status: 1,
                        subscriptionId: clientpaymentsRiminderData.rows[x]['subscriptionsId']
                    });
                }
            }

            return clientpaymentsRiminderData;
        } catch (error) {
            throw error;
        }
    };

    list = async(information, centerId, filterPrivateFields = true) => {
        try {
            let db = createInstance(["clientSubscription"], information);
            db.subscriptions.belongsTo(db.clients, {
                foreignKey: "clientId",
                targetKey: "clientId"
            });
            db.subscriptions.belongsTo(db.centers, {
                foreignKey: "centerId",
                targetKey: "centerId"
            });
            db.subscriptions.belongsTo(db.pricepacks, {
                foreignKey: "pricepacks",
                targetKey: "pricepackId"
            });
            let subscriptions = await db.subscriptions.findAll({
                where: {
                    status: 1,
                    centerId: centerId
                },
                include: [{
                        model: db.clients,
                        attributes: ["clientName"]
                    },
                    {
                        model: db.centers,
                        attributes: ["centerName"]
                    },
                    {
                        model: db.pricepacks,
                        attributes: ["pricepackName", "serviceName"]
                    }
                ],
                raw: true
            });
            db.sequelize.close();
            if (filterPrivateFields) {
                return await filterFields(subscriptions, PUBLIC_FIELDS);
            }
        } catch (error) {
            throw error;
        }
    };

    clientSubscription = async(
        information,
        clientId,
        filterPrivateFields = true
    ) => {
        try {
            let db = createInstance(["clientSubscription"], information);
            db.subscriptions.belongsTo(db.clients, {
                foreignKey: "clientId",
                targetKey: "clientId"
            });
            db.subscriptions.belongsTo(db.centers, {
                foreignKey: "centerId",
                targetKey: "centerId"
            });
            db.subscriptions.belongsTo(db.pricepacks, {
                foreignKey: "pricepacks",
                targetKey: "pricepackId"
            });
            let subscriptions = await db.subscriptions.findAll({
                where: {
                    status: 1,
                    clientId: clientId
                },
                include: [{
                        model: db.clients,
                        attributes: ["clientName"]
                    },
                    {
                        model: db.centers,
                        attributes: ["centerName"]
                    },
                    {
                        model: db.pricepacks,
                        attributes: ["pricepackName", "serviceName"]
                    }
                ],
                raw: true
            });
            db.sequelize.close();
            if (filterPrivateFields) {
                return await filterFields(subscriptions, PUBLIC_FIELDS);
            }
        } catch (error) {
            throw error;
        }
    };

    details = async(information, subscriptionId, filterPrivateFields = true) => {
        try {
            let db = createInstance(
                ["clientSubscription", "client", "clientCenter", "clientService"],
                information
            );
            db.subscriptions.belongsTo(db.clients, {
                foreignKey: "clientId",
                targetKey: "clientId"
            });
            db.subscriptions.belongsTo(db.centers, {
                foreignKey: "centerId",
                targetKey: "centerId"
            });
            db.subscriptions.belongsTo(db.pricepacks, {
                foreignKey: "pricepacks",
                targetKey: "pricepackId"
            });
            let subscriptions = await db.subscriptions.findAll({
                where: {
                    status: 1,
                    subscriptionId: subscriptionId
                },
                include: [{
                        model: db.clients,
                        attributes: ["clientName"]
                    },
                    {
                        model: db.centers,
                        attributes: ["centerName"]
                    },
                    {
                        model: db.pricepacks,
                        attributes: ["pricepackName", "serviceName"]
                    }
                ],
                raw: true
            });
            db.sequelize.close();
            if (filterPrivateFields) {
                return await filterFields(subscriptions, PUBLIC_FIELDS);
            }
        } catch (error) {
            throw error;
        }
    };

    update = async(
        information,
        subscriptionId,
        clientId,
        businessId,
        centerId,
        services,
        pricepacks,
        totalSessionUnit,
        totalSessionType,
        offerId,
        taxGST,
        taxSGST,
        totalAmount,
        paymentType,
        numberOfInstallments,
        paymentDueDate,
        subscriptionDateTime
    ) => {
        try {
            let db = createInstance(["clientSubscription"], information);
            let subscriptions = await db.subscriptions.update({
                clientId: clientId,
                businessId: businessId,
                centerId: centerId,
                services: services,
                pricepacks: pricepacks,
                totalSessionUnit: totalSessionUnit,
                totalSessionType: totalSessionType,
                offerId: offerId,
                taxGST: taxGST,
                taxSGST: taxSGST,
                totalAmount: totalAmount,
                paymentType: paymentType,
                numberOfInstallments: numberOfInstallments,
                paymentDueDate: paymentDueDate,
                subscriptionDateTime: subscriptionDateTime,
                updateDateTime: Date.now()
            }, {
                where: {
                    subscriptionId: subscriptionId,
                    status: 1
                }
            });
            db.sequelize.close();
            return subscriptions;
        } catch (error) {
            throw error;
        }
    };

    remove = async(information, subscriptionId) => {
        try {
            let db = createInstance(["clientSubscription"], information);
            let subscriptions = await db.subscriptions.update({
                status: 0,
                updateDateTime: Date.now()
            }, {
                where: {
                    subscriptionId: subscriptionId
                }
            });
            db.sequelize.close();
            return subscriptions;
        } catch (error) {
            throw error;
        }
    };

    listAllSubscriptionSchedule = async(
        information,
        businessId,
        clientId,
        filterPrivateFields = true
    ) => {
        try {
            let db = createInstance(["clientSubscription"], information);
            let subscriptions = await db.subscriptions.findAll({
                where: {
                    status: 1,
                    businessId: businessId,
                    clientId: clientId
                },
                raw: true
            });

            var dbClient = createInstance(["client"], information);
            var clientDetails = await dbClient.clients.findAll({
                where: {
                    status: 1,
                    clientId: clientId
                },
                raw: true
            });

            var merged = [{
                    clientDetails: clientDetails
                },
                {
                    subscriptions: subscriptions
                }
            ];

            db.sequelize.close();
            return await merged;
            // if (filterPrivateFields) {
            //   return await filterFields(subscriptions, PUBLIC_FIELDS);
            // }
        } catch (error) {
            throw error;
        }
    };

    expenseIncomeDashboard = async(information, filterPrivateFields = true) => {
        try {
            var mergedAllActiveClients = [];
            let todayDate = await hyphenDateFormat(new Date);
            var businessIdinformation = information.split(",")[1];
            let dbclientpayments = createInstance(["clientPayments"], information);
            let dbsubscriptions = createInstance(["clientSubscription"], information);
            let dbClients = createInstance(["client"], information);
            let dbpricepack = createInstance(["clientPricepack"], information);
            let dbclientexpense = createInstance(["clientExpense"], information);
            let dbpaymentTransactions = createInstance(["paymentTransactions"], information);
            let dbcenters = createInstance(["clientCenter"], information);

            dbclientpayments.clientpayments.belongsTo(dbsubscriptions.subscriptions, {
                foreignKey: "subscriptionsId",
                targetKey: "subscriptionId"
            });
            dbsubscriptions.subscriptions.belongsTo(dbClients.clients, {
                foreignKey: "clientId",
                targetKey: "clientId"
            });
            dbsubscriptions.subscriptions.belongsTo(dbpricepack.pricepacks, {
                foreignKey: "pricepacks",
                targetKey: "pricepackId"
            });
            dbclientpayments.clientpayments.belongsTo(dbpaymentTransactions.paymentTransactions, {
                foreignKey: "clientpaymentId",
                targetKey: "clientpaymentId"
            });
            dbpaymentTransactions.paymentTransactions.belongsTo(dbsubscriptions.subscriptions, {
                foreignKey: "subscriptionsId",
                targetKey: "subscriptionId"
            });

            dbclientexpense.clientexpense.belongsTo(dbcenters.centers, {
                foreignKey: "centerId",
                targetKey: "centerId"
            });

            var itemConditions = {};
            var itemGroupConditions = {};
            var itemGroupConditionsPricePack = {};


            let clientpaymentsIncome = await dbclientpayments.clientpayments.findAll({
                attributes: [
                    [
                        dbclientpayments.sequelize.fn(
                            "sum",
                            dbclientpayments.sequelize.col("payble_amount")
                        ),
                        "totalIncome"
                    ],
                    [
                        dbclientpayments.sequelize.fn(
                            "FORMAT",
                            dbclientpayments.sequelize.fn(
                                "sum",
                                dbclientpayments.sequelize.col("payble_amount")
                            ),
                            2
                        ),
                        "formated_totalIncome"
                    ]
                ],
                include: [{
                    model: dbpaymentTransactions.paymentTransactions,
                    attributes: []
                }],
                where: {
                    status: 1,
                    $paymentStatus$: { in: [1, 2]
                    },
                    businessId: businessIdinformation,
                    payment_date: dbclientpayments.sequelize.where(
                        dbclientpayments.sequelize.fn(
                            "YEAR",
                            dbclientpayments.sequelize.col("payment_date")
                        ),
                        dbclientpayments.sequelize.fn(
                            "YEAR",
                            dbclientpayments.sequelize.fn("CURDATE")
                        )
                    ),
                    // [dbpaymentTransactions.sequelize.Op.and]: [{
                    //     $clientpaymentId$: { in: dbpaymentTransactions.sequelize.literal("(SELECT `clientpaymentId` FROM `clientpayments` WHERE `subscriptionsId` = '" + subscriptionId + "')")
                    //     }
                    // }]
                },
                raw: true
            });


            let clientpaymentsExpense = await dbclientexpense.clientexpense.findAll({
                attributes: [
                    [
                        dbclientexpense.sequelize.fn(
                            "sum",
                            dbclientexpense.sequelize.col("amount")
                        ),
                        "totalExpense"
                    ],
                    [
                        dbclientpayments.sequelize.fn(
                            "FORMAT",
                            dbclientpayments.sequelize.fn(
                                "sum",
                                dbclientpayments.sequelize.col("amount")
                            ),
                            2
                        ),
                        "formated_totalExpense"
                    ]
                ],
                where: {
                    status: 1,
                    businessId: businessIdinformation,
                    payment_date: dbclientexpense.sequelize.where(
                        dbclientexpense.sequelize.fn(
                            "YEAR",
                            dbclientexpense.sequelize.col("dateOfPayment")
                        ),
                        dbclientexpense.sequelize.fn(
                            "YEAR",
                            dbclientexpense.sequelize.fn("CURDATE")
                        )
                    )
                },
                raw: true
            });

            //  ======================================clientpaymentsRiminderData Start ========================================================
            let clientpaymentsRiminder = {};
            let dbclientScheduleMap = createInstance(["clientScheduleMap"], information);
            dbclientpayments.clientpayments.belongsTo(dbsubscriptions.subscriptions, {
                foreignKey: "subscriptionsId",
                targetKey: "subscriptionId"
            });
            dbsubscriptions.subscriptions.belongsTo(dbClients.clients, {
                foreignKey: "clientId",
                targetKey: "clientId"
            });
            dbsubscriptions.subscriptions.belongsTo(dbpricepack.pricepacks, {
                foreignKey: "pricepacks",
                targetKey: "pricepackId"
            });
            dbclientpayments.clientpayments.belongsTo(dbpaymentTransactions.paymentTransactions, {
                foreignKey: "clientpaymentId",
                targetKey: "clientpaymentId"
            });
            dbsubscriptions.subscriptions.belongsTo(dbclientScheduleMap.clientschedulemap, {
                foreignKey: "subscriptionId",
                targetKey: "subscriptionId"
            });

            let filter = "";


            let recentIncomeclientpayments = {};
            let recentExpenseclientExpense = {};

            let itemclientpaymentsRiminderData = {
                status: 1,
                businessId: businessIdinformation,
                clientpaymentId: {
                    [dbclientpayments.sequelize.Op.in]:
                    // [Sequelize.literal("(SELECT MIN(`clientpaymentId`) FROM `clientpayments` WHERE `payment_due_date` >= '" + todayDate + "' AND `payment_end_date` >= '" + todayDate + "' AND `status` = '1' GROUP BY `subscriptionsId`)")]
                        [Sequelize.literal("(SELECT MIN(`clientpaymentId`) as `clientpaymentId` FROM `clientpayments` INNER JOIN `subscriptions` AS `subscription` ON `clientpayments`.`subscriptionsId` = `subscription`.`subscriptionId` INNER JOIN `pricepacks` AS `subscription->pricepack` ON `subscription`.`pricepacks` = `subscription->pricepack`.`pricepackId` WHERE Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day ) <= '" + todayDate + "' OR ( ( `clientpayments`.`payment_due_date` <= '" + todayDate + "' AND `clientpayments`.`payment_end_date` >= '" + todayDate + "' ) OR ( `clientpayments`.`payment_due_date` >= '" + todayDate + "' AND `clientpayments`.`payment_end_date` >= '" + todayDate + "' ) ) AND `clientpayments`.`status` = '1' GROUP BY `clientpayments`.`subscriptionsId` UNION select c.clientpaymentId from clientpayments as c, ( SELECT a.clientpaymentId, a.curent_subcription_number, a.payment_end_date, COUNT(a.subscriptionsId) FROM clientpayments a LEFT JOIN clientpayments b ON a.subscriptionsId = b.subscriptionsId AND b.status = 1 WHERE b.status = 1 AND a.status = 1 GROUP BY a.subscriptionsId, a.clientpaymentId having a.curent_subcription_number = COUNT(a.subscriptionsId) AND a.payment_end_date < CURDATE() ) as total WHERE c.clientpaymentId IN (total.clientpaymentId))")]
                },
                [dbClients.sequelize.Op.or]: [{
                        "$subscription.client.clientName$": {
                            like: "%" + filter + "%"
                        }
                    },
                    {
                        "$subscription.pricepack.pricepackName$": {
                            like: "%" + filter + "%"
                        }
                    }
                ],
                [dbclientScheduleMap.sequelize.Op.and]: [Sequelize.literal("( ( `subscription->clientschedulemap`.`clientScheduleMapId` IS NULL OR `subscription->clientschedulemap`.`sessionCount` IS NULL ) OR ( `subscription->clientschedulemap`.`sessionCount` < `subscription->pricepack`.`serviceDuration` ) )")],
            };
            let itemGroupConditions = {
                status: 1,
                businessId: businessIdinformation,
            };
            let clientpaymentsRiminderDataCount = await dbclientpayments.clientpayments.findAndCount({
                attributes: [
                    "clientpaymentId",
                    "subscriptionsId",
                    "due_amount",
                    "payble_amount",
                    "payment_due_date",
                    "payment_end_date", ["UpdateDateTime", "updated_at"],
                    "paymentStatus", [dbclientpayments.sequelize.literal("DATE(Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day ))"), 'subcription_end_Date'],
                    "paymentReminder_status",
                    "paymentReminder_date",
                ],
                include: [{
                    model: dbsubscriptions.subscriptions,
                    attributes: [
                        "clientId",
                        "amountPaidSoFar",
                        "status_amountPaidSoFar",
                        "clientJoiningDate",
                        "subscriptionDateTime",
                        "installment_frequency",
                    ],
                    include: [{
                            model: dbClients.clients,
                            attributes: {
                                include: [
                                    "clientName", [
                                        dbClients.clients.sequelize.literal(
                                            'CASE WHEN photoUrl  is NULL THEN CONCAT("' +
                                            KNACK_UPLOAD_URL.localUrl +
                                            '" , "noimage.jpg" ) ELSE CONCAT("' +
                                            KNACK_UPLOAD_URL.localUrl +
                                            '" , photoUrl ) END'
                                        ),
                                        "photoUrl"
                                    ],
                                ],
                                exclude: []
                            },
                            where: itemGroupConditions
                        },
                        {
                            model: dbpricepack.pricepacks,
                            attributes: ["pricepackName"],
                            where: {}
                        },
                        {
                            model: dbclientScheduleMap.clientschedulemap,
                            required: false,
                            attributes: ["sessionCount"],
                            where: {
                                // [dbclientScheduleMap.sequelize.Op.and]: [Sequelize.literal("`subscription->clientschedulemap`.`scheduleId`= `subscription->clientschedulemap`.`parentScheduleId`")],
                                [dbclientScheduleMap.sequelize.Op.and]: [Sequelize.literal("( `subscription->clientschedulemap`.`scheduleId` = `subscription->clientschedulemap`.`parentScheduleId` ) AND ( `subscription->clientschedulemap`.`scheduleId` = (select min(`scheduleId`) from `clientschedulemap` `inSched` where `inSched`.`subscriptionId` = `subscription`.`subscriptionId` ) )")],
                            }
                        }
                    ],
                    where: {}
                }],
                order: [
                    ["CreateDateTime", "DESC"]
                ],
                where: itemclientpaymentsRiminderData,
                subQuery: false,
                raw: true
            });

            let clientpaymentsRiminderData = await dbclientpayments.clientpayments.findAndCount({
                attributes: [
                    "clientpaymentId",
                    "subscriptionsId",
                    "due_amount",
                    "payble_amount",
                    "payment_due_date",
                    "payment_end_date", ["UpdateDateTime", "updated_at"],
                    "paymentStatus", [dbclientpayments.sequelize.literal("DATE(Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day ))"), 'subcription_end_Date'],
                    "paymentReminder_status",
                    "paymentReminder_date",
                ],
                include: [{
                    model: dbsubscriptions.subscriptions,
                    attributes: [
                        "clientId",
                        "amountPaidSoFar",
                        "status_amountPaidSoFar",
                        "clientJoiningDate",
                        "subscriptionDateTime",
                        "installment_frequency",
                    ],
                    include: [{
                            model: dbClients.clients,
                            attributes: {
                                include: [
                                    "clientName", [
                                        dbClients.clients.sequelize.literal(
                                            'CASE WHEN photoUrl  is NULL THEN CONCAT("' +
                                            KNACK_UPLOAD_URL.localUrl +
                                            '" , "noimage.jpg" ) ELSE CONCAT("' +
                                            KNACK_UPLOAD_URL.localUrl +
                                            '" , photoUrl ) END'
                                        ),
                                        "photoUrl"
                                    ],
                                ],
                                exclude: []
                            },
                            where: itemGroupConditions
                        },
                        {
                            model: dbpricepack.pricepacks,
                            attributes: ["pricepackName"],
                            where: {}
                        },
                        {
                            model: dbclientScheduleMap.clientschedulemap,
                            required: false,
                            attributes: ["sessionCount"],
                            where: {}
                        }
                    ],
                    where: {}
                }],
                order: [
                    ["CreateDateTime", "DESC"]
                ],
                where: itemclientpaymentsRiminderData,
                subQuery: false,
                offset: 0,
                limit: 3,
                raw: true
            });
            // if (clientpaymentsRiminderData.rows.length > 0) {
            //     for (let x = 0; x < clientpaymentsRiminderData.rows.length; x++) {
            //         clientpaymentsRiminderData.rows[x]["sessionInfo"] = await sessionInfo(information, {
            //             status: 1,
            //             subscriptionId: clientpaymentsRiminderData.rows[x]['subscriptionsId']
            //         });
            //         if (clientpaymentsRiminderData.rows[x]["subscription.installment_frequency"] == '6') {
            //             let new_data = '';
            //             let cal_new_amount = '';
            //             if (clientpaymentsRiminderData.rows[x]["subscription.status_amountPaidSoFar"] == 1) {
            //                 let date_check = (clientpaymentsRiminderData.rows[x]['subscription.clientJoiningDate'] != null) ? clientpaymentsRiminderData.rows[x]['subscription.clientJoiningDate'] : clientpaymentsRiminderData.rows[x]['subscription.subscriptionDateTime'];
            //                 cal_new_amount = await this.getTotalShowDataOthers(information, clientpaymentsRiminderData.rows[x]['subscriptionsId'], date_check);
            //                 new_data = Number(cal_new_amount[0].new_balance) - Number(clientpaymentsRiminderData.rows[x]["subscription.amountPaidSoFar"]);
            //             } else {
            //                 let date_check = (clientpaymentsRiminderData.rows[x]['subscription.clientJoiningDate'] != null) ? clientpaymentsRiminderData.rows[x]['subscription.clientJoiningDate'] : clientpaymentsRiminderData.rows[x]['subscription.subscriptionDateTime'];
            //                 cal_new_amount = await this.getTotalShowDataOthers(information, clientpaymentsRiminderData.rows[x]['subscriptionsId'], date_check);
            //                 new_data = Number(cal_new_amount[0].new_balance);
            //             }
            //             clientpaymentsRiminderData.rows[x]["getTotalShowData"] = [{
            //                 new_balance: new_data
            //             }];
            //             clientpaymentsRiminderData.rows[x]["getTotalDue"] = await this.getTotalDue(information, clientpaymentsRiminderData.rows[x]['subscriptionsId']);
            //             let getTotalPaid = await this.getTotalPaid(information, clientpaymentsRiminderData.rows[x]['subscriptionsId']);
            //             if (clientpaymentsRiminderData.rows[x]["subscription.status_amountPaidSoFar"] == '1') {
            //                 getTotalPaid[0].total_paid_amount = Number(getTotalPaid[0].total_paid_amount);
            //             } else {
            //                 getTotalPaid[0].total_paid_amount = Number(getTotalPaid[0].total_paid_amount) - Number(clientpaymentsRiminderData.rows[x]["subscription.amountPaidSoFar"]);
            //             }
            //             clientpaymentsRiminderData.rows[x]["getTotalPaid"] = getTotalPaid;
            //         } else {
            //             let new_data = '';
            //             let cal_new_amount = '';
            //             if (clientpaymentsRiminderData.rows[x]["subscription.status_amountPaidSoFar"] == 1) {
            //                 let date_check = (clientpaymentsRiminderData.rows[x]['subscription.clientJoiningDate'] != null) ? clientpaymentsRiminderData.rows[x]['subscription.clientJoiningDate'] : clientpaymentsRiminderData.rows[x]['subscription.subscriptionDateTime'];
            //                 cal_new_amount = await this.getTotalShowData(information, clientpaymentsRiminderData.rows[x]['subscriptionsId'], date_check);
            //                 new_data = Number(cal_new_amount[0].new_balance) - Number(clientpaymentsRiminderData.rows[x]["subscription.amountPaidSoFar"]);
            //             } else {
            //                 let date_check = (clientpaymentsRiminderData.rows[x]['subscription.clientJoiningDate'] != null) ? clientpaymentsRiminderData.rows[x]['subscription.clientJoiningDate'] : clientpaymentsRiminderData.rows[x]['subscription.subscriptionDateTime'];
            //                 cal_new_amount = await this.getTotalShowData(information, clientpaymentsRiminderData.rows[x]['subscriptionsId'], date_check);
            //                 new_data = Number(cal_new_amount[0].new_balance);
            //             }
            //             clientpaymentsRiminderData.rows[x]["getTotalShowData"] = [{
            //                 new_balance: new_data
            //             }];
            //             clientpaymentsRiminderData.rows[x]["getTotalDue"] = await this.getTotalDue(information, clientpaymentsRiminderData.rows[x]['subscriptionsId']);
            //             let getTotalPaid = await this.getTotalPaid(information, clientpaymentsRiminderData.rows[x]['subscriptionsId']);
            //             if (clientpaymentsRiminderData.rows[x]["subscription.status_amountPaidSoFar"] == '1') {
            //                 getTotalPaid[0].total_paid_amount = Number(getTotalPaid[0].total_paid_amount);
            //             } else {
            //                 getTotalPaid[0].total_paid_amount = Number(getTotalPaid[0].total_paid_amount) - Number(clientpaymentsRiminderData.rows[x]["subscription.amountPaidSoFar"]);
            //             }
            //             clientpaymentsRiminderData.rows[x]["getTotalPaid"] = getTotalPaid;

            //         }
            //         let listclientPayments = await dbclientpayments.clientpayments.findAll({
            //             where: {
            //                 businessId: businessIdinformation,
            //                 subscriptionsId: clientpaymentsRiminderData.rows[x]['subscriptionsId'],
            //                 status: 1,
            //                 "$payble_amount$": {
            //                     ne: null
            //                 }
            //             },
            //             raw: true
            //         });
            //         clientpaymentsRiminderData.rows[x]["subcription_payment_status"] = listclientPayments.length;
            //     }
            // }
            if (clientpaymentsRiminderData.rows.length > 0) {
                for (let x = 0; x < clientpaymentsRiminderData.rows.length; x++) {
                    clientpaymentsRiminderData.rows[x]["sessionInfo"] = await sessionInfo(information, {
                        status: 1,
                        subscriptionId: clientpaymentsRiminderData.rows[x]['subscriptionsId']
                    });
                    if (clientpaymentsRiminderData.rows[x]["subscription.installment_frequency"] == '6') {
                        let new_data = '';
                        let cal_new_amount = '';
                        if (clientpaymentsRiminderData.rows[x]["subscription.status_amountPaidSoFar"] == 1) {
                            let date_check = (clientpaymentsRiminderData.rows[x]['subscription.clientJoiningDate'] != null) ? clientpaymentsRiminderData.rows[x]['subscription.clientJoiningDate'] : clientpaymentsRiminderData.rows[x]['subscription.subscriptionDateTime'];
                            cal_new_amount = await this.getTotalShowDataOthers(information, clientpaymentsRiminderData.rows[x]['subscriptionsId'], date_check);
                            new_data = Number(cal_new_amount[0].new_balance) - Number(clientpaymentsRiminderData.rows[x]["subscription.amountPaidSoFar"]);
                        } else {
                            let date_check = await hyphenDateFormat(new Date());
                            cal_new_amount = await this.getTotalShowDataOthers(information, clientpaymentsRiminderData.rows[x]['subscriptionsId'], date_check);
                            new_data = Number(cal_new_amount[0].new_balance);
                        }
                        clientpaymentsRiminderData.rows[x]["getTotalShowData"] = [{
                            new_balance: new_data
                        }];
                        clientpaymentsRiminderData.rows[x]["getTotalDue"] = await this.getTotalDue(information, clientpaymentsRiminderData.rows[x]['subscriptionsId']);
                        let getTotalPaid = await this.getTotalPaid(information, clientpaymentsRiminderData.rows[x]['subscriptionsId']);
                        if (clientpaymentsRiminderData.rows[x]["subscription.status_amountPaidSoFar"] == '1') {
                            getTotalPaid[0].total_paid_amount = Number(getTotalPaid[0].total_paid_amount);
                        } else {
                            getTotalPaid[0].total_paid_amount = Number(getTotalPaid[0].total_paid_amount) - Number(clientpaymentsRiminderData.rows[x]["subscription.amountPaidSoFar"]);
                        }
                        clientpaymentsRiminderData.rows[x]["getTotalPaid"] = getTotalPaid;
                    } else {
                        let new_data = '';
                        let cal_new_amount = '';
                        if (clientpaymentsRiminderData.rows[x]["subscription.status_amountPaidSoFar"] == 1) {
                            let date_check = (clientpaymentsRiminderData.rows[x]['subscription.clientJoiningDate'] != null) ? clientpaymentsRiminderData.rows[x]['subscription.clientJoiningDate'] : clientpaymentsRiminderData.rows[x]['subscription.subscriptionDateTime'];
                            cal_new_amount = await this.getTotalShowData(information, clientpaymentsRiminderData.rows[x]['subscriptionsId'], date_check);
                            new_data = Number(cal_new_amount[0].new_balance) - Number(clientpaymentsRiminderData.rows[x]["subscription.amountPaidSoFar"]);
                        } else {
                            // let date_check = (clientpaymentsRiminderData.rows[x]['subscription.clientJoiningDate'] != null) ? clientpaymentsRiminderData.rows[x]['subscription.clientJoiningDate'] : clientpaymentsRiminderData.rows[x]['subscription.subscriptionDateTime'];
                            let date_check = clientpaymentsRiminderData.rows[x]['payment_end_date'];
                            cal_new_amount = await this.getTotalShowData(information, clientpaymentsRiminderData.rows[x]['subscriptionsId'], date_check);
                            new_data = Number(cal_new_amount[0].new_balance);
                        }
                        clientpaymentsRiminderData.rows[x]["getTotalShowData"] = [{
                            new_balance: new_data
                        }];
                        clientpaymentsRiminderData.rows[x]["getTotalDue"] = await this.getTotalDue(information, clientpaymentsRiminderData.rows[x]['subscriptionsId']);
                        let getTotalPaid = await this.getTotalPaid(information, clientpaymentsRiminderData.rows[x]['subscriptionsId']);
                        if (clientpaymentsRiminderData.rows[x]["subscription.status_amountPaidSoFar"] == '1') {
                            getTotalPaid[0].total_paid_amount = Number(getTotalPaid[0].total_paid_amount);
                        } else {
                            getTotalPaid[0].total_paid_amount = Number(getTotalPaid[0].total_paid_amount) - Number(clientpaymentsRiminderData.rows[x]["subscription.amountPaidSoFar"]);
                        }
                        clientpaymentsRiminderData.rows[x]["getTotalPaid"] = getTotalPaid;

                    }
                    let listclientPayments = await dbclientpayments.clientpayments.findAll({
                        where: {
                            businessId: businessIdinformation,
                            subscriptionsId: clientpaymentsRiminderData.rows[x]['subscriptionsId'],
                            status: 1,
                            "$payble_amount$": {
                                ne: null
                            }
                        },
                        raw: true
                    });
                    clientpaymentsRiminderData.rows[x]["subcription_payment_status"] = listclientPayments.length;
                }
            }
            clientpaymentsRiminder.count = clientpaymentsRiminderDataCount.count;
            clientpaymentsRiminder.rows = clientpaymentsRiminderData.rows;

            //  ======================================clientpaymentsRiminderData End ========================================================

            // let recentIncomeclientpaymentsCount = await dbclientpayments.clientpayments.findAll({
            //     attributes: [
            //         "subscriptionsId",
            //         "due_amount", [
            //             dbclientpayments.sequelize.fn(
            //                 "sum",
            //                 dbclientpayments.sequelize.col("payble_amount")
            //             ),
            //             "totalPayment"
            //         ]
            //     ],
            //     group: ["subscriptionsId", "due_amount"],
            //     include: [{
            //         model: dbsubscriptions.subscriptions,
            //         attributes: ["clientId"],
            //         include: [{
            //             model: dbClients.clients,
            //             attributes: {
            //                 include: [
            //                     [
            //                         dbClients.clients.sequelize.literal(
            //                             'CASE WHEN photoUrl  is NULL THEN CONCAT("' +
            //                             KNACK_UPLOAD_URL.localUrl +
            //                             '" , "noimage.jpg" ) ELSE CONCAT("' +
            //                             KNACK_UPLOAD_URL.localUrl +
            //                             '" , photoUrl ) END'
            //                         ),
            //                         "photoUrl"
            //                     ]
            //                 ],
            //                 exclude: []
            //             },
            //             where: itemGroupConditions
            //         }],
            //         where: itemConditions
            //     }],
            //     where: {
            //         status: 1,
            //         paymentStatus: 2,
            //         businessId: businessIdinformation
            //     },
            //     raw: true
            // });
            // let recentIncomeclientpaymentsData = await dbclientpayments.clientpayments.findAll({
            //     attributes: [
            //         "clientpaymentId",
            //         "subscriptionsId",
            //         "due_amount",
            //         "payble_amount"
            //     ],
            //     include: [{
            //             model: dbsubscriptions.subscriptions,
            //             attributes: ["clientId"],
            //             include: [{
            //                 model: dbClients.clients,
            //                 attributes: {
            //                     include: [
            //                         [
            //                             dbClients.clients.sequelize.literal(
            //                                 'CASE WHEN photoUrl  is NULL THEN CONCAT("' +
            //                                 KNACK_UPLOAD_URL.localUrl +
            //                                 '" , "noimage.jpg" ) ELSE CONCAT("' +
            //                                 KNACK_UPLOAD_URL.localUrl +
            //                                 '" , photoUrl ) END'
            //                             ),
            //                             "photoUrl"
            //                         ]
            //                     ],
            //                     exclude: []
            //                 },
            //                 where: itemGroupConditions
            //             }],
            //             where: itemConditions
            //         },
            //         {
            //             model: dbpaymentTransactions.paymentTransactions,
            //             attributes: []
            //         }
            //     ],
            //     where: {
            //         status: 1,
            //         paymentStatus: 2,
            //         businessId: businessIdinformation
            //     },
            //     order: [
            //         ["payment_end_date", "DESC"]
            //     ],
            //     limit: 3,
            //     offset: 0,
            //     raw: true
            // });



            let recentIncomeclientpaymentsCount = await dbpaymentTransactions.paymentTransactions.findAll({
                attributes: {
                    include: [],
                    exclude: []
                },
                include: [{
                    model: dbsubscriptions.subscriptions,
                    attributes: {
                        include: [],
                        exclude: ["createDateTime", "updateDateTime"]
                    },
                    include: [{
                            model: dbClients.clients,
                            attributes: {
                                include: [
                                    [
                                        dbClients.clients.sequelize.literal(
                                            'CASE WHEN photoUrl  is NULL THEN CONCAT("' +
                                            KNACK_UPLOAD_URL.localUrl +
                                            '" , "noimage.jpg" ) ELSE CONCAT("' +
                                            KNACK_UPLOAD_URL.localUrl +
                                            '" , photoUrl ) END'
                                        ),
                                        "photoUrl"
                                    ],
                                ],
                                exclude: ["createDateTime", "updateDateTime"]
                            },
                            where: {
                                status: 1,
                                businessId: businessIdinformation,
                            }
                        },
                        {
                            model: dbpricepack.pricepacks,
                            attributes: ["pricepackName"],
                            where: {},
                        }
                    ],
                    where: {},
                }],
                order: [
                    ["CreateDateTime", "DESC"]
                ],
                where: {
                    status: 1,
                },
                raw: true
            });
            let recentIncomeclientpaymentsData = await dbpaymentTransactions.paymentTransactions.findAll({
                attributes: {
                    include: [],
                    exclude: []
                },
                include: [{
                    model: dbsubscriptions.subscriptions,
                    attributes: {
                        include: [],
                        exclude: ["createDateTime", "updateDateTime"]
                    },
                    include: [{
                            model: dbClients.clients,
                            attributes: {
                                include: [
                                    [
                                        dbClients.clients.sequelize.literal(
                                            'CASE WHEN photoUrl  is NULL THEN CONCAT("' +
                                            KNACK_UPLOAD_URL.localUrl +
                                            '" , "noimage.jpg" ) ELSE CONCAT("' +
                                            KNACK_UPLOAD_URL.localUrl +
                                            '" , photoUrl ) END'
                                        ),
                                        "photoUrl"
                                    ],
                                ],
                                exclude: ["createDateTime", "updateDateTime"]
                            },
                            where: {
                                status: 1,
                                businessId: businessIdinformation,
                            }
                        },
                        {
                            model: dbpricepack.pricepacks,
                            attributes: ["pricepackName"],
                            where: {},
                        }
                    ],
                    where: {},
                }],
                order: [
                    ["CreateDateTime", "DESC"]
                ],
                where: {
                    status: 1,
                },
                offset: 0,
                limit: 3,
                raw: true
            });

            if (recentIncomeclientpaymentsData.length > 0) {
                for (let x = 0; x < recentIncomeclientpaymentsData.length; x++) {
                    recentIncomeclientpaymentsData[x]["sessionInfo"] = await sessionInfo(information, {
                        status: 1,
                        subscriptionId: recentIncomeclientpaymentsData[x]['subscriptionsId']
                    });
                }
            }
            recentIncomeclientpayments.count = recentIncomeclientpaymentsCount.length;
            recentIncomeclientpayments.raw = recentIncomeclientpaymentsData;
            let recentExpenseclientExpenseCount = await dbclientexpense.clientexpense.findAll({
                where: {
                    status: 1,
                    businessId: businessIdinformation
                },
                raw: true
            });
            let recentExpenseclientExpenseData = await dbclientexpense.clientexpense.findAll({
                attributes: {
                    include: [
                        [
                            dbclientexpense.sequelize.fn(
                                "SUBSTRING",
                                dbclientexpense.sequelize.fn(
                                    "SOUNDEX",
                                    dbclientexpense.sequelize.col("name")
                                ),
                                1,
                                1
                            ),
                            "name_initials"
                        ]
                    ],
                    exclude: []
                },
                include: [{
                    model: dbcenters.centers,
                    attributes: ["centerName"],
                    where: {},
                }],
                where: {
                    status: 1,
                    businessId: businessIdinformation
                },
                limit: 3,
                offset: 0,
                order: [
                    ["UpdateDateTime", "DESC"]
                ],
                raw: true
            });
            recentExpenseclientExpense.count = recentExpenseclientExpenseCount.length;
            recentExpenseclientExpense.raw = recentExpenseclientExpenseData;
            var months = [
                "january",
                "february",
                "march",
                "april",
                "may",
                "june",
                "july",
                "august",
                "september",
                "october",
                "november",
                "december"
            ];
            var monthlyIncomes = {};
            var monthlyExpense = {};

            let newmonthlyIncomes = [];
            let newmonthlyExpenses = [];

            for (var i = 1; i <= 12; i++) {
                let payment = await dbclientpayments.clientpayments.findAll({
                    where: {
                        status: 1,
                        businessId: businessIdinformation,
                        payment_date: dbclientpayments.sequelize.where(
                            dbclientpayments.sequelize.fn(
                                "YEAR",
                                dbclientpayments.sequelize.col("payment_date")
                            ),
                            new Date().getFullYear()
                        ),
                        and: {
                            payment_date: dbclientpayments.sequelize.where(
                                dbclientpayments.sequelize.fn(
                                    "MONTH",
                                    dbclientpayments.sequelize.col("payment_date")
                                ),
                                i
                            )
                        }
                    },
                    include: [{
                        model: dbpaymentTransactions.paymentTransactions,
                        attributes: []
                    }],
                    raw: true
                });
                var payAmount = 0;
                for (var j = 0; j < payment.length; j++) {
                    payAmount += payment[j].payble_amount;
                }
                monthlyIncomes[months[i - 1]] = payAmount;
            }
            let yearIncomesTotal = 0;
            let yearIncomesTotalCal = 0;
            if (monthlyIncomes) {
                for (var k in monthlyIncomes) {
                    if (monthlyIncomes.hasOwnProperty(k)) {
                        yearIncomesTotal += monthlyIncomes[k];
                    }
                }
                yearIncomesTotalCal = yearIncomesTotal;
                yearIncomesTotal = numberWithCommas(yearIncomesTotal);
            }
            if (monthlyIncomes) {
                for (var key in monthlyIncomes) {
                    if (monthlyIncomes.hasOwnProperty(key)) {
                        let modifyData = await getAmountPercentage(information, monthlyIncomes[key], yearIncomesTotalCal);
                        let innerObj = {};
                        innerObj.monthname = key;
                        innerObj.percentage = Number(modifyData);
                        newmonthlyIncomes.push(innerObj);
                    }
                }
            }
            let totalYearIncome = {
                year: new Date().getFullYear(),
                months: monthlyIncomes,
                yearIncomesTotal,
                monthlyIncomesPercentage: newmonthlyIncomes
            };
            for (var i = 1; i <= 12; i++) {
                let expense = await dbclientexpense.clientexpense.findAll({
                    where: {
                        status: 1,
                        businessId: businessIdinformation,
                        dateOfPayment: dbclientexpense.sequelize.where(
                            dbclientexpense.sequelize.fn(
                                "YEAR",
                                dbclientexpense.sequelize.col("dateOfPayment")
                            ),
                            new Date().getFullYear()
                        ),
                        and: {
                            dateOfPayment: dbclientexpense.sequelize.where(
                                dbclientexpense.sequelize.fn(
                                    "MONTH",
                                    dbclientexpense.sequelize.col("dateOfPayment")
                                ),
                                i
                            )
                        }
                    },
                    raw: true
                });
                var expAmount = 0;
                for (var j = 0; j < expense.length; j++) {
                    expAmount += expense[j].amount;
                }
                monthlyExpense[months[i - 1]] = expAmount;
            }
            let yearExpenseTotal = 0;
            let yearExpenseTotalCal = 0;
            if (monthlyExpense) {
                for (var k in monthlyExpense) {
                    if (monthlyExpense.hasOwnProperty(k)) {
                        yearExpenseTotal += monthlyExpense[k];
                    }
                }
                yearExpenseTotalCal = yearExpenseTotal;
                yearExpenseTotal = numberWithCommas(yearExpenseTotal);
            }

            if (monthlyExpense) {
                for (var key in monthlyExpense) {
                    if (monthlyExpense.hasOwnProperty(key)) {
                        let modifyData = await getAmountPercentage(information, monthlyExpense[key], yearExpenseTotalCal);
                        let innerObj = {};
                        innerObj.monthname = key;
                        innerObj.percentage = Number(modifyData);
                        newmonthlyExpenses.push(innerObj);
                    }
                }
            }

            let totalYearExpense = {
                year: new Date().getFullYear(),
                months: monthlyExpense,
                yearExpenseTotal,
                monthlyExpensePercentage: newmonthlyExpenses
            };
            dbclientpayments.sequelize.close();
            dbsubscriptions.sequelize.close();
            dbClients.sequelize.close();
            dbpricepack.sequelize.close();
            dbclientexpense.sequelize.close();
            dbcenters.sequelize.close();
            var mergedSub = {},
                mergedSub = Object.assign.apply(Object, [{
                        clientpaymentsIncome
                    },
                    {
                        clientpaymentsExpense
                    },
                    {
                        clientpaymentsRiminder
                    },
                    {
                        recentIncomeclientpayments
                    },
                    {
                        recentExpenseclientExpense
                    },
                    {
                        totalYearIncome
                    },
                    {
                        totalYearExpense
                    }
                ]);
            mergedAllActiveClients = mergedAllActiveClients.concat(mergedSub);
            return mergedAllActiveClients;
        } catch (error) {
            throw error;
        }
    };



    getTotalShowData = async(
        information,
        subscriptionsId,
        date
    ) => {
        try {
            let businessIdinformation = information.split(",")[1];
            let dbsubscriptions = createInstance(["clientSubscription"], information);
            let todayDate = await hyphenDateFormat(date);
            // let todayDate = '2018-07-04';
            let data = await dbsubscriptions.sequelize.query(
                "SELECT (SUM(`due_amount`) - CASE WHEN SUM(`payble_amount`) is NULL THEN 0 ELSE SUM(`payble_amount`) END) AS `new_balance` FROM `clientpayments` WHERE `payment_due_date` <= '" + todayDate + "' AND `status` = '1' AND `subscriptionsId` = '" + subscriptionsId + "'", {
                    type: dbsubscriptions.sequelize.QueryTypes.SELECT
                }
            );
            dbsubscriptions.sequelize.close();
            return data;
        } catch (error) {
            throw error;
        }
    };



    getTotalShowDataOthers = async(
        information,
        subscriptionsId,
        date
    ) => {
        try {
            let final = {};
            let finalReturn = [];
            let finalReturnCal = 0;
            var due_amount_local = 0;
            var payble_amount_local = 0;
            let businessIdinformation = information.split(",")[1];
            let dbsubscriptions = createInstance(["clientSubscription"], information);
            let todayDate = await hyphenDateFormat(date);
            let data = await dbsubscriptions.sequelize.query(
                "( SELECT `payment_due_date`, `payment_end_date`, `due_amount`, `payble_amount` FROM `clientpayments` WHERE (`payment_due_date` >= '" + todayDate + "' AND `curent_subcription_number` = 1 ) AND `status` = '1' AND `subscriptionsId` = '" + subscriptionsId + "' ) union ( SELECT `payment_due_date`, `payment_end_date`, `due_amount`, `payble_amount` FROM `clientpayments` WHERE ( `payment_due_date` <= '" + todayDate + "' OR `payment_end_date` <= '" + todayDate + "' ) AND `status` = '1' AND `subscriptionsId` = '" + subscriptionsId + "' )", {
                    type: dbsubscriptions.sequelize.QueryTypes.SELECT
                }
            );
            dbsubscriptions.sequelize.close();
            if (data) {
                for (let x = 0; x < data.length; x++) {
                    due_amount_local += Number(data[x].due_amount);
                    payble_amount_local += Number(data[x].payble_amount);
                }
                finalReturnCal = due_amount_local - payble_amount_local;
            }
            var a = [{
                new_balance: finalReturnCal
            }];
            return a;
        } catch (error) {
            throw error;
        }
    };


    getTotalDue = async(
        information,
        subscriptionsId
    ) => {
        try {

            let businessIdinformation = information.split(",")[1];
            let dbsubscriptions = createInstance(["clientSubscription"], information);
            let todayDate = await hyphenDateFormat(new Date);
            let data = await dbsubscriptions.sequelize.query(
                "SELECT SUM(`due_amount`) as `total_current_due` FROM `clientpayments` WHERE `subscriptionsId` = '" + subscriptionsId + "' AND `status` = '1' ", {
                    type: dbsubscriptions.sequelize.QueryTypes.SELECT
                }
            );
            dbsubscriptions.sequelize.close();
            return data;
        } catch (error) {
            throw error;
        }
    };


    getTotalPaid = async(
        information,
        subscriptionsId
    ) => {
        try {
            let businessIdinformation = information.split(",")[1];
            let dbsubscriptions = createInstance(["clientSubscription"], information);
            let data = await dbsubscriptions.sequelize.query(
                "SELECT SUM(`payble_amount`) as `total_paid_amount` FROM `clientpayments` WHERE `subscriptionsId` = '" + subscriptionsId + "' AND `status` = '1' ", {
                    type: dbsubscriptions.sequelize.QueryTypes.SELECT
                }
            );
            dbsubscriptions.sequelize.close();
            return data;
        } catch (error) {
            throw error;
        }
    };

    // clientPaymentdetails = async(
    //     information,
    //     clientpaymentId,
    //     params,
    //     filterPrivateFields = true
    // ) => {
    //     try {
    //         let dbclientPayments = createInstance(["clientPayments"], information);
    //         let dbsubscriptions = createInstance(["clientSubscription"], information);
    //         let dbclient = createInstance(["client"], information);
    //         let dbcenters = createInstance(["clientCenter"], information);
    //         let dbpricepacks = createInstance(["clientPricepack"], information);
    //         let dbservices = createInstance(["clientService"], information);
    //         let dbservicepricepackmap = createInstance(
    //             ["servicePricepackMap"],
    //             information
    //         );
    //         dbclientPayments.clientpayments.belongsTo(this.businessModel, {
    //             foreignKey: "businessId",
    //             targetKey: "businessId"
    //         });
    //         dbclientPayments.clientpayments.belongsTo(dbsubscriptions.subscriptions, {
    //             foreignKey: "subscriptionsId",
    //             targetKey: "subscriptionId"
    //         });
    //         dbsubscriptions.subscriptions.belongsTo(dbclient.clients, {
    //             foreignKey: "clientId",
    //             targetKey: "clientId"
    //         });
    //         dbclient.clients.belongsTo(dbcenters.centers, {
    //             foreignKey: "centerId",
    //             targetKey: "centerId"
    //         });
    //         dbsubscriptions.subscriptions.belongsTo(dbpricepacks.pricepacks, {
    //             foreignKey: "pricepacks",
    //             targetKey: "pricepackId"
    //         });
    //         dbsubscriptions.subscriptions.belongsTo(
    //             dbservicepricepackmap.servicepricepackmap, {
    //                 foreignKey: "pricepacks",
    //                 targetKey: "pricepackId"
    //             }
    //         );
    //         dbservicepricepackmap.servicepricepackmap.belongsTo(
    //             dbpricepacks.pricepacks, {
    //                 foreignKey: "pricepackId",
    //                 targetKey: "pricepackId"
    //             }
    //         );
    //         dbservicepricepackmap.servicepricepackmap.belongsTo(dbservices.services, {
    //             foreignKey: "serviceId",
    //             targetKey: "serviceId"
    //         });
    //         let clientpaymentsDetails = await dbclientPayments.clientpayments.findAll({
    //             attributes: [
    //                 "clientpaymentId",
    //                 "payment_type",
    //                 "invoice_number",
    //                 "payble_amount",
    //                 "subscriptionsId", [
    //                     dbclientPayments.sequelize.literal(
    //                         "(SELECT IFNULL(SUM(`payble_amount`),0) FROM `clientpayments` INNER join `paymentTransactions` AS `paymentTransactions` ON `clientpayments`.`clientpaymentId` = `paymentTransactions`.`clientpaymentId` WHERE `subscriptionsId` = " +
    //                         dbsubscriptions.sequelize.col("subscriptionId").col +
    //                         " AND `paymentStatus` IN (2,3))"
    //                     ),
    //                     "totalPayment"
    //                 ],
    //                 [
    //                     dbclientPayments.sequelize.literal(
    //                         "(SELECT IFNULL(SUM(`due_amount`),0) FROM `clientpayments` INNER join `paymentTransactions` AS `paymentTransactions` ON `clientpayments`.`clientpaymentId` = `paymentTransactions`.`clientpaymentId` WHERE `subscriptionsId` = " +
    //                         dbsubscriptions.sequelize.col("subscriptionId").col +
    //                         " AND `paymentStatus` = 1)"
    //                     ),
    //                     "totalDueAmount"
    //                 ],
    //                 [
    //                     dbclientPayments.sequelize.literal(
    //                         "(SELECT `payment_date` FROM `paymentTransactions` LEFT join `clientpayments` AS `clientpayments` ON `paymentTransactions`.`clientpaymentId` = `clientpayments`.`clientpaymentId` WHERE `paymentTransactions`.`clientpaymentId` = '" + clientpaymentId + "' ORDER BY `payment_date` DESC LIMIT 1)"
    //                     ),
    //                     "payment_date"
    //                 ]
    //             ],
    //             group: ["subscriptionsId"],
    //             where: {
    //                 status: 1,
    //                 clientpaymentId: clientpaymentId
    //             },
    //             include: [{
    //                     model: dbsubscriptions.subscriptions,
    //                     attributes: ["subscriptionId"],
    //                     include: [{
    //                             model: dbclient.clients,
    //                             attributes: [
    //                                 "clientName",
    //                                 "contactNumber",
    //                                 "emailId",
    //                                 "area",
    //                                 "pin",
    //                                 "city", [
    //                                     dbclient.clients.sequelize.literal(
    //                                         'CASE WHEN `subscription->client`.photoUrl  is NULL THEN CONCAT("' +
    //                                         KNACK_UPLOAD_URL.localUrl +
    //                                         '" , "noimage.jpg" ) ELSE CONCAT("' +
    //                                         KNACK_UPLOAD_URL.localUrl +
    //                                         '" , `subscription->client`.photoUrl ) END'
    //                                     ),
    //                                     "client_photoUrl"
    //                                 ]
    //                             ],
    //                             include: [{
    //                                 model: dbcenters.centers,
    //                                 attributes: ["centerName"]
    //                             }]
    //                         },
    //                         {
    //                             model: dbpricepacks.pricepacks,
    //                             attributes: ["pricepackName"]
    //                         }
    //                     ]
    //                 },
    //                 {
    //                     model: this.businessModel,
    //                     attributes: [
    //                         "businessName", [
    //                             this.businessModel.sequelize.fn(
    //                                 "SUBSTRING",
    //                                 this.businessModel.sequelize.fn(
    //                                     "SOUNDEX",
    //                                     this.businessModel.sequelize.col("businessName")
    //                                 ),
    //                                 1,
    //                                 1
    //                             ),
    //                             "businessName_initials"
    //                         ]
    //                     ]
    //                 }
    //             ],
    //             raw: true
    //         });
    //         if (clientpaymentsDetails) {
    //             let serviceAll = await dbservicepricepackmap.servicepricepackmap.findAll({
    //                 attributes: {
    //                     include: [],
    //                     exclude: [
    //                         `servicepricepackmapId`,
    //                         `pricepackId`,
    //                         `serviceId`,
    //                         `businessId`,
    //                         `serviceDuration`,
    //                         `createDateTime`,
    //                         `updateDateTime`,
    //                         `status`
    //                     ]
    //                 },
    //                 where: {
    //                     status: 1,
    //                     pricepackId: clientpaymentsDetails[0]["subscription.pricepack.pricepackId"]
    //                 },
    //                 include: [{
    //                     model: dbservices.services,
    //                     attributes: [
    //                         [
    //                             dbservicepricepackmap.sequelize.fn(
    //                                 "GROUP_CONCAT",
    //                                 dbservicepricepackmap.sequelize.col("serviceName")
    //                             ),
    //                             "serviceNameList"
    //                         ]
    //                     ]
    //                 }],
    //                 raw: true
    //             });
    //             clientpaymentsDetails[0]["serviceNameList"] = serviceAll;
    //         }

    //         var arr = [];
    //         var todayDate = new Date().toISOString().slice(0, 10);
    //         let next_payment_due_date = null;
    //         let pendingPaymentDate;
    //         let getsubscriptionsId = await dbclientPayments.clientpayments.findAll({
    //             attributes: ["subscriptionsId"],
    //             group: ["subscriptionsId"],
    //             where: {
    //                 status: 1,
    //                 clientpaymentId: clientpaymentId
    //             },
    //             raw: true
    //         });
    //         if (getsubscriptionsId.length > 0) {
    //             pendingPaymentDate = await dbclientPayments.clientpayments.findAll({
    //                 attributes: ["payment_due_date"],
    //                 order: [
    //                     ["payment_due_date", "ASC"]
    //                 ],
    //                 where: {
    //                     paymentStatus: 1,
    //                     subscriptionsId: getsubscriptionsId[0].subscriptionsId
    //                 },
    //                 raw: true
    //             });
    //         }
    //         if (pendingPaymentDate) {
    //             for (var prop in pendingPaymentDate) {
    //                 if (pendingPaymentDate.hasOwnProperty(prop)) {
    //                     let innerObj = {};
    //                     innerObj = pendingPaymentDate[prop]["payment_due_date"];
    //                     arr.push(innerObj);
    //                 }
    //             }
    //         }

    //         function getDateNextAndPrevious(array, val, dir) {
    //             for (var i = 0; i < array.length; i++) {
    //                 if (dir == true) {
    //                     if (new Date(array[i]) > new Date(val)) {
    //                         return array[i - 1] || 0;
    //                     }
    //                 } else {
    //                     if (new Date(array[i]) >= new Date(val)) {
    //                         return array[i];
    //                     }
    //                 }
    //             }
    //         }
    //         if (clientpaymentsDetails) {
    //             clientpaymentsDetails[0]["next_payment_due_date"] = getDateNextAndPrevious(arr, todayDate) ? getDateNextAndPrevious(arr, todayDate) : next_payment_due_date;
    //             clientpaymentsDetails[0]["sessionInfo"] = await sessionInfo(information, {
    //                 status: 1,
    //                 subscriptionId: clientpaymentsDetails[0]["subscription.subscriptionId"]
    //             });
    //         }
    //         dbclientPayments.sequelize.close();
    //         dbsubscriptions.sequelize.close();
    //         dbclient.sequelize.close();
    //         dbcenters.sequelize.close();
    //         dbpricepacks.sequelize.close();
    //         dbservices.sequelize.close();
    //         dbservicepricepackmap.sequelize.close();

    //         return 1;

    //         if (params[0]) {
    //             var content = makeEmailTemplate(
    //                 "src/emailTemplate/ClientreportTemplate.js",
    //                 clientpaymentsDetails
    //             );
    //             const filenameRender = clientpaymentsDetails[0]["clientpaymentId"];
    //             fs.writeFile(
    //                 "uploads/paymentReceived/html/" + filenameRender + ".html",
    //                 content,
    //                 function(err) {
    //                     if (err) {
    //                         return console.log(err);
    //                     } else {
    //                         var html = fs.readFileSync(
    //                             "uploads/paymentReceived/html/" + filenameRender + ".html",
    //                             "utf8"
    //                         );
    //                         pdf1
    //                             .create(html, {
    //                                 format: "Letter"
    //                             })
    //                             .toFile(
    //                                 "uploads/paymentReceived/content/" + filenameRender + ".pdf",
    //                                 function(err, res) {
    //                                     if (err) return console.log(err);
    //                                 }
    //                             );
    //                     }
    //                 }
    //             );
    //             sendEmail(
    //                 clientpaymentsDetails[0]["subscription.client.emailId"],
    //                 "PAYMENT INVOICE ",
    //                 "INVOICE", {
    //                     filename: filenameRender + ".pdf",
    //                     path: "uploads/paymentReceived/content/" + filenameRender + ".pdf",
    //                     contentType: "application/pdf"
    //                 }
    //             );
    //         }
    //         return await clientpaymentsDetails;
    //     } catch (error) {
    //         throw error;
    //     }
    // };



    clientPaymentdetails = async(
        information,
        clientpaymentId,
        params,
        filterPrivateFields = true
    ) => {
        try {
            let clientpaymentsDetails = [];
            let dbpaymentTransactions = createInstance(["paymentTransactions"], information);
            let transactions = await dbpaymentTransactions.paymentTransactions.findAll({
                attributes: {
                    include: [],
                    exclude: []
                },
                where: {
                    status: 1,
                    paymentTransactionId: clientpaymentId
                },
                raw: true
            });
            dbpaymentTransactions.sequelize.close();



            let clientTotalPaymentdb = createInstance(["clientPayments"], information);
            let clientTotalPaymentLastDate = await clientTotalPaymentdb.clientpayments.findAll({
                attributes: ['payment_end_date'],
                where: {
                    status: 1,
                    subscriptionsId: transactions[0]['subscriptionsId'],
                },
                order: [
                    ['curent_subcription_number', 'DESC']
                ],
                limit: 1,
                raw: true
            });
            clientTotalPaymentdb.sequelize.close();
            transactions[0]['clientTotalPaymentLastDate'] = clientTotalPaymentLastDate[0].payment_end_date;
            if (transactions) {
                clientpaymentsDetails = transactions[0]['payment_content'];
                clientpaymentsDetails[0]['clientTotalPaymentLastDate'] = clientTotalPaymentLastDate[0].payment_end_date;
            }
            let temp_client = {
                clientId: (clientpaymentsDetails[0].clientId) ? clientpaymentsDetails[0].clientId : ''
            }
            let temp_client_Data = await clientInfo(information, temp_client);
            if (temp_client_Data) {
                if (temp_client_Data[0].emailId) {
                    if (params[0]) {
                        var content = makeEmailTemplate(
                            "src/emailTemplate/ClientreportTemplate.js",
                            clientpaymentsDetails
                        );
                        const filenameRender = clientpaymentsDetails[0]["parentSubscriptionId"] + '__' + Date.now();
                        fs.writeFile(
                            "uploads/paymentReceived/html/" + filenameRender + ".html",
                            content,
                            function(err) {
                                if (err) {
                                    return console.log(err);
                                } else {
                                    var html = fs.readFileSync(
                                        "uploads/paymentReceived/html/" + filenameRender + ".html",
                                        "utf8"
                                    );
                                    pdf1
                                        .create(html, {
                                            format: "Letter"
                                        })
                                        .toFile(
                                            "uploads/paymentReceived/content/" + filenameRender + ".pdf",
                                            function(err, res) {
                                                if (err) return console.log(err);
                                                sendEmail(
                                                    temp_client_Data[0].emailId,
                                                    "PAYMENT INVOICE ",
                                                    "INVOICE", {
                                                        filename: filenameRender + ".pdf",
                                                        path: "uploads/paymentReceived/content/" + filenameRender + ".pdf",
                                                        contentType: "application/pdf"
                                                    }
                                                );
                                            }
                                        );
                                }
                            }
                        );
                    }
                }
            }
            return transactions;
        } catch (error) {
            throw error;
        }
    };



    paymentReminderAction = async(
        information,
        clientpaymentId,
        reminderType
    ) => {
        try {
            let clientData = [];
            let dbLog = createInstance(["log"], information);
            let clientTotalPaymentdb = createInstance(["clientPayments"], information);
            let businessIdinformation = information.split(",")[1];
            let clientpaymentIdList = clientpaymentId.split(',');
            let todayDateCheck = await hyphenDateFormat(new Date());
            let todaytoLocaleTimeString = new Date().toLocaleTimeString("en-US");
            let message = '';
            let durationTypeName = '';
            if (clientpaymentIdList.length > 0) {
                for (let i = 0; i < clientpaymentIdList.length; i++) {
                    let clientTotalPaymentData = await clientTotalPaymentdb.clientpayments.findAll({
                        where: {
                            clientpaymentId: clientpaymentIdList[i],
                            businessId: businessIdinformation,
                        },
                        raw: true
                    });
                    if (clientTotalPaymentData.length > 0) {
                        for (let m = 0; m < clientTotalPaymentData.length; m++) {
                            if (clientTotalPaymentData[m].subscriptionsId) {
                                let subscriptionDetails = await SubscriptionDB.details(information, clientTotalPaymentData[m].subscriptionsId);
                                let clientId = subscriptionDetails[0].clientId;
                                if (subscriptionDetails) {
                                    if (subscriptionDetails[0].getSubcriptionPaymentFormat.length > 0) {
                                        let someArray = [];
                                        for (let k = 0; k < subscriptionDetails[0].getSubcriptionPaymentFormat.length; k++) {
                                            if (subscriptionDetails[0].getSubcriptionPaymentFormat[k].clientpaymentId == clientTotalPaymentData[m].clientpaymentId) {
                                                //delete subscriptionDetails[0].getSubcriptionPaymentFormat[k];
                                                someArray.push(subscriptionDetails[0].getSubcriptionPaymentFormat[k]);
                                            }
                                        }
                                        subscriptionDetails[0].getSubcriptionPaymentFormat = someArray;
                                    }
                                }
                                var getobj = {
                                    businessId: businessIdinformation,
                                    clientId: clientId
                                };
                                clientData = await clientInfo(information, getobj);
                                let totalSessions = (subscriptionDetails[0].sessionInfo[0].totalSession) ? subscriptionDetails[0].sessionInfo[0].totalSession : 0;
                                let attendedSessions = (subscriptionDetails[0].sessionInfo[0].attendedSession) ? subscriptionDetails[0].sessionInfo[0].attendedSession : 0;
                                let remainingSessions = (subscriptionDetails[0].sessionInfo[0].totalSession - subscriptionDetails[0].sessionInfo[0].attendedSession);
                                let serviceList = (subscriptionDetails[0].serviceNameList[0]['service.serviceNameList']) ? subscriptionDetails[0].serviceNameList[0]['service.serviceNameList'] : '';
                                let paymentDueAmt = 0;
                                let paymentDueDate = '';
                                if (subscriptionDetails[0].getSubcriptionPaymentFormat[0]) {
                                    if (subscriptionDetails[0].getSubcriptionPaymentFormat[0].due_amount) {
                                        paymentDueAmt = subscriptionDetails[0].getSubcriptionPaymentFormat[0].due_amount;
                                    }
                                }
                                if (subscriptionDetails[0].getSubcriptionPaymentFormat[0]) {
                                    if (subscriptionDetails[0].getSubcriptionPaymentFormat[0].payment_end_date) {
                                        paymentDueDate = await hyphenDateFormat(subscriptionDetails[0].getSubcriptionPaymentFormat[0].payment_end_date);
                                    }
                                }
                                let pricepack = subscriptionDetails[0]['pricepack.pricepackName'];
                                let durationTypeData = subscriptionDetails[0]['pricepack.durationType'];
                                // if (durationTypeData == '1') {
                                //     durationTypeName = 'Session';
                                // } else if (durationTypeData == '2') {
                                //     durationTypeName = 'Day';
                                // } else if (durationTypeData == '3') {
                                //     durationTypeName = 'Week';
                                // } else if (durationTypeData == '4') {
                                //     durationTypeName = 'Month';
                                // } else if (durationTypeData == '5') {
                                //     durationTypeName = 'Year';
                                // } else if (durationTypeData == '6') {
                                //     durationTypeName = 'Item';
                                // } else {
                                //     durationTypeName = 'Consultancy(Hours)';
                                // }
                                if (durationTypeData == '1') {
                                    durationTypeName = 'Session';
                                } else {
                                    durationTypeName = 'Day';
                                }
                                if (reminderType == 0) {
                                    message = `Hi ` + clientData[0].clientName + `, this is a gentle reminder from ` + subscriptionDetails[0].businessinfo[0].businessType + ` to pay your outstanding payment of INR ` + paymentDueAmt + ` due on ` + paymentDueDate + ` for our services under ` + subscriptionDetails[0].sessionInfo[0].message + ` ` + pricepack + `. You now have ` + remainingSessions + ` ` + durationTypeName + ` remaining. For any other queries, please contact ` + HELPLINE + `. To pay online, click on the link below.`;
                                } else {
                                    message = `Hi ` + clientData[0].clientName + `, this is a gentle reminder from ` + subscriptionDetails[0].businessinfo[0].businessType + ` to pay your outstanding payment towards ` + subscriptionDetails[0].sessionInfo[0].message + ` ` + pricepack + ` of INR ` + paymentDueAmt + `, ` + remainingSessions + ` ` + durationTypeName + ` left. For any other queries, please contact ` + HELPLINE + `. To pay online, click on the link below.-bell`;
                                }
                                let header = 'PAYMENT REMINDER';
                                let new_current_date = await hyphenDateFormat(new Date());
                                let sessionPercentage = await getSessionPercentage(information, clientTotalPaymentData[m].subscriptionsId);
                                for (let x = 0; x < subscriptionDetails[0].getSubcriptionPaymentFormat.length; x++) {
                                    if ((subscriptionDetails[0]['pricepack.durationType'] == '1') && (sessionPercentage.percentage >= '70')) {
                                        await clientTotalPaymentdb.clientpayments.update({
                                            paymentReminder_status: 0
                                        }, {
                                            where: {
                                                clientpaymentId: subscriptionDetails[0].getSubcriptionPaymentFormat[x].clientpaymentId,
                                                businessId: subscriptionDetails[0].getSubcriptionPaymentFormat[x].businessId,
                                            }
                                        });
                                        if (clientData) {
                                            if (clientData[0].contactNumber) {
                                                await sendSmsMobile(clientData[0].contactNumber, message);
                                            }
                                            if (clientData[0].emailId) {
                                                let emailBody = `Hi ` + clientData[0].clientName + `<br><br>, 
                                                    This is a gentle reminder to pay your outstanding payment of INR ` + paymentDueAmt + ` due on ` + paymentDueDate + ` for our services under ` + subscriptionDetails[0].sessionInfo[0].message + serviceList + `. You now have ` + remainingSessions + ` remaining <br><br>
                                                    Kindly clear your balance to enjoy un-interrupted services.<br><br>
                                                    For any help or assistance, reach out to us anytime at +91 ` + HELPLINE + ` <br><br>
                                                    Kind regards,<br><br>` + subscriptionDetails[0].businessinfo[0].businessType;
                                                let subject = `Payment of INR ` + paymentDueAmt + ` pending for ` + subscriptionDetails[0].sessionInfo[0].message + ` ` + serviceList;
                                                await sendEmail(
                                                    clientData[0].emailId,
                                                    subject,
                                                    emailBody
                                                );
                                            }
                                            const newLog = {
                                                businessId: subscriptionDetails[0].getSubcriptionPaymentFormat[x].businessId,
                                                activity: {
                                                    Status: 2,
                                                    header: 'Payments due TODAY',
                                                    activityId: subscriptionDetails[0].getSubcriptionPaymentFormat[x].clientpaymentId,
                                                    activityName: 'update',
                                                    activityDate: todayDateCheck + ' ' + todaytoLocaleTimeString,
                                                    activityTime: todayDateCheck + ' ' + todaytoLocaleTimeString,
                                                    message: 'You have 1 payments due TODAY. They have already been reminded once.',
                                                    attendance: '',
                                                    payment: paymentDueAmt
                                                },
                                                referenceTable: 'clientpayments',
                                            };
                                            let log = await dbLog.log.create(newLog);
                                        }
                                    }
                                    if ((subscriptionDetails[0]['pricepack.durationType'] != '1')) {
                                        await clientTotalPaymentdb.clientpayments.update({
                                            paymentReminder_status: 0
                                        }, {
                                            where: {
                                                clientpaymentId: subscriptionDetails[0].getSubcriptionPaymentFormat[x].clientpaymentId,
                                                businessId: subscriptionDetails[0].getSubcriptionPaymentFormat[x].businessId,
                                            }
                                        });
                                        if (clientData) {
                                            if (clientData[0].contactNumber) {
                                                await sendSmsMobile(clientData[0].contactNumber, message);
                                            }
                                            if (clientData[0].emailId) {
                                                let emailBody = `Hi ` + clientData[0].clientName + `<br><br>, 
                                                    This is a gentle reminder to pay your outstanding payment of INR ` + paymentDueAmt + ` due on ` + paymentDueDate + ` for our services under ` + subscriptionDetails[0].sessionInfo[0].message + ` ` + serviceList + `. You now have ` + remainingSessions + ` ` + durationTypeName + ` remaining <br><br>
                                                    Kindly clear your balance to enjoy un-interrupted services.<br><br>
                                                    For any help or assistance, reach out to us anytime at +91 ` + HELPLINE + ` <br><br>
                                                    Kind regards,<br><br>` + subscriptionDetails[0].businessinfo[0].businessType;
                                                let subject = `Payment of INR ` + paymentDueAmt + ` pending for ` + subscriptionDetails[0].sessionInfo[0].message + ` ` + serviceList;
                                                await sendEmail(
                                                    clientData[0].emailId,
                                                    subject,
                                                    emailBody
                                                );
                                            }
                                        }
                                        const newLogNew = {
                                            businessId: subscriptionDetails[0].getSubcriptionPaymentFormat[x].businessId,
                                            activity: {
                                                Status: 2,
                                                header: 'Payments due TODAY',
                                                activityId: subscriptionDetails[0].getSubcriptionPaymentFormat[x].clientpaymentId,
                                                activityName: 'update',
                                                activityDate: todayDateCheck + ' ' + todaytoLocaleTimeString,
                                                activityTime: todayDateCheck + ' ' + todaytoLocaleTimeString,
                                                message: 'You have 1 payments due TODAY. They have already been reminded once.',
                                                attendance: '',
                                                payment: paymentDueAmt
                                            },
                                            referenceTable: 'clientpayments',
                                        };
                                        let log = await dbLog.log.create(newLogNew);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            clientTotalPaymentdb.sequelize.close();
            dbLog.sequelize.close();
            return true;
        } catch (error) {
            throw error;
        }
    };


    paymentReminderActionDb = async(
        information
    ) => {
        try {
            let businessIdinformation = information.split(",")[1];
            let todayFormat = await hyphenDateFormat(new Date());
            let seqdb = new Sequelize(businessIdinformation, KNACKDB.user, KNACKDB.password, {
                host: KNACKDB.host,
                port: 3306,
                dialect: "mysql",
                pool: {
                    max: 100,
                    min: 0,
                    idle: 10000
                }
            });
            let paymentReminderList = await seqdb.query("SELECT GROUP_CONCAT(`clientpaymentId`) AS `clientpaymentId` FROM `clientpayments` WHERE `paymentReminder_status` IS NULL AND `paymentReminder_date` = '" + todayFormat + "' AND `status` = 1 AND `businessId` = '" + businessIdinformation + "'", {
                type: Sequelize.QueryTypes.SELECT
            });
            return paymentReminderList;
        } catch (error) {
            throw error;
        }
    };



}
export default new PendingpaymentDB();