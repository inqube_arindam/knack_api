import {
    filterFields,
    createInstance,
    clientInfo,
    date_ISO_Convert,
    getPaymentCalculation,
    hyphenDateFormat,
    sendSmsMobile,
    sessionInfo
} from "../../lib/common";
import {
    KNACKDB,
    KNACK_DBSETTINGS,
    KNACK_UPLOAD_URL,
    PEGINATION_LIMIT
} from "../../lib/constants.js";
import Sequelize from "sequelize";
var csv = require('fast-csv');
var fs = require('fs');
const PUBLIC_FIELDS = [
    "businessId",
    "centerId",
    "clientId",
    "clientName",
    "contactNumber",
    "emailId",
    "dateOfBirth",
    "area",
    "pin",
    "city",
    "photoUrl",
    "filepath"
];
import {
    ApplicationError
} from "../../lib/errors";

import SubscriptionDB from "../../db/ClientModels/ClientSubscriptionModel";

export class ClientDB {
    clients = [];

    //Adding new client detail
    create = async(
        information,
        businessId,
        //centerId,
        clientName,
        contactNumber,
        emailId,
        alternateEmail,
        area,
        pin,
        city,
        dateOfBirth,
        photoUrl
    ) => {
        // const newClient = {
        //   businessId,
        //   centerId,
        //   clientName,
        //   contactNumber,
        //   emailId,
        //   area,
        //   pin,
        //   city,
        //   dateOfBirth,
        //   photoUrl
        // };
        // try {
        //   let db = createInstance(["client"], information);
        //   let clients = await db.clients.create(newClient);
        //   db.sequelize.close();
        //   return await filterFields(clients, PUBLIC_FIELDS, true);
        // } catch (error) {
        //   throw error;
        // }

        try {
            let arrayOfStrings = information.split(",");
            let userId = arrayOfStrings.length > 2 ? arrayOfStrings[2] : null;
            let db = createInstance(["client"], information);

            if (photoUrl) {
                // if (!req.files) {
                //   return res.status(400).send('No files were uploaded.');
                // }

                // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
                // let sampleFile = req.files.client_profile;
                var filename = photoUrl.name;

                // Use the mv() method to place the file somewhere on your server
                await photoUrl.mv("./uploads/clients/" + filename, function(err) {
                    // if (err) {
                    //   return res.status(500).send(err);
                    // }
                    // res.send('File uploaded!');
                });
                var photoUrl = filename;
            } else {
                var photoUrl = null;
            }
            let dateNow = new Date();
            let dob;
            if (dateOfBirth) {
                dob = (dateOfBirth == '') ? null : await formatDate(dateOfBirth);
            } else {
                dob = null;
            }
            const newClient = {
                businessId,
                //centerId,
                clientName,
                contactNumber,
                emailId,
                alternateEmail,
                area,
                pin,
                city,
                dateOfBirth: dob,
                photoUrl,
                createdBy: userId
                // clientJoiningDate: dateNow
            };

            let clients = await db.clients.create(newClient);

            let text = "You have been successfully added to the Knack as a Client";
            await sendSmsMobile(contactNumber, text);

            // Log entry
            // let dbLog = createInstance(["log"], information);
            // const newLog = {
            //     businessId: businessId,
            //     activity: {
            //         header: 'Client Added',
            //         ClientName: clientName,
            //         ContactNumber: contactNumber,
            //         EmailId: emailId,
            //         Area: area,
            //         Pin: pin,
            //         City: city,
            //         activityDate: new Date(),
            //         activityTime: new Date(),
            //         message: 'Attendance Recorded',
            //     },
            //     referenceTable: 'Client',
            // };

            // let log = await dbLog.log.create(newLog);

            db.sequelize.close();
            return await filterFields(clients, PUBLIC_FIELDS, true);
        } catch (error) {
            throw error;
        }
    };

    listAll = async(information, businessId, pricepackId, filterPrivateFields = true) => {
        try {
            let db = createInstance(["client"], information);
            let clients = await db.clients.findAll({
                order: [
                    ["clientName", "asc"]
                ],
                where: {
                    businessId: businessId,
                    status: 1
                },
                raw: true
            });
            let dbSub = createInstance(["clientSubscription"], information);
            let dbPPack = createInstance(["clientPricepack"], information);

            var filterData = [];
            var arr = [];
            var c = 0;
            var count = 0;
            for (var i = 65; i <= 90; i++) {
                arr[c] = String.fromCharCode(i).toLowerCase();
                let filterClients = await db.clients.findAll({
                    order: [
                        ["clientName", "asc"]
                    ],
                    where: {
                        businessId: businessId,
                        status: 1,
                        clientName: {
                            like: [arr[c] + "%"]
                        }
                    },
                    raw: true
                });


                if (filterClients.length) { //this condition hides the empty arrays of names
                    var merged = {};
                    var m = [];
                    var con = [];
                    for (var k = 0; k < filterClients.length; k++) {
                        var arrnew = [];
                        let flag = 0;
                        var isphoto = (filterClients[k].photoUrl === null) ? 'noimage.jpg' : filterClients[k].photoUrl;

                        let subscriptions = await dbSub.subscriptions.findAll({
                            where: {
                                clientId: filterClients[k].clientId,
                                renewl_status: 1,
                                status: 1
                            },
                            raw: true
                        });
                        //checking if subscription is available
                        let subscriptionStatus;
                        let subscriptionId;
                        if (subscriptions.length > 0) {
                            for (let subscription of subscriptions) {
                                let pricepack = await dbPPack.pricepacks.findAll({
                                    where: {
                                        pricepackId: subscription.pricepacks
                                    }
                                });
                                let today = new Date();
                                let subscription_validity = new Date(subscription.subscriptionDateTime);
                                subscription_validity.setDate(subscription_validity.getDate() + parseInt(pricepack[0].pricepackValidity));
                                subscriptionStatus = (subscription_validity > today) ? 'active' : 'inactive';
                                if (subscriptionStatus == 'active') break;

                            }
                            //checking if pricepack available
                            for (let subscription of subscriptions) {
                                let pricepack = await dbPPack.pricepacks.findAll({
                                    where: {
                                        pricepackId: subscription.pricepacks
                                    }
                                });
                                if (pricepack[0].pricepackId == pricepackId[0]) {
                                    var ppid = pricepack[0].pricepackId;
                                    var ppname = pricepack[0].pricepackName;
                                    flag = 1;
                                    break;
                                }
                            }
                            subscriptionId = subscriptions[0].subscriptionId;
                        } else {
                            subscriptionStatus = 'lead';
                            subscriptionId = '';
                        }

                        if (flag == 1) {
                            Array.prototype.push.apply(arrnew, [filterClients[k]]);
                            Array.prototype.push.apply(arrnew, [{
                                newphotoUrl: KNACK_UPLOAD_URL.localUrl + isphoto
                            }]);
                            Array.prototype.push.apply(arrnew, [{
                                count: count++
                            }]);
                            Array.prototype.push.apply(arrnew, [{
                                subscriptionId: subscriptionId
                            }]);
                            Array.prototype.push.apply(arrnew, [{
                                subscriptionStatus: subscriptionStatus
                            }]);
                            Array.prototype.push.apply(arrnew, [{
                                pricepackStatus: 1
                            }]);
                            Array.prototype.push.apply(arrnew, [{
                                Pricepack: {
                                    pricepackId: ppid,
                                    pricepackName: ppname
                                }
                            }]);
                            m = Object.assign.apply(Object, arrnew);
                            con = con.concat(m);
                        } else {
                            Array.prototype.push.apply(arrnew, [filterClients[k]]);
                            Array.prototype.push.apply(arrnew, [{
                                newphotoUrl: KNACK_UPLOAD_URL.localUrl + isphoto
                            }]);
                            Array.prototype.push.apply(arrnew, [{
                                count: count++
                            }]);
                            Array.prototype.push.apply(arrnew, [{
                                subscriptionId: subscriptionId
                            }]);
                            Array.prototype.push.apply(arrnew, [{
                                subscriptionStatus: subscriptionStatus
                            }]);
                            Array.prototype.push.apply(arrnew, [{
                                pricepackStatus: 0
                            }]);
                            m = Object.assign.apply(Object, arrnew);
                            con = con.concat(m);
                        }
                    }

                    merged = Object.assign.apply(Object, [{
                        title: arr[c].toUpperCase(),
                        data: con
                    }]);
                    filterData = filterData.concat(merged);
                }
                c++;
            }
            dbSub.sequelize.close();
            dbPPack.sequelize.close();
            return await filterData;
        } catch (error) {
            throw error;
        }
    };


    listAllClient = async(information, param, filterPrivateFields = true) => {
        try {
            var businessIdinformation = information.split(",")[1];
            let todayFormat = await date_ISO_Convert(new Date());
            // let todayFormat = "2018-10-24";
            let dbsubscriptions = createInstance(["clientSubscription"], information);
            let dbClients = createInstance(["client"], information);
            let dbpricepack = createInstance(["clientPricepack"], information);
            dbClients.clients.hasMany(dbsubscriptions.subscriptions, {
                foreignKey: "clientId",
                targetKey: "clientId"
            });
            dbsubscriptions.subscriptions.belongsTo(dbpricepack.pricepacks, {
                foreignKey: "pricepacks",
                targetKey: "pricepackId"
            });
            let filter = (param[0]) ? param[0] : '';
            var pageNo = (param['page']) ? param['page'] : 1;
            var limitPerPage = PEGINATION_LIMIT;
            var offsetLimit = (pageNo - 1) * limitPerPage;
            var itemConditions = {};
            var itemGroupConditions = {};
            var itemGroupConditionsPricePack = {};
            let clientsList = await dbClients.clients.findAndCount({
                attributes: {
                    include: [
                        [dbClients.clients.sequelize.literal('CASE WHEN photoUrl  is NULL THEN CONCAT("' + KNACK_UPLOAD_URL.localUrl + '" , "noimage.jpg" ) ELSE CONCAT("' + KNACK_UPLOAD_URL.localUrl + '" , photoUrl ) END'), 'photoFullUrl'],
                        [dbClients.sequelize.fn('SUBSTRING', dbClients.sequelize.fn('SOUNDEX', dbClients.sequelize.col('clientName')), 1, 1), 'name_initials']
                    ],
                    exclude: []
                },
                where: {
                    status: 1,
                    businessId: businessIdinformation,
                    clientName: {
                        like: '%' + filter + '%'
                    }
                },
                order: [
                    ['clientName', 'ASC']
                ],
                paranoid: false,
                required: true,
                offset: offsetLimit,
                limit: limitPerPage,
                // include: [{
                //     model: dbsubscriptions.subscriptions,
                //     attributes: ["subscriptionId", "clientId"],
                //     where: itemConditions,
                //     paranoid: false,
                //     required: false,
                //     include: [{
                //         model: dbpricepack.pricepacks,
                //         attributes: ["pricepackName"],
                //         where: itemGroupConditionsPricePack,
                //     }]
                // }],
                subQuery: false,
                raw: true
            });

            dbsubscriptions.sequelize.close();
            dbClients.sequelize.close();
            dbpricepack.sequelize.close();

            if (clientsList.rows.length > 0) {
                for (var i = 0; i < clientsList.rows.length; i++) {
                    clientsList.rows[i]["clientType"] = await this.clientType(information, clientsList.rows[i]["clientId"], todayFormat);
                }
            }

            return clientsList;
        } catch (error) {
            throw error;
        }
    };


    listAllClientNew = async(
        information,
        status,
        search_data,
        param
    ) => {
        try {
            var businessIdinformation = information.split(",")[1];
            let todayFormat = await date_ISO_Convert(new Date());
            // let todayFormat = "2018-10-24";
            let dbsubscriptions = createInstance(["clientSubscription"], information);
            let dbClients = createInstance(["client"], information);
            let dbpricepack = createInstance(["clientPricepack"], information);
            dbClients.clients.hasMany(dbsubscriptions.subscriptions, {
                foreignKey: "clientId",
                targetKey: "clientId"
            });
            dbsubscriptions.subscriptions.belongsTo(dbpricepack.pricepacks, {
                foreignKey: "pricepacks",
                targetKey: "pricepackId"
            });

            let dbCSM = createInstance(["clientScheduleMap"], information);
            let dbSchedule = createInstance(["clientSchedule"], information);
            let dbServices = createInstance(["clientService"], information);
            let dbSubscription = createInstance(["clientSubscription"], information);
            let dbPricepack = createInstance(["clientPricepack"], information);

            dbCSM.clientschedulemap.belongsTo(dbSchedule.schedule, {
                foreignKey: "scheduleId",
                targetKey: "scheduleId"
            });
            dbSchedule.schedule.belongsTo(dbServices.services, {
                foreignKey: "serviceId",
                targetKey: "serviceId"
            });
            dbSubscription.subscriptions.belongsTo(dbPricepack.pricepacks, {
                foreignKey: "pricepacks",
                targetKey: "pricepackId"
            });

            let filter = (search_data) ? search_data : '';
            var pageNo = (param.page) ? param.page : 1;
            var limitPerPage = PEGINATION_LIMIT;
            var offsetLimit = (pageNo - 1) * limitPerPage;
            var itemConditions = {};
            var itemGroupConditions = {};
            var itemGroupConditionsPricePack = {};

            var itemConditionsForClient = {
                status: 1,
                businessId: businessIdinformation,
                clientName: {
                    like: '%' + filter + '%'
                }
            };
            if ((status) && (isNaN(parseFloat(status)) != true)) {
                itemConditionsForClient = {
                    status: Number(status),
                    businessId: businessIdinformation,
                    clientName: {
                        like: '%' + filter + '%'
                    }
                };
            }
            let clientsList = await dbClients.clients.findAndCount({
                attributes: {
                    include: [
                        [dbClients.clients.sequelize.literal('CASE WHEN photoUrl  is NULL THEN CONCAT("' + KNACK_UPLOAD_URL.localUrl + '" , "noimage.jpg" ) ELSE CONCAT("' + KNACK_UPLOAD_URL.localUrl + '" , photoUrl ) END'), 'photoFullUrl'],
                        [dbClients.sequelize.fn('SUBSTRING', dbClients.sequelize.fn('SOUNDEX', dbClients.sequelize.col('clientName')), 1, 1), 'name_initials']
                    ],
                    exclude: []
                },
                where: itemConditionsForClient,
                order: [
                    ['clientName', 'ASC']
                ],
                paranoid: false,
                required: true,
                offset: offsetLimit,
                limit: limitPerPage,
                subQuery: false,
                raw: true
            });
            if (clientsList.rows.length > 0) {
                for (var i = 0; i < clientsList.rows.length; i++) {
                    let bookings = await dbCSM.clientschedulemap.findAll({
                        where: {
                            clientId: clientsList.rows[i].clientId
                        },
                        include: [{
                            model: dbSchedule.schedule,
                            include: [{
                                model: dbServices.services,
                                attributes: ['serviceName']
                            }],
                        }],
                        order: [
                            [{
                                    model: dbSchedule.schedule
                                },
                                'scheduleDate',
                                'DESC'
                            ]
                        ],
                        raw: true
                    });
                    let bookingArr = [];
                    for (let booking of bookings) {
                        let subscriptions = await dbSubscription.subscriptions.find({
                            where: {
                                subscriptionId: booking.subscriptionId
                            },
                            include: [{
                                model: dbPricepack.pricepacks
                            }],
                            raw: true
                        });
                        booking['subscriptionDetails'] = await SubscriptionDB.details(information, booking.subscriptionId);
                        booking['bookingName'] = booking['schedule.scheduleName'] == null ? booking['schedule.service.serviceName'] : booking['schedule.scheduleName'];
                        for (let schedule of booking.attendanceType) {
                            booking['sessionInfo'] = booking.sessionCount + '/' + subscriptions['pricepack.serviceDuration'];
                            bookingArr.push(booking);
                            break;
                        }
                    }

                    clientsList.rows[i]["booking_list"] = bookingArr;
                }
            }

            return clientsList;

            // if (clientsList.rows.length > 0) {
            //     for (var i = 0; i < clientsList.rows.length; i++) {
            //         clientsList.rows[i]["services"] = await dbsubscriptions.sequelize.query("SELECT services.serviceId,serviceName,`servicepricepackmap`.pricepackId,pricepackName,pricepackValidity,pricepacks.serviceDuration,clientschedulemap.scheduleId,sessionCount,scheduleName,scheduleDate,startTime,endTime,subscriptions.subscriptionId,subscriptions.subscriptionDateTime,(SELECT DATE_ADD(subscriptionDateTime, INTERVAL pricepackValidity DAY)) as validity FROM `services` LEFT OUTER JOIN `servicepricepackmap` ON services.serviceId = servicepricepackmap.serviceId LEFT OUTER JOIN `subscriptions` ON `servicepricepackmap`.pricepackId = `subscriptions`.pricepacks LEFT OUTER JOIN `pricepacks` ON servicepricepackmap.pricepackId = pricepacks.pricepackId LEFT OUTER JOIN `clientschedulemap` ON clientschedulemap.clientId = `subscriptions`.clientId LEFT OUTER JOIN `schedule` ON clientschedulemap.scheduleId = schedule.scheduleId WHERE `subscriptions`.clientId='" + clientsList.rows[i]["clientId"] + "'", {
            //             type: dbsubscriptions.sequelize.QueryTypes.SELECT
            //         })
            //         clientsList.rows[i]["pricepacks"] = await dbsubscriptions.subscriptions.findAll({
            //             attributes: ['subscriptionId', 'subscriptionDateTime'],
            //             where: {
            //                 clientId: clientsList.rows[i]["clientId"]
            //             },
            //             include: [{
            //                 model: dbpricepack.pricepacks,
            //                 attributes: ['pricepackId', 'pricepackName', 'pricepackValidity', 'serviceDuration'],
            //             }],
            //             raw: true
            //         });
            //         clientsList.rows[i]["schedule"] = await dbsubscriptions.sequelize.query("SELECT clientschedulemap.scheduleId,scheduleName,scheduleDate,startTime,endTime FROM `schedule` LEFT OUTER JOIN `clientschedulemap` ON schedule.scheduleId = clientschedulemap.scheduleId  WHERE `clientschedulemap`.clientId='" + clientsList.rows[i]["clientId"] + "'", {
            //             type: dbsubscriptions.sequelize.QueryTypes.SELECT
            //         });
            //     }
            // }
            // for (var x = 0; x < clientsList.rows.length; x++) {
            //     if (clientsList.rows[x]["services"].length > 0) {
            //         for (var y = 0; y < clientsList.rows[x]["services"].length; y++) {

            //             clientsList.rows[x]["services"][y]["paidAmount"] = await dbsubscriptions.sequelize.query("SELECT SUM(payble_amount) as paidAmount FROM `clientpayments` WHERE `subscriptionsId` = '" + clientsList.rows[x]["services"][y]["subscriptionId"] + "'GROUP BY `subscriptionsId`", {
            //                 type: dbsubscriptions.sequelize.QueryTypes.SELECT
            //             });

            //             clientsList.rows[x]["services"][y]["dueAmount"] = await dbsubscriptions.sequelize.query("SELECT distinct(due_amount) FROM `clientpayments` WHERE `subscriptionsId` = '" + clientsList.rows[x]["services"][y]["subscriptionId"] + "'", {
            //                 type: dbsubscriptions.sequelize.QueryTypes.SELECT
            //             });
            //         }
            //     }
            // }
            dbsubscriptions.sequelize.close();
            dbClients.sequelize.close();
            dbpricepack.sequelize.close();
            return clientsList;
        } catch (error) {
            throw error;
        }
    };

    clientType = async(
        information,
        clientid,
        today,
    ) => {
        try {
            let businessIdinformation = information.split(",")[1];
            let dbsubscriptions = createInstance(["clientSubscription"], information);
            let final = {
                "activeClient": false,
                "inactiveClient": false,
                "leadClient": false
            }
            let activeClientListCount = await dbsubscriptions.sequelize.query(
                "SELECT COUNT(*) AS count FROM `clients` WHERE `clientid` IN ( SELECT DISTINCT(`clientid`) FROM `subscriptions` INNER JOIN `pricepacks` AS `pricepack` ON `subscriptions`.`pricepacks` = `pricepack`.`pricepackid` WHERE '" + today + "' BETWEEN `subscriptiondatetime` AND Date_add( `subscriptions`.`subscriptiondatetime`,INTERVAL `pricepack`.`pricepackvalidity` day)) AND `clients`.`status` = 1 AND `clients`.`businessId` = '" + businessIdinformation + "' AND `clients`.`clientId` = '" + clientid + "' ", {
                    type: dbsubscriptions.sequelize.QueryTypes.SELECT
                }
            );
            let inactiveClientListCount = await dbsubscriptions.sequelize.query(
                "SELECT count(*) count FROM `clients` WHERE `clientId` in ( select `clientid` from ( SELECT DISTINCT (`clientid`), `subscriptionId` FROM `subscriptions` INNER JOIN `pricepacks` AS `pricepack` ON `subscriptions`.`pricepacks` = `pricepack`.`pricepackid` WHERE Date_add( `subscriptions`.`subscriptiondatetime`, INTERVAL `pricepack`.`pricepackvalidity` day) < '" + today + "') as `p` WHERE `p`.`clientid` not in (SELECT `a`.`clientid` FROM ( SELECT `clientid` FROM `clients` WHERE `clientid` IN ( SELECT DISTINCT (`clientid`) FROM `subscriptions` INNER JOIN `pricepacks` AS `pricepack` ON `subscriptions`.`pricepacks` = `pricepack`.`pricepackid` WHERE '" + today + "' BETWEEN `subscriptiondatetime` AND Date_add( `subscriptions`.`subscriptiondatetime`, INTERVAL `pricepack`.`pricepackvalidity` day) ) AND `clients`.`status` = 1 AND `clients`.`businessId` = '" + businessIdinformation + "') `a` LEFT JOIN ( SELECT `clientid` FROM `clients` WHERE `clientid` IN ( SELECT DISTINCT (`clientid`) FROM `subscriptions` INNER JOIN `pricepacks` AS `pricepack` ON `subscriptions`.`pricepacks` = `pricepack`.`pricepackid` WHERE Date_add( `subscriptions`.`subscriptiondatetime`, INTERVAL `pricepack`.`pricepackvalidity` day) < '" + today + "' )AND `clients`.`status` = 1 AND `clients`.`businessId` = '" + businessIdinformation + "') `b` ON `a`.`clientid` = `b`.`clientid` WHERE `b`.`clientid` IS NOT NULL )  )  AND `clients`.`clientId` = '" + clientid + "' ", {
                    type: dbsubscriptions.sequelize.QueryTypes.SELECT
                }
            );
            dbsubscriptions.sequelize.close();
            if ((activeClientListCount[0].count > 0)) {
                final.activeClient = true;
            } else if ((inactiveClientListCount[0].count > 0)) {
                final.inactiveClient = true;
            } else {
                final.leadClient = true;
            }
            return final;
        } catch (error) {
            throw error;
        }
    };

    listClientServicePricepack = async(information, param, filterPrivateFields = true) => {
        try {
            var businessIdinformation = information.split(",")[1];
            let getid = param['id'];
            let dbService = createInstance(["clientService"], information);
            let todayFormat = await date_ISO_Convert(new Date());
            let dbsubscriptions = createInstance(["clientSubscription"], information);
            let dbClients = createInstance(["client"], information);
            let dbpricepack = createInstance(["clientPricepack"], information);

            // check id is service id or pricepack id
            let isservice = await dbService.services.findAll({
                where: {
                    status: 1,
                    serviceId: getid
                },
                raw: true
            });

            let ispricepack = await dbpricepack.pricepacks.findAll({
                where: {
                    status: 1,
                    pricepackId: getid
                },
                raw: true
            });

            let setid;
            if (isservice.length > 0) {
                setid = getid;
            } else if (ispricepack.length > 0) {
                setid = getid;
            } else {
                throw new ApplicationError('Please send either service or pricepack id', 401);
            }

            // dbClients.clients.hasMany(dbsubscriptions.subscriptions, {
            //     foreignKey: "clientId",
            //     targetKey: "clientId"
            // });
            dbsubscriptions.subscriptions.belongsTo(dbpricepack.pricepacks, {
                foreignKey: "pricepacks",
                targetKey: "pricepackId"
            });
            let filter = (param[0]) ? param[0] : '';
            var pageNo = (param['page']) ? param['page'] : 1;
            var limitPerPage = PEGINATION_LIMIT;
            var offsetLimit = (pageNo - 1) * limitPerPage;
            // var itemConditions = { pricepacks: setid };
            var itemConditions = {};
            var itemGroupConditions = {};
            var itemGroupConditionsPricePack = {

            };
            let clientsList = await dbClients.clients.findAndCount({
                attributes: {
                    include: [
                        [dbClients.clients.sequelize.literal('CASE WHEN photoUrl  is NULL THEN CONCAT("' + KNACK_UPLOAD_URL.localUrl + '" , "noimage.jpg" ) ELSE CONCAT("' + KNACK_UPLOAD_URL.localUrl + '" , photoUrl ) END'), 'photoFullUrl'],
                        [dbClients.sequelize.fn('SUBSTRING', dbClients.sequelize.fn('SOUNDEX', dbClients.sequelize.col('clientName')), 1, 1), 'name_initials']
                    ],
                    exclude: []
                },
                where: {
                    status: 1,
                    businessId: businessIdinformation,
                    clientName: {
                        like: '%' + filter + '%'
                    }
                },
                order: [
                    ['clientName', 'ASC']
                ],
                paranoid: false,
                required: true,
                offset: offsetLimit,
                limit: limitPerPage,
                // include: [{
                //     model: dbsubscriptions.subscriptions,
                //     attributes: ["subscriptionId", "clientId", "pricepacks"],
                //     where: itemConditions,
                //     paranoid: false,
                //     required: false,
                //     include: [{
                //         model: dbpricepack.pricepacks,
                //         attributes: ["pricepackId", "pricepackName"],
                //         where: itemGroupConditionsPricePack,
                //     }]
                // }],
                subQuery: false,
                raw: true
            });


            //if (clientsList.rows.length > 0) {
            for (var i = 0; i < clientsList.rows.length; i++) {
                let pricepackArr = [];
                clientsList.rows[i]["clientType"] = await this.clientTypeServicePricepack(information, clientsList.rows[i]["clientId"], setid, todayFormat);
                clientsList.rows[i]["addedClient"] = false;
                clientsList.rows[i]["subscribeClient"] = false;
                clientsList.rows[i]["pricepacks"] = await dbsubscriptions.subscriptions.findAll({
                    attributes: ["subscriptionId"],
                    where: {
                        clientId: clientsList.rows[i]["clientId"]
                    },
                    include: [{
                        model: dbpricepack.pricepacks,
                        attributes: ['pricepackId', 'pricepackName'],
                    }],
                    raw: true
                });
                for (var j = 0; j < clientsList.rows[i]["pricepacks"].length; j++) {
                    if (clientsList.rows[i]["pricepacks"][j]['subscriptionId']) {
                        clientsList.rows[i]["subscribeClient"] = true;

                        if (clientsList.rows[i]["pricepacks"][j]["pricepack.pricepackId"] == setid) {
                            clientsList.rows[i]["addedClient"] = true;
                            break;
                        }
                    }

                }
            }
            //}


            // let uniqueClientId = [];
            // let finalData = [];
            // clientsList.rows.forEach(function (client, index) {
            //     if (!inArray(client.clientId, uniqueClientId)) {
            //         uniqueClientId.push(client.clientId);
            //         finalData.push(client)
            //     }
            // })

            dbsubscriptions.sequelize.close();
            dbClients.sequelize.close();
            dbpricepack.sequelize.close();

            // return {
            //     count: finalData.length,
            //     rows: finalData.slice(offsetLimit, (offsetLimit + limitPerPage))
            // };
            return clientsList;

        } catch (error) {
            throw error;
        }
    };

    leadClientList = async(information, param, filterPrivateFields = true) => {
        try {
            var businessIdinformation = information.split(",")[1];
            let dbService = createInstance(["clientService"], information);
            let todayFormat = await date_ISO_Convert(new Date());
            let dbsubscriptions = createInstance(["clientSubscription"], information);
            let dbClients = createInstance(["client"], information);
            let dbpricepack = createInstance(["clientPricepack"], information);

            dbsubscriptions.subscriptions.belongsTo(dbpricepack.pricepacks, {
                foreignKey: "pricepacks",
                targetKey: "pricepackId"
            });
            let filter = (param[0]) ? param[0] : '';
            var pageNo = (param['page']) ? param['page'] : 1;
            var limitPerPage = PEGINATION_LIMIT;
            var offsetLimit = (pageNo - 1) * limitPerPage;
            // var itemConditions = { pricepacks: setid };
            var itemConditions = {};
            var itemGroupConditions = {};
            var itemGroupConditionsPricePack = {

            };

            let newLeadsClientListCount = await dbClients.clients.findAll({
                attributes: [
                    [dbClients.sequelize.fn("COUNT", dbClients.sequelize.col("clientId")), "count"]
                ],
                where: {
                    businessId: businessIdinformation,
                    status: 1,
                    clientId: {
                        notIn: dbClients.sequelize.literal("( SELECT `clientId` FROM `subscriptions` AS `subscriptions` WHERE `subscriptions`.`status` = 1 )")
                    }
                },
                raw: true
            });
            let newLeadsClientList = await dbClients.clients.findAll({
                attributes: {
                    include: [
                        [dbClients.clients.sequelize.literal('CASE WHEN photoUrl  is NULL THEN CONCAT("' + KNACK_UPLOAD_URL.localUrl + '" , "noimage.jpg" ) ELSE CONCAT("' + KNACK_UPLOAD_URL.localUrl + '" , photoUrl ) END'), 'photoFullUrl'],
                        [dbClients.sequelize.fn('SUBSTRING', dbClients.sequelize.fn('SOUNDEX', dbClients.sequelize.col('clientName')), 1, 1), 'name_initials']
                    ],
                    exclude: []
                },
                order: [
                    ["updateDateTime", "DESC"]
                ],
                where: {
                    businessId: businessIdinformation,
                    status: 1,
                    clientId: {
                        notIn: dbClients.sequelize.literal("( SELECT `clientId` FROM `subscriptions` AS `subscriptions` WHERE `subscriptions`.`status` = 1 )")
                    },
                    clientName: {
                        like: '%' + filter + '%'
                    }
                },
                order: [
                    ['clientName', 'ASC']
                ],
                paranoid: false,
                required: true,
                offset: offsetLimit,
                limit: limitPerPage,
                subQuery: false,
                raw: true
            });
            let new_leads = {
                "count": newLeadsClientListCount[0].count,
                "rows": newLeadsClientList
            }

            dbsubscriptions.sequelize.close();
            dbClients.sequelize.close();
            dbpricepack.sequelize.close();

            return new_leads;

        } catch (error) {
            throw error;
        }
    };


    clientTypeServicePricepack = async(
        information,
        clientid,
        setid,
        today,
    ) => {
        try {
            let businessIdinformation = information.split(",")[1];
            let dbsubscriptions = createInstance(["clientSubscription"], information);
            let final = {
                "activeClient": false,
                "inactiveClient": false,
                "leadClient": false
            }
            let activeClientListCount = await dbsubscriptions.sequelize.query(
                "SELECT COUNT(*) AS count FROM `clients` WHERE `clientid` IN ( SELECT DISTINCT(`clientid`) FROM `subscriptions` INNER JOIN `pricepacks` AS `pricepack` ON `subscriptions`.`pricepacks` = `pricepack`.`pricepackid` WHERE '" + today + "' BETWEEN `subscriptiondatetime` AND Date_add( `subscriptions`.`subscriptiondatetime`,INTERVAL `pricepack`.`pricepackvalidity` day) AND `subscriptions`.`pricepacks` = '" + setid + "' ) AND `clients`.`status` = 1 AND `clients`.`businessId` = '" + businessIdinformation + "' AND `clients`.`clientId` = '" + clientid + "' ", {
                    type: dbsubscriptions.sequelize.QueryTypes.SELECT
                }
            );
            let inactiveClientListCount = await dbsubscriptions.sequelize.query(
                "SELECT count(*) count FROM `clients` WHERE `clientId` in ( select `clientid` from ( SELECT DISTINCT (`clientid`), `subscriptionId` FROM `subscriptions` INNER JOIN `pricepacks` AS `pricepack` ON `subscriptions`.`pricepacks` = `pricepack`.`pricepackid` WHERE Date_add( `subscriptions`.`subscriptiondatetime`, INTERVAL `pricepack`.`pricepackvalidity` day) < '" + today + "') as `p` WHERE `p`.`clientid` not in (SELECT `a`.`clientid` FROM ( SELECT `clientid` FROM `clients` WHERE `clientid` IN ( SELECT DISTINCT (`clientid`) FROM `subscriptions` INNER JOIN `pricepacks` AS `pricepack` ON `subscriptions`.`pricepacks` = `pricepack`.`pricepackid` WHERE '" + today + "' BETWEEN `subscriptiondatetime` AND Date_add( `subscriptions`.`subscriptiondatetime`, INTERVAL `pricepack`.`pricepackvalidity` day) ) AND `clients`.`status` = 1 AND `clients`.`businessId` = '" + businessIdinformation + "') `a` LEFT JOIN ( SELECT `clientid` FROM `clients` WHERE `clientid` IN ( SELECT DISTINCT (`clientid`) FROM `subscriptions` INNER JOIN `pricepacks` AS `pricepack` ON `subscriptions`.`pricepacks` = `pricepack`.`pricepackid` WHERE Date_add( `subscriptions`.`subscriptiondatetime`, INTERVAL `pricepack`.`pricepackvalidity` day) < '" + today + "' )AND `clients`.`status` = 1 AND `clients`.`businessId` = '" + businessIdinformation + "') `b` ON `a`.`clientid` = `b`.`clientid` WHERE `b`.`clientid` IS NOT NULL )  )  AND `clients`.`clientId` = '" + clientid + "' ", {
                    type: dbsubscriptions.sequelize.QueryTypes.SELECT
                }
            );
            dbsubscriptions.sequelize.close();
            if ((activeClientListCount[0].count > 0)) {
                final.activeClient = true;
            } else if ((inactiveClientListCount[0].count > 0)) {
                final.inactiveClient = true;
            } else {
                final.leadClient = true;
            }
            return final;
        } catch (error) {
            throw error;
        }
    };

    //Listing all the clients under the passed business and Center IDs
    listAllcenter = async(information, centerId, filterPrivateFields = true) => {
        try {
            var getobj = {
                centerId: centerId,
                status: 1
            };
            let clientListCentre = await clientInfo(information, getobj);
            // let setPhoto = [];
            // for (var i = 0; i < clientListCentre.length; i++) {
            //   setPhoto[i] = {
            //     newphotoUrl: KNACK_UPLOAD_URL.localUrl + clientListCentre[i].photoUrl
            //   };
            // }
            // const combinedObject = Object.assign(setPhoto, clientListCentre);
            // console.log(combinedObject);

            let db = createInstance(["client"], information);
            let clients = await db.clients.findAll({
                where: {
                    centerId: centerId,
                    status: 1
                },
                raw: true
            });
            db.sequelize.close();
            if (filterPrivateFields) {
                return await filterFields(clientListCentre, PUBLIC_FIELDS);
            }
        } catch (error) {
            throw error;
        }
    };

    //Returning detail of one client
    clientDetails = async(information, clientId, filterPrivateFields = true) => {
        try {
            var businessIdinformation = information.split(",")[1];
            let todayDate = await hyphenDateFormat(new Date);
            let dbClients = createInstance(["client"], information);
            let dbSubs = createInstance(["clientSubscription"], information);
            let notedb = createInstance(["notes"], information);
            let dbclientpayments = createInstance(["clientPayments"], information);
            let dbsubscriptions = createInstance(["clientSubscription"], information);
            let dbpricepack = createInstance(["clientPricepack"], information);
            let dbclientexpense = createInstance(["clientExpense"], information);
            let dbpaymentTransactions = createInstance(["paymentTransactions"], information);
            let dbclientScheduleMap = createInstance(["clientScheduleMap"], information);
            dbclientpayments.clientpayments.belongsTo(dbsubscriptions.subscriptions, {
                foreignKey: "subscriptionsId",
                targetKey: "subscriptionId"
            });
            dbsubscriptions.subscriptions.belongsTo(dbClients.clients, {
                foreignKey: "clientId",
                targetKey: "clientId"
            });
            dbsubscriptions.subscriptions.belongsTo(dbpricepack.pricepacks, {
                foreignKey: "pricepacks",
                targetKey: "pricepackId"
            });
            dbclientpayments.clientpayments.belongsTo(dbpaymentTransactions.paymentTransactions, {
                foreignKey: "clientpaymentId",
                targetKey: "clientpaymentId"
            });
            dbsubscriptions.subscriptions.belongsTo(dbclientScheduleMap.clientschedulemap, {
                foreignKey: "subscriptionId",
                targetKey: "subscriptionId"
            });

            let allNotes = [];
            let clients = await dbClients.clients.findAll({
                attributes: {
                    include: ['clientId', 'clientName', 'contactNumber', 'emailId', [dbClients.clients.sequelize.literal('CASE WHEN photoUrl  is NULL THEN CONCAT("' + KNACK_UPLOAD_URL.localUrl + '" , "noimage.jpg" ) ELSE CONCAT("' + KNACK_UPLOAD_URL.localUrl + '" , photoUrl ) END'), 'photoUrl'],
                        [dbClients.clients.sequelize.literal('CASE WHEN photoUrl  is NULL THEN CONCAT("' + KNACK_UPLOAD_URL.localUrl + '" , "noimage.jpg" ) ELSE CONCAT("' + KNACK_UPLOAD_URL.localUrl + '" , photoUrl ) END'), 'photoFullUrl'],
                    ]
                },
                where: {
                    clientId: clientId,
                    status: 1
                },
                raw: true
            });

            if (clients.length == 0) {
                throw new ApplicationError("client does not exist.", 401);
            }

            // Client Subscription
            var mergedAllClients = [];
            for (var i = 0; i < clients.length; i++) {
                // let subscriptions = await dbSubs.subscriptions.findAll({
                //     where: {
                //         status: 1,
                //         clientId: clients[i].clientId
                //     },
                //     raw: true
                // });
                clients[i]["clientType"] = await this.clientType(information, clients[i].clientId, todayDate);

                let itemclientpaymentsRiminderData = {
                    status: 1,
                    businessId: businessIdinformation,
                    clientpaymentId: {
                        [dbclientpayments.sequelize.Op.in]:
                        // [Sequelize.literal("(SELECT MIN(`clientpaymentId`) FROM `clientpayments` WHERE `payment_due_date` >= '" + todayDate + "' AND `payment_end_date` >= '" + todayDate + "' AND `status` = '1' GROUP BY `subscriptionsId`)")]
                        // [Sequelize.literal("(SELECT MIN(`clientpaymentId`) FROM `clientpayments` WHERE (`payment_due_date` <= '" + todayDate + "' AND `payment_end_date` >= '" + todayDate + "' AND `status` = '1' ) OR ( `payment_due_date` >= '" + todayDate + "' AND `payment_end_date` >= '" + todayDate + "' AND `status` = '1' ) AND `status` = '1' GROUP BY `subscriptionsId`)")]
                        // [Sequelize.literal("(SELECT MIN(`clientpaymentId`) FROM `clientpayments` INNER JOIN `subscriptions` AS `subscription` ON `clientpayments`.`subscriptionsId` = `subscription`.`subscriptionId` INNER JOIN `pricepacks` AS `subscription->pricepack` ON `subscription`.`pricepacks` = `subscription->pricepack`.`pricepackId` WHERE Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day ) <= '" + todayDate + "' OR ( ( `clientpayments`.`payment_due_date` <= '" + todayDate + "' AND `clientpayments`.`payment_end_date` >= '" + todayDate + "' ) OR ( `clientpayments`.`payment_due_date` >= '" + todayDate + "' AND `clientpayments`.`payment_end_date` >= '" + todayDate + "' ) AND `clientpayments`.`status` = '1' ) GROUP BY `clientpayments`.`subscriptionsId` ORDER BY `payment_end_date` ASC )")]
                            [Sequelize.literal("(SELECT MIN(`clientpaymentId`) as `clientpaymentId` FROM `clientpayments` INNER JOIN `subscriptions` AS `subscription` ON `clientpayments`.`subscriptionsId` = `subscription`.`subscriptionId` INNER JOIN `pricepacks` AS `subscription->pricepack` ON `subscription`.`pricepacks` = `subscription->pricepack`.`pricepackId` WHERE ( DATE( Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day )) >= '" + todayDate + "' ) AND ( ( `clientpayments`.`payment_due_date` <= '" + todayDate + "' AND `clientpayments`.`payment_end_date` >= '" + todayDate + "' ) OR ( `clientpayments`.`payment_due_date` >= '" + todayDate + "' AND `clientpayments`.`payment_end_date` >= '" + todayDate + "' )) AND `clientpayments`.`status` = '1' GROUP BY `clientpayments`.`subscriptionsId` UNION select c.clientpaymentId from clientpayments as c, ( SELECT a.clientpaymentId, a.curent_subcription_number, a.payment_end_date, COUNT(a.subscriptionsId) FROM clientpayments a LEFT JOIN clientpayments b ON a.subscriptionsId = b.subscriptionsId AND b.status = 1 WHERE b.status = 1 AND a.status = 1 GROUP BY a.subscriptionsId, a.clientpaymentId having a.curent_subcription_number = COUNT(a.subscriptionsId) AND a.payment_end_date < CURDATE() ) as total WHERE ( DATE( Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day )) >= '" + todayDate + "' ) AND c.clientpaymentId IN (total.clientpaymentId))")]
                    },
                    [dbclientScheduleMap.sequelize.Op.and]: [Sequelize.literal("( ( `subscription->clientschedulemap`.`clientScheduleMapId` IS NULL OR `subscription->clientschedulemap`.`sessionCount` IS NULL ) OR ( `subscription->clientschedulemap`.`sessionCount` < `subscription->pricepack`.`serviceDuration` ) )")],
                };
                let itemGroupConditions = {
                    status: 1,
                    businessId: businessIdinformation,
                    clientId: clients[i].clientId,
                };
                let clientpaymentsRiminderData = await dbclientpayments.clientpayments.findAndCount({
                    attributes: [
                        "clientpaymentId",
                        "subscriptionsId",
                        "due_amount",
                        "payble_amount",
                        "payment_due_date",
                        "payment_end_date", ["UpdateDateTime", "updated_at"],
                        "paymentStatus", [dbclientpayments.sequelize.literal("DATE(Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day ))"), 'subcription_end_Date']
                    ],
                    include: [{
                        model: dbsubscriptions.subscriptions,
                        attributes: [
                            "clientId",
                            "amountPaidSoFar",
                            "status_amountPaidSoFar",
                            "clientJoiningDate",
                            "subscriptionDateTime",
                            "installment_frequency",
                        ],
                        include: [{
                                model: dbClients.clients,
                                attributes: {
                                    include: [
                                        "clientName", [
                                            dbClients.clients.sequelize.literal(
                                                'CASE WHEN photoUrl  is NULL THEN CONCAT("' +
                                                KNACK_UPLOAD_URL.localUrl +
                                                '" , "noimage.jpg" ) ELSE CONCAT("' +
                                                KNACK_UPLOAD_URL.localUrl +
                                                '" , photoUrl ) END'
                                            ),
                                            "photoUrl"
                                        ],
                                    ],
                                    exclude: []
                                },
                                where: itemGroupConditions
                            },
                            {
                                model: dbpricepack.pricepacks,
                                attributes: ["pricepackName"],
                                where: {}
                            },
                            {
                                model: dbclientScheduleMap.clientschedulemap,
                                required: false,
                                attributes: ["sessionCount"],
                                where: {
                                    // [dbclientScheduleMap.sequelize.Op.and]: [Sequelize.literal("`subscription->clientschedulemap`.`scheduleId`= `subscription->clientschedulemap`.`parentScheduleId`")],
                                    [dbclientScheduleMap.sequelize.Op.and]: [Sequelize.literal("( `subscription->clientschedulemap`.`scheduleId` = `subscription->clientschedulemap`.`parentScheduleId` ) AND ( `subscription->clientschedulemap`.`scheduleId` = (select min(`scheduleId`) from `clientschedulemap` `inSched` where `inSched`.`subscriptionId` = `subscription`.`subscriptionId` ) )")],
                                }
                            }
                        ],
                        where: {}
                    }],
                    order: [
                        ["CreateDateTime", "DESC"]
                    ],
                    where: itemclientpaymentsRiminderData,
                    subQuery: false,
                    offset: 0,
                    limit: 1,
                    raw: true
                });
                var noOfSub = {
                    noOfSubscriptions: clientpaymentsRiminderData.count
                };
                var a = {
                    clientId: clients[i].clientId
                };
                var b = {
                    clientName: clients[i].clientName
                };
                var merged = [];
                merged.push(a);
                merged.push(b);
                merged.push(noOfSub);

                var mergedClient = {};
                mergedClient = Object.assign.apply(Object, merged);
                mergedAllClients = mergedAllClients.concat(mergedClient);
            }

            let newmergedAllClients = [];
            // Client Notes

            let notes = await notedb.notes.findAll({
                order: [
                    ["updateDateTime", "desc"]
                ],
                where: {
                    clientId: clientId,
                    status: 1,
                    reminder_date: {
                        [notedb.sequelize.Op.gt]: new Date()
                    }
                },
                order: [
                    ['reminder_date', 'ASC'],
                    ['reminder_time', 'ASC']
                ],

                raw: true
            });
            notes.forEach(function(note, index) {
                allNotes.push(note);
            })

            let pastNotes = await notedb.notes.findAll({
                order: [
                    ["updateDateTime", "desc"]
                ],
                where: {
                    clientId: clientId,
                    status: 1,
                    reminder_date: {
                        [notedb.sequelize.Op.lt]: new Date()
                    }
                },
                order: [
                    ['reminder_date', 'DESC'],
                    ['reminder_time', 'DESC']
                ],
                raw: true
            });
            pastNotes.forEach(function(note, index) {
                allNotes.push(note);
            })

            dbSubs.sequelize.close();
            notedb.sequelize.close();
            dbClients.sequelize.close();
            dbclientpayments.sequelize.close();
            dbsubscriptions.sequelize.close();
            dbpricepack.sequelize.close();
            dbclientexpense.sequelize.close();
            dbpaymentTransactions.sequelize.close();
            dbclientScheduleMap.sequelize.close();

            return await [{
                'ongoingSubscription': mergedAllClients[0].noOfSubscriptions
            }, {
                'pendingAmount': ''
            }, {
                'clientInfo': clients
            }, {
                'recentNotes': allNotes
            }];

        } catch (error) {
            throw error;
        }
    };

    //Update specific clients's information
    update = async(
        information,
        clientId,
        clientName,
        contactNumber,
        emailId,
        alternateEmail,
        area,
        pin,
        city,
        dateOfBirth,
        photoUrl,
        alternateNumber
    ) => {
        try {
            let db = createInstance(["client"], information);

            if (photoUrl) {
                // if (!req.files) {
                //   return res.status(400).send('No files were uploaded.');
                // }

                // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
                // let sampleFile = req.files.client_profile;
                var filename = photoUrl.name;

                // Use the mv() method to place the file somewhere on your server
                await photoUrl.mv("./uploads/clients/" + filename, function(err) {
                    // if (err) {
                    //   return res.status(500).send(err);
                    // }
                    // res.send('File uploaded!');
                });
                var photoUrl = filename;
            } else {
                var photoUrl = null;
            }

            let dob;
            if (dateOfBirth) {
                dob = (dateOfBirth == '') ? null : await formatDate(dateOfBirth);
            } else {
                dob = null;
            }

            let clients = await db.clients.update({
                clientName: clientName,
                contactNumber: contactNumber,
                emailId: emailId,
                alternateEmail: alternateEmail,
                dateOfBirth: dob,
                area: area,
                pin: pin,
                city: city,
                photoUrl: photoUrl,
                alternateNumber: alternateNumber,
                updateDateTime: Date.now()
            }, {
                where: {
                    clientId: clientId
                }
            });
            db.sequelize.close();
            return await clients;
        } catch (error) {
            throw error;
        }
    };

    //Removing client's detail, i.e. setting status=0, multiple clients can be deleted by passing multiple ids using ,(comma) separator
    remove = async(information, clientId) => {
        try {
            let db = createInstance(["client"], information);
            let clients = await db.clients.update({
                status: 0,
                updateDateTime: Date.now()
            }, {
                where: {
                    clientId: clientId
                }
            });
            db.sequelize.close();
            return await clients;
        } catch (error) {
            throw error;
        }
    };

    //search all the clients based on clients' names under the passed business ID
    searchClient = async(
        information,
        businessId,
        clientName,
        filterPrivateFields = true
    ) => {
        try {
            const Op = Sequelize.Op;
            let db = createInstance(["client"], information);

            var filterData = [];
            var arr = [];
            var c = 0;
            var count = 0;
            for (var i = 65; i <= 90; i++) {
                arr[c] = String.fromCharCode(i).toLowerCase();
                let filterClients = await db.clients.findAll({
                    order: [
                        ["clientName", "asc"]
                    ],
                    where: {
                        businessId: businessId,
                        status: 1,
                        [Op.and]: [{
                            clientName: {
                                like: [arr[c] + "%"]
                            }
                        }, {
                            clientName: {
                                like: ["%" + clientName + "%"]
                            }
                        }]
                    },
                    raw: true
                });

                if (filterClients.length) { //this condition hides the empty arrays of names
                    var merged = {};
                    var m = [];
                    var con = [];
                    for (var k = 0; k < filterClients.length; k++) {
                        var arrnew = [];
                        Array.prototype.push.apply(arrnew, [filterClients[k]]);
                        Array.prototype.push.apply(arrnew, [{
                            count: count++
                        }]);
                        m = Object.assign.apply(Object, arrnew);
                        con = con.concat(m);
                    }
                    merged = Object.assign.apply(Object, [{
                        title: arr[c].toUpperCase(),
                        data: con
                    }]);
                    filterData = filterData.concat(merged);
                }
                c++;
            }

            db.sequelize.close();
            return await filterData;
            // if (filterPrivateFields) {
            //   return await filterFields(clients, PUBLIC_FIELDS);
            // }
        } catch (error) {
            throw error;
        }
    };

    //Listing all the clients under the passed business and Center IDs
    listingClients = async(
        information,
        businessId,
        filterPrivateFields = true
    ) => {
        try {
            //counting total and active clients
            let db = createInstance(["client"], information);
            let total_clients = await db.clients.findAll({
                where: {
                    businessId: businessId
                        //status: 1
                },
                raw: true
            });

            let active_clients = await db.clients.findAll({
                where: {
                    businessId: businessId,
                    status: 1
                },
                raw: true
            });

            var totalClients = total_clients.length;
            var activeClients = active_clients.length;
            var clients = {
                totalClients: totalClients,
                activeClients: activeClients
            };
            //end of counting total and active clients

            var days = 5;
            var today = new Date();
            var pastDate = new Date(today.getTime() - days * 24 * 60 * 60 * 1000);
            let recent_clients = await db.clients.findAll({
                where: {
                    businessId: businessId,
                    status: 1,
                    createDateTime: {
                        // between: [startDate, endDate]
                        between: [pastDate, today]
                    }
                },
                raw: true
            });
            //var recentClients = { recentClients: recent_clients };

            var sequelize = require("sequelize");
            let dbSub = createInstance(["clientSubscription"], information);
            if (total_clients[0]) {
                var seqdb = new Sequelize(businessId, KNACKDB.user, KNACKDB.password, {
                    host: KNACKDB.host,
                    port: 3306,
                    dialect: "mysql",
                    pool: {
                        max: 100,
                        min: 0,
                        idle: 10000
                    }
                });
                var top_three = await seqdb.query(
                    "select clientId, count(subscriptionId) as subcount from subscriptions group by clientId  order by subcount desc limit 3", {
                        type: Sequelize.QueryTypes.SELECT //this code will remove the duplicate data (meta data)
                    }
                );
            }

            var mergedTopClients = [];

            var mergedTopClients1 = [];
            //let dbSubs = createInstance(["clientSubscription"], information);
            for (var i = 0; i < top_three.length; i++) {
                let top_clients = await db.clients.findAll({
                    where: {
                        clientId: top_three[i].clientId,
                        status: 1
                    },
                    raw: true
                });
                mergedTopClients = mergedTopClients.concat(top_clients);
            }

            for (var i = 0; i < mergedTopClients.length; i++) {
                let subscriptions = await dbSub.subscriptions.findAll({
                    where: {
                        status: 1,
                        clientId: mergedTopClients[i].clientId
                    },
                    raw: true
                });

                var noOfSub = {
                    noOfSubscriptions: subscriptions.length
                };
                var merged = [];
                merged.push(mergedTopClients[i]);
                merged.push(noOfSub);

                var mergedClient = {};
                mergedClient = Object.assign.apply(Object, merged);
                mergedTopClients1 = mergedTopClients1.concat(mergedClient);
            }

            // console.log()
            //console.log(mergedTopClients);

            /**********************************upcoming client renewals (later) */

            // var days = 5;
            // var today = new Date();
            // var futureDate = new Date(today.getTime() + (days * 24 * 60 * 60 * 1000));
            let client_payment_renewal = await db.clients.findAll({
                where: {
                    businessId: businessId,
                    status: 1
                        // createDateTime: {
                        //   between: [today, futureDate]
                        // }
                },
                raw: true
            });
            // console.log(client_payment_renewal);
            let dbSubs = createInstance(["clientSubscription"], information);
            for (var i = 0; i < client_payment_renewal.length; i++) {
                let subscriptions = await dbSubs.subscriptions.findAll({
                    where: {
                        status: 1,
                        clientId: client_payment_renewal[i].clientId
                    },
                    raw: true
                });

                // console.log(subscriptions);
                let clientTotalPaymentdb = createInstance(
                    ["clientPayments"],
                    information
                );
                for (var j = 0; j < subscriptions.length; j++) {
                    //console.log(subscriptions[j].subscriptionId);
                    let clientTotalPayment = await clientTotalPaymentdb.clientpayments.findAll({
                        where: {
                            status: 1,
                            subscriptionsId: subscriptions[j].subscriptionId
                        },
                        raw: true
                    });
                }
            }

            /************************************************************* */

            var final = {
                clients: clients,
                recentClients: recent_clients,
                topClients: mergedTopClients1
            };

            db.sequelize.close();
            return await final;
        } catch (error) {
            throw error;
        }
    };

    //Listing all the clients under the passed business and Center IDs
    clientNoOfSubscription = async(
        information,
        businessId,
        status,
        filterPrivateFields = true
    ) => {
        try {
            if (status == 1) {
                let db = createInstance(["client"], information);
                var clients = await db.clients.findAll({
                    where: {
                        businessId: businessId,
                        status: 1
                    },
                    raw: true
                });
            } else if (status == 2) {
                let db = createInstance(["client"], information);
                var clients = await db.clients.findAll({
                    where: {
                        businessId: businessId,
                        status: 0
                    },
                    raw: true
                });
            } else {
                throw new ApplicationError(
                    "Wrong Value ! 1 for Active Clients. 2 for Inactive Clients.",
                    401
                );
            }

            var mergedAllClients = [];
            let dbSubs = createInstance(["clientSubscription"], information);
            for (var i = 0; i < clients.length; i++) {
                let subscriptions = await dbSubs.subscriptions.findAll({
                    where: {
                        status: 1,
                        clientId: clients[i].clientId
                    },
                    raw: true
                });

                var noOfSub = {
                    noOfSubscriptions: subscriptions.length
                };
                var a = {
                    clientId: clients[i].clientId
                };
                var b = {
                    clientName: clients[i].clientName
                };
                var merged = [];
                merged.push(a);
                merged.push(b);
                merged.push(noOfSub);

                var mergedClient = {};
                mergedClient = Object.assign.apply(Object, merged);
                mergedAllClients = mergedAllClients.concat(mergedClient);
            }

            //db.sequelize.close();
            return await mergedAllClients;
        } catch (error) {
            throw error;
        }
    };

    //Listing all the clients under the passed business and Center IDs
    clientDetailsDashboard = async(
        information,
        businessId,
        clientId,
        filterPrivateFields = true
    ) => {
        try {
            let db = createInstance(["client"], information);
            let clients = await db.clients.findAll({
                where: {
                    businessId: businessId,
                    clientId: clientId,
                    status: 1
                },
                raw: true
            });

            var mergedAllClients = [];
            let dbSubs = createInstance(["clientSubscription"], information);
            let subscriptions = await dbSubs.subscriptions.findAll({
                where: {
                    status: 1,
                    clientId: clients[0].clientId
                },
                raw: true
            });

            var totalAmount = 0;
            for (var i = 0; i < subscriptions.length; i++) {
                let dbpp = createInstance(["clientPricepack"], information);
                let pricepacks = await dbpp.pricepacks.findAll({
                    where: {
                        status: 1,
                        pricepackId: subscriptions[i].pricepacks
                    },
                    raw: true
                });
                totalAmount += pricepacks[0].amount;
            }

            var totalPayment = 0;
            let clientTotalPaymentdb = createInstance(
                ["clientPayments"],
                information
            );
            for (var i = 0; i < subscriptions.length; i++) {
                let clientTotalPayment = await clientTotalPaymentdb.clientpayments.findAll({
                    where: {
                        status: 1,
                        subscriptionsId: subscriptions[i].subscriptionId
                    },
                    raw: true
                });
                totalPayment += clientTotalPayment[0].totalAmountPaid;
            }

            var pr = [];
            var ph = [];
            var days = 5;
            var today = new Date();
            var pastDate = new Date(today.getTime() - days * 24 * 60 * 60 * 1000);
            for (var i = 0; i < subscriptions.length; i++) {
                let paymentHistory = await clientTotalPaymentdb.clientpayments.findAll({
                    where: {
                        status: 1,
                        subscriptionsId: subscriptions[i].subscriptionId,
                        payment_date: {
                            between: [pastDate, today]
                        }
                    },
                    raw: true
                });
                ph = ph.concat(paymentHistory);
            }
            var pending = totalAmount - totalPayment;
            var pendingAmount = {
                pendingAmount: pending
            };

            var noOfSub = {
                ongoingSubscriptions: subscriptions.length
            };
            // var a = { clientId: clients[0].clientId };
            // var b = { clientName: clients[0].clientName };
            var merged = [];
            // merged.push(a);
            // merged.push(b);
            merged.push(noOfSub);
            merged.push(pendingAmount);

            var mergedClient = {};
            mergedClient = Object.assign.apply(Object, merged);
            mergedAllClients = mergedAllClients.concat({
                clients: clients
            });
            mergedAllClients = mergedAllClients.concat(mergedClient);
            mergedAllClients = mergedAllClients.concat({
                recentActivities: [{
                    paymentReceived: ph,
                    paymentReminder: pr
                }]
            });

            db.sequelize.close();
            return await mergedAllClients;
        } catch (error) {
            throw error;
        }
    };

    //display multiple subscription with schedule details and attendance details of a client. (show subscriptions only if the client is scheduled with the subscription)
    clientRecordAttendance = async(
        information,
        businessId,
        clientId,
        filterPrivateFields = true
    ) => {
        try {
            let db = createInstance(["clientSubscription"], information);
            let subscriptions = await db.subscriptions.findAll({
                where: {
                    status: 1,
                    businessId: businessId,
                    clientId: clientId
                },
                raw: true
            });

            var mergedAll = [];
            let dbClientSchedule = createInstance(["clientScheduleMap"], information);
            let dbPricepack = createInstance(["clientPricepack"], information);
            for (var i = 0; i < subscriptions.length; i++) {
                let pricepacks = await dbPricepack.pricepacks.findAll({
                    where: {
                        status: 1,
                        pricepackId: subscriptions[i].pricepacks
                    },
                    raw: true
                });

                let clientschedule = await dbClientSchedule.clientschedulemap.findAll({
                    where: {
                        status: 1,
                        subscriptionId: subscriptions[i].subscriptionId
                    },
                    raw: true
                });
                if (clientschedule[0]) {
                    //var attendanceKeys = [];
                    var attendanceValues = [];
                    // console.log(clientschedule[0].attendanceType);
                    Object.keys(clientschedule[0].attendanceType).forEach(function(key) {
                        //attendanceKeys.push(key);
                        attendanceValues.push(clientschedule[0].attendanceType[key]);
                        // var value = attendance[0].attendanceType[key];
                        // console.log(key + ':' + value);
                    });
                    // console.log(attendanceValues);
                    // console.log(attendanceValues.length);
                    var countTotal = attendanceValues.length;
                    var countPresent = 0,
                        countAbsent = 0,
                        countExcused = 0,
                        countUnmarked = 0;
                    for (var j = 0; j < countTotal; j++) {
                        if (attendanceValues[j] == 1) countPresent++;
                        else if (attendanceValues[j] == 2) countAbsent++;
                        else if (attendanceValues[j] == 3) countExcused++;
                        else if (attendanceValues[j] == 4) countUnmarked++;
                    }
                    var perPresent = 100 / countTotal * countPresent;
                    var perAbsent = 100 / countTotal * countAbsent;
                    var perExcused = 100 / countTotal * countExcused;
                    var perUnmarked = 100 / countTotal * countUnmarked;

                    var attendance = {
                        present: perPresent,
                        absent: perAbsent,
                        excused: perExcused,
                        unmarked: perUnmarked
                    };

                    let dbSchedule = createInstance(["clientSchedule"], information);
                    let schedule = await dbSchedule.schedule.findAll({
                        where: {
                            scheduleId: clientschedule[0].scheduleId,
                            status: 1
                        },
                        raw: true
                    });

                    let db = createInstance(["clientBatches"], information);
                    let batch = await db.batches.findAll({
                        where: {
                            status: 1,
                            batchId: schedule[0].batchId
                        },
                        raw: true
                    });
                    // console.log(schedule);
                    // console.log(schedule[0].scheduleDate);
                    // console.log(schedule[0].startTime);
                    // console.log(schedule[0].endTime);
                    var sub = {
                        subscriptionId: subscriptions[i].subscriptionId
                    };
                    var price = {
                        pricepackId: pricepacks[0].pricepackId,
                        pricepackName: pricepacks[0].pricepackName
                    };
                    var batchDetails = {
                        batchName: batch[0].batchName
                    };
                    var scheduleData = {
                        scheduleName: schedule[0].scheduleName,
                        scheduleDate: schedule[0].scheduleDate,
                        startTime: schedule[0].startTime,
                        endTime: schedule[0].endTime
                    };

                    var merged = [];
                    merged.push(sub);
                    merged.push(price);
                    merged.push(batchDetails);
                    merged.push(scheduleData);
                    merged.push({
                        attendance: attendance
                    });
                    var mergedSub = {};
                    mergedSub = Object.assign.apply(Object, merged);
                    mergedAll = mergedAll.concat(mergedSub);
                }
            }

            db.sequelize.close();
            return await mergedAll;
        } catch (error) {
            throw error;
        }
    };

    //search all the clients based on clients' names under the passed business ID
    // topClients = async(
    //     information,
    //     params,
    //     filterPrivateFields = true
    // ) => {
    //     try {
    //         let businessIdinformation = information.split(",")[1];
    //         let dbsubscriptions = createInstance(["clientSubscription"], information);
    //         let dbclientpayments = createInstance(["clientPayments"], information);
    //         let dbClients = createInstance(["client"], information);
    //         let dbpricepack = createInstance(["clientPricepack"], information);
    //         dbclientpayments.clientpayments.belongsTo(dbsubscriptions.subscriptions, {
    //             foreignKey: "subscriptionsId",
    //             targetKey: "subscriptionId"
    //         });
    //         dbClients.clients.belongsTo(dbsubscriptions.subscriptions, {
    //             foreignKey: "clientId",
    //             targetKey: "clientId"
    //         });
    //         dbsubscriptions.subscriptions.belongsTo(dbpricepack.pricepacks, {
    //             foreignKey: "pricepacks",
    //             targetKey: "pricepackId"
    //         });
    //         let orderBy;
    //         let filter = params[0] ? params[0] : "";
    //         var pageNo = params["page"] ? params["page"] : 1;
    //         var limitPerPage = PEGINATION_LIMIT;
    //         var offsetLimit = (pageNo - 1) * limitPerPage;
    //         if (params["order"] == 1) {
    //             orderBy = "ORDER BY `total_paid_amount` DESC";
    //         } else {
    //             orderBy = "ORDER BY `total_paid_amount` ASC";
    //         }

    //         // let whereCondition = {
    //         //     [dbClients.clients.sequelize.literal('(SELECT SUM(`payble_amount`) FROM `clientpayments` WHERE `subscriptionsId` = `subscription`.`subscriptionId`)')] 
    //         // };

    //         console.log("===================================================================");
    //         let topClientList = await dbClients.clients.findAndCount({
    //             attributes: {
    //                 include: [
    //                     [dbClients.clients.sequelize.literal('CASE WHEN photoUrl  is NULL THEN CONCAT("' + KNACK_UPLOAD_URL.localUrl + '" , "noimage.jpg" ) ELSE CONCAT("' + KNACK_UPLOAD_URL.localUrl + '" , photoUrl ) END'), 'photoFullUrl'],
    //                     [dbClients.clients.sequelize.literal('( SELECT SUM(`payble_amount`) FROM `clientpayments` WHERE `subscriptionsId` = `subscription`.`subscriptionId` )'), 'total_paid_amount']
    //                 ],
    //                 exclude: []
    //             },
    //             include: [{
    //                 model: dbsubscriptions.subscriptions,
    //                 attributes: [],
    //                 required: true,
    //                 include: [{
    //                     model: dbpricepack.pricepacks,
    //                     attributes: [],
    //                     required: true
    //                 }]
    //             }],
    //             // where: whereCondition,
    //             order: [dbClients.clients.sequelize.literal('(SELECT SUM(`payble_amount`) FROM `clientpayments` WHERE `subscriptionsId` = `subscription`.`subscriptionId`) DESC')],
    //             limit: limitPerPage,
    //             offset: offsetLimit,
    //             raw: true
    //         });
    //         console.log("===================================================================");
    //         dbclientpayments.sequelize.close();
    //         dbsubscriptions.sequelize.close();
    //         dbClients.sequelize.close();
    //         dbpricepack.sequelize.close();
    //         return topClientList;


    //     } catch (error) {
    //         throw error;
    //     }
    // };
    topClients = async(
        information,
        params,
        filterPrivateFields = true
    ) => {
        try {
            let businessIdinformation = information.split(",")[1];
            let dbclientpayments = createInstance(["clientPayments"], information);
            let dbsubscriptions = createInstance(["clientSubscription"], information);
            let dbClients = createInstance(["client"], information);
            let dbpricepacks = createInstance(["clientPricepack"], information);

            dbclientpayments.clientpayments.belongsTo(dbsubscriptions.subscriptions, {
                foreignKey: "subscriptionsId",
                targetKey: "subscriptionId"
            });
            dbsubscriptions.subscriptions.belongsTo(dbClients.clients, {
                foreignKey: "clientId",
                targetKey: "clientId"
            });
            dbClients.clients.belongsTo(dbsubscriptions.subscriptions, {
                foreignKey: "clientId",
                targetKey: "clientId"
            });
            dbsubscriptions.subscriptions.belongsTo(dbpricepacks.pricepacks, {
                foreignKey: "pricepacks",
                targetKey: "pricepackId"
            });

            let todayFormat = await date_ISO_Convert(new Date());
            // let todayFormat = "2018-08-13";
            let orderBy = " ORDER BY `total_paid_amount` DESC";
            let limitPerPage = PEGINATION_LIMIT;
            let offsetLimit = 0;
            let activeClientListCount = await dbsubscriptions.sequelize.query(
                "SELECT `clients`.*, CASE WHEN photoUrl is NULL THEN CONCAT('" + KNACK_UPLOAD_URL.localUrl + "' , 'noimage.jpg' ) ELSE CONCAT('" + KNACK_UPLOAD_URL.localUrl + "' , photoUrl ) END AS `photoFullUrl`, `subscriptions`.`subscriptionId` , (SELECT SUM(`payble_amount`)  FROM `clientpayments` WHERE `subscriptionsId` = `subscriptions`.`subscriptionId`) as `total_paid_amount`, (SELECT (SUM(`due_amount`) - SUM(`payble_amount`))  FROM `clientpayments` WHERE `subscriptionsId` = `subscriptions`.`subscriptionId`) as `total_pending_amount`  FROM `clients` LEFT JOIN `subscriptions` AS `subscriptions` ON `subscriptions`.`clientid` = `clients`.`clientid` INNER JOIN `pricepacks` AS `pricepack` ON `subscriptions`.`pricepacks` = `pricepack`.`pricepackid` WHERE `clients`.`status` = 1 AND `clients`.`businessId` = '" + businessIdinformation + "' AND '" + todayFormat + "' BETWEEN `subscriptiondatetime` AND Date_add( `subscriptions`.`subscriptiondatetime`,INTERVAL `pricepack`.`pricepackvalidity` day) HAVING `total_paid_amount` IS NOT NULL AND `total_pending_amount` IS NOT NULL ", {
                    type: dbsubscriptions.sequelize.QueryTypes.SELECT
                }
            );
            let activeClientList = await dbsubscriptions.sequelize.query(
                "SELECT `clients`.*, CASE WHEN photoUrl is NULL THEN CONCAT('" + KNACK_UPLOAD_URL.localUrl + "' , 'noimage.jpg' ) ELSE CONCAT('" + KNACK_UPLOAD_URL.localUrl + "' , photoUrl ) END AS `photoFullUrl`, `subscriptions`.`subscriptionId` , (SELECT SUM(`payble_amount`)  FROM `clientpayments` WHERE `subscriptionsId` = `subscriptions`.`subscriptionId`) as `total_paid_amount`, (SELECT (SUM(`due_amount`) - SUM(`payble_amount`))  FROM `clientpayments` WHERE `subscriptionsId` = `subscriptions`.`subscriptionId`) as `total_pending_amount`  FROM `clients` LEFT JOIN `subscriptions` AS `subscriptions` ON `subscriptions`.`clientid` = `clients`.`clientid` INNER JOIN `pricepacks` AS `pricepack` ON `subscriptions`.`pricepacks` = `pricepack`.`pricepackid` WHERE `clients`.`status` = 1 AND `clients`.`businessId` = '" + businessIdinformation + "' AND '" + todayFormat + "' BETWEEN `subscriptiondatetime` AND Date_add( `subscriptions`.`subscriptiondatetime`,INTERVAL `pricepack`.`pricepackvalidity` day) HAVING `total_paid_amount` IS NOT NULL AND `total_pending_amount` IS NOT NULL " + orderBy + " LIMIT " +
                limitPerPage +
                " OFFSET " +
                offsetLimit, {
                    type: dbsubscriptions.sequelize.QueryTypes.SELECT
                }
            );
            dbclientpayments.sequelize.close();
            dbsubscriptions.sequelize.close();
            dbClients.sequelize.close();
            dbpricepacks.sequelize.close();
            return {
                "count": activeClientListCount.length,
                "rows": activeClientList
            };
        } catch (error) {
            throw error;
        }
    };

    //Listing active_clients(1), inactive_clients(2) and top_lients(3) under the passed businessId
    showClients = async(
        information,
        businessId,
        option,
        filterPrivateFields = true
    ) => {
        try {
            let dbClients = createInstance(["client"], information);
            let active_clients = await dbClients.clients.findAll({
                where: {
                    businessId: businessId,
                    status: 1
                },
                raw: true
            });

            //active clients
            var mergedAllActiveClients = [];
            let dbSubActiveClients = createInstance(
                ["clientSubscription"],
                information
            );
            for (var i = 0; i < active_clients.length; i++) {
                let subscriptionsActiveClients = await dbSubActiveClients.subscriptions.findAll({
                    where: {
                        status: 1,
                        businessId: businessId,
                        clientId: active_clients[i].clientId
                    },
                    raw: true
                });
                var clientphoto =
                    KNACK_UPLOAD_URL.localUrl + active_clients[i].photoUrl;
                var clientsData = Array();

                clientsData[i] = {
                    clientId: active_clients[i].clientId,
                    businessId: active_clients[i].businessId,
                    centerId: active_clients[i].centerId,
                    clientName: active_clients[i].clientName,
                    contactNumber: active_clients[i].contactNumber,
                    alternateNumber: active_clients[i].alternateNumber,
                    emailId: active_clients[i].emailId,
                    dateOfBirth: active_clients[i].dateOfBirth,
                    area: active_clients[i].area,
                    pin: active_clients[i].pin,
                    city: active_clients[i].city,
                    createDateTime: active_clients[i].createDateTime,
                    status: active_clients[i].status,
                    photoUrl: clientphoto
                };

                var lenActiveSub = subscriptionsActiveClients.length;
                var merged = [];
                //merged.push(active_clients[i]);
                merged.push(clientsData[i]);
                merged.push({
                    noOfSubscriptions: lenActiveSub
                });
                var mergedSub = {};
                mergedSub = Object.assign.apply(Object, merged);
                mergedAllActiveClients = mergedAllActiveClients.concat(mergedSub);
            }
            //active clients end

            //inactive clients
            let inactive_clients = await dbClients.clients.findAll({
                where: {
                    businessId: businessId,
                    status: 0
                },
                raw: true
            });

            var mergedAllInactiveClients = [];
            let dbSubInactiveClients = createInstance(
                ["clientSubscription"],
                information
            );
            for (var i = 0; i < inactive_clients.length; i++) {
                let subscriptionsInactiveClients = await dbSubInactiveClients.subscriptions.findAll({
                    where: {
                        status: 1,
                        businessId: businessId,
                        clientId: inactive_clients[i].clientId
                    },
                    raw: true
                });

                var clientphoto =
                    KNACK_UPLOAD_URL.localUrl + active_clients[i].photoUrl;
                var clientsData = Array();
                clientsData[i] = {
                    clientId: inactive_clients[i].clientId,
                    businessId: inactive_clients[i].businessId,
                    centerId: inactive_clients[i].centerId,
                    clientName: inactive_clients[i].clientName,
                    contactNumber: inactive_clients[i].contactNumber,
                    alternateNumber: inactive_clients[i].alternateNumber,
                    emailId: inactive_clients[i].emailId,
                    dateOfBirth: inactive_clients[i].dateOfBirth,
                    area: inactive_clients[i].area,
                    pin: inactive_clients[i].pin,
                    city: inactive_clients[i].city,
                    createDateTime: inactive_clients[i].createDateTime,
                    status: inactive_clients[i].status,
                    photoUrl: clientphoto
                };

                var lenInactiveSub = subscriptionsInactiveClients.length;
                var merged = [];
                merged.push(inactive_clients[i]);
                merged.push({
                    noOfSubscriptions: lenInactiveSub
                });
                var mergedSub = {};
                mergedSub = Object.assign.apply(Object, merged);
                mergedAllInactiveClients = mergedAllInactiveClients.concat(mergedSub);
            }
            //inactive clients end

            //top clients
            var sequelize = require("sequelize");
            var mergedAllTopClients = [];
            let dbSub = createInstance(["clientSubscription"], information);
            if (active_clients[0]) {
                var seqdb = new Sequelize(businessId, "root", "root", {
                    host: KNACKDB.host,
                    port: 3306,
                    dialect: "mysql",
                    pool: {
                        max: 100,
                        min: 0,
                        idle: 10000
                    }
                });
                var top_clients = await seqdb.query(
                    "select clientId, count(subscriptionId) as subscriptions from subscriptions group by clientId  order by subscriptions desc", {
                        type: Sequelize.QueryTypes.SELECT //this code will remove the duplicate data (meta data)
                    }
                );
                for (var i = 0; i < top_clients.length; i++) {
                    let top_clients_list = await dbClients.clients.findAll({
                        where: {
                            businessId: businessId,
                            clientid: top_clients[i].clientId,
                            status: 1
                        },
                        raw: true
                    });
                    var clientphoto =
                        KNACK_UPLOAD_URL.localUrl + active_clients[i].photoUrl;
                    var clientsData = Array();
                    clientsData[i] = {
                        clientId: top_clients_list[0].clientId,
                        businessId: top_clients_list[0].businessId,
                        centerId: top_clients_list[0].centerId,
                        clientName: top_clients_list[0].clientName,
                        contactNumber: top_clients_list[0].contactNumber,
                        alternateNumber: top_clients_list[0].alternateNumber,
                        emailId: top_clients_list[0].emailId,
                        dateOfBirth: top_clients_list[0].dateOfBirth,
                        area: top_clients_list[0].area,
                        pin: top_clients_list[0].pin,
                        city: top_clients_list[0].city,
                        createDateTime: top_clients_list[0].createDateTime,
                        status: top_clients_list[0].status,
                        photoUrl: clientphoto
                    };

                    var lenTopSub = top_clients.length;
                    var merged = [];
                    //merged.push(active_clients[i]);
                    merged.push(clientsData[i]);
                    merged.push({
                        noOfSubscriptions: top_clients[i].subscriptions
                    });
                    var mergedSub = {};
                    mergedSub = Object.assign.apply(Object, merged);
                    mergedAllTopClients = mergedAllTopClients.concat(mergedSub);
                }
            }
            //top clients end

            if (option == 1) {
                return await mergedAllActiveClients;
            } else if (option == 2) {
                return await mergedAllInactiveClients;
            } else if (option == 3) {
                return await mergedAllTopClients;
            } else {
                throw new ApplicationError(
                    "Wrong Value. 1 for active clients. 2 for inactive clients. 3 for top clients.",
                    401
                );
            }
        } catch (error) {
            throw error;
        }
    };




    // activeClientListRoute = async(
    //     information,
    //     params,
    //     filterPrivateFields = true
    // ) => {
    //     try {
    //         // let todayFormat = await date_ISO_Convert(new Date());
    //         // // let todayFormat = "2018-08-13";
    //         // let orderBy;
    //         // let businessIdinformation = information.split(",")[1];
    //         // let dbsubscriptions = createInstance(["clientSubscription"], information);
    //         // let filter = params[0] ? params[0] : "";
    //         // var pageNo = params["page"] ? params["page"] : 1;
    //         // var limitPerPage = PEGINATION_LIMIT;
    //         // var offsetLimit = (pageNo - 1) * limitPerPage;
    //         // if (params["order"] == 1) {
    //         //     orderBy = "ORDER BY `clients`.`clientname` ASC";
    //         // } else if (params["order"] == 2) {
    //         //     orderBy = "ORDER BY `clients`.`createDateTime` DESC";
    //         // } else {
    //         //     orderBy = "ORDER BY `clients`.`createDateTime` ASC";
    //         // }
    //         // let activeClientListCount = await dbsubscriptions.sequelize.query(
    //         //     "SELECT COUNT(*) AS count FROM `clients` WHERE `clientid` IN ( SELECT DISTINCT(`clientid`) FROM `subscriptions` INNER JOIN `pricepacks` AS `pricepack` ON `subscriptions`.`pricepacks` = `pricepack`.`pricepackid` WHERE '" + todayFormat + "' BETWEEN `subscriptiondatetime` AND Date_add( `subscriptions`.`subscriptiondatetime`,INTERVAL `pricepack`.`pricepackvalidity` day)) AND `clients`.`status` = 1 AND `clients`.`businessId` = '" + businessIdinformation + "' AND `clients`.`clientName` LIKE '%" + filter + "%' " + orderBy, {
    //         //         type: dbsubscriptions.sequelize.QueryTypes.SELECT
    //         //     }
    //         // );
    //         // let activeClientList = await dbsubscriptions.sequelize.query(
    //         //     "SELECT *,CASE WHEN photoUrl  is NULL THEN CONCAT('" + KNACK_UPLOAD_URL.localUrl + "' , 'noimage.jpg' ) ELSE CONCAT('" + KNACK_UPLOAD_URL.localUrl + "' , photoUrl ) END AS `photoFullUrl` FROM `clients` WHERE `clientid` IN ( SELECT DISTINCT(`clientid`) FROM `subscriptions` INNER JOIN `pricepacks` AS `pricepack` ON `subscriptions`.`pricepacks` = `pricepack`.`pricepackid` WHERE '" + todayFormat + "' BETWEEN `subscriptiondatetime` AND Date_add( `subscriptions`.`subscriptiondatetime`,INTERVAL `pricepack`.`pricepackvalidity` day)) AND `clients`.`status` = 1 AND `clients`.`businessId` = '" + businessIdinformation + "' AND `clients`.`clientName` LIKE '%" + filter + "%' " + orderBy + " LIMIT " +
    //         //     limitPerPage +
    //         //     " OFFSET " +
    //         //     offsetLimit, {
    //         //         type: dbsubscriptions.sequelize.QueryTypes.SELECT
    //         //     }
    //         // );
    //         // dbsubscriptions.sequelize.close();
    //         // return {
    //         //     "count": activeClientListCount[0].count,
    //         //     "rows": activeClientList
    //         // };

    //         var mergedAllActiveClients = [];
    //         let todayDate = await hyphenDateFormat(new Date);
    //         // let todayDate = '2018-07-04';
    //         var businessIdinformation = information.split(",")[1];
    //         let dbclientpayments = createInstance(["clientPayments"], information);
    //         let dbsubscriptions = createInstance(["clientSubscription"], information);
    //         let dbClients = createInstance(["client"], information);
    //         let dbpricepack = createInstance(["clientPricepack"], information);
    //         let dbclientexpense = createInstance(["clientExpense"], information);
    //         let dbpaymentTransactions = createInstance(["paymentTransactions"], information);
    //         let dbclientScheduleMap = createInstance(["clientScheduleMap"], information);
    //         dbclientpayments.clientpayments.belongsTo(dbsubscriptions.subscriptions, {
    //             foreignKey: "subscriptionsId",
    //             targetKey: "subscriptionId"
    //         });
    //         dbsubscriptions.subscriptions.belongsTo(dbClients.clients, {
    //             foreignKey: "clientId",
    //             targetKey: "clientId"
    //         });
    //         dbsubscriptions.subscriptions.belongsTo(dbpricepack.pricepacks, {
    //             foreignKey: "pricepacks",
    //             targetKey: "pricepackId"
    //         });
    //         dbclientpayments.clientpayments.belongsTo(dbpaymentTransactions.paymentTransactions, {
    //             foreignKey: "clientpaymentId",
    //             targetKey: "clientpaymentId"
    //         });
    //         dbsubscriptions.subscriptions.belongsTo(dbclientScheduleMap.clientschedulemap, {
    //             foreignKey: "subscriptionId",
    //             targetKey: "subscriptionId"
    //         });

    //         let filter = params[0] ? params[0] : "";
    //         var pageNo = params["page"] ? params["page"] : 1;
    //         var limitPerPage = PEGINATION_LIMIT;
    //         var offsetLimit = (pageNo - 1) * limitPerPage;

    //         let clientpaymentsRiminder = {};
    //         let recentIncomeclientpayments = {};
    //         let recentExpenseclientExpense = {};

    //         let itemclientpaymentsRiminderData = {
    //             status: 1,
    //             businessId: businessIdinformation,
    //             clientpaymentId: {
    //                 [dbclientpayments.sequelize.Op.in]:
    //                 // [Sequelize.literal("(SELECT MIN(`clientpaymentId`) FROM `clientpayments` WHERE `payment_due_date` >= '" + todayDate + "' AND `payment_end_date` >= '" + todayDate + "' AND `status` = '1' GROUP BY `subscriptionsId`)")]
    //                 // [Sequelize.literal("(SELECT MIN(`clientpaymentId`) FROM `clientpayments` WHERE (`payment_due_date` <= '" + todayDate + "' AND `payment_end_date` >= '" + todayDate + "' AND `status` = '1' ) OR ( `payment_due_date` >= '" + todayDate + "' AND `payment_end_date` >= '" + todayDate + "' AND `status` = '1' ) AND `status` = '1' GROUP BY `subscriptionsId`)")]
    //                 // [Sequelize.literal("(SELECT MIN(`clientpaymentId`) FROM `clientpayments` INNER JOIN `subscriptions` AS `subscription` ON `clientpayments`.`subscriptionsId` = `subscription`.`subscriptionId` INNER JOIN `pricepacks` AS `subscription->pricepack` ON `subscription`.`pricepacks` = `subscription->pricepack`.`pricepackId` WHERE Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day ) <= '" + todayDate + "' OR ( ( `clientpayments`.`payment_due_date` <= '" + todayDate + "' AND `clientpayments`.`payment_end_date` >= '" + todayDate + "' ) OR ( `clientpayments`.`payment_due_date` >= '" + todayDate + "' AND `clientpayments`.`payment_end_date` >= '" + todayDate + "' ) AND `clientpayments`.`status` = '1' ) GROUP BY `clientpayments`.`subscriptionsId` ORDER BY `payment_end_date` ASC )")]
    //                     [Sequelize.literal("(SELECT MIN(`clientpaymentId`) as `clientpaymentId` FROM `clientpayments` INNER JOIN `subscriptions` AS `subscription` ON `clientpayments`.`subscriptionsId` = `subscription`.`subscriptionId` INNER JOIN `pricepacks` AS `subscription->pricepack` ON `subscription`.`pricepacks` = `subscription->pricepack`.`pricepackId` WHERE ( DATE( Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day )) >= '" + todayDate + "' ) AND ( ( `clientpayments`.`payment_due_date` <= '" + todayDate + "' AND `clientpayments`.`payment_end_date` >= '" + todayDate + "' ) OR ( `clientpayments`.`payment_due_date` >= '" + todayDate + "' AND `clientpayments`.`payment_end_date` >= '" + todayDate + "' )) AND `clientpayments`.`status` = '1' GROUP BY `clientpayments`.`subscriptionsId` UNION select c.clientpaymentId from clientpayments as c, ( SELECT a.clientpaymentId, a.curent_subcription_number, a.payment_end_date, COUNT(a.subscriptionsId) FROM clientpayments a LEFT JOIN clientpayments b ON a.subscriptionsId = b.subscriptionsId AND b.status = 1 WHERE b.status = 1 AND a.status = 1 GROUP BY a.subscriptionsId, a.clientpaymentId having a.curent_subcription_number = COUNT(a.subscriptionsId) AND a.payment_end_date < CURDATE() ) as total WHERE ( DATE( Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day )) >= '" + todayDate + "' ) AND c.clientpaymentId IN (total.clientpaymentId))")]
    //             },
    //             [dbClients.sequelize.Op.or]: [{
    //                     "$subscription.client.clientName$": {
    //                         like: "%" + filter + "%"
    //                     }
    //                 },
    //                 {
    //                     "$subscription.pricepack.pricepackName$": {
    //                         like: "%" + filter + "%"
    //                     }
    //                 }
    //             ],
    //             [dbclientScheduleMap.sequelize.Op.and]: [Sequelize.literal("( ( `subscription->clientschedulemap`.`clientScheduleMapId` IS NULL OR `subscription->clientschedulemap`.`sessionCount` IS NULL ) OR ( `subscription->clientschedulemap`.`sessionCount` < `subscription->pricepack`.`serviceDuration` ) )")],
    //         };
    //         let itemGroupConditions = {
    //             status: 1,
    //             businessId: businessIdinformation,
    //         };
    //         let clientpaymentsRiminderData = await dbclientpayments.clientpayments.findAndCount({
    //             attributes: [
    //                 "clientpaymentId",
    //                 "subscriptionsId",
    //                 "due_amount",
    //                 "payble_amount",
    //                 "payment_due_date",
    //                 "payment_end_date", ["UpdateDateTime", "updated_at"],
    //                 "paymentStatus", [dbclientpayments.sequelize.literal("DATE(Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day ))"), 'subcription_end_Date']
    //             ],
    //             include: [{
    //                 model: dbsubscriptions.subscriptions,
    //                 attributes: [
    //                     "clientId",
    //                     "amountPaidSoFar",
    //                     "status_amountPaidSoFar",
    //                     "clientJoiningDate",
    //                     "subscriptionDateTime",
    //                     "installment_frequency",
    //                 ],
    //                 include: [{
    //                         model: dbClients.clients,
    //                         attributes: {
    //                             include: [
    //                                 "clientName", [
    //                                     dbClients.clients.sequelize.literal(
    //                                         'CASE WHEN photoUrl  is NULL THEN CONCAT("' +
    //                                         KNACK_UPLOAD_URL.localUrl +
    //                                         '" , "noimage.jpg" ) ELSE CONCAT("' +
    //                                         KNACK_UPLOAD_URL.localUrl +
    //                                         '" , photoUrl ) END'
    //                                     ),
    //                                     "photoUrl"
    //                                 ],
    //                             ],
    //                             exclude: []
    //                         },
    //                         where: itemGroupConditions
    //                     },
    //                     {
    //                         model: dbpricepack.pricepacks,
    //                         attributes: ["pricepackName"],
    //                         where: {}
    //                     },
    //                     {
    //                         model: dbclientScheduleMap.clientschedulemap,
    //                         required: false,
    //                         attributes: ["sessionCount"],
    //                         where: {
    //                             // [dbclientScheduleMap.sequelize.Op.and]: [Sequelize.literal("`subscription->clientschedulemap`.`scheduleId`= `subscription->clientschedulemap`.`parentScheduleId`")],
    //                             [dbclientScheduleMap.sequelize.Op.and]: [Sequelize.literal("( `subscription->clientschedulemap`.`scheduleId` = `subscription->clientschedulemap`.`parentScheduleId` ) AND ( `subscription->clientschedulemap`.`scheduleId` = (select min(`scheduleId`) from `clientschedulemap` `inSched` where `inSched`.`subscriptionId` = `subscription`.`subscriptionId` ) )")],
    //                         }
    //                     }
    //                 ],
    //                 where: {}
    //             }],
    //             order: [
    //                 ["CreateDateTime", "DESC"]
    //             ],
    //             where: itemclientpaymentsRiminderData,
    //             subQuery: false,
    //             offset: offsetLimit,
    //             limit: limitPerPage,
    //             raw: true
    //         });

    //         clientpaymentsRiminder.count = clientpaymentsRiminderData.count;
    //         clientpaymentsRiminder.rows = clientpaymentsRiminderData.rows;
    //         return await clientpaymentsRiminder;

    //     } catch (error) {
    //         throw error;
    //     }
    // };




    activeClientListRoute = async(
        information,
        params,
        filterPrivateFields = true
    ) => {
        try {
            let clientpaymentsRiminder = {};
            let todayDate = await hyphenDateFormat(new Date);
            let businessIdinformation = information.split(",")[1];
            let dbClients = createInstance(["client"], information);
            let filter = params[0] ? params[0] : "";
            var pageNo = params["page"] ? params["page"] : 1;
            var limitPerPage = PEGINATION_LIMIT;
            var offsetLimit = (pageNo - 1) * limitPerPage;
            let itemclientpaymentsRiminderData = {
                status: 1,
                businessId: businessIdinformation,
                clientId: {
                    [dbClients.sequelize.Op.in]: [Sequelize.literal("( SELECT `subscription`.`clientId` AS `subscription.clientId` FROM `clientpayments` AS `clientpayments` INNER JOIN `subscriptions` AS `subscription` ON `clientpayments`.`subscriptionsId` = `subscription`.`subscriptionId` INNER JOIN `clients` AS `subscription->client` ON `subscription`.`clientId` = `subscription->client`.`clientId` AND `subscription->client`.`status` = 1 AND `subscription->client`.`businessId` = '" + businessIdinformation + "' INNER JOIN `pricepacks` AS `subscription->pricepack` ON `subscription`.`pricepacks` = `subscription->pricepack`.`pricepackId` LEFT OUTER JOIN `clientschedulemap` AS `subscription->clientschedulemap` ON `subscription`.`subscriptionId` = `subscription->clientschedulemap`.`subscriptionId` AND ( ( `subscription->clientschedulemap`.`scheduleId` = `subscription->clientschedulemap`.`parentScheduleId` ) AND ( `subscription->clientschedulemap`.`scheduleId` = ( select min(`scheduleId`) from `clientschedulemap` `inSched` where `inSched`.`subscriptionId` = `subscription`.`subscriptionId` ) ) ) WHERE ( `subscription->client`.`clientName` LIKE '%" + filter + "%' OR `subscription->pricepack`.`pricepackName` LIKE '%" + filter + "%' ) AND ( ( ( `subscription->clientschedulemap`.`clientScheduleMapId` IS NULL OR `subscription->clientschedulemap`.`sessionCount` IS NULL ) OR ( `subscription->clientschedulemap`.`sessionCount` < `subscription->pricepack`.`serviceDuration` ) ) ) AND `clientpayments`.`status` = 1 AND `clientpayments`.`businessId` = '" + businessIdinformation + "' AND `clientpayments`.`clientpaymentId` IN ( ( SELECT MIN(`clientpaymentId`) as `clientpaymentId` FROM `clientpayments` INNER JOIN `subscriptions` AS `subscription` ON `clientpayments`.`subscriptionsId` = `subscription`.`subscriptionId` INNER JOIN `pricepacks` AS `subscription->pricepack` ON `subscription`.`pricepacks` = `subscription->pricepack`.`pricepackId` WHERE ( DATE( Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day ) ) >= '" + todayDate + "' ) AND ( ( `clientpayments`.`payment_due_date` <= '" + todayDate + "' AND `clientpayments`.`payment_end_date` >= '" + todayDate + "' ) OR ( `clientpayments`.`payment_due_date` >= '" + todayDate + "' AND `clientpayments`.`payment_end_date` >= '" + todayDate + "' ) ) AND `clientpayments`.`status` = '1' GROUP BY `clientpayments`.`subscriptionsId` UNION select c.clientpaymentId from clientpayments as c, ( SELECT a.clientpaymentId, a.curent_subcription_number, a.payment_end_date, COUNT(a.subscriptionsId) FROM clientpayments a LEFT JOIN clientpayments b ON a.subscriptionsId = b.subscriptionsId AND b.status = 1 WHERE b.status = 1 AND a.status = 1 GROUP BY a.subscriptionsId, a.clientpaymentId having a.curent_subcription_number = COUNT(a.subscriptionsId) AND a.payment_end_date < CURDATE() ) as total WHERE ( DATE( Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day ) ) >= '" + todayDate + "' ) AND c.clientpaymentId IN (total.clientpaymentId) ) ) GROUP BY `subscription.clientId` )")]
                },
            };
            let clientpaymentsRiminderData = await dbClients.clients.findAndCount({
                attributes: [
                    ["clientId", "subscription.clientId"],
                    ["clientId", "subscription.client.clientId"],
                    ["clientName", "subscription.client.clientName"],
                    ["contactNumber", "subscription.client.contactNumber"],
                    [
                        dbClients.clients.sequelize.literal(
                            'CASE WHEN photoUrl  is NULL THEN CONCAT("' +
                            KNACK_UPLOAD_URL.localUrl +
                            '" , "noimage.jpg" ) ELSE CONCAT("' +
                            KNACK_UPLOAD_URL.localUrl +
                            '" , photoUrl ) END'
                        ),
                        "subscription.client.photoUrl"
                    ]
                ],
                order: [
                    ["CreateDateTime", "DESC"]
                ],
                where: itemclientpaymentsRiminderData,
                offset: offsetLimit,
                limit: limitPerPage,
                raw: true
            });
            clientpaymentsRiminder.count = clientpaymentsRiminderData.count;
            clientpaymentsRiminder.rows = clientpaymentsRiminderData.rows;
            dbClients.sequelize.close();
            return await clientpaymentsRiminder;
        } catch (error) {
            throw error;
        }
    };


    // inactiveClientListRoute = async(
    //     information,
    //     params,
    //     filterPrivateFields = true
    // ) => {
    //     try {
    //         let todayFormat = await date_ISO_Convert(new Date());
    //         // let todayFormat = "2018-08-13";
    //         let orderBy;
    //         let businessIdinformation = information.split(",")[1];
    //         let dbsubscriptions = createInstance(["clientSubscription"], information);
    //         let filter = params[0] ? params[0] : "";
    //         var pageNo = params["page"] ? params["page"] : 1;
    //         var limitPerPage = PEGINATION_LIMIT;
    //         var offsetLimit = (pageNo - 1) * limitPerPage;
    //         if (params["order"] == 1) {
    //             orderBy = "ORDER BY `clients`.`clientname` ASC";
    //         } else if (params["order"] == 2) {
    //             orderBy = "ORDER BY `clients`.`createDateTime` DESC";
    //         } else {
    //             orderBy = "ORDER BY `clients`.`createDateTime` ASC";
    //         }
    //         let inactiveClientListCount = await dbsubscriptions.sequelize.query(
    //             "SELECT count(*) count FROM `clients` WHERE `clientId` in ( select `clientid` from ( SELECT DISTINCT (`clientid`), `subscriptionId` FROM `subscriptions` INNER JOIN `pricepacks` AS `pricepack` ON `subscriptions`.`pricepacks` = `pricepack`.`pricepackid` WHERE Date_add( `subscriptions`.`subscriptiondatetime`, INTERVAL `pricepack`.`pricepackvalidity` day) < '" + todayFormat + "') as `p` WHERE `p`.`clientid` not in (SELECT `a`.`clientid` FROM ( SELECT `clientid` FROM `clients` WHERE `clientid` IN ( SELECT DISTINCT (`clientid`) FROM `subscriptions` INNER JOIN `pricepacks` AS `pricepack` ON `subscriptions`.`pricepacks` = `pricepack`.`pricepackid` WHERE '" + todayFormat + "' BETWEEN `subscriptiondatetime` AND Date_add( `subscriptions`.`subscriptiondatetime`, INTERVAL `pricepack`.`pricepackvalidity` day) ) AND `clients`.`status` = 1 AND `clients`.`businessId` = '" + businessIdinformation + "') `a` LEFT JOIN ( SELECT `clientid` FROM `clients` WHERE `clientid` IN ( SELECT DISTINCT (`clientid`) FROM `subscriptions` INNER JOIN `pricepacks` AS `pricepack` ON `subscriptions`.`pricepacks` = `pricepack`.`pricepackid` WHERE Date_add( `subscriptions`.`subscriptiondatetime`, INTERVAL `pricepack`.`pricepackvalidity` day) < '" + todayFormat + "' )AND `clients`.`status` = 1 AND `clients`.`businessId` = '" + businessIdinformation + "') `b` ON `a`.`clientid` = `b`.`clientid` WHERE `b`.`clientid` IS NOT NULL )  )  AND `clients`.`clientName` LIKE '%" + filter + "%' ", {
    //                 type: dbsubscriptions.sequelize.QueryTypes.SELECT
    //             }
    //         );
    //         let inactiveClientList = await dbsubscriptions.sequelize.query(
    //             "SELECT *,CASE WHEN photoUrl  is NULL THEN CONCAT('" + KNACK_UPLOAD_URL.localUrl + "' , 'noimage.jpg' ) ELSE CONCAT('" + KNACK_UPLOAD_URL.localUrl + "' , photoUrl ) END AS `photoFullUrl` FROM `clients` WHERE `clientId` in ( select `clientid` from ( SELECT DISTINCT (`clientid`), `subscriptionId` FROM `subscriptions` INNER JOIN `pricepacks` AS `pricepack` ON `subscriptions`.`pricepacks` = `pricepack`.`pricepackid` WHERE Date_add( `subscriptions`.`subscriptiondatetime`, INTERVAL `pricepack`.`pricepackvalidity` day) < '" + todayFormat + "') as `p` WHERE `p`.`clientid` not in ( SELECT `a`.`clientid` FROM ( SELECT `clientid` FROM `clients` WHERE `clientid` IN ( SELECT DISTINCT (`clientid`) FROM `subscriptions` INNER JOIN `pricepacks` AS `pricepack` ON `subscriptions`.`pricepacks` = `pricepack`.`pricepackid` WHERE '" + todayFormat + "' BETWEEN `subscriptiondatetime` AND Date_add( `subscriptions`.`subscriptiondatetime`, INTERVAL `pricepack`.`pricepackvalidity` day) ) AND `clients`.`status` = 1 AND `clients`.`businessId` = '" + businessIdinformation + "' ) `a` LEFT JOIN ( SELECT `clientid` FROM `clients` WHERE `clientid` IN ( SELECT DISTINCT (`clientid`) FROM `subscriptions` INNER JOIN `pricepacks` AS `pricepack` ON `subscriptions`.`pricepacks` = `pricepack`.`pricepackid` WHERE Date_add( `subscriptions`.`subscriptiondatetime`, INTERVAL `pricepack`.`pricepackvalidity` day) < '" + todayFormat + "') AND `clients`.`status` = 1 AND `clients`.`businessId` = '" + businessIdinformation + "') `b` ON `a`.`clientid` = `b`.`clientid` WHERE `b`.`clientid` IS NOT NULL ) ) AND `clients`.`clientName` LIKE '%" + filter + "%' " + orderBy + " LIMIT " + limitPerPage + " OFFSET " + offsetLimit, {
    //                 type: dbsubscriptions.sequelize.QueryTypes.SELECT
    //             }
    //         );
    //         dbsubscriptions.sequelize.close();
    //         return {
    //             "count": inactiveClientListCount[0].count,
    //             "rows": inactiveClientList
    //         };
    //     } catch (error) {
    //         throw error;
    //     }
    // };


    // inactiveClientListRoute = async(
    //     information,
    //     params,
    //     filterPrivateFields = true
    // ) => {
    //     try {
    //         var businessIdinformation = information.split(",")[1];
    //         let dbpaymentTransactions = createInstance(["paymentTransactions"], information);
    //         let dbclientpayments = createInstance(["clientPayments"], information);
    //         let dbClients = createInstance(["client"], information);
    //         let dbsubscriptions = createInstance(["clientSubscription"], information);
    //         let dbpricepack = createInstance(["clientPricepack"], information);
    //         let dbservicepricepackmap = createInstance(["servicePricepackMap"], information);
    //         let dbservices = createInstance(["clientService"], information);
    //         let dbclientScheduleMap = createInstance(["clientScheduleMap"], information);
    //         let todayDate = await hyphenDateFormat(new Date);
    //         dbclientpayments.clientpayments.belongsTo(dbsubscriptions.subscriptions, {
    //             foreignKey: "subscriptionsId",
    //             targetKey: "subscriptionId"
    //         });
    //         dbsubscriptions.subscriptions.belongsTo(dbClients.clients, {
    //             foreignKey: "clientId",
    //             targetKey: "clientId"
    //         });
    //         dbsubscriptions.subscriptions.belongsTo(dbpricepack.pricepacks, {
    //             foreignKey: "pricepacks",
    //             targetKey: "pricepackId"
    //         });
    //         dbclientpayments.clientpayments.belongsTo(dbpaymentTransactions.paymentTransactions, {
    //             foreignKey: "clientpaymentId",
    //             targetKey: "clientpaymentId"
    //         });
    //         dbservicepricepackmap.servicepricepackmap.belongsTo(dbservices.services, {
    //             foreignKey: "serviceId",
    //             targetKey: "serviceId"
    //         });
    //         dbsubscriptions.subscriptions.belongsTo(dbclientScheduleMap.clientschedulemap, {
    //             foreignKey: "subscriptionId",
    //             targetKey: "subscriptionId"
    //         });
    //         let filter = params[0] ? params[0] : "";
    //         var pageNo = params["page"] ? params["page"] : 1;
    //         var limitPerPage = PEGINATION_LIMIT;
    //         var offsetLimit = (pageNo - 1) * limitPerPage;
    //         let orderBy;
    //         if (params["order"] == 1) {
    //             orderBy = [
    //                 ["CreateDateTime", "DESC"]
    //             ];
    //         } else {
    //             orderBy = [
    //                 ["CreateDateTime", "ASC"]
    //             ];
    //         }
    //         let itemConditionsclientpayments = {
    //             status: 1,
    //             is_last_payment: 1,
    //             businessId: businessIdinformation,
    //             [dbClients.sequelize.Op.and]: [{
    //                     "$subscription.client.clientName$": {
    //                         like: "%" + filter + "%"
    //                     }
    //                 },
    //                 {
    //                     "$subscription.pricepack.pricepackName$": {
    //                         like: "%" + filter + "%"
    //                     }
    //                 }
    //             ],
    //             [dbClients.sequelize.Op.and]: [Sequelize.literal("(`subscription->clientschedulemap`.`sessionCount` = `subscription->pricepack`.`serviceDuration` )")],
    //         };
    //         let itemGroupConditions = {
    //             status: 1,
    //             businessId: businessIdinformation,
    //         };
    //         let itemGroupConditionsPricePack = {};
    //         let itemConditions = {
    //             renewl_status: 1
    //         };

    //         let paymentData = await dbclientpayments.clientpayments.findAndCount({
    //             attributes: [
    //                 "clientpaymentId",
    //                 "subscriptionsId",
    //                 "due_amount",
    //                 "payble_amount",
    //                 "payment_due_date",
    //                 "payment_end_date", ["UpdateDateTime", "updated_at"],
    //                 [
    //                     dbClients.clients.sequelize.literal(
    //                         'Date_add( `subscription`.`subscriptiondatetime`,INTERVAL `subscription->pricepack`.`pricepackvalidity` day)'
    //                     ),
    //                     "subcription_end_Date"
    //                 ],
    //                 [
    //                     dbclientpayments.sequelize.literal("( SELECT IFNULL( SUM(`payble_amount`), 0 ) FROM `clientpayments` INNER join `paymentTransactions` AS `paymentTransactions` ON `clientpayments`.`clientpaymentId` = `paymentTransactions`.`clientpaymentId` WHERE `clientpayments`.`subscriptionsId` = `subscription`.`subscriptionId` )"),
    //                     "total_subcription_payemt"
    //                 ]
    //             ],
    //             include: [{
    //                 model: dbsubscriptions.subscriptions,
    //                 attributes: ["clientId", "status_amountPaidSoFar", "amountPaidSoFar"],
    //                 include: [{
    //                         model: dbClients.clients,
    //                         attributes: {
    //                             include: [
    //                                 "clientName", [
    //                                     dbClients.clients.sequelize.literal(
    //                                         'CASE WHEN photoUrl  is NULL THEN CONCAT("' +
    //                                         KNACK_UPLOAD_URL.localUrl +
    //                                         '" , "noimage.jpg" ) ELSE CONCAT("' +
    //                                         KNACK_UPLOAD_URL.localUrl +
    //                                         '" , photoUrl ) END'
    //                                     ),
    //                                     "photoUrl"
    //                                 ],
    //                             ],
    //                             exclude: []
    //                         },
    //                         where: itemGroupConditions
    //                     },
    //                     {
    //                         model: dbpricepack.pricepacks,
    //                         attributes: ["pricepackName", "amount", "serviceDuration"],
    //                         where: itemGroupConditionsPricePack,
    //                     },
    //                     {
    //                         model: dbclientScheduleMap.clientschedulemap,
    //                         attributes: ["scheduleId", "sessionCount"],
    //                         require: false,
    //                         where: {
    //                             [dbclientScheduleMap.sequelize.Op.and]: [Sequelize.literal("( `subscription->clientschedulemap`.`scheduleId` = `subscription->clientschedulemap`.`parentScheduleId` ) AND ( `subscription->clientschedulemap`.`scheduleId` = (select min(`scheduleId`) from `clientschedulemap` `inSched` where `inSched`.`subscriptionId` = `subscription`.`subscriptionId` ) )")],
    //                         }
    //                     }
    //                 ],
    //                 where: itemConditions
    //             }],
    //             order: orderBy,
    //             where: itemConditionsclientpayments,
    //             subQuery: false,
    //             offset: offsetLimit,
    //             limit: limitPerPage,
    //             raw: true
    //         });
    //         if (paymentData.rows) {
    //             for (var i = 0; i < paymentData.rows.length; i++) {
    //                 let serviceAll = await dbservicepricepackmap.servicepricepackmap.findAll({
    //                     attributes: {
    //                         include: [],
    //                         exclude: [`servicepricepackmapId`, `serviceDuration_number`, `pricepackId`, `serviceId`, `businessId`, `serviceDuration`, `createDateTime`, `updateDateTime`, `status`]
    //                     },
    //                     where: {
    //                         status: 1,
    //                         pricepackId: paymentData.rows[i]["subscription.pricepack.pricepackId"]
    //                     },
    //                     include: [{
    //                         model: dbservices.services,
    //                         attributes: [
    //                             [dbservicepricepackmap.sequelize.fn('GROUP_CONCAT', dbservicepricepackmap.sequelize.col('serviceName')), 'serviceNameList']
    //                         ],
    //                     }],
    //                     raw: true
    //                 });
    //                 let serviceAllID = await dbservicepricepackmap.servicepricepackmap.findAll({
    //                     attributes: {
    //                         include: [],
    //                         exclude: [`servicepricepackmapId`, `pricepackId`, `serviceId`, `businessId`, `serviceDuration`, `createDateTime`, `updateDateTime`, `status`]
    //                     },
    //                     where: {
    //                         status: 1,
    //                         pricepackId: paymentData.rows[i]["subscription.pricepack.pricepackId"]
    //                     },
    //                     include: [{
    //                         model: dbservices.services,
    //                         attributes: [
    //                             'serviceId'
    //                         ],
    //                     }],
    //                     raw: true
    //                 });
    //                 paymentData.rows[i]["serviceIDList"] = serviceAllID;
    //                 paymentData.rows[i]["serviceNameList"] = serviceAll;
    //                 if (serviceAllID) {
    //                     for (var x = 0; x < serviceAllID.length; x++) {
    //                         let payble_amount_perPackage = paymentData.rows[i]["subscription.pricepack.amount"];
    //                         let serviceId_perPackage = serviceAllID[x]["service.serviceId"];
    //                         let pricepackId_perPackage = paymentData.rows[i].pricepackId;
    //                         paymentData.rows[i]["getPaymentCalculation"] = await getPaymentCalculation(information, payble_amount_perPackage, serviceId_perPackage, pricepackId_perPackage);
    //                     }
    //                 }
    //                 paymentData.rows[i]["sessionInfo"] = await sessionInfo(information, {
    //                     status: 1,
    //                     subscriptionId: paymentData.rows[i]['subscriptionsId']
    //                 });

    //             }
    //         }
    //         dbclientpayments.sequelize.close();
    //         dbClients.sequelize.close();
    //         dbsubscriptions.sequelize.close();
    //         dbpricepack.sequelize.close();
    //         dbpaymentTransactions.sequelize.close();
    //         dbclientpayments.sequelize.close();
    //         dbservicepricepackmap.sequelize.close();
    //         dbclientScheduleMap.sequelize.close();
    //         return paymentData;
    //     } catch (error) {
    //         throw error;
    //     }
    // };



    inactiveClientListRoute = async(
        information,
        params,
        filterPrivateFields = true
    ) => {
        try {
            let inactiveClientList = {};
            let todayFormat = await date_ISO_Convert(new Date());
            let orderBy;
            let businessIdinformation = information.split(",")[1];
            let filter = params[0] ? params[0] : "";
            var pageNo = params["page"] ? params["page"] : 1;
            var limitPerPage = PEGINATION_LIMIT;
            var offsetLimit = (pageNo - 1) * limitPerPage;
            if (params["order"] == 1) {
                orderBy = "ORDER BY p1.`clientname` ASC";
            } else if (params["order"] == 2) {
                orderBy = "ORDER BY p1.`createDateTime` DESC";
            } else {
                orderBy = "ORDER BY p1.`createDateTime` ASC";
            }
            let dbsubscriptions = createInstance(["clientSubscription"], information);
            let activeClientListCount = await dbsubscriptions.sequelize.query(
                "SELECT p1.*, CASE WHEN p1.photoUrl  is NULL THEN CONCAT('" + KNACK_UPLOAD_URL.localUrl + "' , 'noimage.jpg' ) ELSE CONCAT('" + KNACK_UPLOAD_URL.localUrl + "' , p1.photoUrl ) END AS photoUrl, ( SELECT COUNT(1) FROM `clientpayments` AS `clientpayments` INNER JOIN `subscriptions` AS `subscription` ON `clientpayments`.`subscriptionsId` = `subscription`.`subscriptionId` INNER JOIN `clients` AS `subscription->client` ON `subscription`.`clientId` = `subscription->client`.`clientId` AND `subscription->client`.`status` = 1 AND `subscription->client`.`businessId` = '" + businessIdinformation + "' INNER JOIN `pricepacks` AS `subscription->pricepack` ON `subscription`.`pricepacks` = `subscription->pricepack`.`pricepackId` LEFT OUTER JOIN `clientschedulemap` AS `subscription->clientschedulemap` ON `subscription`.`subscriptionId` = `subscription->clientschedulemap`.`subscriptionId` AND ( ( `subscription->clientschedulemap`.`scheduleId` = `subscription->clientschedulemap`.`parentScheduleId` ) AND ( `subscription->clientschedulemap`.`scheduleId` = ( select min(`scheduleId`) from `clientschedulemap` `inSched` where `inSched`.`subscriptionId` = `subscription`.`subscriptionId` ) ) ) WHERE ( `subscription->client`.`clientName` LIKE '%%' OR `subscription->pricepack`.`pricepackName` LIKE '%%' ) AND ( ( ( `subscription->clientschedulemap`.`clientScheduleMapId` IS NULL OR `subscription->clientschedulemap`.`sessionCount` IS NULL ) OR ( `subscription->clientschedulemap`.`sessionCount` < `subscription->pricepack`.`serviceDuration` ) ) ) AND `clientpayments`.`status` = 1 AND `clientpayments`.`businessId` = '" + businessIdinformation + "' AND `clientpayments`.`clientpaymentId` IN ( ( SELECT MIN(`clientpaymentId`) as `clientpaymentId` FROM `clientpayments` INNER JOIN `subscriptions` AS `subscription` ON `clientpayments`.`subscriptionsId` = `subscription`.`subscriptionId` INNER JOIN `pricepacks` AS `subscription->pricepack` ON `subscription`.`pricepacks` = `subscription->pricepack`.`pricepackId` WHERE ( DATE( Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day ) ) >= '" + todayFormat + "' ) AND ( ( `clientpayments`.`payment_due_date` <= '" + todayFormat + "' AND `clientpayments`.`payment_end_date` >= '" + todayFormat + "' ) OR ( `clientpayments`.`payment_due_date` >= '" + todayFormat + "' AND `clientpayments`.`payment_end_date` >= '" + todayFormat + "' ) ) AND `clientpayments`.`status` = '1' AND `subscription->client`.`clientId` = p1.`clientId` GROUP BY `clientpayments`.`subscriptionsId` UNION select c.clientpaymentId from clientpayments as c, ( SELECT a.clientpaymentId, a.curent_subcription_number, a.payment_end_date, COUNT(a.subscriptionsId) FROM clientpayments a LEFT JOIN clientpayments b ON a.subscriptionsId = b.subscriptionsId AND b.status = 1 WHERE b.status = 1 AND a.status = 1 GROUP BY a.subscriptionsId, a.clientpaymentId having a.curent_subcription_number = COUNT(a.subscriptionsId) AND a.payment_end_date < CURDATE() ) as total WHERE ( DATE( Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day ) ) >= '" + todayFormat + "' ) AND c.clientpaymentId IN (total.clientpaymentId) AND `subscription->client`.`clientId` = p1.`clientId` ) ) ) as `current_subcription_count` FROM `clients` AS p1 having `current_subcription_count` = 0 AND p1.`clientName` LIKE '%" + filter + "%' " + orderBy + " LIMIT " + limitPerPage + " OFFSET " + offsetLimit, {
                    type: dbsubscriptions.sequelize.QueryTypes.SELECT
                }
            );
            dbsubscriptions.sequelize.close();
            inactiveClientList.count = activeClientListCount.length;
            inactiveClientList.rows = activeClientListCount;
            return await inactiveClientList;

        } catch (error) {
            throw error;
        }
    };



    clientType = async(
        information,
        clientid,
        today,
    ) => {
        try {
            let businessIdinformation = information.split(",")[1];
            let dbsubscriptions = createInstance(["clientSubscription"], information);
            let final = {
                "activeClient": false,
                "inactiveClient": false,
                "leadClient": false
            }
            let activeClientListCount = await dbsubscriptions.sequelize.query(
                "SELECT COUNT(*) AS count FROM `clients` WHERE `clientid` IN ( SELECT DISTINCT(`clientid`) FROM `subscriptions` INNER JOIN `pricepacks` AS `pricepack` ON `subscriptions`.`pricepacks` = `pricepack`.`pricepackid` WHERE '" + today + "' BETWEEN `subscriptiondatetime` AND Date_add( `subscriptions`.`subscriptiondatetime`,INTERVAL `pricepack`.`pricepackvalidity` day)) AND `clients`.`status` = 1 AND `clients`.`businessId` = '" + businessIdinformation + "' AND `clients`.`clientId` = '" + clientid + "' ", {
                    type: dbsubscriptions.sequelize.QueryTypes.SELECT
                }
            );
            let inactiveClientListCount = await dbsubscriptions.sequelize.query(
                "SELECT count(*) count FROM `clients` WHERE `clientId` in ( select `clientid` from ( SELECT DISTINCT (`clientid`), `subscriptionId` FROM `subscriptions` INNER JOIN `pricepacks` AS `pricepack` ON `subscriptions`.`pricepacks` = `pricepack`.`pricepackid` WHERE Date_add( `subscriptions`.`subscriptiondatetime`, INTERVAL `pricepack`.`pricepackvalidity` day) < '" + today + "') as `p` WHERE `p`.`clientid` not in (SELECT `a`.`clientid` FROM ( SELECT `clientid` FROM `clients` WHERE `clientid` IN ( SELECT DISTINCT (`clientid`) FROM `subscriptions` INNER JOIN `pricepacks` AS `pricepack` ON `subscriptions`.`pricepacks` = `pricepack`.`pricepackid` WHERE '" + today + "' BETWEEN `subscriptiondatetime` AND Date_add( `subscriptions`.`subscriptiondatetime`, INTERVAL `pricepack`.`pricepackvalidity` day) ) AND `clients`.`status` = 1 AND `clients`.`businessId` = '" + businessIdinformation + "') `a` LEFT JOIN ( SELECT `clientid` FROM `clients` WHERE `clientid` IN ( SELECT DISTINCT (`clientid`) FROM `subscriptions` INNER JOIN `pricepacks` AS `pricepack` ON `subscriptions`.`pricepacks` = `pricepack`.`pricepackid` WHERE Date_add( `subscriptions`.`subscriptiondatetime`, INTERVAL `pricepack`.`pricepackvalidity` day) < '" + today + "' )AND `clients`.`status` = 1 AND `clients`.`businessId` = '" + businessIdinformation + "') `b` ON `a`.`clientid` = `b`.`clientid` WHERE `b`.`clientid` IS NOT NULL )  )  AND `clients`.`clientId` = '" + clientid + "' ", {
                    type: dbsubscriptions.sequelize.QueryTypes.SELECT
                }
            );
            dbsubscriptions.sequelize.close();
            if ((activeClientListCount[0].count > 0)) {
                final.activeClient = true;
            } else if ((inactiveClientListCount[0].count > 0)) {
                final.inactiveClient = true;
            } else {
                final.leadClient = true;
            }
            return final;
        } catch (error) {
            throw error;
        }
    };


    listCurrentPastSubcriptionPerClient = async(
        information,
        params,
        filterPrivateFields = true
    ) => {
        try {
            let todayFormat = await date_ISO_Convert(new Date());
            // let todayFormat = "2018-08-23";
            let orderBy;
            let total_record = {};
            let clientId = params['clientId'];
            let businessIdinformation = information.split(",")[1];
            let dbpaymentTransactions = createInstance(["paymentTransactions"], information);
            let dbclientpayments = createInstance(["clientPayments"], information);
            let dbClients = createInstance(["client"], information);
            let dbsubscriptions = createInstance(["clientSubscription"], information);
            let dbpricepack = createInstance(["clientPricepack"], information);
            let dbservicepricepackmap = createInstance(["servicePricepackMap"], information);
            let dbservices = createInstance(["clientService"], information);
            dbclientpayments.clientpayments.belongsTo(dbsubscriptions.subscriptions, {
                foreignKey: "subscriptionsId",
                targetKey: "subscriptionId"
            });
            dbsubscriptions.subscriptions.belongsTo(dbClients.clients, {
                foreignKey: "clientId",
                targetKey: "clientId"
            });
            dbsubscriptions.subscriptions.belongsTo(dbpricepack.pricepacks, {
                foreignKey: "pricepacks",
                targetKey: "pricepackId"
            });
            dbclientpayments.clientpayments.belongsTo(dbpaymentTransactions.paymentTransactions, {
                foreignKey: "clientpaymentId",
                targetKey: "clientpaymentId"
            });
            dbservicepricepackmap.servicepricepackmap.belongsTo(dbservices.services, {
                foreignKey: "serviceId",
                targetKey: "serviceId"
            });
            let filter = params[0] ? params[0] : "";
            var pageNo = params["page"] ? params["page"] : 1;
            var limitPerPage = PEGINATION_LIMIT;
            var offsetLimit = (pageNo - 1) * limitPerPage;
            if (params["order"] == 1) {
                orderBy = "ORDER BY `subcription_type` DESC";
            } else {
                orderBy = "ORDER BY `subcription_type` ASC";
            }
            // let paymentData = await dbsubscriptions.sequelize.query("( SELECT `clientpayments`.`clientpaymentId`, `clientpayments`.`subscriptionsId`, `clientpayments`.`due_amount`, `clientpayments`.`payble_amount`, `clientpayments`.`payment_due_date`, `clientpayments`.`payment_end_date`, `clientpayments`.`UpdateDateTime` AS `updated_at`, `clientpayments`.`paymentStatus`, Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day ) AS `subcription_end_Date`, `subscription`.`clientId` AS `subscription.clientId`, `subscription`.`amountPaidSoFar` AS `subscription.amountPaidSoFar`, `subscription`.`status_amountPaidSoFar` AS `subscription.status_amountPaidSoFar`, `subscription->client`.`clientId` AS `subscription.client.clientId`, `subscription->client`.`businessId` AS `subscription.client.businessId`, `subscription->client`.`centerId` AS `subscription.client.centerId`, `subscription->client`.`clientName` AS `subscription.client.clientName`, `subscription->client`.`contactNumber` AS `subscription.client.contactNumber`, `subscription->client`.`alternateNumber` AS `subscription.client.alternateNumber`, `subscription->client`.`emailId` AS `subscription.client.emailId`, `subscription->client`.`alternateEmail` AS `subscription.client.alternateEmail`, `subscription->client`.`dateOfBirth` AS `subscription.client.dateOfBirth`, `subscription->client`.`area` AS `subscription.client.area`, `subscription->client`.`pin` AS `subscription.client.pin`, `subscription->client`.`city` AS `subscription.client.city`, `subscription->client`.`photoUrl` AS `subscription.client.photoUrl`, `subscription->client`.`createDateTime` AS `subscription.client.createDateTime`, `subscription->client`.`updateDateTime` AS `subscription.client.updateDateTime`, `subscription->client`.`status` AS `subscription.client.status`, `subscription->client`.`clientName` AS `subscription.client.clientName`, CASE WHEN photoUrl is NULL THEN CONCAT( '" + KNACK_UPLOAD_URL.localUrl + "', 'noimage.jpg' ) ELSE CONCAT( '" + KNACK_UPLOAD_URL.localUrl + "', photoUrl ) END AS `subscription.client.photoUrl`, `subscription->pricepack`.`pricepackId` AS `subscription.pricepack.pricepackId`, `subscription->pricepack`.`pricepackName` AS `subscription.pricepack.pricepackName`, 1 as `subcription_type` FROM `clientpayments` AS `clientpayments` INNER JOIN `subscriptions` AS `subscription` ON `clientpayments`.`subscriptionsId` = `subscription`.`subscriptionId` INNER JOIN `clients` AS `subscription->client` ON `subscription`.`clientId` = `subscription->client`.`clientId` AND `subscription->client`.`status` = 1 AND `subscription->client`.`businessId` = '" + businessIdinformation + "' INNER JOIN `pricepacks` AS `subscription->pricepack` ON `subscription`.`pricepacks` = `subscription->pricepack`.`pricepackId` LEFT OUTER JOIN `clientschedulemap` AS `subscription->clientschedulemap` ON `subscription`.`subscriptionId` = `subscription->clientschedulemap`.`subscriptionId` AND `subscription->clientschedulemap`.`scheduleId`= `subscription->clientschedulemap`.`parentScheduleId` WHERE ( `subscription->client`.`clientName` LIKE '%" + filter + "%' OR `subscription->pricepack`.`pricepackName` LIKE '%" + filter + "%' ) AND ( ( `subscription->clientschedulemap`.`clientScheduleMapId` IS NULL OR `subscription->clientschedulemap`.`sessionCount` IS NULL ) OR ( `subscription->clientschedulemap`.`sessionCount` < `subscription->pricepack`.`serviceDuration` ) ) AND `clientpayments`.`status` = 1 AND `clientpayments`.`paymentStatus` = 1 AND `clientpayments`.`businessId` = '" + businessIdinformation + "' AND `subscription->client`.`clientId` = '" + clientId + "' AND `clientpayments`.`clientpaymentId` IN ( ( SELECT MIN(`clientpaymentId`) FROM `clientpayments` WHERE `payment_due_date` >= '" + todayFormat + "' AND `payment_end_date` >= '" + todayFormat + "' AND `status` = '1' AND `paymentStatus` = '1' GROUP BY `subscriptionsId` ) ) ) union all ( SELECT `clientpayments`.`clientpaymentId`, `clientpayments`.`subscriptionsId`, `clientpayments`.`due_amount`, `clientpayments`.`payble_amount`, `clientpayments`.`payment_due_date`, `clientpayments`.`payment_end_date`, `clientpayments`.`UpdateDateTime` AS `updated_at`, `clientpayments`.`paymentStatus`, Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day ) AS `subcription_end_Date`, `subscription`.`clientId` AS `subscription.clientId`, `subscription`.`amountPaidSoFar` AS `subscription.amountPaidSoFar`, `subscription`.`status_amountPaidSoFar` AS `subscription.status_amountPaidSoFar`, `subscription->client`.`clientId` AS `subscription.client.clientId`, `subscription->client`.`businessId` AS `subscription.client.businessId`, `subscription->client`.`centerId` AS `subscription.client.centerId`, `subscription->client`.`clientName` AS `subscription.client.clientName`, `subscription->client`.`contactNumber` AS `subscription.client.contactNumber`, `subscription->client`.`alternateNumber` AS `subscription.client.alternateNumber`, `subscription->client`.`emailId` AS `subscription.client.emailId`, `subscription->client`.`alternateEmail` AS `subscription.client.alternateEmail`, `subscription->client`.`dateOfBirth` AS `subscription.client.dateOfBirth`, `subscription->client`.`area` AS `subscription.client.area`, `subscription->client`.`pin` AS `subscription.client.pin`, `subscription->client`.`city` AS `subscription.client.city`, `subscription->client`.`photoUrl` AS `subscription.client.photoUrl`, `subscription->client`.`createDateTime` AS `subscription.client.createDateTime`, `subscription->client`.`updateDateTime` AS `subscription.client.updateDateTime`, `subscription->client`.`status` AS `subscription.client.status`, `subscription->client`.`clientName` AS `subscription.client.clientName`, CASE WHEN photoUrl is NULL THEN CONCAT( '" + KNACK_UPLOAD_URL.localUrl + "', 'noimage.jpg' ) ELSE CONCAT( '" + KNACK_UPLOAD_URL.localUrl + "', photoUrl ) END AS `subscription.client.photoUrl`, `subscription->pricepack`.`pricepackId` AS `subscription.pricepack.pricepackId`, `subscription->pricepack`.`pricepackName` AS `subscription.pricepack.pricepackName`, 0 as `subcription_type` FROM `clientpayments` AS `clientpayments` INNER JOIN `subscriptions` AS `subscription` ON `clientpayments`.`subscriptionsId` = `subscription`.`subscriptionId` AND `subscription`.`renewl_status` = 1 INNER JOIN `clients` AS `subscription->client` ON `subscription`.`clientId` = `subscription->client`.`clientId` AND `subscription->client`.`status` = 1 AND `subscription->client`.`businessId` = '" + businessIdinformation + "' AND `subscription->client`.`clientId` = '" + clientId + "' INNER JOIN `pricepacks` AS `subscription->pricepack` ON `subscription`.`pricepacks` = `subscription->pricepack`.`pricepackId` LEFT OUTER JOIN `clientschedulemap` AS `subscription->clientschedulemap` ON `subscription`.`subscriptionId` = `subscription->clientschedulemap`.`subscriptionId` AND `subscription->clientschedulemap`.`scheduleId`= `subscription->clientschedulemap`.`parentScheduleId` WHERE (`subscription->client`.`clientName` LIKE '%" + filter + "%' OR `subscription->pricepack`.`pricepackName` LIKE '%" + filter + "%') AND ( Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day ) < '" + todayFormat + "' OR ( `subscription->clientschedulemap`.`sessionCount` = `subscription->pricepack`.`serviceDuration` ) ) AND `clientpayments`.`status` = 1 AND `clientpayments`.`is_last_payment` = 1 AND `clientpayments`.`businessId` = '" + businessIdinformation + "' ) " + orderBy + " LIMIT " + limitPerPage + " OFFSET " + offsetLimit, {
            //     type: dbsubscriptions.sequelize.QueryTypes.SELECT
            // });
            let paymentData = await dbsubscriptions.sequelize.query("( SELECT `clientpayments`.`clientpaymentId`, `clientpayments`.`subscriptionsId`, `clientpayments`.`due_amount`, `clientpayments`.`payble_amount`, `clientpayments`.`payment_due_date`, `clientpayments`.`payment_end_date`, `clientpayments`.`UpdateDateTime` AS `updated_at`, `clientpayments`.`paymentStatus`, DATE(DATE( Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day ) )) AS `subcription_end_Date`, `subscription`.`clientId` AS `subscription.clientId`, `subscription`.`amountPaidSoFar` AS `subscription.amountPaidSoFar`, `subscription`.`status_amountPaidSoFar` AS `subscription.status_amountPaidSoFar`, `subscription`.`clientJoiningDate` AS `subscription.clientJoiningDate`, `subscription`.`subscriptionDateTime` AS `subscription.subscriptionDateTime`, `subscription`.`installment_frequency` AS `subscription.installment_frequency`, `subscription->client`.`clientId` AS `subscription.client.clientId`, `subscription->client`.`businessId` AS `subscription.client.businessId`, `subscription->client`.`centerId` AS `subscription.client.centerId`, `subscription->client`.`clientName` AS `subscription.client.clientName`, `subscription->client`.`contactNumber` AS `subscription.client.contactNumber`, `subscription->client`.`alternateNumber` AS `subscription.client.alternateNumber`, `subscription->client`.`emailId` AS `subscription.client.emailId`, `subscription->client`.`alternateEmail` AS `subscription.client.alternateEmail`, `subscription->client`.`dateOfBirth` AS `subscription.client.dateOfBirth`, `subscription->client`.`area` AS `subscription.client.area`, `subscription->client`.`pin` AS `subscription.client.pin`, `subscription->client`.`city` AS `subscription.client.city`, `subscription->client`.`photoUrl` AS `subscription.client.photoUrl`, `subscription->client`.`createDateTime` AS `subscription.client.createDateTime`, `subscription->client`.`updateDateTime` AS `subscription.client.updateDateTime`, `subscription->client`.`status` AS `subscription.client.status`, `subscription->client`.`clientName` AS `subscription.client.clientName`, CASE WHEN photoUrl is NULL THEN CONCAT( '" + KNACK_UPLOAD_URL.localUrl + "', 'noimage.jpg' ) ELSE CONCAT( '" + KNACK_UPLOAD_URL.localUrl + "', photoUrl ) END AS `subscription.client.photoUrl`, `subscription->pricepack`.`pricepackId` AS `subscription.pricepack.pricepackId`, `subscription->pricepack`.`pricepackName` AS `subscription.pricepack.pricepackName`, `subscription->clientschedulemap`.`clientScheduleMapId` AS `subscription.clientschedulemap.clientScheduleMapId`, `subscription->clientschedulemap`.`sessionCount` AS `subscription.clientschedulemap.sessionCount`, 1 as `subcription_type` FROM `clientpayments` AS `clientpayments` INNER JOIN `subscriptions` AS `subscription` ON `clientpayments`.`subscriptionsId` = `subscription`.`subscriptionId` INNER JOIN `clients` AS `subscription->client` ON `subscription`.`clientId` = `subscription->client`.`clientId` AND `subscription->client`.`status` = 1 AND `subscription->client`.`businessId` = '" + businessIdinformation + "' AND `subscription->client`.`clientId` = '" + clientId + "' INNER JOIN `pricepacks` AS `subscription->pricepack` ON `subscription`.`pricepacks` = `subscription->pricepack`.`pricepackId` LEFT OUTER JOIN `clientschedulemap` AS `subscription->clientschedulemap` ON `subscription`.`subscriptionId` = `subscription->clientschedulemap`.`subscriptionId` AND ( ( `subscription->clientschedulemap`.`scheduleId` = `subscription->clientschedulemap`.`parentScheduleId` ) AND ( `subscription->clientschedulemap`.`scheduleId` = ( select min(`scheduleId`) from `clientschedulemap` `inSched` where `inSched`.`subscriptionId` = `subscription`.`subscriptionId` ) ) ) WHERE ( `subscription->client`.`clientName` LIKE '%" + filter + "%' OR `subscription->pricepack`.`pricepackName` LIKE '%" + filter + "%' ) AND ( ( ( `subscription->clientschedulemap`.`clientScheduleMapId` IS NULL OR `subscription->clientschedulemap`.`sessionCount` IS NULL ) OR ( `subscription->clientschedulemap`.`sessionCount` < `subscription->pricepack`.`serviceDuration` ) ) ) AND `clientpayments`.`status` = 1 AND `clientpayments`.`businessId` = '" + businessIdinformation + "' AND `clientpayments`.`clientpaymentId` IN ( ( SELECT MIN(`clientpaymentId`) as `clientpaymentId` FROM `clientpayments` INNER JOIN `subscriptions` AS `subscription` ON `clientpayments`.`subscriptionsId` = `subscription`.`subscriptionId` INNER JOIN `pricepacks` AS `subscription->pricepack` ON `subscription`.`pricepacks` = `subscription->pricepack`.`pricepackId` WHERE ( DATE( Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day ) ) >= '" + todayFormat + "' ) AND ( ( `clientpayments`.`payment_due_date` <= '" + todayFormat + "' AND `clientpayments`.`payment_end_date` >= '" + todayFormat + "' ) OR ( `clientpayments`.`payment_due_date` >= '" + todayFormat + "' AND `clientpayments`.`payment_end_date` >= '" + todayFormat + "' ) ) AND `clientpayments`.`status` = '1' GROUP BY `clientpayments`.`subscriptionsId` UNION select c.clientpaymentId from clientpayments as c, ( SELECT a.clientpaymentId, a.curent_subcription_number, a.payment_end_date, COUNT(a.subscriptionsId) FROM clientpayments a LEFT JOIN clientpayments b ON a.subscriptionsId = b.subscriptionsId AND b.status = 1 WHERE b.status = 1 AND a.status = 1 GROUP BY a.subscriptionsId, a.clientpaymentId having a.curent_subcription_number = COUNT(a.subscriptionsId) AND a.payment_end_date < CURDATE() ) as total WHERE ( DATE( Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day ) ) >= '" + todayFormat + "' ) AND c.clientpaymentId IN (total.clientpaymentId) ) ) ) union ( SELECT `clientpayments`.`clientpaymentId`, `clientpayments`.`subscriptionsId`, `clientpayments`.`due_amount`, `clientpayments`.`payble_amount`, `clientpayments`.`payment_due_date`, `clientpayments`.`payment_end_date`, `clientpayments`.`UpdateDateTime` AS `updated_at`, `clientpayments`.`paymentStatus`, DATE(DATE( Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day ) )) AS `subcription_end_Date`, `subscription`.`clientId` AS `subscription.clientId`, `subscription`.`amountPaidSoFar` AS `subscription.amountPaidSoFar`, `subscription`.`status_amountPaidSoFar` AS `subscription.status_amountPaidSoFar`, `subscription`.`clientJoiningDate` AS `subscription.clientJoiningDate`, `subscription`.`subscriptionDateTime` AS `subscription.subscriptionDateTime`, `subscription`.`installment_frequency` AS `subscription.installment_frequency`, `subscription->client`.`clientId` AS `subscription.client.clientId`, `subscription->client`.`businessId` AS `subscription.client.businessId`, `subscription->client`.`centerId` AS `subscription.client.centerId`, `subscription->client`.`clientName` AS `subscription.client.clientName`, `subscription->client`.`contactNumber` AS `subscription.client.contactNumber`, `subscription->client`.`alternateNumber` AS `subscription.client.alternateNumber`, `subscription->client`.`emailId` AS `subscription.client.emailId`, `subscription->client`.`alternateEmail` AS `subscription.client.alternateEmail`, `subscription->client`.`dateOfBirth` AS `subscription.client.dateOfBirth`, `subscription->client`.`area` AS `subscription.client.area`, `subscription->client`.`pin` AS `subscription.client.pin`, `subscription->client`.`city` AS `subscription.client.city`, `subscription->client`.`photoUrl` AS `subscription.client.photoUrl`, `subscription->client`.`createDateTime` AS `subscription.client.createDateTime`, `subscription->client`.`updateDateTime` AS `subscription.client.updateDateTime`, `subscription->client`.`status` AS `subscription.client.status`, `subscription->client`.`clientName` AS `subscription.client.clientName`, CASE WHEN photoUrl is NULL THEN CONCAT( '" + KNACK_UPLOAD_URL.localUrl + "', 'noimage.jpg' ) ELSE CONCAT( '" + KNACK_UPLOAD_URL.localUrl + "', photoUrl ) END AS `subscription.client.photoUrl`, `subscription->pricepack`.`pricepackId` AS `subscription.pricepack.pricepackId`, `subscription->pricepack`.`pricepackName` AS `subscription.pricepack.pricepackName`, `subscription->clientschedulemap`.`clientScheduleMapId` AS `subscription.clientschedulemap.clientScheduleMapId`, `subscription->clientschedulemap`.`sessionCount` AS `subscription.clientschedulemap.sessionCount`, 0 as `subcription_type` FROM `clientpayments` AS `clientpayments` INNER JOIN `subscriptions` AS `subscription` ON `clientpayments`.`subscriptionsId` = `subscription`.`subscriptionId` AND `subscription`.`renewl_status` = 1 INNER JOIN `clients` AS `subscription->client` ON `subscription`.`clientId` = `subscription->client`.`clientId` AND `subscription->client`.`status` = 1 AND `subscription->client`.`businessId` = '" + businessIdinformation + "' AND `subscription->client`.`clientId` = '" + clientId + "' INNER JOIN `pricepacks` AS `subscription->pricepack` ON `subscription`.`pricepacks` = `subscription->pricepack`.`pricepackId` INNER JOIN `clientschedulemap` AS `subscription->clientschedulemap` ON `subscription`.`subscriptionId` = `subscription->clientschedulemap`.`subscriptionId` AND ( ( `subscription->clientschedulemap`.`scheduleId` = `subscription->clientschedulemap`.`parentScheduleId` ) AND ( `subscription->clientschedulemap`.`scheduleId` = ( select min(`scheduleId`) from `clientschedulemap` `inSched` where `inSched`.`subscriptionId` = `subscription`.`subscriptionId` ) ) ) WHERE ( `subscription->client`.`clientName` LIKE '%" + filter + "%' OR `subscription->pricepack`.`pricepackName` LIKE '%" + filter + "%' ) AND ( ( DATE( Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day ) ) < '" + todayFormat + "' ) OR ( `subscription->clientschedulemap`.`sessionCount` = `subscription->pricepack`.`serviceDuration` ) ) AND `clientpayments`.`status` = 1 AND `clientpayments`.`is_last_payment` = 1 AND `clientpayments`.`businessId` = '" + businessIdinformation + "' )" + orderBy + " LIMIT " + limitPerPage + " OFFSET " + offsetLimit, {
                type: dbsubscriptions.sequelize.QueryTypes.SELECT
            });
            if (paymentData) {
                for (var i = 0; i < paymentData.length; i++) {
                    let serviceAll = await dbservicepricepackmap.servicepricepackmap.findAll({
                        attributes: {
                            include: [],
                            exclude: [`servicepricepackmapId`, `serviceDuration_number`, `pricepackId`, `serviceId`, `businessId`, `serviceDuration`, `createDateTime`, `updateDateTime`, `status`]
                        },
                        where: {
                            status: 1,
                            pricepackId: paymentData[i]["subscription.pricepack.pricepackId"]
                        },
                        include: [{
                            model: dbservices.services,
                            attributes: [
                                [dbservicepricepackmap.sequelize.fn('GROUP_CONCAT', dbservicepricepackmap.sequelize.col('serviceName')), 'serviceNameList']
                            ],
                        }],
                        raw: true
                    });
                    let serviceAllID = await dbservicepricepackmap.servicepricepackmap.findAll({
                        attributes: {
                            include: [],
                            exclude: [`servicepricepackmapId`, `pricepackId`, `serviceId`, `businessId`, `serviceDuration`, `createDateTime`, `updateDateTime`, `status`]
                        },
                        where: {
                            status: 1,
                            pricepackId: paymentData[i]["subscription.pricepack.pricepackId"]
                        },
                        include: [{
                            model: dbservices.services,
                            attributes: [
                                'serviceId'
                            ],
                        }],
                        raw: true
                    });
                    paymentData[i]["serviceIDList"] = serviceAllID;
                    paymentData[i]["serviceNameList"] = serviceAll;
                    if (serviceAllID) {
                        for (var x = 0; x < serviceAllID.length; x++) {
                            let payble_amount_perPackage = paymentData[i]["subscription.pricepack.amount"];
                            let serviceId_perPackage = serviceAllID[x]["subscriptionsId"];
                            let pricepackId_perPackage = paymentData[i].pricepackId;
                            // // paymentData[i]["getPaymentCalculation"] = await getPaymentCalculation(information, payble_amount_perPackage, serviceId_perPackage, pricepackId_perPackage);
                            // paymentData[i]["getTotalDue"] = await this.getTotalDue(information, paymentData[i].subscriptionsId);
                            // paymentData[i]["getTotalPaid"] = await this.getTotalPaid(information, paymentData[i].subscriptionsId);
                        }
                    }
                }
            }
            if (paymentData.length > 0) {
                for (let x = 0; x < paymentData.length; x++) {
                    paymentData[x]["sessionInfo"] = await sessionInfo(information, {
                        status: 1,
                        subscriptionId: paymentData[x]['subscriptionsId']
                    });
                    if (paymentData[x]["subscription.installment_frequency"] == '6') {
                        let new_data = '';
                        let cal_new_amount = '';
                        if (paymentData[x]["subscription.status_amountPaidSoFar"] == 1) {
                            let date_check = (paymentData[x]['subscription.clientJoiningDate'] != null) ? paymentData[x]['subscription.clientJoiningDate'] : paymentData[x]['subscription.subscriptionDateTime'];
                            cal_new_amount = await this.getTotalShowDataOthers(information, paymentData[x]['subscriptionsId'], date_check);
                            new_data = Number(cal_new_amount[0].new_balance) - Number(paymentData[x]["subscription.amountPaidSoFar"]);
                        } else {
                            let date_check = await hyphenDateFormat(new Date());
                            cal_new_amount = await this.getTotalShowDataOthers(information, paymentData[x]['subscriptionsId'], date_check);
                            new_data = Number(cal_new_amount[0].new_balance);
                        }
                        paymentData[x]["getTotalShowData"] = [{
                            new_balance: new_data
                        }];
                        paymentData[x]["getTotalDue"] = await this.getTotalDue(information, paymentData[x]['subscriptionsId']);
                        let getTotalPaid = await this.getTotalPaid(information, paymentData[x]['subscriptionsId']);
                        if (paymentData[x]["subscription.status_amountPaidSoFar"] == '1') {
                            getTotalPaid[0].total_paid_amount = Number(getTotalPaid[0].total_paid_amount);
                        } else {
                            getTotalPaid[0].total_paid_amount = Number(getTotalPaid[0].total_paid_amount) - Number(paymentData[x]["subscription.amountPaidSoFar"]);
                        }
                        paymentData[x]["getTotalPaid"] = getTotalPaid;
                    } else {
                        let new_data = '';
                        let cal_new_amount = '';
                        if (paymentData[x]["subscription.status_amountPaidSoFar"] == 1) {
                            let date_check = (paymentData[x]['subscription.clientJoiningDate'] != null) ? paymentData[x]['subscription.clientJoiningDate'] : paymentData[x]['subscription.subscriptionDateTime'];
                            cal_new_amount = await this.getTotalShowData(information, paymentData[x]['subscriptionsId'], date_check);
                            new_data = Number(cal_new_amount[0].new_balance) - Number(paymentData[x]["subscription.amountPaidSoFar"]);
                        } else {
                            // let date_check = (paymentData[x]['subscription.clientJoiningDate'] != null) ? paymentData[x]['subscription.clientJoiningDate'] : paymentData[x]['subscription.subscriptionDateTime'];
                            let date_check = paymentData[x]['payment_end_date'];
                            cal_new_amount = await this.getTotalShowData(information, paymentData[x]['subscriptionsId'], date_check);
                            new_data = Number(cal_new_amount[0].new_balance);
                        }
                        paymentData[x]["getTotalShowData"] = [{
                            new_balance: new_data
                        }];
                        paymentData[x]["getTotalDue"] = await this.getTotalDue(information, paymentData[x]['subscriptionsId']);
                        let getTotalPaid = await this.getTotalPaid(information, paymentData[x]['subscriptionsId']);
                        if (paymentData[x]["subscription.status_amountPaidSoFar"] == '1') {
                            getTotalPaid[0].total_paid_amount = Number(getTotalPaid[0].total_paid_amount);
                        } else {
                            getTotalPaid[0].total_paid_amount = Number(getTotalPaid[0].total_paid_amount) - Number(paymentData[x]["subscription.amountPaidSoFar"]);
                        }
                        paymentData[x]["getTotalPaid"] = getTotalPaid;

                    }
                    let listclientPayments = await dbclientpayments.clientpayments.findAll({
                        where: {
                            businessId: businessIdinformation,
                            subscriptionsId: paymentData[x]['subscriptionsId'],
                            status: 1,
                            "$payble_amount$": {
                                ne: null
                            }
                        },
                        raw: true
                    });
                    paymentData[x]["subcription_payment_status"] = listclientPayments.length;
                }
            }
            dbclientpayments.sequelize.close();
            dbClients.sequelize.close();
            dbsubscriptions.sequelize.close();
            dbpricepack.sequelize.close();
            dbpaymentTransactions.sequelize.close();
            dbclientpayments.sequelize.close();
            dbservicepricepackmap.sequelize.close();
            dbservices.sequelize.close();
            total_record.count = paymentData.length;
            total_record.rows = paymentData;
            return total_record;
        } catch (error) {
            throw error;
        }
    };


    getTotalShowData = async(
        information,
        subscriptionsId,
        date
    ) => {
        try {
            let businessIdinformation = information.split(",")[1];
            let dbsubscriptions = createInstance(["clientSubscription"], information);
            let todayDate = await hyphenDateFormat(date);
            // let todayDate = '2018-07-04';
            if (subscriptionsId) {
                console.log("============================================================")
            }
            let data = await dbsubscriptions.sequelize.query(
                "SELECT (SUM(`due_amount`) - CASE WHEN SUM(`payble_amount`) is NULL THEN 0 ELSE SUM(`payble_amount`) END) AS `new_balance` FROM `clientpayments` WHERE `payment_due_date` <= '" + todayDate + "' AND `status` = '1' AND `subscriptionsId` = '" + subscriptionsId + "'", {
                    type: dbsubscriptions.sequelize.QueryTypes.SELECT
                }
            );
            if (subscriptionsId) {
                console.log("============================================================")
            }
            dbsubscriptions.sequelize.close();
            return data;
        } catch (error) {
            throw error;
        }
    };



    getTotalShowDataOthers = async(
        information,
        subscriptionsId,
        date
    ) => {
        try {
            let final = {};
            let finalReturn = [];
            let finalReturnCal = 0;
            var due_amount_local = 0;
            var payble_amount_local = 0;
            let businessIdinformation = information.split(",")[1];
            let dbsubscriptions = createInstance(["clientSubscription"], information);
            let todayDate = await hyphenDateFormat(date);
            let data = await dbsubscriptions.sequelize.query(
                "( SELECT `payment_due_date`, `payment_end_date`, `due_amount`, `payble_amount` FROM `clientpayments` WHERE (`payment_due_date` >= '" + todayDate + "' AND `curent_subcription_number` = 1 ) AND `status` = '1' AND `subscriptionsId` = '" + subscriptionsId + "' ) union ( SELECT `payment_due_date`, `payment_end_date`, `due_amount`, `payble_amount` FROM `clientpayments` WHERE ( `payment_due_date` <= '" + todayDate + "' OR `payment_end_date` <= '" + todayDate + "' ) AND `status` = '1' AND `subscriptionsId` = '" + subscriptionsId + "' )", {
                    type: dbsubscriptions.sequelize.QueryTypes.SELECT
                }
            );
            dbsubscriptions.sequelize.close();
            if (data) {
                for (let x = 0; x < data.length; x++) {
                    due_amount_local += Number(data[x].due_amount);
                    payble_amount_local += Number(data[x].payble_amount);
                }
                finalReturnCal = due_amount_local - payble_amount_local;
            }
            var a = [{
                new_balance: finalReturnCal
            }];
            return a;
        } catch (error) {
            throw error;
        }
    };


    getTotalDue = async(
        information,
        subscriptionsId
    ) => {
        try {

            let businessIdinformation = information.split(",")[1];
            let dbsubscriptions = createInstance(["clientSubscription"], information);
            let todayDate = await hyphenDateFormat(new Date);
            let data = await dbsubscriptions.sequelize.query(
                "SELECT SUM(`due_amount`) as `total_current_due` FROM `clientpayments` WHERE `subscriptionsId` = '" + subscriptionsId + "' AND `paymentStatus` = '1' AND ( ( `payment_due_date` = '" + todayDate + "' AND `payment_end_date` = '" + todayDate + "' ) OR ( `payment_due_date` <= '" + todayDate + "' ) )", {
                    type: dbsubscriptions.sequelize.QueryTypes.SELECT
                }
            );
            dbsubscriptions.sequelize.close();
            return data;
        } catch (error) {
            throw error;
        }
    };


    getTotalPaid = async(
        information,
        subscriptionsId
    ) => {
        try {
            let businessIdinformation = information.split(",")[1];
            let dbsubscriptions = createInstance(["clientSubscription"], information);
            let data = await dbsubscriptions.sequelize.query(
                "SELECT SUM(`payble_amount`) as `total_paid_amount` FROM `clientpayments` WHERE `subscriptionsId` = '" + subscriptionsId + "'", {
                    type: dbsubscriptions.sequelize.QueryTypes.SELECT
                }
            );
            dbsubscriptions.sequelize.close();
            return data;
        } catch (error) {
            throw error;
        }
    };

    userClientManagementDashboard = async(
        information,
        filterPrivateFields = true
    ) => {
        try {
            let final = [];
            let businessIdinformation = information.split(",")[1];
            let dbclientpayments = createInstance(["clientPayments"], information);
            let dbsubscriptions = createInstance(["clientSubscription"], information);
            let dbClients = createInstance(["client"], information);
            let dbpricepacks = createInstance(["clientPricepack"], information);
            let dbpaymentTransactions = createInstance(["paymentTransactions"], information);
            let dbservicepricepackmap = createInstance(["servicePricepackMap"], information);
            let dbservices = createInstance(["clientService"], information);
            let dbclientschedulemap = createInstance(["clientScheduleMap"], information);
            let todayDate = await hyphenDateFormat(new Date);

            dbclientpayments.clientpayments.belongsTo(dbsubscriptions.subscriptions, {
                foreignKey: "subscriptionsId",
                targetKey: "subscriptionId"
            });
            dbsubscriptions.subscriptions.belongsTo(dbClients.clients, {
                foreignKey: "clientId",
                targetKey: "clientId"
            });
            dbClients.clients.belongsTo(dbsubscriptions.subscriptions, {
                foreignKey: "clientId",
                targetKey: "clientId"
            });
            dbsubscriptions.subscriptions.belongsTo(dbpricepacks.pricepacks, {
                foreignKey: "pricepacks",
                targetKey: "pricepackId"
            });
            dbclientpayments.clientpayments.belongsTo(dbsubscriptions.subscriptions, {
                foreignKey: "subscriptionsId",
                targetKey: "subscriptionId"
            });
            dbsubscriptions.subscriptions.belongsTo(dbClients.clients, {
                foreignKey: "clientId",
                targetKey: "clientId"
            });
            dbsubscriptions.subscriptions.belongsTo(dbpricepacks.pricepacks, {
                foreignKey: "pricepacks",
                targetKey: "pricepackId"
            });
            dbclientpayments.clientpayments.belongsTo(dbpaymentTransactions.paymentTransactions, {
                foreignKey: "clientpaymentId",
                targetKey: "clientpaymentId"
            });
            dbservicepricepackmap.servicepricepackmap.belongsTo(dbservices.services, {
                foreignKey: "serviceId",
                targetKey: "serviceId"
            });

            dbsubscriptions.subscriptions.belongsTo(dbclientschedulemap.clientschedulemap, {
                foreignKey: "subscriptionId",
                targetKey: "subscriptionId"
            });


            // =================================lead client List start ==================================================
            let newLeadsClientListCount = await dbClients.clients.findAll({
                attributes: [
                    [dbClients.sequelize.fn("COUNT", dbClients.sequelize.col("clientId")), "count"]
                ],
                where: {
                    businessId: businessIdinformation,
                    status: 1,
                    clientId: {
                        notIn: dbClients.sequelize.literal("( SELECT `clientId` FROM `subscriptions` AS `subscriptions` WHERE `subscriptions`.`status` = 1 )")
                    }
                },
                raw: true
            });
            let newLeadsClientList = await dbClients.clients.findAll({
                attributes: {
                    include: [
                        [dbClients.clients.sequelize.literal('CASE WHEN photoUrl  is NULL THEN CONCAT("' + KNACK_UPLOAD_URL.localUrl + '" , "noimage.jpg" ) ELSE CONCAT("' + KNACK_UPLOAD_URL.localUrl + '" , photoUrl ) END'), 'photoFullUrl'],
                        [dbClients.sequelize.fn('SUBSTRING', dbClients.sequelize.fn('SOUNDEX', dbClients.sequelize.col('clientName')), 1, 1), 'name_initials']
                    ],
                    exclude: []
                },
                order: [
                    ["updateDateTime", "DESC"]
                ],
                where: {
                    businessId: businessIdinformation,
                    status: 1,
                    clientId: {
                        notIn: dbClients.sequelize.literal("( SELECT `clientId` FROM `subscriptions` AS `subscriptions` WHERE `subscriptions`.`status` = 1 )")
                    }
                },
                offset: 0,
                limit: 3,
                raw: true
            });
            let new_leads = {
                "count": newLeadsClientListCount[0].count,
                "rows": newLeadsClientList
            }
            final.push({
                "new_leads": new_leads
            });

            // =================================lead client List end ==================================================


            // =================================renewals client List start ==================================================

            let itemConditionsclientpayments = {
                status: 1,
                // paymentStatus: 1,
                is_last_payment: 1,
                businessId: businessIdinformation,
                // [dbClients.sequelize.Op.and]: [Sequelize.literal("( ( `subscription->clientschedulemap`.`clientScheduleMapId` IS NOT NULL OR `subscription->clientschedulemap`.`sessionCount` IS NOT NULL ) OR ( `subscription->clientschedulemap`.`sessionCount` = `subscription->pricepack`.`serviceDuration` ) )")],
                // [dbClients.sequelize.Op.and]: [Sequelize.literal("(`subscription->clientschedulemap`.`sessionCount` = `subscription->pricepack`.`serviceDuration` )")],
                [dbClients.sequelize.Op.and]: [Sequelize.literal("(DATE( Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day )) < '" + todayDate + "') OR (`subscription->clientschedulemap`.`sessionCount` = `subscription->pricepack`.`serviceDuration` )")],
            };
            let itemGroupConditions = {
                status: 1,
                businessId: businessIdinformation,
            };
            let itemGroupConditionsPricePack = {};
            let itemConditions = {
                renewl_status: 1
            };

            let paymentData = await dbclientpayments.clientpayments.findAndCount({
                attributes: [
                    "clientpaymentId",
                    "subscriptionsId",
                    "due_amount",
                    "payble_amount",
                    "payment_due_date",
                    "payment_end_date", ["UpdateDateTime", "updated_at"],
                    [
                        dbClients.clients.sequelize.literal(
                            'DATE(Date_add( `subscription`.`subscriptiondatetime`,INTERVAL `subscription->pricepack`.`pricepackvalidity` day))'
                        ),
                        "subcription_end_Date"
                    ],
                    [
                        dbclientpayments.sequelize.literal("( SELECT IFNULL( SUM(`payble_amount`), 0 ) FROM `clientpayments` INNER join `paymentTransactions` AS `paymentTransactions` ON `clientpayments`.`clientpaymentId` = `paymentTransactions`.`clientpaymentId` WHERE `clientpayments`.`subscriptionsId` = `subscription`.`subscriptionId` )"),
                        "total_subcription_payemt"
                    ]
                ],
                include: [{
                    model: dbsubscriptions.subscriptions,
                    attributes: ["clientId", "status_amountPaidSoFar", "amountPaidSoFar"],
                    include: [{
                            model: dbClients.clients,
                            attributes: {
                                include: [
                                    "clientName", [
                                        dbClients.clients.sequelize.literal(
                                            'CASE WHEN photoUrl  is NULL THEN CONCAT("' +
                                            KNACK_UPLOAD_URL.localUrl +
                                            '" , "noimage.jpg" ) ELSE CONCAT("' +
                                            KNACK_UPLOAD_URL.localUrl +
                                            '" , photoUrl ) END'
                                        ),
                                        "photoUrl"
                                    ],
                                ],
                                exclude: []
                            },
                            where: itemGroupConditions
                        },
                        {
                            model: dbpricepacks.pricepacks,
                            attributes: ["pricepackName", "amount", "serviceDuration"],
                            where: itemGroupConditionsPricePack,
                        },
                        {
                            model: dbclientschedulemap.clientschedulemap,
                            attributes: ["scheduleId", "sessionCount"],
                            require: false,
                            where: {
                                // [dbclientschedulemap.sequelize.Op.and]: [Sequelize.literal("`subscription->clientschedulemap`.`scheduleId`= `subscription->clientschedulemap`.`parentScheduleId`")],
                                [dbclientschedulemap.sequelize.Op.and]: [Sequelize.literal("( `subscription->clientschedulemap`.`scheduleId` = `subscription->clientschedulemap`.`parentScheduleId` ) AND ( `subscription->clientschedulemap`.`scheduleId` = (select min(`scheduleId`) from `clientschedulemap` `inSched` where `inSched`.`subscriptionId` = `subscription`.`subscriptionId` ) )")],
                            }
                        }
                    ],
                    where: itemConditions
                }],
                order: orderBy,
                where: itemConditionsclientpayments,
                subQuery: false,
                offset: 0,
                limit: 3,
                raw: true
            });
            if (paymentData.rows) {
                for (var i = 0; i < paymentData.rows.length; i++) {
                    let serviceAll = await dbservicepricepackmap.servicepricepackmap.findAll({
                        attributes: {
                            include: [],
                            exclude: [`servicepricepackmapId`, `serviceDuration_number`, `pricepackId`, `serviceId`, `businessId`, `serviceDuration`, `createDateTime`, `updateDateTime`, `status`]
                        },
                        where: {
                            status: 1,
                            pricepackId: paymentData.rows[i]["subscription.pricepack.pricepackId"]
                        },
                        include: [{
                            model: dbservices.services,
                            attributes: [
                                [dbservicepricepackmap.sequelize.fn('GROUP_CONCAT', dbservicepricepackmap.sequelize.col('serviceName')), 'serviceNameList']
                            ],
                        }],
                        raw: true
                    });
                    let serviceAllID = await dbservicepricepackmap.servicepricepackmap.findAll({
                        attributes: {
                            include: [],
                            exclude: [`servicepricepackmapId`, `pricepackId`, `serviceId`, `businessId`, `serviceDuration`, `createDateTime`, `updateDateTime`, `status`]
                        },
                        where: {
                            status: 1,
                            pricepackId: paymentData.rows[i]["subscription.pricepack.pricepackId"]
                        },
                        include: [{
                            model: dbservices.services,
                            attributes: [
                                'serviceId'
                            ],
                        }],
                        raw: true
                    });
                    paymentData.rows[i]["serviceIDList"] = serviceAllID;
                    paymentData.rows[i]["serviceNameList"] = serviceAll;
                    if (serviceAllID) {
                        for (var x = 0; x < serviceAllID.length; x++) {
                            let payble_amount_perPackage = paymentData.rows[i]["subscription.pricepack.amount"];
                            let serviceId_perPackage = serviceAllID[x]["service.serviceId"];
                            let pricepackId_perPackage = paymentData.rows[i].pricepackId;
                            paymentData.rows[i]["getPaymentCalculation"] = await getPaymentCalculation(information, payble_amount_perPackage, serviceId_perPackage, pricepackId_perPackage);
                        }
                    }
                    paymentData.rows[i]["sessionInfo"] = await sessionInfo(information, {
                        status: 1,
                        subscriptionId: paymentData.rows[i]['subscriptionsId']
                    });

                }
            }
            if (paymentData.rows.length > 0) {
                for (let x = 0; x < paymentData.rows.length; x++) {
                    paymentData.rows[x]["sessionInfo"] = await sessionInfo(information, {
                        status: 1,
                        subscriptionId: paymentData.rows[x]['subscriptionsId']
                    });
                    if (paymentData.rows[x]["subscription.installment_frequency"] == '6') {
                        let new_data = '';
                        let cal_new_amount = '';
                        if (paymentData.rows[x]["subscription.status_amountPaidSoFar"] == 1) {
                            let date_check = (paymentData.rows[x]['subscription.clientJoiningDate'] != null) ? paymentData.rows[x]['subscription.clientJoiningDate'] : paymentData.rows[x]['subscription.subscriptionDateTime'];
                            cal_new_amount = await this.getTotalShowDataOthers(information, paymentData.rows[x]['subscriptionsId'], date_check);
                            new_data = Number(cal_new_amount[0].new_balance) - Number(paymentData.rows[x]["subscription.amountPaidSoFar"]);
                        } else {
                            let date_check = await hyphenDateFormat(new Date());
                            cal_new_amount = await this.getTotalShowDataOthers(information, paymentData.rows[x]['subscriptionsId'], date_check);
                            new_data = Number(cal_new_amount[0].new_balance);
                        }
                        paymentData.rows[x]["getTotalShowData"] = [{
                            new_balance: new_data
                        }];
                        paymentData.rows[x]["getTotalDue"] = await this.getTotalDue(information, paymentData.rows[x]['subscriptionsId']);
                        let getTotalPaid = await this.getTotalPaid(information, paymentData.rows[x]['subscriptionsId']);
                        if (paymentData.rows[x]["subscription.status_amountPaidSoFar"] == '1') {
                            getTotalPaid[0].total_paid_amount = Number(getTotalPaid[0].total_paid_amount);
                        } else {
                            getTotalPaid[0].total_paid_amount = Number(getTotalPaid[0].total_paid_amount) - Number(paymentData.rows[x]["subscription.amountPaidSoFar"]);
                        }
                        paymentData.rows[x]["getTotalPaid"] = getTotalPaid;
                    } else {
                        let new_data = '';
                        let cal_new_amount = '';
                        if (paymentData.rows[x]["subscription.status_amountPaidSoFar"] == 1) {
                            let date_check = (paymentData.rows[x]['subscription.clientJoiningDate'] != null) ? paymentData.rows[x]['subscription.clientJoiningDate'] : paymentData.rows[x]['subscription.subscriptionDateTime'];
                            cal_new_amount = await this.getTotalShowData(information, paymentData.rows[x]['subscriptionsId'], date_check);
                            new_data = Number(cal_new_amount[0].new_balance) - Number(paymentData.rows[x]["subscription.amountPaidSoFar"]);
                        } else {
                            // let date_check = (paymentData.rows[x]['subscription.clientJoiningDate'] != null) ? paymentData.rows[x]['subscription.clientJoiningDate'] : paymentData.rows[x]['subscription.subscriptionDateTime'];
                            let date_check = paymentData.rows[x]['payment_end_date'];
                            cal_new_amount = await this.getTotalShowData(information, paymentData.rows[x]['subscriptionsId'], date_check);
                            new_data = Number(cal_new_amount[0].new_balance);
                        }
                        paymentData.rows[x]["getTotalShowData"] = [{
                            new_balance: new_data
                        }];
                        paymentData.rows[x]["getTotalDue"] = await this.getTotalDue(information, paymentData.rows[x]['subscriptionsId']);
                        let getTotalPaid = await this.getTotalPaid(information, paymentData.rows[x]['subscriptionsId']);
                        if (paymentData.rows[x]["subscription.status_amountPaidSoFar"] == '1') {
                            getTotalPaid[0].total_paid_amount = Number(getTotalPaid[0].total_paid_amount);
                        } else {
                            getTotalPaid[0].total_paid_amount = Number(getTotalPaid[0].total_paid_amount) - Number(paymentData.rows[x]["subscription.amountPaidSoFar"]);
                        }
                        paymentData.rows[x]["getTotalPaid"] = getTotalPaid;
                    }
                    let listclientPayments = await dbclientpayments.clientpayments.findAll({
                        where: {
                            businessId: businessIdinformation,
                            subscriptionsId: paymentData.rows[x]['subscriptionsId'],
                            status: 1,
                            "$payble_amount$": {
                                ne: null
                            }
                        },
                        raw: true
                    });
                    paymentData.rows[x]["subcription_payment_status"] = listclientPayments.length;
                }
            }

            //================== PAST SUBSCRIPTION COUNT CAL Start=======================







            //================== PAST SUBSCRIPTION COUNT CAL End=======================
            let client_renewals = {
                "count": paymentData.count,
                "rows": paymentData.rows
            }
            final.push({
                "client_renewals": client_renewals
            });
            // =================================renewals client List end ==================================================


            // =================================top client List start ==================================================

            let todayFormat = await date_ISO_Convert(new Date());
            // let todayFormat = "2018-08-13";
            let orderBy = " ORDER BY `total_paid_amount` DESC";
            let limitPerPage = 3;
            let offsetLimit = 0;
            let activeClientListCount = await dbsubscriptions.sequelize.query(
                "SELECT `clients`.*, CASE WHEN photoUrl is NULL THEN CONCAT('" + KNACK_UPLOAD_URL.localUrl + "' , 'noimage.jpg' ) ELSE CONCAT('" + KNACK_UPLOAD_URL.localUrl + "' , photoUrl ) END AS `photoFullUrl`, `subscriptions`.`subscriptionId` , (SELECT SUM(`payble_amount`)  FROM `clientpayments` WHERE `subscriptionsId` = `subscriptions`.`subscriptionId`) as `total_paid_amount`, (SELECT (SUM(`due_amount`) - SUM(`payble_amount`))  FROM `clientpayments` WHERE `subscriptionsId` = `subscriptions`.`subscriptionId`) as `total_pending_amount`  FROM `clients` LEFT JOIN `subscriptions` AS `subscriptions` ON `subscriptions`.`clientid` = `clients`.`clientid` INNER JOIN `pricepacks` AS `pricepack` ON `subscriptions`.`pricepacks` = `pricepack`.`pricepackid` WHERE `clients`.`status` = 1 AND `clients`.`businessId` = '" + businessIdinformation + "' AND '" + todayFormat + "' BETWEEN `subscriptiondatetime` AND Date_add( `subscriptions`.`subscriptiondatetime`,INTERVAL `pricepack`.`pricepackvalidity` day) HAVING `total_paid_amount` IS NOT NULL AND `total_pending_amount` IS NOT NULL ", {
                    type: dbsubscriptions.sequelize.QueryTypes.SELECT
                }
            );
            let activeClientList = await dbsubscriptions.sequelize.query(
                "SELECT `clients`.*, CASE WHEN photoUrl is NULL THEN CONCAT('" + KNACK_UPLOAD_URL.localUrl + "' , 'noimage.jpg' ) ELSE CONCAT('" + KNACK_UPLOAD_URL.localUrl + "' , photoUrl ) END AS `photoFullUrl`, `subscriptions`.`subscriptionId` , (SELECT SUM(`payble_amount`)  FROM `clientpayments` WHERE `subscriptionsId` = `subscriptions`.`subscriptionId`) as `total_paid_amount`, (SELECT (SUM(`due_amount`) - SUM(`payble_amount`))  FROM `clientpayments` WHERE `subscriptionsId` = `subscriptions`.`subscriptionId`) as `total_pending_amount`  FROM `clients` LEFT JOIN `subscriptions` AS `subscriptions` ON `subscriptions`.`clientid` = `clients`.`clientid` INNER JOIN `pricepacks` AS `pricepack` ON `subscriptions`.`pricepacks` = `pricepack`.`pricepackid` WHERE `clients`.`status` = 1 AND `clients`.`businessId` = '" + businessIdinformation + "' AND '" + todayFormat + "' BETWEEN `subscriptiondatetime` AND Date_add( `subscriptions`.`subscriptiondatetime`,INTERVAL `pricepack`.`pricepackvalidity` day) HAVING `total_paid_amount` IS NOT NULL AND `total_pending_amount` IS NOT NULL " + orderBy + " LIMIT " +
                limitPerPage +
                " OFFSET " +
                offsetLimit, {
                    type: dbsubscriptions.sequelize.QueryTypes.SELECT
                }
            );
            let top_clients = {
                "count": activeClientListCount.length,
                "rows": activeClientList
            };
            final.push({
                "top_clients": top_clients
            });

            // =================================top client List end ==================================================

            // =================================active client List start ==================================================
            let orderBy_active_client = "";
            let limitPerPage_active_client = 3;
            let offsetLimit_active_client = 0;
            // let activeClientListCount_new = await dbsubscriptions.sequelize.query(
            //     "SELECT COUNT(*) AS count FROM `clients` WHERE `clientid` IN ( SELECT DISTINCT(`clientid`) FROM `subscriptions` INNER JOIN `pricepacks` AS `pricepack` ON `subscriptions`.`pricepacks` = `pricepack`.`pricepackid` WHERE '" + todayFormat + "' BETWEEN `subscriptiondatetime` AND Date_add( `subscriptions`.`subscriptiondatetime`,INTERVAL `pricepack`.`pricepackvalidity` day)) AND `clients`.`status` = 1 AND `clients`.`businessId` = '" + businessIdinformation + "' " + orderBy_active_client, {
            //         type: dbsubscriptions.sequelize.QueryTypes.SELECT
            //     }
            // );
            // let activeClientList_new = await dbsubscriptions.sequelize.query(
            //     "SELECT *,CASE WHEN photoUrl  is NULL THEN CONCAT('" + KNACK_UPLOAD_URL.localUrl + "' , 'noimage.jpg' ) ELSE CONCAT('" + KNACK_UPLOAD_URL.localUrl + "' , photoUrl ) END AS `photoFullUrl` FROM `clients` WHERE `clientid` IN ( SELECT DISTINCT(`clientid`) FROM `subscriptions` INNER JOIN `pricepacks` AS `pricepack` ON `subscriptions`.`pricepacks` = `pricepack`.`pricepackid` WHERE '" + todayFormat + "' BETWEEN `subscriptiondatetime` AND Date_add( `subscriptions`.`subscriptiondatetime`,INTERVAL `pricepack`.`pricepackvalidity` day)) AND `clients`.`status` = 1 AND `clients`.`businessId` = '" + businessIdinformation + "' " + orderBy_active_client + " LIMIT " +
            //     limitPerPage_active_client +
            //     " OFFSET " +
            //     offsetLimit_active_client, {
            //         type: dbsubscriptions.sequelize.QueryTypes.SELECT
            //     }
            // );


            let clientpaymentsRiminder = {};
            let itemclientpaymentsRiminderData = {
                status: 1,
                businessId: businessIdinformation,
                clientId: {
                    [dbClients.sequelize.Op.in]: [Sequelize.literal("( SELECT `subscription`.`clientId` AS `subscription.clientId` FROM `clientpayments` AS `clientpayments` INNER JOIN `subscriptions` AS `subscription` ON `clientpayments`.`subscriptionsId` = `subscription`.`subscriptionId` INNER JOIN `clients` AS `subscription->client` ON `subscription`.`clientId` = `subscription->client`.`clientId` AND `subscription->client`.`status` = 1 AND `subscription->client`.`businessId` = '" + businessIdinformation + "' INNER JOIN `pricepacks` AS `subscription->pricepack` ON `subscription`.`pricepacks` = `subscription->pricepack`.`pricepackId` LEFT OUTER JOIN `clientschedulemap` AS `subscription->clientschedulemap` ON `subscription`.`subscriptionId` = `subscription->clientschedulemap`.`subscriptionId` AND ( ( `subscription->clientschedulemap`.`scheduleId` = `subscription->clientschedulemap`.`parentScheduleId` ) AND ( `subscription->clientschedulemap`.`scheduleId` = ( select min(`scheduleId`) from `clientschedulemap` `inSched` where `inSched`.`subscriptionId` = `subscription`.`subscriptionId` ) ) ) WHERE ( `subscription->client`.`clientName` LIKE '%%' OR `subscription->pricepack`.`pricepackName` LIKE '%%' ) AND ( ( ( `subscription->clientschedulemap`.`clientScheduleMapId` IS NULL OR `subscription->clientschedulemap`.`sessionCount` IS NULL ) OR ( `subscription->clientschedulemap`.`sessionCount` < `subscription->pricepack`.`serviceDuration` ) ) ) AND `clientpayments`.`status` = 1 AND `clientpayments`.`businessId` = '" + businessIdinformation + "' AND `clientpayments`.`clientpaymentId` IN ( ( SELECT MIN(`clientpaymentId`) as `clientpaymentId` FROM `clientpayments` INNER JOIN `subscriptions` AS `subscription` ON `clientpayments`.`subscriptionsId` = `subscription`.`subscriptionId` INNER JOIN `pricepacks` AS `subscription->pricepack` ON `subscription`.`pricepacks` = `subscription->pricepack`.`pricepackId` WHERE ( DATE( Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day ) ) >= '" + todayDate + "' ) AND ( ( `clientpayments`.`payment_due_date` <= '" + todayDate + "' AND `clientpayments`.`payment_end_date` >= '" + todayDate + "' ) OR ( `clientpayments`.`payment_due_date` >= '" + todayDate + "' AND `clientpayments`.`payment_end_date` >= '" + todayDate + "' ) ) AND `clientpayments`.`status` = '1' GROUP BY `clientpayments`.`subscriptionsId` UNION select c.clientpaymentId from clientpayments as c, ( SELECT a.clientpaymentId, a.curent_subcription_number, a.payment_end_date, COUNT(a.subscriptionsId) FROM clientpayments a LEFT JOIN clientpayments b ON a.subscriptionsId = b.subscriptionsId AND b.status = 1 WHERE b.status = 1 AND a.status = 1 GROUP BY a.subscriptionsId, a.clientpaymentId having a.curent_subcription_number = COUNT(a.subscriptionsId) AND a.payment_end_date < CURDATE() ) as total WHERE ( DATE( Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day ) ) >= '" + todayDate + "' ) AND c.clientpaymentId IN (total.clientpaymentId) ) ) GROUP BY `subscription.clientId` )")]
                },
            };
            let clientpaymentsRiminderData = await dbClients.clients.findAndCount({
                attributes: [
                    ["clientId", "subscription.clientId"],
                    ["clientId", "subscription.client.clientId"],
                    ["clientName", "subscription.client.clientName"],
                    ["contactNumber", "subscription.client.contactNumber"],
                    [
                        dbClients.clients.sequelize.literal(
                            'CASE WHEN photoUrl  is NULL THEN CONCAT("' +
                            KNACK_UPLOAD_URL.localUrl +
                            '" , "noimage.jpg" ) ELSE CONCAT("' +
                            KNACK_UPLOAD_URL.localUrl +
                            '" , photoUrl ) END'
                        ),
                        "subscription.client.photoUrl"
                    ]
                ],
                order: [
                    ["CreateDateTime", "DESC"]
                ],
                where: itemclientpaymentsRiminderData,
                offset: offsetLimit_active_client,
                limit: limitPerPage_active_client,
                raw: true
            });
            clientpaymentsRiminder.count = clientpaymentsRiminderData.count;
            clientpaymentsRiminder.rows = clientpaymentsRiminderData.rows;
            let active_clients = {
                "count": clientpaymentsRiminderData.count,
                "rows": clientpaymentsRiminderData.rows
            };
            final.push({
                "active_clients": active_clients
            });
            // =================================active client List end ==================================================

            let clients = await dbClients.clients.findAll({
                order: [
                    ["clientName", "asc"]
                ],
                where: {
                    businessId: businessIdinformation,
                    status: 1
                },
                raw: true
            });
            final.push({
                "all_clients": clients.length
            });
            dbclientpayments.sequelize.close();
            dbsubscriptions.sequelize.close();
            dbClients.sequelize.close();
            dbpricepacks.sequelize.close();
            dbpaymentTransactions.sequelize.close();
            dbservicepricepackmap.sequelize.close();
            dbservices.sequelize.close();
            dbclientschedulemap.sequelize.close();

            return final;
        } catch (error) {
            throw error;
        }
    };

    createClientMultiple = async(information, businessId, clientData) => {
        try {
            let totalclient = clientData.length;
            let counter = 0;
            let dbClient = createInstance(["client"], information);
            clientData.forEach(function(client, index, arr) {
                dbClient.clients.create({
                    businessId: businessId,
                    clientName: client.clientName,
                    contactNumber: client.contactNumber
                }).then(() => {
                    counter++;
                    if (totalclient === counter) {
                        dbClient.sequelize.close();
                    }
                });
            })
        } catch (error) {
            throw error;
        }
    };


    listClientOnServiceAndSchedule = async(information, serviceId, scheduleId, page, param, filterPrivateFields = true) => {
        try {
            var businessIdinformation = information.split(",")[1];
            let filter = (param[0]) ? param[0] : '';
            var pageNo = (page) ? page : 1;
            var limitPerPage = PEGINATION_LIMIT;
            var offsetLimit = (pageNo - 1) * limitPerPage;
            let dbClients = createInstance(["client"], information);
            let dbClientSchedule = createInstance(["clientScheduleMap"], information);
            let dbSPM = createInstance(["servicePricepackMap"], information);
            let dbPP = createInstance(["clientPricepack"], information);
            let dbSubscription = createInstance(["clientSubscription"], information);

            dbSPM.servicepricepackmap.belongsTo(dbPP.pricepacks, {
                foreignKey: "pricepackId",
                targetKey: "pricepackId"
            });
            dbPP.pricepacks.hasOne(dbSubscription.subscriptions, {
                foreignKey: "pricepacks",
                targetKey: "pricepackId"
            });
            dbSubscription.subscriptions.belongsTo(dbClients.clients, {
                foreignKey: "clientId",
                targetKey: "clientId"
            });


            dbSubscription.subscriptions.hasOne(dbClientSchedule.clientschedulemap, {
                foreignKey: "subscriptionId",
                targetKey: "subscriptionId"
            });

            let services = await dbSPM.servicepricepackmap.findAndCount({

                attributes: ["servicepricepackmapId", "serviceId", "businessId"],
                include: [{
                    model: dbPP.pricepacks,

                    attributes: ["pricepackId", "pricepackName"],
                    required: true,

                    include: [{
                        model: dbSubscription.subscriptions,

                        attributes: ["subscriptionId"],

                        required: true,

                        where: {
                            status: 1
                        },
                        include: [{
                            model: dbClients.clients,

                            attributes: ["clientName", "contactNumber", "emailId", "photoUrl"],

                            where: {
                                clientName: {
                                    like: '%' + filter + '%'
                                }

                            }

                        }, ],

                    }]
                }],
                where: {
                    status: 1,
                    serviceId: serviceId,
                    businessId: businessIdinformation
                },
                raw: true,
                paranoid: false,
                required: true,
                offset: offsetLimit,
                limit: limitPerPage,

            });


            for (let i = 0; i < services["rows"].length; i++) {
                let checkSchedule = await dbClientSchedule.clientschedulemap.findAll({
                    where: {
                        status: 1,
                        clientId: services["rows"][i]["pricepack.subscription.client.clientId"]
                    },
                    raw: true
                });


                let arr = [];

                let checkStat = 0;
                for (let j = 0; j < checkSchedule.length; j++) {
                    let Obj = {};
                    if (checkSchedule[j]["scheduleId"] == scheduleId) {
                        Obj["id"] = checkSchedule[j]["scheduleId"];
                        checkStat = 1;
                        // Obj["isAdded"]=1;
                        arr.push(Obj);

                    } else {
                        Obj["id"] = checkSchedule[j]["scheduleId"];
                        // Obj["isAdded"]=0;
                        arr.push(Obj);
                    }
                }
                services["rows"][i]["name_initials"] = services["rows"][i]["pricepack.subscription.client.clientName"].split("")[0].toUpperCase();
                if (checkStat == 1) services["rows"][i]["isAdded"] = 1;

                else services["rows"][i]["isAdded"] = 0;


                services["rows"][i]["Schedules"] = arr;


                var isphoto = (services["rows"][i]["pricepack.subscription.client.photoUrl"] === null) ? 'noimage.jpg' :
                    services["rows"][i]["pricepack.subscription.client.photoUrl"];

                services["rows"][i]["pricepack.subscription.client.photoUrl"] = KNACK_UPLOAD_URL.localUrl + isphoto;

            }
            dbSPM.sequelize.close();
            dbClients.sequelize.close();
            dbClientSchedule.sequelize.close();
            dbSPM.sequelize.close();
            dbPP.sequelize.close();
            dbSubscription.sequelize.close();

            services["rows"].sort(function(a, b) {

                return a.name_initials.localeCompare(b.name_initials);
            });

            return services;

        } catch (error) {
            throw error;
        }
    };

    listClientOnService = async(information, serviceId, page, param, filterPrivateFields = true) => {
        try {
            var businessIdinformation = information.split(",")[1];
            let filter = (param[0]) ? param[0] : '';
            var pageNo = (page) ? page : 1;
            var limitPerPage = PEGINATION_LIMIT;
            var offsetLimit = (pageNo - 1) * limitPerPage;
            let dbClients = createInstance(["client"], information);
            let dbClientSchedule = createInstance(["clientScheduleMap"], information);
            let dbSPM = createInstance(["servicePricepackMap"], information);
            let dbPP = createInstance(["clientPricepack"], information);
            let dbSubscription = createInstance(["clientSubscription"], information);

            dbSPM.servicepricepackmap.belongsTo(dbPP.pricepacks, {
                foreignKey: "pricepackId",
                targetKey: "pricepackId"
            });
            dbPP.pricepacks.hasOne(dbSubscription.subscriptions, {
                foreignKey: "pricepacks",
                targetKey: "pricepackId"
            });
            dbSubscription.subscriptions.belongsTo(dbClients.clients, {
                foreignKey: "clientId",
                targetKey: "clientId"
            });


            dbSubscription.subscriptions.hasOne(dbClientSchedule.clientschedulemap, {
                foreignKey: "subscriptionId",
                targetKey: "subscriptionId"
            });

            let services = await dbSPM.servicepricepackmap.findAndCount({

                attributes: ["servicepricepackmapId", "serviceId", "businessId"],
                include: [{
                    model: dbPP.pricepacks,

                    attributes: ["pricepackId", "pricepackName"],
                    required: true,

                    include: [{
                        model: dbSubscription.subscriptions,

                        attributes: ["subscriptionId"],

                        required: true,

                        where: {
                            status: 1
                        },
                        include: [{
                            model: dbClients.clients,

                            attributes: ["clientName", "contactNumber", "emailId", "photoUrl"],

                            where: {
                                clientName: {
                                    like: '%' + filter + '%'
                                }

                            }

                        }, ],

                    }]
                }],
                where: {
                    status: 1,
                    serviceId: serviceId,
                    businessId: businessIdinformation
                },
                raw: true,
                paranoid: false,
                required: true,
                offset: offsetLimit,
                limit: limitPerPage,

            });


            for (let i = 0; i < services["rows"].length; i++) {
                let checkSchedule = await dbClientSchedule.clientschedulemap.findAll({
                    where: {
                        status: 1,
                        clientId: services["rows"][i]["pricepack.subscription.client.clientId"]
                    },
                    raw: true
                });


                let arr = [];

                let checkStat = 0;

                services["rows"][i]["name_initials"] = services["rows"][i]["pricepack.subscription.client.clientName"].split("")[0].toUpperCase();
                if (checkStat == 1) services["rows"][i]["isAdded"] = 1;

                else services["rows"][i]["isAdded"] = 0;


                services["rows"][i]["Schedules"] = arr;


                var isphoto = (services["rows"][i]["pricepack.subscription.client.photoUrl"] === null) ? 'noimage.jpg' :
                    services["rows"][i]["pricepack.subscription.client.photoUrl"];

                services["rows"][i]["pricepack.subscription.client.photoUrl"] = KNACK_UPLOAD_URL.localUrl + isphoto;

            }
            dbSPM.sequelize.close();
            dbClients.sequelize.close();
            dbClientSchedule.sequelize.close();
            dbSPM.sequelize.close();
            dbPP.sequelize.close();
            dbSubscription.sequelize.close();

            services["rows"].sort(function(a, b) {

                return a.name_initials.localeCompare(b.name_initials);
            });

            return services;

        } catch (error) {
            throw error;
        }
    };

    csvUpload = async(information, businessId, csvUrl) => {
        try {
            if (csvUrl) {
                var filePath = './uploads/clients/' + csvUrl.name + Date.now();
                await csvUrl.mv(filePath, function(err) {
                    if (err) {
                        throw error;
                    }
                });
                let clientData = [];
                let dbClient = createInstance(["client"], information);
                csv.fromPath(filePath)
                    .on("data", function(data) {
                        clientData.push(data); // push each row
                    })
                    .on("end", function() {
                        let counter = 0;
                        let totalclient = clientData.length;
                        clientData.forEach(function(client, index, arr) {
                            if (index > 0) {
                                dbClient.clients.create({
                                    businessId: businessId,
                                    clientName: client[0],
                                    contactNumber: client[1],
                                    emailId: client[2],
                                    alternateNumber: client[3],
                                    alternateEmail: client[4],
                                    pin: client[5],
                                    area: client[6],
                                    dateOfBirth: client[7]
                                }).then(() => {
                                    counter++;
                                    if (totalclient === (counter + 1)) {
                                        fs.unlinkSync(filePath);
                                        dbClient.sequelize.close();
                                    }
                                });
                            }
                        })

                    })

            } else {
                return null;
            }
        } catch (error) {
            throw error;
        }
    };

}



function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}
var inArray = function(needle, haystack) {
    var length = haystack.length;
    for (var i = 0; i < length; i++) {
        if (haystack[i] == needle) return true;
    }
    return false;
}
export default new ClientDB();