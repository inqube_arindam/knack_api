import {
    filterFields,
    createInstance,
    makeEmailTemplate,
    numberWithCommas,
    sessionInfo,
    hyphenDateFormat,
    getAmountPercentage
} from "../../lib/common";
import {
    KNACKDB,
    KNACK_UPLOAD_URL,
    PEGINATION_LIMIT
} from "../../lib/constants.js";
import { ApplicationError } from "../../lib/errors";

export class ExpenseDB {
    expense = [];

    addExpense = async(
        information,
        businessId,
        name,
        amount,
        expenseType,
        centerId,
        dateOfPayment,
        modeOfPayment,
        chequeNumber
    ) => {
        if (modeOfPayment == 2) {
            if (chequeNumber == "" || chequeNumber == null) {
                throw new ApplicationError("Please provide cheque number.", 422);
            }
        }

        //balance section
        let clientTotalPaymentdb = createInstance(["clientPayments"], information);
        let clientTotalPayment = await clientTotalPaymentdb.clientpayments.findAll({
            where: {
                status: 1,
                businessId: businessId
            },
            raw: true
        });

        var totalIncome = 0;
        for (var i = 0; i < clientTotalPayment.length; i++) {
            totalIncome += clientTotalPayment[i].totalAmountPaid;
        }

        let clientTotalExpensedb = createInstance(["clientExpense"], information);
        let clientTotalExpense = await clientTotalExpensedb.clientexpense.findAll({
            where: {
                status: 1,
                businessId: businessId
            },
            raw: true
        });

        var totalExpense = 0;
        for (var i = 0; i < clientTotalExpense.length; i++) {
            totalExpense += clientTotalExpense[i].amount;
        }

        var balance = totalIncome - totalExpense;

        if (amount > balance) {
            throw new ApplicationError("Insufficent Balance", 422);
        }
        //end of balance section
        const newExpense = {
            businessId,
            name,
            amount,
            expenseType,
            centerId,
            dateOfPayment,
            modeOfPayment,
            chequeNumber
        };
        try {
            let db = createInstance(["clientExpense"], information);
            let dbLog = createInstance(["log"], information);

            let expense = await db.clientexpense.create(newExpense);

            let header = 'Expense Recorded';
            var message = expense.dataValues.name + ' just recorded expense for ' + expense.dataValues.expenseType;
            const newLog = {
                businessId: businessId,
                activity: {
                    Status: 1,
                    header: header,
                    activityId: expense.dataValues.clientexpenseId,
                    activityName: 'update',
                    activityDate: expense.dataValues.dateOfPayment.toLocaleDateString("en-US"),
                    activityTime: expense.dataValues.dateOfPayment.toLocaleTimeString("en-US"),
                    message: message,
                    attendance: '',
                    payment: amount
                },
                referenceTable: 'clientexpense',
            };
            let log = await dbLog.log.create(newLog);


            db.sequelize.close();
            dbLog.sequelize.close();

            return await expense;
            //   return await filterFields(services, PUBLIC_FIELDS, true);
        } catch (error) {
            throw error;
        }
    };

    listExpense = async(information, businessId, filterPrivateFields = true) => {
        try {
            let db = createInstance(["clientExpense"], information);
            let expense = await db.clientexpense.findAll({
                where: {
                    status: 1,
                    businessId: businessId
                },
                raw: true
            });

            var modeOfPayment;
            var mergedExpenses = [];

            for (var i = 0; i < expense.length; i++) {
                if (parseInt(expense[i].modeOfPayment) == 1) {
                    modeOfPayment = "cash";
                } else if (parseInt(expense[i].modeOfPayment) == 2) {
                    modeOfPayment = "cheque";
                }

                let dbCenter = createInstance(["clientCenter"], information);
                let centers = await dbCenter.centers.findAll({
                    where: {
                        status: 1,
                        businessId: businessId,
                        centerId: expense[i].centerId
                    },
                    raw: true
                });
                //var mop = a[i];
                var a = { centerName: centers[0].centerName };
                var b = { modeOfPayment: modeOfPayment };
                var merged = [];
                merged.push(expense[i]);
                merged.push(a);
                merged.push(b);

                var merged1 = {};
                merged1 = Object.assign.apply(Object, merged);
                //console.log(merged1);
                mergedExpenses = mergedExpenses.concat(merged1);
            }
            db.sequelize.close();
            // if (filterPrivateFields) {
            //     return await filterFields(offerings, PUBLIC_FIELDS);
            // }
            return await mergedExpenses;
            // return await expense;
        } catch (error) {
            throw error;
        }
    };

    listRecentExpense = async(
        information,
        params,
        filterPrivateFields = true
    ) => {
        try {
            let orderBy;
            var businessIdinformation = information.split(",")[1];
            let filter = params[0] ? params[0] : "";
            var pageNo = params["page"] ? params["page"] : 1;
            var limitPerPage = PEGINATION_LIMIT;
            var offsetLimit = (pageNo - 1) * limitPerPage;
            if ((params["expence_order"] == 2) && (params["price_order"] == 2)) {
                orderBy = [
                    ["CreateDateTime", "DESC"]
                ];
            } else if (params["expence_order"] == 2 && params["price_order"] == 1) {
                orderBy = [
                    ["amount", "DESC"],
                ];
            } else if ((params["expence_order"] == 2) && (params["price_order"] == 0)) {
                orderBy = [
                    ["amount", "ASC"],
                ];
            } else if ((params["expence_order"] == 1) && (params["price_order"] == 2)) {
                orderBy = [
                    ["CreateDateTime", "DESC"],
                ];
            } else {
                orderBy = [
                    ["CreateDateTime", "ASC"],
                ];
            }
            let dbclientExpense = createInstance(["clientExpense"], information);
            let dbCenters = createInstance(["clientCenter"], information);
            dbclientExpense.clientexpense.belongsTo(dbCenters.centers, {
                foreignKey: "centerId",
                targetKey: "centerId"
            });
            let whereCondition = {
                status: 1,
                businessId: businessIdinformation,
                [dbclientExpense.sequelize.Op.or]: [{
                        name: {
                            like: "%" + filter + "%"
                        }
                    },
                    {
                        expensetype: {
                            like: "%" + filter + "%"
                        }
                    },
                    {
                        "$center.centerName$": {
                            like: "%" + filter + "%"
                        }
                    },
                    {
                        chequeNumber: {
                            like: "%" + filter + "%"
                        }
                    },
                    {
                        clientexpenseId: {
                            like: "%" + filter + "%"
                        }
                    }
                ]
            };
            let expenseClientList = await dbclientExpense.clientexpense.findAndCountAll({
                include: [{
                    model: dbCenters.centers,
                    attributes: [
                        "centerName", [
                            dbclientExpense.sequelize.fn(
                                "SUBSTRING",
                                dbclientExpense.sequelize.fn(
                                    "SOUNDEX",
                                    dbclientExpense.sequelize.col("name")
                                ),
                                1,
                                1
                            ),
                            "name_initials"
                        ]
                    ],
                    group: ["centerId"],
                    where: {
                        status: 1,
                        businessId: businessIdinformation
                    }
                }],
                where: whereCondition,
                // having: {
                //     "$center.centerName$": {
                //         like: "%" + filter + "%"
                //     }
                // },
                required: false,
                order: orderBy,
                subQuery: false,
                offset: offsetLimit,
                limit: limitPerPage,
                raw: true
            });
            dbclientExpense.sequelize.close();
            dbCenters.sequelize.close();
            return expenseClientList;
        } catch (error) {
            throw error;
        }
    };

    expenseMonth = async(
        information,
        businessId,
        dateOfPayment,
        YM,
        IE,
        params,
        filterPrivateFields = true
    ) => {
        try {
            var dop = dateOfPayment;
            var date = dop.split("-");
            var y = parseInt(date[0]);
            var m = parseInt(date[1]);
            var d = parseInt(date[2]);
            var y1 = y.toString();

            var months = [
                "january",
                "february",
                "march",
                "april",
                "may",
                "june",
                "july",
                "august",
                "september",
                "october",
                "november",
                "december"
            ];
            var final = {};
            var sequelize = require("sequelize");
            if (YM == 1 && IE == 1) {
                var monthlyIncomes = {};
                let clientTotalPaymentdb = createInstance(["clientPayments"], information);
                let dbpaymentTransactions = createInstance(["paymentTransactions"], information);
                for (var i = 1; i <= 12; i++) {
                    let payment = await dbpaymentTransactions.paymentTransactions.findAll({
                        where: {
                            status: 1,
                            paid_amount: {
                                [dbpaymentTransactions.sequelize.Op.ne]: null
                            },
                            payment_date: dbpaymentTransactions.sequelize.where(
                                dbpaymentTransactions.sequelize.fn("DATE_FORMAT", dbpaymentTransactions.sequelize.col("payment_date"), '%Y-%m'),
                                '>=',
                                dbpaymentTransactions.sequelize.fn("DATE_FORMAT", y + "-" + i + "-" + '01', '%Y-%m')
                            ),
                            and: {
                                payment_date: dbpaymentTransactions.sequelize.where(
                                    dbpaymentTransactions.sequelize.fn("DATE_FORMAT", dbpaymentTransactions.sequelize.col("payment_date"), '%Y-%m'),
                                    '<=',
                                    dbpaymentTransactions.sequelize.fn("DATE_FORMAT", y + "-" + i + "-" + '01', '%Y-%m')
                                )
                            }
                        },
                        raw: true
                    });
                    var payAmount = 0;
                    for (var j = 0; j < payment.length; j++) {
                        payAmount += payment[j].paid_amount;
                    }
                    monthlyIncomes[months[i - 1]] = payAmount;
                }
                let yearIncomesTotal = 0;
                let yearIncomesTotalCal = 0;
                if (monthlyIncomes) {
                    for (var k in monthlyIncomes) {
                        if (monthlyIncomes.hasOwnProperty(k)) {
                            yearIncomesTotal += monthlyIncomes[k];
                        }
                    }
                    yearIncomesTotalCal = yearIncomesTotal;
                    yearIncomesTotal = numberWithCommas(yearIncomesTotal);
                }
                let newmonthlyIncomes = [];
                if (monthlyIncomes) {
                    for (var key in monthlyIncomes) {
                        if (monthlyIncomes.hasOwnProperty(key)) {
                            let modifyData = await getAmountPercentage(information, monthlyIncomes[key], yearIncomesTotalCal);
                            let innerObj = {};
                            innerObj.monthname = key;
                            innerObj.percentage = Number(modifyData);
                            newmonthlyIncomes.push(innerObj);
                        }
                    }
                }
                final = { year: y, months: monthlyIncomes, monthlyIncomesPercentage: newmonthlyIncomes, yearIncomesTotal };
            } else if (YM == 1 && IE == 2) {
                var monthlyExpense = {};
                let db = createInstance(["clientExpense"], information);
                var yearExpenseTotalCal = 0;
                for (var i = 1; i <= 12; i++) {
                    let expense = await db.clientexpense.findAll({
                        where: {
                            status: 1,
                            businessId: businessId,
                            dateOfPayment: sequelize.where(
                                sequelize.fn("YEAR", sequelize.col("dateOfPayment")),
                                y
                            ),
                            and: {
                                dateOfPayment: sequelize.where(
                                    sequelize.fn("MONTH", sequelize.col("dateOfPayment")),
                                    i
                                )
                            }
                        },
                        raw: true
                    });
                    var expAmount = 0;
                    for (var j = 0; j < expense.length; j++) {
                        expAmount += expense[j].amount;
                    }
                    monthlyExpense[months[i - 1]] = expAmount;
                    yearExpenseTotalCal += expAmount;
                }
                let newmonthlyExpenses = [];
                if (monthlyExpense) {
                    for (var key in monthlyExpense) {
                        if (monthlyExpense.hasOwnProperty(key)) {
                            let modifyData = await getAmountPercentage(information, monthlyExpense[key], yearExpenseTotalCal);
                            let innerObj = {};
                            innerObj.monthname = key;
                            innerObj.percentage = Number(modifyData);
                            newmonthlyExpenses.push(innerObj);
                        }
                    }
                }
                final = { year: y, monthlyExpensePercentage: newmonthlyExpenses, months: monthlyExpense };
            } else if (YM == 2 && IE == 1) {
                //income section (month)
                // var businessIdinformation = information.split(",")[1];
                // let dbclientpayments = createInstance(["clientPayments"], information);
                // let dbClients = createInstance(["client"], information);
                // let dbsubscriptions = createInstance(["clientSubscription"], information);
                // let dbpricepack = createInstance(["clientPricepack"], information);
                // let dbpaymentTransactions = createInstance(["paymentTransactions"], information);
                // dbclientpayments.clientpayments.belongsTo(
                //     dbsubscriptions.subscriptions, {
                //         foreignKey: "subscriptionsId",
                //         targetKey: "subscriptionId"
                //     }
                // );
                // dbsubscriptions.subscriptions.belongsTo(dbClients.clients, {
                //     foreignKey: "clientId",
                //     targetKey: "clientId"
                // });
                // dbsubscriptions.subscriptions.belongsTo(dbpricepack.pricepacks, {
                //     foreignKey: "pricepacks",
                //     targetKey: "pricepackId"
                // });
                // dbpaymentTransactions.paymentTransactions.belongsTo(dbsubscriptions.subscriptions, {
                //     foreignKey: "subscriptionsId",
                //     targetKey: "subscriptionId"
                // });
                // let filter = params[0] ? params[0] : "";
                // var pageNo = params["page"] ? params["page"] : 1;
                // var limitPerPage = PEGINATION_LIMIT;
                // var offsetLimit = (pageNo - 1) * limitPerPage;

                // var itemConditions = {};
                // // var itemGroupConditions = {};
                // let itemGroupConditions = {
                //     clientName: {
                //         like: "%" + filter + "%"
                //     }
                // };
                // var itemGroupConditionsPricePack = {};

                // let paymentData = await dbclientpayments.clientpayments.findAll({
                //     attributes: [
                //         "clientpaymentId",
                //         "CreateDateTime",
                //         "subscriptionsId",
                //         "due_amount",
                //         "payble_amount", ["UpdateDateTime", "updated_at"],
                //         [
                //             dbclientpayments.sequelize.fn(
                //                 "sum",
                //                 dbclientpayments.sequelize.col("payble_amount")
                //             ),
                //             "totalPayment"
                //         ],
                //         [
                //             dbclientpayments.sequelize.fn(
                //                 "FORMAT",
                //                 dbclientpayments.sequelize.col("payble_amount"),
                //                 2
                //             ),
                //             "formated_paid_amount"
                //         ]
                //     ],
                //     group: [
                //         "subscriptionsId",
                //         "clientpaymentId",
                //         "due_amount",
                //         "payble_amount",
                //         "updated_at",
                //     ],
                //     include: [{
                //         model: dbsubscriptions.subscriptions,
                //         attributes: ["clientId"],
                //         include: [{
                //                 model: dbClients.clients,
                //                 attributes: {
                //                     include: [
                //                         [
                //                             dbClients.clients.sequelize.literal(
                //                                 'CASE WHEN photoUrl  is NULL THEN CONCAT("' +
                //                                 KNACK_UPLOAD_URL.localUrl +
                //                                 '" , "noimage.jpg" ) ELSE CONCAT("' +
                //                                 KNACK_UPLOAD_URL.localUrl +
                //                                 '" , photoUrl ) END'
                //                             ),
                //                             "photoUrl"
                //                         ]
                //                     ],
                //                     exclude: []
                //                 },
                //                 where: itemGroupConditions
                //             },
                //             {
                //                 model: dbpricepack.pricepacks,
                //                 attributes: ["pricepackName"],
                //                 where: itemGroupConditionsPricePack
                //             }
                //         ],
                //         where: itemConditions
                //     }],
                //     order: [
                //         ["UpdateDateTime", "DESC"]
                //     ],

                //     where: {
                //         status: 1,
                //         businessId: businessIdinformation,
                //         payble_amount: {
                //             [dbclientpayments.sequelize.Op.ne]: null
                //         },
                //         payment_due_date: dbclientpayments.sequelize.where(
                //             dbclientpayments.sequelize.fn("DATE_FORMAT", dbclientpayments.sequelize.col("payment_due_date"), '%Y-%m'),
                //             '>=',
                //             dbclientpayments.sequelize.fn("DATE_FORMAT", dateOfPayment, '%Y-%m')
                //         ),
                //         and: {
                //             payment_end_date: dbclientpayments.sequelize.where(
                //                 dbclientpayments.sequelize.fn("DATE_FORMAT", dbclientpayments.sequelize.col("payment_end_date"), '%Y-%m'),
                //                 '<=',
                //                 dbclientpayments.sequelize.fn("DATE_FORMAT", dateOfPayment, '%Y-%m')
                //             )
                //         }
                //     },
                //     subQuery: false,
                //     offset: offsetLimit,
                //     limit: limitPerPage,
                //     raw: true
                // });
                // let totalpayAmountDataTotalwithoutSearch = await dbclientpayments.clientpayments.findAll({
                //     attributes: [
                //         "clientpaymentId",
                //         "CreateDateTime",
                //         "subscriptionsId",
                //         "due_amount",
                //         "payble_amount", ["UpdateDateTime", "updated_at"],
                //         [
                //             dbclientpayments.sequelize.fn(
                //                 "sum",
                //                 dbclientpayments.sequelize.col("payble_amount")
                //             ),
                //             "totalPayment"
                //         ],
                //         [
                //             dbclientpayments.sequelize.fn(
                //                 "FORMAT",
                //                 dbclientpayments.sequelize.col("payble_amount"),
                //                 2
                //             ),
                //             "formated_paid_amount"
                //         ]
                //     ],
                //     group: [
                //         "subscriptionsId",
                //         "clientpaymentId",
                //         "due_amount",
                //         "payble_amount",
                //         "updated_at",
                //     ],
                //     include: [{
                //         model: dbsubscriptions.subscriptions,
                //         attributes: ["clientId"],
                //         include: [{
                //                 model: dbClients.clients,
                //                 attributes: {
                //                     include: [
                //                         [
                //                             dbClients.clients.sequelize.literal(
                //                                 'CASE WHEN photoUrl  is NULL THEN CONCAT("' +
                //                                 KNACK_UPLOAD_URL.localUrl +
                //                                 '" , "noimage.jpg" ) ELSE CONCAT("' +
                //                                 KNACK_UPLOAD_URL.localUrl +
                //                                 '" , photoUrl ) END'
                //                             ),
                //                             "photoUrl"
                //                         ]
                //                     ],
                //                     exclude: []
                //                 },
                //                 where: {}
                //             },
                //             {
                //                 model: dbpricepack.pricepacks,
                //                 attributes: ["pricepackName"],
                //                 where: itemGroupConditionsPricePack
                //             }
                //         ],
                //         where: itemConditions
                //     }],
                //     order: [
                //         ["UpdateDateTime", "DESC"]
                //     ],
                //     where: {
                //         status: 1,
                //         businessId: businessIdinformation,
                //         payble_amount: {
                //             [dbclientpayments.sequelize.Op.ne]: null
                //         },
                //         payment_due_date: dbclientpayments.sequelize.where(
                //             dbclientpayments.sequelize.fn("DATE_FORMAT", dbclientpayments.sequelize.col("payment_due_date"), '%Y-%m'),
                //             '>=',
                //             dbclientpayments.sequelize.fn("DATE_FORMAT", dateOfPayment, '%Y-%m')
                //         ),
                //         and: {
                //             payment_end_date: dbclientpayments.sequelize.where(
                //                 dbclientpayments.sequelize.fn("DATE_FORMAT", dbclientpayments.sequelize.col("payment_end_date"), '%Y-%m'),
                //                 '<=',
                //                 dbclientpayments.sequelize.fn("DATE_FORMAT", dateOfPayment, '%Y-%m')
                //             )
                //         }
                //     },
                //     subQuery: false,
                //     raw: true
                // });
                // var totalpayAmountData = 0;
                // for (let j = 0; j < totalpayAmountDataTotalwithoutSearch.length; j++) {
                //     totalpayAmountData += totalpayAmountDataTotalwithoutSearch[j].totalPayment;
                // }
                // dbclientpayments.sequelize.close();
                // dbClients.sequelize.close();
                // dbsubscriptions.sequelize.close();
                // dbpricepack.sequelize.close();
                // dbpaymentTransactions.sequelize.close();

                // if (paymentData.length > 0) {
                //     for (let x = 0; x < paymentData.length; x++) {
                //         paymentData[x]["sessionInfo"] = await sessionInfo(information, {
                //             status: 1,
                //             subscriptionId: paymentData[x]['subscriptionsId']
                //         });
                //     }
                // }

                // final = {
                //     total: totalpayAmountData,
                //     paymentClientList: [{ count: paymentData.length, raw: paymentData }]
                // };





                var mergedAllActiveClients = [];
                let todayDate = await hyphenDateFormat(new Date);
                // let todayDate = '2018-07-04';
                var businessIdinformation = information.split(",")[1];
                let dbclientpayments = createInstance(["clientPayments"], information);
                let dbsubscriptions = createInstance(["clientSubscription"], information);
                let dbClients = createInstance(["client"], information);
                let dbpricepack = createInstance(["clientPricepack"], information);
                let dbclientexpense = createInstance(["clientExpense"], information);
                let dbpaymentTransactions = createInstance(["paymentTransactions"], information);
                let dbclientScheduleMap = createInstance(["clientScheduleMap"], information);
                dbclientpayments.clientpayments.belongsTo(dbsubscriptions.subscriptions, {
                    foreignKey: "subscriptionsId",
                    targetKey: "subscriptionId"
                });
                dbsubscriptions.subscriptions.belongsTo(dbClients.clients, {
                    foreignKey: "clientId",
                    targetKey: "clientId"
                });
                dbsubscriptions.subscriptions.belongsTo(dbpricepack.pricepacks, {
                    foreignKey: "pricepacks",
                    targetKey: "pricepackId"
                });
                dbclientpayments.clientpayments.belongsTo(dbpaymentTransactions.paymentTransactions, {
                    foreignKey: "clientpaymentId",
                    targetKey: "clientpaymentId"
                });
                dbsubscriptions.subscriptions.belongsTo(dbclientScheduleMap.clientschedulemap, {
                    foreignKey: "parentSubscriptionId",
                    targetKey: "subscriptionId"
                });
                dbpaymentTransactions.paymentTransactions.belongsTo(dbsubscriptions.subscriptions, {
                    foreignKey: "subscriptionsId",
                    targetKey: "subscriptionId"
                });

                let filter = params[0] ? params[0] : "";
                var pageNo = params["page"] ? params["page"] : 1;
                var limitPerPage = PEGINATION_LIMIT;
                var offsetLimit = (pageNo - 1) * limitPerPage;


                let itemclientpaymentsRiminderData = {
                    status: 1,
                    [dbpaymentTransactions.sequelize.Op.or]: [{
                            "$subscription.client.clientName$": {
                                like: "%" + filter + "%"
                            }
                        },
                        {
                            "$subscription.pricepack.pricepackName$": {
                                like: "%" + filter + "%"
                            }
                        }
                    ],
                };
                let clientpaymentsRiminderData = await dbpaymentTransactions.paymentTransactions.findAndCount({
                    attributes: {
                        include: [],
                        exclude: ["payment_content"]
                    },
                    include: [{
                        model: dbsubscriptions.subscriptions,
                        attributes: {
                            include: [],
                            exclude: ["createDateTime", "updateDateTime"]
                        },
                        include: [{
                                model: dbClients.clients,
                                attributes: {
                                    include: [
                                        [
                                            dbClients.clients.sequelize.literal(
                                                'CASE WHEN photoUrl  is NULL THEN CONCAT("' +
                                                KNACK_UPLOAD_URL.localUrl +
                                                '" , "noimage.jpg" ) ELSE CONCAT("' +
                                                KNACK_UPLOAD_URL.localUrl +
                                                '" , photoUrl ) END'
                                            ),
                                            "photoUrl"
                                        ],
                                    ],
                                    exclude: ["createDateTime", "updateDateTime"]
                                },
                                where: {
                                    status: 1,
                                    businessId: businessIdinformation,
                                }
                            },
                            {
                                model: dbpricepack.pricepacks,
                                attributes: ["pricepackName"],
                                where: {},
                            }
                        ],
                        where: {
                            status: 1,
                            payment_date: dbclientpayments.sequelize.where(
                                dbclientpayments.sequelize.fn("DATE_FORMAT", dbclientpayments.sequelize.col("payment_date"), '%Y-%m'),
                                '>=',
                                dbclientpayments.sequelize.fn("DATE_FORMAT", dateOfPayment, '%Y-%m')
                            ),
                            and: {
                                payment_date: dbclientpayments.sequelize.where(
                                    dbclientpayments.sequelize.fn("DATE_FORMAT", dbclientpayments.sequelize.col("payment_date"), '%Y-%m'),
                                    '<=',
                                    dbclientpayments.sequelize.fn("DATE_FORMAT", dateOfPayment, '%Y-%m')
                                )
                            }
                        },
                    }],
                    order: [
                        ["CreateDateTime", "DESC"]
                    ],
                    where: itemclientpaymentsRiminderData,
                    offset: offsetLimit,
                    limit: limitPerPage,
                    raw: true
                });

                if (clientpaymentsRiminderData.rows.length > 0) {
                    for (let x = 0; x < clientpaymentsRiminderData.rows.length; x++) {
                        clientpaymentsRiminderData.rows[x]["sessionInfo"] = await sessionInfo(information, {
                            status: 1,
                            subscriptionId: clientpaymentsRiminderData.rows[x]['subscriptionsId']
                        });
                    }
                }
                let totalpayAmountData = await dbpaymentTransactions.sequelize.query(
                    "SELECT SUM(`paid_amount`) AS `total_paid_amount` FROM `paymentTransactions` AS `paymentTransactions` INNER JOIN `subscriptions` AS `subscription` ON `paymentTransactions`.`subscriptionsId` = `subscription`.`subscriptionId` AND `subscription`.`status` = 1 AND DATE_FORMAT(`payment_date`, '%Y-%m')>= DATE_FORMAT('" + dateOfPayment + "', '%Y-%m') AND ( DATE_FORMAT(`payment_date`, '%Y-%m')<= DATE_FORMAT('" + dateOfPayment + "', '%Y-%m') ) INNER JOIN `clients` AS `subscription->client` ON `subscription`.`clientId` = `subscription->client`.`clientId` AND `subscription->client`.`status` = 1 AND `subscription->client`.`businessId` = '" + businessIdinformation + "' INNER JOIN `pricepacks` AS `subscription->pricepack` ON `subscription`.`pricepacks` = `subscription->pricepack`.`pricepackId` WHERE `paymentTransactions`.`status` = 1", {
                        type: dbpaymentTransactions.sequelize.QueryTypes.SELECT
                    }
                );
                final = {
                    total: totalpayAmountData,
                    paymentClientList: [{ count: clientpaymentsRiminderData.count, raw: clientpaymentsRiminderData.rows }]
                };
                dbclientpayments.sequelize.close();
                dbsubscriptions.sequelize.close();
                dbClients.sequelize.close();
                dbpricepack.sequelize.close();
                dbclientexpense.sequelize.close();
                dbpaymentTransactions.sequelize.close();
                dbclientScheduleMap.sequelize.close();
                //end of income section (month)
                return await final;
            } else if (YM == 2 && IE == 2) {
                //expense
                var businessIdinformation = information.split(",")[1];
                let filter = params[0] ? params[0] : "";
                var pageNo = params["page"] ? params["page"] : 1;
                var limitPerPage = PEGINATION_LIMIT;
                var offsetLimit = (pageNo - 1) * limitPerPage;
                let dbclientExpense = createInstance(["clientExpense"], information);
                let dbCenters = createInstance(["clientCenter"], information);
                dbclientExpense.clientexpense.belongsTo(dbCenters.centers, {
                    foreignKey: "centerId",
                    targetKey: "centerId"
                });
                let expenseTotalPrice = await dbclientExpense.clientexpense.findAll({
                    attributes: [
                        [
                            dbclientExpense.sequelize.fn(
                                "sum",
                                dbclientExpense.sequelize.col("amount")
                            ),
                            "expenseTotalPrice"
                        ],
                        [
                            dbclientExpense.sequelize.fn("MONTHNAME", dateOfPayment),
                            "monthName"
                        ]
                    ],
                    where: {
                        status: 1,
                        businessId: businessId,
                        dateOfPayment: dbclientExpense.sequelize.where(
                            dbclientExpense.sequelize.fn(
                                "YEAR",
                                dbclientExpense.sequelize.col("dateOfPayment")
                            ),
                            y
                        ),
                        and: {
                            dateOfPayment: dbclientExpense.sequelize.where(
                                dbclientExpense.sequelize.fn(
                                    "MONTH",
                                    dbclientExpense.sequelize.col("dateOfPayment")
                                ),
                                m
                            )
                        }
                    },
                    raw: true
                });

                let expenseClientList = await dbclientExpense.clientexpense.findAndCountAll({
                    attributes: {
                        include: [
                            [
                                dbclientExpense.sequelize.fn(
                                    "SUBSTRING",
                                    dbclientExpense.sequelize.fn(
                                        "SOUNDEX",
                                        dbclientExpense.sequelize.col("name")
                                    ),
                                    1,
                                    1
                                ),
                                "name_initials"
                            ]
                        ],
                        exclude: []
                    },
                    include: [{
                        model: dbCenters.centers,
                        attributes: ["centerName"],
                        where: {
                            status: 1,
                            businessId: businessIdinformation
                        }
                    }],
                    where: {
                        status: 1,
                        businessId: businessId,
                        dateOfPayment: dbclientExpense.sequelize.where(
                            dbclientExpense.sequelize.fn(
                                "YEAR",
                                dbclientExpense.sequelize.col("dateOfPayment")
                            ),
                            y
                        ),
                        and: {
                            dateOfPayment: dbclientExpense.sequelize.where(
                                dbclientExpense.sequelize.fn(
                                    "MONTH",
                                    dbclientExpense.sequelize.col("dateOfPayment")
                                ),
                                m
                            )
                        },
                        name: {
                            like: "%" + filter + "%"
                        }
                    },
                    subQuery: false,
                    offset: offsetLimit,
                    limit: limitPerPage,
                    raw: true
                });
                dbclientExpense.sequelize.close();
                dbCenters.sequelize.close();
                final = { expenseTotalPrice, expenseClientList };
            }

            return await final;
        } catch (error) {
            throw error;
        }
    };
    getExpenseType = async(
        information,
        params,
        filterPrivateFields = true
    ) => {
        try {
            let businessIdinformation = information.split(",")[1];
            let filter = params[0] ? params[0] : "";
            let clientTotalExpensedb = createInstance(["clientExpense"], information);
            let clientTotalExpense = await clientTotalExpensedb.clientexpense.findAll({
                attributes: ["expenseType"],
                where: {
                    status: 1,
                    businessId: businessIdinformation,
                    expenseType: {
                        like: "%" + filter + "%"
                    }
                },
                group: ["expenseType"],
                raw: true
            });
            clientTotalExpensedb.sequelize.close();
            return await clientTotalExpense;
        } catch (error) {
            throw error;
        }
    };

    remove = async(information, clientexpenseId) => {
        let clientTotalExpensedb = createInstance(["clientExpense"], information);
        try {
            let packages = await clientTotalExpensedb.clientexpense.update({
                status: 0,
                updateDateTime: Date.now()
            }, {
                where: {
                    clientexpenseId: clientexpenseId
                }
            });
            clientTotalExpensedb.sequelize.close();
            return packages;
        } catch (error) {
            throw error;
        }
    };


}
export default new ExpenseDB();