import { filterFields, createInstance, commaSeparatedValues, tConv24, convartday, hyphenDateFormat, sessionInfo, makeEmailTemplate, sendEmail, KNACK_ATTENDANCE_REPORT, attendanceCountOfSchedule, sendSmsMobile, CheckMainOrCenterAdmin, toLogEntry, sessionInfoBasedOnService, dateAttendance, sessionCount,expiryDate } from "../../lib/common";
import { ApplicationError } from "../../lib/errors";
import { start } from "repl";
import Sequelize from "sequelize";
import BaseModel from "../../db/BaseModel";
import {
    KNACKDB,
    KNACK_DBSETTINGS,
    KNACK_UPLOAD_URL,
    KNACK_TEAM_UPLOAD_URL,
    UPLOAD_DIR
} from "../../lib/constants.js";
import loginSignUpHelperDB from "../HelperModels/LoginSignUpHelperModel";
import BusinessSchema from "../../schemas/business.schema";
import { addEmployee, viewEmployee } from "../ClientHelperModels/ClientEmployeeHelperModel";


const PUBLIC_FIELDS = [
    "businessId",
    "scheduleId",
    "clientId",
    "attendanceType"
];

var fs = require("fs");
var path = require("path");
var pdf1 = require("html-pdf");

export class AttendanceDB {
    attendance = [];

    clientAttendance = async (
        information,
        scheduleId,
        businessId,
        clientId,
        attendanceType,
        attendanceDate
    ) => {
        try {
            let db = createInstance(["clientScheduleMap"], information);
            let attendance = await db.clientschedulemap.findAll({
                where: {
                    businessId: businessId,
                    scheduleId: scheduleId,
                    clientId: clientId
                }
            });

            var arrKey = [];
            var arrVal = [];
            // console.log('++++++++++++++++++++++++++');
            // console.log(attendance[0].attendanceType);
            //console.log(attendance[1]);
            var obj = attendance[0].attendanceType;
            var i = 0;
            for (var key in obj) {
                if (obj.hasOwnProperty(key)) {
                    var val = obj[key];
                    //console.log(key + ':' + val);
                    if (key == attendanceDate) {
                        val = attendanceType;
                        //console.log(key + ':' + val);
                        arrKey[i] = key;
                        arrVal[i] = val;
                        i++;
                    } else {
                        // console.log(key+':'+val);
                        arrKey[i] = key;
                        arrVal[i] = val;
                        i++;
                    }
                }
            }

            var obj = {};
            for (var i = 0; i < arrKey.length; i++) {
                obj[arrKey[i]] = arrVal[i];
            }

            // console.log(obj);
            var finalAttendance = JSON.stringify(obj);
            if (finalAttendance.includes('"')) {
                obj = JSON.parse(finalAttendance);
            }
            // console.log(obj);

            let attendanceNew = await db.clientschedulemap.update({
                attendanceType: obj,
                updateDateTime: Date.now()
            }, {
                    where: {
                        clientId: clientId
                    }
                });

            //console.log(obj);

            db.sequelize.close();
            return attendanceNew;
        } catch (error) {
            throw error;
        }
    };

    updateAttendanceMulitipleClient = async (
        information,
        scheduleId,
        businessId,
        clientId,
        attendanceType,
        attendanceDate
    ) => {
        try {

            let db = createInstance(["clientScheduleMap"], information);
            let str_array2 = await commaSeparatedValues(clientId);
            let str_array4 = await commaSeparatedValues(attendanceType);

            if (str_array2.length != str_array4.length) {
                throw new ApplicationError(
                    "Number of Clients and Number of Attendances doesn't match",
                    409
                );
            }

            /**********- loop for updating all the clients -********** */
            for (var j = 0; j < str_array2.length; j++) {
                var attendance = await db.clientschedulemap.findAll({
                    where: {
                        businessId: businessId,
                        scheduleId: scheduleId,
                        clientId: str_array2[j]
                    }
                });

                var obj = attendance[0].attendanceType; //getting attendance type of the clients one by one depending on the value of outer loop

                /********- separating keys and values from obj() into two different arrays -******** */
                var arrKey = [];
                var arrVal = [];
                var i = 0;
                for (var key in obj) {
                    if (obj.hasOwnProperty(key)) {
                        var val = obj[key];
                        //console.log(key + ':' + val);
                        if (key == attendanceDate) {
                            val = str_array4[j];
                            //console.log(key + ':' + val);
                            arrKey[i] = key;
                            arrVal[i] = val;
                            i++;
                        } else {
                            //console.log(key+':'+val);
                            arrKey[i] = key;
                            arrVal[i] = val;
                            i++;
                        }
                    }
                }
                /********************************************************************************** */
                /********************-wrapping the arrays into json object-************************ */
                var obj = {};
                for (var i = 0; i < arrKey.length; i++) {
                    obj[arrKey[i]] = arrVal[i];
                }
                var finalAttendance = JSON.stringify(obj);
                if (finalAttendance.includes('"')) {
                    obj = JSON.parse(finalAttendance);
                }
                /********************************************************************************** */

                var attendanceNew = await db.clientschedulemap.update({
                    attendanceType: obj,
                    updateDateTime: Date.now()
                }, {
                        where: {
                            clientId: str_array2[j],
                            scheduleId: scheduleId
                        }
                    });
            }
            /************************************************************************************* */
            db.sequelize.close();
            return attendanceNew;
        } catch (error) {
            throw error;
        }
    };

    // Booking Attendance Record New
    newupdateAttendanceMulitipleClient = async (
        information,
        scheduleId,
        businessId,
        clientId,
        attendanceType,
        attendanceDate,
        time
    ) => {
        try {
            let pricepackName = [];
            let datas = information.split(",");
            let empId = datas[2];
            let dbClients = createInstance(["client"], information);
            let dbCSM = createInstance(["clientScheduleMap"], information);
            let dbPricepack = createInstance(["clientPricepack"], information);
            let dbSubscription = createInstance(["clientSubscription"], information);
            let dbSPM = createInstance(["servicePricepackMap"], information);
            let dbSchedule = createInstance(["clientSchedule"], information);
            let db = createInstance(["teamScheduleMap"], information);
            let dbLog = createInstance(["log"], information);
            dbCSM.clientschedulemap.belongsTo(dbClients.clients, {
                foreignKey: "clientId",
                targetKey: "clientId"
            });
            dbCSM.clientschedulemap.belongsTo(dbSchedule.schedule, {
                foreignKey: "scheduleId",
                targetKey: "scheduleId"
            });
            dbSubscription.subscriptions.belongsTo(dbPricepack.pricepacks, {
                targetKey: "pricepackId",
                foreignKey: "pricepacks"
            });

            //------------getting the name of the booking-------------------

            let scheduledata = await dbSchedule.schedule.findAll({
                where: {
                    scheduleId: scheduleId
                }
            });


            //------------end of getting the name of the booking-----------------------

            //------------getting the name of the booking-------------------

            var sender = await BusinessSchema.Business().find({
                attributes: ["businessName"],
                where: {
                    businessId: businessId
                },
                raw: true
            });

            console.log(sender);

            //------------end of getting the name of the booking----------------------



            let LogStatus = 0;
            if (!empId) throw new ApplicationError("No UserId", 401);
            let checkAdmin = await CheckMainOrCenterAdmin(information);
            let UserStatus = checkAdmin[1].UserStatus;
            let givenDateTime = new Date(attendanceDate + ' ' + time);
            let clientIdArray = await commaSeparatedValues(clientId);
            let attendanceArray = await commaSeparatedValues(attendanceType);
            let today = new Date();

            if (clientIdArray.length != attendanceArray.length) {
                throw new ApplicationError(
                    "Number of Clients and Number of Attendances doesn't match",
                    422 //unprocessable entity, InvalidArgumentException
                );
            }

            let allClients = await dbCSM.clientschedulemap.findAll({
                where: {
                    businessId: businessId,
                    scheduleId: scheduleId,
                    clientId: clientIdArray,
                },
                include: [{
                    model: dbSchedule.schedule,
                    attributes: ['serviceId']
                },
                {
                    model: dbClients.clients,
                    attributes: ['clientName']
                }
                ],
                raw: true
            });

            let clientAtd = 0;
            let countClientAtd = 0;
            for (let client of allClients) {
                let subscription = await dbSubscription.subscriptions.find({
                    where: {
                        subscriptionId: client.subscriptionId
                    },
                    include: [{ model: dbPricepack.pricepacks }],
                    raw: true
                });

                let spm = await dbSPM.servicepricepackmap.find({
                    attributes: ['serviceDuration'],
                    where: {
                        serviceId: client['schedule.serviceId'],
                        pricepackId: subscription.pricepacks
                    }
                });

                let pricepackExpiryDate = new Date(subscription.subscriptionDateTime);
                pricepackExpiryDate.setDate(pricepackExpiryDate.getDate() + subscription["pricepack.pricepackValidity"]);

                let sessionCount = 0;
                sessionCount += subscription.totalSessionsAttended == null ? 0 : subscription.totalSessionsAttended;
                let clientAttendance = client.attendanceType;
                let clientAttendanceUpdated = [];
                let atdFound = 0;

                //updating session count of the main row (masterParent)
                let newSchedule = await dbCSM.clientschedulemap.find({
                    where: {
                        clientId: client.clientId,
                        scheduleId: scheduleId
                    },
                    raw: true
                });
                let allSchedules = await dbCSM.clientschedulemap.findAll({
                    where: {
                        clientId: client.clientId,
                        masterParentScheduleId: newSchedule.masterParentScheduleId
                    },
                    raw: true
                });
                for (let oneSchedule of allSchedules) {
                    for (let atd of oneSchedule.attendanceType) {
                        let atdDateTime = new Date(atd.scdate + ' ' + atd.scheduleStartTime);
                        var atdValue = +atdDateTime === +givenDateTime ? attendanceArray[clientAtd] : atd.atd;
                        sessionCount += (atdValue == 1 || atdValue == 2) ? 1 : 0;
                    }
                }

                //if (spm.serviceDuration >= sessionCount || +(new Date(pricepackExpiryDate)) < +(new Date(today))) {
                if ((subscription["pricepack.durationType"] == 1 && spm.serviceDuration >= sessionCount) || (+(new Date(pricepackExpiryDate)) >= +(new Date(today)))) {
                    //updating the attendance
                    for (let atd of clientAttendance) {
                        let atdDateTime = new Date(atd.scdate + ' ' + atd.scheduleStartTime);
                        var atdValue = +atdDateTime === +givenDateTime ? attendanceArray[clientAtd] : atd.atd;
                        if (+atdDateTime === +givenDateTime && atd.atd != atdValue) {
                            atdFound = 1;
                            countClientAtd++;
                        }
                        let date = new Date();
                        let currentDate = await hyphenDateFormat(date);
                        let currentTime = date.toLocaleTimeString();
                        clientAttendanceUpdated.push({
                            id: atd.id,
                            scdate: atd.scdate,
                            atd: parseInt(atdValue),
                            atdDate: atdFound == 1 ? currentDate : atd.atdDate,
                            atdTime: atdFound == 1 ? currentTime : atd.atdTime,
                            atdBy: atdFound == 1 ? empId : atd.atdBy,
                            reschedule: atd.reschedule,
                            rescheduleId: atd.rescheduleId,
                            rescheduleDate: atd.rescheduleDate,
                            scheduleStartTime: atd.scheduleStartTime,
                            scheduleEndTime: atd.scheduleEndTime,
                            scheduleFrom: atd.scheduleFrom,
                            centerId: atd.centerId,
                            rescheduleBy: atd.rescheduleBy,
                            comment: atd.comment
                        });
                        // sessionCount += (atdValue == 1 || atdValue == 2) ? 1 : 0;
                    }

                    //update the rescheduled row
                    await dbCSM.clientschedulemap.update({
                        attendanceType: clientAttendanceUpdated,
                        // sessionCount: sessionCount,
                        updateDateTime: Date.now()
                    }, {
                            where: {
                                clientId: client.clientId,
                                scheduleId: scheduleId
                            }
                        });

                    //update the main row
                    await dbCSM.clientschedulemap.update({
                        // attendanceType: clientAttendanceUpdated,
                        sessionCount: sessionCount,
                        updateDateTime: Date.now()
                    }, {
                            where: {
                                clientId: client.clientId,
                                scheduleId: newSchedule.masterParentScheduleId
                            }
                        });
                    //end of changes

                    let PricepackNames = await dbSubscription.subscriptions.findAll({
                        where: {
                            subscriptionId: client.subscriptionId
                        },
                        include: [{ model: dbPricepack.pricepacks }],
                        raw: true
                    });

                    //Client SMS for attendance
                    let clients = await dbClients.clients.findAll({
                        where: {
                            clientId: client.clientId
                        },
                        raw: true
                    });

                    let Status;
                    let text;
                    let subject;
                    let contents;

                    let setObj = { status: 1, subscriptionId: client.subscriptionId };
                    let session = await sessionInfo(information, setObj);

                    let expiry = await expiryDate(information, client.subscriptionId);

                    // if (attendanceArray[clientAtd] == 1) Status = "present";
                    // if (attendanceArray[clientAtd] == 2) Status = "absent";
                    // if (attendanceArray[clientAtd] == 3) Status = "excused";
                    // if (atdFound == 1) {
                    //     LogStatus = 1;
                    //     pricepackName.push({ PricePackName: PricepackNames[0]["pricepack.pricepackName"], pricepackId: PricepackNames[0]["pricepack.pricepackId"] });

                    //     await sendSmsMobile(clients[0].contactNumber, "You have been marked " + Status + " for the class on " + attendanceDate);

                    // }

                    if (attendanceArray[clientAtd] == 1) {
                        Status = "present";
                        text = "Hi " + clients[0].clientName + " , thank you for attending the session of " + session[0]["pricepackName"] + " for " + sender.businessName + "  You still have " + session[0]["remainingSession"] + " sessions remaining and your plan expires on " + expiry + " . For any other queries, please contact +91 9812345678.";
                        subject = "Senders name: " + sender.businessName + "       Attendance record of " + attendanceDate + " - " + Status;
                        contents = "Hi " + clients[0].clientName + ", Thank you for attending the session of " + session[0]["pricepackName"] + ". You still have " + session[0]["remainingSession"] + " sessions remaining and your plan expires on " + expiry + ". For any help or assistance, reach out to us anytime at +91 9812345678  Kind regards, " + sender.businessName;
                    }

                    if (attendanceArray[clientAtd] == 2) {
                        Status = "absent";
                        text = "Hi " + clients[0].clientName + " , sorry you couldnot make it to the session of " + session[0]["pricepackName"] + " for " + sender.businessName + ". You still have " + session[0]["remainingSession"] + " sessions remaining and your plan expires on " + expiry + ". For any other queries, please contact +91 9812345678. ";
                        subject = "Senders name:  " + sender.businessName + "      Attendance record of " + attendanceDate + " - " + Status;
                        contents = "Hi " + clients[0].clientName + ",  Sorry you could'nt make it to the session of " + session[0]["pricepackName"] + ". You have " + session[0]["remainingSession"] + " sessions remaining and your plan expires on " + expiry + ".  For any help or assistance, reach out to us anytime at +91 9812345678  Kind regards, " + sender.businessName;
                    }
                    if (attendanceArray[clientAtd] == 3) {
                        Status = "excused";
                        text = "Hi " + clients[0].clientName + " , sorry you couldnot make it to the session of " + session[0]["pricepackName"] + " for " + sender.businessName + ". You still have " + session[0]["remainingSession"] + " sessions remaining and your plan expires on " + expiry + ". For any other queries, please contact +91 9812345678. ";;
                        subject = "Senders name:  " + sender.businessName + "      Attendance record of " + attendanceDate + " - " + Status;
                        contents = "Hi " + clients[0].clientName + ",  Sorry you could'nt make it to the session of " + session[0]["pricepackName"] + ". You have " + session[0]["remainingSession"] + " sessions remaining and your plan expires on " + expiry + ".  For any help or assistance, reach out to us anytime at +91 9812345678  Kind regards, " + sender.businessName;;
                    }
                    if (atdFound == 1) {
                        LogStatus = 1;
                        pricepackName.push({ PricePackName: PricepackNames[0]["pricepack.pricepackName"], pricepackId: PricepackNames[0]["pricepack.pricepackId"] });

                        if (Status)

                            await sendSmsMobile(clients[0].contactNumber, text);

                        if (clients[0].emailId) {

                            sendEmail(clients[0].emailId, subject, contents);
                        }
                    }

                }

                clientAtd++;
            }
            dbCSM.sequelize.close();

            let setCond = {
                status: 1,
                scheduleId: scheduleId
            };

            let AttendanceRecord = await dateAttendance(information, setCond, attendanceDate, time);

            if (LogStatus == 1) {

                let dbClientSchedule = createInstance(["clientSchedule"], information);
                let ScheduleName = await dbClientSchedule.schedule.findAll({
                    attributes: ['scheduleName'],
                    where: {
                        scheduleId: scheduleId
                    },
                    raw: true
                });

                const newLog = {
                    businessId: businessId,
                    activity: {
                        Status: 1,
                        header: 'Attendance Recorded',
                        ScheduleId: scheduleId,
                        PricepackDetails: pricepackName,
                        AttendanceDate: attendanceDate,
                        Time: time,
                        NotificationText: "Attendance for " + ScheduleName[0]["scheduleName"] + " is successfully recorded. View the record on your right",
                        AttendanceBy: empId,
                        UserStatus: UserStatus,
                        AttendanceData: AttendanceRecord,
                        activityDate: new Date(),
                        activityTime: new Date(),
                        message: 'Attendance Recorded',
                    },
                    referenceTable: 'clientScheduleMap',
                };
                await toLogEntry(newLog, information);

            }

            dbLog.sequelize.close();

            if (countClientAtd > 0) return [1];
            else return [0];
        } catch (error) {
            throw error;
        }
    };

    // Booking Attendance Record Old Code 
    listAttendance = async (
        information,
        scheduleId,
        businessId,
        attendanceDate,
        searchKey
        //attendanceType
    ) => {
        if (searchKey[0] === '') {
            try {
                let dbClients = createInstance(["clientScheduleMap"], information);
                let allClients = await dbClients.clientschedulemap.findAll({
                    where: {
                        businessId: businessId,
                        scheduleId: scheduleId
                    }
                });

                var scheduleDate = [{ scheduleDate: attendanceDate }];

                //getting schedule name or batch name as booking name
                let db = createInstance(["clientSchedule"], information);
                let checkSchedule = await db.schedule.findAll({
                    where: {
                        scheduleId: scheduleId,
                        status: 1
                    },
                    raw: true
                });
                db.sequelize.close();

                let dbCenter = createInstance(["clientCenter"], information);
                if (checkSchedule.length > 0) {
                    var center = await dbCenter.centers.find({
                        where: {
                            status: 1,
                            centerId: checkSchedule[0].centerId
                        },
                        raw: true
                    });
                }
                dbCenter.sequelize.close();

                let dbService = createInstance(["clientService"], information);
                let services = await dbService.services.findAll({
                    where: {
                        serviceId: checkSchedule[0].serviceId
                    }
                });
                db.sequelize.close();

                // Set Day and Time
                var stime = tConv24(checkSchedule[0].startTime);
                var etime = tConv24(checkSchedule[0].endTime);
                let stime = stime + '-' + etime;
                let dayint = convartday(checkSchedule[0].repeat_days);
                let day_time = ', ' + dayint + ', ' + stime;
                // End

                let booking;
                if (checkSchedule[0].scheduleName) {
                    booking = checkSchedule[0].scheduleName;
                } else {
                    booking = services[0].serviceName + day_time;
                }

                var startTime = checkSchedule[0].startTime;
                var endTime = checkSchedule[0].endTime;

                var bookingName = [{ bookingName: booking }];
                var start = [{ startTime: startTime }];
                var end = [{ endTime: endTime }];

                //if the date is rescheduled.. change it later...****************
                let rescheduleDate = "";

                var header = [];
                Array.prototype.push.apply(header, scheduleDate);
                Array.prototype.push.apply(header, bookingName);
                Array.prototype.push.apply(header, start);
                Array.prototype.push.apply(header, end);
                Array.prototype.push.apply(header, [{ centerId: center.centerId }]);
                Array.prototype.push.apply(header, [{ centerName: center.centerName }]);
                Array.prototype.push.apply(header, [{ rescheduleDate: rescheduleDate }]);
                header = Object.assign.apply(Object, header);

                var merged = [];
                var countPresent = 0,
                    countAbsent = 0,
                    countExcused = 0,
                    countUnmarked = 0,
                    countTotal = 0;
                for (var j = 0; j < allClients.length; j++) {
                    var mergedClientSchedule = [];
                    /* one client with attendance of a specific day */
                    let db = createInstance(["clientScheduleMap"], information);
                    let attendance = await db.clientschedulemap.findAll({
                        where: {
                            businessId: businessId,
                            scheduleId: scheduleId,
                            clientId: allClients[j].clientId
                        }
                    });

                    //getting the name of the client from clients table
                    let dbClientsName = createInstance(["client"], information);
                    let clients = await dbClientsName.clients.findAll({
                        where: {
                            clientId: allClients[j].clientId
                        },
                        raw: true
                    }); //code ends

                    var isphoto = (clients[0].photoUrl === null) ? KNACK_UPLOAD_URL.localUrl + 'noimage.jpg' : KNACK_UPLOAD_URL.localUrl + clients[0].photoUrl;

                    //console.log(clients[0].clientName);

                    var attendanceKeys = [];
                    var attendanceValues = [];
                    Object.keys(attendance[0].attendanceType).forEach(function (key) {
                        attendanceKeys.push(key);
                        attendanceValues.push(attendance[0].attendanceType[key]);

                    });

                    var dateOfAttendance, attendanceText;
                    for (var i = 0; i < attendanceKeys.length; i++) {
                        if (attendanceKeys[i] == attendanceDate) {
                            dateOfAttendance = attendanceValues[i];
                            if (dateOfAttendance == 1) attendanceText = 'P';
                            else if (dateOfAttendance == 2) attendanceText = 'A';
                            else if (dateOfAttendance == 3) attendanceText = 'E';
                            else if (dateOfAttendance == 4) attendanceText = 'U';
                            break;
                        } else dateOfAttendance = null;
                    }

                    //change it soon.. if client reschedule..
                    let clientRescheduleDate = "";
                    if (1) { }

                    var mergedOld = [];
                    if (dateOfAttendance != null) {

                        Array.prototype.push.apply(mergedClientSchedule, [
                            { clientId: allClients[j].clientId }
                        ]);
                        Array.prototype.push.apply(mergedClientSchedule, [
                            { clientName: clients[0].clientName }
                        ]);
                        Array.prototype.push.apply(mergedClientSchedule, [
                            { clientPhoto: isphoto }
                        ]);
                        Array.prototype.push.apply(mergedClientSchedule, [
                            { attendance: dateOfAttendance }
                        ]);
                        Array.prototype.push.apply(mergedClientSchedule, [
                            { attendanceText: attendanceText }
                        ]);
                        Array.prototype.push.apply(mergedClientSchedule, [
                            { rescheduleDate: clientRescheduleDate }
                        ]);
                        mergedOld = Object.assign.apply(Object, mergedClientSchedule);
                    }
                    /* one client with attendance of a specific day */
                    db.sequelize.close();
                    merged = merged.concat(mergedOld);

                    if (dateOfAttendance) {
                        countTotal++;
                    }
                    if (dateOfAttendance == 1) {
                        countPresent++;
                    }
                    if (dateOfAttendance == 2) {
                        countAbsent++;
                    }
                    if (dateOfAttendance == 3) {
                        countExcused++;
                    }
                    if (dateOfAttendance == 4) {
                        countUnmarked++;
                    }
                }
                let attendance = {
                    Present: countPresent,
                    Absent: countAbsent,
                    Excused: countExcused,
                    Unmarked: countUnmarked,
                };
                let info = [];
                if (countPresent + countAbsent + countExcused + countUnmarked > 0) {
                    info = [{ header: header }, { Percentage: [attendance] }, { attendanceOfThisDay: merged }];
                }
                // return [{ header: header }, { Percentage: [percent] }, { attendanceOfThisDay: merged }];
                return info;
            } catch (error) {
                throw error;
            }


        } else {
            var header = [];
            let dbClientsName = createInstance(["client"], information);
            var clients = await dbClientsName.clients.findAll({
                where: {
                    clientName: {
                        like: ['%' + searchKey[0] + '%']
                    }
                },
                raw: true
            }); //code ends
            if (clients.length) {

                var isphoto = (clients[0].photoUrl === null) ? KNACK_UPLOAD_URL.localUrl + 'noimage.jpg' : KNACK_UPLOAD_URL.localUrl + clients[0].photoUrl;

                let dbClients = createInstance(["clientScheduleMap"], information);
                let allClients = await dbClients.clientschedulemap.findAll({
                    where: {
                        businessId: businessId,
                        scheduleId: scheduleId
                    }
                });
                var scheduleDate = [{ scheduleDate: attendanceDate }];

                let dbSchedule = createInstance(["clientSchedule"], information);
                let checkSchedule = await dbSchedule.schedule.findAll({
                    where: {
                        scheduleId: scheduleId,
                        status: 1
                    },
                    raw: true
                });
                dbSchedule.sequelize.close();
                let dbService = createInstance(["clientService"], information);
                let services = await dbService.services.findAll({
                    where: {
                        serviceId: checkSchedule[0].serviceId
                    }
                });
                dbService.sequelize.close();



                // Set Day and Time
                var stime = tConv24(checkSchedule[0].startTime);
                var etime = tConv24(checkSchedule[0].endTime);
                let stime = stime + '-' + etime;
                let dayint = convartday(checkSchedule[0].repeat_days);
                let day_time = ', ' + dayint + ', ' + stime;
                // End
                var booking = services[0].serviceName + day_time;

                var startTime = checkSchedule[0].startTime;
                var endTime = checkSchedule[0].endTime;

                var bookingName = [{ bookingName: booking }];
                var start = [{ startTime: startTime }];
                var end = [{ endTime: endTime }];


                var header = [];
                Array.prototype.push.apply(header, scheduleDate);
                Array.prototype.push.apply(header, bookingName);
                Array.prototype.push.apply(header, start);
                Array.prototype.push.apply(header, end);

                header = Object.assign.apply(Object, header);

                var merged = [];
                var countPresent = 0,
                    countAbsent = 0,
                    countExcused = 0,
                    countUnmarked = 0,
                    countTotal = 0;

                for (let i = 0; i < clients.length; i++) {
                    var mergedClientSchedule = [];
                    /* one client with attendance of a specific day */
                    let db = createInstance(["clientScheduleMap"], information);
                    let attendance = await db.clientschedulemap.findAll({
                        where: {
                            businessId: businessId,
                            scheduleId: scheduleId,
                            clientId: clients[i].clientId
                        }
                    });

                    if (attendance.length > 0) {
                        var attendanceKeys = [];
                        var attendanceValues = [];
                        Object.keys(attendance[0].attendanceType).forEach(function (key) {
                            attendanceKeys.push(key);
                            attendanceValues.push(attendance[0].attendanceType[key]);
                        });
                        var dateOfAttendance, attendanceText;
                        for (var i = 0; i < attendanceKeys.length; i++) {
                            if (attendanceKeys[i] == attendanceDate) {
                                dateOfAttendance = attendanceValues[i];
                                if (dateOfAttendance == 1) attendanceText = 'PRESENT';
                                else if (dateOfAttendance == 2) attendanceText = 'ABSENT';
                                else if (dateOfAttendance == 3) attendanceText = 'EXCUSED';
                                else if (dateOfAttendance == 4) attendanceText = 'U';
                                break;
                            } else dateOfAttendance = null;
                        }

                        var mergedOld = [];

                        Array.prototype.push.apply(mergedClientSchedule, [
                            { clientId: allClients[i].clientId }
                        ]);
                        Array.prototype.push.apply(mergedClientSchedule, [
                            { clientName: clients[0].clientName }
                        ]);
                        Array.prototype.push.apply(mergedClientSchedule, [
                            { clientPhoto: isphoto }
                        ]);
                        Array.prototype.push.apply(mergedClientSchedule, [
                            { attendance: dateOfAttendance }
                        ]);
                        Array.prototype.push.apply(mergedClientSchedule, [
                            { attendanceText: attendanceText }
                        ]);
                        mergedOld = Object.assign.apply(Object, mergedClientSchedule);

                        /* one client with attendance of a specific day */
                        db.sequelize.close();
                        merged = merged.concat(mergedOld);

                        if (dateOfAttendance) {
                            countTotal++;
                        }
                        if (dateOfAttendance == 1) {
                            countPresent++;
                        }
                        if (dateOfAttendance == 2) {
                            countAbsent++;
                        }
                        if (dateOfAttendance == 3) {
                            countExcused++;
                        }
                        if (dateOfAttendance == 4) {
                            countUnmarked++;
                        }
                    } else {
                        return [];
                    }

                    /* section for calculating percentage of present, absent and the rest depending on the total clients */
                    var perPresent = 100 / countTotal * countPresent;
                    var perAbsent = 100 / countTotal * countAbsent;
                    var perExcused = 100 / countTotal * countExcused;
                    var perUnmarked = 100 / countTotal * countUnmarked;
                    /* section ends here */

                    var percent = {
                        Present: perPresent,
                        Absent: perAbsent,
                        Excused: perExcused,
                        Unmarked: perUnmarked
                    };
                    return [{ header: header }, { Percentage: [percent] }, { attendanceOfThisDay: merged }];

                }
            } else {
                return [];
            }
        }
    };

    // Booking Attendance Record New
    newlistAttendance_ = async (
        information,
        scheduleId,
        attendanceDate,
        searchKey
        //attendanceType
    ) => {
        try {
            var businessIdinformation = information.split(",")[1];
            var db = createInstance(["clientSchedule"], information);
            let dbService = createInstance(["clientService"], information);
            let dbClientsName = createInstance(["client"], information);
            let dbClients = createInstance(["clientScheduleMap"], information);
            let dbCenter = createInstance(["clientCenter"], information);
            db.schedule.belongsTo(dbService.services, {
                foreignKey: "serviceId",
                targetKey: "serviceId"
            });
            db.schedule.belongsTo(dbCenter.centers, {
                foreignKey: "centerId",
                targetKey: "centerId"
            });
            dbClients.clientschedulemap.belongsTo(dbClientsName.clients, {
                foreignKey: "clientId",
                targetKey: "clientId"
            });

            let checkDate = await db.schedule.find({
                where: {
                    scheduleId: scheduleId,
                    status: 1
                },
                raw: true
            });

            // db.sequelize.close();
            let x = checkDate.scheduleTime;
            // console.log(x);
            let flag = 0;
            for (let s_time = 0; s_time < checkDate.scheduleTime.length; s_time++) {
                if (checkDate.scheduleTime[s_time].scdate == attendanceDate) {
                    flag = 1;
                    break;
                }
            }
            if (flag == 0) {
                return ("Data not found !");
            }
            var RecheduledDate = null;

            let allClients = await dbClients.clientschedulemap.findAll({
                where: {
                    businessId: businessIdinformation,
                    scheduleId: scheduleId
                }
            });
            var scheduleDate = [{ scheduleDate: attendanceDate }];

            //getting schedule name or batch name as booking name        
            let checkSchedule = await db.schedule.findAll({
                where: {
                    scheduleId: scheduleId,
                    status: 1
                },
                include: [
                    { model: dbService.services },
                    { model: dbCenter.centers },
                ],
                raw: true
            });

            if (!checkSchedule) {
                return ("Data not found 2!");
            }
            var data = checkSchedule[0].scheduleTime;


            for (let x = 0; x < data.length; x++) {

                if (data[x]["reschedule"] === 2) {
                    if (data[x]["rescheduleDate"] === attendanceDate) {
                        RecheduledDate = data[x]["rescheduleDate"];
                    }
                }
            }

            //console.log(checkSchedule["service.serviceName"]);
            var booking = checkSchedule[0].scheduleName;
            var startTime = checkSchedule[0].startTime;
            var endTime = checkSchedule[0].endTime;
            if (booking == null) {
                booking = checkSchedule["service.serviceName"];
            }
            var bookingName = [{ bookingName: booking }];
            var start = [{ startTime: startTime }];
            var end = [{ endTime: endTime }];
            var showscheduleId = [{ scheduleId: scheduleId }];
            var rescheduleMsg = [{ Reschedule: "Rescheduled from " + RecheduledDate }];
            var header = [];
            Array.prototype.push.apply(header, showscheduleId);
            Array.prototype.push.apply(header, scheduleDate);
            Array.prototype.push.apply(header, bookingName);
            Array.prototype.push.apply(header, start);
            Array.prototype.push.apply(header, end);
            Array.prototype.push.apply(header, [{ centerId: checkSchedule[0]['center.centerId'] }])
            Array.prototype.push.apply(header, [{ centerName: checkSchedule[0]['center.centerName'] }])
            if (RecheduledDate === null) {
                Array.prototype.push.apply(header, [{ Reschedule: "" }]);
            } else {
                Array.prototype.push.apply(header, rescheduleMsg);
            }
            header = Object.assign.apply(Object, header);
            var merged = [];
            var countPresent = 0,
                countAbsent = 0,
                countExcused = 0,
                countUnmarked = 0,
                countTotal = 0;
            for (var j = 0; j < allClients.length; j++) {
                var mergedClientSchedule = [];
                /* one client with attendance of a specific day */
                //let db = createInstance(["clientScheduleMap"], information);
                let attendance = await dbClients.clientschedulemap.findAll({
                    where: {
                        businessId: businessIdinformation,
                        scheduleId: scheduleId,
                        clientId: allClients[j].clientId,
                    },
                    include: [{
                        model: dbClientsName.clients,
                        attributes: {
                            include: [
                                "clientName", [
                                    dbClientsName.clients.sequelize.literal(
                                        'CASE WHEN photoUrl  is NULL THEN CONCAT("' +
                                        KNACK_UPLOAD_URL.localUrl +
                                        '" , "noimage.jpg" ) ELSE CONCAT("' +
                                        KNACK_UPLOAD_URL.localUrl +
                                        '" , photoUrl ) END'
                                    ),
                                    "photoUrl"
                                ],
                            ],
                            exclude: []
                        },
                        where: {
                            clientName: {
                                like: ['%' + searchKey[0] + '%']
                            }
                        }
                    }

                    ],
                    raw: true
                });
                //console.log(attendance);
                if (attendance.length > 0) {
                    var attendanceKeys = [];
                    var attendanceValues = [];
                    var dateOfAttendance;
                    var attendanceText;
                    var clientReschedule = "";
                    var clientRescheduleStatus = 1;
                    var clientRescheduleFrom = "";

                    //console.log(attendance[0]["attendanceType"]);
                    let data = attendance[0]["attendanceType"];
                    //console.log(data);
                    for (let x = 0; x < data.length; x++) {


                        attendanceKeys.push(data[x]["scdate"]);
                        attendanceValues.push(data[x]);

                        for (let i = 0; i < attendanceKeys.length; i++) {

                            //console.log(attendanceKeys[i]);
                            if (attendanceKeys[i] === attendanceDate) {
                                // console.log(attendanceValues[i]);
                                dateOfAttendance = attendanceValues[i];
                                ///console.log(dateOfAttendance);

                                if (dateOfAttendance["atd"] == 1) attendanceText = 'P';
                                else if (dateOfAttendance["atd"] == 2) attendanceText = 'A';
                                else if (dateOfAttendance["atd"] == 3 || dateOfAttendance["atd"] == 7) attendanceText = 'E';
                                else if (dateOfAttendance["atd"] == 4) attendanceText = 'U';
                                else attendanceText = "";
                                if (dateOfAttendance["atd"] == 7) clientRescheduleStatus = 2;
                                else clientRescheduleStatus = 1;
                                if (dateOfAttendance["atd"] == 7) clientReschedule = "Rescheduled by client to " + data[i]["rescheduleDate"];
                                else clientReschedule = "";
                                clientRescheduleFrom = data[i]["scheduleFrom"].trim() == "" ? "" : data[i]["scheduleFrom"];
                                break;
                            } else dateOfAttendance = null;
                        }
                    }

                    var mergedOld = [];
                    Array.prototype.push.apply(mergedClientSchedule, [
                        { clientId: allClients[j].clientId }
                    ]);
                    Array.prototype.push.apply(mergedClientSchedule, [
                        { clientName: attendance[0]["client.clientName"] }
                    ]);
                    Array.prototype.push.apply(mergedClientSchedule, [
                        { photoUrl: attendance[0]["client.photoUrl"] }
                    ]);
                    Array.prototype.push.apply(mergedClientSchedule, [
                        { attendance: dateOfAttendance }
                    ]);
                    Array.prototype.push.apply(mergedClientSchedule, [
                        { attendanceText: attendanceText }
                    ]);
                    Array.prototype.push.apply(mergedClientSchedule, [
                        { clientRescheduleStatus: clientRescheduleStatus }
                    ]);
                    Array.prototype.push.apply(mergedClientSchedule, [
                        { clientReschedule: clientReschedule }
                    ]);
                    Array.prototype.push.apply(mergedClientSchedule, [
                        { clientRescheduleFrom: clientRescheduleFrom }
                    ]);
                    mergedOld = Object.assign.apply(Object, mergedClientSchedule);

                    /* one client with attendance of a specific day */
                    merged = merged.concat(mergedOld);

                    if (dateOfAttendance) {
                        countTotal++;
                    }
                    if (dateOfAttendance["atd"] == 1) {
                        countPresent++;
                    }
                    if (dateOfAttendance["atd"] == 2) {
                        countAbsent++;
                    }
                    if (dateOfAttendance["atd"] == 3 || dateOfAttendance["atd"] == 7) {
                        countExcused++;
                    }
                    if (dateOfAttendance["atd"] == 4) {
                        countUnmarked++;
                    }
                }

            }

            /* section for calculating percentage of present, absent and the rest depending on the total clients */
            var perPresent = (100 / countTotal * countPresent).toFixed(0);
            var perAbsent = (100 / countTotal * countAbsent).toFixed(0);
            var perExcused = (100 / countTotal * countExcused).toFixed(0);
            var perUnmarked = (100 / countTotal * countUnmarked).toFixed(0);
            /* section ends here */

            var percent = {
                Present: perPresent,
                Absent: perAbsent,
                Excused: perExcused,
                Unmarked: perUnmarked
            };
            db.sequelize.close();
            dbService.sequelize.close();
            dbClientsName.sequelize.close();
            dbClients.sequelize.close();
            dbCenter.sequelize.close();
            return [{ header: header }, { Percentage: [percent] }, { attendanceOfThisDay: merged }];
        } catch (error) {
            throw error;
        }
    };

    newlistAttendance = async (
        information,
        scheduleId,
        attendanceDate,
        time,
        searchKey
        //attendanceType
    ) => {
        try {

            let s_date = attendanceDate.split("-"),
                s_time = time.split(":");
            let id = "" + s_date[0] + s_date[1] + s_date[2] + s_time[0] + s_time[1];
            var businessIdinformation = information.split(",")[1];
            var db = createInstance(["clientSchedule"], information);
            let dbService = createInstance(["clientService"], information);
            let dbClientsName = createInstance(["client"], information);
            let dbClients = createInstance(["clientScheduleMap"], information);
            let dbCenter = createInstance(["clientCenter"], information);
            db.schedule.belongsTo(dbService.services, {
                foreignKey: "serviceId",
                targetKey: "serviceId"
            });
            db.schedule.belongsTo(dbCenter.centers, {
                foreignKey: "centerId",
                targetKey: "centerId"
            });
            dbClients.clientschedulemap.belongsTo(dbClientsName.clients, {
                foreignKey: "clientId",
                targetKey: "clientId"
            });

            let checkDate = await db.schedule.find({
                where: {
                    scheduleId: scheduleId,
                    status: 1
                },
                raw: true
            });

            // db.sequelize.close();
            let x = checkDate.scheduleTime;
            // console.log(x);
            let flag = 0;
            for (let s_time = 0; s_time < checkDate.scheduleTime.length; s_time++) {
                if (checkDate.scheduleTime[s_time].scdate == attendanceDate && checkDate.scheduleTime[s_time].scheduleStartTime == time) {
                    flag = 1;
                    break;
                }
            }
            if (flag == 0) {
                return [];
            }
            var RescheduleDate = 0;

            let allClients = await dbClients.clientschedulemap.findAll({
                where: {
                    businessId: businessIdinformation,
                    scheduleId: scheduleId
                }
            });
            var scheduleDate = [{ scheduleDate: attendanceDate }];

            //getting schedule name or batch name as booking name        
            let checkSchedule = await db.schedule.findAll({
                where: {
                    scheduleId: scheduleId,
                    status: 1
                },
                include: [
                    { model: dbService.services },
                    { model: dbCenter.centers },
                ],
                raw: true
            });

            if (!checkSchedule) {
                return [];
            }
            let data = checkSchedule[0].scheduleTime;



            for (let x = 0; x < data.length; x++) {

                if (data[x]["scdate"] === attendanceDate && data[x]["scheduleStartTime"] === time) {
                    console.log(data[x]);
                    RescheduleDate = data[x]["scheduleFrom"];

                }
            }

            //console.log(checkSchedule["service.serviceName"]);
            var booking = checkSchedule[0].scheduleName;
            var startTime = checkSchedule[0].startTime;
            var endTime = checkSchedule[0].endTime;
            if (booking == null) {
                booking = checkSchedule["service.serviceName"];
            }
            var bookingName = [{ bookingName: booking }];
            var start = [{ startTime: startTime }];
            var end = [{ endTime: endTime }];
            var showscheduleId = [{ scheduleId: scheduleId }];

            if (RescheduleDate == 0) {
                var rescheduleMsg = [{ Reschedule: "" }];
            } else {
                var rescheduleMsg = [{ Reschedule: "Rescheduled From " + RescheduleDate }];
            }

            var header = [];
            Array.prototype.push.apply(header, showscheduleId);
            Array.prototype.push.apply(header, scheduleDate);
            Array.prototype.push.apply(header, bookingName);
            Array.prototype.push.apply(header, start);
            Array.prototype.push.apply(header, end);
            Array.prototype.push.apply(header, [{ centerId: checkSchedule[0]['center.centerId'] }])
            Array.prototype.push.apply(header, [{ centerName: checkSchedule[0]['center.centerName'] }])
            if (RescheduleDate === null) {
                Array.prototype.push.apply(header, [{ Reschedule: "" }]);
            } else {
                Array.prototype.push.apply(header, rescheduleMsg);
            }
            header = Object.assign.apply(Object, header);
            var merged = [];
            var countPresent = 0,
                countAbsent = 0,
                countExcused = 0,
                countUnmarked = 0,
                countTotal = 0;
            for (var j = 0; j < allClients.length; j++) {
                if (allClients[j].attendanceType.length > 0) {
                    var mergedClientSchedule = [];
                    /* one client with attendance of a specific day */
                    //let db = createInstance(["clientScheduleMap"], information);
                    let attendance = await dbClients.clientschedulemap.findAll({
                        where: {
                            businessId: businessIdinformation,
                            scheduleId: scheduleId,
                            clientId: allClients[j].clientId,
                        },
                        include: [{
                            model: dbClientsName.clients,
                            attributes: {
                                include: [
                                    "clientName", [
                                        dbClientsName.clients.sequelize.literal(
                                            'CASE WHEN photoUrl  is NULL THEN CONCAT("' +
                                            KNACK_UPLOAD_URL.localUrl +
                                            '" , "noimage.jpg" ) ELSE CONCAT("' +
                                            KNACK_UPLOAD_URL.localUrl +
                                            '" , photoUrl ) END'
                                        ),
                                        "photoUrl"
                                    ],
                                ],
                                exclude: []
                            },
                            where: {
                                clientName: {
                                    like: ['%' + searchKey[0] + '%']
                                }
                            }
                        }

                        ],
                        raw: true
                    });
                    //console.log(attendance);
                    if (attendance.length > 0) {
                        var attendanceKeys = [];
                        var attendanceValues = [];
                        var dateOfAttendance;
                        var attendanceText;
                        var clientReschedule = "";
                        var clientRescheduleStatus = 1;
                        var clientRescheduleFrom = "";

                        //console.log(attendance[0]["attendanceType"]);
                        let data = attendance[0]["attendanceType"];
                        //console.log(data);
                        for (let x = 0; x < data.length; x++) {


                            attendanceKeys.push(data[x]["scdate"]);
                            attendanceValues.push(data[x]);

                            for (let i = 0; i < attendanceKeys.length; i++) {

                                //console.log(attendanceKeys[i]);
                                if (attendanceKeys[i] === attendanceDate) {
                                    // console.log(attendanceValues[i]);
                                    dateOfAttendance = attendanceValues[i];
                                    ///console.log(dateOfAttendance);

                                    if (dateOfAttendance["atd"] == 1) attendanceText = 'P';
                                    else if (dateOfAttendance["atd"] == 2) attendanceText = 'A';
                                    else if (dateOfAttendance["atd"] == 3 || dateOfAttendance["atd"] == 7) attendanceText = 'E';
                                    else if (dateOfAttendance["atd"] == 4) attendanceText = 'U';
                                    else attendanceText = "";
                                    if (dateOfAttendance["atd"] == 7) clientRescheduleStatus = 2;
                                    else clientRescheduleStatus = 1;
                                    if (dateOfAttendance["atd"] == 7) clientReschedule = "Rescheduled by client to " + data[i]["rescheduleDate"];
                                    else clientReschedule = "";
                                    clientRescheduleFrom = data[i]["scheduleFrom"].trim() == "" ? "" : data[i]["scheduleFrom"];
                                    break;
                                } else dateOfAttendance = '';
                            }
                        }

                        var mergedOld = [];
                        Array.prototype.push.apply(mergedClientSchedule, [
                            { clientId: allClients[j].clientId }
                        ]);
                        Array.prototype.push.apply(mergedClientSchedule, [
                            { clientName: attendance[0]["client.clientName"] }
                        ]);
                        Array.prototype.push.apply(mergedClientSchedule, [
                            { photoUrl: attendance[0]["client.photoUrl"] }
                        ]);
                        Array.prototype.push.apply(mergedClientSchedule, [
                            { attendance: dateOfAttendance }
                        ]);
                        Array.prototype.push.apply(mergedClientSchedule, [
                            { attendanceText: attendanceText }
                        ]);
                        Array.prototype.push.apply(mergedClientSchedule, [
                            { clientRescheduleStatus: clientRescheduleStatus }
                        ]);
                        Array.prototype.push.apply(mergedClientSchedule, [
                            { clientReschedule: clientReschedule }
                        ]);
                        Array.prototype.push.apply(mergedClientSchedule, [
                            { clientRescheduleFrom: clientRescheduleFrom }
                        ]);
                        mergedOld = Object.assign.apply(Object, mergedClientSchedule);

                        /* one client with attendance of a specific day */
                        merged = merged.concat(mergedOld);

                        if (dateOfAttendance) {
                            countTotal++;
                        }
                        if (dateOfAttendance["atd"] == 1) {
                            countPresent++;
                        }
                        if (dateOfAttendance["atd"] == 2) {
                            countAbsent++;
                        }
                        if (dateOfAttendance["atd"] == 3 || dateOfAttendance["atd"] == 7) {
                            countExcused++;
                        }
                        if (dateOfAttendance["atd"] == 4) {
                            countUnmarked++;
                        }
                    }

                }
            }

            /* section for calculating percentage of present, absent and the rest depending on the total clients */
            var perPresent = (100 / countTotal * countPresent).toFixed(0);
            var perAbsent = (100 / countTotal * countAbsent).toFixed(0);
            var perExcused = (100 / countTotal * countExcused).toFixed(0);
            var perUnmarked = (100 / countTotal * countUnmarked).toFixed(0);
            /* section ends here */

            var percent = {
                Present: countPresent,
                Absent: countAbsent,
                Excused: countExcused,
                Unmarked: countUnmarked
            };
            db.sequelize.close();
            dbService.sequelize.close();
            dbClientsName.sequelize.close();
            dbClients.sequelize.close();
            dbCenter.sequelize.close();
            return [{ header: header }, { Percentage: [percent] }, { attendanceOfThisDay: merged }];
        } catch (error) {
            throw error;
        }
    };

    showAttendance = async (
        information,
        businessId,
        scheduleId,
        attendanceDate
    ) => {
        try {
            let dbClients = createInstance(["clientScheduleMap"], information);
            let allClients = await dbClients.clientschedulemap.findAll({
                where: {
                    businessId: businessId,
                    scheduleId: scheduleId
                }
            });

            var merged = [];
            for (var j = 0; j < allClients.length; j++) {
                var mergedClientSchedule = [];
                /* one client with attendance of a specific day */
                let db = createInstance(["clientScheduleMap"], information);
                let attendance = await db.clientschedulemap.findAll({
                    where: {
                        businessId: businessId,
                        scheduleId: scheduleId,
                        clientId: allClients[j].clientId
                    }
                });

                //getting the name of the client from clients table
                let dbClientsName = createInstance(["client"], information);
                let clients = await dbClientsName.clients.findAll({
                    where: {
                        clientId: allClients[j].clientId
                    },
                    raw: true
                }); //code ends

                var attendanceKeys = [];
                var attendanceValues = [];
                Object.keys(attendance[0].attendanceType).forEach(function (key) {
                    attendanceKeys.push(key);
                    attendanceValues.push(attendance[0].attendanceType[key]);
                });

                var dateOfAttendance, attendanceText;
                for (var i = 0; i < attendanceKeys.length; i++) {
                    if (attendanceKeys[i] == attendanceDate) {
                        dateOfAttendance = attendanceValues[i];
                        if (dateOfAttendance == 1) attendanceText = "P";
                        else if (dateOfAttendance == 2) attendanceText = "A";
                        else if (dateOfAttendance == 3) attendanceText = "E";
                        else if (dateOfAttendance == 4) attendanceText = "U";
                        break;
                    } else dateOfAttendance = null;
                }

                var mergedOld = [];

                Array.prototype.push.apply(mergedClientSchedule, [
                    { clientId: allClients[j].clientId }
                ]);
                Array.prototype.push.apply(mergedClientSchedule, [
                    { clientName: clients[0].clientName }
                ]);
                Array.prototype.push.apply(mergedClientSchedule, [
                    { attendance: dateOfAttendance }
                ]);
                Array.prototype.push.apply(mergedClientSchedule, [
                    { attendanceText: attendanceText }
                ]);

                mergedOld = Object.assign.apply(Object, mergedClientSchedule);
                db.sequelize.close();
                merged = merged.concat(mergedOld);
            }
            return await merged;
        } catch (error) {
            throw error;
        }
    };

    attendance = [];
    createAttendance = async (
        information,
        businessId,
        scheduleId,
        clientId,
        attendanceType
    ) => {
        try {
            //code for getting dates and wrapping into json ends here

            var std = "2018-02-12";
            var sdate = std.split("-");
            var stYear = parseInt(sdate[0]);
            var stMonth = parseInt(sdate[1]) - 1;
            var stDate = parseInt(sdate[2]);

            var end = "2018-04-04";
            var edate = end.split("-");
            var enYear = parseInt(edate[0]);
            var enMonth = parseInt(edate[1]) - 1;
            var enDate = parseInt(edate[2]);

            var interval = 3;

            //..................this code will collect the dates by the interval
            // Returns an array of dates between the two dates
            var getDates = function (startDate, endDate) {
                var dates = [],
                    currentDate = startDate,
                    addDays = function (days) {
                        var date = new Date(this.valueOf());
                        date.setDate(date.getDate() + days);
                        return date;
                    };
                while (currentDate <= endDate) {
                    dates.push(currentDate);
                    currentDate = addDays.call(currentDate, 1);
                }
                return dates;
            };

            // Usage
            var dates = getDates(
                new Date(stYear, stMonth, stDate),
                new Date(enYear, enMonth, enDate)
            );
            var i = 0;
            var newYear, newMonth, newDate;
            var arrayOfDate = new Array();
            dates.forEach(function (date) {
                //console.log(date);
                newYear = dates[i].getFullYear();
                newMonth = dates[i].getMonth() + 1;
                newDate = dates[i].getDate();
                if (newMonth >= 0 && newMonth < 10) newMonth = "0" + newMonth;
                if (newDate >= 0 && newDate < 10) newDate = "0" + newDate;

                //console.log(newYear + '-' + newMonth + '-' + newDate);
                arrayOfDate[i] = newYear + "-" + newMonth + "-" + newDate;
                i++;
            });

            //...........wrapping into JSON object...
            var output = {};
            for (var it = 0; it < arrayOfDate.length; it = it + interval) {
                //console.log('mydate: '+arrayOfDate[2]);
                //console.log('mydate: ' + arrayOfDate[it]);
                output[arrayOfDate[it]] = "4";
            }
            var finalAttendance = JSON.stringify(output);
            if (finalAttendance.includes('"')) {
                attendanceType = JSON.parse(finalAttendance);
            }
            // console.log(JSON.stringify(output));
            //var attendanceType = { "2018-05-12": "5", "2018-04-03": "5" };
            // console.log(attendanceType);

            //.....code for getting dates and wrapping into json ends here

            const newAttendance = {
                businessId,
                scheduleId,
                clientId,
                attendanceType
            };

            let db = createInstance(["clientScheduleMap"], information);
            let checkSchedule = await db.clientschedulemap.findAll({
                where: {
                    clientId: clientId,
                    status: 1
                },
                raw: true
            });

            if (!checkSchedule[0]) {
                let attendance = await db.clientschedulemap.create(newAttendance);
                db.sequelize.close();
                return await filterFields(attendance, PUBLIC_FIELDS, true);
            } else {
                db.sequelize.close();
                throw new ApplicationError(
                    "Selected client is already scheduled for selected date and time",
                    409
                );
            }
        } catch (error) {
            throw error;
        }
    };

    // Change date 11-06-18
    // ClientBookingAttendence = async (
    //   information,
    //   businessId,
    //   scheduleId,
    //   clientId,
    //   searchKey
    // ) => {
    //   try {
    //     let Present = 0;
    //     let Absent = 0;
    //     let Execused = 0;
    //     let Unmarked = 0;
    //     let Rescheduled = 0;
    //     let Arr = [];
    //     let dbClients = createInstance(["clientScheduleMap"], information);
    //     let dbSubscription = createInstance(["clientSubscription"], information);
    //     let dbPricepack = createInstance(["clientPricepack"], information);
    //     let dbTeam = createInstance(["clientEmployee"], information);
    //     let db = createInstance(["clientSchedule"], information);

    //     dbClients.clientschedulemap.belongsTo(db.schedule, {
    //       foreignKey: "scheduleId",
    //       targetKey: "scheduleId"
    //     })
    //     dbClients.clientschedulemap.belongsTo(dbSubscription.subscriptions, {
    //       foreignKey: "subscriptionId",
    //       targetKey: "subscriptionId"
    //     });
    //     dbSubscription.subscriptions.belongsTo(dbPricepack.pricepacks, {
    //       foreignKey: "pricepacks",
    //       targetKey: "pricepackId"
    //     });
    //     let allClients = await dbClients.clientschedulemap.findAll({
    //       include: [{ model: dbSubscription.subscriptions, include: [{ model: dbPricepack.pricepacks }] }, { model: db.schedule }],
    //       where: {
    //         businessId: businessId,
    //         scheduleId: scheduleId,
    //         clientId: clientId,
    //         status: 1
    //       },

    //       raw: true
    //     });

    //     let Obj = {};
    //     let HeaderArr = [];
    //     Obj["PricePackName"] = allClients[0]["subscription.pricepack.pricepackName"];
    //     Obj["startTime"] = allClients[0]["schedule.startTime"];
    //     Obj["endTime"] = allClients[0]["schedule.endTime"];
    //     HeaderArr.push(Obj);
    //     var merged = [];

    //     var attendanceKeys = [];
    //     var attendanceValues = [];

    //     let Attendance = allClients[0]["attendanceType"];

    //     for (let i = 0; i < Attendance.length; i++) {
    //       attendanceKeys.push(Attendance[i]["scdate"]);
    //       attendanceValues.push(Attendance[i]);
    //     }

    //     let AttendancebodyArr = [];
    //     for (let i = 0; i < attendanceKeys.length; i++) {
    //       let dateOfAttendance, attendanceText, RescheduleDate;
    //       let Obj1 = {};
    //       dateOfAttendance = attendanceValues[i];
    //       if (dateOfAttendance["atd"] == 1) { attendanceText = 'P'; RescheduleDate = dateOfAttendance["scheduleFrom"]; Present++; }
    //       else if (dateOfAttendance["atd"] == 2) { attendanceText = 'A'; RescheduleDate = dateOfAttendance["scheduleFrom"]; Absent++; }
    //       else if (dateOfAttendance["atd"] == 3 || dateOfAttendance["atd"] == 7) { attendanceText = 'E'; RescheduleDate = dateOfAttendance["scheduleFrom"]; Execused++; }
    //       else if (dateOfAttendance["atd"] == 4) { attendanceText = 'U'; RescheduleDate = dateOfAttendance["scheduleFrom"]; Unmarked++; }
    //       else if (dateOfAttendance["atd"] == 6) {
    //         attendanceText = 'RS'; RescheduleDate = dateOfAttendance["scheduleFrom"];
    //         Rescheduled++;
    //       }
    //       Obj1["Date"] = attendanceKeys[i];
    //       Obj1["Attendance"] = attendanceText;
    //       Obj1["RescheduleFrom"] = RescheduleDate;
    //       Obj1["atdBy"] = dateOfAttendance["atdBy"];
    //       Obj1["atdDate"] = dateOfAttendance["atdDate"];
    //       Obj1["atdTime"] = dateOfAttendance["atdTime"];
    //       // let employees = await dbteam.employees.findAll({
    //       //   where: {

    //       //   }, raw: true
    //       // });
    //       if (dateOfAttendance["rescheduleBy"]) {
    //         if (dateOfAttendance["rescheduleBy"].trim() == "") {
    //           Obj1["RescheduleBy"] = "";
    //         } else {
    //           let employees = await viewEmployee(information, dbTeam, {
    //             employeeId: dateOfAttendance["rescheduleBy"]
    //           });
    //           Obj1["RescheduleBy"] = employees[0].employeeName;
    //         }
    //       } else {
    //         Obj1["RescheduleBy"] = "";
    //       }
    //       Obj1["RescheduleById"] = dateOfAttendance["rescheduleBy"];
    //       Obj1["RescheduledDate"] = dateOfAttendance["rescheduleDate"];
    //       AttendancebodyArr.push(Obj1);

    //     }
    //     let Obj3 = {}; let Obj3Arr = [];
    //     Obj3["Present"] = Present;
    //     Obj3["Absent"] = Absent;
    //     Obj3["Absent"] = Absent;
    //     Obj3["Unmarked"] = Unmarked;
    //     Obj3["Execused"] = Rescheduled + "/" + Execused;
    //     Obj3Arr.push(Obj3);
    //     Array.prototype.push.apply(Arr, [{ Header: HeaderArr }]);

    //     if (searchKey[0] != "") {

    //       for (let i = 0; i < AttendancebodyArr.length; i++) {
    //         let date = AttendancebodyArr[i]["Date"];
    //         if (date.includes(searchKey[0])) {
    //           Array.prototype.push.apply(Arr, [{ Attendance: AttendancebodyArr[i] }]);
    //           return Arr;
    //         }
    //       }
    //     }
    //     Array.prototype.push.apply(Arr, [{ Attendance: AttendancebodyArr }]);
    //     Array.prototype.push.apply(Arr, [{ DataCounts: Obj3Arr }]);

    //     return Arr;
    //   } catch (error) {
    //     throw error;
    //   }
    // };

    ClientBookingAttendence = async (
        information,
        businessId,
        scheduleId,
        clientId,
        searchKey
    ) => {
        try {
            let Present = 0;
            let Absent = 0;
            let Execused = 0;
            let Unmarked = 0;
            let Rescheduled = 0;
            let Arr = [];
            let dbClients = createInstance(["clientScheduleMap"], information);
            let dbSubscription = createInstance(["clientSubscription"], information);
            let dbPricepack = createInstance(["clientPricepack"], information);
            let dbTeam = createInstance(["clientEmployee"], information);
            let db = createInstance(["clientSchedule"], information);

            dbClients.clientschedulemap.belongsTo(db.schedule, {
                foreignKey: "scheduleId",
                targetKey: "scheduleId"
            })
            dbClients.clientschedulemap.belongsTo(dbSubscription.subscriptions, {
                foreignKey: "subscriptionId",
                targetKey: "subscriptionId"
            });
            dbSubscription.subscriptions.belongsTo(dbPricepack.pricepacks, {
                foreignKey: "pricepacks",
                targetKey: "pricepackId"
            });
            let allClients = await dbClients.clientschedulemap.findAll({
                include: [{ model: dbSubscription.subscriptions, include: [{ model: dbPricepack.pricepacks }] }, { model: db.schedule }],
                where: {
                    businessId: businessId,
                    scheduleId: scheduleId,
                    clientId: clientId,
                    status: 1
                },

                raw: true
            });

            let Obj = {};
            let HeaderArr = [];
            Obj["PricePackName"] = allClients[0]["subscription.pricepack.pricepackName"];
            Obj["startTime"] = allClients[0]["schedule.startTime"];
            Obj["endTime"] = allClients[0]["schedule.endTime"];
            HeaderArr.push(Obj);
            var merged = [];

            var attendanceKeys = [];
            var attendanceValues = [];

            let Attendance = allClients[0]["attendanceType"];

            for (let i = 0; i < Attendance.length; i++) {
                attendanceKeys.push(Attendance[i]["scdate"]);
                attendanceValues.push(Attendance[i]);
            }

            let AttendancebodyArr = [];
            for (let i = 0; i < attendanceKeys.length; i++) {
                let dateOfAttendance, attendanceText, RescheduleDate;
                let Obj1 = {};
                dateOfAttendance = attendanceValues[i];
                if (dateOfAttendance["atd"] == 1) {
                    attendanceText = 'P';
                    RescheduleDate = dateOfAttendance["scheduleFrom"];
                    Present++;
                } else if (dateOfAttendance["atd"] == 2) {
                    attendanceText = 'A';
                    RescheduleDate = dateOfAttendance["scheduleFrom"];
                    Absent++;
                } else if (dateOfAttendance["atd"] == 3 || dateOfAttendance["atd"] == 7) {
                    attendanceText = 'E';
                    RescheduleDate = dateOfAttendance["scheduleFrom"];
                    Execused++;
                } else if (dateOfAttendance["atd"] == 4) {
                    attendanceText = 'U';
                    RescheduleDate = dateOfAttendance["scheduleFrom"];
                    Unmarked++;
                } else if (dateOfAttendance["atd"] == 6) {
                    attendanceText = 'RS';
                    RescheduleDate = dateOfAttendance["scheduleFrom"];
                    Rescheduled++;
                }
                Obj1["Date"] = attendanceKeys[i];
                Obj1["Attendance"] = attendanceText;
                Obj1["RescheduleFrom"] = RescheduleDate;
                Obj1["atdBy"] = dateOfAttendance["atdBy"];
                Obj1["atdDate"] = dateOfAttendance["atdDate"];
                Obj1["atdTime"] = dateOfAttendance["atdTime"];
                // let employees = await dbteam.employees.findAll({
                //   where: {

                //   }, raw: true
                // });

                for (let j of allClients[0]["schedule.scheduleTime"]) {
                    // console.log(dateOfAttendance["scdate"]);
                    if (j.scdate == dateOfAttendance["scdate"]) {
                        if (j.rescheduleBy) {
                            if (j.rescheduleBy.trim() == "") {
                                Obj1["RescheduleBy"] = "";
                            } else {
                                let employees = await viewEmployee(information, dbTeam, {
                                    employeeId: j.rescheduleBy
                                });
                                Obj1["RescheduleBy"] = employees[0].employeeName;
                            }
                        } else {
                            Obj1["RescheduleBy"] = "";
                        }
                        Obj1["RescheduleById"] = j.rescheduleBy;
                        Obj1["RescheduledDate"] = j.scdate;
                    }
                }
                AttendancebodyArr.push(Obj1);
            }
            let Obj3 = {};
            let Obj3Arr = [];
            Obj3["Present"] = Present;
            Obj3["Absent"] = Absent;
            Obj3["Absent"] = Absent;
            Obj3["Unmarked"] = Unmarked;
            Obj3["Execused"] = Rescheduled + "/" + Execused;
            Obj3Arr.push(Obj3);
            Array.prototype.push.apply(Arr, [{ Header: HeaderArr }]);

            if (searchKey[0] != "") {

                for (let i = 0; i < AttendancebodyArr.length; i++) {
                    let date = AttendancebodyArr[i]["Date"];
                    if (date.includes(searchKey[0])) {
                        Array.prototype.push.apply(Arr, [{ Attendance: AttendancebodyArr[i] }]);
                        return Arr;
                    }
                }
            }
            Array.prototype.push.apply(Arr, [{ Attendance: AttendancebodyArr }]);
            Array.prototype.push.apply(Arr, [{ DataCounts: Obj3Arr }]);

            return Arr;
        } catch (error) {
            throw error;
        }
    };

    showClientAttendanceReport = async (
        information,
        clientId,
        scheduleId,
        status,
        option
    ) => {
        try {


            let arr = information.split(",");
            let clientsArr = clientId;
            let scheduleArr = scheduleId;
            let statusArr = status;
            if (clientsArr.length != scheduleArr.length) {
                return ["Mismatch in no of clients and schedule Id"];
            }
            let businessId = arr[1];
            let info = [];
            let html_info = [];


            let dbClientScheduleMap = createInstance(["clientScheduleMap"], information);
            let dbClientsName = createInstance(["client"], information);
            let dbSchedule = createInstance(["clientSchedule"], information);
            let dbService = createInstance(["clientService"], information);
            let dbCenterId = createInstance(["clientCenter"], information);
            let dbSubscription = createInstance(["clientSubscription"], information);
            let dbPricepack = createInstance(["clientPricepack"], information);
            let dbSPM = createInstance(["servicePricepackMap"], information);
            let x = await BusinessSchema.Business().findAll({
                where: {
                    businessId: businessId
                },
                raw: true
            });
            dbClientScheduleMap.clientschedulemap.belongsTo(dbClientsName.clients, {
                foreignKey: "clientId",
                targetKey: "clientId"
            });
            dbClientScheduleMap.clientschedulemap.belongsTo(dbSchedule.schedule, {
                foreignKey: "scheduleId",
                targetKey: "scheduleId"
            });
            dbClientScheduleMap.clientschedulemap.belongsTo(dbSubscription.subscriptions, {
                foreignKey: "subscriptionId",
                targetKey: "subscriptionId"
            });
            dbSubscription.subscriptions.belongsTo(dbPricepack.pricepacks, {
                foreignKey: "pricepacks",
                targetKey: "pricepackId"
            });
            dbSchedule.schedule.belongsTo(dbService.services, {
                foreignKey: "serviceId",
                targetKey: "serviceId"
            });
            dbSchedule.schedule.belongsTo(dbCenterId.centers, {
                foreignKey: "centerId",
                targetKey: "centerId"
            });
            dbSPM.servicepricepackmap.belongsTo(dbService.services, {
                foreignKey: "serviceId",
                targetKey: "serviceId"
            });
            for (let i = 0; i < scheduleArr.length; i++) {
                let FinalInfo = {};
                let info = [];
                let attendanceInfo = [];
                let att = [];

                if (statusArr[i] == 1) {
                    let data = await dbClientScheduleMap.clientschedulemap.findAll({
                        include: [{
                            model: dbSchedule.schedule,
                            include: [{
                                model: dbService.services
                            }, {
                                model: dbCenterId.centers
                            }]
                        },
                        {
                            model: dbClientsName.clients
                        },
                        {
                            model: dbSubscription.subscriptions,
                            include: [{
                                model: dbPricepack.pricepacks
                            }]
                        }
                        ],
                        where: {
                            status: 1,
                            scheduleId: scheduleArr[i],
                            businessId: businessId,
                            clientId: clientsArr[i]
                        },
                        raw: true
                    });


                    if (data.length > 0) {


                        // let getObj = {
                        //     status: 1,
                        //     subscriptionId: data[0]["subscription.subscriptionId"],
                        //     serviceId: data[0]["schedule.service.serviceId"]
                        // }
                        // let xyz = await sessionInfoBasedOnService(information, getObj);

                        let getObj = {
                            status: 1,
                            subscriptionId: data[0]["subscription.subscriptionId"],
                            serviceId: data[0]["schedule.service.serviceId"],
                            scheduleId: scheduleId,
                        }
                        let xyz = await sessionCount(information, getObj);

                        let Details = {};
                        Details["Name"] = data[0]["client.clientName"];
                        Details["ContactNumber"] = data[0]["client.contactNumber"];
                        Details["Center"] = data[0]["schedule.center.centerName"];
                        Details["PricePack"] = data[0]["subscription.pricepack.pricepackName"];
                        Details["Sessions"] = xyz.message;
                        FinalInfo["User"] = x;
                        FinalInfo["Data"] = [Details];
                        let AttendanceObject = data[0]["attendanceType"];
                        for (let i = 0; i < AttendanceObject.length; i++) {
                            let Obj = {};
                            if (option == 2) {


                                let tday = new Date().getTime();
                                let check = new Date(AttendanceObject[i]["scdate"]).getTime();
                                if (check <= tday) {
                                    Obj["Date"] = AttendanceObject[i]["scdate"];
                                    Obj["Service"] = data[0]["schedule.service.serviceName"];
                                    Obj["AttendanceValue"] = AttendanceObject[i]["atd"];
                                    Obj["PricePack"] = AttendanceObject[i]["subscription.pricepack.pricepackName"];
                                    if (AttendanceObject[i]["atd"] === 1) {
                                        Obj["Attendance"] = "Present"
                                    } else if (AttendanceObject[i]["atd"] === 2) {
                                        Obj["Attendance"] = "Absent"
                                    } else if (AttendanceObject[i]["atd"] === 4) {
                                        Obj["Attendance"] = "Unmarked"
                                    } else {
                                        if (AttendanceObject[i]["atd"] === 3 || AttendanceObject[i]["atd"] === 6 || AttendanceObject[i]["atd"] === 7) {
                                            if (AttendanceObject[i]["reschedule"] == 2) {
                                                Obj["Attendance"] = "Execused" + " rescheduled to " + AttendanceObject[i]["rescheduleDate"];
                                            } else {
                                                Obj["Attendance"] = "Execused";
                                            }
                                        }
                                    }
                                    if (AttendanceObject[i]["scheduleFrom"]) {
                                        Obj["scheduleFrom"] = AttendanceObject[i]["scheduleFrom"];
                                    } else {
                                        Obj["scheduleFrom"] = "";
                                    }

                                    att.push(Obj);
                                }

                            } else {


                                Obj["Date"] = AttendanceObject[i]["scdate"];
                                Obj["Service"] = data[0]["schedule.service.serviceName"];
                                Obj["AttendanceValue"] = AttendanceObject[i]["atd"];
                                Obj["PricePack"] = AttendanceObject[i]["subscription.pricepack.pricepackName"];
                                if (AttendanceObject[i]["atd"] === 1) {
                                    Obj["Attendance"] = "Present"
                                } else if (AttendanceObject[i]["atd"] === 2) {
                                    Obj["Attendance"] = "Absent"
                                } else if (AttendanceObject[i]["atd"] === 4) {
                                    Obj["Attendance"] = "Unmarked"
                                } else {
                                    if (AttendanceObject[i]["atd"] === 3 || AttendanceObject[i]["atd"] === 6 || AttendanceObject[i]["atd"] === 7) {
                                        if (AttendanceObject[i]["reschedule"] == 2) {
                                            Obj["Attendance"] = "Execused" + " rescheduled to " + AttendanceObject[i]["rescheduleDate"];
                                        } else {
                                            Obj["Attendance"] = "Execused";
                                        }
                                    }
                                }
                                if (AttendanceObject[i]["scheduleFrom"]) {
                                    Obj["scheduleFrom"] = AttendanceObject[i]["scheduleFrom"];
                                } else {
                                    Obj["scheduleFrom"] = "";
                                }
                                att.push(Obj);
                            }
                        }

                        FinalInfo["Attendance"] = att;
                        info.push(FinalInfo);
                        html_info = [];


                        let filename = clientsArr[i] + new Date().getTime();
                        var content = await makeEmailTemplate("src/emailTemplate/multiple_AttendanceReport.js", info);
                        fs.writeFile("uploads/attendance/" + filename + ".html", content, function (err) {
                            if (err) {
                                return console.log(err);
                            } else {
                                var html = fs.readFileSync('uploads/attendance/' + filename + '.html', 'utf8');
                                pdf1.create(html, {
                                    format: 'Letter'
                                }).toFile('uploads/attendance/pdf/' + filename + '.pdf', function (err, res) {
                                    if (err) return console.log(err);
                                    else {

                                    }
                                });
                            }
                        })

                        let link = UPLOAD_DIR.localUrl + "attendance/pdf/" + filename + '.pdf';
                        FinalInfo["AttendancePdf"] = link;
                        html_info.push(FinalInfo);

                        console.log("---------------------------------------------------" + data[0]["client.emailId"]);
                        //  Send mail
                        sendEmail(data[0]["client.emailId"], "ATTENDANCE", "ATTENDANCE SHEET", {
                            filename: filename + '.pdf',
                            path: 'uploads/attendance/pdf/' + filename + '.pdf',
                            contentType: "application/pdf"
                        });
                    }
                    return html_info;
                }

            }
            dbClientScheduleMap.sequelize.close();
            dbClientsName.sequelize.close();
            dbSchedule.sequelize.close();
            dbService.sequelize.close();
            dbCenterId.sequelize.close();
            dbSubscription.sequelize.close();
            dbPricepack.sequelize.close();
            return info;
        } catch (error) {
            throw error;
        }
    };

}
export default new AttendanceDB();