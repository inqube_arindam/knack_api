import { filterFields, createInstance, commaSeparatedValues, getAttendance, getScheduleTimeTable, hyphenDateFormat, intersect_arrays, diff_arrays, attendanceInfo, tConv24, convartday, attendanceInfoOfClientAndDate, rsPaginate, sessionInfo, getDayOfMonth, countAttendance, getRepeatDays, attendanceInfoCalculate, CheckMainOrCenterAdmin, toLogEntry, getAttendanceJSON, bookingInfo, bookingCountInfo, sessionCalc, getAttendanceSheet, getBookingGraph, getAtdSheet, expiryDate, sendSmsMobile, sendEmail } from "../../lib/common";
import { ApplicationError } from "../../lib/errors";
import { start } from "repl";
import Sequelize from "sequelize";
import BaseModel from "../../db/BaseModel";
import ClientScheduleMapModel from "./ClientScheduleMapModel";
import ClientBatchModel from "./ClientBatchModel";
import {
  KNACKDB,
  KNACK_DBSETTINGS,
  KNACK_UPLOAD_URL,
  KNACK_TEAM_UPLOAD_URL,
  PEGINATION_LIMIT
} from "../../lib/constants.js";

import { ongoingSubSchedule, pastSubSchedule, currentWeekSchedule, currentMonthSchedule } from "../../db/ClientHelperModels/ClientScheduleHelperModel";

import loginSignUpHelperDB from "../HelperModels/LoginSignUpHelperModel";
import BusinessSchema from "../../schemas/business.schema";

const PUBLIC_FIELDS = [
  "scheduleId",
  "businessId",
  "centerId",
  //"clientId",
  "serviceId",
  "scheduleName",
  "teamId",
  "scheduleDate",
  "startTime",
  "endTime",
  "is_repeat",
  "repeatation_type",
  "repeat_number",
  "repeat_days",
  "repeat_ends",
  "repeat_ends_date",
  "parent_scheduleId",
  "subscriptionId",
  "numberOfClients"
];

export class ScheduleDB extends BaseModel {
  schedules = [];

  //Adding new schedule details old code
  create = async (
    information,
    businessId,
    centerId,
    clientId,
    serviceId,
    scheduleName,
    teamId,
    scheduleDate,
    startTime,
    endTime,
    batchId,
    is_repeat,
    repeatation_type,
    repeat_number,
    repeat_days,
    repeat_ends,
    repeat_ends_date,
    parent_scheduleId,
    subscriptionId,
    numberOfClients
  ) => {
    let clientIdArray = await commaSeparatedValues(clientId); //get client list
    let teamIdArray = await commaSeparatedValues(teamId); //get employee list
    let subscriptionIdArray = await commaSeparatedValues(subscriptionId); //get subscription list

    let noc = parseInt(numberOfClients);
    if (noc === parseInt(noc, 10)) { } else {
      var numberOfClients = null;
    }

    //a date after two years as per recent logic
    let repeatEndsDate, RepeatEndsDateHyphen;
    if (is_repeat == 1) {
      if (repeat_ends == 1) {
        repeatEndsDate = new Date(repeat_ends_date);
      } else if (repeat_ends == 2) {
        repeatEndsDate = new Date(scheduleDate);
        repeatEndsDate.setFullYear(repeatEndsDate.getFullYear() + 5);
      } else {
        throw new ApplicationError(
          "Error in repeat_ends",
          401
        );
      }
    } else {

    }
    RepeatEndsDateHyphen = await hyphenDateFormat(repeatEndsDate);

    //check for existing clients.........
    let dbClients = createInstance(["client"], information);
    for (let client of clientIdArray) {
      let clients = await dbClients.clients.findAll({
        where: {
          businessId: businessId,
          status: 1,
          clientId: client
        },
        raw: true
      });
      if (!clients[0]) {
        throw new ApplicationError(
          "No such Clients Exist",
          409
        );
      }
    }
    dbClients.sequelize.close();

    //check for respective team in database....
    let dbTeam = createInstance(["clientEmployee"], information);
    for (let team of teamIdArray) {
      let emp = await dbTeam.employees.findAll({
        where: {
          businessId: businessId,
          status: 1,
          employeeId: team
        },
        raw: true
      });
      if (!emp[0]) {
        throw new ApplicationError(
          "No such Team Exists",
          409
        );
      }
    }
    dbTeam.sequelize.close();

    // .......check for subscriptionId.....
    if (clientIdArray.length != subscriptionIdArray.length) {
      throw new ApplicationError(
        "Mismatch in number of clients and Subscriptions",
        409
      );
    }

    let final_repeat_days = await getRepeatDays(repeat_days);

    //attendance calculation part
    var atdType = [];
    for (var i = 0; i < clientIdArray.length; i++) {
      let dbSubscription = createInstance(["clientSubscription"], information);
      let subscriptions = await dbSubscription.subscriptions.findAll({
        where: {
          status: 1,
          clientId: clientIdArray[i]
        },
        raw: true
      });
      if (subscriptions.length < 1) {
        throw new ApplicationError(
          "No Subscription",
          401
        );
      }
      dbSubscription.sequelize.close();

      let dbPricepack = createInstance(["clientPricepack"], information);
      let pricepack = await dbPricepack.pricepacks.findAll({
        where: {
          status: 1,
          pricepackId: subscriptions[0].pricepacks
        },
        raw: true
      });
      dbPricepack.sequelize.close();

      var today = new Date();
      let expiryDate = new Date(scheduleDate);
      expiryDate.setDate(expiryDate.getDate() + parseInt(pricepack[0].pricepackValidity));
      let scheduleEndsDay = await hyphenDateFormat(expiryDate);

      let endDay = (new Date(repeatEndsDate) > new Date(scheduleEndsDay)) ? scheduleEndsDay : RepeatEndsDateHyphen;
      atdType[i] = await getAttendance(
        scheduleDate,
        is_repeat,
        repeatation_type,
        repeat_number,
        final_repeat_days,
        repeat_ends,
        endDay
      );
    }
    //attendance part finished

    //scheduleTime calculation
    let scheduleTimeTable = await getScheduleTimeTable(
      startTime, endTime,
      scheduleDate,
      is_repeat,
      repeatation_type,
      repeat_number,
      final_repeat_days,
      repeat_ends,
      RepeatEndsDateHyphen
    );
    //end of scheduleTime calculation



    const newSchedule = {
      businessId,
      centerId,
      serviceId,
      scheduleName,
      scheduleDate,
      startTime,
      endTime,
      batchId,
      is_repeat,
      repeatation_type,
      repeat_number,
      repeat_days: final_repeat_days,
      repeat_ends,
      repeat_ends_date: repeatEndsDate,
      parent_scheduleId,
      scheduleTime: scheduleTimeTable,
      numberOfClients
    };
    try {
      //new code added (check for Service and Center)
      let dbService = createInstance(["clientService"], information);
      let dbCenter = createInstance(["clientCenter"], information);
      let services = await dbService.services.findAll({
        where: {
          status: 1,
          businessId: businessId,
          serviceId: serviceId
        },
        raw: true,

      });
      if (!services[0]) {
        throw new ApplicationError(
          "No such Center or Service Exists",
          409
        );
      }
      dbService.sequelize.close();
      dbCenter.sequelize.close();
      //new code ends

      let dbClientSchedule = createInstance(["clientSchedule"], information);
      let checkSchedule = await dbClientSchedule.schedule.findAll({
        where: {
          scheduleDate: scheduleDate,
          startTime: startTime,
          endTime: endTime,
          status: 1
        },
        raw: true
      });

      if (!checkSchedule[0]) {
        let schedules = await dbClientSchedule.schedule.create(newSchedule);
        let update_schedules = await dbClientSchedule.schedule.update({
          parent_scheduleId: schedules.scheduleId
        }, {
            where: {
              scheduleId: schedules.scheduleId
            }
          });
        var scheduleId = schedules.scheduleId;

        //code block for inserting client and schedule data into map table
        let dbCSMap = createInstance(["clientScheduleMap"], information);
        let checkSchedule = await dbCSMap.clientschedulemap.findAll({
          where: {
            clientId: clientId,
            scheduleId: scheduleId,
            status: 1
          },
          raw: true
        });

        if (!checkSchedule[0]) {
          for (var i = 0; i < clientIdArray.length; i++) {
            clientId = clientIdArray[i];
            subscriptionId = subscriptionIdArray[i];
            var attendanceType = atdType[i];
            let clientschedulesmap = await dbCSMap.clientschedulemap.create({
              businessId,
              scheduleId,
              clientId,
              attendanceType,
              subscriptionId
            });
          }
        } //code block ends
        dbCSMap.sequelize.close();

        let dbTeamSchedule = createInstance(["teamScheduleMap"], information);
        let checkSchedule2 = await dbTeamSchedule.teamschedulemap.findAll({
          where: {
            teamId: teamId,
            scheduleId: scheduleId,
            status: 1
          },
          raw: true
        });

        if (!checkSchedule2[0]) {
          for (var i = 0; i < teamIdArray.length; i++) {
            teamId = teamIdArray[i];
            let teamschedulesmap = await dbTeamSchedule.teamschedulemap.create({
              businessId,
              scheduleId,
              teamId
            });
          }
        } //code block ends
        dbClientSchedule.sequelize.close();
        dbTeamSchedule.sequelize.close();
        return await schedules;
      } else {
        throw new ApplicationError(
          "Selected client is already scheduled for selected date and time",
          409
        );
      }
    } catch (error) {
      throw error;
    }
  };

  //Adding new schedule details New code
  createSchedule = async (
    information,
    businessId,
    centerId,
    clientId,
    serviceId,
    scheduleName,
    teamId,
    scheduleDate,
    startTime,
    endTime,
    batchId,
    is_repeat,
    repeatation_type,
    monthly_type,
    repeat_number,
    repeat_days,
    repeat_ends,
    repeat_ends_date,
    parent_scheduleId,
    subscriptionId,
    numberOfClients
  ) => {
    try {
      let arrayOfStrings = information.split(",");
      let userId = arrayOfStrings.length > 2 ? arrayOfStrings[2] : null;
      //=================connections=============================
      let dbBusiness = createInstance(["clientBusiness"], information);
      let dbClientSchedule = createInstance(["clientSchedule"], information);
      let dbCSMap = createInstance(["clientScheduleMap"], information);
      let dbTeamSchedule = createInstance(["teamScheduleMap"], information);
      let dbClients = createInstance(["client"], information);
      let dbTeam = createInstance(["clientEmployee"], information);
      let dbSubscription = createInstance(["clientSubscription"], information);
      let dbPricepack = createInstance(["clientPricepack"], information);
      let dbService = createInstance(["clientService"], information);
      let dbCenter = createInstance(["clientCenter"], information);
      let dbTeamScheduleMap = createInstance(["teamScheduleMap"], information);

      //=================getting exact values===========================
      let clientIdArray = await commaSeparatedValues(clientId); //get client list
      let teamIdArray = await commaSeparatedValues(teamId); //get employee list
      let subscriptionIdArray = await commaSeparatedValues(subscriptionId); //get subscription list
      let final_repeat_days = await getRepeatDays(repeat_days);


      //==========a date after five years as per recent logic==========
      let repeatEndsDate, RepeatEndsDateHyphen;
      if (is_repeat == 1) {
        if (repeat_ends == 1) {
          repeatEndsDate = new Date(repeat_ends_date);
        } else if (repeat_ends == 2) {
          repeatEndsDate = new Date(scheduleDate);
          repeatEndsDate.setFullYear(repeatEndsDate.getFullYear() + 5);
        } else {
          throw new ApplicationError(
            "Error in repeat_ends",
            401
          );
        }
      } else {
        repeatEndsDate = new Date(scheduleDate);
      }
      RepeatEndsDateHyphen = await hyphenDateFormat(repeatEndsDate);

      //================existing clients=====================
      for (let client of clientIdArray) {
        let clients = await dbClients.clients.findAll({
          where: {
            businessId: businessId,
            status: 1,
            clientId: client
          },
          raw: true
        });
        if (!clients[0]) {
          throw new ApplicationError(
            "No such Clients Exist",
            409
          );
        }
      }

      //=============existing team members=================
      for (let team of teamIdArray) {
        let emp = await dbTeam.employees.findAll({
          where: {
            businessId: businessId,
            status: 1,
            employeeId: team
          },
          raw: true
        });
        if (!emp[0]) {
          throw new ApplicationError(
            "No such Team Exists",
            409
          );
        }
      }

      //=============clientId vs subscriptionId==================
      if (clientIdArray.length != subscriptionIdArray.length) {
        throw new ApplicationError(
          "Mismatch in number of clients and Subscriptions",
          409
        );
      }

      //==============number of clients================
      let noc = parseInt(numberOfClients);
      if (noc === parseInt(noc, 10)) { } else {
        var numberOfClients = null;
      }

      //===================existing schedule===================
      dbClientSchedule.schedule.belongsTo(dbTeamScheduleMap.teamschedulemap, {
        foreignKey: "scheduleId",
        targetKey: "scheduleId"
      });
      let checkSchedule = await dbClientSchedule.schedule.findAll({
        where: {
          scheduleDate: scheduleDate,
          startTime: startTime,
          endTime: endTime,
          serviceId: serviceId,
          centerId: centerId,
          status: 1
        },
        include: [{
          model: dbTeamScheduleMap.teamschedulemap,
          where: {
            teamId: teamId
          }
        }],
        raw: true
      });
      if (checkSchedule[0]) {
        throw new ApplicationError(
          "date and time is already scheduled !",
          401
        );
      }


      //==============businessDays vs scheduleDays===================
      let business = await dbBusiness.businessinfo.find({
        attributes: ["business_days"],
        where: {
          status: 1,
          businessId: businessId
        },
        raw: true
      });
      if (business.business_days == undefined || business.business_days == null || business.business_days.trim() == "") {
        throw new ApplicationError(
          "Please Add Business Days to Proceed !",
          401
        );
      }
      let bdays = await commaSeparatedValues(business.business_days);
      if (is_repeat == 1 && repeatation_type == 2) {
        let repeatDays = await commaSeparatedValues(final_repeat_days);
        for (let rd of repeatDays) {
          if (bdays.includes(rd) == false) {
            throw new ApplicationError(
              "Error in repeat_days. given days and business days are different !",
              401
            );
          }
        }
      }

      //===========making scheduleTime(booking JSON)=================
      let scheduleTimeTable = await getScheduleTimeTable(
        startTime,
        endTime,
        scheduleDate,
        centerId,
        is_repeat,
        repeatation_type = is_repeat == 0 ? 0 : repeatation_type,
        monthly_type = is_repeat == 0 ? 0 : monthly_type,
        repeat_number = is_repeat == 0 ? 0 : repeat_number,
        repeat_days = is_repeat == 0 ? 0 : final_repeat_days,
        repeat_ends = is_repeat == 0 ? 0 : repeat_ends,
        RepeatEndsDateHyphen = is_repeat == 0 ? scheduleDate : RepeatEndsDateHyphen,
        bdays
      );
      if (typeof scheduleTimeTable == 'undefined') {
        throw new ApplicationError(
          "Please enter valid data",
          409
        );
      }
      else {
        var atdType = [];
        for (var i = 0; i < clientIdArray.length; i++) {
          let subscriptions = await dbSubscription.subscriptions.findAll({
            where: {
              status: 1,
              clientId: clientIdArray[i],
              subscriptionId: subscriptionIdArray[i]
            },
            raw: true
          });
          if (subscriptions.length < 1) {
            throw new ApplicationError(
              "No Subscription",
              401
            );
          }
          let pricepack = await dbPricepack.pricepacks.findAll({
            where: {
              status: 1,
              pricepackId: subscriptions[0].pricepacks
            },
            raw: true
          });
          var today = new Date();
          let expiryDate = new Date(scheduleDate);
          expiryDate.setDate(expiryDate.getDate() + parseInt(pricepack[0].pricepackValidity));

          let scheduleEndsDay = await hyphenDateFormat(expiryDate);
          //end day is when (repeatEndsDate is given-end-date if repeat ends 1 or 5 years from now if 2)
          let endDay = (new Date(repeatEndsDate) > new Date(scheduleEndsDay)) ? scheduleEndsDay : RepeatEndsDateHyphen;

          let scheduleId = "";
          let subscriptionDate = new Date(subscriptions[0].subscriptionDateTime);
          let pval = parseInt(pricepack[0].pricepackValidity);
          let subscriptionExpiry = subscriptionDate.getTime() + (pval * 24 * 60 * 60 * 1000);
          let scdate = (new Date(scheduleTimeTable[0].scdate)) > (new Date(subscriptionDate)) ?
            new Date(scheduleTimeTable[0].scdate) : new Date(subscriptionDate);
          atdType[i] = await getAttendanceSheet(scheduleTimeTable, scdate, subscriptionExpiry);

        }
      }

      //=============service exists !=====================
      let services = await dbService.services.findAll({
        where: {
          status: 1,
          businessId: businessId,
          serviceId: serviceId
        },
        raw: true,

      });
      if (!services[0]) {
        throw new ApplicationError(
          "No such Center or Service Exists",
          409
        );
      }

      //=================create the schedule=======================
      const newSchedule = {
        businessId,
        centerId,
        serviceId,
        scheduleName,
        scheduleDate,
        startTime,
        endTime,
        batchId,
        is_repeat,
        repeatation_type: is_repeat == 0 ? null : repeatation_type,
        repeat_number: is_repeat == 0 ? null : repeat_number,
        repeat_days: is_repeat == 0 ? null : final_repeat_days,
        repeat_ends: is_repeat == 0 ? 1 : repeat_ends,
        repeat_ends_date: is_repeat == 0 ? scheduleDate : repeatEndsDate,
        monthly_type: is_repeat == 0 ? null : monthly_type,
        parent_scheduleId,
        scheduleTime: scheduleTimeTable,
        numberOfClients,
        createdBy: userId
      };
      let schedules = await dbClientSchedule.schedule.create(newSchedule);
      let update_schedules = await dbClientSchedule.schedule.update({
        parent_scheduleId: schedules.scheduleId
      }, {
          where: {
            scheduleId: schedules.scheduleId
          }
        });
      var scheduleId = schedules.scheduleId;

      //================insert clients=========================
      let checkClientInSchedule = await dbCSMap.clientschedulemap.findAll({
        where: {
          clientId: clientId,
          scheduleId: scheduleId,
          status: 1
        },
        raw: true
      });
      for (var i = 0; i < clientIdArray.length; i++) {
        clientId = clientIdArray[i];
        subscriptionId = subscriptionIdArray[i];
        var attendanceType = atdType[i];
        let parentScheduleId = schedules.scheduleId;
        let masterParentScheduleId = schedules.scheduleId;
        let clientschedulesmap = await dbCSMap.clientschedulemap.create({
          businessId,
          scheduleId,
          clientId,
          attendanceType,
          subscriptionId,
          parentScheduleId,
          masterParentScheduleId
        });
      }

      //======================assign team================================
      let checkTeamInSchedule = await dbTeamSchedule.teamschedulemap.findAll({
        where: {
          teamId: teamId,
          scheduleId: scheduleId,
          status: 1
        },
        raw: true
      });
      if (!checkTeamInSchedule[0]) {
        for (var i = 0; i < teamIdArray.length; i++) {
          teamId = teamIdArray[i];
          let teamschedulesmap = await dbTeamSchedule.teamschedulemap.create({
            businessId,
            scheduleId,
            teamId
          });
        }
      }

      //Log Entry
      let dbLog = createInstance(["log"], information);
      const newLog = {
        businessId: businessId,
        activity: {
          Status: 1,
          header: 'Schedule Created',
          ScheduleId: scheduleId,
          BookingName: scheduleName,
          TeamId: teamId,
          scheduleDate: scheduleDate,
          NotificationText: "This booking has been added to your calendar",
          startTime: startTime,
          endTime: endTime,
          activityDate: new Date(),
          activityTime: new Date(),
          message: 'Schedule Recorded',
        },
        referenceTable: 'clientScheduleMap,ClientSchedule',
      };
      await toLogEntry(newLog, information);
      //End Log Entry

      //===============disconnect and return=======================
      dbCSMap.sequelize.close();
      dbPricepack.sequelize.close();
      dbSubscription.sequelize.close();
      dbBusiness.sequelize.close();
      dbClients.sequelize.close();
      dbService.sequelize.close();
      dbCenter.sequelize.close();
      dbClientSchedule.sequelize.close();
      dbTeamSchedule.sequelize.close();
      dbTeam.sequelize.close();
      return await schedules;

    } catch (error) {
      throw error;
    }
  };


  listAll = async (information, date, filterPrivateFields = true) => {
    try {
      //let date_given = new Date(date);
      let date_given = new Date().toJSON().slice(0, 10);

      let db = createInstance(["clientSchedule"], information);
      let schedules = await db.schedule.findAll({
        where: {
          status: 1,
          schedule_status: 1
        },
        raw: true
      });

      //pending attendance part
      var dbclients = createInstance(["clientScheduleMap"], information);
      var countUnmarked = 0;
      var countSchedule = 0;
      for (var i = 0; i < schedules.length; i++) {
        let clients = await dbclients.clientschedulemap.findAll({
          where: {
            status: 1,
            scheduleId: schedules[i].scheduleId
          },
          raw: true
        });

        for (var j = 0; j < clients.length; j++) {
          var attendanceKeys = [];
          var attendanceValues = [];
          Object.keys(clients[j].attendanceType).forEach(function (key) {
            attendanceKeys.push(key);
            attendanceValues.push(clients[j].attendanceType[key]);
          });
          for (var k = 0; k < attendanceKeys.length; k++) {
            var atdKey = new Date(attendanceKeys[k]);
            if (atdKey < new Date(date_given)) {
              continue;
            } else break;
          }
          for (var l = 0; l < k; l++) {
            if (attendanceValues[l] == 4) countUnmarked++;
          }
        }
        if (countUnmarked > 0) countSchedule++;
      }
      //end of pending attendance



      var dateObj = new Date();
      var month = dateObj.getUTCMonth() + 1; //months from 1-12
      if (month >= 0 && month <= 9) month = "0" + month;
      var day = dateObj.getUTCDate();
      if (day >= 0 && day <= 9) day = "0" + day;
      var year = dateObj.getUTCFullYear();
      var newdate = year + "-" + month + "-" + day;
      var today = newdate.toString();

      var countToday = 0;
      var countUpcoming = 0;
      var countPast = 0;
      for (var i = 0; i < schedules.length; i++) {
        var dateGiven = schedules[i].scheduleDate;
        var dateGiven = dateGiven.toString();
        if (today == dateGiven) {
          countToday++;
        } else if (today < dateGiven) {
          countUpcoming++;
        } else if (today > dateGiven) {
          countPast++;
        }
      }

      /*code for finding all schedules */
      /*main loop*/
      var upcoming = [];
      var pastSchedules = [];
      var todaysBooking = [];
      var upcomingJSON = [];
      var pastSchedulesJSON = [];
      var todaysBookingJSON = [];
      for (var i = 0; i < schedules.length; i++) {
        var idate = schedules[i].scheduleDate;
        //var idate = '2018-02-08';
        idate = idate.split("-");

        var myYear = parseInt(idate[0]);
        var myMonth = parseInt(idate[1]);
        var myDate = parseInt(idate[2]);

        var d = new Date();
        var date = d.getDate();
        var month = d.getMonth() + 1;
        var year = d.getFullYear();

        if (myYear > year) {
          upcoming = upcoming.concat(schedules[i].scheduleId);
          upcomingJSON = upcomingJSON.concat(schedules[i]);
        } else if (myYear == year && myMonth > month) {
          upcoming = upcoming.concat(schedules[i].parent_scheduleId);
          upcomingJSON = upcomingJSON.concat(schedules[i]);
        } else if (myYear == year && myMonth == month && myDate > date) {
          upcoming = upcoming.concat(schedules[i].parent_scheduleId);
          upcomingJSON = upcomingJSON.concat(schedules[i]);
        } else if (myYear < year) {
          pastSchedules = pastSchedules.concat(schedules[i].parent_scheduleId);
          pastSchedulesJSON = pastSchedulesJSON.concat(schedules[i]);
        } else if (myYear == year && myMonth < month) {
          pastSchedules = pastSchedules.concat(schedules[i].parent_scheduleId);
          pastSchedulesJSON = pastSchedulesJSON.concat(schedules[i]);
        } else if (myYear == year && myMonth == month && myDate < date) {
          pastSchedules = pastSchedules.concat(schedules[i].parent_scheduleId);
          pastSchedulesJSON = pastSchedulesJSON.concat(schedules[i]);
        } else {
          todaysBooking = todaysBooking.concat(schedules[i].parent_scheduleId);
          todaysBookingJSON = todaysBookingJSON.concat(schedules[i]);
        }
      }

      //Todays schedule
      var mergedSchedulesToday = [];
      var dbclients = createInstance(["clientScheduleMap"], information);

      for (var i = 0; i < todaysBooking.length; i++) {
        let clients = await dbclients.clientschedulemap.findAll({
          where: {
            status: 1,
            //scheduleId: schedules[i].scheduleId,
            scheduleId: todaysBooking[i]
          },
          raw: true
        });

        //client and attendance part
        let countPresent = 0,
          countAbsent = 0,
          countExcused = 0,
          countUnmarked = 0,
          countTotal = 0;
        var db = createInstance(["client"], information);
        var clientDetails1 = [];

        for (var j = 0; j < clients.length; j++) {
          let attendanceKeys = [];
          let attendanceValues = [];
          Object.keys(clients[j].attendanceType).forEach(function (key) {
            attendanceKeys.push(key);
            attendanceValues.push(clients[j].attendanceType[key]);
          });
          //attendance part
          for (let k = 0; k < attendanceKeys.length; k++) {
            let givenDate = await hyphenDateFormat(date_given);
            if (attendanceKeys[k] == givenDate) {
              if (attendanceValues[k] == 1) countPresent++;
              if (attendanceValues[k] == 2) countAbsent++;
              if (attendanceValues[k] == 3) countExcused++;
              if (attendanceValues[k] == 4) countUnmarked++;
              countTotal++;
            }
          }
          //end of finding attendance of individual clients

          var clientDetails = await db.clients.findAll({
            where: {
              status: 1,
              clientId: clients[j].clientId
            },
            raw: true
          });
          //clientDetails1 = clientDetails.concat(clientDetails1);

          // Client Info Start
          var isphoto = (clientDetails[0].photoUrl === null) ? 'noimage.jpg' : clientDetails[0].photoUrl;
          var clientphoto = KNACK_UPLOAD_URL.localUrl + isphoto;
          let clientData = [];
          Array.prototype.push.apply(clientData, [{ clientId: clientDetails[0].clientId, clientPhoto: clientphoto }]);
          let allClientsArray = [];
          allClientsArray.push(clientDetails);
          let allClientsObject = {};
          allClientsObject = Object.assign.apply(Object, clientData);
          clientDetails1 = clientDetails1.concat(allClientsObject);
          // Client Info End

        }

        var schedule1 = [todaysBookingJSON[i]];

        var perPresent = 0,
          perAbsent = 0,
          perExcused = 0,
          perUnmarked = 0;
        if (countTotal != 0) {
          perPresent = ((100 / countTotal) * countPresent).toFixed(2);
          perAbsent = ((100 / countTotal) * countAbsent).toFixed(2);
          perExcused = ((100 / countTotal) * countExcused).toFixed(2);
          perUnmarked = ((100 / countTotal) * countUnmarked).toFixed(2);
        }

        var scheduleAttendance1 = {
          Present: perPresent,
          Absent: perAbsent,
          Excused: perExcused,
          Unmarked: perUnmarked
        };

        //Booking Name
        let batchinfo = createInstance(["clientService"], information);
        var getbatchinfo = await batchinfo.services.findAll({
          where: {
            serviceId: schedule1[0].serviceId,
            status: 1
          },
          raw: true
        });
        batchinfo.sequelize.close();
        // Set Day and Time
        var stime = tConv24(schedule1[0].startTime);
        var etime = tConv24(schedule1[0].endTime);
        let stime = stime + '-' + etime;
        let dayint = convartday(schedule1[0].repeat_days);
        let day_time = ', ' + dayint + ', ' + stime;
        // End
        var bname;
        if (schedule1[0].scheduleName) {
          bname = schedule1[0].scheduleName;
        } else {
          bname = getbatchinfo[0].serviceName + day_time;
        }
        var service_name = [{ serviceName: getbatchinfo[0].serviceName }];

        var booking_name = [{ booking_name: bname }];

        var number_of_clients = [{ numberOfClients: clients.length }];

        var list_of_clients = [{ clients: clientDetails1 }];
        Array.prototype.push.apply(schedule1, booking_name);
        Array.prototype.push.apply(schedule1, number_of_clients);
        Array.prototype.push.apply(schedule1, service_name);
        Array.prototype.push.apply(schedule1, list_of_clients);
        Array.prototype.push.apply(schedule1, [{ attendance: scheduleAttendance1 }]);
        var merged = [];
        merged = Object.assign.apply(Object, schedule1);
        mergedSchedulesToday = mergedSchedulesToday.concat(merged);
      }
      dbclients.sequelize.close();
      //Upcoming Scedule
      var mergedSchedulesUpcoming = [];
      var dbclients = createInstance(["clientScheduleMap"], information);

      for (var i = 0; i < upcoming.length; i++) {
        let clients = await dbclients.clientschedulemap.findAll({
          where: {
            status: 1,
            //scheduleId: schedules[i].scheduleId,
            scheduleId: upcoming[i]
          },
          raw: true
        });



        //client and attendance part
        let countPresent = 0,
          countAbsent = 0,
          countExcused = 0,
          countUnmarked = 0,
          countTotal = 0;
        var db = createInstance(["client"], information);
        var clientDetails1 = [];
        for (var j = 0; j < clients.length; j++) {
          let attendanceKeys = [];
          let attendanceValues = [];
          Object.keys(clients[j].attendanceType).forEach(function (key) {
            attendanceKeys.push(key);
            attendanceValues.push(clients[j].attendanceType[key]);
          });
          //attendance part
          for (let k = 0; k < attendanceKeys.length; k++) {
            let givenDate = await hyphenDateFormat(date_given);
            if (attendanceKeys[k] == givenDate) {
              if (attendanceValues[k] == 1) countPresent++;
              if (attendanceValues[k] == 2) countAbsent++;
              if (attendanceValues[k] == 3) countExcused++;
              if (attendanceValues[k] == 4) countUnmarked++;
              countTotal++;
            }
          }
          //end of finding attendance of individual clients

          var clientDetails = await db.clients.findAll({
            where: {
              status: 1,
              clientId: clients[j].clientId
            },
            raw: true
          });
          //clientDetails1 = clientDetails.concat(clientDetails1);
          // Client Info Start
          var isphoto = (clientDetails[0].photoUrl === null) ? 'noimage.jpg' : clientDetails[0].photoUrl;
          var clientphoto = KNACK_UPLOAD_URL.localUrl + isphoto;
          let clientData = [];
          Array.prototype.push.apply(clientData, [{ clientId: clientDetails[0].clientId, clientPhoto: clientphoto }]);
          let allClientsArray = [];
          allClientsArray.push(clientDetails);
          let allClientsObject = {};
          allClientsObject = Object.assign.apply(Object, clientData);
          clientDetails1 = clientDetails1.concat(allClientsObject);
          // Client Info End
        }
        db.sequelize.close();
        var schedule2 = [upcomingJSON[i]];

        var perPresent = 0,
          perAbsent = 0,
          perExcused = 0,
          perUnmarked = 0;
        if (countTotal != 0) {
          perPresent = ((100 / countTotal) * countPresent).toFixed(2);
          perAbsent = ((100 / countTotal) * countAbsent).toFixed(2);
          perExcused = ((100 / countTotal) * countExcused).toFixed(2);
          perUnmarked = ((100 / countTotal) * countUnmarked).toFixed(2);
        }

        var scheduleAttendance2 = {
          Present: perPresent,
          Absent: perAbsent,
          Excused: perExcused,
          Unmarked: perUnmarked
        };

        //Booking Name
        let batchinfo = createInstance(["clientService"], information);
        var getbatchinfo = await batchinfo.services.findAll({
          where: {
            serviceId: schedule2[0].serviceId,
            status: 1
          },
          raw: true
        });
        batchinfo.sequelize.close();
        var bname;
        // Set Day and Time
        var stime = tConv24(schedule2[0].startTime);
        var etime = tConv24(schedule2[0].endTime);
        let stime = stime + '-' + etime;
        let dayint = convartday(schedule2[0].repeat_days);
        let day_time = ', ' + dayint + ', ' + stime;
        // End
        if (schedule2[0].scheduleName) {
          bname = schedule2[0].scheduleName;
        } else {
          bname = getbatchinfo[0].serviceName + day_time;
        }
        var booking_name = [{ booking_name: bname }];

        var number_of_clients = [{ numberOfClients: clients.length }];
        var list_of_clients = [{ clients: clientDetails1 }];
        Array.prototype.push.apply(schedule2, booking_name);
        Array.prototype.push.apply(schedule2, number_of_clients);
        Array.prototype.push.apply(schedule2, list_of_clients);
        Array.prototype.push.apply(schedule2, [{ attendance: scheduleAttendance2 }]);
        var merged = [];
        merged = Object.assign.apply(Object, schedule2);
        mergedSchedulesUpcoming = mergedSchedulesUpcoming.concat(merged);
      }
      dbclients.sequelize.close();
      //Past Schedule
      var mergedSchedulesPast = [];
      var dbclients = createInstance(["clientScheduleMap"], information);

      for (var i = 0; i < pastSchedules.length; i++) {
        let clients = await dbclients.clientschedulemap.findAll({
          where: {
            status: 1,
            //scheduleId: schedules[i].scheduleId,
            scheduleId: pastSchedules[i]
          },
          raw: true
        });

        //client and attendance part
        let countPresent = 0,
          countAbsent = 0,
          countExcused = 0,
          countUnmarked = 0,
          countTotal = 0;
        var db = createInstance(["client"], information);
        var clientDetails1 = [];
        for (var j = 0; j < clients.length; j++) {
          let attendanceKeys = [];
          let attendanceValues = [];
          Object.keys(clients[j].attendanceType).forEach(function (key) {
            attendanceKeys.push(key);
            attendanceValues.push(clients[j].attendanceType[key]);
          });
          //attendance part
          for (let k = 0; k < attendanceKeys.length; k++) {
            let givenDate = await hyphenDateFormat(dateGiven);
            if (attendanceKeys[k] == givenDate) {
              if (attendanceValues[k] == 1) countPresent++;
              if (attendanceValues[k] == 2) countAbsent++;
              if (attendanceValues[k] == 3) countExcused++;
              if (attendanceValues[k] == 4) countUnmarked++;
              countTotal++;
            }
          }
          //end of finding attendance of individual clients


          var clientDetails = await db.clients.findAll({
            where: {
              status: 1,
              clientId: clients[j].clientId
            },
            raw: true
          });
          //clientDetails1 = clientDetails.concat(clientDetails1);
          // Client Info Start
          var isphoto = (clientDetails[0].photoUrl === null) ? 'noimage.jpg' : clientDetails[0].photoUrl;
          var clientphoto = KNACK_UPLOAD_URL.localUrl + isphoto;
          let clientData = [];
          Array.prototype.push.apply(clientData, [{ clientId: clientDetails[0].clientId, clientPhoto: clientphoto }]);
          let allClientsArray = [];
          allClientsArray.push(clientDetails);
          let allClientsObject = {};
          allClientsObject = Object.assign.apply(Object, clientData);
          clientDetails1 = clientDetails1.concat(allClientsObject);
          // Client Info End
        }
        db.sequelize.close();
        var schedule3 = [pastSchedulesJSON[i]];


        var perPresent = 0,
          perAbsent = 0,
          perExcused = 0,
          perUnmarked = 0;
        if (countTotal != 0) {
          perPresent = ((100 / countTotal) * countPresent).toFixed(2);
          perAbsent = ((100 / countTotal) * countAbsent).toFixed(2);
          perExcused = ((100 / countTotal) * countExcused).toFixed(2);
          perUnmarked = ((100 / countTotal) * countUnmarked).toFixed(2);
        }

        var scheduleAttendance3 = {
          Present: perPresent,
          Absent: perAbsent,
          Excused: perExcused,
          Unmarked: perUnmarked
        };


        //Booking Name
        let batchinfo = createInstance(["clientService"], information);
        var getbatchinfo = await batchinfo.services.findAll({
          where: {
            serviceId: schedule3[0].serviceId,
            status: 1
          },
          raw: true
        });

        // Set Day and Time
        var stime = tConv24(schedule3[0].startTime);
        var etime = tConv24(schedule3[0].endTime);
        let stime = stime + '-' + etime;
        let dayint = convartday(schedule3[0].repeat_days);
        let day_time = ', ' + dayint + ', ' + stime;
        // End

        if (schedule3[0].scheduleName) {
          bname = schedule3[0].scheduleName;
        } else {
          bname = getbatchinfo[0].serviceName + day_time;
        }
        var booking_name = [{ booking_name: bname }];

        var number_of_clients = [{ numberOfClients: clients.length }];
        var list_of_clients = [{ clients: clientDetails1 }];
        Array.prototype.push.apply(schedule3, booking_name);
        Array.prototype.push.apply(schedule3, number_of_clients);
        Array.prototype.push.apply(schedule3, list_of_clients);
        Array.prototype.push.apply(schedule3, [{ attendance: scheduleAttendance3 }]);
        var merged = [];
        merged = Object.assign.apply(Object, schedule3);
        mergedSchedulesPast = mergedSchedulesPast.concat(merged);
      }
      dbclients.sequelize.close();
      var count = [
        { todaysBooking: countToday },
        { upcomingBooking: countUpcoming },
        { pastBooking: countPast },
        { pendingAttendance: countSchedule }
      ];
      var mergedSchedules = [
        { Today: mergedSchedulesToday },
        { Upcoming: mergedSchedulesUpcoming },
        { Past: mergedSchedulesPast }
      ];

      var mergedCount = [Object.assign.apply(Object, count)];
      return await [{ Booking: mergedCount }, { Schedules: mergedSchedules }];
      // if (filterPrivateFields) {
      //   return await filterFields(mergedSchedules, PUBLIC_FIELDS);
      // }
    } catch (error) {
      throw error;
    }
  };


  scheduleDetail__3_07 = async (
    information,
    scheduleId,
    filterPrivateFields = true
  ) => {
    try {
      let dbSchedule = createInstance(["clientSchedule"], information);
      let dbCenters = createInstance(["clientCenter"], information);
      let dbTeamMap = createInstance(["teamScheduleMap"], information);
      let dbTeam = createInstance(["clientEmployee"], information);
      let dbCSM = createInstance(["clientScheduleMap"], information);
      var dbClients = createInstance(["client"], information);
      let dbSubscription = createInstance(["clientSubscription"], information);
      let dbPricepack = createInstance(["clientPricepack"], information);

      dbSubscription.subscriptions.belongsTo(dbPricepack.pricepacks, {
        foreignKey: "pricepacks",
        targetKey: "pricepackId"
      });

      let schedules = await dbSchedule.schedule.findAll({
        where: {
          status: 1,
          scheduleId: scheduleId,
          schedule_status: 1
        },
        raw: true
      });

      //Center Details
      let centers = await dbCenters.centers.findAll({
        where: {
          status: 1,
          centerId: schedules[0].centerId
        },
        raw: true
      });

      //Team members under a schedule
      let employees = await dbTeamMap.teamschedulemap.findAll({
        where: {
          scheduleId: scheduleId,
          status: 1
        },
        raw: true
      });

      var teamDetails1 = [];
      for (var i = 0; i < employees.length; i++) {
        let teamDetails = await dbTeam.employees.findAll({
          where: {
            status: 1,
            employeeId: employees[i].teamId
          },
          raw: true
        });
        var isphoto = (teamDetails[0].photoUrl === null) ? 'noimage.jpg' : teamDetails[0].photoUrl;
        var teamphoto = KNACK_TEAM_UPLOAD_URL.localUrl + isphoto;

        Array.prototype.push.apply(teamDetails, [{ teamphoto: teamphoto }]);
        teamDetails = [Object.assign.apply(Object, teamDetails)]; //array of object

        teamDetails1 = teamDetails.concat(teamDetails1);

      }

      //code for counting the clients of a schedule from client-schedule-mapping table
      let clients = await dbCSM.clientschedulemap.findAll({
        where: {
          status: 1,
          masterParentScheduleId: scheduleId
        },
        raw: true
      });

      var clientDetails1 = [];
      for (var i = 0; i < clients.length; i++) {
        var clientDetails = await dbClients.clients.findAll({
          where: {
            status: 1,
            clientId: clients[i].clientId
          },
          raw: true
        });
        // clientDetails1 = clientDetails.concat(clientDetails1);

        let subscriptionDetails = await dbSubscription.subscriptions.find({
          attributes: ['subscriptionId'],
          where: {
            subscriptionId: clients[i].subscriptionId
          },
          include: [
            { model: dbPricepack.pricepacks, attributes: ['serviceDuration'] }
          ],
          raw: true
        });

        //let sessionInfo = clients[i].sessionCount + '/' + subscriptionDetails['pricepack.serviceDuration'];

        let setObj = { status: 1, subscriptionId: clients[i].subscriptionId };
        let session = await sessionInfo(information, setObj);
        let sessionInformation = session[0].attendedSession + '/' + session[0].totalSession;

        var countPresent = 0,
          countAbsent = 0,
          countExcused = 0,
          countUnmarked = 0,
          countTotal = clients[i].attendanceType.length;
        for (var j = 0; j < clients[i].attendanceType.length; j++) {
          if (clients[i].attendanceType[j].atd == 1) countPresent++;
          else if (clients[i].attendanceType[j].atd == 2) countAbsent++;
          else if (clients[i].attendanceType[j].atd == 3) countExcused++;
          else if (clients[i].attendanceType[j].atd == 4) countUnmarked++;
        }
        /* section for calculating percentage of present, absent and the rest depending on the total clients */
        var perPresent = (100 / countTotal * countPresent).toFixed(0);
        var perAbsent = (100 / countTotal * countAbsent).toFixed(0);
        var perExcused = (100 / countTotal * countExcused).toFixed(0);
        var perUnmarked = (100 / countTotal * countUnmarked).toFixed(0);
        /* section ends here */

        var percent = {
          Present: perPresent,
          Absent: perAbsent,
          Excused: perExcused,
          Unmarked: perUnmarked
        };
        if (clientDetails.length > 0) {
          var isphoto = (clientDetails[0].photoUrl === null) ? 'noimage.jpg' : clientDetails[0].photoUrl;
          var clientphoto = KNACK_UPLOAD_URL.localUrl + isphoto;
        }

        Array.prototype.push.apply(clientDetails, [
          { clientphoto: clientphoto },
          { attendance: percent },
          { clientsubscriptionId: clients[i].subscriptionId },
          { sessionInfo: sessionInformation }
        ]);
        clientDetails = [Object.assign.apply(Object, clientDetails)]; //array of object

        clientDetails1 = clientDetails.concat(clientDetails1);
      }

      var merged = [];
      var list_of_clients = [{ clients: clientDetails1 }];

      var list_of_emps = [{ teamMembers: teamDetails1 }];
      Array.prototype.push.apply(schedules, list_of_emps);
      Array.prototype.push.apply(schedules, [{ centerDetails: centers }]);
      Array.prototype.push.apply(schedules, [
        { numberOfClients: clients.length }
      ]);
      Array.prototype.push.apply(schedules, list_of_clients);

      merged = Object.assign.apply(Object, schedules);
      //code ends
      dbSchedule.sequelize.close();
      dbCenters.sequelize.close();
      dbTeamMap.sequelize.close();
      dbTeam.sequelize.close();
      dbCSM.sequelize.close();
      dbClients.sequelize.close();
      dbSubscription.sequelize.close();
      dbPricepack.sequelize.close();
      return await merged;
      // if (filterPrivateFields) {
      //   return await filterFields(schedules, PUBLIC_FIELDS);
      // }
    } catch (error) {
      throw error;
    }
  };

  scheduleDetail = async (
    information,
    scheduleId,
    filterPrivateFields = true
  ) => {
    try {
      let dbSchedule = createInstance(["clientSchedule"], information);
      let dbCenters = createInstance(["clientCenter"], information);
      let dbTeamMap = createInstance(["teamScheduleMap"], information);
      let dbTeam = createInstance(["clientEmployee"], information);
      let dbCSM = createInstance(["clientScheduleMap"], information);
      var dbClients = createInstance(["client"], information);
      let dbSubscription = createInstance(["clientSubscription"], information);
      let dbPricepack = createInstance(["clientPricepack"], information);

      dbSubscription.subscriptions.belongsTo(dbPricepack.pricepacks, {
        foreignKey: "pricepacks",
        targetKey: "pricepackId"
      });

      let schedules = await dbSchedule.schedule.findAll({
        where: {
          status: 1,
          scheduleId: scheduleId,
          schedule_status: 1
        },
        raw: true
      });

      //Center Details
      let centers = await dbCenters.centers.findAll({
        where: {
          status: 1,
          centerId: schedules[0].centerId
        },
        raw: true
      });

      //Team members under a schedule
      let employees = await dbTeamMap.teamschedulemap.findAll({
        where: {
          scheduleId: scheduleId,
          status: 1
        },
        raw: true
      });

      var teamDetails1 = [];
      for (var i = 0; i < employees.length; i++) {
        let teamDetails = await dbTeam.employees.findAll({
          where: {
            status: 1,
            employeeId: employees[i].teamId
          },
          raw: true
        });
        var isphoto = (teamDetails[0].photoUrl === null) ? 'noimage.jpg' : teamDetails[0].photoUrl;
        var teamphoto = KNACK_TEAM_UPLOAD_URL.localUrl + isphoto;

        Array.prototype.push.apply(teamDetails, [{ teamphoto: teamphoto }]);
        teamDetails = [Object.assign.apply(Object, teamDetails)]; //array of object

        teamDetails1 = teamDetails.concat(teamDetails1);

      }

      //code for counting the clients of a schedule from client-schedule-mapping table
      let clients = await dbCSM.clientschedulemap.findAll({
        where: {
          status: 1,
          // masterParentScheduleId: scheduleId // changed due to  client duplicacy
          scheduleId: scheduleId
        },
        raw: true
      });

      var clientDetails1 = [];
      for (var i = 0; i < clients.length; i++) {
        if (clients[i].attendanceType.length > 0) {
          var clientDetails = await dbClients.clients.findAll({
            where: {
              status: 1,
              clientId: clients[i].clientId
            },
            raw: true
          });
          // clientDetails1 = clientDetails.concat(clientDetails1);

          let subscriptionDetails = await dbSubscription.subscriptions.find({
            attributes: ['subscriptionId'],
            where: {
              subscriptionId: clients[i].subscriptionId
            },
            include: [
              { model: dbPricepack.pricepacks, attributes: ['serviceDuration'] }
            ],
            raw: true
          });

          //let sessionInfo = clients[i].sessionCount + '/' + subscriptionDetails['pricepack.serviceDuration'];

          let setObj = { status: 1, subscriptionId: clients[i].subscriptionId };
          //let session = await sessionInfo(information, setObj);
          let session = await sessionCalc(information, clients[i].subscriptionId, schedules[0].serviceId);
          let sessionInformation = session[0].durationType == 1 ? session[0].attendedSession + '/' + session[0].totalSession : "" + session[0].attendedSession;

          var countPresent = 0,
            countAbsent = 0,
            countExcused = 0,
            countUnmarked = 0,
            countTotal = clients[i].attendanceType.length;
          for (var j = 0; j < clients[i].attendanceType.length; j++) {
            if (clients[i].attendanceType[j].atd == 1) countPresent++;
            else if (clients[i].attendanceType[j].atd == 2) countAbsent++;
            else if (clients[i].attendanceType[j].atd == 3) countExcused++;
            else if (clients[i].attendanceType[j].atd == 4) countUnmarked++;
          }
          /* section for calculating percentage of present, absent and the rest depending on the total clients */
          var perPresent = (100 / countTotal * countPresent).toFixed(0);
          var perAbsent = (100 / countTotal * countAbsent).toFixed(0);
          var perExcused = (100 / countTotal * countExcused).toFixed(0);
          var perUnmarked = (100 / countTotal * countUnmarked).toFixed(0);
          /* section ends here */

          var percent = {
            Present: perPresent,
            Absent: perAbsent,
            Excused: perExcused,
            Unmarked: perUnmarked
          };
          if (clientDetails.length > 0) {
            var isphoto = (clientDetails[0].photoUrl === null) ? 'noimage.jpg' : clientDetails[0].photoUrl;
            var clientphoto = KNACK_UPLOAD_URL.localUrl + isphoto;
          }

          Array.prototype.push.apply(clientDetails, [
            { clientphoto: clientphoto },
            { attendance: percent },
            { clientsubscriptionId: clients[i].subscriptionId },
            { sessionInfo: sessionInformation }
          ]);
          clientDetails = [Object.assign.apply(Object, clientDetails)]; //array of object

          clientDetails1 = clientDetails.concat(clientDetails1);
        }
      }

      var merged = [];
      var list_of_clients = [{ clients: clientDetails1 }];

      var list_of_emps = [{ teamMembers: teamDetails1 }];
      Array.prototype.push.apply(schedules, list_of_emps);
      Array.prototype.push.apply(schedules, [{ centerDetails: centers }]);
      Array.prototype.push.apply(schedules, [
        { numberOfClients: clientDetails1.length }
      ]);
      Array.prototype.push.apply(schedules, list_of_clients);

      merged = Object.assign.apply(Object, schedules);
      //code ends
      dbSchedule.sequelize.close();
      dbCenters.sequelize.close();
      dbTeamMap.sequelize.close();
      dbTeam.sequelize.close();
      dbCSM.sequelize.close();
      dbClients.sequelize.close();
      dbSubscription.sequelize.close();
      dbPricepack.sequelize.close();
      return await merged;
      // if (filterPrivateFields) {
      //   return await filterFields(schedules, PUBLIC_FIELDS);
      // }
    } catch (error) {
      throw error;
    }
  };

  cancelSchedule_2 = async (
    information,
    flag,
    businessId,
    scheduleId,
    scheduleDate,
    comment
  ) => {
    try {
      let dbSchedule = createInstance(["clientSchedule"], information);
      let dbCSM = createInstance(["clientScheduleMap"], information);
      let dbEmployee = createInstance(["clientEmployee"], information);


      let empId = information.split(",")[2];
      let x = await CheckMainOrCenterAdmin(information);


      //==========cancel the booking=================
      if (flag == 2) {
        let data = await dbCSM.clientschedulemap.findAll({
          where: {
            scheduleId: scheduleId,
            status: 1
          },
          raw: true
        });
        for (var i = 0; i < data.length; i++) {
          let x = data[i].attendanceType;
          for (let j = 0; j < x.length; j++) {
            if (x[j]["atd"] == 1 || x[j]["atd"] == 2) {
              return [" Attendance for the Schedule hase already been given to client/clients", 401];
            }
          }
        }
        var schedules = await dbSchedule.schedule.update({
          status: 0
        }, {
            where: {
              status: 1,
              businessId: businessId,
              scheduleId: scheduleId
            }
          });
        dbSchedule.sequelize.close();
        dbCSM.sequelize.close();
        return await schedules;
      }

      //=============cancel a schedule======================
      else if (flag == 1) {
        let schedule = await dbSchedule.schedule.find({
          where: {
            status: 1,
            scheduleId: scheduleId
          },
          raw: true
        });
        let found = 0,
          flag = 0;
        let newScheduleTimeTable = [];
        let scheduletime = schedule.scheduleTime;
        for (let i = 0; i < schedule.scheduleTime.length; i++) {
          if (+(new Date(scheduletime[i]["scdate"])) === +(new Date(scheduleDate))) {
            if (parseInt(scheduletime[i]["reschedule"]) === 2) {
              throw new ApplicationError(
                "Cannot Cancel A Rescheduled Date",
                401
              );
            } else if (parseInt(parseInt(scheduletime[i]["reschedule"])) === 3) {
              throw new ApplicationError(
                "Date is already cancelled",
                401
              );
            } else {
              let newComment = "";
              if (comment) newComment = comment;
              let cancelledSchedule = {
                scdate: scheduletime[i]['scdate'],
                reschedule: 3,
                rescheduleDate: scheduletime[i]["rescheduleDate"],
                scheduleStartTime: scheduletime[i]["scheduleStartTime"],
                scheduleEndTime: scheduletime[i]["scheduleEndTime"],
                scheduleFrom: scheduletime[i]['scheduleFrom'],
                centerId: scheduletime[i]['centerId'],
                rescheduleBy: scheduletime[i]['rescheduleBy'],
                comment: newComment
              };
              newScheduleTimeTable.push(cancelledSchedule);
              found = 1;
            }
          } else {
            newScheduleTimeTable.push(scheduletime[i]);
          }
        }
        let infoObj = [];
        if (found == 0) {
          throw new ApplicationError(
            "schedule date isn't found",
            401
          );
        } else {
          let infoArray = [];
          var schedules = await dbSchedule.schedule.update({
            scheduleTime: newScheduleTimeTable,
            updateDateTime: Date.now()
          }, {
              where: {
                scheduleId: scheduleId
              }
            });
        }
        let clientsOfSchedule = await dbCSM.clientschedulemap.findAll({
          where: {
            scheduleId: scheduleId,
            status: 1
          },
          raw: true
        });
        var flagUpdate = [0];
        for (var i = 0; i < clientsOfSchedule.length; i++) {
          var attendanceKeys = [];
          var attendanceValues = [];
          let x = clientsOfSchedule[i].attendanceType;
          for (let j = 0; j < x.length; j++) {
            attendanceKeys.push(x[j]["scdate"]);
            attendanceValues.push(x[j]);
          }
          var output = [];
          for (var d = 0; d < attendanceKeys.length; d++) {
            if (attendanceKeys[d] == scheduleDate) {
              attendanceValues[d]["atd"] = '5';
              attendanceValues[d]["comment"] = comment;
            }
            output.push(attendanceValues[d]);
          }
          var clientSchedule = await dbCSM.clientschedulemap.update({
            attendanceType: output
          }, {
              where: {
                status: 1,
                businessId: businessId,
                scheduleId: scheduleId
              }
            });
          if (clientSchedule > 0 && flagUpdate[0] != 1) flagUpdate = [1]; //schedules>0 means there must be atleast one schedule that is updated. otherwise show flagUpdate = 0
        }
        dbSchedule.sequelize.close();
        dbCSM.sequelize.close();
        //added--------------------------
        let DateInfo;
        if (flag == 1) {
          DateInfo = scheduleDate;
        }
        let NotificationText;
        if (x[1]["UserStatus"] == 2) {
          let employees = await dbEmployee.employees.findAll({
            where: {
              employeeId: empId
            },
            raw: true
          });

          NotificationText = employees[0].employeeName + ' just cancelled a booking on ' + scheduleDate;
        }

        let dbLog = createInstance(["log"], information);
        const newLog = {
          businessId: businessId,
          activity: {
            header: 'Schedule Cancelled',
            Status: 2,
            ScheduleId: scheduleId,
            Information: DateInfo,
            UserStatus: x[1]["UserStatus"],
            NotificationText: NotificationText,
            activityDate: new Date(),
            activityTime: new Date(),
            message: 'Schedule Cancelled',
          },
          referenceTable: 'clientScheduleMap,clientSchedule',
        };

        let log = await dbLog.log.create(newLog);
        dbLog.sequelize.close();
        //--------------------------------------
        return schedules;
      } else {
        throw new ApplicationError(
          "Wrong Value ! 1 for Specific Date. 2 for total schedule.",
          401
        );
      }

    } catch (error) {
      throw error;
    }
  };

  cancelSchedule = async (
    information,
    flag,
    businessId,
    scheduleId,
    scheduleDate,
    scheduleStartTime,
    comment
  ) => {
    try {
      let dbSchedule = createInstance(["clientSchedule"], information);
      let dbCSM = createInstance(["clientScheduleMap"], information);
      let dbEmployee = createInstance(["clientEmployee"], information);

      let dbClient = createInstance(["client"], information);

      dbCSM.clientschedulemap.belongsTo(dbClient.clients, {
        foreignKey: "clientId",
        targetKey: "clientId"
      });

      let empId = information.split(",")[2];
      let checkMC = await CheckMainOrCenterAdmin(information);

      //==========cancel the booking=================
      if (flag == 2) {
        let data = await dbCSM.clientschedulemap.findAll({
          where: {
            scheduleId: scheduleId,
            status: 1
          },
          raw: true
        });
        for (var i = 0; i < data.length; i++) {
          let x = data[i].attendanceType;
          for (let j = 0; j < x.length; j++) {
            if (x[j]["atd"] == 1 || x[j]["atd"] == 2) {
              return [" Attendance for the Schedule has already been given to client/clients", 401];
            }
          }
        }
        var schedules = await dbSchedule.schedule.update({
          status: 0
        }, {
            where: {
              status: 1,
              businessId: businessId,
              scheduleId: scheduleId
            }
          });
        dbSchedule.sequelize.close();
        dbCSM.sequelize.close();
        return await schedules;
      }

      //=============cancel a schedule======================
      else if (flag == 1) {
        let schedule = await dbSchedule.schedule.find({
          where: {
            status: 1,
            scheduleId: scheduleId
          },
          raw: true
        });
        let found = 0,
          flag = 0;
        let newScheduleTimeTable = [];
        let scheduletime = schedule.scheduleTime;
        for (let i = 0; i < schedule.scheduleTime.length; i++) {
          let givenDateTime = new Date(scheduleDate + ' ' + scheduleStartTime);
          let scheduleDateTime = new Date(scheduletime[i]["scdate"] + ' ' + scheduletime[i]["scheduleStartTime"]);
          if (+scheduleDateTime === +givenDateTime) {
            if (parseInt(scheduletime[i]["reschedule"]) === 2) {
              throw new ApplicationError(
                "Cannot Cancel A Rescheduled Date",
                401
              );
            } else if (parseInt(parseInt(scheduletime[i]["reschedule"])) === 3) {
              throw new ApplicationError(
                "Date is already cancelled",
                401
              );
            } else {
              let newComment = "";
              if (comment) newComment = comment;
              let cancelledSchedule = {
                scdate: scheduletime[i]['scdate'],
                reschedule: 3,
                rescheduleDate: scheduletime[i]["rescheduleDate"],
                scheduleStartTime: scheduletime[i]["scheduleStartTime"],
                scheduleEndTime: scheduletime[i]["scheduleEndTime"],
                scheduleFrom: scheduletime[i]['scheduleFrom'],
                centerId: scheduletime[i]['centerId'],
                rescheduleBy: scheduletime[i]['rescheduleBy'],
                comment: newComment
              };
              newScheduleTimeTable.push(cancelledSchedule);
              found = 1;
            }
          } else {
            newScheduleTimeTable.push(scheduletime[i]);
          }
        }
        let infoObj = [];
        if (found == 0) {
          throw new ApplicationError(
            "schedule date isn't found",
            401
          );
        } else {
          let infoArray = [];
          var schedules = await dbSchedule.schedule.update({
            scheduleTime: newScheduleTimeTable,
            updateDateTime: Date.now()
          }, {
              where: {
                scheduleId: scheduleId
              }
            });
        }
        let clients = await dbCSM.clientschedulemap.findAll({
          where: {
            scheduleId: scheduleId,
            status: 1
          },
          include: [{
            model: dbClient.clients,
            required: true
          }],
          raw: true
        });
        var flagUpdate = [0];
        for (let client of clients) {
          var output = [];
          for (let clientAtd of client.attendanceType) {
            let givenDateTime = new Date(scheduleDate + ' ' + scheduleStartTime);
            let scheduleDateTime = new Date(clientAtd.scdate + ' ' + clientAtd.scheduleStartTime);
            if (+scheduleDateTime === +givenDateTime) {
              clientAtd.atd = 5;
              clientAtd.reschedule = 3;
              clientAtd.comment = comment;
            }
            output.push(clientAtd);
          }
          var clientSchedule = await dbCSM.clientschedulemap.update({
            attendanceType: output
          }, {
              where: {
                status: 1,
                businessId: businessId,
                scheduleId: scheduleId
              }
            }).then(() => {
              let text = "Dear customer, this is to inform you that the booking on " + scheduleDate + "," + scheduleStartTime + " has been cancelled due to " + comment + ". For any other queries, please contact +91 9812345678. ";
              sendSmsMobile(client["client.contactNumber"], text);

              if (client["client.emailId"]) {
                console.log(client["client.emailId"]);

                let sender = BusinessSchema.Business().find({
                  attributes: ["businessName"],
                  where: {
                    businessId: information.split(",")[1]
                  },
                  raw: true
                }).then(result => {

                  let subject = "Senders name:" + result.businessName + " Booking cancelled: " + scheduleDate + "," + scheduleStartTime;
                  let message = "Hi " + client["client.clientName"] + " ,this is to inform you that the booking on " + scheduleDate + "," + scheduleStartTime + " has been cancelled due to " + comment + ". For any other queries, please contact +91 9812345678. Kind regards, " + result.businessName;

                  sendEmail(client["client.emailId"], subject, message);
                })
              }
            });

          if (clientSchedule > 0 && flagUpdate[0] != 1) flagUpdate = [1]; //schedules>0 means there must be atleast one schedule that is updated. otherwise show flagUpdate = 0
        }
        dbSchedule.sequelize.close();
        dbCSM.sequelize.close();

        // Log added--------------------------
        let DateInfo;
        if (flag == 1) {
          DateInfo = scheduleDate;
        }
        let NotificationText;
        if (checkMC[1]["UserStatus"] == 2) {
          let employees = await dbEmployee.employees.findAll({
            where: {
              employeeId: empId
            },
            raw: true
          });

          NotificationText = employees[0].employeeName + ' just cancelled a booking on ' + scheduleDate;
        }

        let dbLog = createInstance(["log"], information);
        const newLog = {
          businessId: businessId,
          activity: {
            header: 'Schedule Cancelled',
            Status: 2,
            ScheduleId: scheduleId,
            Information: DateInfo,
            UserStatus: checkMC[1]["UserStatus"],
            NotificationText: NotificationText,
            activityDate: new Date(),
            activityTime: new Date(),
            message: 'Schedule Cancelled',
          },
          referenceTable: 'clientScheduleMap,clientSchedule',
        };

        let log = await dbLog.log.create(newLog);
        dbLog.sequelize.close();
        // Log End--------------------------------------

        return schedules;
      } else {
        throw new ApplicationError(
          "Wrong Value ! 1 for Specific Date. 2 for total schedule.",
          401
        );
      }

    } catch (error) {
      throw error;
    }
  };

  // Update Schedule
  updateSchedule__ = async (
    information,
    scheduleId,
    businessId,
    teamId,
    scheduleName,
    scheduleDate,
    startTime,
    endTime,
    centerId,
    batchId,
    is_repeat,
    repeatation_type,
    monthly_type,
    repeat_number,
    repeat_days,
    repeat_ends,
    repeat_ends_date,
    numberOfClients,
    comments
  ) => {
    try {
      let dbSchedule = createInstance(["clientSchedule"], information);
      let dbCSM = createInstance(["clientScheduleMap"], information);
      var dbTSM = createInstance(["teamScheduleMap"], information);
      let dbSubscription = createInstance(["clientSubscription"], information);
      let dbPricepack = createInstance(["clientPricepack"], information);
      let dbBusiness = createInstance(["clientBusiness"], information);

      let final_repeat_days = await getRepeatDays(repeat_days);


      let business = await dbBusiness.businessinfo.find({
        attributes: ["business_days"],
        where: {
          status: 1,
          businessId: businessId
        },
        raw: true
      });

      if (business.business_days == undefined || business.business_days == null || business.business_days.trim() == "") {
        throw new ApplicationError(
          "Please Add Business Days to Proceed !",
          401
        );
      }
      let bdays = await commaSeparatedValues(business.business_days);
      if (is_repeat == 1 && repeatation_type == 2) {
        let repeatDays = await commaSeparatedValues(final_repeat_days);
        for (let rd of repeatDays) {
          if (bdays.includes(rd) == false) {
            throw new ApplicationError(
              "Error in repeat_days. given days and business days are different !",
              401
            );
          }
        }
      }

      //get the schedule
      let findSchedule = await dbSchedule.schedule.find({
        where: {
          scheduleId: scheduleId,
          status: 1
        },
        raw: true
      });

      if (findSchedule == null) { //schedule isn't found
        throw new ApplicationError(
          "schedule is not found",
          401
        );
      } else { //schedule is found
        const Op = Sequelize.Op;
        let checkSchedule = await dbSchedule.schedule.findAll({
          where: {
            scheduleDate: scheduleDate,
            startTime: startTime,
            endTime: endTime,
            centerId: centerId,
            status: 1,
            scheduleId: {
              [Op.notIn]: [scheduleId]
            }
          },
          raw: true
        });
        if (checkSchedule.length > 0) { //date and time is already scheduled
          throw new ApplicationError("Conflicting Date and Time", 409);
        } else if (!checkSchedule[0]) { //if the schedule date and time exist
          //schedule time calculation
          let repeatEndsDate, RepeatEndsDateHyphen;
          if (repeat_ends == 1) {
            repeatEndsDate = new Date(repeat_ends_date);
          } else if (repeat_ends == 2) {
            repeatEndsDate = new Date(scheduleDate);
            repeatEndsDate.setFullYear(repeatEndsDate.getFullYear() + 5);
          } else {
            throw new ApplicationError(
              "Error in repeat_ends",
              401
            );
          }

          RepeatEndsDateHyphen = await hyphenDateFormat(repeatEndsDate);
          let scheduleTimeTable = await getScheduleTimeTable(
            startTime,
            endTime,
            scheduleDate,
            centerId,
            is_repeat,
            repeatation_type = is_repeat == 0 ? 0 : repeatation_type,
            monthly_type = is_repeat == 0 ? 0 : monthly_type,
            repeat_number = is_repeat == 0 ? 0 : repeat_number,
            repeat_days = is_repeat == 0 ? 0 : final_repeat_days,
            repeat_ends = is_repeat == 0 ? 0 : repeat_ends,
            repeat_ends_date = is_repeat == 0 ? scheduleDate : RepeatEndsDateHyphen,
            bdays
          );
          //end of schedule time calculation

          if (typeof scheduleTimeTable == 'undefined') {
            throw new ApplicationError(
              "Please enter valid data",
              409
            );
          }

          //checking client attendance
          let clients = await dbCSM.clientschedulemap.findAll({
            where: {
              status: 1,
              scheduleId: scheduleId
            },
            raw: true
          });
          var flag = 0;
          for (let i = 0; i < clients.length; i++) {
            let arr = Object.values(clients[i].attendanceType);
            arr.forEach(function (x) {
              if (x["atd"] != 4) {
                flag = 1;
              }
            })
          }
          //end of checking attendance

          //getting the common and uncommon team members from database and frontEnd
          let tsMap = await dbTSM.teamschedulemap.findAll({
            where: {
              scheduleId: scheduleId
            },
            raw: true
          });
          var oldArray = tsMap.map((element) => element.teamId);
          var teamIdArray = await commaSeparatedValues(teamId);
          var commonArray = await intersect_arrays(teamIdArray, oldArray);
          var uncommonOldArray = await diff_arrays(oldArray, commonArray); //uncommon values of commonArray won't be shown
          var uncommonNewArray = await diff_arrays(teamIdArray, commonArray);
          //end of differentiating team members

          if (flag === 0) { //no client attendance. delete old items and insert new
            //update schedule
            let x = await dbSchedule.schedule.findAll({
              where: {
                scheduleId: scheduleId
              }
            });
            let schedules = await dbSchedule.schedule.create({
              scheduleDate: scheduleDate,
              scheduleName: scheduleName,
              startTime: startTime,
              endTime: endTime,
              centerId: centerId,
              numberOfClients: numberOfClients.trim() == "" ? null : parseInt(numberOfClients),
              comments: comments,
              scheduleTime: scheduleTimeTable,
              repeat_ends_date: repeat_ends_date,
              parent_scheduleId: findSchedule.parent_scheduleId,
              businessId: businessId,
              serviceId: x[0].serviceId,
              is_repeat: is_repeat,
              repeatation_type: is_repeat == 0 ? null : repeatation_type,
              repeat_number: is_repeat == 0 ? null : repeat_number,
              repeat_days: is_repeat == 0 ? null : final_repeat_days,
              repeat_ends: is_repeat == 0 ? 1 : repeat_ends,
              repeat_ends_date: is_repeat == 0 ? scheduleDate : repeat_ends_date,
              updateDateTime: Date.now()
            });

            let newScheduleId = await dbSchedule.schedule.findAll({
              attributes: ['scheduleId'],
              where: {
                scheduleId: schedules.scheduleId,
                status: 1
              }
            });
            var newScheduleID = newScheduleId[0].scheduleId;
            let updateSchedule = await dbSchedule.schedule.update({
              status: 0
            }, {
                where: {
                  scheduleId: scheduleId
                }
              });
            // end of updating schedule


            //updating client schedule map. inserting subscribed clients into updated schedule
            for (let i = 0; i < clients.length; i++) {
              let subscriptions = await dbSubscription.subscriptions.findAll({
                where: {
                  status: 1,
                  clientId: clients[i].clientId,
                  subscriptionId: clients[i].subscriptionId
                },
                raw: true
              });
              if (subscriptions.length > 0 && subscriptions[0].status == 1) { //subscription is found and ongoing
                let pricepack = await dbPricepack.pricepacks.findAll({
                  where: {
                    status: 1,
                    pricepackId: subscriptions[0].pricepacks
                  },
                  raw: true
                });
                let expiryDate = new Date(scheduleDate);
                expiryDate.setDate(expiryDate.getDate() + parseInt(pricepack[0].pricepackValidity));
                let scheduleEndsDay = await hyphenDateFormat(expiryDate);
                let endDay = (new Date(repeatEndsDate) > new Date(scheduleEndsDay)) ? scheduleEndsDay : RepeatEndsDateHyphen;
                // let atdType = await getAttendance(
                //     scheduleId,
                //     centerId,
                //     startTime,
                //     endTime,
                //     scheduleDate,
                //     is_repeat,
                //     repeatation_type = is_repeat == 0 ? 0 : repeatation_type,
                //     monthly_type = is_repeat == 0 ? 0 : monthly_type,
                //     repeat_number = is_repeat == 0 ? 0 : repeat_number,
                //     repeat_days = is_repeat == 0 ? 0 : final_repeat_days,
                //     repeat_ends = is_repeat == 0 ? 0 : repeat_ends,
                //     endDay = is_repeat == 0 ? scheduleDate : endDay,
                //     bdays
                // );

                /////////////

                // let atdType = [];
                // for (let day = 0; day < parseInt(pricepack[0].pricepackValidity); day++) {
                //   if (scheduleTimeTable[day]) {
                //     let s_date = scheduleDate.split("-"),
                //       s_time = startTime.split(":");
                //     let id = "" + s_date[0] + s_date[1] + s_date[2] + s_time[0] + s_time[1];
                //     atdType.push({
                //       id: id,
                //       scdate: scheduleTimeTable[day].scdate,
                //       atd: 4,
                //       atdBy: "",
                //       atdDate: "",
                //       atdTime: "",
                //       reschedule: 1,
                //       rescheduleId: scheduleId,
                //       rescheduleDate: scheduleTimeTable[day].scdate,
                //       scheduleStartTime: startTime,
                //       scheduleEndTime: endTime,
                //       scheduleFrom: "",
                //       centerId: centerId,
                //       rescheduleBy: "",
                //       comment: "",
                //     });
                //   }
                // }

                let atdType = await getAttendanceJSON(scheduleTimeTable, scheduleTimeTable[0].scdate, pricepack[0].pricepackValidity);

                let clientschedulesmap = await dbCSM.clientschedulemap.create({
                  businessId: businessId,
                  scheduleId: schedules.scheduleId,
                  clientId: clients[i].clientId,
                  attendanceType: atdType,
                  subscriptionId: clients[i].subscriptionId,
                  parentScheduleId: schedules.scheduleId,
                  masterParentScheduleId: schedules.scheduleId
                });
              }
            }
            let clients_update = await dbCSM.clientschedulemap.update({
              status: 0
            }, {
                where: {
                  scheduleId: scheduleId
                },
                raw: true
              });
            //end of updating client-schedule-map

            //updating team-schedule-map 
            for (let element of oldArray) {
              await dbTSM.teamschedulemap.update({
                status: 0
              }, {
                  where: {
                    status: 1,
                    teamId: element,
                    scheduleId: scheduleId
                  }
                });
            }
            for (let element of teamIdArray) {
              let x = await dbTSM.teamschedulemap.create({
                businessId: businessId,
                scheduleId: newScheduleID,
                teamId: element
              });
            }
            //end of updating team-schedule-map
          } else { //attendance found. update only.


            let schedules = await dbSchedule.schedule.find({
              where: {
                scheduleId: scheduleId
              },
              raw: true
            });

            let ScheduleDate = schedules.scheduleTime;
            let newScheduleDate = [];
            for (let i = 0; i < ScheduleDate.length; i++) {

              if ((new Date(ScheduleDate[i]["scdate"]).getTime()) > (new Date().getTime())) {
                console.log(ScheduleDate[i]["scdate"]);
                ScheduleDate[i]["scheduleEndTime"] = endTime;
                ScheduleDate[i]["scheduleStartTime"] = startTime;
                newScheduleDate.push(ScheduleDate[i]);
              } else {
                newScheduleDate.push(ScheduleDate[i]);
              }
            }

            // //Update schedule
            let newschedules = await dbSchedule.schedule.update({
              startTime: startTime,
              scheduleName: scheduleName,
              centerId: centerId,
              numberOfClients: numberOfClients.trim() == "" ? findSchedule.numberOfClients : parseInt(numberOfClients),
              endTime: endTime,
              scheduleTime: newScheduleDate,
              updateDateTime: Date.now()
            }, {
                where: {
                  scheduleId: scheduleId
                }
              });
            // end of updating schedule

            var newScheduleID = scheduleId;
            let AttendanceDate = [];
            //updating client table
            for (let client of clients) {

              let subscriptions = await dbSubscription.subscriptions.findAll({
                where: {
                  subscriptionId: clients[i].subscriptionId,
                  status: 1,
                  clientId: client.clientId
                },
                raw: true
              });
              if (subscriptions.length == 0 || subscriptions[0].status == 0) {
                let clients_update = await dbCSM.clientschedulemap.update({
                  status: 0
                }, {
                    where: {
                      clientId: client.clientId
                    },
                    raw: true
                  });
              } else {
                //Update the NEw attendanceType here
                let clients_update = await dbCSM.clientschedulemap.findAll({
                  where: {
                    clientId: client.clientId,
                    scheduleId: scheduleId
                  }
                });


                let x = clients_update[0].attendanceType;

                let newAttendanceType = [];
                for (let i = 0; i < x.length; i++) {
                  if ((new Date(x[i]["scdate"]).getTime()) > (new Date().getTime())) {
                    x[i]["scheduleEndTime"] = endTime;
                    x[i]["scheduleStartTime"] = startTime;
                    newAttendanceType.push(x[i]);
                    AttendanceDate.push(x[i]["scdate"]);
                  } else {
                    newAttendanceType.push(x[i]);
                  }
                }

                let UpdatedClients = await dbCSM.clientschedulemap.update({

                  attendanceType: newAttendanceType
                }, {
                    where: {
                      clientId: client.clientId,
                      scheduleId: scheduleId
                    }
                  });

              }
            }
            // //end of updating clients


            //team update
            for (let element of commonArray) {
              var TSUpdate = await dbTSM.teamschedulemap.update({
                status: 1
              }, {
                  where: {
                    status: 0,
                    teamId: element,
                    scheduleId: scheduleId
                  }
                });
            }
            for (let element of uncommonOldArray) {
              await dbTSM.teamschedulemap.update({
                status: 0
              }, {
                  where: {
                    status: 1,
                    teamId: element
                  }
                });
            }
            for (let element of uncommonNewArray) {
              let teamId = element;
              const newEmployee = {
                businessId,
                scheduleId,
                teamId
              };
              await dbTSM.teamschedulemap.create(newEmployee);
            }
            //end of updating team
          }

          //show updated data
          let updated_data = await dbSchedule.schedule.findAll({
            where: {
              scheduleId: newScheduleID
            }
          });
          //end of showing updated data

          dbSchedule.sequelize.close();
          dbTSM.sequelize.close();
          dbCSM.sequelize.close();
          dbSubscription.sequelize.close();
          dbPricepack.sequelize.close();
          dbBusiness.sequelize.close();
          return await updated_data;
        }
      }
    } catch (error) {
      throw error;
    }
  };

  updateSchedule = async (
    information,
    scheduleId,
    businessId,
    teamId,
    scheduleName,
    scheduleDate,
    startTime,
    endTime,
    centerId,
    batchId,
    is_repeat,
    repeatation_type,
    monthly_type,
    repeat_number,
    repeat_days,
    repeat_ends,
    repeat_ends_date,
    numberOfClients,
    comments
  ) => {
    try {
      //==============================connections======================================
      let dbSchedule = createInstance(["clientSchedule"], information);
      let dbCSM = createInstance(["clientScheduleMap"], information);
      var dbTSM = createInstance(["teamScheduleMap"], information);
      let dbSubscription = createInstance(["clientSubscription"], information);
      let dbPricepack = createInstance(["clientPricepack"], information);
      let dbBusiness = createInstance(["clientBusiness"], information);

      //================a checking between givenRepeatDays and businessDays=====================
      let final_repeat_days = await getRepeatDays(repeat_days);
      let business = await dbBusiness.businessinfo.find({
        attributes: ["business_days"],
        where: {
          status: 1,
          businessId: businessId
        },
        raw: true
      });
      if (business.business_days == undefined || business.business_days == null || business.business_days.trim() == "") {
        throw new ApplicationError(
          "Please Add Business Days to Proceed !",
          401
        );
      }
      let bdays = await commaSeparatedValues(business.business_days);
      if (is_repeat == 1 && repeatation_type == 2) {
        let repeatDays = await commaSeparatedValues(final_repeat_days);
        for (let rd of repeatDays) {
          if (bdays.includes(rd) == false) {
            throw new ApplicationError(
              "Error in repeat_days. given days and business days are different !",
              401
            );
          }
        }
      }

      let findSchedule = await dbSchedule.schedule.find({
        where: {
          scheduleId: scheduleId,
          status: 1
        },
        raw: true
      });
      //============================schedule isn't found==========================
      if (findSchedule == null) {
        throw new ApplicationError(
          "schedule is not found",
          401
        );
      }
      //=============================schedule is found=============================
      else {
        const Op = Sequelize.Op;
        let checkSchedule = await dbSchedule.schedule.findAll({
          where: {
            scheduleDate: scheduleDate,
            startTime: startTime,
            endTime: endTime,
            centerId: centerId,
            status: 1,
            scheduleId: {
              [Op.notIn]: [scheduleId]
            }
          },
          raw: true
        });
        //date and time is already scheduled
        if (checkSchedule.length > 0) {
          throw new ApplicationError("Conflicting Date and Time", 409);
        }
        //if the schedule date and time exist
        else if (!checkSchedule[0]) {
          //schedule time calculation
          let repeatEndsDate, RepeatEndsDateHyphen;
          if (repeat_ends == 1) {
            repeatEndsDate = new Date(repeat_ends_date);
          } else if (repeat_ends == 2) {
            repeatEndsDate = new Date(scheduleDate);
            repeatEndsDate.setFullYear(repeatEndsDate.getFullYear() + 5);
          } else {
            throw new ApplicationError(
              "Error in repeat_ends",
              401
            );
          }
          RepeatEndsDateHyphen = await hyphenDateFormat(repeatEndsDate);
          let scheduleTimeTable = await getScheduleTimeTable(
            startTime,
            endTime,
            scheduleDate,
            centerId,
            is_repeat,
            repeatation_type = is_repeat == 0 ? 0 : repeatation_type,
            monthly_type = is_repeat == 0 ? 0 : monthly_type,
            repeat_number = is_repeat == 0 ? 0 : repeat_number,
            repeat_days = is_repeat == 0 ? 0 : final_repeat_days,
            repeat_ends = is_repeat == 0 ? 0 : repeat_ends,
            repeat_ends_date = is_repeat == 0 ? scheduleDate : RepeatEndsDateHyphen,
            bdays
          );
          if (typeof scheduleTimeTable == 'undefined') {
            throw new ApplicationError(
              "Please enter valid data",
              409
            );
          }
          //checking client attendance
          let clients = await dbCSM.clientschedulemap.findAll({
            where: {
              status: 1,
              scheduleId: scheduleId
            },
            raw: true
          });
          var flag = 0;
          for (let i = 0; i < clients.length; i++) {
            let arr = Object.values(clients[i].attendanceType);
            arr.forEach(function (x) {
              if (x["atd"] != 4) {
                flag = 1;
              }
            })
          }
          //filtering the common and uncommon team members
          let tsMap = await dbTSM.teamschedulemap.findAll({
            where: {
              scheduleId: scheduleId
            },
            raw: true
          });
          var oldArray = tsMap.map((element) => element.teamId);
          var teamIdArray = await commaSeparatedValues(teamId);
          var commonArray = await intersect_arrays(teamIdArray, oldArray);
          var uncommonOldArray = await diff_arrays(oldArray, commonArray); //uncommon values of commonArray won't be shown
          var uncommonNewArray = await diff_arrays(teamIdArray, commonArray);


          //==================no client attendance. delete old items and insert new=====================
          if (flag === 0) {
            let x = await dbSchedule.schedule.findAll({
              where: {
                scheduleId: scheduleId
              }
            });
            //updating schedule
            let schedules = await dbSchedule.schedule.create({
              scheduleDate: scheduleDate,
              scheduleName: scheduleName,
              startTime: startTime,
              endTime: endTime,
              centerId: centerId,
              numberOfClients: numberOfClients.trim() == "" ? null : parseInt(numberOfClients),
              comments: comments,
              scheduleTime: scheduleTimeTable,
              repeat_ends_date: repeat_ends_date,
              parent_scheduleId: findSchedule.parent_scheduleId,
              businessId: businessId,
              serviceId: x[0].serviceId,
              is_repeat: is_repeat,
              repeatation_type: is_repeat == 0 ? null : repeatation_type,
              repeat_number: is_repeat == 0 ? null : repeat_number,
              repeat_days: is_repeat == 0 ? null : final_repeat_days,
              repeat_ends: is_repeat == 0 ? 1 : repeat_ends,
              repeat_ends_date: is_repeat == 0 ? scheduleDate : repeat_ends_date,
              updateDateTime: Date.now()
            });

            let newScheduleId = await dbSchedule.schedule.findAll({
              attributes: ['scheduleId'],
              where: {
                scheduleId: schedules.scheduleId,
                status: 1
              }
            });
            var newScheduleID = newScheduleId[0].scheduleId;
            let updateSchedule = await dbSchedule.schedule.update({
              status: 0
            }, {
                where: {
                  scheduleId: scheduleId
                }
              });
            //updating client schedule map. inserting subscribed clients into updated schedule
            for (let i = 0; i < clients.length; i++) {
              let subscriptions = await dbSubscription.subscriptions.findAll({
                where: {
                  status: 1,
                  clientId: clients[i].clientId,
                  subscriptionId: clients[i].subscriptionId
                },
                raw: true
              });
              if (subscriptions.length > 0 && subscriptions[0].status == 1) { //subscription is found and ongoing
                let pricepack = await dbPricepack.pricepacks.findAll({
                  where: {
                    status: 1,
                    pricepackId: subscriptions[0].pricepacks
                  },
                  raw: true
                });
                let expiryDate = new Date(scheduleDate);
                expiryDate.setDate(expiryDate.getDate() + parseInt(pricepack[0].pricepackValidity));
                let scheduleEndsDay = await hyphenDateFormat(expiryDate);
                let endDay = (new Date(repeatEndsDate) > new Date(scheduleEndsDay)) ? scheduleEndsDay : RepeatEndsDateHyphen;

                // let atdType = await getAttendanceJSON(scheduleTimeTable, scheduleTimeTable[0].scdate, pricepack[0].pricepackValidity);

                let subscriptionDate = new Date(subscriptions[0].subscriptionDateTime);
                let pval = parseInt(pricepack[0].pricepackValidity);
                let subscriptionExpiry = subscriptionDate.getTime() + (pval * 24 * 60 * 60 * 1000);
                let scdate = (new Date(scheduleTimeTable[0].scdate)) > (new Date(subscriptionDate)) ?
                  new Date(scheduleTimeTable[0].scdate) : new Date(subscriptionDate);
                let atdType = await getAttendanceSheet(scheduleTimeTable, scdate, subscriptionExpiry);

                let clientschedulesmap = await dbCSM.clientschedulemap.create({
                  businessId: businessId,
                  scheduleId: schedules.scheduleId,
                  clientId: clients[i].clientId,
                  attendanceType: atdType,
                  subscriptionId: clients[i].subscriptionId,
                  parentScheduleId: schedules.scheduleId,
                  masterParentScheduleId: schedules.scheduleId
                });
              }
            }
            let clients_update = await dbCSM.clientschedulemap.update({
              status: 0
            }, {
                where: {
                  scheduleId: scheduleId
                },
                raw: true
              });
            //updating team-schedule-map 
            for (let element of oldArray) {
              await dbTSM.teamschedulemap.update({
                status: 0
              }, {
                  where: {
                    status: 1,
                    teamId: element,
                    scheduleId: scheduleId
                  }
                });
            }
            for (let element of teamIdArray) {
              let x = await dbTSM.teamschedulemap.create({
                businessId: businessId,
                scheduleId: newScheduleID,
                teamId: element
              });
            }
          }
          //=======================attendance found. update only=======================
          else {
            let schedules = await dbSchedule.schedule.find({
              where: {
                scheduleId: scheduleId
              },
              raw: true
            });
            let ScheduleDate = schedules.scheduleTime;
            let newScheduleDate = [];
            for (let i = 0; i < ScheduleDate.length; i++) {

              if ((new Date(ScheduleDate[i]["scdate"]).getTime()) > (new Date().getTime())) {
                console.log(ScheduleDate[i]["scdate"]);
                ScheduleDate[i]["scheduleEndTime"] = endTime;
                ScheduleDate[i]["scheduleStartTime"] = startTime;
                newScheduleDate.push(ScheduleDate[i]);
              } else {
                newScheduleDate.push(ScheduleDate[i]);
              }
            }

            // //Update schedule
            let newschedules = await dbSchedule.schedule.update({
              startTime: startTime,
              scheduleName: scheduleName,
              centerId: centerId,
              numberOfClients: numberOfClients.trim() == "" ? findSchedule.numberOfClients : parseInt(numberOfClients),
              endTime: endTime,
              scheduleTime: newScheduleDate,
              updateDateTime: Date.now()
            }, {
                where: {
                  scheduleId: scheduleId
                }
              });
            // end of updating schedule

            var newScheduleID = scheduleId;
            let AttendanceDate = [];

            //updating clientschedulemap table
            for (let client of clients) {

              let subscriptions = await dbSubscription.subscriptions.findAll({
                where: {
                  status: 1,
                  clientId: client.clientId,
                  subscriptionId: client.subscriptionId
                },
                raw: true
              });
              if (subscriptions.length == 0 || subscriptions[0].status == 0) {
                let clients_update = await dbCSM.clientschedulemap.update({
                  status: 0
                }, {
                    where: {
                      clientId: client.clientId
                    },
                    raw: true
                  });
              } else {
                //Update the NEw attendanceType here
                let clients_update = await dbCSM.clientschedulemap.findAll({
                  where: {
                    clientId: client.clientId,
                    scheduleId: scheduleId
                  }
                });


                let x = clients_update[0].attendanceType;

                let newAttendanceType = [];
                for (let i = 0; i < x.length; i++) {
                  if ((new Date(x[i]["scdate"]).getTime()) > (new Date().getTime())) {
                    x[i]["scheduleEndTime"] = endTime;
                    x[i]["scheduleStartTime"] = startTime;
                    newAttendanceType.push(x[i]);
                    AttendanceDate.push(x[i]["scdate"]);
                  } else {
                    newAttendanceType.push(x[i]);
                  }
                }

                let UpdatedClients = await dbCSM.clientschedulemap.update({
                  attendanceType: newAttendanceType
                }, {
                    where: {
                      clientId: client.clientId,
                      scheduleId: scheduleId
                    }
                  });
              }
            }
            //team update
            for (let element of commonArray) {
              var TSUpdate = await dbTSM.teamschedulemap.update({
                status: 1
              }, {
                  where: {
                    status: 0,
                    teamId: element,
                    scheduleId: scheduleId
                  }
                });
            }
            for (let element of uncommonOldArray) {
              await dbTSM.teamschedulemap.update({
                status: 0
              }, {
                  where: {
                    status: 1,
                    teamId: element
                  }
                });
            }
            for (let element of uncommonNewArray) {
              let teamId = element;
              const newEmployee = {
                businessId,
                scheduleId,
                teamId
              };
              await dbTSM.teamschedulemap.create(newEmployee);
            }
          }

          //=========================show updated data========================
          let updated_data = await dbSchedule.schedule.findAll({
            where: {
              scheduleId: newScheduleID
            }
          });

          dbSchedule.sequelize.close();
          dbTSM.sequelize.close();
          dbCSM.sequelize.close();
          dbSubscription.sequelize.close();
          dbPricepack.sequelize.close();
          dbBusiness.sequelize.close();
          return await updated_data;
        }
      }
    } catch (error) {
      throw error;
    }
  };

  remove = async (information, scheduleId) => {
    try {
      let db = createInstance(["clientSchedule"], information);
      let schedules = await db.schedule.update({
        status: 0,
        updateDateTime: Date.now()
      }, {
          where: {
            scheduleId: scheduleId
          }
        });
      db.sequelize.close();
      return schedules;
    } catch (error) {
      throw error;
    }
  };

  remove = async (information, scheduleId) => {
    try {
      let db = createInstance(["clientSchedule"], information);
      let schedules = await db.schedule.update({
        status: 0,
        updateDateTime: Date.now()
      }, {
          where: {
            scheduleId: scheduleId
          }
        });
      db.sequelize.close();
      return schedules;
    } catch (error) {
      throw error;
    }
  };

  //Adding new clients to schedule
  addNewClient_24_07 = async (information, businessId, clientId, scheduleId, subscriptionId) => {
    try {
      //============connections====================
      let dbSchedule = createInstance(["clientSchedule"], information);
      let dbBusiness = createInstance(["clientBusiness"], information);
      let dbCSM = createInstance(["clientScheduleMap"], information);
      let dbPricepack = createInstance(["clientPricepack"], information);
      let dbSubscription = createInstance(["clientSubscription"], information);
      dbSubscription.subscriptions.belongsTo(dbPricepack.pricepacks, {
        foreignKey: "pricepacks",
        targetKey: "pricepackId"
      });

      //====================declarations===================
      let clientIdArr = await commaSeparatedValues(clientId);
      let subscriptionIdArr = await commaSeparatedValues(subscriptionId);
      if (clientIdArr.length != subscriptionIdArr.length) {
        throw new ApplicationError(
          "number of clients and subscriptions doesn't match",
          401
        );
      }

      //==============booking details=================
      var checkSchedule = await dbSchedule.schedule.findAll({
        where: {
          scheduleId: scheduleId,
          //parent_scheduleId: scheduleId,//24-07-18 unable to add new clients into existing schedule which is edited
          status: 1
        },
        raw: true
      });
      if (!checkSchedule[0]) {
        throw new ApplicationError(
          "schedule not found",
          401
        );
      }
      let centerId = checkSchedule[0].centerId;
      let startTime = checkSchedule[0].startTime;
      let endTime = checkSchedule[0].endTime;
      let scheduleTime = checkSchedule[0].scheduleTime;
      let today = new Date();
      today.setHours(0, 0, 0, 0);
      let dateArr = [];
      for (let schedule of scheduleTime) {
        if (new Date(schedule.scdate) >= today) {
          dateArr.push(schedule.scdate);
        }
      }

      //==================clients=======================
      let checkClientSchedule = await dbCSM.clientschedulemap.findAll({
        where: {
          clientId: clientId,
          scheduleId: scheduleId,
          status: 1
        },
        raw: true
      });
      if (checkClientSchedule.length > 0) {
        throw new ApplicationError(
          "client is already assigned into this schedule",
          401
        );
      }
      let subArr = [];
      for (let i = 0; i < clientIdArr.length; i++) {
        let subscription = await dbSubscription.subscriptions.find({
          where: {
            status: 1,
            clientId: clientIdArr[i],
            subscriptionId: subscriptionIdArr[i]
          },
          include: [
            { model: dbPricepack.pricepacks, attributes: ['pricepackValidity'] }
          ],
          raw: true
        });
        if (subscription.length < 1) {
          throw new ApplicationError(
            "Error in Subscription",
            401
          );
        } else {
          subArr.push(subscription['pricepack.pricepackValidity']);
        }
      }
      let returnVal = [];
      for (let client = 0; client < clientIdArr.length; client++) {
        let getDates = [];
        for (let val = 0; val < subArr[client]; val++) {
          getDates.push({
            scdate: dateArr[val],
            atd: 4,
            atdBy: "",
            atdDate: "",
            atdTime: "",
            reschedule: 1,
            rescheduleId: scheduleId,
            rescheduleDate: dateArr[val],
            scheduleStartTime: startTime,
            scheduleEndTime: endTime,
            scheduleFrom: "",
            centerId: centerId,
            rescheduleBy: "",
            comment: "",
          });
        }
        let addClient = await dbCSM.clientschedulemap.create({
          businessId,
          scheduleId,
          subscriptionId: subscriptionIdArr[client],
          parentScheduleId: scheduleId,
          masterParentScheduleId: scheduleId,
          clientId: clientIdArr[client],
          attendanceType: getDates
        });
        returnVal.push(addClient);
      }

      dbSchedule.sequelize.close();
      dbBusiness.sequelize.close();
      dbCSM.sequelize.close();
      dbPricepack.sequelize.close();
      dbSubscription.sequelize.close();
      return returnVal;
    } catch (error) {
      throw error;
    }
  };

  addNewClient = async (information, businessId, clientId, scheduleId, subscriptionId) => {
    try {
      let dbSchedule = createInstance(["clientSchedule"], information);
      let dbCSM = createInstance(["clientScheduleMap"], information);
      let dbPricepack = createInstance(["clientPricepack"], information);
      let dbSubscription = createInstance(["clientSubscription"], information);

      let clientIdArr = await commaSeparatedValues(clientId);
      let subscriptionIdArr = await commaSeparatedValues(subscriptionId);
      if (clientIdArr.length != subscriptionIdArr.length) {
        throw new ApplicationError(
          "number of clients and subscriptions doesn't match",
          401
        );
      }
      let schedule = await dbSchedule.schedule.find({
        where: {
          scheduleId: scheduleId
        },
        raw: true
      });
      if (schedule == null) {
        throw new ApplicationError(
          "schedule is not found",
          401
        );
      }
      let checkClientSchedule = await dbCSM.clientschedulemap.findAll({
        where: {
          clientId: clientId,
          scheduleId: scheduleId,
          status: 1
        },
        raw: true
      });
      if (checkClientSchedule.length > 0) {
        throw new ApplicationError(
          "client is already assigned into this schedule",
          401
        );
      }

      let returnVal = [];
      for (let i = 0; i < clientIdArr.length; i++) {
        let subscriptions = await dbSubscription.subscriptions.findAll({
          where: {
            status: 1,
            clientId: clientIdArr[i],
            subscriptionId: subscriptionIdArr[i]
          },
          raw: true
        });
        if (subscriptions.length > 0) {
          let pricepack = await dbPricepack.pricepacks.findAll({
            where: {
              status: 1,
              pricepackId: subscriptions[0].pricepacks
            },
            raw: true
          });
          let subscriptionDate = new Date(subscriptions[0].subscriptionDateTime);
          let pval = parseInt(pricepack[0].pricepackValidity);
          let subscriptionExpiry = subscriptionDate.getTime() + (pval * 24 * 60 * 60 * 1000);
          let scdate = (new Date()) > (new Date(subscriptionDate)) ?
            new Date() : new Date(subscriptionDate);
          // let atdType = await getAtdSheet(schedule.scheduleTime, scdate, subscriptionExpiry);
          let startDate = await hyphenDateFormat(scdate);
          let atdType = await getAtdSheet(schedule.scheduleTime, startDate, subscriptionExpiry);
          let addClient = await dbCSM.clientschedulemap.create({
            businessId,
            scheduleId,
            subscriptionId: subscriptionIdArr[i],
            parentScheduleId: scheduleId,
            masterParentScheduleId: scheduleId,
            clientId: clientIdArr[i],
            attendanceType: atdType
          });
          returnVal.push(addClient);
        }
      }

      dbSchedule.sequelize.close();
      dbCSM.sequelize.close();
      dbPricepack.sequelize.close();
      dbSubscription.sequelize.close();
      return returnVal;
    } catch (error) {
      throw error;
    }
  };

  // Calendar View Booking
  showScheduleOnDate__based_on_booking = async (information, businessId, date, filterPrivateFields = true) => {
    try {
      let dbSchedule = createInstance(["clientSchedule"], information);
      let dbCSM = createInstance(["clientScheduleMap"], information);
      let dbClients = createInstance(["client"], information);
      let dbService = createInstance(["clientService"], information);
      dbSchedule.schedule.belongsTo(dbService.services, {
        foreignKey: "serviceId",
        targetKey: "serviceId"
      });
      let schedules = await dbSchedule.schedule.findAll({
        where: {
          status: 1,
          businessId: businessId
        },
        include: [{
          model: dbService.services,
          attributes: ['serviceName']
        }],
        raw: true
      });

      let mergedSchedules = [];
      for (let schedule of schedules) {

        let mergedInformation = [];
        let countPresent = 0,
          countAbsent = 0,
          countExcused = 0,
          countUnmarked = 0,
          countTotal = 0;

        let x = schedule["scheduleTime"];
        for (let i = 0; i < x.length; i++) {

          let mergedClients = [];
          let scheduleAttendance = {
            present: "0",
            absent: "0",
            excused: "0",
            unmarked: "0",
            totalClients: 0,
            markedClients: 0
          };
          let keyValuePair = { rescheduled: 1, message: "", comment: "", messageRescheduleFrom: "" };
          if (+(new Date(x[i]["scdate"])) == +(new Date(date))) {
            if (x[i]["reschedule"] == 2) { //if rescheduled
              let clientsOfSchedule = await dbCSM.clientschedulemap.findAll({
                where: {
                  scheduleId: schedule.scheduleId,
                  status: 1
                },
                raw: true
              });

              for (let cs of clientsOfSchedule) {
                let clients = await dbClients.clients.findAll({
                  where: {
                    clientId: cs.clientId,
                    status: 1
                  },
                  raw: true
                });
                if (clients.length > 0) {
                  var isphoto = (clients[0].photoUrl === null) ? KNACK_UPLOAD_URL.localUrl + 'noimage.jpg' : KNACK_UPLOAD_URL.localUrl + clients[0].photoUrl;

                  mergedClients = mergedClients.concat({ clientId: clients[0].clientId, clientName: clients[0].clientName, contactNumber: clients[0].contactNumber, emailId: clients[0].emailId, dateOfBirth: clients[0].dateOfBirth, clientJoiningDate: clients[0].clientJoiningDate, createDateTime: clients[0].createDateTime, updateDateTime: clients[0].updateDateTime, clientphoto: isphoto, status: clients[0].status });
                }
              }
              //

              let message = x[i]["rescheduleDate"].trim() == "" ? "" : 'Booking Rescheduled to ' + x[i]["rescheduleDate"];
              let messageRescheduleFrom = x[i]["scheduleFrom"].trim() == "" ? "" : 'Booking Rescheduled from ' + x[i]["scheduleFrom"];
              let comment = x[i]["comment"];
              keyValuePair = { rescheduled: 2, message: message, comment: comment, messageRescheduleFrom: messageRescheduleFrom };
              let sdate = new Date(date);
              let obj = { scheduleId: schedule.scheduleId };
              scheduleAttendance = await attendanceInfoOfClientAndDate(information, obj, sdate);
            } else if (x[i]["reschedule"] == 1) {
              //if schedule is on that day
              let clientsOfSchedule = await dbCSM.clientschedulemap.findAll({
                where: {
                  scheduleId: schedule.scheduleId,
                  status: 1
                },
                raw: true
              });
              for (let cs of clientsOfSchedule) { //getting clients of the schedule along with attendances
                let attendanceKeys = [];
                let attendanceValues = [];

                let key = cs.attendanceType;

                for (let j = 0; j < key.length; j++) {
                  attendanceKeys.push(key[j]["scdate"]);
                  attendanceValues.push(key[j]);
                }

                for (let k = 0; k < attendanceKeys.length; k++) {
                  let ccv = (attendanceKeys[k] == date) ? 'match' : 'not match';

                  if (attendanceKeys[k] == date && attendanceValues[k]["atd"] != 5 && attendanceValues[k]["atd"] != 6) { // noteeeeeeeeee
                    if (attendanceValues[k]["atd"] == 1) countPresent++;
                    if (attendanceValues[k]["atd"] == 2) countAbsent++;
                    if (attendanceValues[k]["atd"] == 3) countExcused++;
                    if (attendanceValues[k]["atd"] == 4) countUnmarked++;
                    countTotal++;
                    let clients = await dbClients.clients.findAll({
                      where: {
                        clientId: cs.clientId,
                        status: 1
                      },
                      raw: true
                    });
                    if (clients.length > 0) {
                      var isphoto = (clients[0].photoUrl === null) ? KNACK_UPLOAD_URL.localUrl + 'noimage.jpg' : KNACK_UPLOAD_URL.localUrl + clients[0].photoUrl;

                      mergedClients = mergedClients.concat({ clientId: clients[0].clientId, clientName: clients[0].clientName, contactNumber: clients[0].contactNumber, emailId: clients[0].emailId, dateOfBirth: clients[0].dateOfBirth, clientJoiningDate: clients[0].clientJoiningDate, createDateTime: clients[0].createDateTime, updateDateTime: clients[0].updateDateTime, clientphoto: isphoto, status: clients[0].status });
                    }
                  }
                }
              }

              //end of finding attendance of individual clients
              let perPresent, perAbsent, perExcused, perUnmarked;
              // let keyValuePairArray = [];
              if (mergedClients.length > 0) {
                perPresent = (100 / countTotal * countPresent).toFixed(0),
                  perAbsent = (100 / countTotal * countAbsent).toFixed(0),
                  perExcused = (100 / countTotal * countExcused).toFixed(0),
                  perUnmarked = (100 / countTotal * countUnmarked).toFixed(0);

                scheduleAttendance = {
                  present: perPresent,
                  absent: perAbsent,
                  excused: perExcused,
                  unmarked: perUnmarked,
                  totalClients: countTotal,
                  markedClients: countPresent + countAbsent + countExcused
                };
              }
              let messageRescheduleFrom = x[i]["scheduleFrom"].trim() == "" ? "" : 'Booking Rescheduled from ' + x[i]["scheduleFrom"];
              keyValuePair = { rescheduled: 1, message: "", comment: "", messageRescheduleFrom: messageRescheduleFrom };
            } else if (x[i]["reschedule"] == 3) { //cancelled booking
              let clientsOfSchedule = await dbCSM.clientschedulemap.findAll({
                where: {
                  scheduleId: schedule.scheduleId,
                  status: 1
                },
                raw: true
              });

              for (let cs of clientsOfSchedule) {
                let clients = await dbClients.clients.findAll({
                  where: {
                    clientId: cs.clientId,
                    status: 1
                  },
                  raw: true
                });
                var isphoto = (clients[0].photoUrl === null) ? KNACK_UPLOAD_URL.localUrl + 'noimage.jpg' : KNACK_UPLOAD_URL.localUrl + clients[0].photoUrl;
                mergedClients = mergedClients.concat({ clientId: clients[0].clientId, clientName: clients[0].clientName, contactNumber: clients[0].contactNumber, emailId: clients[0].emailId, dateOfBirth: clients[0].dateOfBirth, clientJoiningDate: clients[0].clientJoiningDate, createDateTime: clients[0].createDateTime, updateDateTime: clients[0].updateDateTime, clientphoto: isphoto, status: clients[0].status });
              }

              let message = "schedule is cancelled";
              let messageRescheduleFrom = "";
              let comment = x[i]["comment"];
              keyValuePair = { rescheduled: 3, message: message, comment: comment, messageRescheduleFrom: messageRescheduleFrom };
              let sdate = new Date(date);
              let obj = { scheduleId: schedule.scheduleId };
              scheduleAttendance = await attendanceInfoOfClientAndDate(information, obj, sdate);
              scheduleAttendance["present"] = scheduleAttendance["present"] == null ? 0 : scheduleAttendance["present"];
              scheduleAttendance["absent"] = scheduleAttendance["absent"] == null ? 0 : scheduleAttendance["absent"];
              scheduleAttendance["excused"] = scheduleAttendance["excused"] == null ? 0 : scheduleAttendance["excused"];
              scheduleAttendance["unmarked"] = scheduleAttendance["unmarked"] == null ? 0 : scheduleAttendance["unmarked"];
            }
            let keyValuePairArray = [];
            Array.prototype.push.apply(keyValuePairArray, [{ schedule: schedule }]);
            Array.prototype.push.apply(keyValuePairArray, [{ status: keyValuePair }]);
            Array.prototype.push.apply(keyValuePairArray, [{ clients: mergedClients }]);
            Array.prototype.push.apply(keyValuePairArray, [{ attendance: scheduleAttendance }]);

            let keyValuePairObj = [];
            keyValuePairObj = Object.assign.apply(Object, keyValuePairArray);
            mergedInformation = mergedInformation.concat(keyValuePairObj);
            break;
          }
        }
        mergedSchedules = mergedSchedules.concat(mergedInformation);
      }

      dbClients.sequelize.close();
      dbCSM.sequelize.close();
      dbSchedule.sequelize.close();
      return await mergedSchedules;
    } catch (error) {
      throw error;
    }
  };

  showScheduleOnDate = async (information, businessId, date, filterPrivateFields = true) => {
    try {
      let dbSchedule = createInstance(["clientSchedule"], information);
      let dbCSM = createInstance(["clientScheduleMap"], information);
      let dbClients = createInstance(["client"], information);
      let dbService = createInstance(["clientService"], information);
      dbSchedule.schedule.belongsTo(dbService.services, {
        foreignKey: "serviceId",
        targetKey: "serviceId"
      });
      let schedules = await dbSchedule.schedule.findAll({
        where: {
          status: 1,
          businessId: businessId
        },
        include: [{
          model: dbService.services,
          attributes: ['serviceName']
        }],
        raw: true
      });

      let mergedSchedules = [];
      for (let schedule of schedules) {
        let mergedInformation = [];
        for (let scheduletime of schedule.scheduleTime) {
          let mergedClients = [];
          let scheduleAttendance = {
            present: "0",
            absent: "0",
            excused: "0",
            unmarked: "0",
            totalClients: 0,
            markedClients: 0
          };
          let keyValuePair = { rescheduled: 1, message: "", comment: "", messageRescheduleFrom: "" };
          if (+(new Date(scheduletime.scdate)) === +(new Date(date))) {
            let countPresent = 0,
              countAbsent = 0,
              countExcused = 0,
              countUnmarked = 0,
              countTotal = 0;
            if (scheduletime.reschedule == 2) { //if rescheduled
              let clients = await dbCSM.clientschedulemap.findAll({
                where: {
                  scheduleId: schedule.scheduleId,
                  status: 1
                },
                raw: true
              });
              for (let client of clients) {
                let clientDetail = await dbClients.clients.findAll({
                  where: {
                    clientId: client.clientId,
                    status: 1
                  },
                  raw: true
                });
                if (clientDetail.length > 0) {
                  var isphoto = (clientDetail[0].photoUrl === null) ? KNACK_UPLOAD_URL.localUrl + 'noimage.jpg' : KNACK_UPLOAD_URL.localUrl + clientDetail[0].photoUrl;
                  mergedClients = mergedClients.concat({ clientId: clientDetail[0].clientId, clientName: clientDetail[0].clientName, contactNumber: clientDetail[0].contactNumber, emailId: clientDetail[0].emailId, dateOfBirth: clientDetail[0].dateOfBirth, clientJoiningDate: clientDetail[0].clientJoiningDate, createDateTime: clientDetail[0].createDateTime, updateDateTime: clientDetail[0].updateDateTime, clientphoto: isphoto, status: clientDetail[0].status });
                }
              }
              let message = scheduletime.rescheduleDate.trim() == "" ? "" : 'Booking Rescheduled to ' + scheduletime.rescheduleDate;
              let messageRescheduleFrom = scheduletime.scheduleFrom.trim() == "" ? "" : 'Booking Rescheduled from ' + scheduletime.scheduleFrom;
              let comment = scheduletime.comment;
              keyValuePair = { rescheduled: 2, message: message, comment: comment, messageRescheduleFrom: messageRescheduleFrom };
              let sdate = new Date(date);
              let obj = { scheduleId: schedule.scheduleId };
              scheduleAttendance = await attendanceInfoOfClientAndDate(information, obj, sdate);
            } else if (scheduletime.reschedule == 1) {
              let clients = await dbCSM.clientschedulemap.findAll({
                where: {
                  scheduleId: schedule.scheduleId,
                  status: 1
                },
                raw: true
              });
              for (let client of clients) { //getting clients of the schedule along with attendances
                for (let clientAttendance of client.attendanceType) {
                  let clientAtdDateTime = new Date(clientAttendance.scdate + ' ' + clientAttendance.scheduleStartTime);
                  let scheduleDateTime = new Date(scheduletime.scdate + ' ' + scheduletime.scheduleStartTime);
                  if (+clientAtdDateTime === +scheduleDateTime && clientAttendance.atd != 5 && clientAttendance.atd != 6) {
                    if (clientAttendance.atd == 1) countPresent++;
                    if (clientAttendance.atd == 2) countAbsent++;
                    if (clientAttendance.atd == 3) countExcused++;
                    if (clientAttendance.atd == 4) countUnmarked++;
                    countTotal++;
                    let clientDetail = await dbClients.clients.findAll({
                      where: {
                        clientId: client.clientId,
                        status: 1
                      },
                      raw: true
                    });
                    if (clientDetail.length > 0) {
                      var isphoto = (clientDetail[0].photoUrl === null) ? KNACK_UPLOAD_URL.localUrl + 'noimage.jpg' : KNACK_UPLOAD_URL.localUrl + clientDetail[0].photoUrl;
                      mergedClients = mergedClients.concat({ clientId: clientDetail[0].clientId, clientName: clientDetail[0].clientName, contactNumber: clientDetail[0].contactNumber, emailId: clientDetail[0].emailId, dateOfBirth: clientDetail[0].dateOfBirth, clientJoiningDate: clientDetail[0].clientJoiningDate, createDateTime: clientDetail[0].createDateTime, updateDateTime: clientDetail[0].updateDateTime, clientphoto: isphoto, status: clientDetail[0].status });
                    }
                  }
                }
              }
              let perPresent, perAbsent, perExcused, perUnmarked;
              if (mergedClients.length > 0) {
                perPresent = (100 / countTotal * countPresent).toFixed(0),
                  perAbsent = (100 / countTotal * countAbsent).toFixed(0),
                  perExcused = (100 / countTotal * countExcused).toFixed(0),
                  perUnmarked = (100 / countTotal * countUnmarked).toFixed(0);
                scheduleAttendance = {
                  present: perPresent,
                  absent: perAbsent,
                  excused: perExcused,
                  unmarked: perUnmarked,
                  totalClients: countTotal,
                  markedClients: countPresent + countAbsent + countExcused
                };
              }
              let messageRescheduleFrom = scheduletime.scheduleFrom.trim() == "" ? "" : 'Booking Rescheduled from ' + scheduletime.scheduleFrom;
              keyValuePair = { rescheduled: 1, message: "", comment: "", messageRescheduleFrom: messageRescheduleFrom };
            } else if (scheduletime.reschedule == 3) { //cancelled booking
              let clients = await dbCSM.clientschedulemap.findAll({
                where: {
                  scheduleId: schedule.scheduleId,
                  status: 1
                },
                raw: true
              });

              for (let client of clients) {
                let clientDetail = await dbClients.clients.findAll({
                  where: {
                    clientId: client.clientId,
                    status: 1
                  },
                  raw: true
                });
                var isphoto = (clientDetail[0].photoUrl === null) ? KNACK_UPLOAD_URL.localUrl + 'noimage.jpg' : KNACK_UPLOAD_URL.localUrl + clientDetail[0].photoUrl;
                mergedClients = mergedClients.concat({ clientId: clientDetail[0].clientId, clientName: clientDetail[0].clientName, contactNumber: clientDetail[0].contactNumber, emailId: clientDetail[0].emailId, dateOfBirth: clientDetail[0].dateOfBirth, clientJoiningDate: clientDetail[0].clientJoiningDate, createDateTime: clientDetail[0].createDateTime, updateDateTime: clientDetail[0].updateDateTime, clientphoto: isphoto, status: clientDetail[0].status });
              }

              let message = "schedule is cancelled";
              let messageRescheduleFrom = "";
              let comment = scheduletime.comment;
              keyValuePair = { rescheduled: 3, message: message, comment: comment, messageRescheduleFrom: messageRescheduleFrom };
              let sdate = new Date(date);
              let obj = { scheduleId: schedule.scheduleId };
              scheduleAttendance = await attendanceInfoOfClientAndDate(information, obj, sdate);
              scheduleAttendance["present"] = scheduleAttendance["present"] == null ? 0 : scheduleAttendance["present"];
              scheduleAttendance["absent"] = scheduleAttendance["absent"] == null ? 0 : scheduleAttendance["absent"];
              scheduleAttendance["excused"] = scheduleAttendance["excused"] == null ? 0 : scheduleAttendance["excused"];
              scheduleAttendance["unmarked"] = scheduleAttendance["unmarked"] == null ? 0 : scheduleAttendance["unmarked"];
            }
            let keyValuePairArray = [];
            let uniqueClients = mergedClients.filter((client, index, self) =>
              index === self.findIndex((t) => (
                t.clientId == client.clientId &&
                t.clientName == client.clientName &&
                t.emailId == client.emailId &&
                t.contactNumber == client.contactNumber
              ))
            );
            //if (uniqueClients.length > 0) {
            Array.prototype.push.apply(keyValuePairArray, [{
              schedule: {
                scheduleId: schedule.scheduleId,
                businessId: schedule.businessId,
                centerId: schedule.centerId,
                rescheduleId: schedule.rescheduleId,
                serviceId: schedule.serviceId,
                scheduleName: schedule.scheduleName,
                scheduleDate: scheduletime.scdate,
                startTime: scheduletime.scheduleStartTime,
                endTime: scheduletime.scheduleEndTime,
                scheduleType: schedule.scheduleType,
                parent_scheduleId: schedule.parent_scheduleId,
                schedule_status: schedule.schedule_status,
                // scheduleTime: schedule.scheduleTime,
                numberOfClients: schedule.numberOfClients,
                batchId: schedule.batchId,
                comments: schedule.comments,
                is_repeat: schedule.is_repeat,
                repeatation_type: schedule.repeatation_type,
                repeat_number: schedule.repeat_number,
                repeat_days: schedule.repeat_days,
                monthly_type: schedule.monthly_type,
                repeat_ends: schedule.repeat_ends,
                repeat_ends_date: schedule.repeat_ends_date,
                createDateTime: schedule.createDateTime,
                updateDateTime: schedule.updateDateTime,
                status: schedule.status,
                'service.serviceName': schedule["service.serviceName"]
              }
            }]);
            Array.prototype.push.apply(keyValuePairArray, [{ status: keyValuePair }]);
            Array.prototype.push.apply(keyValuePairArray, [{ clients: uniqueClients }]);
            Array.prototype.push.apply(keyValuePairArray, [{ attendance: scheduleAttendance }]);
            let keyValuePairObj = [];
            keyValuePairObj = Object.assign.apply(Object, keyValuePairArray);
            mergedInformation = mergedInformation.concat(keyValuePairObj);
            //}
          }
        }
        mergedSchedules = mergedSchedules.concat(mergedInformation);
      }

      dbClients.sequelize.close();
      dbCSM.sequelize.close();
      dbSchedule.sequelize.close();
      return await mergedSchedules;
    } catch (error) {
      throw error;
    }
  };

  // Show User Schedules
  showUserSchedules = async (
    information,
    scheduleId,
  ) => {
    try {
      let db = createInstance(["clientSchedule"], information);
      let schedule = await db.schedule.find({
        where: {
          status: 1,
          scheduleId: scheduleId
        },
        raw: true
      });

      let mergedInformation = [];
      if (!schedule.scheduleTime) {
        throw new ApplicationError(
          "Schedule Data Error !",
          401
        );
      } else {
        let mergedSchedules = [];

        for (let key = 0; key < schedule.scheduleTime.length; key++) {
          if (schedule.scheduleTime[key].reschedule == 1) {
            mergedSchedules.push(schedule.scheduleTime[key]);
          }
        }

        //merging center information
        let dbCenter = createInstance(["clientCenter"], information);
        let center = await dbCenter.centers.find({
          where: {
            centerId: schedule.centerId
          },
          raw: true
        });
        dbCenter.sequelize.close();
        let infoArray = [];
        Array.prototype.push.apply(infoArray, [{ scheduleId: schedule.scheduleId }]);
        Array.prototype.push.apply(infoArray, [{ centerId: center.centerId }]);
        Array.prototype.push.apply(infoArray, [{ centerName: center.centerName }]);
        Array.prototype.push.apply(infoArray, [{ scheduleTimeTable: mergedSchedules }]);
        let infoObj = [];
        infoObj = Object.assign.apply(Object, infoArray);
        mergedInformation = mergedInformation.concat(infoObj);
      }
      db.sequelize.close();
      return mergedInformation;
    } catch (error) {
      throw error;
    }
  }

  // User Reschedule
  userReschedule = async (
    information,
    scheduleId,
    rescheduleFrom,
    rescheduleFromTime,
    rescheduleTo,
    startTime,
    endTime,
    centerId,
    comment
  ) => {
    try {
      function compare(a, b) {
        if (new Date(a.scdate) < new Date(b.scdate))
          return -1;
        if (new Date(a.scdate) > new Date(b.scdate))
          return 1;
        return 0;
      }
      let empId = information.split(",")[2];
      let user_id = await CheckMainOrCenterAdmin(information);
      let dbEmployee = createInstance(["clientEmployee"], information);
      let db = createInstance(["clientSchedule"], information);
      let dbClient = createInstance(["client"], information);
      let dbSub = createInstance(["clientSubscription"], information);
      let dbPricepack = createInstance(["clientPricepack"], information);
      let dbCSM = createInstance(["clientScheduleMap"], information);

      let getInfo = information.split(",");
      let adminId = getInfo[2];
      let dbBusinessId = getInfo[1];
      let getLogin = await loginSignUpHelperDB.viewLogin({ businessId: dbBusinessId });

      dbCSM.clientschedulemap.belongsTo(dbSub.subscriptions, {
        targetKey: "subscriptionId",
        foreignKey: "subscriptionId"
      });

      dbSub.subscriptions.belongsTo(dbPricepack.pricepacks, {
        foreignKey: "pricepacks",
        targetKey: "pricepackId"
      });

      // if (+(new Date(rescheduleTo)) === +(new Date(rescheduleFrom))) {
      // throw new ApplicationError(
      // "Can't reschedule on same date",
      // 401
      // );
      // }
      // if (rescheduleFromId == undefined || rescheduleFromId == null || rescheduleFromId.trim() == "") {
      // throw new ApplicationError(
      // "Please provide rescheduleFromId !",
      // 401
      // );
      // }
      if (+(new Date(rescheduleTo)) < +(new Date(rescheduleFrom))) {
        throw new ApplicationError(
          "rescheduleTo must be greater than rescheduleFrom",
          401
        );
      } else {

        let schedule = await db.schedule.find({
          where: {
            status: 1,
            scheduleId: scheduleId
          },
          raw: true
        });

        //checking if the scheduleDate, startTime and centerId exists or not
        for (let key = 0; key < schedule.scheduleTime.length; key++) {
          if (schedule.scheduleTime[key].scdate == rescheduleTo &&
            schedule.scheduleTime[key].scheduleStartTime == startTime &&
            schedule.scheduleTime[key].centerId == centerId
          ) {
            throw new ApplicationError(
              "Cannot reschedule on same date and time and in same same centre !",
              409
            );
          }
        }

        //UPDATE SCHEDULE TABLE
        let found = 0,
          flag = 0;
        let newScheduleTimeTable = [];
        let rescheduledDate = null;
        for (let key = 0; key < schedule.scheduleTime.length; key++) {
          let sc_date = new Date(schedule.scheduleTime[key].scdate + ' ' + schedule.scheduleTime[key].scheduleStartTime);
          let reschedule_from = new Date(rescheduleFrom + ' ' + rescheduleFromTime);
          if (+sc_date === +reschedule_from) {
            if (parseInt(schedule.scheduleTime[key].reschedule) === 2) {
              // throw new ApplicationError(
              // "Cannot reschedule an already rescheduled date",
              // 401
              // );
              // let s_date = schedule.scheduleTime[key].scdate.split("-"), s_time = schedule.scheduleTime[key].scheduleStartTime.split(":");
              // let id = "" + s_date[0] + s_date[1] + s_date[2] + s_time[0] + s_time[1];
              let rescheduleTime = {
                id: schedule.scheduleTime[key].id,
                scdate: schedule.scheduleTime[key].scdate,
                reschedule: schedule.scheduleTime[key].reschedule,
                rescheduleDate: schedule.scheduleTime[key].rescheduleDate,
                scheduleStartTime: schedule.scheduleTime[key].scheduleStartTime,
                scheduleEndTime: schedule.scheduleTime[key].scheduleEndTime,
                scheduleFrom: schedule.scheduleTime[key].scheduleFrom,
                centerId: schedule.scheduleTime[key].centerId,
                rescheduleBy: schedule.scheduleTime[key].rescheduleBy,
                comment: schedule.scheduleTime[key].comment
              };
              newScheduleTimeTable.push(rescheduleTime);
            } else if (parseInt(schedule.scheduleTime[key].reschedule) === 3) {
              throw new ApplicationError(
                "Date is cancelled",
                401
              );
            } else {
              // let s_date = schedule.scheduleTime[key].scdate.split("-"), s_time = schedule.scheduleTime[key].scheduleStartTime.split(":");
              // let id = "" + s_date[0] + s_date[1] + s_date[2] + s_time[0] + s_time[1];
              let newComment = "";
              if (comment) newComment = comment;
              let rescheduleTime = {
                id: schedule.scheduleTime[key].id,
                scdate: schedule.scheduleTime[key].scdate,
                reschedule: 2,
                rescheduleDate: rescheduleTo,
                scheduleStartTime: schedule.scheduleTime[key].scheduleStartTime,
                scheduleEndTime: schedule.scheduleTime[key].scheduleEndTime,
                scheduleFrom: schedule.scheduleTime[key].scheduleFrom,
                centerId: schedule.scheduleTime[key].centerId,
                rescheduleBy: empId,
                comment: newComment
              };
              newScheduleTimeTable.push(rescheduleTime);
              found = 1;
              rescheduledDate = schedule.scheduleTime[key].scdate;
            }
          } else {
            if (+(new Date(schedule.scheduleTime[key].scdate)) > +(new Date(rescheduleTo)) && flag == 0) {
              let s_date = rescheduleFrom.split("-"),
                s_time = startTime.split(":");
              let id = "" + s_date[0] + s_date[1] + s_date[2] + s_time[0] + s_time[1];
              let rescheduleTime = {
                id: id,
                scdate: rescheduleTo,
                reschedule: 1,
                rescheduleDate: rescheduleTo,
                scheduleStartTime: startTime,
                scheduleEndTime: endTime,
                scheduleFrom: rescheduledDate,
                centerId: centerId.trim() == "" ? "" : centerId,
                rescheduleBy: "",
                comment: ""
              };
              newScheduleTimeTable.push(rescheduleTime);
              flag = 1;
            } else if (+(new Date(schedule.scheduleTime[key].scdate)) === +(new Date(rescheduleTo))) {
              // throw new ApplicationError(
              // "Selected date is already scheduled. Please try another one.",
              // 401
              // );
              let s_date = rescheduleFrom.split("-"),
                s_time = startTime.split(":");
              let id = "" + s_date[0] + s_date[1] + s_date[2] + s_time[0] + s_time[1];
              let newSchedule = {
                id: id,
                scdate: rescheduleTo,
                reschedule: 1,
                rescheduleDate: rescheduleTo,
                scheduleStartTime: startTime,
                scheduleEndTime: endTime,
                scheduleFrom: rescheduledDate,
                centerId: centerId.trim() == "" ? "" : centerId,
                rescheduleBy: "",
                comment: ""
              };
              newScheduleTimeTable.push(newSchedule);
              flag = 1;
            }
            newScheduleTimeTable.push(schedule.scheduleTime[key]);
          }
        }
        if (found == 1 && flag == 0) { //for inserting into the last position of the array of date
          let s_date = rescheduleFrom.split("-"),
            s_time = startTime.split(":");
          let id = "" + s_date[0] + s_date[1] + s_date[2] + s_time[0] + s_time[1];
          newScheduleTimeTable.push({
            id: id,
            scdate: rescheduleTo,
            reschedule: 1,
            rescheduleDate: rescheduleTo,
            scheduleStartTime: startTime,
            scheduleEndTime: endTime,
            scheduleFrom: rescheduledDate,
            centerId: centerId.trim() == "" ? "" : centerId,
            rescheduleBy: "",
            comment: ""
          });
        }
        let infoObj = [];
        if (found == 0) {
          throw new ApplicationError(
            "schedule date-time isn't found",
            401
          );
        } else {
          newScheduleTimeTable.sort(compare);
          var schedules = await db.schedule.update({
            scheduleTime: newScheduleTimeTable,
            updateDateTime: Date.now()
          }, {
              where: {
                scheduleId: scheduleId
              }
            });
        }

        //UPDATE CLIENT-SCHEDULE-MAP TABLE
        let clientScheduleMap = await dbCSM.clientschedulemap.findAll({
          where: {
            status: 1,
            scheduleId: scheduleId
          },
          include: [{
            model: dbSub.subscriptions,
            include: [{
              model: dbPricepack.pricepacks
            }],
          }],
          raw: true
        });
        for (let i of clientScheduleMap) {
          let foundClientDate = 0,
            flagClient = 0,
            foundClientDateValue = null;
          let newClientScheduleTimeTable = [];
          for (let key = 0; key < i.attendanceType.length; key++) {
            let sc_date = new Date(i.attendanceType[key].scdate + ' ' + i.attendanceType[key].scheduleStartTime);
            let reschedule_from = new Date(rescheduleFrom + ' ' + rescheduleFromTime);
            if (+sc_date === +reschedule_from) {
              // let s_date = i.attendanceType[key].scdate.split("-"), s_time = i.attendanceType[key].scheduleStartTime.split(":");
              // let id = "" + s_date[0] + s_date[1] + s_date[2] + s_time[0] + s_time[1];
              let newComment = "";
              if (comment) newComment = comment;
              newClientScheduleTimeTable.push({
                id: i.attendanceType[key].id,
                atd: 6,
                atdBy: i.attendanceType[key].atdBy,
                atdDate: i.attendanceType[key].atdDate,
                atdTime: i.attendanceType[key].atdTime,
                centerId: i.attendanceType[key].centerId,
                scdate: i.attendanceType[key].scdate,
                reschedule: 2,
                rescheduleId: i.attendanceType[key].rescheduleId,
                rescheduleDate: rescheduleTo,
                scheduleStartTime: i.attendanceType[key].scheduleStartTime,
                scheduleEndTime: i.attendanceType[key].scheduleEndTime,
                scheduleFrom: i.attendanceType[key].scheduleFrom,
                rescheduleBy: i.attendanceType[key].rescheduleBy,
                comment: newComment
              });
              foundClientDate = 1;
              foundClientDateValue = i.attendanceType[key].scdate;
            } else {
              if (+(new Date(key)) > +(new Date(rescheduleTo)) && flagClient == 0) {
                let s_date = rescheduleFrom.split("-"),
                  s_time = startTime.split(":");
                let id = "" + s_date[0] + s_date[1] + s_date[2] + s_time[0] + s_time[1];
                newClientScheduleTimeTable.push({
                  id: id,
                  atd: 4,
                  atdBy: "",
                  atdDate: "",
                  atdTime: "",
                  centerId: centerId.trim() == "" ? "" : centerId,
                  scdate: rescheduleTo,
                  reschedule: 1,
                  rescheduleId: "",
                  rescheduleDate: rescheduleTo,
                  scheduleStartTime: startTime,
                  scheduleEndTime: endTime,
                  scheduleFrom: i.attendanceType[key].scdate,
                  rescheduleBy: "",
                  comment: ""
                });
                flagClient = 1;
              }
              newClientScheduleTimeTable.push(i.attendanceType[key]);
            }
          }
          if (foundClientDate == 1 && flagClient == 0) {
            let s_date = rescheduleFrom.split("-"),
              s_time = startTime.split(":");
            let id = "" + s_date[0] + s_date[1] + s_date[2] + s_time[0] + s_time[1];
            newClientScheduleTimeTable.push({
              id: id,
              atd: 4,
              atdBy: "",
              atdDate: "",
              atdTime: "",
              centerId: centerId.trim() == "" ? "" : centerId,
              scdate: rescheduleTo,
              reschedule: 1,
              rescheduleId: "",
              rescheduleDate: rescheduleTo,
              scheduleStartTime: startTime,
              scheduleEndTime: endTime,
              scheduleFrom: foundClientDateValue,
              rescheduleBy: "",
              comment: ""
            });
          }

          newClientScheduleTimeTable.sort(compare);
          let clientSchedules = await dbCSM.clientschedulemap.update({
            attendanceType: newClientScheduleTimeTable,
            updateDateTime: Date.now()
          }, {
              where: {
                clientId: i.clientId
              }
            });

          let clientdata = await dbClient.clients.findAll({

            attributes: ["contactNumber", "emailId", "clientName"],
            where: {

              clientId: i.clientId
            },

            raw: true
          }).then(result => {


            let text = "Dear customer, this is to inform you that the booking for " + i["subscription.pricepack.pricepackName"] + " on " + rescheduleFrom + "," + rescheduleFromTime + " has been rescheduled to " + rescheduleTo + "," + startTime + " to " + endTime + " due to " + comment + ". For any other queries, please contact +91 9812345678. ";
            sendSmsMobile(result[0]["contactNumber"], text);

            if (result[0]["emailId"]) {

              let sender = BusinessSchema.Business().find({
                attributes: ["businessName"],
                where: {
                  businessId: dbBusinessId
                },
                raw: true
              }).then(resultsendet => {


                console.log(resultsendet);

                let subject = "Senders name:" + resultsendet.businessName + " Booking rescheduled: From " + rescheduleFrom + " to " + rescheduleTo + " , " + startTime;
                let message = "Hi " + result[0]["clientName"] + " ,this is to inform you that the booking for " + schedule.scheduleName + " on " + rescheduleFrom + "," + rescheduleFromTime + " has been rescheduled to " + rescheduleTo + "," + startTime + " to " + endTime + " due to " + comment + ". For any other queries, please contact +91 9812345678. Kind regards, " + resultsendet.businessName;

                sendEmail(result[0]["emailId"], subject, message);

              });
            }

          });
        }

        dbCSM.sequelize.close();
        db.sequelize.close();

        //Log Entry......
        let NotificationText;
        if (user_id[1].UserStatus == 2) {
          let employees = await dbEmployee.employees.findAll({
            where: {
              employeeId: adminId
            },
            raw: true
          });
          NotificationText = employees[0].employeeName + ' just rescheduled  a booking on ' + rescheduleTo;
        } else {
          NotificationText = "";
        }

        const newLog = {
          businessId: dbBusinessId,
          activity: {
            header: 'Class Rescheduled',
            Status: 2,
            ScheduleId: scheduleId,
            NotificationText: NotificationText,
            RescheduledFrom: rescheduleFrom,
            RescheduledBy: empId,
            UserStatus: user_id[1]["UserStatus"],
            activityDate: new Date(),
            activityTime: new Date(),
            message: 'Class Rescheduled',
          },
          referenceTable: 'clientScheduleModel',
        };
        let dbLog = createInstance(["log"], information);
        let log = await dbLog.log.create(newLog);

        return schedules;
      }
    } catch (error) {
      throw error;
    }
  }

  // Show Client Schedules
  showClientSchedules = async (
    information,
    scheduleId,
    clientId,
    date
  ) => {
    try {
      let dbSchedule = createInstance(["clientSchedule"], information);
      let dbCSM = createInstance(["clientScheduleMap"], information);
      let dbTSM = createInstance(["teamScheduleMap"], information);
      let dbEmployee = createInstance(["clientEmployee"], information);
      let dbCenter = createInstance(["clientCenter"], information);
      dbSchedule.schedule.belongsTo(dbCenter.centers, {
        foreignKey: "centerId",
        targetKey: "centerId",
        as: 'CenterData'
      });
      dbTSM.teamschedulemap.belongsTo(dbEmployee.employees, {
        foreignKey: "teamId",
        targetKey: "employeeId"
      });

      let schedule = await dbSchedule.schedule.findAll({
        include: [{ model: dbCenter.centers, as: 'CenterData' }],
        where: {
          scheduleId: scheduleId
        }
      });
      let Obj = {};
      let Obj1 = {};
      Obj["schedule"] = schedule;
      let clients = await dbCSM.clientschedulemap.findAll({
        where: {
          masterParentScheduleId: scheduleId,
          status: 1,
          clientId: clientId
        },
        raw: true
      });
      let team = await dbTSM.teamschedulemap.findAll({
        include: [{ model: dbEmployee.employees }],
        where: {
          scheduleId: scheduleId,
          status: 1,
        },
        raw: true
      });
      let dateArr = [];
      console.log(clients.length);
      for (let i = 0; i < clients.length; i++) {
        let x = clients[i].attendanceType;
        for (let j = 0; j < x.length; j++) {
          if (x[j]["atd"] == 3) {
            x[j]["selected"] = x[j]["scdate"] == date ? 1 : 0;
            dateArr.push(x[j]);
          }
        }
      }
      for (let eachteam = 0; eachteam < team.length; eachteam++) {
        team[eachteam]['employee.photoUrl'] = KNACK_UPLOAD_URL.localUrl + team[eachteam]['employee.photoUrl'];
      }

      Obj["team"] = team;
      Obj1["dates"] = dateArr;

      dbSchedule.sequelize.close();
      dbCSM.sequelize.close();
      dbTSM.sequelize.close();
      dbEmployee.sequelize.close();
      dbCenter.sequelize.close();

      return [Obj, Obj1];
    } catch (error) {
      throw error;
    }
  }

  //Reschedule details
  rescheduleData = [];
  createReschedule = async (
    information,
    businessId,
    rescheduleId,
    centerId,
    scheduleDate,
    startTime,
    endTime,
    scheduleType,
    parent_scheduleId,
    comments
  ) => {
    try {
      let db = createInstance(["clientSchedule"], information);
      let checkReschedule = await db.schedule.findAll({
        where: {
          scheduleId: rescheduleId,
          status: 1
        },
        raw: true
      });
      var sc_type;
      if (checkReschedule[0].scheduleId) {
        sc_type = 2;
      } else {
        sc_type = 1;
      }
      var parent_scid;
      var schedule_status;
      if (checkReschedule[0]) {
        let db = createInstance(["clientSchedule"], information);
        if (checkReschedule[0].parent_scheduleId) {
          parent_scid = checkReschedule[0].parent_scheduleId;
          let update_schedules = await db.schedule.update({
            schedule_status: 2,
            parent_scheduleId: parent_scid
          }, {
              where: {
                scheduleId: rescheduleId
              }
            });
        } else {
          parent_scid = checkReschedule[0].scheduleId;
          let update_schedules = await db.schedule.update({
            schedule_status: 2,
            parent_scheduleId: parent_scid
          }, {
              where: {
                scheduleId: rescheduleId
              }
            });
          db.sequelize.close();
        }
      } else {
        throw new ApplicationError("Schedule ID Not available", 422);
      }
      const newReSchedule = {
        rescheduleId: checkReschedule[0].scheduleId,
        businessId: checkReschedule[0].businessId,
        batchId: checkReschedule[0].batchId,
        serviceId: checkReschedule[0].serviceId,
        is_repeat: checkReschedule[0].is_repeat,
        repeatation_type: checkReschedule[0].repeatation_type,
        repeat_number: checkReschedule[0].repeat_number,
        repeat_ends: checkReschedule[0].repeat_ends,
        teamId: checkReschedule[0].teamId,
        repeat_number: checkReschedule[0].repeat_number,
        centerId,
        scheduleDate,
        startTime,
        endTime,
        scheduleType: sc_type,
        parent_scheduleId: parent_scid,
        comments
      };

      if (checkReschedule[0]) {
        let rescheduleData = await db.schedule.create(newReSchedule);
        db.sequelize.close();
        return await filterFields(rescheduleData, PUBLIC_FIELDS, true);
      } else {
        db.sequelize.close();
        throw new ApplicationError("Error", 409);
      }
    } catch (error) {
      throw error;
    }
  };

  // Booking Management / Schedule Dashboard New 
  scheduleDashboard = async (
    information
  ) => {
    try {
      //create connections
      let dbSchedule = createInstance(["clientSchedule"], information);
      let dbCSM = createInstance(["clientScheduleMap"], information);
      let dbClients = createInstance(["client"], information);
      let dbService = createInstance(["clientService"], information);

      dbSchedule.schedule.belongsTo(dbService.services, {
        foreignKey: "serviceId",
        targetKey: "serviceId"
      });

      //necessary information
      let today = new Date(); //today
      let showUpto = 3; //7 days
      let getInfo = information.split(",");
      let businessId = getInfo[1];

      var setObj = {
        businessId: businessId,
        status: 1
      };
      let todaysBooking = await bookingCountInfo(information, setObj, today);

      //booking section
      let pendingAtd = 0;
      let mergedSchedules = [];
      let schedules = await dbSchedule.schedule.findAll({
        where: {
          status: 1,
          businessId: businessId
        },
        include: [{
          model: dbService.services,
          attributes: ['serviceName'],
        }],
        raw: true
      });
      let getAttendance = await attendanceInfoCalculate(information, { businessId: businessId, status: 1 }, today);
      pendingAtd = getAttendance.unmarked;
      for (let schedule of schedules) {

        //pending attendance
        let allCSM = await dbCSM.clientschedulemap.findAll({
          where: {
            status: 1,
            scheduleId: schedule.scheduleId
          },
          raw: true
        });
        // let countUnmarkedAtd = 0;
        // for (let client of allCSM) {
        //   for (let clientAtd of client.attendanceType) {
        //     let atdKey = new Date(clientAtd.scdate);
        //     if (atdKey < today) {
        //       if (clientAtd.atd == 4) { countUnmarkedAtd++; break; }
        //       continue;
        //     } else break;
        //   }
        // }
        // if (countUnmarkedAtd > 0) pendingAtd++;

        for (let schedule_time of schedule.scheduleTime) {

          //difference of days of schedules from today
          let date1 = new Date(schedule_time.scdate);
          let date2 = new Date(today);
          let timeDiff = Math.abs(date2.getTime() - date1.getTime());
          let diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

          //today's booking
          // let sdate = await hyphenDateFormat(schedule_time.scdate);
          // let tday = await hyphenDateFormat(today);

          // if (sdate == tday) {
          //   breakloop: for (let csm of allCSM) {
          //     for (let atd of csm.attendanceType) {
          //       if (atd.scdate == tday) {
          //         todaysBooking++;
          //         break breakloop;
          //       }
          //     }
          //   }
          // }

          if (diffDays <= showUpto) {
            let datetime = new Date(schedule_time.scdate + ' ' + schedule_time.scheduleStartTime + ' UTC');
            console.log("=================");
            console.log(datetime);

            let td = new Date();
            let d = td.toLocaleDateString();
            let t = td.toLocaleTimeString();
            let localDateTime = new Date(d + ' ' + t + ' UTC');
            console.log("=================");
            console.log("Local Date Time");
            console.log(localDateTime);

            var indianTimeZoneVal = new Date().toLocaleString({ timeZone: 'Asia/kolkata' });
            console.log("=================");
            console.log("indianTimeZoneVal");

            console.log(indianTimeZoneVal);

            var givendate = new Date();
            let dd = givendate.toString();
            let cc = new Date(dd + ' UTC');
            console.log("=================");
            console.log("Given date");
            console.log(dd);
            console.log(cc);

            let now = new Date();
            let date = await hyphenDateFormat(now);
            let time = now.toLocaleTimeString();
            let currentdatetime = new Date(date + ' ' + time + ' UTC');
            console.log("=================");
            console.log("Current Date Time");
            console.log(time);

            if ((new Date(datetime)) > (new Date(localDateTime))) {

              //clients under the schedule
              let mergedClients = [];
              for (let getClient of allCSM) {
                for (let clientAtd of getClient.attendanceType) {
                  let clientdatetime = new Date(clientAtd.scdate + ' ' + clientAtd.scheduleStartTime + ' UTC');
                  //if (clientAtd.scdate == schedule_time.scdate) {
                  if (+clientdatetime === +datetime) {
                    let clients = await dbClients.clients.findAll({
                      where: {
                        clientId: getClient.clientId,
                        status: 1
                      },
                      raw: true
                    });
                    if (clients.length > 0) {
                      let isphoto = (clients[0].photoUrl === null) ? KNACK_UPLOAD_URL.localUrl + 'noimage.jpg' : KNACK_UPLOAD_URL.localUrl + clients[0].photoUrl;

                      mergedClients = mergedClients.concat({ clientId: clients[0].clientId, clientName: clients[0].clientName, contactNumber: clients[0].contactNumber, emailId: clients[0].emailId, dateOfBirth: clients[0].dateOfBirth, clientJoiningDate: clients[0].clientJoiningDate, createDateTime: clients[0].createDateTime, updateDateTime: clients[0].updateDateTime, clientphoto: isphoto, status: clients[0].status });
                    }
                  }
                }
              }

              //attendance of the schedule of the booking
              let sdate = new Date(schedule_time.scdate);
              let obj = { scheduleId: schedule.scheduleId };
              let atd = await attendanceInfoOfClientAndDate(information, obj, sdate);

              //merging
              if (mergedClients.length > 0) {
                let keyValuePairArray = [];
                let message = schedule_time.rescheduleDate == schedule_time.scdate ? "" : 'Booking Rescheduled to ' + schedule_time.rescheduleDate;
                let comment = schedule_time.comment;
                schedule['scheduleDate'] = schedule_time.scdate;
                let keyValuePair = { rescheduled: schedule_time.reschedule, message: message, comment: comment };

                // Set Booking Name 
                var stime = tConv24(schedule.startTime);
                var etime = tConv24(schedule.endTime);
                let stime = stime + '-' + etime;
                let dayint = convartday(schedule.repeat_days);
                let day_time = ', ' + dayint + ', ' + stime;

                let bname;
                if (schedule.scheduleName) {
                  bname = schedule.scheduleName;
                } else {
                  bname = schedule['service.serviceName'] + day_time;
                }

                Array.prototype.push.apply(keyValuePairArray, [{
                  schedule: {
                    scheduleName: bname,
                    serviceName: schedule['service.serviceName'],
                    scheduleId: schedule.scheduleId,
                    parentScheduleId: schedule.parent_scheduleId,
                    scheduleDate: schedule_time.scdate,
                    startTime: schedule_time.scheduleStartTime,
                    endTime: schedule_time.scheduleEndTime
                  }
                }]);
                Array.prototype.push.apply(keyValuePairArray, [{ rescheduleStatus: keyValuePair }]);
                Array.prototype.push.apply(keyValuePairArray, [{ clients: mergedClients }]);
                Array.prototype.push.apply(keyValuePairArray, [{ attendance: atd }]);
                let keyValuePairObj = [];
                keyValuePairObj = Object.assign.apply(Object, keyValuePairArray);
                mergedSchedules = mergedSchedules.concat(keyValuePairObj);
              }
            }
          }
        }
      }

      mergedSchedules.sort(function (a, b) {
        let dateA = new Date(a.schedule.scheduleDate + " " + a.schedule.startTime);
        let dateB = new Date(b.schedule.scheduleDate + " " + b.schedule.startTime);
        console.log("=================");
        console.log(dateA - dateB);
        return dateA - dateB;
      });

      // Show booking graph
      let graph = await getBookingGraph(information);

      // formatting the data
      let finalInfo = [{
        graph: graph,
        booking: [{
          todaysBooking: todaysBooking,
          pendingAttendance: pendingAtd
        }]
      }, {
        schedules: [{
          allUpcomingSchedule: mergedSchedules.length,
          Upcoming: mergedSchedules
        }]
      }];

      //end connections
      dbService.sequelize.close();
      dbClients.sequelize.close();
      dbCSM.sequelize.close();
      dbSchedule.sequelize.close();
      return await finalInfo;
    } catch (error) {
      throw error;
    }
  }

  //Attendance History
  AttendanceHistory = async (
    information,
    filter,
    Search,
    filterPrivateFields = true
  ) => {
    try {
      var merged = [];
      var mergedSchedules = [];
      let db = createInstance(["clientSchedule"], information);
      let dbCSMap = createInstance(["clientScheduleMap"], information);
      let dbService = createInstance(["clientService"], information);

      let schedules = await db.schedule.findAll({
        where: {
          status: 1,
        },
        raw: true
      });

      for (let j = 0; j < schedules.length; j++) {
        if (Search[0] === '') {
          var services = await dbService.services.findAll({
            where: {
              serviceId: schedules[j].serviceId,
            },
            raw: true
          });
        } else {
          var services = await dbService.services.findAll({
            where: {
              serviceId: schedules[j].serviceId,
              serviceName: {
                like: ['%' + Search[0] + '%']
              }
            },
            raw: true
          });
          if (services.length === 0) {
            //return("no data found");
          }
        }

        if (services.length > 0) {

          let flag = 0;
          let CSMap = await dbCSMap.clientschedulemap.findAll({
            where: {
              scheduleId: schedules[j].scheduleId,
              status: 1
            },
            raw: true
          });

          if ((CSMap.length > 0) && (+(new Date(schedules[j].scheduleDate)) <= +(new Date()))) {
            let foundSchedule = 0;
            for (let i = 0; i < CSMap.length; i++) {
              let x = CSMap[i].attendanceType;
              for (let key in CSMap[i].attendanceType) {
                if (+(new Date(key)) <= +(new Date())) {
                  if (x[key] === '4') {
                    flag = 1;
                    foundSchedule = 1;
                  }
                } else {
                  break;
                }
              }
            }

          } else {
            flag = 2;
          }

          let bookingName;
          if (schedules[j].scheduleName === null) {
            bookingName = services[0].serviceName;
          } else {
            bookingName = schedules[j].scheduleName;
          }

          if (flag === 0) {
            var arr = [];
            Array.prototype.push.apply(arr, [{ BookingName: bookingName }]);
            Array.prototype.push.apply(arr, [{ ServiceName: services[0].serviceName }]);
            Array.prototype.push.apply(arr, [{ StartTime: schedules[j].startTime }]);
            Array.prototype.push.apply(arr, [{ EndTime: schedules[j].endTime }]);
            Array.prototype.push.apply(arr, [{ ScheduleId: schedules[j].scheduleId }]);
            merged = Object.assign.apply(Object, arr);
            mergedSchedules = mergedSchedules.concat(merged);
          }

        }
      }
      if (filter === '1') {
        mergedSchedules.reverse();
      }
      return mergedSchedules;
    } catch (error) {
      throw error;
    }
  };

  // New Attendance History
  newattendanceHistory_old_logic = async (
    information,
    booking_date,
    schedule_time,
    searchKey,
    filterPrivateFields = true
  ) => {
    try {

      let userId = information.split(',')[2];
      let centerId = information.split(',')[3];

      let permittedSchedules = createInstance(["teamScheduleMap"], information);
      let dbSchedule = createInstance(["clientSchedule"], information);
      permittedSchedules.teamschedulemap.belongsTo(dbSchedule.schedule, {
        foreignKey: "scheduleId",
        targetKey: "scheduleId"
      });

      let CheckUser = await loginSignUpHelperDB.viewLogin({ businessId: information.split(',')[1], userId: userId });
      let UserStatus = 0;
      if (CheckUser.length == 0) throw new ApplicationError("Invalid User", 401);
      if (CheckUser[0].userType == 2) {
        var checkSchedule = await permittedSchedules.teamschedulemap.findAll({
          attributes: ['scheduleId'],
          where: {
            teamId: userId,
            status: 1,
            businessId: information.split(',')[1]
          },
          include: [{ model: dbSchedule.schedule, attributes: ['centerId'], where: { centerId: centerId } }],
          raw: true,
        });


        if (checkSchedule.length == 0) {
          throw new ApplicationError("User is not permitted to give attendance", 401);
        }

        UserStatus = 2;

      } else if (CheckUser[0].userType == 1) {
        UserStatus = 1;
      } else {
        throw new ApplicationError("Error in UserId, Please check", 401);
      }
      let Obj = {};
      if (UserStatus == 1) {
        Obj = {
          status: 1,
          businessId: information.split(',')[1],
        };
      } else {
        let scheArr = [];
        for (let i = 0; i < checkSchedule.length; i++) {
          scheArr.push(checkSchedule[i].scheduleId);
        }

        Obj = {
          status: 1,
          businessId: information.split(',')[1],
          scheduleId: {
            [Sequelize.Op.in]: scheArr
          }
        };
      }


      var businessIdinformation = information.split(",")[1];
      let td = new Date().toISOString().split('T')[0];
      let today = new Date(td);

      let dbCSM = createInstance(["clientScheduleMap"], information);
      let dbServices = createInstance(["clientService"], information);

      dbSchedule.schedule.belongsTo(dbCSM.clientschedulemap, {
        foreignKey: "scheduleId",
        targetKey: "scheduleId"
      });
      dbSchedule.schedule.belongsTo(dbServices.services, {
        foreignKey: "serviceId",
        targetKey: "serviceId"
      });

      let pageNo = parseInt(searchKey[0]);
      let showLimit;
      if (pageNo > 0) {
        pageNo = parseInt(searchKey[0]);
        showLimit = PEGINATION_LIMIT; //show 2 records per page
      } else {
        pageNo = 1;
        showLimit = 1000; //show first 1000 records if no page number is given
      }

      let orderBy;
      if ((searchKey["booking_date"] == 2) && (searchKey["schedule_time"] == 2)) {
        orderBy = [
          ["scheduleDate", "ASC"],
          ["startTime"]
        ];
      } else if (searchKey["booking_date"] == 2 && searchKey["schedule_time"] == 1) {
        orderBy = [
          ["scheduleDate", "ASC"],
        ];
      } else if ((searchKey["booking_date"] == 2) && (searchKey["schedule_time"] == 0)) {
        orderBy = [
          ["scheduleDate", "ASC"],
        ];
      } else if ((searchKey["booking_date"] == 1) && (searchKey["schedule_time"] == 2)) {
        orderBy = [
          ["scheduleDate", "ASC"],
        ];
      } else {
        orderBy = [
          ["scheduleDate", "ASC"],
        ];
      }

      let schedules = await dbSchedule.schedule.findAll({
        where: Obj,
        // where: {
        //   status: 1,
        //   businessId: businessIdinformation,
        // },
        include: [{
          model: dbServices.services,
          attributes: ['serviceName'],
        }],
        order: orderBy,
        raw: true
      });

      //showing the necessary data
      let pendingSchedules = [];
      for (let schedule of schedules) { //loop through all the bookings
        let clients = await dbCSM.clientschedulemap.findAll({
          where: {
            scheduleId: schedule.scheduleId,
            status: 1
          },
          raw: true
        });
        let foundSchedule = 0,
          foundUnmarked = 0,
          beyondBoundary = 0;
        if (clients.length > 0) {
          for (let client of clients) {
            for (let keyClient = 0; keyClient < client.attendanceType.length; keyClient++) {
              if ((new Date(client.attendanceType[keyClient].scdate)) <= (new Date(today))) {
                foundSchedule = 1;
                if (client.attendanceType[keyClient].atd == 4) {
                  foundUnmarked = 1;
                  break;
                }
              } else if ((new Date(client.attendanceType[keyClient].scdate)) >= (new Date(today))) {
                beyondBoundary = 1;
                break;
              }
              if (foundUnmarked == 1) break;
            }
          }
          if (beyondBoundary == 1 && foundSchedule == 0) continue;
          else if (foundSchedule = 1 && foundUnmarked == 0) {
            let scheduleArr = [];
            Array.prototype.push.apply(scheduleArr, [{ scheduleId: schedule.scheduleId }]);
            schedule['bookingName'] = schedule.scheduleName === null ? schedule['service.serviceName'] : schedule.scheduleName;
            Array.prototype.push.apply(scheduleArr, [{ scheduleName: schedule.bookingName }]);
            Array.prototype.push.apply(scheduleArr, [{ serviceName: schedule['service.serviceName'] }]);
            Array.prototype.push.apply(scheduleArr, [{ scheduleStartTime: schedule.startTime }]);
            Array.prototype.push.apply(scheduleArr, [{ scheduleEndTime: schedule.endTime }]);
            let scheduleObj = [];
            scheduleObj = Object.assign.apply(Object, scheduleArr);
            pendingSchedules = pendingSchedules.concat(scheduleObj);
          }
        }
      }

      //pagination & search
      let data = await rsPaginate(pendingSchedules, pageNo, showLimit);
      let resultSet = [];
      if (searchKey[1].trim() == '') {
        resultSet = data;
      } else {
        for (let i = 0; i < data.length; i++) {
          let schedule_name = data[i].scheduleName.toLowerCase();
          let service_name = data[i].serviceName.toLowerCase();
          if (schedule_name.includes(searchKey[1]) ||
            service_name.includes(searchKey[1])) {
            resultSet = resultSet.concat(data[i]);
          }
        }
      }

      dbServices.sequelize.close();
      dbCSM.sequelize.close();
      dbSchedule.sequelize.close();
      return await resultSet;
    } catch (error) {
      throw error;
    }
  };

  newattendanceHistory = async (
    information,
    booking_date,
    schedule_time,
    searchKey,
    filterPrivateFields = true
  ) => {
    try {
      let userId = information.split(',')[2];
      let centerId = information.split(',')[3];

      let permittedSchedules = createInstance(["teamScheduleMap"], information);
      let dbSchedule = createInstance(["clientSchedule"], information);
      permittedSchedules.teamschedulemap.belongsTo(dbSchedule.schedule, {
        foreignKey: "scheduleId",
        targetKey: "scheduleId"
      });

      let CheckUser = await loginSignUpHelperDB.viewLogin({ businessId: information.split(',')[1], userId: userId });
      let UserStatus = 0;
      if (CheckUser.length == 0) throw new ApplicationError("Invalid User", 401);
      if (CheckUser[0].userType == 2) {
        var checkSchedule = await permittedSchedules.teamschedulemap.findAll({
          attributes: ['scheduleId'],
          where: {
            teamId: userId,
            status: 1,
            businessId: information.split(',')[1]
          },
          include: [{ model: dbSchedule.schedule, attributes: ['centerId'], where: { centerId: centerId } }],
          raw: true,
        });


        if (checkSchedule.length == 0) {
          throw new ApplicationError("User is not permitted to give attendance", 401);
        }

        UserStatus = 2;

      } else if (CheckUser[0].userType == 1) {
        UserStatus = 1;
      } else {
        throw new ApplicationError("Error in UserId, Please check", 401);
      }
      let Obj = {};
      if (UserStatus == 1) {
        Obj = {
          status: 1,
          businessId: information.split(',')[1],
        };
      } else {
        let scheArr = [];
        for (let i = 0; i < checkSchedule.length; i++) {
          scheArr.push(checkSchedule[i].scheduleId);
        }

        Obj = {
          status: 1,
          businessId: information.split(',')[1],
          scheduleId: {
            [Sequelize.Op.in]: scheArr
          }
        };
      }


      var businessIdinformation = information.split(",")[1];
      let td = new Date().toISOString().split('T')[0];
      // let today = new Date(td);
      let today = new Date();

      let dbCSM = createInstance(["clientScheduleMap"], information);
      let dbServices = createInstance(["clientService"], information);

      dbSchedule.schedule.belongsTo(dbCSM.clientschedulemap, {
        foreignKey: "scheduleId",
        targetKey: "scheduleId"
      });
      dbSchedule.schedule.belongsTo(dbServices.services, {
        foreignKey: "serviceId",
        targetKey: "serviceId"
      });

      let pageNo = parseInt(searchKey[0]);
      let showLimit;
      if (pageNo > 0) {
        pageNo = parseInt(searchKey[0]);
        showLimit = PEGINATION_LIMIT; //show 2 records per page
      } else {
        pageNo = 1;
        showLimit = 1000; //show first 1000 records if no page number is given
      }

      let orderBy;
      if ((searchKey["booking_date"] == 2) && (searchKey["schedule_time"] == 2)) {
        orderBy = [
          ["scheduleDate", "ASC"],
          ["startTime"]
        ];
      } else if (searchKey["booking_date"] == 2 && searchKey["schedule_time"] == 1) {
        orderBy = [
          ["scheduleDate", "ASC"],
        ];
      } else if ((searchKey["booking_date"] == 2) && (searchKey["schedule_time"] == 0)) {
        orderBy = [
          ["scheduleDate", "ASC"],
        ];
      } else if ((searchKey["booking_date"] == 1) && (searchKey["schedule_time"] == 2)) {
        orderBy = [
          ["scheduleDate", "ASC"],
        ];
      } else {
        orderBy = [
          ["scheduleDate", "ASC"],
        ];
      }

      let schedules = await dbSchedule.schedule.findAll({
        where: Obj,
        // where: {
        //   status: 1,
        //   businessId: businessIdinformation,
        // },
        include: [{
          model: dbServices.services,
          attributes: ['serviceName'],
        }],
        order: orderBy,
        raw: true
      });

      //showing the necessary data
      let pendingSchedules = [];
      nextSchedule: for (let schedule of schedules) { //loop through all the bookings
        for (let stime of schedule.scheduleTime) {
          let sdatetime = new Date(stime.scdate + ' ' + stime.scheduleStartTime);
          if (+sdatetime <= +(new Date(today))) {
            let clients = await dbCSM.clientschedulemap.findAll({
              where: {
                scheduleId: schedule.scheduleId,
                status: 1
              },
              raw: true
            });
            if (clients.length > 0) {
              let countClientAtd = 0;
              nextClient: for (let client of clients) {
                for (let ctime of client.attendanceType) {
                  let cdatetime = new Date(ctime.scdate + ' ' + ctime.scheduleStartTime);
                  if (+sdatetime === +cdatetime) {
                    if (
                      ctime.atd == 1 ||
                      ctime.atd == 2 ||
                      ctime.atd == 3 ||
                      ctime.atd == 6 ||
                      ctime.atd == 7
                    ) {
                      countClientAtd++;
                      continue nextClient;
                    }
                  }
                }
              }
              if (countClientAtd == clients.length) {
                let scheduleArr = [];
                Array.prototype.push.apply(scheduleArr, [{ scheduleId: schedule.scheduleId }]);
                schedule['bookingName'] = schedule.scheduleName === null ? schedule['service.serviceName'] : schedule.scheduleName;
                Array.prototype.push.apply(scheduleArr, [{ scheduleName: schedule.bookingName }]);
                Array.prototype.push.apply(scheduleArr, [{ serviceName: schedule['service.serviceName'] }]);
                Array.prototype.push.apply(scheduleArr, [{ scheduleDate: schedule.scheduleDate }]);
                Array.prototype.push.apply(scheduleArr, [{ scheduleStartTime: schedule.startTime }]);
                Array.prototype.push.apply(scheduleArr, [{ scheduleEndTime: schedule.endTime }]);
                let scheduleObj = [];
                scheduleObj = Object.assign.apply(Object, scheduleArr);
                pendingSchedules = pendingSchedules.concat(scheduleObj);
                continue nextSchedule;
              }
            }
          }
        }
      }

      //pagination & search
      let data = await rsPaginate(pendingSchedules, pageNo, showLimit);
      let resultSet = [];
      if (searchKey[1].trim() == '') {
        resultSet = data;
      } else {
        for (let i = 0; i < data.length; i++) {
          let schedule_name = data[i].scheduleName.toLowerCase();
          let service_name = data[i].serviceName.toLowerCase();
          if (schedule_name.includes(searchKey[1]) ||
            service_name.includes(searchKey[1])) {
            resultSet = resultSet.concat(data[i]);
          }
        }
      }

      dbServices.sequelize.close();
      dbCSM.sequelize.close();
      dbSchedule.sequelize.close();
      return await resultSet;
    } catch (error) {
      throw error;
    }
  };

  getAttendanceHistory = async (
    information,
    params,
    filterPrivateFields = true
  ) => {
    try {

      var merged = [];
      var mergedSchedules = [];

      var businessIdinformation = information.split(",")[1];
      let dbSchedule = createInstance(["clientSchedule"], information);
      let dbCSMap = createInstance(["clientScheduleMap"], information);
      let dbService = createInstance(["clientService"], information);

      dbSchedule.schedule.belongsTo(dbCSMap.clientschedulemap, {
        foreignKey: "scheduleId",
        targetKey: "scheduleId"
      });
      dbSchedule.schedule.belongsTo(dbService.services, {
        foreignKey: "serviceId",
        targetKey: "serviceId"
      });


      let filter = (params[0]) ? params[0] : '';
      var pageNo = (params['page']) ? params['page'] : 1;
      var limitPerPage = PEGINATION_LIMIT;
      var offsetLimit = (pageNo - 1) * limitPerPage;
      let orderBy;
      let itemConditions = {
        "scheduleDate": {
          [dbSchedule.sequelize.Op.gte]: +(new Date())
        }
      };


      if ((params["booking_date"] == 2) && (params["schedule_time"] == 2)) {
        orderBy = [
          ["scheduleDate", "DESC"],
          ["startTime"]
        ];
      } else if (params["booking_date"] == 2 && params["schedule_time"] == 1) {
        orderBy = [
          ["scheduleDate", "DESC"],
        ];
      } else if ((params["booking_date"] == 2) && (params["schedule_time"] == 0)) {
        orderBy = [
          ["scheduleDate", "DESC"],
        ];
      } else if ((params["booking_date"] == 1) && (params["schedule_time"] == 2)) {
        orderBy = [
          ["scheduleDate", "DESC"],
        ];
      } else {
        orderBy = [
          ["scheduleDate", "ASC"],
        ];
      }


      let itemConditionsclientpayments = {
        status: 1,
        businessId: businessIdinformation,
        scheduleDate: {
          lte: new Date()
        },

        // [dbSchedule.sequelize.Op.and]: [{
        //   [dbSchedule.sequelize.Op.gt]:
        //     dbSchedule.sequelize.where(dbSchedule.sequelize.literal('attendancetype->"$[*].code" > 123'))
        // }

        //   // dbSchedule.sequelize.where (
        //   //   dbSchedule.sequelize.literal('attendancetype->"$[*].code"'), "123"
        //   // )

        // ],



        [dbSchedule.sequelize.Op.or]: [{
          "$scheduleName$": {
            like: "%" + filter + "%"
          }
        },
        {
          "$service.serviceName$": {
            like: "%" + filter + "%"
          }
        }
        ]
      };

      let clientSchedules = await dbSchedule.schedule.findAll({
        attributes: {

          exclude: ['scheduleTime']
        },
        include: [{
          model: dbCSMap.clientschedulemap,
          attributes: ["clientScheduleMapId", "businessId", "scheduleId", "subscriptionId", "clientId", "attendanceType", "status"],
          //group: ["scheduleId"],

          //attributes: [[ dbSchedule.sequelize.fn('JSON_VALUE', dbSchedule.sequelize.col('attendancetype'), '$.code'), '123']],
          // attributes : {
          //   include: [
          //     "clientScheduleMapId",
          //     "businessId",
          //     "scheduleId",
          //     "subscriptionId",
          //     "clientId",
          //     "attendanceType",
          //     "status",
          //     [dbCSMap.clientschedulemap.sequelize.literal('(SELECT json_extract(attendancetype,"$[*]") FROM clientschedulemap WHERE  json_extract(attendancetype, "$[*].code") > "123")'), 'attendancetype']
          //   ],
          // },           

          //attributes : {exclude:['scheduleTime']},
        }, { model: dbService.services }],




        // include : [{
        //     model: dbService.services,

        //   }],

        order: orderBy,
        where: itemConditionsclientpayments,
        subQuery: false,
        offset: offsetLimit,
        limit: limitPerPage,
        raw: true

      });

      let flag = 0;
      //return clientSchedules;
      if (clientSchedules.length > 0) {
        var sd = clientSchedules[0]['clientschedulemap.attendanceType'];

        for (let j = 0; j < clientSchedules.length; j++) {

          if ((+(new Date(clientSchedules[j].scheduleDate)) <= +(new Date()))) {
            let foundSchedule = 0;
            for (let i = 0; i < clientSchedules[j]['clientschedulemap.attendanceType'].length; i++) {
              let x = clientSchedules[j]['clientschedulemap.attendanceType'][i].scdate;
              let atd_val = clientSchedules[j]['clientschedulemap.attendanceType'][i].atd;
              if (+(new Date(x)) <= +(new Date())) {
                if (atd_val === 4) {
                  flag = 1;
                }
              }

              // for (let key in clientSchedules[j]['clientschedulemap.attendanceType']) {
              //   if (+(new Date(key)) <= +(new Date())) {
              //     if (x[key] === '4') {
              //       flag = 1;
              //       foundSchedule = 1;
              //     }
              //   } else {
              //     break;
              //   }
              // }
            }
          }


          // if (clientSchedules[j].scheduleName === null) {
          //   bookingName = services[0].serviceName;
          // }
          // else {
          //   bookingName = clientSchedules[j].scheduleName;
          // }
          let bookingName;

          if (flag === 0) {
            var arr = [];
            //Array.prototype.push.apply(arr, [{ BookingName: bookingName }]);
            //Array.prototype.push.apply(arr, [{ ServiceName: services[0].serviceName }]);
            Array.prototype.push.apply(arr, [{ ScheduleId: clientSchedules[j].scheduleId }]);
            Array.prototype.push.apply(arr, [{ ClientId: clientSchedules[j]['clientschedulemap.clientId'] }]);
            Array.prototype.push.apply(arr, [{ StartTime: clientSchedules[j].startTime }]);
            Array.prototype.push.apply(arr, [{ EndTime: clientSchedules[j].endTime }]);

            merged = Object.assign.apply(Object, arr);
            mergedSchedules = mergedSchedules.concat(merged);
          }
        }
      } else {
        flag = 2;
      }



      let final = { count: clientSchedules.length, raw: mergedSchedules };
      return final;

    } catch (error) {
      throw error;
    }
  }

  // Booking Attendance History New
  BookingAttendanceHistory = async (
    information,
    scheduleId,
    booking_date,
    schedule_time,
    searchKey,
    filterPrivateFields = true
  ) => {
    try {
      let getInfo = information.split(",");
      let businessId = getInfo[1];
      let today = new Date();
      let dbSchedule = createInstance(["clientSchedule"], information);
      let dbCSM = createInstance(["clientScheduleMap"], information);
      let dbServices = createInstance(["clientService"], information);
      dbCSM.clientschedulemap.belongsTo(dbSchedule.schedule, {
        foreignKey: "scheduleId",
        targetKey: "scheduleId"
      });
      dbSchedule.schedule.belongsTo(dbServices.services, {
        foreignKey: "serviceId",
        targetKey: "serviceId"
      });

      Array.prototype.groupBy = function (prop) {
        return this.reduce(function (groups, item) {
          const val = item[prop]
          groups[val] = groups[val] || []
          groups[val].push(item)
          return groups
        }, {})
      }

      let orderBy;
      if ((searchKey["booking_date"] == 2) && (searchKey["schedule_time"] == 2)) {
        orderBy = [
          ["scheduleDate", "ASC"],
          ["startTime"]
        ];
      } else if (searchKey["booking_date"] == 2 && searchKey["schedule_time"] == 1) {
        orderBy = [
          ["scheduleDate", "ASC"],
        ];
      } else if ((searchKey["booking_date"] == 2) && (searchKey["schedule_time"] == 0)) {
        orderBy = [
          ["scheduleDate", "ASC"],
        ];
      } else if ((searchKey["booking_date"] == 1) && (searchKey["schedule_time"] == 2)) {
        orderBy = [
          ["scheduleDate", "ASC"],
        ];
      } else {
        orderBy = [
          ["scheduleDate", "ASC"],
        ];
      }

      //fetching data
      let clients = await dbCSM.clientschedulemap.findAll({
        where: {
          scheduleId: scheduleId,
          status: 1
        },
        include: [{
          model: dbSchedule.schedule,
          include: [{
            model: dbServices.services,
            attributes: ['serviceName'],
          },]
        }],
        raw: true
      });

      //showing the necessary data      
      let pastSchedules = [],
        allSchedules = [];
      for (let client of clients) {
        for (let keyClient = 0; keyClient < client.attendanceType.length; keyClient++) {
          if (+(new Date(client.attendanceType[keyClient].scdate)) < +(new Date(today))) {
            let arrTemp = [];
            Array.prototype.push.apply(arrTemp, [client.attendanceType[keyClient]]);
            Array.prototype.push.apply(arrTemp, [{ clientId: client.clientId }]);
            Array.prototype.push.apply(arrTemp, [{ scheduleId: client['schedule.scheduleId'] }]);
            let clientAtd = [];
            clientAtd = Object.assign.apply(Object, arrTemp);
            allSchedules.push(clientAtd);
          }
        }
      }
      const schedulesByDate = allSchedules.groupBy('scdate');
      for (let key of Object.keys(schedulesByDate)) {
        let cid = schedulesByDate[key].map((client) => {
          return client.clientId;
        });
        let sid = schedulesByDate[key].map((client) => {
          return client.scheduleId;
        });
        let scheduleFrom = "",
          reschedule = "",
          rescheduleDate = "";
        for (let stime of clients[0]['schedule.scheduleTime']) {
          if (key == stime.scdate) {
            scheduleFrom = stime.scheduleFrom;
            reschedule = stime.reschedule;
            rescheduleDate = stime.rescheduleDate;
          }
        }
        let schedule_date = new Date(key);
        let setObj = { scheduleId: sid, clientId: cid, status: 1 };
        let attendance = await attendanceInfoOfClientAndDate(information, setObj, schedule_date);
        pastSchedules.push({
          scheduleDate: key,
          atdBy: "",
          atdOn: "",
          scheduleFrom: scheduleFrom,
          reschedule: reschedule,
          rescheduleTo: reschedule == 2 ? rescheduleDate : "",
          attendance: attendance
        });
      }

      //pagination
      let pageNo = parseInt(searchKey[0]);
      let data = await rsPaginate(pastSchedules, pageNo, PEGINATION_LIMIT);

      //search
      let resultSet = [];
      if (searchKey[1].trim() == '') {
        resultSet = data;
      } else {
        for (let i = 0; i < data.length; i++) {
          if (data[i].scheduleDate.includes(searchKey[1])) {
            resultSet = resultSet.concat(data[i]);
          }
        }
      }

      //formatting the resultSet
      clients[0]['schedule.bookingName'] = clients[0]['schedule.scheduleName'] === null ? clients[0]['schedule.service.serviceName'] : clients[0]['schedule.scheduleName'];
      let info = {
        header: {
          scheduleId: clients[0]['schedule.scheduleId'],
          scheduleName: clients[0]['schedule.bookingName'],
          startTime: clients[0]['schedule.startTime'],
          endTime: clients[0]['schedule.endTime']
        },
        attendanceHistory: resultSet
      };

      //disconnect
      dbServices.sequelize.close();
      dbCSM.sequelize.close();
      dbSchedule.sequelize.close();
      return await info;
    } catch (error) {
      throw error;
    }
  };

  BookingAttendanceHistory = async (
    information,
    scheduleId,
    booking_date,
    schedule_time,
    searchKey,
    filterPrivateFields = true
  ) => {
    try {
      let getInfo = information.split(",");
      let businessId = getInfo[1];
      let today = new Date();
      let dbSchedule = createInstance(["clientSchedule"], information);
      let dbCSM = createInstance(["clientScheduleMap"], information);
      let dbServices = createInstance(["clientService"], information);
      dbCSM.clientschedulemap.belongsTo(dbSchedule.schedule, {
        foreignKey: "scheduleId",
        targetKey: "scheduleId"
      });
      dbSchedule.schedule.belongsTo(dbServices.services, {
        foreignKey: "serviceId",
        targetKey: "serviceId"
      });

      Array.prototype.groupBy = function (prop) {
        return this.reduce(function (groups, item) {
          const val = item[prop]
          groups[val] = groups[val] || []
          groups[val].push(item)
          return groups
        }, {})
      }

      let orderBy;
      if ((searchKey["booking_date"] == 2) && (searchKey["schedule_time"] == 2)) {
        orderBy = [
          ["scheduleDate", "ASC"],
          ["startTime"]
        ];
      } else if (searchKey["booking_date"] == 2 && searchKey["schedule_time"] == 1) {
        orderBy = [
          ["scheduleDate", "ASC"],
        ];
      } else if ((searchKey["booking_date"] == 2) && (searchKey["schedule_time"] == 0)) {
        orderBy = [
          ["scheduleDate", "ASC"],
        ];
      } else if ((searchKey["booking_date"] == 1) && (searchKey["schedule_time"] == 2)) {
        orderBy = [
          ["scheduleDate", "ASC"],
        ];
      } else {
        orderBy = [
          ["scheduleDate", "ASC"],
        ];
      }

      //fetching data
      let clients = await dbCSM.clientschedulemap.findAll({
        where: {
          scheduleId: scheduleId,
          status: 1
        },
        include: [{
          model: dbSchedule.schedule,
          include: [{
            model: dbServices.services,
            attributes: ['serviceName'],
          },]
        }],
        raw: true
      });

      //showing the necessary data      
      let pastSchedules = [],
        allSchedules = [];
      for (let client of clients) {
        for (let keyClient = 0; keyClient < client.attendanceType.length; keyClient++) {
          if (+(new Date(client.attendanceType[keyClient].scdate)) < +(new Date(today))) {
            let arrTemp = [];
            Array.prototype.push.apply(arrTemp, [client.attendanceType[keyClient]]);
            Array.prototype.push.apply(arrTemp, [{ clientId: client.clientId }]);
            Array.prototype.push.apply(arrTemp, [{ scheduleId: client['schedule.scheduleId'] }]);
            let clientAtd = [];
            clientAtd = Object.assign.apply(Object, arrTemp);
            allSchedules.push(clientAtd);
          }
        }
      }
      const schedulesByDate = allSchedules.groupBy('scdate');
      for (let key of Object.keys(schedulesByDate)) {
        let cid = schedulesByDate[key].map((client) => {
          return client.clientId;
        });
        let sid = schedulesByDate[key].map((client) => {
          return client.scheduleId;
        });
        let scheduleFrom = "",
          reschedule = "",
          rescheduleDate = "";
        for (let stime of clients[0]['schedule.scheduleTime']) {
          if (key == stime.scdate) {
            scheduleFrom = stime.scheduleFrom;
            reschedule = stime.reschedule;
            rescheduleDate = stime.rescheduleDate;
          }
        }
        let schedule_date = new Date(key);
        let setObj = { scheduleId: sid, clientId: cid, status: 1 };
        let attendance = await attendanceInfoOfClientAndDate(information, setObj, schedule_date);
        pastSchedules.push({
          scheduleDate: key,
          atdBy: "",
          atdOn: "",
          scheduleFrom: scheduleFrom,
          reschedule: reschedule,
          rescheduleTo: reschedule == 2 ? rescheduleDate : "",
          attendance: attendance
        });
      }

      //pagination
      let pageNo = parseInt(searchKey[0]);
      // let showLimit;
      // if (pageNo > 0) {
      //   pageNo = parseInt(searchKey[0]);
      //   showLimit = 2; //show 2 records per page
      // } else {
      //   pageNo = 1;
      //   showLimit = 1000; //show first 1000 records if no page number is given
      // }

      let data = await rsPaginate(pastSchedules, pageNo, PEGINATION_LIMIT);

      //search
      let resultSet = [];
      if (searchKey[1].trim() == '') {
        resultSet = data;
      } else {
        for (let i = 0; i < data.length; i++) {
          if (data[i].scheduleDate.includes(searchKey[1])) {
            resultSet = resultSet.concat(data[i]);
          }
        }
      }

      //formatting the resultSet
      clients[0]['schedule.bookingName'] = clients[0]['schedule.scheduleName'] === null ? clients[0]['schedule.service.serviceName'] : clients[0]['schedule.scheduleName'];
      let info = {
        header: {
          scheduleId: clients[0]['schedule.scheduleId'],
          scheduleName: clients[0]['schedule.bookingName'],
          startTime: clients[0]['schedule.startTime'],
          endTime: clients[0]['schedule.endTime']
        },
        attendanceHistory: resultSet
      };

      //disconnect
      dbServices.sequelize.close();
      dbCSM.sequelize.close();
      dbSchedule.sequelize.close();
      return await info;
    } catch (error) {
      throw error;
    }
  };

  recordAttendanceBooking = async (
    information,
    businessId,
    filterPrivateFields = true
  ) => {
    try {
      let today = new Date();
      let dbSchedule = createInstance(["clientSchedule"], information);
      let dbCSM = createInstance(["clientScheduleMap"], information);
      let dbClients = createInstance(["client"], information);
      let dbServices = createInstance(["clientService"], information);

      let schedules = await dbSchedule.schedule.findAll({
        where: {
          status: 1,
          businessId: businessId
        },
        raw: true
      });

      let pendingSchedules = [];
      for (let schedule of schedules) { //loop through all the bookings
        for (let key of Object.keys(schedule.scheduleTime)) { //loop through the schedules
          if (+(new Date(key)) < today) { //checking if the date is past
            let clients = await dbCSM.clientschedulemap.findAll({
              where: {
                scheduleId: schedule.scheduleId,
                status: 1
              },
              raw: true
            });

            let services = await dbServices.services.findAll({
              attributes: ['serviceName'],
              where: {
                serviceId: schedule.serviceId
              }
            });

            schedule['ServiceName'] = services[0].serviceName;

            // Set booking name
            if (schedule.scheduleName === null) {
              schedule['bookingName'] = schedule.ServiceName;
            } else {
              schedule['bookingName'] = schedule.scheduleName;
            }

            //clients
            let present = 0,
              absent = 0,
              excused = 0,
              unmarked = 0;
            let foundSchedule = 0;
            for (let client of clients) { //loop through all the clients
              for (let atd of Object.keys(client.attendanceType)) { //loop through attendances
                if (+(new Date(atd)) == +(new Date(key)) && client.attendanceType[atd] == 4) { //checking if attendanceDate and scheduleDate is same.

                  let schedule_date = new Date(key);
                  let setObj = { scheduleId: schedule.scheduleId, status: 1 };


                  let attendance = await attendanceInfoOfClientAndDate(information, setObj, schedule_date)
                  let scheduleArr = [];
                  Array.prototype.push.apply(scheduleArr, [{ scheduleId: schedule.scheduleId }]);
                  Array.prototype.push.apply(scheduleArr, [{ scheduleName: schedule.bookingName }]);
                  Array.prototype.push.apply(scheduleArr, [{ serviceName: schedule.ServiceName }]);
                  Array.prototype.push.apply(scheduleArr, [{ scheduleDate: key }]);
                  Array.prototype.push.apply(scheduleArr, [{ scheduleStartTime: schedule.scheduleTime[key].scheduleStartTime }]);
                  Array.prototype.push.apply(scheduleArr, [{ scheduleEndTime: schedule.scheduleTime[key].scheduleEndTime }]);
                  Array.prototype.push.apply(scheduleArr, [{ attendance: attendance }]);
                  let scheduleObj = [];
                  scheduleObj = Object.assign.apply(Object, scheduleArr);
                  pendingSchedules = pendingSchedules.concat(scheduleObj);


                  foundSchedule = 1;
                  break;
                }
              }
              if (foundSchedule == 1) break;
            }
          }
        }

      }

      dbServices.sequelize.close();
      dbClients.sequelize.close();
      dbCSM.sequelize.close();
      dbSchedule.sequelize.close();

      return await pendingSchedules;
    } catch (error) {
      throw error;
    }
  };

  // Record Attendance New Code
  newrecordAttendanceBooking__ = async ( //working with the new json
    information,
    businessId,
    booking_date,
    schedule_time,
    searchKey,
    filterPrivateFields = true
  ) => {
    try {
      let today = new Date();
      let dbSchedule = createInstance(["clientSchedule"], information);
      let dbCSM = createInstance(["clientScheduleMap"], information);
      let dbServices = createInstance(["clientService"], information);

      dbSchedule.schedule.belongsTo(dbCSM.clientschedulemap, {
        foreignKey: "scheduleId",
        targetKey: "scheduleId"
      });
      dbSchedule.schedule.belongsTo(dbServices.services, {
        foreignKey: "serviceId",
        targetKey: "serviceId"
      });

      let pageNo = parseInt(searchKey[0]);
      let orderBy;
      let schedules = await dbSchedule.schedule.findAll({
        where: {
          status: 1,
          businessId: businessId,
        },
        include: [{
          model: dbServices.services,
          attributes: ['serviceName'],
        }],
        // order: orderBy,
        raw: true
      });

      //showing the necessary data
      let pendingSchedules = [];
      for (let schedule of schedules) { //loop through all the bookings
        let clients = await dbCSM.clientschedulemap.findAll({
          where: {
            scheduleId: schedule.scheduleId,
            status: 1
          },
          raw: true
        });
        let present = 0,
          absent = 0,
          excused = 0,
          unmarked = 0;
        let sid = [],
          sdate = [],
          stime = [],
          etime = [];
        for (let client of clients) {
          for (let keyClient = 0; keyClient < client.attendanceType.length; keyClient++) {
            if (+(new Date(client.attendanceType[keyClient].scdate)) < +(new Date(today)) && client.attendanceType[keyClient].atd == 4) {

              //validation
              if (sid.includes(schedule.scheduleId) && sdate.includes(client.attendanceType[keyClient].scdate) &&
                stime.includes(client.attendanceType[keyClient].scheduleStartTime) && etime.includes(client.attendanceType[keyClient].scheduleEndTime)
              ) continue;
              else {
                let setObj = { scheduleId: schedule.scheduleId, status: 1 };
                let schedule_date = new Date(client.attendanceType[keyClient].scdate);
                let attendance = await attendanceInfoOfClientAndDate(information, setObj, schedule_date)

                let scheduleArr = [];
                Array.prototype.push.apply(scheduleArr, [{ scheduleId: schedule.scheduleId }]);
                schedule['bookingName'] = schedule.scheduleName === null ? schedule['service.serviceName'] : schedule.scheduleName;
                Array.prototype.push.apply(scheduleArr, [{ scheduleName: schedule.bookingName }]);
                Array.prototype.push.apply(scheduleArr, [{ serviceName: schedule['service.serviceName'] }]);
                Array.prototype.push.apply(scheduleArr, [{ scheduleDate: client.attendanceType[keyClient].scdate }]);
                Array.prototype.push.apply(scheduleArr, [{ scheduleStartTime: client.attendanceType[keyClient].scheduleStartTime }]);
                Array.prototype.push.apply(scheduleArr, [{ scheduleEndTime: client.attendanceType[keyClient].scheduleEndTime }]);
                Array.prototype.push.apply(scheduleArr, [{ attendance: attendance }]);
                let scheduleObj = [];
                scheduleObj = Object.assign.apply(Object, scheduleArr);
                pendingSchedules = pendingSchedules.concat(scheduleObj);
              }
              if (!sid.includes(schedule.scheduleId)) sid.push(schedule.scheduleId);
              if (!sdate.includes(client.attendanceType[keyClient].scdate)) sdate.push(client.attendanceType[keyClient].scdate);
              if (!stime.includes(client.attendanceType[keyClient].scheduleStartTime)) stime.push(client.attendanceType[keyClient].scheduleStartTime);
              if (!etime.includes(client.attendanceType[keyClient].scheduleEndTime)) etime.push(client.attendanceType[keyClient].scheduleEndTime);
            }
          }
        }
      }

      let p_schedule = pendingSchedules.filter((schedule, index, self) =>
        index === self.findIndex((t) => (
          t.scheduleId == schedule.scheduleId &&
          t.scheduleDate == schedule.scheduleDate &&
          t.scheduleStartTime == schedule.scheduleStartTime &&
          t.scheduleEndTime == schedule.scheduleEndTime
        ))
      )

      if ((searchKey["booking_date"] == 1) && (searchKey["schedule_time"] == 1)) {
        p_schedule.sort(function (a, b) {
          let b_mill = new Date(b.scheduleDate).getTime() + Number(b.scheduleStartTime.split(':')[0]) * 60 + Number(b.scheduleStartTime.split(':')[1]) * 1000;
          let a_mill = new Date(a.scheduleDate).getTime() + Number(a.scheduleStartTime.split(':')[0]) * 60 + Number(a.scheduleStartTime.split(':')[1]) * 1000;
          return a_mill - b_mill;
        });
      } else {
        p_schedule.sort(function (a, b) {
          let b_mill = new Date(b.scheduleDate).getTime() + Number(b.scheduleStartTime.split(':')[0]) * 60 + Number(b.scheduleStartTime.split(':')[1]) * 1000;
          let a_mill = new Date(a.scheduleDate).getTime() + Number(a.scheduleStartTime.split(':')[0]) * 60 + Number(a.scheduleStartTime.split(':')[1]) * 1000;
          return b_mill - a_mill;
        });
      }

      //pagination & search
      let data = await rsPaginate(p_schedule, pageNo, PEGINATION_LIMIT);
      let resultSet = [];
      if (searchKey[1].trim() == '') {
        resultSet = data;
      } else {
        for (let i = 0; i < data.length; i++) {
          let schedule_name = data[i].scheduleName.toLowerCase();
          let service_name = data[i].serviceName.toLowerCase();
          if (schedule_name.includes(searchKey[1]) ||
            service_name.includes(searchKey[1])) {
            resultSet = resultSet.concat(data[i]);
          }
        }
      }

      dbServices.sequelize.close();
      dbCSM.sequelize.close();
      dbSchedule.sequelize.close();
      return await resultSet;
    } catch (error) {
      throw error;
    }
  };

  newrecordAttendanceBooking = async ( //working with the new json
    information,
    businessId,
    booking_date,
    schedule_time,
    searchKey,
    filterPrivateFields = true
  ) => {
    try {

      let userId = information.split(',')[2];
      let centerId = information.split(',')[3];
      let permittedSchedules = createInstance(["teamScheduleMap"], information);

      let dbSchedule = createInstance(["clientSchedule"], information);
      permittedSchedules.teamschedulemap.belongsTo(dbSchedule.schedule, {
        foreignKey: "scheduleId",
        targetKey: "scheduleId"
      });

      let CheckUser = await loginSignUpHelperDB.viewLogin({ businessId: businessId, userId: userId });
      let UserStatus = 0;
      if (CheckUser.length == 0) throw new ApplicationError("Invalid User", 401);

      if (CheckUser[0].userType == 2) {
        var checkSchedule = await permittedSchedules.teamschedulemap.findAll({
          attributes: ['scheduleId'],
          where: {
            teamId: userId,
            status: 1,
            businessId: information.split(',')[1]
          },
          include: [{ model: dbSchedule.schedule, attributes: ['centerId'], where: { centerId: centerId } }],
          raw: true,
        });

        if (checkSchedule.length == 0) {
          throw new ApplicationError("User is not permitted to give attendance", 401);
        }

        UserStatus = 2;
      } else if (CheckUser[0].userType == 1) {
        UserStatus = 1;
      } else {
        throw new ApplicationError("Error in UserId, Please check", 401);
      }

      let Obj = {};
      if (UserStatus == 1) {
        Obj = {
          status: 1,
          businessId: businessId,
        };
      } else {

        let scheArr = [];
        for (let i = 0; i < checkSchedule.length; i++) {
          scheArr.push(checkSchedule[i].scheduleId);
        }

        Obj = {
          status: 1,
          businessId: businessId,
          scheduleId: {
            [Sequelize.Op.in]: scheArr
          }
        };
      }

      //the actual code ......
      let today = new Date();
      //let dbSchedule = createInstance(["clientSchedule"], information);
      let dbCSM = createInstance(["clientScheduleMap"], information);
      let dbServices = createInstance(["clientService"], information);

      dbSchedule.schedule.belongsTo(dbCSM.clientschedulemap, {
        foreignKey: "scheduleId",
        targetKey: "scheduleId"
      });
      dbSchedule.schedule.belongsTo(dbServices.services, {
        foreignKey: "serviceId",
        targetKey: "serviceId"
      });

      let pageNo = parseInt(searchKey[0]);
      let orderBy;

      let schedules = await dbSchedule.schedule.findAll({
        where: Obj,
        // {
        //   status: 1,
        //   businessId: businessId,
        // },
        include: [{
          model: dbServices.services,
          attributes: ['serviceName'],
        }],
        //order: orderBy,
        raw: true
      });

      //showing the necessary data
      let pendingSchedules = [];
      for (let schedule of schedules) { //loop through all the bookings
        let clients = await dbCSM.clientschedulemap.findAll({
          where: {
            scheduleId: schedule.scheduleId,
            status: 1
          },
          raw: true
        });

        let present = 0,
          absent = 0,
          excused = 0,
          unmarked = 0;
        let sid = [],
          sdate = [],
          stime = [],
          etime = [];
        for (let client of clients) {
          for (let keyClient = 0; keyClient < client.attendanceType.length; keyClient++) {
            if (+(new Date(client.attendanceType[keyClient].scdate)) < +(new Date(today)) && client.attendanceType[keyClient].atd == 4) {

              //validation
              if (sid.includes(schedule.scheduleId) && sdate.includes(client.attendanceType[keyClient].scdate) &&
                stime.includes(client.attendanceType[keyClient].scheduleStartTime) && etime.includes(client.attendanceType[keyClient].scheduleEndTime)
              ) continue;
              else {
                let setObj = { scheduleId: schedule.scheduleId, status: 1 };
                let schedule_date = new Date(client.attendanceType[keyClient].scdate);
                let attendance = await attendanceInfoOfClientAndDate(information, setObj, schedule_date)

                let scheduleArr = [];
                Array.prototype.push.apply(scheduleArr, [{ scheduleId: schedule.scheduleId }]);
                schedule['bookingName'] = schedule.scheduleName === null ? schedule['service.serviceName'] : schedule.scheduleName;
                Array.prototype.push.apply(scheduleArr, [{ scheduleName: schedule.bookingName }]);
                Array.prototype.push.apply(scheduleArr, [{ serviceName: schedule['service.serviceName'] }]);
                Array.prototype.push.apply(scheduleArr, [{ scheduleDate: client.attendanceType[keyClient].scdate }]);
                Array.prototype.push.apply(scheduleArr, [{ scheduleStartTime: client.attendanceType[keyClient].scheduleStartTime }]);
                Array.prototype.push.apply(scheduleArr, [{ scheduleEndTime: client.attendanceType[keyClient].scheduleEndTime }]);
                Array.prototype.push.apply(scheduleArr, [{ attendance: attendance }]);
                let scheduleObj = [];
                scheduleObj = Object.assign.apply(Object, scheduleArr);
                pendingSchedules = pendingSchedules.concat(scheduleObj);
              }
              if (!sid.includes(schedule.scheduleId)) sid.push(schedule.scheduleId);
              if (!sdate.includes(client.attendanceType[keyClient].scdate)) sdate.push(client.attendanceType[keyClient].scdate);
              if (!stime.includes(client.attendanceType[keyClient].scheduleStartTime)) stime.push(client.attendanceType[keyClient].scheduleStartTime);
              if (!etime.includes(client.attendanceType[keyClient].scheduleEndTime)) etime.push(client.attendanceType[keyClient].scheduleEndTime);
            }
          }
        }
      }


      // for removing the duplicate data
      let p_schedule = pendingSchedules.filter((schedule, index, self) =>
        index === self.findIndex((t) => (
          t.scheduleId == schedule.scheduleId &&
          t.scheduleDate == schedule.scheduleDate &&
          t.scheduleStartTime == schedule.scheduleStartTime &&
          t.scheduleEndTime == schedule.scheduleEndTime
        ))
      )


      if ((searchKey["booking_date"] == 1) && (searchKey["schedule_time"] == 1)) {
        p_schedule.sort(function (a, b) {
          let b_mill = new Date(b.scheduleDate).getTime() + Number(b.scheduleStartTime.split(':')[0]) * 60 + Number(b.scheduleStartTime.split(':')[1]) * 1000;
          let a_mill = new Date(a.scheduleDate).getTime() + Number(a.scheduleStartTime.split(':')[0]) * 60 + Number(a.scheduleStartTime.split(':')[1]) * 1000;
          return a_mill - b_mill;
        });
      } else {
        p_schedule.sort(function (a, b) {
          let b_mill = new Date(b.scheduleDate).getTime() + Number(b.scheduleStartTime.split(':')[0]) * 60 + Number(b.scheduleStartTime.split(':')[1]) * 1000;
          let a_mill = new Date(a.scheduleDate).getTime() + Number(a.scheduleStartTime.split(':')[0]) * 60 + Number(a.scheduleStartTime.split(':')[1]) * 1000;
          return b_mill - a_mill;
        });
      }

      //pagination & search
      let data = await rsPaginate(p_schedule, pageNo, 1000);
      let resultSet = [];
      if (searchKey[1].trim() == '') {
        resultSet = data;
      } else {
        for (let i = 0; i < data.length; i++) {
          let schedule_name = data[i].scheduleName.toLowerCase();
          let service_name = data[i].serviceName.toLowerCase();
          if (schedule_name.includes(searchKey[1]) ||
            service_name.includes(searchKey[1])) {
            resultSet = resultSet.concat(data[i]);
          }
        }
      }
      dbServices.sequelize.close();
      dbCSM.sequelize.close();
      dbSchedule.sequelize.close();
      return await resultSet;
    } catch (error) {
      throw error;
    }
  };

  newattendanceHistory = async (
    information,
    booking_date,
    schedule_time,
    searchKey,
    filterPrivateFields = true
  ) => {
    try {
      let userId = information.split(',')[2];
      let centerId = information.split(',')[3];

      let permittedSchedules = createInstance(["teamScheduleMap"], information);
      let dbSchedule = createInstance(["clientSchedule"], information);
      permittedSchedules.teamschedulemap.belongsTo(dbSchedule.schedule, {
        foreignKey: "scheduleId",
        targetKey: "scheduleId"
      });

      let CheckUser = await loginSignUpHelperDB.viewLogin({ businessId: information.split(',')[1], userId: userId });
      let UserStatus = 0;
      if (CheckUser.length == 0) throw new ApplicationError("Invalid User", 401);
      if (CheckUser[0].userType == 2) {
        var checkSchedule = await permittedSchedules.teamschedulemap.findAll({
          attributes: ['scheduleId'],
          where: {
            teamId: userId,
            status: 1,
            businessId: information.split(',')[1]
          },
          include: [{ model: dbSchedule.schedule, attributes: ['centerId'], where: { centerId: centerId } }],
          raw: true,
        });


        if (checkSchedule.length == 0) {
          throw new ApplicationError("User is not permitted to give attendance", 401);
        }

        UserStatus = 2;

      } else if (CheckUser[0].userType == 1) {
        UserStatus = 1;
      } else {
        throw new ApplicationError("Error in UserId, Please check", 401);
      }
      let Obj = {};
      if (UserStatus == 1) {
        Obj = {
          status: 1,
          businessId: information.split(',')[1],
        };
      } else {
        let scheArr = [];
        for (let i = 0; i < checkSchedule.length; i++) {
          scheArr.push(checkSchedule[i].scheduleId);
        }

        Obj = {
          status: 1,
          businessId: information.split(',')[1],
          scheduleId: {
            [Sequelize.Op.in]: scheArr
          }
        };
      }


      var businessIdinformation = information.split(",")[1];
      let td = new Date().toISOString().split('T')[0];
      let today = new Date(td);
      // let today = new Date();

      let dbCSM = createInstance(["clientScheduleMap"], information);
      let dbServices = createInstance(["clientService"], information);

      dbSchedule.schedule.belongsTo(dbCSM.clientschedulemap, {
        foreignKey: "scheduleId",
        targetKey: "scheduleId"
      });
      dbSchedule.schedule.belongsTo(dbServices.services, {
        foreignKey: "serviceId",
        targetKey: "serviceId"
      });

      let pageNo = parseInt(searchKey[0]);
      let showLimit;
      if (pageNo > 0) {
        pageNo = parseInt(searchKey[0]);
        showLimit = PEGINATION_LIMIT; //show 2 records per page
      } else {
        pageNo = 1;
        showLimit = 1000; //show first 1000 records if no page number is given
      }

      let orderBy;
      if ((searchKey["booking_date"] == 2) && (searchKey["schedule_time"] == 2)) {
        orderBy = [
          ["scheduleDate", "ASC"],
          ["startTime"]
        ];
      } else if (searchKey["booking_date"] == 2 && searchKey["schedule_time"] == 1) {
        orderBy = [
          ["scheduleDate", "ASC"],
        ];
      } else if ((searchKey["booking_date"] == 2) && (searchKey["schedule_time"] == 0)) {
        orderBy = [
          ["scheduleDate", "ASC"],
        ];
      } else if ((searchKey["booking_date"] == 1) && (searchKey["schedule_time"] == 2)) {
        orderBy = [
          ["scheduleDate", "ASC"],
        ];
      } else {
        orderBy = [
          ["scheduleDate", "ASC"],
        ];
      }

      let schedules = await dbSchedule.schedule.findAll({
        where: Obj,
        // where: {
        //   status: 1,
        //   businessId: businessIdinformation,
        // },
        include: [{
          model: dbServices.services,
          attributes: ['serviceName'],
        }],
        order: orderBy,
        raw: true
      });

      //showing the necessary data
      let pendingSchedules = [];
      nextSchedule: for (let schedule of schedules) { //loop through all the bookings
        for (let stime of schedule.scheduleTime) {
          // let sdatetime = new Date(stime.scdate + ' ' + stime.scheduleStartTime);
          if (+new Date(stime.scdate) <= +(new Date(today))) {
            let clients = await dbCSM.clientschedulemap.findAll({
              where: {
                scheduleId: schedule.scheduleId,
                status: 1
              },
              raw: true
            });
            if (clients.length > 0) {
              let countClientAtd = 0;
              nextClient: for (let client of clients) {
                for (let ctime of client.attendanceType) {
                  // let cdatetime = new Date(ctime.scdate + ' ' + ctime.scheduleStartTime);
                  if (+new Date(stime.scdate) === +new Date(ctime.scdate)) {
                    if (
                      ctime.atd == 1 ||
                      ctime.atd == 2 ||
                      ctime.atd == 3 ||
                      ctime.atd == 6 ||
                      ctime.atd == 7
                    ) {
                      countClientAtd++;
                      continue nextClient;
                    }
                  }
                }
              }
              if (countClientAtd == clients.length) {
                let scheduleArr = [];
                Array.prototype.push.apply(scheduleArr, [{ scheduleId: schedule.scheduleId }]);
                schedule['bookingName'] = schedule.scheduleName === null ? schedule['service.serviceName'] : schedule.scheduleName;
                Array.prototype.push.apply(scheduleArr, [{ scheduleName: schedule.bookingName }]);
                Array.prototype.push.apply(scheduleArr, [{ serviceName: schedule['service.serviceName'] }]);
                Array.prototype.push.apply(scheduleArr, [{ scheduleDate: schedule.scheduleDate }]);
                Array.prototype.push.apply(scheduleArr, [{ scheduleStartTime: schedule.startTime }]);
                Array.prototype.push.apply(scheduleArr, [{ scheduleEndTime: schedule.endTime }]);
                let scheduleObj = [];
                scheduleObj = Object.assign.apply(Object, scheduleArr);
                pendingSchedules = pendingSchedules.concat(scheduleObj);
                continue nextSchedule;
              }
            }
          }
        }
      }

      //pagination & search
      let data = await rsPaginate(pendingSchedules, pageNo, showLimit);
      let resultSet = [];
      if (searchKey[1].trim() == '') {
        resultSet = data;
      } else {
        for (let i = 0; i < data.length; i++) {
          let schedule_name = data[i].scheduleName.toLowerCase();
          let service_name = data[i].serviceName.toLowerCase();
          if (schedule_name.includes(searchKey[1]) ||
            service_name.includes(searchKey[1])) {
            resultSet = resultSet.concat(data[i]);
          }
        }
      }

      dbServices.sequelize.close();
      dbCSM.sequelize.close();
      dbSchedule.sequelize.close();
      return await resultSet;
    } catch (error) {
      throw error;
    }
  };

  // Booking Attendance History New, i will removed after testing
  newBookingAttendanceHistory_1 = async (
    information,
    scheduleId,
    booking_date,
    schedule_time,
    searchKey,
    filterPrivateFields = true
  ) => {
    try {
      let getInfo = information.split(",");
      let businessId = getInfo[1];
      let today = new Date();
      let dbSchedule = createInstance(["clientSchedule"], information);
      let dbCSM = createInstance(["clientScheduleMap"], information);
      let dbServices = createInstance(["clientService"], information);

      dbSchedule.schedule.belongsTo(dbCSM.clientschedulemap, {
        foreignKey: "scheduleId",
        targetKey: "scheduleId"
      });
      dbSchedule.schedule.belongsTo(dbServices.services, {
        foreignKey: "serviceId",
        targetKey: "serviceId"
      });

      let orderBy;
      if ((searchKey["booking_date"] == 2) && (searchKey["schedule_time"] == 2)) {
        orderBy = [
          ["scheduleDate", "ASC"],
          ["startTime"]
        ];
      } else if (searchKey["booking_date"] == 2 && searchKey["schedule_time"] == 1) {
        orderBy = [
          ["scheduleDate", "ASC"],
        ];
      } else if ((searchKey["booking_date"] == 2) && (searchKey["schedule_time"] == 0)) {
        orderBy = [
          ["scheduleDate", "ASC"],
        ];
      } else if ((searchKey["booking_date"] == 1) && (searchKey["schedule_time"] == 2)) {
        orderBy = [
          ["scheduleDate", "ASC"],
        ];
      } else {
        orderBy = [
          ["scheduleDate", "ASC"],
        ];
      }

      let schedule = await dbSchedule.schedule.find({
        where: {
          status: 1,
          scheduleId: scheduleId,
        },
        include: [{
          model: dbServices.services,
          attributes: ['serviceName'],
        }],
        order: orderBy,
        raw: true
      });

      //showing the necessary data
      let pendingSchedules = [];

      let foundUnmarked = 0;
      for (let key = 0; key < schedule.scheduleTime.length; key++) { //loop through the schedules
        if (+(new Date(schedule.scheduleTime[key].scdate)) < today) { //checking if the date is past
          let schedule_date = new Date(schedule.scheduleTime[key].scdate);

          let setObj = { scheduleId: schedule.scheduleId, status: 1 };
          let attendance = await attendanceInfoOfClientAndDate(information, setObj, schedule_date);
          if (parseInt(attendance.unmarked) > 0) foundUnmarked = 1;
          let scheduleArr = [];
          if (schedule.scheduleTime[key].reschedule != 2) {
            Array.prototype.push.apply(scheduleArr, [{ scheduleDate: schedule.scheduleTime[key].scdate }]);
            Array.prototype.push.apply(scheduleArr, [{ scheduleStartTime: schedule.scheduleTime[key].scheduleStartTime }]);
            Array.prototype.push.apply(scheduleArr, [{ scheduleEndTime: schedule.scheduleTime[key].scheduleEndTime }]);
            Array.prototype.push.apply(scheduleArr, [{ atdBy: "" }]);
            Array.prototype.push.apply(scheduleArr, [{ atdOn: "" }]);
            Array.prototype.push.apply(scheduleArr, [{ scheduleFrom: schedule.scheduleTime[key].scheduleFrom }]);
            Array.prototype.push.apply(scheduleArr, [{ reschedule: schedule.scheduleTime[key].reschedule }]);
            Array.prototype.push.apply(scheduleArr, [{ rescheduleTo: schedule.scheduleTime[key].rescheduleDate }]);
            Array.prototype.push.apply(scheduleArr, [{ rescheduled_Status: schedule.scheduleTime[key].reschedule == 2 ? "rescheduled to " + schedule.scheduleTime[key].rescheduleDate : "" }]);
            Array.prototype.push.apply(scheduleArr, [{ rescheduled_From: schedule.scheduleTime[key].scheduleFrom == "" ? "" : "rescheduled from " + schedule.scheduleTime[key].scheduleFrom }]);
            Array.prototype.push.apply(scheduleArr, [{ attendance: attendance }]);
            let scheduleObj = [];
            scheduleObj = Object.assign.apply(Object, scheduleArr);
            pendingSchedules = pendingSchedules.concat(scheduleObj);
          }
        }
      }

      //pagination & search
      let pageNo = parseInt(searchKey[0]);
      let data = await rsPaginate(pendingSchedules, pageNo, PEGINATION_LIMIT);
      let resultSet = [];
      if (searchKey[1].trim() == '') {
        resultSet = data;
      } else {
        for (let i = 0; i < data.length; i++) {
          let schedule_name = data[i].scheduleName.toLowerCase();
          let service_name = data[i].serviceName.toLowerCase();
          if (schedule_name.includes(searchKey[1]) ||
            service_name.includes(searchKey[1])) {
            resultSet = resultSet.concat(data[i]);
          }
        }
      }

      schedule['bookingName'] = schedule.scheduleName === null ? schedule['service.serviceName'] : schedule.scheduleName;
      let info = {
        header: {
          scheduleId: schedule.scheduleId,
          scheduleName: schedule.bookingName,
          startTime: schedule.startTime,
          endTime: schedule.endTime
        },
        attendanceHistory: foundUnmarked == 0 ? resultSet : []

      };

      dbServices.sequelize.close();
      dbCSM.sequelize.close();
      dbSchedule.sequelize.close();
      return await info;
    } catch (error) {
      throw error;
    }
  };


  newBookingAttendanceHistory = async (
    information,
    scheduleId,
    booking_date,
    schedule_time,
    searchKey,
    filterPrivateFields = true
  ) => {
    try {
      let getInfo = information.split(",");
      let businessId = getInfo[1];
      let today = new Date();
      let dbSchedule = createInstance(["clientSchedule"], information);
      let dbCSM = createInstance(["clientScheduleMap"], information);
      let dbServices = createInstance(["clientService"], information);
      let dbEmployee = createInstance(["clientEmployee"], information);

      dbSchedule.schedule.belongsTo(dbCSM.clientschedulemap, {
        foreignKey: "scheduleId",
        targetKey: "scheduleId"
      });
      dbSchedule.schedule.belongsTo(dbServices.services, {
        foreignKey: "serviceId",
        targetKey: "serviceId"
      });

      let orderBy;
      if ((searchKey["booking_date"] == 2) && (searchKey["schedule_time"] == 2)) {
        orderBy = [
          ["scheduleDate", "ASC"],
          ["startTime"]
        ];
      } else if (searchKey["booking_date"] == 2 && searchKey["schedule_time"] == 1) {
        orderBy = [
          ["scheduleDate", "ASC"],
        ];
      } else if ((searchKey["booking_date"] == 2) && (searchKey["schedule_time"] == 0)) {
        orderBy = [
          ["scheduleDate", "ASC"],
        ];
      } else if ((searchKey["booking_date"] == 1) && (searchKey["schedule_time"] == 2)) {
        orderBy = [
          ["scheduleDate", "ASC"],
        ];
      } else {
        orderBy = [
          ["scheduleDate", "ASC"],
        ];
      }

      let schedule = await dbSchedule.schedule.find({
        where: {
          status: 1,
          scheduleId: scheduleId,
        },
        include: [{
          model: dbServices.services,
          attributes: ['serviceName'],
        }],
        order: orderBy,
        raw: true
      });

      let clients = await dbCSM.clientschedulemap.findAll({
        where: {
          scheduleId: schedule.scheduleId,
          status: 1
        },
        raw: true
      });

      //showing the necessary data
      let pendingSchedules = [];
      // let foundUnmarked = 0;
      for (let key = 0; key < schedule.scheduleTime.length; key++) { //loop through the schedules
        if (+(new Date(schedule.scheduleTime[key].scdate)) < today) { //checking if the date is past
          let schedule_date = new Date(schedule.scheduleTime[key].scdate);

          let setObj = { scheduleId: schedule.scheduleId, status: 1 };
          let attendance = await attendanceInfoOfClientAndDate(information, setObj, schedule_date);
          // if (parseInt(attendance.unmarked) > 0) foundUnmarked = 1;

          let scheduleArr = [];
          if (schedule.scheduleTime[key].reschedule != 2 && parseInt(attendance.unmarked) == 0 && parseInt(attendance.totalClients) > 0) {

            let dateComp = new Date("1900-01-01");
            let getAtdBy = "";
            let getAtdOn = "";
            for (let client of clients) {
              for (let c_atd of client.attendanceType) {
                if (+schedule_date === +(new Date(c_atd.scdate))) {
                  let atdDateTime = new Date(c_atd.atdDate + ' ' + c_atd.atdTime);
                  if (+dateComp < +atdDateTime) {
                    let getEmployee = await dbEmployee.employees.find({
                      where: {
                        employeeId: c_atd.atdBy
                      },
                      raw: true
                    });
                    if (getEmployee != null) {
                      getAtdBy = getEmployee.employeeName;
                      getAtdOn = c_atd.atdDate + ' ' + c_atd.atdTime;
                      dateComp = atdDateTime;
                    } else {
                      getAtdBy = '';
                      getAtdOn = c_atd.atdDate + ' ' + c_atd.atdTime;
                      dateComp = atdDateTime;
                    }
                  }
                }
              }
            }

            Array.prototype.push.apply(scheduleArr, [{ scheduleDate: schedule.scheduleTime[key].scdate }]);
            Array.prototype.push.apply(scheduleArr, [{ scheduleStartTime: schedule.scheduleTime[key].scheduleStartTime }]);
            Array.prototype.push.apply(scheduleArr, [{ scheduleEndTime: schedule.scheduleTime[key].scheduleEndTime }]);
            Array.prototype.push.apply(scheduleArr, [{ atdBy: getAtdBy }]);
            Array.prototype.push.apply(scheduleArr, [{ atdOn: getAtdOn }]);
            Array.prototype.push.apply(scheduleArr, [{ scheduleFrom: schedule.scheduleTime[key].scheduleFrom }]);
            Array.prototype.push.apply(scheduleArr, [{ reschedule: schedule.scheduleTime[key].reschedule }]);
            Array.prototype.push.apply(scheduleArr, [{ rescheduleTo: schedule.scheduleTime[key].rescheduleDate }]);
            Array.prototype.push.apply(scheduleArr, [{ rescheduled_Status: schedule.scheduleTime[key].reschedule == 2 ? "rescheduled to " + schedule.scheduleTime[key].rescheduleDate : "" }]);
            Array.prototype.push.apply(scheduleArr, [{ rescheduled_From: schedule.scheduleTime[key].scheduleFrom == "" ? "" : "rescheduled from " + schedule.scheduleTime[key].scheduleFrom }]);
            Array.prototype.push.apply(scheduleArr, [{ attendance: attendance }]);
            let scheduleObj = [];
            scheduleObj = Object.assign.apply(Object, scheduleArr);
            pendingSchedules = pendingSchedules.concat(scheduleObj);
          }
        }
      }

      //pagination & search
      let pageNo = parseInt(searchKey[0]);
      let data = await rsPaginate(pendingSchedules, pageNo, PEGINATION_LIMIT);
      let resultSet = [];
      if (searchKey[1].trim() == '') {
        resultSet = data;
      } else {
        for (let i = 0; i < data.length; i++) {
          let schedule_name = data[i].scheduleName.toLowerCase();
          let service_name = data[i].serviceName.toLowerCase();
          if (schedule_name.includes(searchKey[1]) ||
            service_name.includes(searchKey[1])) {
            resultSet = resultSet.concat(data[i]);
          }
        }
      }

      schedule['bookingName'] = schedule.scheduleName === null ? schedule['service.serviceName'] : schedule.scheduleName;
      let info = {
        header: {
          scheduleId: schedule.scheduleId,
          scheduleName: schedule.bookingName,
          startTime: schedule.startTime,
          endTime: schedule.endTime
        },
        // attendanceHistory: foundUnmarked == 0 ? resultSet : []
        attendanceHistory: resultSet
      };

      dbEmployee.sequelize.close();
      dbServices.sequelize.close();
      dbCSM.sequelize.close();
      dbSchedule.sequelize.close();
      return await info;
    } catch (error) {
      throw error;
    }
  };


  showSchedulesOfClient = async (
    information,
    scheduleId,
    clientId,
    date,
    filterPrivateFields = true
  ) => {
    try {

      let dbCSM = createInstance(["clientScheduleMap"], information);
      let clientSchedule = await dbCSM.clientschedulemap.find({
        where: {
          clientId: clientId,
          scheduleId: scheduleId,
          status: 1
        },
        raw: true
      });

      let mergedInformation = [];
      if (clientSchedule) {
        if (!clientSchedule.attendanceType) {
          throw new ApplicationError(
            "Schedule Data Error !",
            401
          );
        } else {
          let mergedSchedules = [];
          Object.keys(clientSchedule.attendanceType).map(function (key) {
            if (+(new Date(key)) === +(new Date(date))) {
              if (clientSchedule.attendanceType[key] === '3') {
                let keyValuePairArray = [];
                // let keyValuePair = { [key]: clientSchedule.attendanceType[key] };
                let keyValuePair = { 'date': key };
                mergedSchedules = mergedSchedules.concat(keyValuePair);
              }
            }
          });

          let infoArray = [];
          Array.prototype.push.apply(infoArray, [{ scheduleId: clientSchedule.scheduleId }]);
          Array.prototype.push.apply(infoArray, [{ clientId: clientSchedule.clientId }]);
          Array.prototype.push.apply(infoArray, [{ attendance: mergedSchedules }]);
          let infoObj = [];
          infoObj = Object.assign.apply(Object, infoArray);
          mergedInformation = mergedInformation.concat(infoObj);
        }
      }


      dbCSM.sequelize.close();
      return mergedInformation;
    } catch (error) {
      throw error;
    }
  };

  schedulesWithSameService_ = async (
    information,
    scheduleId,
    centerId,
    date,
    filterPrivateFields = true
  ) => {
    try {
      let dbSchedule = createInstance(["clientSchedule"], information);
      let dbCSM = createInstance(["clientScheduleMap"], information);
      let dbCenter = createInstance(["clientCenter"], information);
      let dbClients = createInstance(["client"], information);
      let dbService = createInstance(["clientService"], information);
      dbSchedule.schedule.belongsTo(dbService.services, {
        foreignKey: "serviceId",
        targetKey: "serviceId"
      });
      dbSchedule.schedule.hasOne(dbCSM.clientschedulemap, {
        foreignKey: "scheduleId",
        targetKey: "scheduleId"
      });
      dbCSM.clientschedulemap.belongsTo(dbClients.clients, {
        foreignKey: "clientId",
        targetKey: "clientId"
      });
      //provided schedule information
      let givenSchedule = await dbSchedule.schedule.find({
        where: {
          scheduleId: scheduleId,
          status: 1
        },
        raw: true
      });

      //getting other schedules with the same serviceId of the given schedule
      let schedules = await dbSchedule.schedule.findAll({
        where: {
          status: 1,
          centerId: centerId,
          serviceId: givenSchedule.serviceId,
        },
        include: [{
          model: dbService.services
        }],
        raw: true
      });

      let mergedSchedules = [];
      for (let schedule of schedules) {

        // for (let key of Object.keys(schedule.scheduleTime)) {
        for (let key of schedule.scheduleTime) {
          if (+(new Date(key.scdate)) === +(new Date(date))) {
            let sameRepetition = schedule.repeatation_type == givenSchedule.repeatation_type ? 1 : 0;
            let mergedScheduleArr = [];


            let clientSchedules = await dbCSM.clientschedulemap.findAll({
              where: {
                status: 1,
                scheduleId: schedule.scheduleId
              },
              include: [{
                model: dbClients.clients
              }],
              raw: true
            });
            let mergedClients = [];
            for (let clientSchedule of clientSchedules) {

              // for (let atd of Object.keys(clientSchedule.attendanceType)) {
              for (let atd of clientSchedule.attendanceType) {
                if (+(new Date(atd.scdate)) === +(new Date(date))) {
                  let isphoto = (clientSchedule["client.photoUrl"] === null) ? KNACK_UPLOAD_URL.localUrl + 'noimage.jpg' : KNACK_UPLOAD_URL.localUrl + clientSchedule["client.photoUrl"];
                  mergedClients = mergedClients.concat({ clientId: clientSchedule["client.clientId"], clientName: clientSchedule["client.clientName"], clientphoto: isphoto });
                }
              }
            }

            let bookingName;
            // Set Day and Time
            var stime = tConv24(schedule.startTime);
            var etime = tConv24(schedule.endTime);
            let stime = stime + '-' + etime;
            let dayint = convartday(schedule.repeat_days);
            let day_time = ', ' + dayint + ', ' + stime;
            // End

            if (schedule.scheduleName == '') {
              bookingName = schedule["service.serviceName"] + day_time;
            } else {
              bookingName = schedule["scheduleName"];
            }

            let today = new Date();
            let yesterday = today.getTime() - 24 * 60 * 60 * 1000;
            let tomorrow = today.getTime() + 24 * 60 * 60 * 1000;
            let scheduleDay;

            if ((new Date(date)).toLocaleDateString() == (new Date(today)).toLocaleDateString()) {
              scheduleDay = 'today';
            } else if ((new Date(date)).toLocaleDateString() == (new Date(yesterday)).toLocaleDateString()) {
              scheduleDay = 'yesterday';
            } else if ((new Date(date)).toLocaleDateString() == (new Date(tomorrow)).toLocaleDateString()) {
              scheduleDay = 'tomorrow';
            } else {
              scheduleDay = date;
            }

            // Array.prototype.push.apply(mergedScheduleArr, [schedule]);
            Array.prototype.push.apply(mergedScheduleArr, [{ scheduleId: schedule.scheduleId }]);
            Array.prototype.push.apply(mergedScheduleArr, [{ bookingName: bookingName }]);
            Array.prototype.push.apply(mergedScheduleArr, [{ serviceName: schedule["service.serviceName"] }]);
            Array.prototype.push.apply(mergedScheduleArr, [{ scheduleDate: scheduleDay }]);
            Array.prototype.push.apply(mergedScheduleArr, [{ startTime: schedule.startTime }]);
            Array.prototype.push.apply(mergedScheduleArr, [{ endTime: schedule.endTime }]);
            Array.prototype.push.apply(mergedScheduleArr, [{ sameRepetition: sameRepetition }]);
            Array.prototype.push.apply(mergedScheduleArr, [{ clients: mergedClients }]);
            if (schedule.scheduleId == scheduleId)
              Array.prototype.push.apply(mergedScheduleArr, [{ selectedSchedule: 1 }]);
            else
              Array.prototype.push.apply(mergedScheduleArr, [{ selectedSchedule: 0 }]);
            let mergedScheduleObj = [];
            mergedScheduleObj = Object.assign.apply(Object, mergedScheduleArr);
            mergedSchedules = mergedSchedules.concat(mergedScheduleObj);
          }
        }
      }

      dbCSM.sequelize.close();
      dbService.sequelize.close();
      dbClients.sequelize.close();
      dbCenter.sequelize.close();
      dbSchedule.sequelize.close();
      return mergedSchedules;
    } catch (error) {
      throw error;
    }
  }

  schedulesWithSameService = async (
    information,
    scheduleId,
    centerId,
    date,
    filterPrivateFields = true
  ) => {
    try {
      let dbSchedule = createInstance(["clientSchedule"], information);
      let dbCSM = createInstance(["clientScheduleMap"], information);
      let dbCenter = createInstance(["clientCenter"], information);
      let dbClients = createInstance(["client"], information);
      let dbService = createInstance(["clientService"], information);
      dbSchedule.schedule.belongsTo(dbService.services, {
        foreignKey: "serviceId",
        targetKey: "serviceId"
      });
      dbSchedule.schedule.hasOne(dbCSM.clientschedulemap, {
        foreignKey: "scheduleId",
        targetKey: "scheduleId"
      });
      dbCSM.clientschedulemap.belongsTo(dbClients.clients, {
        foreignKey: "clientId",
        targetKey: "clientId"
      });
      //provided schedule information
      let givenSchedule = await dbSchedule.schedule.find({
        where: {
          scheduleId: scheduleId,
          status: 1
        },
        raw: true
      });

      //getting other schedules with the same serviceId of the given schedule
      let schedules = await dbSchedule.schedule.findAll({
        where: {
          status: 1,
          centerId: centerId,
          serviceId: givenSchedule.serviceId,
        },
        include: [{
          model: dbService.services
        }],
        raw: true
      });

      let mergedSchedules = [];
      for (let schedule of schedules) {

        // for (let key of Object.keys(schedule.scheduleTime)) {
        for (let key of schedule.scheduleTime) {
          if (+(new Date(key.scdate)) === +(new Date(date))) {
            let sameRepetition = schedule.repeatation_type == givenSchedule.repeatation_type ? 1 : 0;
            let mergedScheduleArr = [];


            let clientSchedules = await dbCSM.clientschedulemap.findAll({
              where: {
                status: 1,
                scheduleId: schedule.scheduleId
              },
              include: [{
                model: dbClients.clients
              }],
              raw: true
            });
            let m_clients = [];
            for (let clientSchedule of clientSchedules) {

              // for (let atd of Object.keys(clientSchedule.attendanceType)) {
              for (let atd of clientSchedule.attendanceType) {
                if (+(new Date(atd.scdate)) === +(new Date(date))) {
                  let isphoto = (clientSchedule["client.photoUrl"] === null) ? KNACK_UPLOAD_URL.localUrl + 'noimage.jpg' : KNACK_UPLOAD_URL.localUrl + clientSchedule["client.photoUrl"];
                  m_clients = m_clients.concat({ clientId: clientSchedule["client.clientId"], clientName: clientSchedule["client.clientName"], clientphoto: isphoto });
                }
              }
            }
            //this code removes duplicate data
            let mergedClients = m_clients.filter((client, index, self) =>
              index === self.findIndex((t) => (
                t.clientId == client.clientId &&
                t.clientName == client.clientName &&
                t.clientphoto == client.clientphoto
              ))
            )

            let bookingName;
            // Set Day and Time
            var stime = tConv24(schedule.startTime);
            var etime = tConv24(schedule.endTime);
            let stime = stime + '-' + etime;
            let dayint = convartday(schedule.repeat_days);
            let day_time = ', ' + dayint + ', ' + stime;
            // End

            if (schedule.scheduleName == '') {
              bookingName = schedule["service.serviceName"] + day_time;
            } else {
              bookingName = schedule["scheduleName"];
            }

            let today = new Date();
            let yesterday = today.getTime() - 24 * 60 * 60 * 1000;
            let tomorrow = today.getTime() + 24 * 60 * 60 * 1000;
            let scheduleDay;

            if ((new Date(date)).toLocaleDateString() == (new Date(today)).toLocaleDateString()) {
              scheduleDay = 'today';
            } else if ((new Date(date)).toLocaleDateString() == (new Date(yesterday)).toLocaleDateString()) {
              scheduleDay = 'yesterday';
            } else if ((new Date(date)).toLocaleDateString() == (new Date(tomorrow)).toLocaleDateString()) {
              scheduleDay = 'tomorrow';
            } else {
              scheduleDay = date;
            }

            // Array.prototype.push.apply(mergedScheduleArr, [schedule]);
            Array.prototype.push.apply(mergedScheduleArr, [{ scheduleId: schedule.scheduleId }]);
            Array.prototype.push.apply(mergedScheduleArr, [{ bookingName: bookingName }]);
            Array.prototype.push.apply(mergedScheduleArr, [{ serviceName: schedule["service.serviceName"] }]);
            Array.prototype.push.apply(mergedScheduleArr, [{ scheduleDate: scheduleDay }]);
            Array.prototype.push.apply(mergedScheduleArr, [{ startTime: key.scheduleStartTime }]);
            Array.prototype.push.apply(mergedScheduleArr, [{ endTime: key.scheduleEndTime }]);
            Array.prototype.push.apply(mergedScheduleArr, [{ sameRepetition: sameRepetition }]);
            Array.prototype.push.apply(mergedScheduleArr, [{ clients: mergedClients }]);
            if (schedule.scheduleId == scheduleId)
              Array.prototype.push.apply(mergedScheduleArr, [{ selectedSchedule: 1 }]);
            else
              Array.prototype.push.apply(mergedScheduleArr, [{ selectedSchedule: 0 }]);
            let mergedScheduleObj = [];
            mergedScheduleObj = Object.assign.apply(Object, mergedScheduleArr);
            mergedSchedules = mergedSchedules.concat(mergedScheduleObj);
          }
        }
      }

      dbCSM.sequelize.close();
      dbService.sequelize.close();
      dbClients.sequelize.close();
      dbCenter.sequelize.close();
      dbSchedule.sequelize.close();
      return mergedSchedules;
    } catch (error) {
      throw error;
    }
  }

  // Client Reschedule  
  clientReschedule = async (
    information,
    scheduleId,
    clientId,
    rescheduleFrom,
    rescheduleFromTime,
    rescheduleTo,
    newScheduleId,
    centerId,
    comment,
    scheduleName,
    startTime,
    endTime,
    scheduleTime,
    numberOfClients,
    teamId
  ) => {
    try {
      let checkAdmin = await CheckMainOrCenterAdmin(information);
      let getInfo = information.split(",");
      let dbBusinessId = getInfo[1];
      let adminId = getInfo[2];
      let dbSchedule = createInstance(["clientSchedule"], information);
      let dbCSM = createInstance(["clientScheduleMap"], information);
      let dbTSM = createInstance(["teamScheduleMap"], information);
      let dbEmployee = createInstance(["clientEmployee"], information);
      let dbClients = createInstance(["client"], information);

      let client = await dbCSM.clientschedulemap.find({
        where: {
          status: 1,
          scheduleId: scheduleId,
          clientId: clientId
        },
        raw: true
      });

      let setObj = { status: 1, subscriptionId: client.subscriptionId };
      let session = await sessionInfo(information, setObj);

      let expiry = await expiryDate(information, client.subscriptionId);

      let updateClientSchedule;
      let newAtd = [];

      if (newScheduleId.trim() != "") { //from existing schedule on that day
        if (scheduleId == newScheduleId) { //if same schedule
          let schedule = await dbSchedule.schedule.find({
            where: {
              scheduleId: scheduleId,
            },
            raw: true
          });
          let atd = [];
          for (let i of client.attendanceType) {
            atd.push(i.scdate);
          }
          let stime = [];
          for (let i of schedule.scheduleTime) {
            stime.push(i.scdate);
          }
          let hasDateCSM = atd.includes(rescheduleTo);
          let hasDateSchedule = stime.includes(rescheduleTo);
          if (hasDateCSM) {
            throw new ApplicationError(
              "Cannot Reschedule On Existing Date of Existing Schedule",
              401
            );
          } else if (!hasDateSchedule) {
            throw new ApplicationError(
              "Date Does Not Exist In The Given Schedule",
              401
            );
          } else if (hasDateSchedule && !hasDateCSM) {
            let updateFlag = 1;
            for (let key of client.attendanceType) {
              if (+(new Date(key.scdate)) == +(new Date(rescheduleFrom)) && key.atd == 7) {
                updateFlag = 0;
              }
            }
            if (updateFlag == 1) {
              let foundClientDate = 0,
                flagClient = 0;
              let newClientScheduleTimeTable = [];
              for (let key of client.attendanceType) {
                let sc_date = new Date(key.scdate + ' ' + key.scheduleStartTime);
                let reschedule_from = new Date(rescheduleFrom + ' ' + rescheduleFromTime);
                if (+sc_date == +reschedule_from) {
                  // newAtd[key] = '7';
                  newAtd.push({
                    id: key.id,
                    scdate: key.scdate,
                    atd: 7,
                    atdBy: key.atdBy,
                    atdDate: key.atdDate,
                    atdTime: key.atdTime,
                    reschedule: 2,
                    rescheduleId: newScheduleId,
                    rescheduleDate: rescheduleTo,
                    scheduleStartTime: key.scheduleStartTime,
                    scheduleEndTime: key.scheduleEndTime,
                    scheduleFrom: key.scheduleFrom,
                    centerId: key.centerId,
                    rescheduleBy: adminId,
                    comment: comment,
                  });
                  foundClientDate = 1;
                } else {
                  let sc_date = new Date(key.scdate + ' ' + key.scheduleStartTime);
                  let reschedule_to = new Date(rescheduleTo + ' ' + schedule.startTime);
                  if (+sc_date > +reschedule_to && flagClient == 0) {
                    // newAtd[rescheduleTo] = '4';
                    let s_date = rescheduleFrom.split("-"),
                      s_time = schedule.startTime.split(":");
                    let id = "" + s_date[0] + s_date[1] + s_date[2] + s_time[0] + s_time[1];
                    newAtd.push({
                      id: id,
                      scdate: rescheduleTo,
                      atd: 4,
                      atdBy: "",
                      atdDate: "",
                      atdTime: "",
                      reschedule: 1,
                      rescheduleId: scheduleId,
                      rescheduleDate: rescheduleTo,
                      scheduleStartTime: schedule.startTime,
                      scheduleEndTime: schedule.endTime,
                      scheduleFrom: rescheduleFrom,
                      centerId: centerId,
                      rescheduleBy: "",
                      comment: "",
                    });
                    flagClient = 1;
                  }
                  // newAtd[key] = client.attendanceType[key].atd;
                  newAtd.push({
                    id: key.id,
                    scdate: key.scdate,
                    atd: key.atd,
                    atdBy: key.atdBy,
                    atdDate: key.atdDate,
                    atdTime: key.atdTime,
                    reschedule: key.reschedule,
                    rescheduleId: key.rescheduleId,
                    rescheduleDate: key.rescheduleDate,
                    scheduleStartTime: key.scheduleStartTime,
                    scheduleEndTime: key.scheduleEndTime,
                    scheduleFrom: key.scheduleFrom,
                    centerId: key.centerId,
                    rescheduleBy: key.rescheduleBy,
                    comment: key.comment,
                  });
                }
              }
              if (foundClientDate == 1 && flagClient == 0) {
                // newAtd[rescheduleTo] = '4';
                let s_date = rescheduleFrom.split("-"),
                  s_time = schedule.startTime.split(":");
                let id = "" + s_date[0] + s_date[1] + s_date[2] + s_time[0] + s_time[1];
                newAtd.push({
                  id: id,
                  scdate: rescheduleTo,
                  atd: 4,
                  atdBy: "",
                  atdDate: "",
                  atdTime: "",
                  reschedule: 1,
                  rescheduleId: scheduleId,
                  rescheduleDate: rescheduleTo,
                  scheduleStartTime: schedule.startTime,
                  scheduleEndTime: schedule.endTime,
                  scheduleFrom: rescheduleFrom,
                  centerId: centerId,
                  rescheduleBy: "",
                  comment: "",
                });
              }

              //update the schedule
              updateClientSchedule = await dbCSM.clientschedulemap.update({
                attendanceType: newAtd,
                updateDateTime: Date.now()
              }, {
                  where: {
                    clientScheduleMapId: client.clientScheduleMapId
                  }
                });
            } else {
              throw new ApplicationError(
                "cannot reschedule an already rescheduled date",
                401
              );
            }
          }
        } else { //other schedule
          let schedule = await dbSchedule.schedule.find({
            where: {
              scheduleId: newScheduleId,
            },
            raw: true
          });

          if (schedule == null) {
            throw new ApplicationError(
              "newScheduleId has not been found !",
              401
            );
          }
          let datetimeExist = 0;
          for (let key of schedule.scheduleTime) {
            let sc_date = new Date(key.scdate + ' ' + key.scheduleStartTime);
            let reschedule_to = new Date(rescheduleTo + ' ' + startTime);
            if (+sc_date === +reschedule_to) {
              datetimeExist = 1;
            }
          }
          if (datetimeExist == 0) {
            throw new ApplicationError(
              "Date & Time is not present in the new schedule",
              401
            );
          }

          let updateFlag = 1;
          for (let key of client.attendanceType) {
            if (+(new Date(key.scdate)) == +(new Date(rescheduleFrom)) && key.atd == 7) {
              updateFlag = 0;
            }
          }
          if (updateFlag == 1) {
            for (let key of client.attendanceType) {
              let sc_date = new Date(key.scdate + ' ' + key.scheduleStartTime);
              let reschedule_from = new Date(rescheduleFrom + ' ' + rescheduleFromTime);
              if (+sc_date == +reschedule_from) {
                // newAtd[key] = '7';
                newAtd.push({
                  id: key.id,
                  scdate: key.scdate,
                  atd: 7,
                  atdBy: key.atdBy,
                  atdDate: key.atdDate,
                  atdTime: key.atdTime,
                  reschedule: 2,
                  rescheduleId: newScheduleId,
                  rescheduleDate: rescheduleTo,
                  scheduleStartTime: key.scheduleStartTime,
                  scheduleEndTime: key.scheduleEndTime,
                  scheduleFrom: key.scheduleFrom,
                  centerId: key.centerId,
                  rescheduleBy: adminId,
                  comment: comment,
                });
              } else {
                // newAtd[key] = client.attendanceType[key].atd;
                newAtd.push({
                  id: key.id,
                  scdate: key.scdate,
                  atd: key.atd,
                  atdBy: key.atdBy,
                  atdDate: key.atdDate,
                  atdTime: key.atdTime,
                  reschedule: key.reschedule,
                  rescheduleId: key.rescheduleId,
                  rescheduleDate: key.rescheduleDate,
                  scheduleStartTime: key.scheduleStartTime,
                  scheduleEndTime: key.scheduleEndTime,
                  scheduleFrom: key.scheduleFrom,
                  centerId: key.centerId,
                  rescheduleBy: key.rescheduleBy,
                  comment: key.comment,
                });
              }
            }


            //update the old row
            if (scheduleId == client.parentScheduleId && scheduleId == client.masterParentScheduleId && client.isActive == 1) { // default schedule is always active
              updateClientSchedule = await dbCSM.clientschedulemap.update({
                attendanceType: newAtd,
                updateDateTime: Date.now()
              }, {
                  where: {
                    clientScheduleMapId: client.clientScheduleMapId
                  }
                });
            } else { //if rescheduled date is again rescheduled
              updateClientSchedule = await dbCSM.clientschedulemap.update({
                attendanceType: newAtd,
                isActive: 0,
                updateDateTime: Date.now()
              }, {
                  where: {
                    clientScheduleMapId: client.clientScheduleMapId
                  }
                });
            }

            //inserting into new row
            let s_date = rescheduleFrom.split("-"),
              s_time = startTime.split(":");
            let id = "" + s_date[0] + s_date[1] + s_date[2] + s_time[0] + s_time[1];
            let newlyCreatedAtd = [{
              id: id,
              scdate: rescheduleTo,
              atd: 4,
              atdBy: "",
              atdDate: "",
              atdTime: "",
              reschedule: 1,
              rescheduleId: "",
              rescheduleDate: rescheduleTo,
              scheduleStartTime: startTime,
              scheduleEndTime: endTime,
              scheduleFrom: rescheduleFrom,
              centerId: centerId,
              rescheduleBy: "",
              comment: "",
            }];
            let clientschedulesmap = await dbCSM.clientschedulemap.create({
              businessId: client.businessId,
              scheduleId: newScheduleId,
              clientId: client.clientId,
              attendanceType: newlyCreatedAtd,
              subscriptionId: client.subscriptionId,
              parentScheduleId: scheduleId,
              masterParentScheduleId: scheduleId,
              isActive: 1
            });
          } else {
            throw new ApplicationError(
              "cannot reschedule an already rescheduled date",
              401
            );
          }
        }

      } else { //new schedule
        let newAtd = [];
        for (let key of client.attendanceType) {
          let sc_date = new Date(key.scdate + ' ' + key.scheduleStartTime);
          let reschedule_from = new Date(rescheduleFrom + ' ' + rescheduleFromTime);
          if (+sc_date == +reschedule_from) {
            newAtd.push({
              id: key.id,
              scdate: key.scdate,
              atd: 7,
              atdBy: key.atdBy,
              atdDate: key.atdDate,
              atdTime: key.atdTime,
              reschedule: 2,
              rescheduleId: newScheduleId,
              rescheduleDate: rescheduleTo,
              scheduleStartTime: key.scheduleStartTime,
              scheduleEndTime: key.scheduleEndTime,
              scheduleFrom: key.scheduleFrom,
              centerId: key.centerId,
              rescheduleBy: adminId,
              comment: comment,
            });
          } else {
            newAtd.push({
              id: key.id,
              scdate: key.scdate,
              atd: key.atd,
              atdBy: key.atdBy,
              atdDate: key.atdDate,
              atdTime: key.atdTime,
              reschedule: key.reschedule,
              rescheduleId: key.rescheduleId,
              rescheduleDate: key.rescheduleDate,
              scheduleStartTime: key.scheduleStartTime,
              scheduleEndTime: key.scheduleEndTime,
              scheduleFrom: key.scheduleFrom,
              centerId: key.centerId,
              rescheduleBy: key.rescheduleBy,
              comment: key.comment,
            });
          }
        }
        let updateCSM = await dbCSM.clientschedulemap.update({
          attendanceType: newAtd
        }, {
            where: {
              clientId: clientId,
              scheduleId: scheduleId
            },
            raw: true
          });
        let s_date = rescheduleFrom.split("-"),
          s_time = startTime.split(":");
        let id = "" + s_date[0] + s_date[1] + s_date[2] + s_time[0] + s_time[1];
        let scheduleTime = [{
          id: id,
          scdate: rescheduleTo,
          reschedule: 1,
          rescheduleDate: rescheduleTo,
          scheduleStartTime: startTime,
          scheduleEndTime: endTime,
          scheduleFrom: rescheduleFrom,
          centerId: "",
          rescheduleBy: "",
          comment: "",
        }];
        let oldSchedule = await dbSchedule.schedule.find({
          where: {
            scheduleId: scheduleId,
          },
          raw: true
        });

        let newSchedule = await dbSchedule.schedule.create({
          businessId: dbBusinessId,
          centerId: centerId,
          serviceId: oldSchedule.serviceId,
          scheduleDate: rescheduleTo,
          startTime: startTime,
          endTime: endTime,
          numberOfClients: numberOfClients,
          scheduleName: scheduleName,
          is_repeat: 0,
          repeat_ends: 1,
          repeat_ends_date: rescheduleTo,
          scheduleTime: scheduleTime,
        });
        let updateNewSchedule = await dbSchedule.schedule.update({
          parent_scheduleId: newSchedule.scheduleId
        }, {
            where: {
              businessId: dbBusinessId,
              scheduleId: newSchedule.scheduleId
            },
            raw: true
          });
        updateClientSchedule = newSchedule == null ? [0] : [1];
        let oldClientSchedule = await dbCSM.clientschedulemap.find({
          where: {
            status: 1,
            scheduleId: scheduleId,
            clientId: clientId
          },
          raw: true
        });
        let newClientSchedule = await dbCSM.clientschedulemap.create({
          businessId: newSchedule.businessId,
          scheduleId: newSchedule.scheduleId,
          clientId: clientId,
          attendanceType: [{
            id: id,
            scdate: rescheduleTo,
            atd: 4,
            atdBy: "",
            atdDate: "",
            atdTime: "",
            reschedule: 1,
            rescheduleId: newSchedule.scheduleId,
            rescheduleDate: rescheduleTo,
            scheduleStartTime: startTime,
            scheduleEndTime: endTime,
            scheduleFrom: rescheduleFrom,
            centerId: centerId,
            rescheduleBy: "",
            comment: "",
          }],
          subscriptionId: oldClientSchedule.subscriptionId,
          parentScheduleId: oldClientSchedule.scheduleId,
          masterParentScheduleId: oldClientSchedule.masterParentScheduleId
        });

        //adding new team members into teamschedulemap
        let deleteTSM = await dbTSM.teamschedulemap.update({
          status: 0
        }, {
            where: {
              scheduleId: oldClientSchedule.scheduleId
            },
            raw: true
          });
        let teamIdArray = await commaSeparatedValues(teamId);
        for (let team of teamIdArray) {
          let newTSM = await dbTSM.teamschedulemap.create({
            businessId: dbBusinessId,
            scheduleId: newSchedule.scheduleId,
            teamId: team
          });
        }
      }

      //sms and email
      let clients = await dbClients.clients.findAll({
        where: {
          clientId: clientId,
        },
        raw: true
      });

      let text = "Hi " + clients[0].clientName + ", thank you for rescheduling your Excused booking for " + session[0]["pricepackName"] + " from " + rescheduleFrom + " to " + rescheduleTo + " , " + startTime + " to " + endTime + ". For any other queries, please contact +91 9812345678.";
      sendSmsMobile(clients[0].contactNumber, text);

      if (clients[0].emailId) {



        let sender = await BusinessSchema.Business().find({
          attributes: ["businessName"],
          where: {
            businessId: dbBusinessId
          },
          raw: true
        });

        let subject = "Senders " + clients[0].clientName + ": " + sender.businessName + " Excused booking rescheduled to 15th June 2018, 4pm";
        let contents = "Hi " + clients[0].clientName + ",Thank you for rescheduling your Excused booking for " + session[0]["pricepackName"] + " from " + rescheduleFrom + " to " + rescheduleTo + ", " + startTime + " to " + endTime + ". For any help or assistance, reach out to us anytime at +91 9812345678 Kind regards, " + sender.businessName;
        sendEmail(clients[0].emailId, subject, contents);

      }

      //Log Entry......
      let NotificationText;
      if (checkAdmin[1].UserStatus == 2) {
        let employees = await dbEmployee.employees.findAll({
          where: {
            employeeId: adminId
          },
          raw: true
        });

        let clients = await dbClients.clients.findAll({
          where: {
            clientId: clientId,
          },
          raw: true
        });

        NotificationText = employees[0].employeeName + ' just rescheduled ' + clients[0].clientName + ' for a booking on ' + rescheduleTo;
      } else {
        NotificationText = "";
      }

      const newLog = {
        businessId: dbBusinessId,
        activity: {
          header: 'Client Rescheduled',
          Status: 2,
          scheduleId,
          ScheduleId: scheduleId,
          ClientId: clientId,
          RescheduledFrom: rescheduleFrom,
          RescheduledTo: rescheduleTo,
          NewScheduleId: newScheduleId,
          RescheduledBy: adminId,
          NotificationText: NotificationText,
          UserStatus: checkAdmin[1].UserStatus,
          activityDate: new Date(),
          activityTime: new Date(),
          message: 'Client Rescheduled',
        },
        referenceTable: 'clientScheduleMap',
      };
      let dbLog = createInstance(["log"], information);
      let log = await dbLog.log.create(newLog);
      // End Log


      return await updateClientSchedule;
      dbTSM.sequelize.close();
      dbCSM.sequelize.close();
      dbSchedule.sequelize.close();
    } catch (error) {
      throw error;
    }
  }

  // Client Bookings
  clientBooking = async (
    information,
    clientId,
    searchKey
  ) => {
    try {
      //(searchKey[0], searchKey[1], searchKey[2]);
      let paginate = searchKey[0],
        filter = searchKey[1],
        search = searchKey[2];

      let pageNo = parseInt(paginate);
      let showLimit;
      if (pageNo > 0) {
        pageNo = parseInt(paginate);
        showLimit = 2; //show 2 records per page
      } else {
        pageNo = 1;
        showLimit = 1000; //show first 1000 records if no page number is given
      }

      let bookings = [];
      let setObj = { clientId: clientId, status: 1 };
      if (filter == 1) { //ongoing subscription
        bookings = await ongoingSubSchedule(information, setObj);
      } else if (filter == 2) { //past subscription
        bookings = await pastSubSchedule(information, setObj);
      } else if (filter == 3) { //current week
        bookings = await currentWeekSchedule(information, setObj);
      } else if (filter == 4) { //current month
        bookings = await currentMonthSchedule(information, setObj);
      }

      //pagination & search
      let data = await rsPaginate(bookings, pageNo, showLimit);
      let resultSet = [];
      if (search.trim() == '') {
        resultSet = data;
      } else {
        for (let i = 0; i < data.length; i++) {
          let schedule_name = data[i]['bookingName'].toLowerCase();
          if (schedule_name.includes(search.toLowerCase())) {
            resultSet = resultSet.concat(data[i]);
          }
        }
      }

      return await resultSet;
    } catch (error) {
      throw error;
    }
  }

  // Client Booking Details
  clientBookingDetails___03_07 = async (
    information,
    clientId,
    scheduleId,
    searchKey
  ) => {
    try {
      //declaration
      let dbSchedule = createInstance(["clientSchedule"], information);
      let dbCSM = createInstance(["clientScheduleMap"], information);
      let dbServices = createInstance(["clientService"], information);
      let dbSubscription = createInstance(["clientSubscription"], information);
      let dbPricepack = createInstance(["clientPricepack"], information);
      dbCSM.clientschedulemap.belongsTo(dbSchedule.schedule, {
        foreignKey: "scheduleId",
        targetKey: "scheduleId"
      });
      dbSchedule.schedule.belongsTo(dbServices.services, {
        foreignKey: "serviceId",
        targetKey: "serviceId"
      });
      dbCSM.clientschedulemap.belongsTo(dbSubscription.subscriptions, {
        foreignKey: "subscriptionId",
        targetKey: "subscriptionId"
      });
      dbSubscription.subscriptions.belongsTo(dbPricepack.pricepacks, {
        foreignKey: "pricepacks",
        targetKey: "pricepackId"
      });

      function compare(a, b) {
        if (new Date(a.scdate) < new Date(b.scdate))
          return -1;
        if (new Date(a.scdate) > new Date(b.scdate))
          return 1;
        return 0;
      }
      let today = new Date();
      let paginate = searchKey[0];

      //validation
      const { Op } = require('sequelize');
      let validate = await dbCSM.clientschedulemap.findAll({
        where: {
          status: 1,
          scheduleId: scheduleId,
          //parentScheduleId: scheduleId,
          clientId: clientId,
        },
        raw: true
      });
      if (validate.length < 1) {
        throw new ApplicationError(
          "Value Mismatch",
          401
        );
      }

      //fetching data
      let clientScheduleMap = await dbCSM.clientschedulemap.findAll({
        where: {
          status: 1,
          scheduleId: scheduleId,
          clientId: clientId
        },
        include: [{
          model: dbSchedule.schedule,
          include: [{
            model: dbServices.services,
            attributes: ['serviceName']
          }

          ]
        },
        {
          model: dbSubscription.subscriptions,
          attributes: ['subscriptionId'],
          include: [{
            model: dbPricepack.pricepacks,
            attributes: ['serviceDuration']
          }]
        }
        ],
        raw: true
      });

      //calculations
      let booking_name = clientScheduleMap[0]['schedule.scheduleName'] === null ?
        clientScheduleMap[0]['schedule.service.serviceName'] : clientScheduleMap[0]['schedule.scheduleName'];
      let schedule_days = [];
      let weekdays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
      let repeat_days = clientScheduleMap[0]['schedule.repeatation_type'] == 2 ?
        await commaSeparatedValues(clientScheduleMap[0]['schedule.repeat_days']) : [];
      repeat_days.forEach((day) => {
        schedule_days.push(weekdays[day]);
      });
      // let all_session_info = await sessionInfo(information, { clientId: clientId });
      // let get_session = all_session_info.filter((session) => {
      //   if (session.scheduleId == scheduleId && session.clientId == clientId) {
      //     return session;
      //   } else return false;
      // });
      // console.log("all_session_info");
      // console.log(all_session_info);
      // console.log("get_session");
      // console.log(get_session);

      let upcoming_bookings = [];
      for (let csm of clientScheduleMap) {
        for (let atd of csm.attendanceType) {
          let datetime = new Date(atd.scdate + ' ' + atd.scheduleStartTime);
          if ((new Date(datetime)) > (new Date(today)) && atd.reschedule == 1) {
            upcoming_bookings.push(atd);
          }
        }
      }
      upcoming_bookings.sort(compare);

      //pagination
      let pageNo = parseInt(paginate);
      let showLimit;
      if (pageNo > 0) {
        pageNo = parseInt(paginate);
        showLimit = PEGINATION_LIMIT; //show 2 records per page
      } else {
        pageNo = 1;
        showLimit = 1000; //show first 1000 records if no page number is given
      }
      let data = await rsPaginate(upcoming_bookings, pageNo, showLimit);

      let setObj = { status: 1, subscriptionId: clientScheduleMap[0].subscriptionId };
      let session = await sessionInfo(information, setObj);
      let sessionInformation = session[0].attendedSession + '/' + session[0].totalSession;

      //formatting the data
      let resultSet = [{
        header: {
          clientId: clientId,
          scheduleId: scheduleId,
          bookingName: booking_name,
          serviceName: clientScheduleMap[0]['schedule.service.serviceName'],
          startDate: clientScheduleMap[0]['schedule.scheduleDate'],
          endDate: clientScheduleMap[0]['schedule.repeat_ends_date'],
          startTime: clientScheduleMap[0]['schedule.startTime'],
          endTime: clientScheduleMap[0]['schedule.endTime'],
          scheduleDays: schedule_days.toString(),
          // sessionInfo: get_session[0].attendedSession + '/' + get_session[0].totalSession
          // sessionInfo: clientScheduleMap[0].countSession + '/' + clientScheduleMap[0]['subscriptions.pricepacks.serviceDuration']
          //sessionInfo: clientScheduleMap[0].sessionCount + '/' + clientScheduleMap[0]['subscription.pricepack.serviceDuration']
          sessionInfo: sessionInformation
        },
        upcomingBookings: data
      }];

      //disconnect & return
      dbSchedule.sequelize.close();
      dbCSM.sequelize.close();
      dbServices.sequelize.close();
      dbSubscription.sequelize.close();
      dbPricepack.sequelize.close();
      return await resultSet;
    } catch (error) {
      throw error;
    }
  }

  clientBookingDetails = async (
    information,
    clientId,
    scheduleId,
    searchKey
  ) => {
    try {
      //declaration
      let dbSchedule = createInstance(["clientSchedule"], information);
      let dbCSM = createInstance(["clientScheduleMap"], information);
      let dbServices = createInstance(["clientService"], information);
      let dbSubscription = createInstance(["clientSubscription"], information);
      let dbPricepack = createInstance(["clientPricepack"], information);
      dbCSM.clientschedulemap.belongsTo(dbSchedule.schedule, {
        foreignKey: "scheduleId",
        targetKey: "scheduleId"
      });
      dbSchedule.schedule.belongsTo(dbServices.services, {
        foreignKey: "serviceId",
        targetKey: "serviceId"
      });
      dbCSM.clientschedulemap.belongsTo(dbSubscription.subscriptions, {
        foreignKey: "subscriptionId",
        targetKey: "subscriptionId"
      });
      dbSubscription.subscriptions.belongsTo(dbPricepack.pricepacks, {
        foreignKey: "pricepacks",
        targetKey: "pricepackId"
      });

      function compare(a, b) {
        if (new Date(a.scdate) < new Date(b.scdate))
          return -1;
        if (new Date(a.scdate) > new Date(b.scdate))
          return 1;
        return 0;
      }
      let today = new Date();
      let paginate = searchKey[0];

      //validation
      const { Op } = require('sequelize');
      let validate = await dbCSM.clientschedulemap.findAll({
        where: {
          status: 1,
          scheduleId: scheduleId,
          //parentScheduleId: scheduleId,
          clientId: clientId,
        },
        raw: true
      });
      if (validate.length < 1) {
        throw new ApplicationError(
          "Value Mismatch",
          401
        );
      }

      //fetching data
      let clientScheduleMap = await dbCSM.clientschedulemap.findAll({
        where: {
          status: 1,
          scheduleId: scheduleId,
          clientId: clientId
        },
        include: [{
          model: dbSchedule.schedule,
          include: [{
            model: dbServices.services,
            attributes: ['serviceName']
          }

          ]
        },
        {
          model: dbSubscription.subscriptions,
          attributes: ['subscriptionId'],
          include: [{
            model: dbPricepack.pricepacks,
            attributes: ['serviceDuration']
          }]
        }
        ],
        raw: true
      });

      //calculations
      let booking_name = clientScheduleMap[0]['schedule.scheduleName'] === null ?
        clientScheduleMap[0]['schedule.service.serviceName'] : clientScheduleMap[0]['schedule.scheduleName'];
      let schedule_days = [];
      let weekdays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
      let repeat_days = clientScheduleMap[0]['schedule.repeatation_type'] == 2 ?
        await commaSeparatedValues(clientScheduleMap[0]['schedule.repeat_days']) : [];
      repeat_days.forEach((day) => {
        schedule_days.push(weekdays[day - 1]);
      });
      // let all_session_info = await sessionInfo(information, { clientId: clientId });
      // let get_session = all_session_info.filter((session) => {
      //   if (session.scheduleId == scheduleId && session.clientId == clientId) {
      //     return session;
      //   } else return false;
      // });
      // console.log("all_session_info");
      // console.log(all_session_info);
      // console.log("get_session");
      // console.log(get_session);

      let upcoming_bookings = [];
      for (let csm of clientScheduleMap) {
        for (let atd of csm.attendanceType) {
          let datetime = new Date(atd.scdate + ' ' + atd.scheduleStartTime);
          if ((new Date(datetime)) > (new Date(today)) && atd.reschedule == 1) {
            upcoming_bookings.push(atd);
          }
        }
      }
      upcoming_bookings.sort(compare);

      //pagination
      let pageNo = parseInt(paginate);
      let showLimit;
      if (pageNo > 0) {
        pageNo = parseInt(paginate);
        showLimit = PEGINATION_LIMIT; //show 2 records per page
      } else {
        pageNo = 1;
        showLimit = 1000; //show first 1000 records if no page number is given
      }
      let data = await rsPaginate(upcoming_bookings, pageNo, showLimit);

      let setObj = { status: 1, subscriptionId: clientScheduleMap[0].subscriptionId };
      let session = await sessionInfo(information, setObj);
      let sessionInformation = session[0].durationType == 1 ? session[0].attendedSession + '/' + session[0].totalSession : "" + session[0].attendedSession;
      let sessionMessage = session[0].message;
      //formatting the data
      let resultSet = [{
        header: {
          clientId: clientId,
          scheduleId: scheduleId,
          bookingName: booking_name,
          serviceName: clientScheduleMap[0]['schedule.service.serviceName'],
          startDate: clientScheduleMap[0]['schedule.scheduleDate'],
          endDate: clientScheduleMap[0]['schedule.repeat_ends_date'],
          startTime: clientScheduleMap[0]['schedule.startTime'],
          endTime: clientScheduleMap[0]['schedule.endTime'],
          scheduleDays: schedule_days.toString(),
          // sessionInfo: get_session[0].attendedSession + '/' + get_session[0].totalSession
          // sessionInfo: clientScheduleMap[0].countSession + '/' + clientScheduleMap[0]['subscriptions.pricepacks.serviceDuration']
          //sessionInfo: clientScheduleMap[0].sessionCount + '/' + clientScheduleMap[0]['subscription.pricepack.serviceDuration']
          sessionInfo: sessionInformation,
          sessionMessage: sessionMessage
        },
        upcomingBookings: data
      }];

      //disconnect & return
      dbSchedule.sequelize.close();
      dbCSM.sequelize.close();
      dbServices.sequelize.close();
      dbSubscription.sequelize.close();
      dbPricepack.sequelize.close();
      return await resultSet;
    } catch (error) {
      throw error;
    }
  }

  // Client Edit Booking
  // Now Client Edit booking working on  'getAttendanceJSON' method
  Client_Edit_booking_01_08 = async (
    information,
    scheduleId,
    clientId,
    businessId,
    teamId,
    scheduleName,
    scheduleDate,
    startTime,
    endTime,
    scheduleFromDate,
    centerId,
    is_repeat,
    repeatation_type,
    monthly_type,
    repeat_number,
    repeat_days,
    repeat_ends,
    repeat_ends_date,
    numberOfClients,
    comments,
    scheduleTo
  ) => {
    try {
      let dbBusiness = createInstance(["clientBusiness"], information);
      let dbSchedule = createInstance(["clientSchedule"], information);
      let dbCSM = createInstance(["clientScheduleMap"], information);
      var dbTSM = createInstance(["teamScheduleMap"], information);
      let dbSubscription = createInstance(["clientSubscription"], information);
      let dbPricepack = createInstance(["clientPricepack"], information);
      dbCSM.clientschedulemap.belongsTo(dbSubscription.subscriptions, {
        foreignKey: "subscriptionId",
        targetKey: "subscriptionId"
      });
      dbSubscription.subscriptions.belongsTo(dbPricepack.pricepacks, {
        foreignKey: "pricepacks",
        targetKey: "pricepackId"
      });

      let findSchedule = await dbSchedule.schedule.find({
        where: {
          scheduleId: scheduleId,
          status: 1
        },
        raw: true
      });

      if (findSchedule == null) {
        throw new ApplicationError(
          "schedule is not found",
          401
        );
      } else {
        let checkSchedule = await dbSchedule.schedule.findAll({
          where: {
            scheduleDate: scheduleDate,
            startTime: startTime,
            endTime: endTime,
            centerId: centerId,
            status: 1
          },
          raw: true
        });
        if (checkSchedule.length > 0) {
          throw new ApplicationError("Conflicting Date and Time", 409);
        } else if (!checkSchedule[0]) {
          let clients = await dbCSM.clientschedulemap.findAll({
            where: {
              status: 1,
              scheduleId: scheduleId,
              clientId: clientId
            },
            raw: true
          });
          if (clients.length == 0) {
            throw new ApplicationError("No such Schedule and Client Exists", 401);
          }
          let data = clients[0]["attendanceType"];
          for (let i = 0; i < data.length; i++) {
            if (+new Date(data[i]["scdate"]) >= +new Date(scheduleFromDate)) {
              if (data[i]["atd"] != 4) {
                throw new ApplicationError("Cannot edit Schedule from the Date as activity detected later dates", 401);
              }
            }
          }
          let data_new_Attendance = [];
          for (let i = 0; i < data.length; i++) {
            if (+new Date(data[i]["scdate"]) < +new Date(scheduleFromDate)) {
              data_new_Attendance.push(data[i]);
            }
          }

          await dbCSM.clientschedulemap.update({
            attendanceType: data_new_Attendance
          }, {
              where: {
                status: 1,
                scheduleId: scheduleId,
                clientId: clientId
              },
              raw: true
            });

          let csm = await dbCSM.clientschedulemap.findAll({
            where: {
              status: 1,
              scheduleId: scheduleId,
              parentScheduleId: scheduleId,
              masterParentScheduleId: scheduleId,
              clientId: clientId
            },
            include: [
              {
                model: dbSubscription.subscriptions,
                include: [
                  {
                    model: dbPricepack.pricepacks
                  }
                ]
              }
            ],
            raw: true
          });

          var teamIdArray = await commaSeparatedValues(teamId);
          let final_repeat_days = await getRepeatDays(repeat_days);
          var newScheduleID;
          let ObjToUpdate = {};

          if (scheduleTo == "") {
            if (+new Date(scheduleFromDate) < +new Date(scheduleDate)) {
              throw new ApplicationError(" ScheduleDate should be less than ScheduleFromDate", 401);
            }

            let repeatEndsDate, RepeatEndsDateHyphen;
            if (repeat_ends == 1) {
              repeatEndsDate = new Date(repeat_ends_date);
            } else if (repeat_ends == 2) {
              repeatEndsDate = new Date(scheduleDate);
              repeatEndsDate.setFullYear(repeatEndsDate.getFullYear() + 5);
            } else {
              throw new ApplicationError(
                "Error in repeat_ends",
                401
              );
            }
            RepeatEndsDateHyphen = await hyphenDateFormat(repeatEndsDate);

            let business = await dbBusiness.businessinfo.find({
              attributes: ["business_days"],
              where: {
                status: 1,
                businessId: businessId
              },
              raw: true
            });
            if (business.business_days == undefined || business.business_days == null || business.business_days.trim() == "") {
              throw new ApplicationError(
                "Please Add Business Days to Proceed !",
                401
              );
            }
            var bdays = await commaSeparatedValues(business.business_days);
            if (is_repeat == 1 && repeatation_type == 2) {
              let repeatDays = await commaSeparatedValues(final_repeat_days);
              for (let rd of repeatDays) {
                if (bdays.includes(rd) == false) {
                  throw new ApplicationError(
                    "Error in repeat_days. given days and business days are different !",
                    401
                  );
                }
              }
            }

            let scheduleTimeTable = await getScheduleTimeTable(
              startTime,
              endTime,
              scheduleDate,
              centerId,
              is_repeat,
              repeatation_type,
              monthly_type,
              repeat_number,
              final_repeat_days,
              repeat_ends,
              RepeatEndsDateHyphen,
              bdays
            );

            if (typeof scheduleTimeTable == 'undefined') {
              throw new ApplicationError(
                "Please enter valid data",
                409
              );
            }

            let DateToUpdate = ((new Date(csm[0]['subscription.subscriptionDateTime']) > new Date(scheduleFromDate)) ? new Date(csm[0]['subscription.subscriptionDateTime']) : new Date(scheduleFromDate));
            let subscriptionExpiry = csm[0]['subscription.subscriptionDateTime'].getTime() + (csm[0]['subscription.pricepack.pricepackValidity'] * 24 * 60 * 60 * 1000);
            let atdType = await getAttendanceSheet(scheduleTimeTable, DateToUpdate, subscriptionExpiry);

            //Creating new Schedule
            let schedules = await dbSchedule.schedule.create({
              scheduleDate: scheduleDate,
              scheduleName: scheduleName,
              startTime: startTime,
              endTime: endTime,
              centerId: centerId,
              numberOfClients: numberOfClients,
              comments: comments,
              scheduleTime: scheduleTimeTable,
              repeat_ends_date: repeatEndsDate,
              parent_scheduleId: findSchedule.parent_scheduleId,
              businessId: businessId,
              serviceId: findSchedule.serviceId,
              is_repeat: is_repeat,
              repeatation_type: repeatation_type,
              repeat_number: repeat_number,
              repeat_days: final_repeat_days,
              repeat_ends: repeat_ends,
              updateDateTime: Date.now(),

              raw: true
            });

            newScheduleID = schedules.scheduleId;

            ObjToUpdate = {
              businessId: businessId,
              scheduleId: newScheduleID,
              clientId: clientId,
              attendanceType: atdType,
              subscriptionId: clients[0].subscriptionId,
              parentScheduleId: scheduleId,
              masterParentScheduleId: clients[0].masterParentScheduleId
            };

            for (let element of teamIdArray) {
              await dbTSM.teamschedulemap.create({
                businessId: businessId,
                scheduleId: newScheduleID,
                teamId: element
              });
            }
          } else {
            let findSchedule_DetailsOfScheduleTo = await dbSchedule.schedule.findAll({
              where: {
                scheduleId: scheduleTo,
                status: 1
              },
              raw: true
            });

            if (findSchedule_DetailsOfScheduleTo.length == 0) {
              throw new ApplicationError("cannot find the ScheduleTo Schedule", 401);
            }

            let DateToUpdate = ((new Date(csm[0]['subscription.subscriptionDateTime']) > new Date(scheduleFromDate)) ? new Date(csm[0]['subscription.subscriptionDateTime']) : new Date(scheduleFromDate));
            let subscriptionExpiry = csm[0]['subscription.subscriptionDateTime'].getTime() + (csm[0]['subscription.pricepack.pricepackValidity'] * 24 * 60 * 60 * 1000);
            let atdType = await getAttendanceSheet(findSchedule_DetailsOfScheduleTo[0].scheduleTime, DateToUpdate, subscriptionExpiry);

            ObjToUpdate = {
              businessId: businessId,
              scheduleId: scheduleTo,
              clientId: clientId,
              attendanceType: atdType,
              subscriptionId: clients[0].subscriptionId,
              parentScheduleId: scheduleId,
              masterParentScheduleId: clients[0].masterParentScheduleId
            };
            newScheduleID = scheduleTo;
          }
          await dbCSM.clientschedulemap.create(ObjToUpdate);
          let updated_data = await dbSchedule.schedule.findAll({
            where: {
              scheduleId: newScheduleID
            }
          });

          dbSchedule.sequelize.close();
          dbTSM.sequelize.close();
          dbCSM.sequelize.close();
          dbSubscription.sequelize.close();
          dbPricepack.sequelize.close();
          dbBusiness.sequelize.close();
          return await updated_data;
        } else {
          throw new ApplicationError(
            "Not Updated",
            409
          );
        }
      }
    } catch (error) {
      throw error;
    }
  };

  Client_Edit_booking = async (
    information,
    scheduleId,
    clientId,
    businessId,
    teamId,
    scheduleName,
    scheduleDate,
    startTime,
    endTime,
    scheduleFromDate,
    centerId,
    is_repeat,
    repeatation_type,
    monthly_type,
    repeat_number,
    repeat_days,
    repeat_ends,
    repeat_ends_date,
    numberOfClients,
    comments,
    scheduleTo
  ) => {
    try {
      let dbBusiness = createInstance(["clientBusiness"], information);
      let dbSchedule = createInstance(["clientSchedule"], information);
      let dbCSM = createInstance(["clientScheduleMap"], information);
      var dbTSM = createInstance(["teamScheduleMap"], information);
      let dbSubscription = createInstance(["clientSubscription"], information);
      let dbPricepack = createInstance(["clientPricepack"], information);
      dbCSM.clientschedulemap.belongsTo(dbSubscription.subscriptions, {
        foreignKey: "subscriptionId",
        targetKey: "subscriptionId"
      });
      dbSubscription.subscriptions.belongsTo(dbPricepack.pricepacks, {
        foreignKey: "pricepacks",
        targetKey: "pricepackId"
      });

      let findSchedule = await dbSchedule.schedule.find({
        where: {
          scheduleId: scheduleId,
          status: 1
        },
        raw: true
      });

      if (findSchedule == null) {
        throw new ApplicationError(
          "schedule is not found",
          401
        );
      } else {
        let checkSchedule = await dbSchedule.schedule.findAll({
          where: {
            scheduleDate: scheduleDate,
            startTime: startTime,
            endTime: endTime,
            centerId: centerId,
            status: 1
          },
          raw: true
        });
        if (checkSchedule.length > 0) {
          throw new ApplicationError("Conflicting Date and Time", 409);
        } else if (!checkSchedule[0]) {
          let clients = await dbCSM.clientschedulemap.findAll({
            where: {
              status: 1,
              scheduleId: scheduleId,
              clientId: clientId
            },
            raw: true
          });
          if (clients.length == 0) {
            throw new ApplicationError("No such Schedule and Client Exists", 401);
          }
          let duplicateClients = await dbCSM.clientschedulemap.findAll({
            where: {
              status: 1,
              scheduleId: scheduleTo,
              clientId: clientId
            },
            raw: true
          });
          if (duplicateClients.length > 0) {
            throw new ApplicationError("Client already exists in scheduleTo schedule", 401);
          } else {
            let data = clients[0]["attendanceType"];
            for (let i = 0; i < data.length; i++) {
              if (+new Date(data[i]["scdate"]) >= +new Date(scheduleFromDate)) {
                if (data[i]["atd"] != 4) {
                  throw new ApplicationError("Cannot edit Schedule from the Date as activity detected later dates", 401);
                }
              }
            }
            let data_new_Attendance = [];
            for (let i = 0; i < data.length; i++) {
              if (+new Date(data[i]["scdate"]) < +new Date(scheduleFromDate)) {
                data_new_Attendance.push(data[i]);
              }
            }
            if (clients[0].clientEditFlag == 1) {
              throw new ApplicationError("Already updated !", 401);
            } else {
              let csm = await dbCSM.clientschedulemap.findAll({
                where: {
                  status: 1,
                  scheduleId: scheduleId,
                  parentScheduleId: scheduleId,
                  masterParentScheduleId: scheduleId,
                  clientId: clientId
                },
                include: [
                  {
                    model: dbSubscription.subscriptions,
                    include: [
                      {
                        model: dbPricepack.pricepacks
                      }
                    ]
                  }
                ],
                raw: true
              });

              var teamIdArray = await commaSeparatedValues(teamId);
              let final_repeat_days = await getRepeatDays(repeat_days);
              var newScheduleID;
              let ObjToUpdate = {};

              if (scheduleTo == "") {
                if (+new Date(scheduleFromDate) < +new Date(scheduleDate)) {
                  throw new ApplicationError("ScheduleDate should be less than ScheduleFromDate", 401);
                }

                let repeatEndsDate, RepeatEndsDateHyphen;
                if (repeat_ends == 1) {
                  repeatEndsDate = new Date(repeat_ends_date);
                } else if (repeat_ends == 2) {
                  repeatEndsDate = new Date(scheduleDate);
                  repeatEndsDate.setFullYear(repeatEndsDate.getFullYear() + 5);
                } else {
                  throw new ApplicationError(
                    "Error in repeat_ends",
                    401
                  );
                }
                RepeatEndsDateHyphen = await hyphenDateFormat(repeatEndsDate);

                let business = await dbBusiness.businessinfo.find({
                  attributes: ["business_days"],
                  where: {
                    status: 1,
                    businessId: businessId
                  },
                  raw: true
                });
                if (business.business_days == undefined || business.business_days == null || business.business_days.trim() == "") {
                  throw new ApplicationError(
                    "Please Add Business Days to Proceed !",
                    401
                  );
                }
                var bdays = await commaSeparatedValues(business.business_days);
                if (is_repeat == 1 && repeatation_type == 2) {
                  let repeatDays = await commaSeparatedValues(final_repeat_days);
                  for (let rd of repeatDays) {
                    if (bdays.includes(rd) == false) {
                      throw new ApplicationError(
                        "Error in repeat_days. given days and business days are different !",
                        401
                      );
                    }
                  }
                }

                let scheduleTimeTable = await getScheduleTimeTable(
                  startTime,
                  endTime,
                  scheduleDate,
                  centerId,
                  is_repeat,
                  repeatation_type,
                  monthly_type,
                  repeat_number,
                  final_repeat_days,
                  repeat_ends,
                  RepeatEndsDateHyphen,
                  bdays
                );

                if (typeof scheduleTimeTable == 'undefined') {
                  throw new ApplicationError(
                    "Please enter valid data",
                    409
                  );
                }

                let DateToUpdate = ((new Date(csm[0]['subscription.subscriptionDateTime']) > new Date(scheduleFromDate)) ? new Date(csm[0]['subscription.subscriptionDateTime']) : new Date(scheduleFromDate));
                let subscriptionExpiry = csm[0]['subscription.subscriptionDateTime'].getTime() + (csm[0]['subscription.pricepack.pricepackValidity'] * 24 * 60 * 60 * 1000);
                let atdType = await getAttendanceSheet(scheduleTimeTable, DateToUpdate, subscriptionExpiry);

                //Creating new Schedule
                let schedules = await dbSchedule.schedule.create({
                  scheduleDate: scheduleDate,
                  scheduleName: scheduleName,
                  startTime: startTime,
                  endTime: endTime,
                  centerId: centerId,
                  numberOfClients: numberOfClients,
                  comments: comments,
                  scheduleTime: scheduleTimeTable,
                  repeat_ends_date: repeatEndsDate,
                  parent_scheduleId: findSchedule.parent_scheduleId,
                  businessId: businessId,
                  serviceId: findSchedule.serviceId,
                  is_repeat: is_repeat,
                  repeatation_type: repeatation_type,
                  repeat_number: repeat_number,
                  repeat_days: final_repeat_days,
                  repeat_ends: repeat_ends,
                  updateDateTime: Date.now(),

                  raw: true
                });

                newScheduleID = schedules.scheduleId;

                ObjToUpdate = {
                  businessId: businessId,
                  scheduleId: newScheduleID,
                  clientId: clientId,
                  attendanceType: atdType,
                  subscriptionId: clients[0].subscriptionId,
                  parentScheduleId: scheduleId,
                  masterParentScheduleId: clients[0].masterParentScheduleId
                };

                for (let element of teamIdArray) {
                  await dbTSM.teamschedulemap.create({
                    businessId: businessId,
                    scheduleId: newScheduleID,
                    teamId: element
                  });
                }
              } else {
                let findSchedule_DetailsOfScheduleTo = await dbSchedule.schedule.findAll({
                  where: {
                    scheduleId: scheduleTo,
                    status: 1
                  },
                  raw: true
                });

                if (findSchedule_DetailsOfScheduleTo.length == 0) {
                  throw new ApplicationError("cannot find the ScheduleTo Schedule", 401);
                }

                let DateToUpdate = ((new Date(csm[0]['subscription.subscriptionDateTime']) > new Date(scheduleFromDate)) ? new Date(csm[0]['subscription.subscriptionDateTime']) : new Date(scheduleFromDate));
                let subscriptionExpiry = csm[0]['subscription.subscriptionDateTime'].getTime() + (csm[0]['subscription.pricepack.pricepackValidity'] * 24 * 60 * 60 * 1000);
                let atdType = await getAttendanceSheet(findSchedule_DetailsOfScheduleTo[0].scheduleTime, DateToUpdate, subscriptionExpiry);

                ObjToUpdate = {
                  businessId: businessId,
                  scheduleId: scheduleTo,
                  clientId: clientId,
                  attendanceType: atdType,
                  subscriptionId: clients[0].subscriptionId,
                  parentScheduleId: scheduleId,
                  masterParentScheduleId: clients[0].masterParentScheduleId
                };
                newScheduleID = scheduleTo;
              }
              await dbCSM.clientschedulemap.update({
                attendanceType: data_new_Attendance,
                clientEditFlag: 1
              }, {
                  where: {
                    status: 1,
                    scheduleId: scheduleId,
                    clientId: clientId
                  },
                  raw: true
                });
              await dbCSM.clientschedulemap.create(ObjToUpdate);
              let updated_data = await dbSchedule.schedule.findAll({
                where: {
                  scheduleId: newScheduleID
                }
              });

              dbSchedule.sequelize.close();
              dbTSM.sequelize.close();
              dbCSM.sequelize.close();
              dbSubscription.sequelize.close();
              dbPricepack.sequelize.close();
              dbBusiness.sequelize.close();
              return await updated_data;
            }
          }
        } else {
          throw new ApplicationError(
            "Not Updated",
            409
          );
        }
      }
    } catch (error) {
      throw error;
    }
  };

  ClientAttendanceHistory = async (
    information,
    businessId,
    clientId,
    booking_date,
    schedule_time,
    searchKey,
    filterPrivateFields = true
  ) => {
    try {
      let attendance, session;

      let td = new Date().toISOString().split('T')[0];
      let today = new Date(td).getTime();
      let dbSchedule = createInstance(["clientSchedule"], information);
      let dbCSM = createInstance(["clientScheduleMap"], information);
      let dbServices = createInstance(["clientService"], information);
      let dbsubscriptions = createInstance(["clientSubscription"], information);
      let dbpricepack = createInstance(["clientPricepack"], information);

      dbCSM.clientschedulemap.belongsTo(dbSchedule.schedule, {
        foreignKey: "scheduleId",
        targetKey: "scheduleId"
      });
      dbSchedule.schedule.belongsTo(dbServices.services, {
        foreignKey: "serviceId",
        targetKey: "serviceId"
      });
      dbsubscriptions.subscriptions.belongsTo(dbpricepack.pricepacks, {
        foreignKey: "pricepacks",
        targetKey: "pricepackId"
      });

      dbCSM.clientschedulemap.belongsTo(dbsubscriptions.subscriptions, {
        foreignKey: "subscriptionId",
        targetKey: "subscriptionId"
      });


      let pageNo = parseInt(searchKey[0]);

      let orderBy;
      if ((searchKey["booking_date"] == 2) && (searchKey["schedule_time"] == 2)) {
        orderBy = [
          [{
            model: dbSchedule.schedule
          }, "scheduleDate", "ASC"],
          ["startTime"]
        ];
      } else if (searchKey["booking_date"] == 2 && searchKey["schedule_time"] == 1) {
        orderBy = [
          ["scheduleDate", "ASC"],
        ];
      } else if ((searchKey["booking_date"] == 2) && (searchKey["schedule_time"] == 0)) {
        orderBy = [
          ["scheduleDate", "ASC"],
        ];
      } else if ((searchKey["booking_date"] == 1) && (searchKey["schedule_time"] == 2)) {
        orderBy = [
          ["scheduleDate", "ASC"],
        ];
      } else {
        orderBy = [
          ["scheduleDate", "ASC"],
        ];
      }


      let schedules = await dbCSM.clientschedulemap.findAll({
        where: {
          status: 1,
          businessId: businessId,
          clientId: clientId
        },
        include: [{
          model: dbSchedule.schedule,
          include: [{
            model: dbServices.services,
            attributes: ['serviceName']
          }],
          order: orderBy,
        },
        {
          model: dbsubscriptions.subscriptions,
          include: [{
            model: dbpricepack.pricepacks
          }]
        }
        ],

        raw: true
      });



      //return schedules;
      let pendingSchedules = [];
      for (let i = 0; i < schedules.length; i++) {

        let getObj = {
          status: 1,
          subscriptionId: schedules[i]["subscription.subscriptionId"]
        }

        let scheduleArr = [];
        let scheduleObj = [];
        let LogicStatus = 0;
        let session = await sessionInfo(information, getObj);
        let attendance = schedules[i].attendanceType;
        let Present = 0;
        let Absent = 0;
        let Execused = 0;
        let Rescheduled = 0;
        let Status = 2;
        for (let j = 0; j < attendance.length; j++) {
          let checkDate = new Date(attendance[j]["scdate"]).getTime();

          if (checkDate <= today) {

            if (attendance[j]["atd"] == 1 || attendance[j]["atd"] == 2 || attendance[j]["atd"] == 3 || attendance[j]["atd"] == 6 || attendance[j]["atd"] == 7) {

              if (LogicStatus == 0) {
                var myDate = new Date(schedules[i]["subscription.subscriptionDateTime"]);
                myDate.setDate(myDate.getDate() + schedules[i]["subscription.pricepack.pricepackValidity"]);

                Array.prototype.push.apply(scheduleArr, [{
                  scheduleId: schedules[i]["schedule.scheduleId"]
                }]);
                schedules[i]['bookingName'] = schedules[i]["schedule.scheduleName"] === null ? schedules[i]['schedule.service.serviceName'] : schedules[i]["schedule.scheduleName"];
                Array.prototype.push.apply(scheduleArr, [{
                  scheduleName: schedules[i]["bookingName"]
                }]);
                Array.prototype.push.apply(scheduleArr, [{
                  PricepackName: schedules[i]['subscription.pricepack.pricepackName']
                }]);
                Array.prototype.push.apply(scheduleArr, [{
                  PricepackExpiryDate: myDate
                }]);
                Array.prototype.push.apply(scheduleArr, [{
                  serviceName: schedules[i]['schedule.service.serviceName']
                }]);
                Array.prototype.push.apply(scheduleArr, [{
                  scheduleStartTime: schedules[i]["schedule.startTime"]
                }]);
                Array.prototype.push.apply(scheduleArr, [{
                  scheduleEndTime: schedules[i]["schedule.endTime"]
                }]);

                LogicStatus = 1;

              }
              //Count the no of attendances
              if (attendance[j]["atd"] == 1) {
                Present++;
                Status = 0;
              } else if (attendance[j]["atd"] == 2) {
                Absent++;
                Status = 0;
              } else if (attendance[j]["atd"] == 3) {
                Execused++;
                Status = 0;
              } else if (attendance[j]["atd"] == 6 || attendance[j]["atd"] == 7) {
                Rescheduled++;
                Status = 0;
              }


            }

          }
          //
          if (j == attendance.length - 1 && LogicStatus == 1) {
            let Obj = {};
            let totalAtd = Present + Absent + Execused + Rescheduled;
            let perPresent = (Present == 0) ? 0 : Present;
            let perAbsent = (Absent == 0) ? 0 : Absent;
            Obj["Present"] = perPresent;
            Obj["Abscent"] = perAbsent;
            Obj["Execused"] = Execused;

            Array.prototype.push.apply(scheduleArr, [{
              Session: Obj
            }]);
            Array.prototype.push.apply(scheduleArr, [{
              SessionInfo: session
            }]);
            scheduleObj = Object.assign.apply(Object, scheduleArr);
            pendingSchedules = pendingSchedules.concat(scheduleObj);

          }
        }

      }
      //pagination & search
      let data = await rsPaginate(pendingSchedules, pageNo, PEGINATION_LIMIT);

      let resultSet = [];
      if (searchKey[1].trim() == '') {
        resultSet = data;
      } else {
        if (data.length > 0) {
          for (let i = 0; i < data.length; i++) {
            let schedule_name = data[i].scheduleName.toLowerCase();
            let service_name = data[i].serviceName.toLowerCase();
            if (schedule_name.includes(searchKey[1]) ||
              service_name.includes(searchKey[1])) {
              resultSet = resultSet.concat(data[i]);
            }
          }
        } else resultSet = [];
      }
      dbServices.sequelize.close();
      dbCSM.sequelize.close();
      dbSchedule.sequelize.close();
      dbsubscriptions.sequelize.close();
      dbpricepack.sequelize.close();
      return await resultSet;
    } catch (error) {
      throw error;
    }
  };

  // Schedule On SameService and Center
  ScheduleOnSameServiceandCenter = async (information,
    scheduleId,
    centerId) => {

    try {
      let getserviceId = scheduleId;
      let dbSchedule = createInstance(["clientSchedule"], information);
      let dbCSM = createInstance(["clientScheduleMap"], information);
      let dbCenter = createInstance(["clientCenter"], information);
      let dbClients = createInstance(["client"], information);
      let dbService = createInstance(["clientService"], information);
      dbSchedule.schedule.belongsTo(dbService.services, {
        foreignKey: "serviceId",
        targetKey: "serviceId"

      });
      dbSchedule.schedule.hasOne(dbCSM.clientschedulemap, {
        foreignKey: "scheduleId",
        targetKey: "scheduleId"
      });
      dbCSM.clientschedulemap.belongsTo(dbClients.clients, {
        foreignKey: "clientId",
        targetKey: "clientId"
      });
      const Op = Sequelize.Op;

      //provided schedule information
      // let givenSchedule = await dbSchedule.schedule.findAll({
      //   where: {
      //     scheduleId: scheduleId,
      //     status: 1
      //   },
      //   raw: true
      // });

      // if (givenSchedule.length == 0) {
      //   throw new ApplicationError(" no schedule data is found", 401);
      // }      

      let WhereCondition = {};
      if (centerId[0]) {
        WhereCondition = {
          status: 1,
          centerId: centerId[0],
          serviceId: getserviceId,
          //scheduleId: { [Op.notIn]: [scheduleId] },
        }
      } else {
        WhereCondition = {
          status: 1,
          serviceId: getserviceId,
          //scheduleId: { [Op.notIn]: [scheduleId] },
        }
      }

      //getting other schedules with the same serviceId of the given schedule
      let schedules = await dbSchedule.schedule.findAll({
        attributes: { exclude: ['scheduleTime'] },
        where: WhereCondition,
        include: [{
          model: dbService.services,
        }],
        raw: true
      });

      if (schedules.length == 0) { throw new ApplicationError("No such Schedule found", 401); }
      let mergedSchedules = [];
      for (let i = 0; i < schedules.length; i++) {
        let clients = await dbCSM.clientschedulemap.findAll({
          where: {
            scheduleId: schedules[i].scheduleId
          },
          include: [{ model: dbClients.clients }],
          raw: true
        });

        let Obj = {};
        let scheduleArr = [];

        Obj["schedule"] = schedules[i];
        let clientArr = [];
        //clientArr.push(Obj);
        for (let j = 0; j < clients.length; j++) {
          let Obj1 = {};
          Obj1["id"] = clients[j]["client.clientId"];
          Obj1["name"] = clients[j]["client.clientName"];
          let isphoto = (clients[j]["client.photoUrl"] === null) ? KNACK_UPLOAD_URL.localUrl + 'noimage.jpg' : KNACK_UPLOAD_URL.localUrl + clients[j]["client.photoUrl"];
          Obj1["photo"] = isphoto;
          clientArr.push(Obj1);
        }
        Obj["schedule"]["client"] = clientArr;
        mergedSchedules.push(Obj);
      }

      if (schedules.length == 0) {
        throw new ApplicationError("No data is found", 401);
      }
      dbCSM.sequelize.close();
      dbService.sequelize.close();
      dbClients.sequelize.close();
      dbCenter.sequelize.close();
      dbSchedule.sequelize.close();
      return mergedSchedules;
    } catch (error) {
      throw error;
    }
  };

  // For Notification

  unscheduledClientList = async (
    information,
    filterPrivateFields = true
  ) => {
    try {
      let businessid = information.split(",");
      var Sequelize = require('sequelize');
      let db = createInstance(["client"], information);
      let dbSub = createInstance(["clientSubscription"], information);
      let dbCSM = createInstance(["clientScheduleMap"], information);

      db.clients.belongsTo(dbSub.subscriptions, {
        foreignKey: "clientId",
        targetKey: "clientId"
      });

      db.clients.belongsTo(dbCSM.clientschedulemap, {
        foreignKey: "clientId",
        targetKey: "clientId",
        as: "ClientsScheduleMap"
      });


      var clients = await db.clients.findAll({
        where: {
          status: 1
        },
        raw: true
      });

      var Sequelize = require('sequelize');
      var arr = [];
      for (var i = 0; i < clients.length; i++) {
        let checkSchedule = await dbCSM.clientschedulemap.findAll({
          attributes: [
            [Sequelize.fn('DISTINCT', Sequelize.col('clientId')), 'client'],
          ],
          where: {
            clientId: clients[i].clientId,
            status: 1
          },
          raw: true
        });

        if (checkSchedule.length > 0) {
          arr.push(checkSchedule[0].client);
        }


      }
      var filteredClients = await db.clients.findAll({
        where: {
          clientId: {
            notIn: arr
          },
          status: 1
        },
        include: [{ model: dbSub.subscriptions, where: { status: 1 }, required: true }],
        raw: true
      });
      db.sequelize.close();
      dbSub.sequelize.close();
      dbCSM.sequelize.close();
      return filteredClients;
    } catch (error) {
      throw error;
    }
  };


  schedulesWithSameServicebutClient = async (
    information,
    scheduleId,
    centerId,
    date,
    clientId,
    filterPrivateFields = true
  ) => {
    try {

      let dbSchedule = createInstance(["clientSchedule"], information);
      let dbCSM = createInstance(["clientScheduleMap"], information);
      let dbCenter = createInstance(["clientCenter"], information);
      let dbClients = createInstance(["client"], information);
      let dbService = createInstance(["clientService"], information);
      dbSchedule.schedule.belongsTo(dbService.services, {
        foreignKey: "serviceId",
        targetKey: "serviceId"
      });
      dbSchedule.schedule.hasOne(dbCSM.clientschedulemap, {
        foreignKey: "scheduleId",
        targetKey: "scheduleId"
      });


      dbCSM.clientschedulemap.belongsTo(dbClients.clients, {
        foreignKey: "clientId",
        targetKey: "clientId"
      });
      //provided schedule information
      let givenSchedule = await dbSchedule.schedule.find({
        where: {
          scheduleId: scheduleId,
          status: 1
        },
        raw: true
      });

      //getting other schedules with the same serviceId of the given schedule except the UserId
      let schedules = await dbSchedule.schedule.findAll({
        where: {
          status: 1,
          centerId: centerId,
          serviceId: givenSchedule.serviceId,
        },
        include: [{
          model: dbService.services
        }],
        raw: true
      });

      let mergedSchedules = [];
      for (let schedule of schedules) {

        // for (let key of Object.keys(schedule.scheduleTime)) {
        for (let key of schedule.scheduleTime) {
          if (+(new Date(key.scdate)) === +(new Date(date))) {
            let sameRepetition = schedule.repeatation_type == givenSchedule.repeatation_type ? 1 : 0;
            let mergedScheduleArr = [];


            let clientSchedules = await dbCSM.clientschedulemap.findAll({
              where: {
                status: 1,
                scheduleId: schedule.scheduleId
              },
              include: [{
                model: dbClients.clients
              }],
              raw: true
            });
            let mergedClients = [];
            let ClientStatus = 0;
            for (let clientSchedule of clientSchedules) {

              // for (let atd of Object.keys(clientSchedule.attendanceType)) {
              for (let atd of clientSchedule.attendanceType) {
                if (+(new Date(atd.scdate)) === +(new Date(date))) {

                  let isphoto = (clientSchedule["client.photoUrl"] === null) ? KNACK_UPLOAD_URL.localUrl + 'noimage.jpg' : KNACK_UPLOAD_URL.localUrl + clientSchedule["client.photoUrl"];
                  if (clientSchedule["client.clientId"] == clientId) {
                    ClientStatus = 1;
                  }

                  mergedClients = mergedClients.concat({
                    clientId: clientSchedule["client.clientId"],
                    clientName: clientSchedule["client.clientName"],
                    clientphoto: isphoto
                  });
                }
              }
            }

            let bookingName;
            // Set Day and Time
            var stime = tConv24(schedule.startTime);
            var etime = tConv24(schedule.endTime);
            let stime = stime + '-' + etime;
            let dayint = convartday(schedule.repeat_days);
            let day_time = ', ' + dayint + ', ' + stime;
            // End

            if (schedule.scheduleName == '') {
              bookingName = schedule["service.serviceName"] + day_time;
            } else {
              bookingName = schedule["scheduleName"];
            }

            let today = new Date();
            let yesterday = today.getTime() - 24 * 60 * 60 * 1000;
            let tomorrow = today.getTime() + 24 * 60 * 60 * 1000;
            let scheduleDay;

            if ((new Date(date)).toLocaleDateString() == (new Date(today)).toLocaleDateString()) {
              scheduleDay = 'today';
            } else if ((new Date(date)).toLocaleDateString() == (new Date(yesterday)).toLocaleDateString()) {
              scheduleDay = 'yesterday';
            } else if ((new Date(date)).toLocaleDateString() == (new Date(tomorrow)).toLocaleDateString()) {
              scheduleDay = 'tomorrow';
            } else {
              scheduleDay = date;
            }

            // Array.prototype.push.apply(mergedScheduleArr, [schedule]);
            Array.prototype.push.apply(mergedScheduleArr, [{
              scheduleId: schedule.scheduleId
            }]);
            Array.prototype.push.apply(mergedScheduleArr, [{
              bookingName: bookingName
            }]);
            Array.prototype.push.apply(mergedScheduleArr, [{
              serviceName: schedule["service.serviceName"]
            }]);
            Array.prototype.push.apply(mergedScheduleArr, [{
              scheduleDate: scheduleDay
            }]);
            Array.prototype.push.apply(mergedScheduleArr, [{
              startTime: schedule.startTime
            }]);
            Array.prototype.push.apply(mergedScheduleArr, [{
              endTime: schedule.endTime
            }]);
            Array.prototype.push.apply(mergedScheduleArr, [{
              sameRepetition: sameRepetition
            }]);
            Array.prototype.push.apply(mergedScheduleArr, [{
              clients: mergedClients
            }]);
            Array.prototype.push.apply(mergedScheduleArr, [{
              clientsStatus: ClientStatus
            }]);

            if (schedule.scheduleId == scheduleId)
              Array.prototype.push.apply(mergedScheduleArr, [{
                selectedSchedule: 1
              }]);
            else
              Array.prototype.push.apply(mergedScheduleArr, [{
                selectedSchedule: 0
              }]);

            let mergedScheduleObj = [];
            mergedScheduleObj = Object.assign.apply(Object, mergedScheduleArr);
            mergedSchedules = mergedSchedules.concat(mergedScheduleObj);
          }
        }
      }

      dbCSM.sequelize.close();
      dbService.sequelize.close();
      dbClients.sequelize.close();
      dbCenter.sequelize.close();
      dbSchedule.sequelize.close();

      let result = [];
      for (let i = 0; i < mergedSchedules.length; i++) {
        if (mergedSchedules[i].clientsStatus == 0) {
          result.push(mergedSchedules[i]);
        }
      }
      return result;
    } catch (error) {
      throw error;
    }
  }

}

export default new ScheduleDB();