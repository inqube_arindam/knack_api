import { ApplicationError } from "../../lib/errors";
import BaseModel from "../BaseModel";
import { filterFields, createInstance } from "../../lib/common";
import {
    KNACKDB,
    KNACK_DBSETTINGS,
    KNACK_UPLOAD_URL,
    KNACK_TEAM_UPLOAD_URL,
    PEGINATION_LIMIT
} from "../../lib/constants.js";
import Sequelize from "sequelize";
import PaymentSchema from "../../schemas/payment.schema.js";
import PackagesSchema from "../../schemas/packages.schema.js";
const PUBLIC_FIELDS = [
    "centerId",
    "businessId",
    "centerName",
    "area",
    "pin",
    "city",
    "landmark",
    "contactNumber"
];

export class CenterDB extends BaseModel {
    constructor(connection) {
        //payment table
        super("payment", connection);
        this.schema = PaymentSchema.Payment();
        this.name = "payment";
        this.db = this.connection;
        this.model = this.connection.model(this.name, this.schema);

        //////////// packages table
        super("packages", connection);
        this.schema_package = PackagesSchema.Packages();
        this.name_package = "packages";
        this.db_package = this.connection;
        this.model_package = this.connection.model(
            this.name_package,
            this.schema_package
        );
    }

    centers = [];
    create = async (
        information,
        businessId,
        centerName,
        area,
        landmark,
        pin,
        city,
        contactNumber
    ) => {
        ///////////////////
        let arrayOfStrings = information.split(",");
        let userId = arrayOfStrings.length > 2 ? arrayOfStrings[2] : null;
        let db = createInstance(["clientCenter"], information);
        /// check number of centers
        let centers_number = await db.centers.findAndCountAll({
            where: {
                status: 1,
                businessId: businessId
            },
            raw: true
        });
        db.sequelize.close();
        var cn = centers_number.count;
        //console.log(cn);

        //// get payment info
        var client_payment_info = await this.model.findAll({
            where: {
                status: 1,
                businessId: businessId
            },
            raw: true
        });
        db.sequelize.close();
        // console.log(client_payment_info);
        var pckid = client_payment_info[0].packageId;
        //console.log(pckid);

        //// get package info
        var client_package_info = await this.model_package.findAll({
            where: {
                status: 1,
                packageId: pckid
            },
            raw: true
        });
        db.sequelize.close();
        var package_centers = client_package_info[0].numberOfCenters;
        //console.log(package_centers);
        const centerdetail = {
            businessId,
            centerName,
            area,
            landmark,
            pin,
            city,
            contactNumber,
            createdBy: userId
        };
        if (cn < package_centers) {
            //////////////////////
            try {
                let db = createInstance(["clientCenter"], information);

                let centers = await db.centers.create(centerdetail);
                db.sequelize.close();
                return await filterFields(centers, PUBLIC_FIELDS, true);
            } catch (error) {
                throw error;
            }
        } else {
            throw new ApplicationError(
                "Unable to add new center, please check package in details",
                401
            );
        }
    };

    listAll = async (information, businessId, filterPrivateFields = true) => {
        //get package information
        // var seqdb = new Sequelize(businessId, "root", "root", {
        //   host: KNACKDB.host,
        //   port: 3306,
        //   dialect: "mysql",
        //   pool: {
        //     max: 100,
        //     min: 0,
        //     idle: 10000
        //   }
        // });
        // let getpackageinfo = await seqdb.query(
        //   "SELECT * FROM `payment` AS `payment` WHERE `payment`.`businessId` = '" +
        //     businessId +
        //     "'"
        // );
        // console.log("custom query");
        // console.log(getpackageinfo);
        // seqdb.close();

        try {
            let db = createInstance(["clientCenter"], information);
            let centers = await db.centers.findAll({
                where: {
                    status: 1,
                    businessId: businessId
                },
                raw: true
            });

            db.sequelize.close();
            if (filterPrivateFields) {
                return await filterFields(centers, PUBLIC_FIELDS);
            }
        } catch (error) {
            throw error;
        }
    };

    centerDetails = async (information, centerId, filterPrivateFields = true) => {
        try {
            let db = createInstance(["clientCenter"], information);
            let centers = await db.centers.findAll({
                where: {
                    status: 1,
                    centerId: centerId
                },
                raw: true
            });
            db.sequelize.close();
            if (filterPrivateFields) {
                return await filterFields(centers, PUBLIC_FIELDS);
            }
        } catch (error) {
            throw error;
        }
    };

    update = async (
        information,
        centerId,
        businessId,
        centerName,
        area,
        landmark,
        pin,
        city,
        contactNumber
    ) => {
        try {
            let db = createInstance(["clientCenter"], information);
            let centers = await db.centers.update({
                businessId: businessId,
                centerName: centerName,
                area: area,
                landmark: landmark,
                pin: pin,
                city: city,
                contactNumber: contactNumber,
                updateDateTime: Date.now()
            }, {
                    where: {
                        centerId: centerId
                    }
                });
            db.sequelize.close();
            return centers;
        } catch (error) {
            throw error;
        }
    };

    remove = async (information, centerId, filterPrivateFields = true) => {
        try {
            let db = createInstance(["clientCenter"], information);
            let centers = await db.centers.update({
                status: 0,
                updateDateTime: Date.now()
            }, {
                    where: {
                        centerId: centerId
                    }
                });

            let update_centers = await db.centers.findAll({
                where: {
                    status: 1,
                },
                raw: true
            });

            db.sequelize.close();
            // return centers;
            if (filterPrivateFields) {
                return await filterFields(update_centers, PUBLIC_FIELDS);
            }
        } catch (error) {
            throw error;
        }
    };

    // List Team or Serive
    listTeamOrService = async (information, centerId, typeId, filterPrivateFields = true) => {
        try {
            var final = [];
            let getInfo = information.split(",");
            let dbBusinessId = getInfo[1]; //get db or business id from information

            let db = createInstance(["clientSchedule"], information);
            let schedules = await db.schedule.findAll({
                // attributes: [
                //   [Sequelize.fn('DISTINCT', Sequelize.col('centerId')) ,'centerId'],
                // ],
                where: {
                    businessId: dbBusinessId,
                    centerId: centerId,
                    status: 1
                },
                raw: true
            });

            if (typeId == 1) { //show employees
                for (var i = 0; i < schedules.length; i++) {
                    let dbTSM = createInstance(["teamScheduleMap"], information);
                    let checkSchedule = await dbTSM.teamschedulemap.findAll({
                        where: {
                            businessId: dbBusinessId,
                            scheduleId: schedules[i].scheduleId,
                            status: 1
                        },
                        raw: true
                    });
                    if (checkSchedule[0]) {
                        let dbEmp = createInstance(["clientEmployee"], information);
                        let employees = await dbEmp.employees.findAll({
                            where: {
                                status: 1,
                                businessId: dbBusinessId,
                                employeeId: checkSchedule[0].teamId
                            },
                            raw: true
                        });

                        var isphoto = (employees[0].photoUrl === null) ? 'noimage.jpg' : employees[0].photoUrl;
                        var teamphoto = KNACK_TEAM_UPLOAD_URL.localUrl + isphoto;

                        if (employees[0].roleId == 1) {
                            Array.prototype.push.apply(employees, [{ designation: "Main Admin" }]);
                        } else if (employees[0].roleId == 2) {
                            Array.prototype.push.apply(employees, [{ designation: "Center Admin" }]);
                        } else {
                            Array.prototype.push.apply(employees, [{ designation: "" }]);
                        }
                        Array.prototype.push.apply(employees, [{ scheduleId: checkSchedule[0].scheduleId }, { photoUrl: teamphoto }]);
                        var merged = [];
                        merged = Object.assign.apply(Object, employees);
                        final = final.concat(merged);
                    }
                }
            } else if (typeId == 2) { //show services
                let dbService = createInstance(["clientService"], information);
                for (var i = 0; i < schedules.length; i++) {
                    let services = await dbService.services.findAll({
                        where: {
                            status: 1,
                            businessId: dbBusinessId,
                            serviceId: schedules[i].serviceId
                        },
                        raw: true
                    });

                    var merged = [];
                    merged = Object.assign.apply(Object, services);
                    final = final.concat(merged);
                }
            }
            return await final;
        } catch (error) {
            throw error;
        }
    };

}
export default new CenterDB();