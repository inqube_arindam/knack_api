import {
    ApplicationError
} from "../../lib/errors";
import {
    KNACKDB,
    KNACK_DBSETTINGS,
    KNACK_UPLOAD_URL,
    PEGINATION_LIMIT
} from "../../lib/constants.js";
import {
    filterFields,
    createInstance,
    commaSeparatedValues,
    clientInfo,
    date_ISO_Convert,
    hyphenDateFormatByDate,
    make_date,
    getPaymentCalculation,
    hyphenDateFormat,
    formatDate,
    getPlusDays,
    getPlusMonths,
    makeEmailTemplate,
    sendEmail,
    sessionInfo
} from "../../lib/common";
import Sequelize from "sequelize";
import BaseModel from "../../db/BaseModel";
import BusinessSchema from "../../schemas/business.schema.js";

var fs = require("fs");
var path = require("path");
var pdf1 = require("html-pdf");

const PUBLIC_FIELDS = [
    "subscriptionId",
    "clientId",
    "client.clientName",
    "businessId",
    "centerId",
    "center.centerName",
    "services",
    "service.offeringName",
    "pricepacks",
    "pricepack.pricepackName",
    "pricepack.serviceName",
    //"service.serviceName",
    "totalSessionUnit",
    "totalSessionType",
    "totalAmount",
    "paymentType",
    "numberOfInstallments",
    "paymentDueDate",
    "subscriptionDateTime",
    "totalSessionsAttended",
    "totalAmountPaid",
    "payment_mode",
    "installment_frequency",
    "payment_date"
];

export class SubscriptionDB extends BaseModel {
    subscriptions = [];


    constructor(connection) {
        //Get Business Model Data
        super("businessinfo", connection);
        this.businessSchema = BusinessSchema.Business();
        this.businessName = "businessinfo";
        this.businessModel = this.connection.model(this.businessName, this.businessSchema);
    }

    create = async(
        information,
        clientId,
        businessId,
        pricepacks,
        payment_type,
        numberOfInstallments,
        installment_frequency,
        due_amount,
        installment_due_on,
        payment_due_date
    ) => {
        try {
            let dbSub = createInstance(["clientSubscription"], information);
            // Check same client and pricepack id is exist or not
            let clientIdArray = await commaSeparatedValues(clientId); //get client list
            for (let client of clientIdArray) {
                let checkSubscription = await dbSub.subscriptions.findAll({
                    where: {
                        businessId: businessId,
                        status: 1,
                        renewl_status: 1,
                        clientId: client,
                        pricepacks: pricepacks
                    },
                    raw: true
                });
                if (checkSubscription[0]) {
                    throw new ApplicationError("Clients '" + checkSubscription[0].clientId + "' already exist into this pricepack", 409);
                }
            }

            function addDays(theDate, days) {
                return new Date(theDate.getTime() + days * 24 * 60 * 60 * 1000);
            }

            var dateValue = 0,
                numberValue = 0;
            if (payment_due_date.includes("-")) {
                dateValue = new Date(payment_due_date);
            } else {
                numberValue = parseInt(payment_due_date);
            }

            if (installment_due_on == 1 && numberValue) {
                throw new ApplicationError(
                    "Error in payment_due_date and installment_due_on",
                    401
                );
            }

            if (installment_due_on == 2 && dateValue) {
                throw new ApplicationError(
                    "Error in payment_due_date and installment_due_on",
                    401
                );
            }

            if (installment_due_on == 3 && numberValue) {
                throw new ApplicationError(
                    "Error in payment_due_date and installment_due_on",
                    401
                );
            }

            if (due_amount < 0) {
                throw new ApplicationError("due_amount should be +ve", 401);
            }

            if (installment_frequency == 6 && installment_due_on == 3) {
                throw new ApplicationError("Please select specefic date", 401);
            }

            if (installment_due_on < 0 || installment_due_on > 3) {
                throw new ApplicationError(
                    "Error in installment_due_on. 1: specific date, 2: day after client joining date, 3: client joining date.",
                    401
                );
            }

            var subscriptionDateTime = new Date();
            var dbPayment = createInstance(["clientPayments"], information);
            var dbClient = createInstance(["client"], information);
            let dbPricepack = createInstance(["clientPricepack"], information);
            let dbservices = createInstance(["clientService"], information);
            let dbservicepricepackmap = createInstance(["servicePricepackMap"], information);
            dbservicepricepackmap.servicepricepackmap.belongsTo(
                dbPricepack.pricepacks, {
                    foreignKey: "pricepackId",
                    targetKey: "pricepackId"
                }
            );
            dbservicepricepackmap.servicepricepackmap.belongsTo(dbservices.services, {
                foreignKey: "serviceId",
                targetKey: "serviceId"
            });

            let isclientjoin = installment_due_on == 3 ? subscriptionDateTime : null;

            let invoice_number = Date.now() + Math.floor(Math.random() * 8999 + 1000);
            let subcriptiondate = subscriptionDateTime;

            // if(installment_frequency == 6){
            // subcriptiondate = subscriptionDateTime;
            // }else{
            // subcriptiondate = (installment_due_on == 3) ? subscriptionDateTime : payment_due_date;
            // }

            /* One Time Payment */
            if (payment_type == 1) {
                //one time payment
                var temp_payment_due_date = payment_due_date;
                //Get pricepack amount
                var pricepack = await dbPricepack.pricepacks.findAll({
                    where: {
                        //status: 1,
                        pricepackId: pricepacks
                    },
                    raw: true
                });

                let serviceAll = await dbservicepricepackmap.servicepricepackmap.findAll({
                    attributes: {
                        include: [],
                        exclude: [
                            "servicepricepackmapId",
                            "businessId",
                            "serviceDuration",
                            "createDateTime",
                            "updateDateTime",
                            "status"
                        ]
                    },
                    where: {
                        pricepackId: pricepacks
                    },
                    include: [{
                            model: dbservices.services,
                            attributes: {
                                include: [],
                                exclude: [
                                    "serviceId",
                                    "serviceName",
                                    "businessId",
                                    "serviceDetails",
                                    "createDateTime",
                                    "updateDateTime",
                                    "status"
                                ]
                            }
                        },
                        {
                            model: dbPricepack.pricepacks,
                            attributes: [
                                ["amount", "pricepack_amount"]
                            ]
                        }
                    ],
                    raw: true
                });
                dbservices.sequelize.close();
                dbservicepricepackmap.sequelize.close();
                let priceCalculation = await getPaymentCalculation(
                    information,
                    serviceAll[0]["pricepack.pricepack_amount"],
                    serviceAll[0].serviceId,
                    serviceAll[0].pricepackId
                );

                var clients = [];
                clients = await commaSeparatedValues(clientId);
                var info = [];
                for (var c = 0; c < clients.length; c++) {
                    var clientId = clients[c];
                    var subscriptions = await dbSub.subscriptions.create({
                        clientId,
                        businessId,
                        pricepacks,
                        subscriptionDateTime: subcriptiondate,
                        clientJoiningDate: isclientjoin
                    });

                    await dbSub.subscriptions.update({
                        parentSubscriptionId: subscriptions.subscriptionId
                    }, {
                        where: {
                            status: 1,
                            subscriptionId: subscriptions.subscriptionId
                        }
                    });

                    if (installment_due_on == 1) {
                        //specific date of month
                        var now = new Date(payment_due_date);
                    } else if (installment_due_on == 3) {
                        //joining date
                        var now = new Date();
                    }

                    // ====
                    var current_date = now; //in the second iteration it is gonna hold a date value. so we have taken temp_payment_due_date(int)
                    var clientpayments = await dbPayment.clientpayments.create({
                        invoice_number,
                        businessId,
                        payment_type,
                        subscriptionsId: subscriptions.subscriptionId,
                        due_amount: priceCalculation.payableAmount,
                        payment_due_date: current_date,
                        payment_end_date: current_date,
                        is_last_payment: 1,
                        curent_subcription_number: 1,
                        paymentReminder_date: current_date,
                    });
                    info = info.concat(subscriptions);
                }
            } else if (payment_type == 2) {
                /* One Time Payment Completed */

                /* Installment Payment */
                //If payment type installment(2) then required installment_frequency and numberOfInstallments
                if (
                    installment_frequency == null ||
                    numberOfInstallments == null ||
                    numberOfInstallments == "" ||
                    installment_frequency == ""
                ) {
                    throw new ApplicationError(
                        "Error in numberOfInstallments or installment_frequency",
                        401
                    );
                }

                var payment_due_date_array = [];
                var payment_end_date_array = [];

                if (installment_frequency == 1) {
                    //daily
                    if (installment_due_on == 1) {
                        //specific date of month
                        var now = new Date(payment_due_date);
                    } else if (installment_due_on == 3) {
                        //joining date
                        var now = new Date();
                    }
                    for (i = 0; i < numberOfInstallments; i++) {
                        payment_due_date_array[i] = addDays(now, i);
                        payment_end_date_array[i] = addDays(now, i);
                    }
                } else if (installment_frequency == 2) {
                    //weekly
                    if (installment_due_on == 1) {
                        //specific date of month
                        var now = new Date(payment_due_date);
                    } else if (installment_due_on == 2) {
                        //days after joining date
                        var today = new Date();
                        var now = addDays(today, payment_due_date);
                    } else if (installment_due_on == 3) {
                        //joining date
                        var now = new Date();
                    }
                    var last_update_date = "";
                    var inst_start_date = "";
                    for (var i = 0; i < numberOfInstallments; i++) {
                        let payment_due_date_temp;
                        let payment_end_date_temp;
                        var nowModifyformatDate = await formatDate(now);
                        if (i == 0) {
                            payment_due_date_array[i] = nowModifyformatDate;
                            payment_end_date_array[i] = nowModifyformatDate;
                            last_update_date = nowModifyformatDate;
                            inst_start_date = await getPlusDays(last_update_date, 7);
                        } else {
                            last_update_date = await getPlusDays(last_update_date, 7);
                            inst_start_date = await getPlusDays(payment_due_date_array[i - 1], 1);
                            payment_due_date_array[i] = last_update_date;
                            payment_end_date_array[i] = inst_start_date;
                        }
                    }
                } else if (installment_frequency == 3) {
                    //monthly
                    if (installment_due_on == 1) {
                        //specific date of month
                        var now = new Date(payment_due_date);
                    } else if (installment_due_on == 3) {
                        //joining date
                        var now = new Date();
                    }
                    var last_update_date = "";
                    var inst_start_date = "";
                    for (var i = 0; i < numberOfInstallments; i++) {
                        let payment_due_date_temp;
                        let payment_end_date_temp;
                        var nowModifyformatDate = await formatDate(now);
                        if (i == 0) {
                            payment_due_date_array[i] = nowModifyformatDate;
                            payment_end_date_array[i] = nowModifyformatDate;
                            last_update_date = nowModifyformatDate;
                            inst_start_date = await getPlusDays(last_update_date, 1);
                        } else {
                            last_update_date = await getPlusMonths(last_update_date, 1);
                            inst_start_date = await getPlusDays(payment_due_date_array[i - 1], 1);
                            payment_due_date_array[i] = last_update_date;
                            payment_end_date_array[i] = inst_start_date;
                        }
                    }
                } else if (installment_frequency == 4) {
                    //quarterly
                    if (installment_due_on == 1) {
                        //specific date of month
                        var now = new Date(payment_due_date);
                    } else if (installment_due_on == 3) {
                        //joining date
                        var now = new Date();
                    }
                    var last_update_date = "";
                    var inst_start_date = "";
                    for (var i = 0; i < numberOfInstallments; i++) {
                        let payment_due_date_temp;
                        let payment_end_date_temp;
                        var nowModifyformatDate = await formatDate(now);
                        if (i == 0) {
                            payment_due_date_array[i] = nowModifyformatDate;
                            payment_end_date_array[i] = nowModifyformatDate;
                            last_update_date = nowModifyformatDate;
                            inst_start_date = await getPlusDays(last_update_date, 3);
                        } else {
                            last_update_date = await getPlusMonths(last_update_date, 3);
                            inst_start_date = await getPlusDays(payment_due_date_array[i - 1], 1);
                            payment_due_date_array[i] = last_update_date;
                            payment_end_date_array[i] = inst_start_date;
                        }
                    }
                } else if (installment_frequency == 5) {
                    //yearly
                    if (installment_due_on == 1) {
                        //specific date of month
                        var now = new Date(payment_due_date);
                    } else if (installment_due_on == 3) {
                        //joining date
                        var now = new Date();
                    }
                    var last_update_date = "";
                    var inst_start_date = "";
                    for (var i = 0; i < numberOfInstallments; i++) {
                        let payment_due_date_temp;
                        let payment_end_date_temp;
                        var nowModifyformatDate = await formatDate(now);
                        if (i == 0) {
                            payment_due_date_array[i] = nowModifyformatDate;
                            payment_end_date_array[i] = nowModifyformatDate;
                            last_update_date = nowModifyformatDate;
                            inst_start_date = await getPlusDays(last_update_date, 12);
                        } else {
                            last_update_date = await getPlusMonths(last_update_date, 12);
                            inst_start_date = await getPlusDays(payment_due_date_array[i - 1], 1);
                            payment_due_date_array[i] = last_update_date;
                            payment_end_date_array[i] = inst_start_date;
                        }
                    }
                } else if (installment_frequency == 6) {
                    //others
                    var date = await commaSeparatedValues(payment_due_date);
                    if (date.length != numberOfInstallments) {
                        throw new ApplicationError(
                            "Error ! Mismatch in payment_due_date and numberOfInstallments",
                            401
                        );
                    }
                    var amtDue = await commaSeparatedValues(due_amount);
                    if (amtDue.length != date.length) {
                        throw new ApplicationError(
                            "Error ! Mismatch in due_amount and payment_due_date",
                            401
                        );
                    }

                    var insDueOn = await commaSeparatedValues(installment_due_on);
                    if (insDueOn.length != date.length) {
                        throw new ApplicationError(
                            "Error ! Mismatch in installment_due_on and payment_due_date",
                            401
                        );
                    }
                    var last_update_date = "";
                    var inst_start_date = "";
                    for (var pd = 0; pd < date.length; pd++) {
                        if (pd == 0) {
                            payment_due_date_array[pd] = new Date(date[pd]);
                            payment_end_date_array[pd] = new Date(date[pd]);
                            last_update_date = new Date(date[pd]);
                        } else {
                            payment_due_date_array[pd] = new Date(date[pd]);
                            last_update_date = await getPlusDays(payment_due_date_array[pd - 1], 1);
                            payment_end_date_array[pd] = last_update_date;
                        }
                    }
                } else {
                    throw new ApplicationError(
                        "error in installment_frequecy. 1: daily. 2: weekly, 3: monthly, 4: quarterly, 5: yearly, 6: others",
                        401
                    );
                }

                var info = [];
                var clients = [];
                clients = await commaSeparatedValues(clientId);
                for (var c = 0; c < clients.length; c++) {
                    let dbSub = createInstance(["clientSubscription"], information);
                    var clientId = clients[c];
                    let subscriptions = await dbSub.subscriptions.create({
                        clientId,
                        businessId,
                        pricepacks,
                        payment_type,
                        installment_frequency,
                        // frequency_number,
                        numberOfInstallments,
                        subscriptionDateTime: subcriptiondate,
                        clientJoiningDate: isclientjoin
                    });

                    info = info.concat(subscriptions);

                    await dbSub.subscriptions.update({
                        parentSubscriptionId: subscriptions.subscriptionId
                    }, {
                        where: {
                            status: 1,
                            subscriptionId: subscriptions.subscriptionId
                        }
                    });
                    dbSub.sequelize.close();
                    var dbPayment = createInstance(["clientPayments"], information);

                    for (var i = 0; i < numberOfInstallments; i++) {
                        var payment_due_date = payment_end_date_array[i];
                        var payment_end_date = payment_due_date_array[i];
                        if (installment_frequency == 6) {
                            var due_amount = amtDue[i];
                        } else {
                            var due_amount = due_amount;
                        }
                        let serviceAll = await dbservicepricepackmap.servicepricepackmap.findAll({
                            attributes: {
                                include: [],
                                exclude: [
                                    "servicepricepackmapId",
                                    "businessId",
                                    "serviceDuration",
                                    "createDateTime",
                                    "updateDateTime",
                                    "status"
                                ]
                            },
                            where: {
                                pricepackId: pricepacks
                            },
                            include: [{
                                    model: dbservices.services,
                                    attributes: {
                                        include: [],
                                        exclude: [
                                            "serviceId",
                                            "serviceName",
                                            "businessId",
                                            "serviceDetails",
                                            "createDateTime",
                                            "updateDateTime",
                                            "status"
                                        ]
                                    }
                                },
                                {
                                    model: dbPricepack.pricepacks,
                                    attributes: [
                                        ["amount", "pricepack_amount"]
                                    ]
                                }
                            ],
                            raw: true
                        });

                        let priceCalculation = await getPaymentCalculation(
                            information,
                            serviceAll[0]["pricepack.pricepack_amount"],
                            serviceAll[0].serviceId,
                            serviceAll[0].pricepackId
                        );
                        let discountPercentage = parseFloat(priceCalculation.discountPercentage);
                        let gstCal = (parseFloat(priceCalculation.sgstPercentage) + parseFloat(priceCalculation.cgstPercentage)) / 100;
                        let due_amount_cal =
                            due_amount - due_amount * discountPercentage / 100;
                        let due_amount_cal_with_gst =
                            due_amount_cal + due_amount_cal * gstCal;
                        let is_last_payment = i == numberOfInstallments - 1 ? 1 : 0;

                        console.log("==============================================================");
                        var clientpayments = await dbPayment.clientpayments.create({
                            invoice_number,
                            businessId,
                            subscriptionsId: subscriptions.subscriptionId,
                            due_amount: due_amount_cal_with_gst,
                            payment_due_date,
                            payment_end_date,
                            payment_type,
                            is_last_payment,
                            curent_subcription_number: Number(i) + 1,
                            paymentReminder_date: payment_due_date,
                            // payment_mode
                        });
                    }

                    if (installment_frequency != 6) {
                        var pdd;
                        if (installment_due_on == 3) {
                            pdd = new Date();
                        } else {
                            pdd = payment_due_date_array[0];
                        }
                        //adding clientJoiningDate in client table
                        let updateClient = await dbClient.clients.update({
                            clientJoiningDate: pdd, //clientJoiningDate is the first payment day for each and every clients
                            updateDateTime: Date.now()
                        }, {
                            where: {
                                clientId: subscriptions.clientId
                            }
                        });
                    }
                    dbPayment.sequelize.close();
                }
            } else {
                /* installment payment completed */
                throw new ApplicationError(
                    "Please provide correct value for payment type. 1 for one time payment. 2 for installment payment.",
                    401
                );
            }
            dbPayment.sequelize.close();
            dbClient.sequelize.close();
            dbPricepack.sequelize.close();
            dbservices.sequelize.close();
            return await info;
        } catch (error) {
            throw error;
        }
    };

    // Renewal Subscription
    renewlSubscription = async(
        information,
        businessId,
        subscriptionId,
        pricepacks,
        services,
        renewl_date,
        amount_paid,
        filterPrivateFields = true
    ) => {
        try {
            var subscriptionDateTime = new Date();
            let invoice_number = Date.now() + Math.floor(Math.random() * 8999 + 1000);
            let dbSub = createInstance(["clientSubscription"], information);
            var dbPayment = createInstance(["clientPayments"], information);
            var dbClient = createInstance(["client"], information);
            let dbPricepack = createInstance(["clientPricepack"], information);
            let dbservices = createInstance(["clientService"], information);
            let dbservicepricepackmap = createInstance(
                ["servicePricepackMap"],
                information
            );
            dbservicepricepackmap.servicepricepackmap.belongsTo(
                dbPricepack.pricepacks, {
                    foreignKey: "pricepackId",
                    targetKey: "pricepackId"
                }
            );
            dbservicepricepackmap.servicepricepackmap.belongsTo(dbservices.services, {
                foreignKey: "serviceId",
                targetKey: "serviceId"
            });
            var subscriptions = await dbSub.subscriptions.findAll({
                where: {
                    renewl_status: 1,
                    subscriptionId: subscriptionId
                },
                raw: true
            });
            var oldSubscriptions = await dbSub.subscriptions.findAll({
                where: {
                    renewl_status: 0,
                    subscriptionId: subscriptionId
                },
                raw: true
            });

            var oldclientpayments = await dbPayment.clientpayments.findAll({
                where: {
                    subscriptionsId: subscriptionId,
                    status: 1
                },
                group: ['subscriptionsId', 'clientpaymentId'],
                limit: 1,
                raw: true
            });

            if (oldSubscriptions.length > 0) {
                throw new ApplicationError(
                    "This subscription is already renewed !",
                    401
                );
            }
            if (subscriptions.length == 0) {
                throw new ApplicationError("No such subscriptions are found !", 401);
            }

            var oldsub = await dbSub.subscriptions.update({
                renewl_status: 0,
                updateDateTime: Date.now()
            }, {
                where: {
                    subscriptionId: subscriptions[0].subscriptionId
                }
            });
            var clientId = subscriptions[0].clientId;
            var installment_frequency = subscriptions[0].installment_frequency;
            var numberOfInstallments = subscriptions[0].numberOfInstallments;
            var parentSubscriptionId = subscriptions[0].parentSubscriptionId;
            let subscriptions = await dbSub.subscriptions.create({
                clientId,
                businessId,
                pricepacks,
                installment_frequency,
                numberOfInstallments,
                subscriptionDateTime,
                parentSubscriptionId,
                clientJoiningDate: new Date(renewl_date)
            });

            function addDays(theDate, days) {
                return new Date(theDate.getTime() + days * 24 * 60 * 60 * 1000);
            }
            // ====
            /* One Time Payment */
            if (installment_frequency == null && numberOfInstallments == null) {
                var payment_due_date = new Date(renewl_date);
                var due_amount = amount_paid;
                var dbPayment = createInstance(["clientPayments"], information);
                var clientpayments = await dbPayment.clientpayments.create({
                    businessId,
                    subscriptionsId: subscriptions.subscriptionId,
                    due_amount,
                    invoice_number,
                    payment_due_date: payment_due_date,
                    payment_end_date: payment_due_date,
                    is_last_payment: 1,
                    curent_subcription_number: 1,
                    paymentReminder_date: payment_due_date,
                    payment_type: oldclientpayments[0].payment_type
                });
            } else {
                /* Installment Payment */
                var payment_due_date_array = [];
                var payment_end_date_array = [];
                if (installment_frequency == 1) {
                    //daily
                    var now = new Date(renewl_date);
                    for (i = 0; i < numberOfInstallments; i++) {
                        payment_due_date_array[i] = addDays(now, i);
                        payment_end_date_array[i] = addDays(now, i);
                    }
                } else if (installment_frequency == 2) {
                    //weekly
                    var now = new Date(renewl_date);
                    var last_update_date = "";
                    var inst_start_date = "";
                    for (var i = 0; i < numberOfInstallments; i++) {
                        let payment_due_date_temp;
                        let payment_end_date_temp;
                        var nowModifyformatDate = await formatDate(now);
                        if (i == 0) {
                            payment_due_date_array[i] = nowModifyformatDate;
                            payment_end_date_array[i] = nowModifyformatDate;
                            last_update_date = nowModifyformatDate;
                            inst_start_date = await getPlusDays(last_update_date, 7);
                        } else {
                            last_update_date = await getPlusDays(last_update_date, 7);
                            inst_start_date = await getPlusDays(payment_due_date_array[i - 1], 1);
                            payment_due_date_array[i] = last_update_date;
                            payment_end_date_array[i] = inst_start_date;
                        }
                    }
                } else if (installment_frequency == 3) {
                    //monthly
                    var now = new Date(renewl_date);
                    var last_update_date = "";
                    var inst_start_date = "";
                    for (var i = 0; i < numberOfInstallments; i++) {
                        let payment_due_date_temp;
                        let payment_end_date_temp;
                        var nowModifyformatDate = await formatDate(now);
                        if (i == 0) {
                            payment_due_date_array[i] = nowModifyformatDate;
                            payment_end_date_array[i] = nowModifyformatDate;
                            last_update_date = nowModifyformatDate;
                            inst_start_date = await getPlusDays(last_update_date, 1);
                        } else {
                            last_update_date = await getPlusMonths(last_update_date, 1);
                            inst_start_date = await getPlusDays(payment_due_date_array[i - 1], 1);
                            payment_due_date_array[i] = last_update_date;
                            payment_end_date_array[i] = inst_start_date;
                        }
                    }
                } else if (installment_frequency == 4) {
                    //quarterly
                    var now = new Date(renewl_date);
                    var last_update_date = "";
                    var inst_start_date = "";
                    for (var i = 0; i < numberOfInstallments; i++) {
                        let payment_due_date_temp;
                        let payment_end_date_temp;
                        var nowModifyformatDate = await formatDate(now);
                        if (i == 0) {
                            payment_due_date_array[i] = nowModifyformatDate;
                            payment_end_date_array[i] = nowModifyformatDate;
                            last_update_date = nowModifyformatDate;
                            inst_start_date = await getPlusDays(last_update_date, 3);
                        } else {
                            last_update_date = await getPlusMonths(last_update_date, 3);
                            inst_start_date = await getPlusDays(payment_due_date_array[i - 1], 1);
                            payment_due_date_array[i] = last_update_date;
                            payment_end_date_array[i] = inst_start_date;
                        }
                    }
                } else if (installment_frequency == 5) {
                    //yearly
                    var now = new Date(renewl_date);
                    var last_update_date = "";
                    var inst_start_date = "";
                    for (var i = 0; i < numberOfInstallments; i++) {
                        let payment_due_date_temp;
                        let payment_end_date_temp;
                        var nowModifyformatDate = await formatDate(now);
                        if (i == 0) {
                            payment_due_date_array[i] = nowModifyformatDate;
                            payment_end_date_array[i] = nowModifyformatDate;
                            last_update_date = nowModifyformatDate;
                            inst_start_date = await getPlusDays(last_update_date, 12);
                        } else {
                            last_update_date = await getPlusMonths(last_update_date, 12);
                            inst_start_date = await getPlusDays(payment_due_date_array[i - 1], 1);
                            payment_due_date_array[i] = last_update_date;
                            payment_end_date_array[i] = inst_start_date;
                        }
                    }
                }

                var dbPayment = createInstance(["clientPayments"], information);
                for (var i = 0; i < numberOfInstallments; i++) {
                    var payment_due_date = payment_end_date_array[i];
                    var payment_end_date = payment_due_date_array[i];
                    let is_last_payment = i == numberOfInstallments - 1 ? 1 : 0;
                    var due_amount = amount_paid;

                    let serviceAll = await dbservicepricepackmap.servicepricepackmap.findAll({
                        attributes: {
                            include: [],
                            exclude: [
                                "servicepricepackmapId",
                                "businessId",
                                "serviceDuration",
                                "createDateTime",
                                "updateDateTime",
                                "status"
                            ]
                        },
                        where: {
                            pricepackId: pricepacks
                        },
                        include: [{
                                model: dbservices.services,
                                attributes: {
                                    include: [],
                                    exclude: [
                                        "serviceId",
                                        "serviceName",
                                        "businessId",
                                        "serviceDetails",
                                        "createDateTime",
                                        "updateDateTime",
                                        "status"
                                    ]
                                }
                            },
                            {
                                model: dbPricepack.pricepacks,
                                attributes: [
                                    ["amount", "pricepack_amount"]
                                ]
                            }
                        ],
                        raw: true
                    });

                    let priceCalculation = await getPaymentCalculation(
                        information,
                        serviceAll[0]["pricepack.pricepack_amount"],
                        serviceAll[0].serviceId,
                        serviceAll[0].pricepackId
                    );
                    let discountPercentage = parseFloat(priceCalculation.discountPercentage);
                    let gstCal = (parseFloat(priceCalculation.sgstPercentage) + parseFloat(priceCalculation.cgstPercentage)) / 100;
                    let due_amount_cal = due_amount - due_amount * discountPercentage / 100;
                    let due_amount_cal_with_gst = due_amount_cal + due_amount_cal * gstCal;
                    var clientpayments = await dbPayment.clientpayments.create({
                        businessId,
                        subscriptionsId: subscriptions.subscriptionId,
                        due_amount: due_amount_cal_with_gst,
                        invoice_number,
                        payment_due_date,
                        payment_end_date,
                        payment_type: oldclientpayments[0].payment_type,
                        is_last_payment,
                        curent_subcription_number: Number(i) + 1,
                        paymentReminder_date: payment_due_date,
                    });
                }
            }

            dbPayment.sequelize.close();
            dbClient.sequelize.close();
            dbPricepack.sequelize.close();
            dbservices.sequelize.close();
            /* End of Installment Payment */

            return await subscriptions;
        } catch (error) {
            throw error;
        }
    };


    listOfPricepacksUnderService = async(
        information,
        serviceId,
        filterPrivateFields = true
    ) => {
        try {
            let dbSPM = createInstance(["servicePricepackMap"], information);
            let spm = await dbSPM.servicepricepackmap.findAll({
                where: {
                    status: 1,
                    serviceId: serviceId
                },
                raw: true
            });

            var info = [];
            for (var i = 0; i < spm.length; i++) {
                let dbPricepack = createInstance(["clientPricepack"], information);
                var pricepack = await dbPricepack.pricepacks.findAll({
                    where: {
                        status: 1,
                        pricepackId: spm[i].pricepackId
                    },
                    raw: true
                });
                info = info.concat(pricepack);
            }
            return await info;
        } catch (error) {
            throw error;
        }
    };

    // currentSubscription = async(
    //     information,
    //     params,
    //     filterPrivateFields = true
    // ) => {
    //     try {
    //         let businessIdinformation = information.split(",")[1];
    //         let dbpaymentTransactions = createInstance(["paymentTransactions"], information);
    //         let dbclientpayments = createInstance(["clientPayments"], information);
    //         let dbClients = createInstance(["client"], information);
    //         let dbsubscriptions = createInstance(["clientSubscription"], information);
    //         let dbpricepack = createInstance(["clientPricepack"], information);
    //         let todayDate = await hyphenDateFormat(new Date);
    //         let dbservicepricepackmap = createInstance(["servicePricepackMap"], information);
    //         let dbservices = createInstance(["clientService"], information);
    //         // let todayDate = "2018-06-22";
    //         let newDataPayment;
    //         dbclientpayments.clientpayments.belongsTo(dbsubscriptions.subscriptions, {
    //             foreignKey: "subscriptionsId",
    //             targetKey: "subscriptionId"
    //         });
    //         dbsubscriptions.subscriptions.belongsTo(dbClients.clients, {
    //             foreignKey: "clientId",
    //             targetKey: "clientId"
    //         });
    //         dbsubscriptions.subscriptions.belongsTo(dbpricepack.pricepacks, {
    //             foreignKey: "pricepacks",
    //             targetKey: "pricepackId"
    //         });
    //         dbclientpayments.clientpayments.belongsTo(dbpaymentTransactions.paymentTransactions, {
    //             foreignKey: "clientpaymentId",
    //             targetKey: "clientpaymentId"
    //         });
    //         dbservicepricepackmap.servicepricepackmap.belongsTo(dbservices.services, {
    //             foreignKey: "serviceId",
    //             targetKey: "serviceId"
    //         });
    //         let filter = params[0] ? params[0] : "";
    //         var pageNo = params["page"] ? params["page"] : 1;
    //         var limitPerPage = PEGINATION_LIMIT;
    //         var offsetLimit = (pageNo - 1) * limitPerPage;
    //         let orderBy;
    //         if (params["order"] == 1) {
    //             orderBy = " ORDER BY `order_field` DESC";
    //         } else {
    //             orderBy = " ORDER BY `order_field` ASC";
    //         }
    //         let paymentData = await dbsubscriptions.sequelize.query('( SELECT `clientpayments`.`clientpaymentId`, `clientpayments`.`subscriptionsId`, `clientpayments`.`due_amount`, `clientpayments`.`payble_amount`, `clientpayments`.`paymentStatus`, `clientpayments`.`is_last_payment`, `clientpayments`.`payment_due_date`, `clientpayments`.`payment_end_date`, `clientpayments`.`UpdateDateTime` AS `updated_at`, `clientpayments`.`CreateDateTime` AS `order_field`, ( SELECT IFNULL( SUM(`payble_amount`), 0 ) FROM `clientpayments` INNER join `paymentTransactions` AS `paymentTransactions` ON `clientpayments`.`clientpaymentId` = `paymentTransactions`.`clientpaymentId` WHERE `subscriptionsId` = `subscription`.`subscriptionId` ) AS `total_subcription_payemt`, `subscription`.`clientId` AS `subscription.clientId`, `subscription`.`subscriptionDateTime` AS `subscription.subscriptionDateTime`, `subscription->client`.`clientId` AS `subscription.client.clientId`, `subscription->client`.`businessId` AS `subscription.client.businessId`, `subscription->client`.`centerId` AS `subscription.client.centerId`, `subscription->client`.`clientName` AS `subscription.client.clientName`, `subscription->client`.`contactNumber` AS `subscription.client.contactNumber`, `subscription->client`.`alternateNumber` AS `subscription.client.alternateNumber`, `subscription->client`.`emailId` AS `subscription.client.emailId`, `subscription->client`.`alternateEmail` AS `subscription.client.alternateEmail`, `subscription->client`.`dateOfBirth` AS `subscription.client.dateOfBirth`, `subscription->client`.`area` AS `subscription.client.area`, `subscription->client`.`pin` AS `subscription.client.pin`, `subscription->client`.`city` AS `subscription.client.city`, `subscription->client`.`photoUrl` AS `subscription.client.photoUrl`, `subscription->client`.`createDateTime` AS `subscription.client.createDateTime`, `subscription->client`.`updateDateTime` AS `subscription.client.updateDateTime`, `subscription->client`.`status` AS `subscription.client.status`, `subscription->client`.`clientName` AS `subscription.client.clientName`, CASE WHEN photoUrl is NULL THEN CONCAT( "' + KNACK_UPLOAD_URL.localUrl + '", "noimage.jpg" ) ELSE CONCAT( "' + KNACK_UPLOAD_URL.localUrl + '", photoUrl ) END AS `subscription.client.photoUrl`, `subscription->pricepack`.`pricepackId` AS `subscription.pricepack.pricepackId`, `subscription->pricepack`.`pricepackName` AS `subscription.pricepack.pricepackName`, `subscription->pricepack`.`amount` AS `subscription.pricepack.amount` FROM `clientpayments` AS `clientpayments` INNER JOIN `subscriptions` AS `subscription` ON `clientpayments`.`subscriptionsId` = `subscription`.`subscriptionId` INNER JOIN `clients` AS `subscription->client` ON `subscription`.`clientId` = `subscription->client`.`clientId` AND `subscription->client`.`status` = 1 AND `subscription->client`.`businessId` = "' + businessIdinformation + '" INNER JOIN `pricepacks` AS `subscription->pricepack` ON `subscription`.`pricepacks` = `subscription->pricepack`.`pricepackId` WHERE ( Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day ) >= "' + todayDate + '" AND `subscription->client`.`clientName` LIKE "%' + filter + '%" AND `subscription->pricepack`.`pricepackName` LIKE "%' + filter + '%" ) AND `curent_subcription_number` = 1 AND `clientpayments`.`status` = 1 AND `clientpayments`.`paymentStatus` = 1 AND `clientpayments`.`businessId` = "' + businessIdinformation + '" AND `payment_due_date` >= "' + todayDate + '" ) Union ( SELECT `clientpayments`.`clientpaymentId`, `clientpayments`.`subscriptionsId`, `clientpayments`.`due_amount`, `clientpayments`.`payble_amount`, `clientpayments`.`paymentStatus`, `clientpayments`.`is_last_payment`, `clientpayments`.`payment_due_date`, `clientpayments`.`payment_end_date`, `clientpayments`.`UpdateDateTime` AS `updated_at`, `clientpayments`.`CreateDateTime` AS `order_field`, ( SELECT IFNULL( SUM(`payble_amount`), 0 ) FROM `clientpayments` INNER join `paymentTransactions` AS `paymentTransactions` ON `clientpayments`.`clientpaymentId` = `paymentTransactions`.`clientpaymentId` WHERE `subscriptionsId` = `subscription`.`subscriptionId` ) AS `total_subcription_payemt`, `subscription`.`clientId` AS `subscription.clientId`, `subscription`.`subscriptionDateTime` AS `subscription.subscriptionDateTime`, `subscription->client`.`clientId` AS `subscription.client.clientId`, `subscription->client`.`businessId` AS `subscription.client.businessId`, `subscription->client`.`centerId` AS `subscription.client.centerId`, `subscription->client`.`clientName` AS `subscription.client.clientName`, `subscription->client`.`contactNumber` AS `subscription.client.contactNumber`, `subscription->client`.`alternateNumber` AS `subscription.client.alternateNumber`, `subscription->client`.`emailId` AS `subscription.client.emailId`, `subscription->client`.`alternateEmail` AS `subscription.client.alternateEmail`, `subscription->client`.`dateOfBirth` AS `subscription.client.dateOfBirth`, `subscription->client`.`area` AS `subscription.client.area`, `subscription->client`.`pin` AS `subscription.client.pin`, `subscription->client`.`city` AS `subscription.client.city`, `subscription->client`.`photoUrl` AS `subscription.client.photoUrl`, `subscription->client`.`createDateTime` AS `subscription.client.createDateTime`, `subscription->client`.`updateDateTime` AS `subscription.client.updateDateTime`, `subscription->client`.`status` AS `subscription.client.status`, `subscription->client`.`clientName` AS `subscription.client.clientName`, CASE WHEN photoUrl is NULL THEN CONCAT( "' + KNACK_UPLOAD_URL.localUrl + '", "noimage.jpg" ) ELSE CONCAT( "' + KNACK_UPLOAD_URL.localUrl + '", photoUrl ) END AS `subscription.client.photoUrl`, `subscription->pricepack`.`pricepackId` AS `subscription.pricepack.pricepackId`, `subscription->pricepack`.`pricepackName` AS `subscription.pricepack.pricepackName`, `subscription->pricepack`.`amount` AS `subscription.pricepack.amount` FROM `clientpayments` AS `clientpayments` INNER JOIN `subscriptions` AS `subscription` ON `clientpayments`.`subscriptionsId` = `subscription`.`subscriptionId` INNER JOIN `clients` AS `subscription->client` ON `subscription`.`clientId` = `subscription->client`.`clientId` AND `subscription->client`.`status` = 1 AND `subscription->client`.`businessId` = "' + businessIdinformation + '" INNER JOIN `pricepacks` AS `subscription->pricepack` ON `subscription`.`pricepacks` = `subscription->pricepack`.`pricepackId` WHERE ( Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day ) >= "' + todayDate + '" AND `subscription->client`.`clientName` LIKE "%' + filter + '%" AND `subscription->pricepack`.`pricepackName` LIKE "%' + filter + '%" ) AND (`payment_due_date` >= "' + todayDate + '" AND `payment_end_date` <= "' + todayDate + '") AND `clientpayments`.`status` = 1 AND `clientpayments`.`paymentStatus` = 1 AND `clientpayments`.`businessId` = "' + businessIdinformation + '" AND `payment_due_date` <= "' + todayDate + '" AND `paymentStatus` = 1 ) ' + orderBy + " LIMIT " + limitPerPage + " OFFSET " + offsetLimit, {
    //             type: dbsubscriptions.sequelize.QueryTypes.SELECT
    //         });


    //         let paymentDataCount = await dbsubscriptions.sequelize.query('( SELECT `clientpayments`.`clientpaymentId`, `clientpayments`.`subscriptionsId`, `clientpayments`.`due_amount`, `clientpayments`.`payble_amount`, `clientpayments`.`paymentStatus`, `clientpayments`.`is_last_payment`, `clientpayments`.`payment_due_date`, `clientpayments`.`payment_end_date`, `clientpayments`.`UpdateDateTime` AS `updated_at`, `clientpayments`.`CreateDateTime` AS `order_field`, ( SELECT IFNULL( SUM(`payble_amount`), 0 ) FROM `clientpayments` INNER join `paymentTransactions` AS `paymentTransactions` ON `clientpayments`.`clientpaymentId` = `paymentTransactions`.`clientpaymentId` WHERE `subscriptionsId` = `subscription`.`subscriptionId` ) AS `total_subcription_payemt`, `subscription`.`clientId` AS `subscription.clientId`, `subscription`.`subscriptionDateTime` AS `subscription.subscriptionDateTime`, `subscription->client`.`clientId` AS `subscription.client.clientId`, `subscription->client`.`businessId` AS `subscription.client.businessId`, `subscription->client`.`centerId` AS `subscription.client.centerId`, `subscription->client`.`clientName` AS `subscription.client.clientName`, `subscription->client`.`contactNumber` AS `subscription.client.contactNumber`, `subscription->client`.`alternateNumber` AS `subscription.client.alternateNumber`, `subscription->client`.`emailId` AS `subscription.client.emailId`, `subscription->client`.`alternateEmail` AS `subscription.client.alternateEmail`, `subscription->client`.`dateOfBirth` AS `subscription.client.dateOfBirth`, `subscription->client`.`area` AS `subscription.client.area`, `subscription->client`.`pin` AS `subscription.client.pin`, `subscription->client`.`city` AS `subscription.client.city`, `subscription->client`.`photoUrl` AS `subscription.client.photoUrl`, `subscription->client`.`createDateTime` AS `subscription.client.createDateTime`, `subscription->client`.`updateDateTime` AS `subscription.client.updateDateTime`, `subscription->client`.`status` AS `subscription.client.status`, `subscription->client`.`clientName` AS `subscription.client.clientName`, CASE WHEN photoUrl is NULL THEN CONCAT( "' + KNACK_UPLOAD_URL.localUrl + '", "noimage.jpg" ) ELSE CONCAT( "' + KNACK_UPLOAD_URL.localUrl + '", photoUrl ) END AS `subscription.client.photoUrl`, `subscription->pricepack`.`pricepackId` AS `subscription.pricepack.pricepackId`, `subscription->pricepack`.`pricepackName` AS `subscription.pricepack.pricepackName`, `subscription->pricepack`.`amount` AS `subscription.pricepack.amount` FROM `clientpayments` AS `clientpayments` INNER JOIN `subscriptions` AS `subscription` ON `clientpayments`.`subscriptionsId` = `subscription`.`subscriptionId` INNER JOIN `clients` AS `subscription->client` ON `subscription`.`clientId` = `subscription->client`.`clientId` AND `subscription->client`.`status` = 1 AND `subscription->client`.`businessId` = "' + businessIdinformation + '" INNER JOIN `pricepacks` AS `subscription->pricepack` ON `subscription`.`pricepacks` = `subscription->pricepack`.`pricepackId` WHERE ( Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day ) >= "' + todayDate + '" AND `subscription->client`.`clientName` LIKE "%' + filter + '%" AND `subscription->pricepack`.`pricepackName` LIKE "%' + filter + '%" ) AND `curent_subcription_number` = 1 AND `clientpayments`.`status` = 1 AND `clientpayments`.`paymentStatus` = 1 AND `clientpayments`.`businessId` = "' + businessIdinformation + '" AND `payment_due_date` >= "' + todayDate + '" ) Union ( SELECT `clientpayments`.`clientpaymentId`, `clientpayments`.`subscriptionsId`, `clientpayments`.`due_amount`, `clientpayments`.`payble_amount`, `clientpayments`.`paymentStatus`, `clientpayments`.`is_last_payment`, `clientpayments`.`payment_due_date`, `clientpayments`.`payment_end_date`, `clientpayments`.`UpdateDateTime` AS `updated_at`, `clientpayments`.`CreateDateTime` AS `order_field`, ( SELECT IFNULL( SUM(`payble_amount`), 0 ) FROM `clientpayments` INNER join `paymentTransactions` AS `paymentTransactions` ON `clientpayments`.`clientpaymentId` = `paymentTransactions`.`clientpaymentId` WHERE `subscriptionsId` = `subscription`.`subscriptionId` ) AS `total_subcription_payemt`, `subscription`.`clientId` AS `subscription.clientId`, `subscription`.`subscriptionDateTime` AS `subscription.subscriptionDateTime`, `subscription->client`.`clientId` AS `subscription.client.clientId`, `subscription->client`.`businessId` AS `subscription.client.businessId`, `subscription->client`.`centerId` AS `subscription.client.centerId`, `subscription->client`.`clientName` AS `subscription.client.clientName`, `subscription->client`.`contactNumber` AS `subscription.client.contactNumber`, `subscription->client`.`alternateNumber` AS `subscription.client.alternateNumber`, `subscription->client`.`emailId` AS `subscription.client.emailId`, `subscription->client`.`alternateEmail` AS `subscription.client.alternateEmail`, `subscription->client`.`dateOfBirth` AS `subscription.client.dateOfBirth`, `subscription->client`.`area` AS `subscription.client.area`, `subscription->client`.`pin` AS `subscription.client.pin`, `subscription->client`.`city` AS `subscription.client.city`, `subscription->client`.`photoUrl` AS `subscription.client.photoUrl`, `subscription->client`.`createDateTime` AS `subscription.client.createDateTime`, `subscription->client`.`updateDateTime` AS `subscription.client.updateDateTime`, `subscription->client`.`status` AS `subscription.client.status`, `subscription->client`.`clientName` AS `subscription.client.clientName`, CASE WHEN photoUrl is NULL THEN CONCAT( "' + KNACK_UPLOAD_URL.localUrl + '", "noimage.jpg" ) ELSE CONCAT( "' + KNACK_UPLOAD_URL.localUrl + '", photoUrl ) END AS `subscription.client.photoUrl`, `subscription->pricepack`.`pricepackId` AS `subscription.pricepack.pricepackId`, `subscription->pricepack`.`pricepackName` AS `subscription.pricepack.pricepackName`, `subscription->pricepack`.`amount` AS `subscription.pricepack.amount` FROM `clientpayments` AS `clientpayments` INNER JOIN `subscriptions` AS `subscription` ON `clientpayments`.`subscriptionsId` = `subscription`.`subscriptionId` INNER JOIN `clients` AS `subscription->client` ON `subscription`.`clientId` = `subscription->client`.`clientId` AND `subscription->client`.`status` = 1 AND `subscription->client`.`businessId` = "' + businessIdinformation + '" INNER JOIN `pricepacks` AS `subscription->pricepack` ON `subscription`.`pricepacks` = `subscription->pricepack`.`pricepackId` WHERE ( Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day ) >= "' + todayDate + '" AND `subscription->client`.`clientName` LIKE "%' + filter + '%" AND `subscription->pricepack`.`pricepackName` LIKE "%' + filter + '%" ) AND (`payment_due_date` >= "' + todayDate + '" AND `payment_end_date` <= "' + todayDate + '") AND `clientpayments`.`status` = 1 AND `clientpayments`.`paymentStatus` = 1 AND `clientpayments`.`businessId` = "' + businessIdinformation + '" AND `payment_due_date` <= "' + todayDate + '" AND `paymentStatus` = 1 ) ' + orderBy, {
    //             type: dbsubscriptions.sequelize.QueryTypes.SELECT
    //         });
    //         newDataPayment = {
    //             "count": paymentDataCount.length,
    //             "rows": paymentData
    //         };
    //         if (newDataPayment.rows) {
    //             for (var i = 0; i < newDataPayment.rows.length; i++) {
    //                 let serviceAll = await dbservicepricepackmap.servicepricepackmap.findAll({
    //                     attributes: {
    //                         include: [],
    //                         exclude: [`servicepricepackmapId`, `pricepackId`, `serviceId`, `businessId`, `serviceDuration`, `createDateTime`, `updateDateTime`, `status`]
    //                     },
    //                     where: {
    //                         status: 1,
    //                         pricepackId: newDataPayment.rows[i]["subscription.pricepack.pricepackId"]
    //                     },
    //                     include: [{
    //                         model: dbservices.services,
    //                         attributes: [
    //                             [dbservicepricepackmap.sequelize.fn('GROUP_CONCAT', dbservicepricepackmap.sequelize.col('serviceName')), 'serviceNameList']
    //                         ],
    //                     }],
    //                     raw: true
    //                 });
    //                 let serviceAllID = await dbservicepricepackmap.servicepricepackmap.findAll({
    //                     attributes: {
    //                         include: [],
    //                         exclude: [`servicepricepackmapId`, `pricepackId`, `serviceId`, `businessId`, `serviceDuration`, `createDateTime`, `updateDateTime`, `status`]
    //                     },
    //                     where: {
    //                         status: 1,
    //                         pricepackId: newDataPayment.rows[i]["subscription.pricepack.pricepackId"]
    //                     },
    //                     include: [{
    //                         model: dbservices.services,
    //                         attributes: [
    //                             'serviceId'
    //                         ],
    //                     }],
    //                     raw: true
    //                 });
    //                 newDataPayment.rows[i]["serviceIDList"] = serviceAllID;
    //                 newDataPayment.rows[i]["serviceNameList"] = serviceAll;
    //                 if (serviceAllID) {
    //                     for (var x = 0; x < serviceAllID.length; x++) {
    //                         let payble_amount_perPackage = newDataPayment.rows[i]["subscription.pricepack.amount"];
    //                         let serviceId_perPackage = serviceAllID[x]["service.serviceId"];
    //                         let pricepackId_perPackage = newDataPayment.rows[i].pricepackId;
    //                         newDataPayment.rows[i]["getPaymentCalculation"] = await getPaymentCalculation(information, payble_amount_perPackage, serviceId_perPackage, pricepackId_perPackage);
    //                     }
    //                 }
    //             }
    //         }
    //         dbclientpayments.sequelize.close();
    //         dbClients.sequelize.close();
    //         dbsubscriptions.sequelize.close();
    //         dbpricepack.sequelize.close();
    //         dbpaymentTransactions.sequelize.close();
    //         dbclientpayments.sequelize.close();
    //         dbservicepricepackmap.sequelize.close();
    //         dbservices.sequelize.close();
    //         return newDataPayment;


    //     } catch (error) {
    //         throw error;
    //     }
    // };




    currentSubscription = async(
        information,
        params,
        filterPrivateFields = true
    ) => {
        try {


            var mergedAllActiveClients = [];
            let todayDate = await hyphenDateFormat(new Date);
            // let todayDate = '2018-07-04';
            var businessIdinformation = information.split(",")[1];
            let dbclientpayments = createInstance(["clientPayments"], information);
            let dbsubscriptions = createInstance(["clientSubscription"], information);
            let dbClients = createInstance(["client"], information);
            let dbpricepack = createInstance(["clientPricepack"], information);
            let dbclientexpense = createInstance(["clientExpense"], information);
            let dbpaymentTransactions = createInstance(["paymentTransactions"], information);
            let dbclientScheduleMap = createInstance(["clientScheduleMap"], information);
            dbclientpayments.clientpayments.belongsTo(dbsubscriptions.subscriptions, {
                foreignKey: "subscriptionsId",
                targetKey: "subscriptionId"
            });
            dbsubscriptions.subscriptions.belongsTo(dbClients.clients, {
                foreignKey: "clientId",
                targetKey: "clientId"
            });
            dbsubscriptions.subscriptions.belongsTo(dbpricepack.pricepacks, {
                foreignKey: "pricepacks",
                targetKey: "pricepackId"
            });
            dbclientpayments.clientpayments.belongsTo(dbpaymentTransactions.paymentTransactions, {
                foreignKey: "clientpaymentId",
                targetKey: "clientpaymentId"
            });
            dbsubscriptions.subscriptions.belongsTo(dbclientScheduleMap.clientschedulemap, {
                foreignKey: "subscriptionId",
                targetKey: "subscriptionId"
            });

            let filter = params[0] ? params[0] : "";
            var pageNo = params["page"] ? params["page"] : 1;
            var limitPerPage = PEGINATION_LIMIT;
            var offsetLimit = (pageNo - 1) * limitPerPage;

            let clientpaymentsRiminder = {};
            let recentIncomeclientpayments = {};
            let recentExpenseclientExpense = {};

            let itemclientpaymentsRiminderData = {
                status: 1,
                businessId: businessIdinformation,
                clientpaymentId: {
                    [dbclientpayments.sequelize.Op.in]:
                    // [Sequelize.literal("(SELECT MIN(`clientpaymentId`) FROM `clientpayments` WHERE `payment_due_date` >= '" + todayDate + "' AND `payment_end_date` >= '" + todayDate + "' AND `status` = '1' GROUP BY `subscriptionsId`)")]
                    // [Sequelize.literal("(SELECT MIN(`clientpaymentId`) FROM `clientpayments` WHERE (`payment_due_date` <= '" + todayDate + "' AND `payment_end_date` >= '" + todayDate + "' AND `status` = '1' ) OR ( `payment_due_date` >= '" + todayDate + "' AND `payment_end_date` >= '" + todayDate + "' AND `status` = '1' ) AND `status` = '1' GROUP BY `subscriptionsId`)")]
                    // [Sequelize.literal("(SELECT MIN(`clientpaymentId`) FROM `clientpayments` INNER JOIN `subscriptions` AS `subscription` ON `clientpayments`.`subscriptionsId` = `subscription`.`subscriptionId` INNER JOIN `pricepacks` AS `subscription->pricepack` ON `subscription`.`pricepacks` = `subscription->pricepack`.`pricepackId` WHERE Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day ) <= '" + todayDate + "' OR ( ( `clientpayments`.`payment_due_date` <= '" + todayDate + "' AND `clientpayments`.`payment_end_date` >= '" + todayDate + "' ) OR ( `clientpayments`.`payment_due_date` >= '" + todayDate + "' AND `clientpayments`.`payment_end_date` >= '" + todayDate + "' ) AND `clientpayments`.`status` = '1' ) GROUP BY `clientpayments`.`subscriptionsId` ORDER BY `payment_end_date` ASC )")]
                        [Sequelize.literal("(SELECT MIN(`clientpaymentId`) as `clientpaymentId` FROM `clientpayments` INNER JOIN `subscriptions` AS `subscription` ON `clientpayments`.`subscriptionsId` = `subscription`.`subscriptionId` INNER JOIN `pricepacks` AS `subscription->pricepack` ON `subscription`.`pricepacks` = `subscription->pricepack`.`pricepackId` WHERE ( DATE( Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day )) >= '" + todayDate + "' ) AND ( ( `clientpayments`.`payment_due_date` <= '" + todayDate + "' AND `clientpayments`.`payment_end_date` >= '" + todayDate + "' ) OR ( `clientpayments`.`payment_due_date` >= '" + todayDate + "' AND `clientpayments`.`payment_end_date` >= '" + todayDate + "' )) AND `clientpayments`.`status` = '1' GROUP BY `clientpayments`.`subscriptionsId` UNION select c.clientpaymentId from clientpayments as c, ( SELECT a.clientpaymentId, a.curent_subcription_number, a.payment_end_date, COUNT(a.subscriptionsId) FROM clientpayments a LEFT JOIN clientpayments b ON a.subscriptionsId = b.subscriptionsId AND b.status = 1 WHERE b.status = 1 AND a.status = 1 GROUP BY a.subscriptionsId, a.clientpaymentId having a.curent_subcription_number = COUNT(a.subscriptionsId) AND a.payment_end_date < CURDATE() ) as total WHERE ( DATE( Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day )) >= '" + todayDate + "' ) AND c.clientpaymentId IN (total.clientpaymentId))")]
                },
                [dbClients.sequelize.Op.or]: [{
                        "$subscription.client.clientName$": {
                            like: "%" + filter + "%"
                        }
                    },
                    {
                        "$subscription.pricepack.pricepackName$": {
                            like: "%" + filter + "%"
                        }
                    }
                ],
                [dbclientScheduleMap.sequelize.Op.and]: [Sequelize.literal("( ( `subscription->clientschedulemap`.`clientScheduleMapId` IS NULL OR `subscription->clientschedulemap`.`sessionCount` IS NULL ) OR ( `subscription->clientschedulemap`.`sessionCount` < `subscription->pricepack`.`serviceDuration` ) )")],
            };
            let itemGroupConditions = {
                status: 1,
                businessId: businessIdinformation,
            };
            let clientpaymentsRiminderData = await dbclientpayments.clientpayments.findAndCount({
                attributes: [
                    "clientpaymentId",
                    "subscriptionsId",
                    "due_amount",
                    "payble_amount",
                    "payment_due_date",
                    "payment_end_date", ["UpdateDateTime", "updated_at"],
                    "paymentStatus", [dbclientpayments.sequelize.literal("DATE(Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day ))"), 'subcription_end_Date']
                ],
                include: [{
                    model: dbsubscriptions.subscriptions,
                    attributes: [
                        "clientId",
                        "amountPaidSoFar",
                        "status_amountPaidSoFar",
                        "clientJoiningDate",
                        "subscriptionDateTime",
                        "installment_frequency",
                    ],
                    include: [{
                            model: dbClients.clients,
                            attributes: {
                                include: [
                                    "clientName", [
                                        dbClients.clients.sequelize.literal(
                                            'CASE WHEN photoUrl  is NULL THEN CONCAT("' +
                                            KNACK_UPLOAD_URL.localUrl +
                                            '" , "noimage.jpg" ) ELSE CONCAT("' +
                                            KNACK_UPLOAD_URL.localUrl +
                                            '" , photoUrl ) END'
                                        ),
                                        "photoUrl"
                                    ],
                                ],
                                exclude: []
                            },
                            where: itemGroupConditions
                        },
                        {
                            model: dbpricepack.pricepacks,
                            attributes: ["pricepackName"],
                            where: {}
                        },
                        {
                            model: dbclientScheduleMap.clientschedulemap,
                            required: false,
                            attributes: ["sessionCount"],
                            where: {
                                // [dbclientScheduleMap.sequelize.Op.and]: [Sequelize.literal("`subscription->clientschedulemap`.`scheduleId`= `subscription->clientschedulemap`.`parentScheduleId`")],
                                [dbclientScheduleMap.sequelize.Op.and]: [Sequelize.literal("( `subscription->clientschedulemap`.`scheduleId` = `subscription->clientschedulemap`.`parentScheduleId` ) AND ( `subscription->clientschedulemap`.`scheduleId` = (select min(`scheduleId`) from `clientschedulemap` `inSched` where `inSched`.`subscriptionId` = `subscription`.`subscriptionId` ) )")],
                            }
                        }
                    ],
                    where: {}
                }],
                order: [
                    ["payment_end_date", "ASC"]
                ],
                where: itemclientpaymentsRiminderData,
                subQuery: false,
                offset: offsetLimit,
                limit: limitPerPage,
                raw: true
            });
            if (clientpaymentsRiminderData.rows.length > 0) {
                for (let x = 0; x < clientpaymentsRiminderData.rows.length; x++) {
                    clientpaymentsRiminderData.rows[x]["sessionInfo"] = await sessionInfo(information, {
                        status: 1,
                        subscriptionId: clientpaymentsRiminderData.rows[x]['subscriptionsId']
                    });
                    if (clientpaymentsRiminderData.rows[x]["subscription.installment_frequency"] == '6') {
                        let new_data = '';
                        let cal_new_amount = '';
                        if (clientpaymentsRiminderData.rows[x]["subscription.status_amountPaidSoFar"] == 1) {
                            let date_check = (clientpaymentsRiminderData.rows[x]['subscription.clientJoiningDate'] != null) ? clientpaymentsRiminderData.rows[x]['subscription.clientJoiningDate'] : clientpaymentsRiminderData.rows[x]['subscription.subscriptionDateTime'];
                            cal_new_amount = await this.getTotalShowDataOthers(information, clientpaymentsRiminderData.rows[x]['subscriptionsId'], date_check);
                            new_data = Number(cal_new_amount[0].new_balance) - Number(clientpaymentsRiminderData.rows[x]["subscription.amountPaidSoFar"]);
                        } else {
                            let date_check = await hyphenDateFormat(new Date());
                            cal_new_amount = await this.getTotalShowDataOthers(information, clientpaymentsRiminderData.rows[x]['subscriptionsId'], date_check);
                            new_data = Number(cal_new_amount[0].new_balance);
                        }
                        clientpaymentsRiminderData.rows[x]["getTotalShowData"] = [{
                            new_balance: new_data
                        }];
                        clientpaymentsRiminderData.rows[x]["getTotalDue"] = await this.getTotalDue(information, clientpaymentsRiminderData.rows[x]['subscriptionsId']);
                        let getTotalPaid = await this.getTotalPaid(information, clientpaymentsRiminderData.rows[x]['subscriptionsId']);
                        if (clientpaymentsRiminderData.rows[x]["subscription.status_amountPaidSoFar"] == '1') {
                            getTotalPaid[0].total_paid_amount = Number(getTotalPaid[0].total_paid_amount);
                        } else {
                            getTotalPaid[0].total_paid_amount = Number(getTotalPaid[0].total_paid_amount) - Number(clientpaymentsRiminderData.rows[x]["subscription.amountPaidSoFar"]);
                        }
                        clientpaymentsRiminderData.rows[x]["getTotalPaid"] = getTotalPaid;
                    } else {
                        let new_data = '';
                        let cal_new_amount = '';
                        if (clientpaymentsRiminderData.rows[x]["subscription.status_amountPaidSoFar"] == 1) {
                            let date_check = (clientpaymentsRiminderData.rows[x]['subscription.clientJoiningDate'] != null) ? clientpaymentsRiminderData.rows[x]['subscription.clientJoiningDate'] : clientpaymentsRiminderData.rows[x]['subscription.subscriptionDateTime'];
                            cal_new_amount = await this.getTotalShowData(information, clientpaymentsRiminderData.rows[x]['subscriptionsId'], date_check);
                            new_data = Number(cal_new_amount[0].new_balance) - Number(clientpaymentsRiminderData.rows[x]["subscription.amountPaidSoFar"]);
                        } else {
                            // let date_check = (clientpaymentsRiminderData.rows[x]['subscription.clientJoiningDate'] != null) ? clientpaymentsRiminderData.rows[x]['subscription.clientJoiningDate'] : clientpaymentsRiminderData.rows[x]['subscription.subscriptionDateTime'];
                            let date_check = clientpaymentsRiminderData.rows[x]['payment_end_date'];
                            cal_new_amount = await this.getTotalShowData(information, clientpaymentsRiminderData.rows[x]['subscriptionsId'], date_check);
                            new_data = Number(cal_new_amount[0].new_balance);
                        }
                        clientpaymentsRiminderData.rows[x]["getTotalShowData"] = [{
                            new_balance: new_data
                        }];
                        clientpaymentsRiminderData.rows[x]["getTotalDue"] = await this.getTotalDue(information, clientpaymentsRiminderData.rows[x]['subscriptionsId']);
                        let getTotalPaid = await this.getTotalPaid(information, clientpaymentsRiminderData.rows[x]['subscriptionsId']);
                        if (clientpaymentsRiminderData.rows[x]["subscription.status_amountPaidSoFar"] == '1') {
                            getTotalPaid[0].total_paid_amount = Number(getTotalPaid[0].total_paid_amount);
                        } else {
                            getTotalPaid[0].total_paid_amount = Number(getTotalPaid[0].total_paid_amount) - Number(clientpaymentsRiminderData.rows[x]["subscription.amountPaidSoFar"]);
                        }
                        clientpaymentsRiminderData.rows[x]["getTotalPaid"] = getTotalPaid;

                    }
                    let listclientPayments = await dbclientpayments.clientpayments.findAll({
                        where: {
                            businessId: businessIdinformation,
                            subscriptionsId: clientpaymentsRiminderData.rows[x]['subscriptionsId'],
                            status: 1,
                            "$payble_amount$": {
                                ne: null
                            }
                        },
                        raw: true
                    });
                    clientpaymentsRiminderData.rows[x]["subcription_payment_status"] = listclientPayments.length;
                }
            }
            clientpaymentsRiminder.count = clientpaymentsRiminderData.count;
            clientpaymentsRiminder.rows = clientpaymentsRiminderData.rows;
            return await clientpaymentsRiminder;
        } catch (error) {
            throw error;
        }
    };



    getTotalShowData = async(
        information,
        subscriptionsId,
        date
    ) => {
        try {
            let businessIdinformation = information.split(",")[1];
            let dbsubscriptions = createInstance(["clientSubscription"], information);
            let todayDate = await hyphenDateFormat(date);
            // let todayDate = '2018-07-04';
            if (subscriptionsId) {
                console.log("============================================================")
            }
            let data = await dbsubscriptions.sequelize.query(
                "SELECT (SUM(`due_amount`) - CASE WHEN SUM(`payble_amount`) is NULL THEN 0 ELSE SUM(`payble_amount`) END) AS `new_balance` FROM `clientpayments` WHERE `payment_due_date` <= '" + todayDate + "' AND `status` = '1' AND `subscriptionsId` = '" + subscriptionsId + "'", {
                    type: dbsubscriptions.sequelize.QueryTypes.SELECT
                }
            );
            if (subscriptionsId) {
                console.log("============================================================")
            }
            dbsubscriptions.sequelize.close();
            return data;
        } catch (error) {
            throw error;
        }
    };



    getTotalShowDataOthers = async(
        information,
        subscriptionsId,
        date
    ) => {
        try {
            let final = {};
            let finalReturn = [];
            let finalReturnCal = 0;
            var due_amount_local = 0;
            var payble_amount_local = 0;
            let businessIdinformation = information.split(",")[1];
            let dbsubscriptions = createInstance(["clientSubscription"], information);
            let todayDate = await hyphenDateFormat(date);
            let data = await dbsubscriptions.sequelize.query(
                "( SELECT `payment_due_date`, `payment_end_date`, `due_amount`, `payble_amount` FROM `clientpayments` WHERE (`payment_due_date` >= '" + todayDate + "' AND `curent_subcription_number` = 1 ) AND `status` = '1' AND `subscriptionsId` = '" + subscriptionsId + "' ) union ( SELECT `payment_due_date`, `payment_end_date`, `due_amount`, `payble_amount` FROM `clientpayments` WHERE ( `payment_due_date` <= '" + todayDate + "' OR `payment_end_date` <= '" + todayDate + "' ) AND `status` = '1' AND `subscriptionsId` = '" + subscriptionsId + "' )", {
                    type: dbsubscriptions.sequelize.QueryTypes.SELECT
                }
            );
            dbsubscriptions.sequelize.close();
            if (data) {
                for (let x = 0; x < data.length; x++) {
                    due_amount_local += Number(data[x].due_amount);
                    payble_amount_local += Number(data[x].payble_amount);
                }
                finalReturnCal = due_amount_local - payble_amount_local;
            }
            var a = [{
                new_balance: finalReturnCal
            }];
            return a;
        } catch (error) {
            throw error;
        }
    };


    getTotalDue = async(
        information,
        subscriptionsId
    ) => {
        try {

            let businessIdinformation = information.split(",")[1];
            let dbsubscriptions = createInstance(["clientSubscription"], information);
            let todayDate = await hyphenDateFormat(new Date);
            let data = await dbsubscriptions.sequelize.query(
                // "SELECT SUM(`due_amount`) as `total_current_due` FROM `clientpayments` WHERE `subscriptionsId` = '" + subscriptionsId + "' AND `paymentStatus` = '1' AND `status` = '1' AND ( ( `payment_due_date` = '" + todayDate + "' AND `payment_end_date` = '" + todayDate + "' ) OR ( `payment_due_date` <= '" + todayDate + "' ) )", {
                "SELECT SUM(`due_amount`) as `total_current_due` FROM `clientpayments` WHERE `subscriptionsId` = '" + subscriptionsId + "' AND `status` = '1' ", {
                    type: dbsubscriptions.sequelize.QueryTypes.SELECT
                }
            );
            dbsubscriptions.sequelize.close();
            return data;
        } catch (error) {
            throw error;
        }
    };


    getTotalPaid = async(
        information,
        subscriptionsId
    ) => {
        try {
            let businessIdinformation = information.split(",")[1];
            let dbsubscriptions = createInstance(["clientSubscription"], information);
            console.log("===============================getTotalPaid=============================================");
            let data = await dbsubscriptions.sequelize.query(
                "SELECT SUM(`payble_amount`) as `total_paid_amount` FROM `clientpayments` WHERE `subscriptionsId` = '" + subscriptionsId + "' AND `status` = '1' ", {
                    type: dbsubscriptions.sequelize.QueryTypes.SELECT
                }
            );
            console.log("==================================getTotalPaid==========================================");
            dbsubscriptions.sequelize.close();
            return data;
        } catch (error) {
            throw error;
        }
    };



    pastSubscription = async(
        information,
        params,
        filterPrivateFields = true
    ) => {
        try {
            var businessIdinformation = information.split(",")[1];
            let dbpaymentTransactions = createInstance(["paymentTransactions"], information);
            let dbclientpayments = createInstance(["clientPayments"], information);
            let dbClients = createInstance(["client"], information);
            let dbsubscriptions = createInstance(["clientSubscription"], information);
            let dbpricepack = createInstance(["clientPricepack"], information);
            let dbservicepricepackmap = createInstance(["servicePricepackMap"], information);
            let dbservices = createInstance(["clientService"], information);
            let dbclientScheduleMap = createInstance(["clientScheduleMap"], information);
            let todayDate = await hyphenDateFormat(new Date);
            // let todayDate = "2018-08-21";
            dbclientpayments.clientpayments.belongsTo(dbsubscriptions.subscriptions, {
                foreignKey: "subscriptionsId",
                targetKey: "subscriptionId"
            });
            dbsubscriptions.subscriptions.belongsTo(dbClients.clients, {
                foreignKey: "clientId",
                targetKey: "clientId"
            });
            dbsubscriptions.subscriptions.belongsTo(dbpricepack.pricepacks, {
                foreignKey: "pricepacks",
                targetKey: "pricepackId"
            });
            dbclientpayments.clientpayments.belongsTo(dbpaymentTransactions.paymentTransactions, {
                foreignKey: "clientpaymentId",
                targetKey: "clientpaymentId"
            });
            dbservicepricepackmap.servicepricepackmap.belongsTo(dbservices.services, {
                foreignKey: "serviceId",
                targetKey: "serviceId"
            });
            // dbpricepack.subscriptions.hasMany(dbservicepricepackmap.servicepricepackmap, {
            //     foreignKey: "pricepacks",
            //     targetKey: "pricepackId"
            // });
            dbsubscriptions.subscriptions.belongsTo(dbclientScheduleMap.clientschedulemap, {
                foreignKey: "subscriptionId",
                targetKey: "subscriptionId"
            });

            let filter = params[0] ? params[0] : "";
            var pageNo = params["page"] ? params["page"] : 1;
            var limitPerPage = PEGINATION_LIMIT;
            var offsetLimit = (pageNo - 1) * limitPerPage;
            let orderBy;
            if (params["order"] == 1) {
                orderBy = [
                    ["payment_end_date", "ASC"]
                ];
            } else {
                orderBy = [
                    ["payment_end_date", "DESC"]
                ];
            }
            let itemConditionsclientpayments = {
                status: 1,
                // paymentStatus: 1,
                is_last_payment: 1,
                businessId: businessIdinformation,
                [dbClients.sequelize.Op.and]: [{
                        "$subscription.client.clientName$": {
                            like: "%" + filter + "%"
                        }
                    },
                    {
                        "$subscription.pricepack.pricepackName$": {
                            like: "%" + filter + "%"
                        }
                    }
                ],
                // [dbClients.sequelize.Op.and]: [Sequelize.literal("( ( `subscription->clientschedulemap`.`clientScheduleMapId` IS NOT NULL OR `subscription->clientschedulemap`.`sessionCount` IS NOT NULL ) OR ( `subscription->clientschedulemap`.`sessionCount` = `subscription->pricepack`.`serviceDuration` ) )")],
                // [dbClients.sequelize.Op.and]: [Sequelize.literal("(`subscription->clientschedulemap`.`sessionCount` = `subscription->pricepack`.`serviceDuration` )")],
                [dbClients.sequelize.Op.and]: [Sequelize.literal("(DATE( Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day )) < '" + todayDate + "') OR (`subscription->clientschedulemap`.`sessionCount` = `subscription->pricepack`.`serviceDuration` )")],
            };
            let itemGroupConditions = {
                status: 1,
                businessId: businessIdinformation,
            };
            let itemGroupConditionsPricePack = {};
            let itemConditions = {
                renewl_status: 1
            };

            let paymentData = await dbclientpayments.clientpayments.findAndCount({
                attributes: [
                    "clientpaymentId",
                    "subscriptionsId",
                    "due_amount",
                    "payble_amount",
                    "payment_due_date",
                    "payment_end_date", ["UpdateDateTime", "updated_at"],
                    [
                        dbClients.clients.sequelize.literal(
                            'DATE(Date_add( `subscription`.`subscriptiondatetime`,INTERVAL `subscription->pricepack`.`pricepackvalidity` day))'
                        ),
                        "subcription_end_Date"
                    ],
                    [
                        dbclientpayments.sequelize.literal("( SELECT IFNULL( SUM(`payble_amount`), 0 ) FROM `clientpayments` INNER join `paymentTransactions` AS `paymentTransactions` ON `clientpayments`.`clientpaymentId` = `paymentTransactions`.`clientpaymentId` WHERE `clientpayments`.`subscriptionsId` = `subscription`.`subscriptionId` )"),
                        "total_subcription_payemt"
                    ]
                ],
                include: [{
                    model: dbsubscriptions.subscriptions,
                    attributes: ["clientId", "status_amountPaidSoFar", "amountPaidSoFar"],
                    include: [{
                            model: dbClients.clients,
                            attributes: {
                                include: [
                                    "clientName", [
                                        dbClients.clients.sequelize.literal(
                                            'CASE WHEN photoUrl  is NULL THEN CONCAT("' +
                                            KNACK_UPLOAD_URL.localUrl +
                                            '" , "noimage.jpg" ) ELSE CONCAT("' +
                                            KNACK_UPLOAD_URL.localUrl +
                                            '" , photoUrl ) END'
                                        ),
                                        "photoUrl"
                                    ],
                                ],
                                exclude: []
                            },
                            where: itemGroupConditions
                        },
                        {
                            model: dbpricepack.pricepacks,
                            attributes: ["pricepackName", "amount", "serviceDuration"],
                            where: itemGroupConditionsPricePack,
                        },
                        {
                            model: dbclientScheduleMap.clientschedulemap,
                            attributes: ["scheduleId", "sessionCount"],
                            require: false,
                            where: {
                                // [dbclientScheduleMap.sequelize.Op.and]: [Sequelize.literal("`subscription->clientschedulemap`.`scheduleId`= `subscription->clientschedulemap`.`parentScheduleId`")],
                                [dbclientScheduleMap.sequelize.Op.and]: [Sequelize.literal("( `subscription->clientschedulemap`.`scheduleId` = `subscription->clientschedulemap`.`parentScheduleId` ) AND ( `subscription->clientschedulemap`.`scheduleId` = (select min(`scheduleId`) from `clientschedulemap` `inSched` where `inSched`.`subscriptionId` = `subscription`.`subscriptionId` ) )")],
                            }
                        }
                    ],
                    where: itemConditions
                }],
                order: orderBy,
                where: itemConditionsclientpayments,
                subQuery: false,
                offset: offsetLimit,
                limit: limitPerPage,
                raw: true
            });
            if (paymentData.rows) {
                for (var i = 0; i < paymentData.rows.length; i++) {
                    let serviceAll = await dbservicepricepackmap.servicepricepackmap.findAll({
                        attributes: {
                            include: [],
                            exclude: [`servicepricepackmapId`, `serviceDuration_number`, `pricepackId`, `serviceId`, `businessId`, `serviceDuration`, `createDateTime`, `updateDateTime`, `status`]
                        },
                        where: {
                            status: 1,
                            pricepackId: paymentData.rows[i]["subscription.pricepack.pricepackId"]
                        },
                        include: [{
                            model: dbservices.services,
                            attributes: [
                                [dbservicepricepackmap.sequelize.fn('GROUP_CONCAT', dbservicepricepackmap.sequelize.col('serviceName')), 'serviceNameList']
                            ],
                        }],
                        raw: true
                    });
                    let serviceAllID = await dbservicepricepackmap.servicepricepackmap.findAll({
                        attributes: {
                            include: [],
                            exclude: [`servicepricepackmapId`, `pricepackId`, `serviceId`, `businessId`, `serviceDuration`, `createDateTime`, `updateDateTime`, `status`]
                        },
                        where: {
                            status: 1,
                            pricepackId: paymentData.rows[i]["subscription.pricepack.pricepackId"]
                        },
                        include: [{
                            model: dbservices.services,
                            attributes: [
                                'serviceId'
                            ],
                        }],
                        raw: true
                    });
                    paymentData.rows[i]["serviceIDList"] = serviceAllID;
                    paymentData.rows[i]["serviceNameList"] = serviceAll;
                    if (serviceAllID) {
                        for (var x = 0; x < serviceAllID.length; x++) {
                            let payble_amount_perPackage = paymentData.rows[i]["subscription.pricepack.amount"];
                            let serviceId_perPackage = serviceAllID[x]["service.serviceId"];
                            let pricepackId_perPackage = paymentData.rows[i].pricepackId;
                            paymentData.rows[i]["getPaymentCalculation"] = await getPaymentCalculation(information, payble_amount_perPackage, serviceId_perPackage, pricepackId_perPackage);
                        }
                    }
                    paymentData.rows[i]["sessionInfo"] = await sessionInfo(information, {
                        status: 1,
                        subscriptionId: paymentData.rows[i]['subscriptionsId']
                    });

                }
            }


            if (paymentData.rows.length > 0) {
                for (let x = 0; x < paymentData.rows.length; x++) {
                    paymentData.rows[x]["sessionInfo"] = await sessionInfo(information, {
                        status: 1,
                        subscriptionId: paymentData.rows[x]['subscriptionsId']
                    });
                    if (paymentData.rows[x]["subscription.installment_frequency"] == '6') {
                        let new_data = '';
                        let cal_new_amount = '';
                        if (paymentData.rows[x]["subscription.status_amountPaidSoFar"] == 1) {
                            let date_check = (paymentData.rows[x]['subscription.clientJoiningDate'] != null) ? paymentData.rows[x]['subscription.clientJoiningDate'] : paymentData.rows[x]['subscription.subscriptionDateTime'];
                            cal_new_amount = await this.getTotalShowDataOthers(information, paymentData.rows[x]['subscriptionsId'], date_check);
                            new_data = Number(cal_new_amount[0].new_balance) - Number(paymentData.rows[x]["subscription.amountPaidSoFar"]);
                        } else {
                            let date_check = await hyphenDateFormat(new Date());
                            cal_new_amount = await this.getTotalShowDataOthers(information, paymentData.rows[x]['subscriptionsId'], date_check);
                            new_data = Number(cal_new_amount[0].new_balance);
                        }
                        paymentData.rows[x]["getTotalShowData"] = [{
                            new_balance: new_data
                        }];
                        paymentData.rows[x]["getTotalDue"] = await this.getTotalDue(information, paymentData.rows[x]['subscriptionsId']);
                        let getTotalPaid = await this.getTotalPaid(information, paymentData.rows[x]['subscriptionsId']);
                        if (paymentData.rows[x]["subscription.status_amountPaidSoFar"] == '1') {
                            getTotalPaid[0].total_paid_amount = Number(getTotalPaid[0].total_paid_amount);
                        } else {
                            getTotalPaid[0].total_paid_amount = Number(getTotalPaid[0].total_paid_amount) - Number(paymentData.rows[x]["subscription.amountPaidSoFar"]);
                        }
                        paymentData.rows[x]["getTotalPaid"] = getTotalPaid;
                    } else {
                        let new_data = '';
                        let cal_new_amount = '';
                        if (paymentData.rows[x]["subscription.status_amountPaidSoFar"] == 1) {
                            let date_check = (paymentData.rows[x]['subscription.clientJoiningDate'] != null) ? paymentData.rows[x]['subscription.clientJoiningDate'] : paymentData.rows[x]['subscription.subscriptionDateTime'];
                            cal_new_amount = await this.getTotalShowData(information, paymentData.rows[x]['subscriptionsId'], date_check);
                            new_data = Number(cal_new_amount[0].new_balance) - Number(paymentData.rows[x]["subscription.amountPaidSoFar"]);
                        } else {
                            // let date_check = (paymentData.rows[x]['subscription.clientJoiningDate'] != null) ? paymentData.rows[x]['subscription.clientJoiningDate'] : paymentData.rows[x]['subscription.subscriptionDateTime'];
                            let date_check = paymentData.rows[x]['payment_end_date'];
                            cal_new_amount = await this.getTotalShowData(information, paymentData.rows[x]['subscriptionsId'], date_check);
                            new_data = Number(cal_new_amount[0].new_balance);
                        }
                        paymentData.rows[x]["getTotalShowData"] = [{
                            new_balance: new_data
                        }];
                        paymentData.rows[x]["getTotalDue"] = await this.getTotalDue(information, paymentData.rows[x]['subscriptionsId']);
                        let getTotalPaid = await this.getTotalPaid(information, paymentData.rows[x]['subscriptionsId']);
                        if (paymentData.rows[x]["subscription.status_amountPaidSoFar"] == '1') {
                            getTotalPaid[0].total_paid_amount = Number(getTotalPaid[0].total_paid_amount);
                        } else {
                            getTotalPaid[0].total_paid_amount = Number(getTotalPaid[0].total_paid_amount) - Number(paymentData.rows[x]["subscription.amountPaidSoFar"]);
                        }
                        paymentData.rows[x]["getTotalPaid"] = getTotalPaid;
                    }
                    let listclientPayments = await dbclientpayments.clientpayments.findAll({
                        where: {
                            businessId: businessIdinformation,
                            subscriptionsId: paymentData.rows[x]['subscriptionsId'],
                            status: 1,
                            "$payble_amount$": {
                                ne: null
                            }
                        },
                        raw: true
                    });
                    paymentData.rows[x]["subcription_payment_status"] = listclientPayments.length;
                }
            }





            // if (paymentData.rows) {
            //     for (var i = 0; i < paymentData.rows.length; i++) {
            //         let listPayment = await dbclientpayments.clientpayments.findAll({
            //             attributes: [
            //                 "due_amount", ['clientpaymentId', 'clientpaymentIds'],
            //                 [
            //                     dbclientpayments.sequelize.literal(
            //                         "(SELECT CASE WHEN SUM(`paid_amount`) IS NULL THEN 0 ELSE SUM(`paid_amount`) end FROM `clientpayments` RIGHT JOIN `paymentTransactions` AS `paymentTransactions` ON `clientpayments`.`clientpaymentId` = `paymentTransactions`.`clientpaymentId` WHERE `clientpayments`.`clientpaymentId` = `paymentTransactions`.`clientpaymentId`)"
            //                     ),
            //                     "total_paid_amount"
            //                 ]
            //             ],
            //             where: {
            //                 status: 1,
            //                 subscriptionsId: paymentData.rows[i].subscriptionsId,
            //                 businessId: businessIdinformation,
            //                 [dbclientpayments.sequelize.Op.and]: [{
            //                     $payment_due_date$: {
            //                         lte: todayDate
            //                     }
            //                 }]
            //             },
            //             raw: true
            //         });
            //         let paid_amount = 0;
            //         let session_amount = 0;
            //         if (listPayment) {
            //             for (var x = 0; x < listPayment.length; x++) {
            //                 session_amount += listPayment[x].due_amount;
            //                 paid_amount += listPayment[x].total_paid_amount;
            //             }
            //         }
            //         paymentData.rows[i]["paymentDetails"] = listPayment;
            //         paymentData.rows[i]["paid_amount"] = paid_amount;
            //         paymentData.rows[i]["total_payble"] = parseInt(session_amount) - parseInt(paid_amount);
            //     }
            // }
            dbclientpayments.sequelize.close();
            dbClients.sequelize.close();
            dbsubscriptions.sequelize.close();
            dbpricepack.sequelize.close();
            dbpaymentTransactions.sequelize.close();
            dbclientpayments.sequelize.close();
            dbservicepricepackmap.sequelize.close();
            dbclientScheduleMap.sequelize.close();
            return paymentData;


        } catch (error) {
            throw error;
        }
    };


    //Search subscriptions based on clients' namee, Pricepack and services
    searchSubscription = async(
        information,
        businessId,
        searchKeyword,
        filterPrivateFields = true
    ) => {
        try {
            let db = createInstance(["clientSubscription"], information);
            let subscriptions = await db.subscriptions.findAll({
                where: {
                    status: 1,
                    businessId: businessId
                },
                raw: true
            });

            var info = [];
            //getting all the subscriptions
            for (var i = 0; i < subscriptions.length; i++) {
                var dbClient = createInstance(["client"], information);
                var clientDetails = await dbClient.clients.findAll({
                    where: {
                        //status: 1,
                        clientId: subscriptions[i].clientId
                    },
                    raw: true
                });
                var clientStatus = (clientDetails[0].status == 1) ? 'Active' : 'Inactive';

                var clientphoto = KNACK_UPLOAD_URL.localUrl + clientDetails[0].photoUrl;

                //getting the pricepack of the subscription
                let dbPricepack = createInstance(["clientPricepack"], information);
                let pricepack = await dbPricepack.pricepacks.findAll({
                    where: {
                        pricepackId: subscriptions[i].pricepacks,
                        status: 1
                    }
                });

                //getting current subscriptions
                //getting all the installment by due_date in ascending order for every subscription 
                var today = new Date(); //**this date can be changed */
                var dbpayment = createInstance(["clientPayments"], information);
                let clientpayments = await dbpayment.clientpayments.findAll({
                    where: {
                        status: 1,
                        businessId: businessId,
                        subscriptionsId: subscriptions[i].subscriptionId,
                        //paymentStatus: !3
                    },
                    order: [
                        ['payment_due_date', 'ASC'],
                    ],
                    raw: true
                });

                //flagdate will hold the date of payment due, old date is an array of passed date
                var flagdate, olddate = [];
                for (var d = 0; d < clientpayments.length; d++) {
                    if (new Date(clientpayments[d].payment_due_date) >= today) {
                        flagdate = new Date(clientpayments[d].payment_due_date);
                        break;
                    }
                    olddate[d] = new Date(clientpayments[d].payment_due_date);
                    if ((new Date(clientpayments[d].payment_due_date)) <= today && today < (new Date(clientpayments[d + 1].payment_due_date))) {
                        flagdate = new Date(clientpayments[d + 1].payment_due_date);
                        break;
                    }
                }

                //installment details for flagdate
                let payments = await dbpayment.clientpayments.findAll({
                    where: {
                        status: 1,
                        businessId: businessId,
                        subscriptionsId: subscriptions[i].subscriptionId,
                        payment_due_date: flagdate
                    },
                    raw: true
                });

                var DueAmount = 0;
                var PaidAmount = 0;
                for (var old = 0; old < olddate.length; old++) {
                    let paymentsOld = await dbpayment.clientpayments.findAll({
                        where: {
                            status: 1,
                            businessId: businessId,
                            subscriptionsId: subscriptions[i].subscriptionId,
                            payment_due_date: olddate[old]
                        },
                        raw: true
                    });
                    DueAmount = DueAmount + paymentsOld[0].due_amount;
                    if (paymentsOld[0].paid_amount != null) {
                        PaidAmount = PaidAmount + paymentsOld[0].paid_amount;
                    }
                }
                let paymentsThisDay = await dbpayment.clientpayments.findAll({
                    where: {
                        status: 1,
                        businessId: businessId,
                        subscriptionsId: subscriptions[i].subscriptionId,
                        payment_due_date: flagdate
                    },
                    raw: true
                });
                DueAmount = DueAmount + paymentsThisDay[0].due_amount;
                if (paymentsThisDay[0].paid_amount != null) {
                    PaidAmount = PaidAmount + paymentsThisDay[0].paid_amount;
                }
                DueAmount -= PaidAmount;

                var varToArray = [];
                Array.prototype.push.apply(varToArray, [{
                    clientPaymentId: paymentsThisDay[0].clientpaymentId
                }]);
                Array.prototype.push.apply(varToArray, [{
                    subscriptionId: subscriptions[i].subscriptionId
                }]);
                Array.prototype.push.apply(varToArray, [{
                    clientId: clientDetails[0].clientId
                }]);
                Array.prototype.push.apply(varToArray, [{
                    clientName: clientDetails[0].clientName
                }]);
                Array.prototype.push.apply(varToArray, [{
                    clientPhoto: clientphoto
                }]);
                Array.prototype.push.apply(varToArray, [{
                    clientContactNumber: clientDetails[0].contactNumber
                }]);
                Array.prototype.push.apply(varToArray, [{
                    clientStatus: clientStatus
                }]);
                Array.prototype.push.apply(varToArray, [{
                        subscriptionDate: subscriptions[i].subscriptionDateTime
                    }

                ]);
                Array.prototype.push.apply(varToArray, [{
                    pricepackId: pricepack[0].pricepackId
                }]);
                Array.prototype.push.apply(varToArray, [{
                    pricepackName: pricepack[0].pricepackName
                }]);
                Array.prototype.push.apply(varToArray, [{
                    dueDate: flagdate
                }]);
                Array.prototype.push.apply(varToArray, [{
                    paidAmount: PaidAmount
                }]);
                Array.prototype.push.apply(varToArray, [{
                    dueAmount: DueAmount
                }]);
                Array.prototype.push.apply(varToArray, [{
                    status: clientDetails[0].status
                }]);
                Array.prototype.push.apply(varToArray, [{
                    'validity': ''
                }]);

                var merged = {};
                merged = Object.assign.apply(Object, varToArray);
                info = info.concat(merged);
            }

            var sortedInfo = [];
            info.forEach(function(client) {
                if (client.clientName.toLowerCase().indexOf(searchKeyword.toLowerCase()) >= 0 ||
                    client.pricepackName.toLowerCase().indexOf(searchKeyword.toLowerCase()) >= 0
                ) {
                    sortedInfo.push(client);
                }
            })

            return await sortedInfo;
        } catch (error) {
            throw error;
        }
    };



    settings1 = async(
        information,
        subscriptionId,

        clientId,
        businessId,
        pricepacks,
        payment_type,
        numberOfInstallments,
        installment_frequency,
        due_amount,
        installment_due_on,
        payment_due_date,

        dateOfJoining,
        totalSessionsAttended,
        amountPaidSoFar
    ) => {
        try {

            let dbSub = createInstance(["clientSubscription"], information);
            let dbpayment = createInstance(["clientPayments"], information);
            let db = createInstance(["clientSubscription"], information);
            let dbPP = createInstance(["clientPricepack"], information);
            let dbPayment = createInstance(["clientPayments"], information);
            let dbClient = createInstance(["client"], information);
            let dbPricepack = createInstance(["clientPricepack"], information);
            let dbservices = createInstance(["clientService"], information);
            let dbservicepricepackmap = createInstance(["servicePricepackMap"], information);
            dbservicepricepackmap.servicepricepackmap.belongsTo(
                dbPricepack.pricepacks, {
                    foreignKey: "pricepackId",
                    targetKey: "pricepackId"
                }
            );
            dbservicepricepackmap.servicepricepackmap.belongsTo(dbservices.services, {
                foreignKey: "serviceId",
                targetKey: "serviceId"
            });

            //validations
            if (payment_type < 1 || payment_type > 2) {
                // throw new ApplicationError(
                //   "Error in payment_due_date. 1: one time payment, 2: installment payment.",
                //   401
                // );
            } else if (payment_type == 2) {
                if (installment_frequency < 1 || installment_frequency > 6) {
                    throw new ApplicationError(
                        "Error in installment_frequency. 1: daily, 2: weekly, 3: monthly, 4: quarterly, 5: yearly, 6: others",
                        401
                    );
                }
                if (due_amount.includes(",") && installment_frequency != 6) {
                    throw new ApplicationError(
                        "Error in due_amount and installment_frequency",
                        401
                    );
                }
                if (installment_due_on.includes(",") && installment_frequency != 6) {
                    throw new ApplicationError(
                        "Error in installment_due_on and installment_frequency",
                        401
                    );
                }
                if (payment_due_date.includes(",") && installment_frequency != 6) {
                    throw new ApplicationError(
                        "Error in payment_due_date and installment_frequency",
                        401
                    );
                }
            }
            let da = await commaSeparatedValues(due_amount);
            for (var eda = 0; eda < da.length; eda++) {
                if (parseInt(da[eda]) < 0) {
                    throw new ApplicationError(
                        "due_amount should contain +ve value",
                        401
                    );
                }
            }
            if (amountPaidSoFar < 0) {
                throw new ApplicationError(
                    "amountPaidSoFar contains -ve value. value should be +ve",
                    401
                );
            }
            //end of validations

            //updating clientJoingingDate in client table

            var subscriptions = await dbSub.subscriptions.findAll({
                where: {
                    status: 1,
                    subscriptionId: subscriptionId
                },
                raw: true
            });


            if (subscriptions[0]) {
                var clientJoinDate = await dbSub.subscriptions.update({
                    clientJoiningDate: dateOfJoining,
                    updateDateTime: Date.now()
                }, {
                    where: {
                        subscriptionId: subscriptionId
                    }
                });
            } else {
                throw new ApplicationError(
                    "Error. Subscription has not been found.",
                    401
                );
            }

            //end of updating clientJoiningDate


            //checking if atleast one payment is done or not            
            let sumOfPayments = await dbpayment.clientpayments.findAll({
                attributes: ['subscriptionsId', [dbpayment.sequelize.fn('sum', dbpayment.sequelize.col('payble_amount')), 'totalPayment']],
                group: ["subscriptionsId"],
                where: {
                    status: 1,
                    businessId: businessId,
                    subscriptionsId: subscriptionId,
                },
                raw: true
            });

            var amount = parseInt(amountPaidSoFar);
            if (sumOfPayments.totalPayment == null) amount = parseInt(amountPaidSoFar);
            else amount = parseInt(sumOfPayments.totalPayment) + parseInt(amountPaidSoFar);
            //end of checking past payment details


            //updating subscription table only if the amounts paid + amountPaidSoFar <=  pricepackAmt
            let getPricepack = await db.subscriptions.findAll({
                where: {
                    status: 1,
                    businessId: businessId,
                    subscriptionId: subscriptionId
                },
                raw: true
            });
            var oldPaymentType = getPricepack[0].payment_type;
            var pricepack = await dbPP.pricepacks.findAll({
                where: {
                    status: 1,
                    pricepackId: getPricepack[0].pricepacks
                },
                raw: true
            });
            if (amount >= pricepack[0].amount) {
                throw new ApplicationError(
                    "addition of amountPaidSoFar and all installment payments shouldn't be more than pricepack amount",
                    401
                );
            } else {
                //updating clientPaymentsTable only if no transaction is done so far
                if (sumOfPayments.totalPayment != null && oldPaymentType != payment_type) {
                    throw new ApplicationError(
                        "payment is already started hence payment_type cannot be changed !",
                        401
                    );
                } else {
                    //updating subscription table
                    if (payment_type == 1) {
                        var ins_freq = null;
                        var ins_num = null;
                    } else if (payment_type == 2) {
                        var ins_freq = installment_frequency;
                        var ins_num = numberOfInstallments;
                    } else {
                        var ins_freq = getPricepack[0].installment_frequency;
                        var ins_num = getPricepack[0].numberOfInstallments;
                        var ptype = getPricepack[0].payment_type;
                    }
                    var subscriptionDateTime = new Date(dateOfJoining);

                    var subscriptions = await dbSub.subscriptions.update({
                        subscriptionDateTime,
                        installment_frequency: ins_freq,
                        numberOfInstallments: ins_num,
                        totalSessionsAttended,
                        amountPaidSoFar,
                        payment_type: ptype,
                        updateDateTime: Date.now()
                    }, {
                        where: {
                            businessId: businessId,
                            subscriptionId: subscriptionId,
                            status: 1
                        }
                    });
                    //end of updating subscription table

                    if (payment_type == 1 || payment_type == 2) { //else not update any data in payment table
                        //updating client payments section
                        let update_client_payment = await dbpayment.clientpayments.update( //deleting old payment details
                            {
                                status: 0
                            }, {
                                where: {
                                    subscriptionsId: subscriptionId,
                                    paid_amount: null, //the rows with paid_amount (which are not null) will not be updated. they will still be there
                                    status: 1
                                }
                            }
                        );

                        function addDays(theDate, days) {
                            return new Date(theDate.getTime() + days * 24 * 60 * 60 * 1000);
                        }

                        // var subscriptionDateTime = new Date();



                        /* One Time Payment */
                        if (payment_type == 1) { //one time payment
                            if (installment_due_on == 1) { //specific date of month
                                var now = new Date(payment_due_date);
                            } else if (installment_due_on == 2) { //days after joining date
                                var today = new Date(dateOfJoining);
                                var now = addDays(today, payment_due_date);
                            } else if (installment_due_on == 3) { //joining date
                                var now = new Date(dateOfJoining);
                            }
                            var payment_due_date = now;
                            var clientpayments = await dbPayment.clientpayments.create({
                                businessId,
                                subscriptionsId: subscriptionId,
                                due_amount: pricepack[0].amount,
                                payment_due_date,
                                paymentReminder_date: payment_due_date,
                                // payment_type
                            });
                        } // One Time Payment Completed
                        /* Installment Payment */
                        else if (parseInt(payment_type) === 2) {
                            var payment_due_date_array = [];
                            if (installment_frequency == 1) { //daily
                                if (installment_due_on == 1) { //specific date of month
                                    var now = new Date(payment_due_date);
                                } else if (installment_due_on == 2) { //days after joining date
                                    var today = new Date(dateOfJoining);
                                    var now = addDays(today, payment_due_date);
                                } else if (installment_due_on == 3) { //joining date
                                    var now = new Date(dateOfJoining);
                                }
                                for (i = 0; i < numberOfInstallments; i++) {
                                    payment_due_date_array[i] = addDays(now, i);
                                }
                            } else if (installment_frequency == 2) { //weekly
                                if (installment_due_on == 1) { //specific date of month
                                    var now = new Date(payment_due_date);
                                } else if (installment_due_on == 2) { //days after joining date
                                    var today = new Date(dateOfJoining);
                                    var now = addDays(today, payment_due_date);
                                } else if (installment_due_on == 3) { //joining date
                                    var now = new Date(dateOfJoining);
                                }
                                var w = 0;
                                for (i = 0; i < numberOfInstallments; i++) {
                                    payment_due_date_array[i] = addDays(now, w);
                                    w = w + 7;
                                }
                            } else if (installment_frequency == 3) { //monthly
                                if (installment_due_on == 1) { //specific date of month
                                    var now = new Date(payment_due_date);
                                } else if (installment_due_on == 2) { //days after joining date
                                    var today = new Date(dateOfJoining);
                                    var now = addDays(today, payment_due_date);
                                } else if (installment_due_on == 3) { //joining date
                                    var now = new Date(dateOfJoining);
                                }
                                var d = now.getDate() + 1;
                                var m = now.getMonth();
                                var y = now.getFullYear();
                                for (var i = 0; i < numberOfInstallments; i++) {
                                    if (m >= 12) {
                                        m = 1;
                                        y = y + 1;
                                    } else {
                                        m = m + 1;
                                    }
                                    var date = y + '-' + m + '-' + d;
                                    payment_due_date_array[i] = new Date(date);
                                }
                            } else if (installment_frequency == 4) { //quarterly
                                if (installment_due_on == 1) { //specific date of month
                                    var now = new Date(payment_due_date);
                                } else if (installment_due_on == 3) { //joining date
                                    var now = new Date(dateOfJoining);
                                }
                                var interval = 0;
                                for (var i = 0; i < numberOfInstallments; i++) {
                                    var q = new Date(now);
                                    q.setMonth(q.getMonth() + interval);
                                    payment_due_date_array[i] = new Date(q);
                                    interval = interval + 3;
                                }
                            } else if (installment_frequency == 5) { //yearly
                                if (installment_due_on == 1) { //specific date of month
                                    var now = new Date(payment_due_date);
                                } else if (installment_due_on == 3) { //joining date
                                    var now = new Date(dateOfJoining);
                                }
                                var d = now.getDate() + 1;
                                var m = now.getMonth() + 1;
                                var y = now.getFullYear();
                                for (var i = 0; i < numberOfInstallments; i++) {
                                    y = y + 1;
                                    var date = y + '-' + m + '-' + d;
                                    payment_due_date_array[i] = new Date(date);
                                }
                            } else if (installment_frequency == 6) { //others
                                var date = await commaSeparatedValues(payment_due_date);
                                if (date.length != numberOfInstallments) {
                                    throw new ApplicationError(
                                        "Error ! Mismatch in payment_due_date and numberOfInstallments",
                                        401
                                    );
                                }
                                var amtDue = await commaSeparatedValues(due_amount);
                                if (amtDue.length != date.length) {
                                    throw new ApplicationError(
                                        "Error ! Mismatch in due_amount and payment_due_date",
                                        401
                                    );
                                }

                                var insDueOn = await commaSeparatedValues(installment_due_on);
                                if (insDueOn.length != date.length) {
                                    throw new ApplicationError(
                                        "Error ! Mismatch in installment_due_on and payment_due_date",
                                        401
                                    );
                                }
                                for (var pd = 0; pd < date.length; pd++) {
                                    if (insDueOn[pd] == 1) { //specific date of month
                                        payment_due_date_array[pd] = new Date(date[pd]);
                                    } else if (insDueOn[pd] == 2) { //days after joining date
                                        if (!isNaN(parseFloat(date[pd])) && isFinite(date[pd])) { //checking isNumber
                                            var doj = new Date(dateOfJoining);
                                            payment_due_date_array[pd] = addDays(doj, date[pd]);
                                        } else {
                                            throw new ApplicationError(
                                                "error in payment_due_date",
                                                401
                                            );
                                        }
                                    } else if (insDueOn[pd] == 3) {
                                        payment_due_date_array[pd] = new Date(dateOfJoining);
                                    }
                                }
                            }



                            for (var i = 0; i < numberOfInstallments; i++) {
                                var payment_due_date = payment_due_date_array[i];

                                if (installment_frequency == 6) {
                                    var due_amount = amtDue[i];
                                } else {
                                    var due_amount = due_amount;
                                }
                                var clientpayments = await dbPayment.clientpayments.create({
                                    businessId,
                                    subscriptionsId: subscriptionId,
                                    due_amount,
                                    payment_due_date,
                                    paymentReminder_date: payment_due_date,
                                    // payment_type,
                                    // payment_mode
                                });
                            }
                        }
                        /* installment payment completed */
                    }
                }
            }
            //end of updating subscription table
            return subscriptions;
        } catch (error) {
            throw error;
        }
    };



    settings = async(
        information,
        subscriptionId,
        clientId,
        businessId,
        pricepacks,
        payment_type,
        numberOfInstallments,
        installment_frequency,
        due_amount,
        installment_due_on,
        payment_due_date,
        dateOfJoining,
        totalSessionsAttended,
        amountPaidSoFar
    ) => {
        try {

            let dbSub = createInstance(["clientSubscription"], information);
            let dbclientPayments = createInstance(["clientPayments"], information);
            // Check same client and pricepack id is exist or not
            let clientIdArray = await commaSeparatedValues(clientId); //get client list
            let listclientPaymentsTotal = await dbclientPayments.clientpayments.findAll({
                where: {
                    businessId: businessId,
                    subscriptionsId: subscriptionId,
                    status: 1,
                    "$payble_amount$": {
                        ne: null
                    }
                },
                raw: true
            });
            for (let client of clientIdArray) {
                let checkSubscription = await dbSub.subscriptions.findAll({
                    where: {
                        businessId: businessId,
                        subscriptionId: subscriptionId,
                        status: 1,
                        "$status_amountPaidSoFar$": {
                            ne: null
                        }
                    },
                    raw: true
                });

                // if (checkSubscription.length > 0) {
                //     throw new ApplicationError("this subcription already use subcription settings ", 409);
                // }


                let listclientPayments = await dbclientPayments.clientpayments.findAll({
                    where: {
                        businessId: businessId,
                        subscriptionsId: subscriptionId,
                        status: 1,
                        "$payble_amount$": {
                            ne: null
                        }
                    },
                    raw: true
                });



                // if (listclientPayments.length > 0) {
                //     throw new ApplicationError("this subcription already payment exist ", 409);
                // }
            }
            dbclientPayments.sequelize.close();



            let dbCSM = createInstance(["clientScheduleMap"], information);
            let dbSchedule = createInstance(["clientSchedule"], information);
            let dbPP = createInstance(["clientPricepack"], information);
            let dbSPM = createInstance(["servicePricepackMap"], information);

            dbCSM.clientschedulemap.belongsTo(dbSchedule.schedule, {
                foreignKey: "scheduleId",
                targetKey: "scheduleId"
            });

            dbCSM.clientschedulemap.belongsTo(dbSub.subscriptions, {
                foreignKey: "subscriptionId",
                targetKey: "subscriptionId"
            });


            dbSub.subscriptions.belongsTo(dbPP.pricepacks, {
                foreignKey: "pricepacks",
                targetKey: "pricepackId"
            });

            dbPP.pricepacks.belongsTo(dbSPM.servicepricepackmap, {
                foreignKey: "pricepackId",
                targetKey: "pricepackId"
            });

            var singleclientschedulemap = await
            dbCSM.clientschedulemap.findAll({
                include: [{
                    model: dbSub.subscriptions,
                    attributes: {
                        include: [],
                        exclude: []
                    },
                    include: [{
                        model: dbPP.pricepacks,
                        attributes: {
                            include: [],
                            exclude: []
                        },
                        include: [{
                            model: dbSPM.servicepricepackmap,
                            attributes: {
                                include: [],
                                exclude: []
                            }
                        }],
                    }],
                }],
                where: {
                    businessId: businessId,
                    subscriptionId: subscriptionId,
                },
                order: [
                    ["createDateTime", "ASC"]
                ],
                limit: 1,
                raw: true
            });
            if (totalSessionsAttended != "") {
                if ((singleclientschedulemap.length > 0)) {
                    if ((singleclientschedulemap[0]['subscription.totalSessionsAttended'] == null) || (totalSessionsAttended == '0') || (singleclientschedulemap[0]['subscription.totalSessionsAttended'] == '0')) {
                        var singleservicepricepackmap = await
                        dbSPM.servicepricepackmap.findAll({
                            where: {
                                businessId: businessId,
                                pricepackId: singleclientschedulemap[0]['subscription.pricepack.pricepackId'],
                            },
                            raw: true
                        });
                        if (singleservicepricepackmap.length == 1) {
                            if (Number(singleservicepricepackmap[0].serviceDuration) >= (Number(totalSessionsAttended) + Number(singleclientschedulemap[0].sessionCount))) {
                                if ((singleclientschedulemap[0]['subscription.totalSessionsAttended'] == "") || (singleclientschedulemap[0]['subscription.totalSessionsAttended'] == null)) {
                                    await dbCSM.clientschedulemap.update({
                                        sessionCount: Number(singleclientschedulemap[0].sessionCount) +
                                            Number(totalSessionsAttended),
                                        updateDateTime: Date.now()
                                    }, {
                                        where: {
                                            scheduleId: singleclientschedulemap[0].parentScheduleId
                                        }
                                    });
                                }
                            } else {
                                throw new ApplicationError("You can not add more " + singleservicepricepackmap[0].serviceDuration + " service duration.", 401);
                            }
                        } else {
                            throw new ApplicationError("You can not edit session attended in multi service combo pricepack.", 401);
                        }
                    }
                } else {
                    throw new ApplicationError("Please create  schedule first", 401);
                }
            }

            dbCSM.sequelize.close();
            dbSchedule.sequelize.close();
            dbPP.sequelize.close();
            dbSPM.sequelize.close();


            function addDays(theDate, days) {
                return new Date(theDate.getTime() + days * 24 * 60 * 60 * 1000);
            }

            var dateValue = 0,
                numberValue = 0;
            if (payment_due_date.includes("-")) {
                dateValue = new Date(payment_due_date);
            } else {
                numberValue = parseInt(payment_due_date);
            }

            if (installment_due_on == 1 && numberValue) {
                throw new ApplicationError(
                    "Error in payment_due_date and installment_due_on",
                    401
                );
            }

            if (installment_due_on == 2 && dateValue) {
                throw new ApplicationError(
                    "Error in payment_due_date and installment_due_on",
                    401
                );
            }

            if (installment_due_on == 3 && numberValue) {
                throw new ApplicationError(
                    "Error in payment_due_date and installment_due_on",
                    401
                );
            }

            if (due_amount < 0) {
                throw new ApplicationError("due_amount should be +ve", 401);
            }

            if (installment_frequency == 6 && installment_due_on == 3) {
                throw new ApplicationError("Please select specefic date", 401);
            }

            if (installment_due_on < 0 || installment_due_on > 3) {
                throw new ApplicationError(
                    "Error in installment_due_on. 1: specific date, 2: day after client joining date, 3: client joining date.",
                    401
                );
            }

            var subscriptionDateTime = new Date();
            var dbPayment = createInstance(["clientPayments"], information);
            var dbClient = createInstance(["client"], information);
            let dbPricepack = createInstance(["clientPricepack"], information);
            let dbservices = createInstance(["clientService"], information);
            let dbservicepricepackmap = createInstance(["servicePricepackMap"], information);
            dbservicepricepackmap.servicepricepackmap.belongsTo(
                dbPricepack.pricepacks, {
                    foreignKey: "pricepackId",
                    targetKey: "pricepackId"
                }
            );
            dbservicepricepackmap.servicepricepackmap.belongsTo(dbservices.services, {
                foreignKey: "serviceId",
                targetKey: "serviceId"
            });

            // let isclientjoin = installment_due_on == 3 ? subscriptionDateTime : new Date(payment_due_date);

            let isclientjoin;


            if (installment_frequency == 6) {
                isclientjoin = subscriptionDateTime;
            } else {
                isclientjoin = installment_due_on == 3 ? subscriptionDateTime : new Date(payment_due_date);
            }



            let invoice_number = Date.now() + Math.floor(Math.random() * 8999 + 1000);
            let subcriptiondate = subscriptionDateTime;

            // if(installment_frequency == 6){
            // subcriptiondate = subscriptionDateTime;
            // }else{
            // subcriptiondate = (installment_due_on == 3) ? subscriptionDateTime : payment_due_date;
            // }



            if (listclientPaymentsTotal.length == 0) {
                let update_client_payment = await dbPayment.clientpayments.update( //deleting old payment details
                    {
                        status: 0
                    }, {
                        where: {
                            subscriptionsId: subscriptionId,
                            payble_amount: null, //the rows with paid_amount (which are not null) will not be updated. they will still be there
                            status: 1
                        }
                    }
                );
            }

            /* One Time Payment */
            if (payment_type == 1) {
                //one time payment
                var temp_payment_due_date = payment_due_date;
                //Get pricepack amount
                var pricepack = await dbPricepack.pricepacks.findAll({
                    where: {
                        //status: 1,
                        pricepackId: pricepacks
                    },
                    raw: true
                });

                let serviceAll = await dbservicepricepackmap.servicepricepackmap.findAll({
                    attributes: {
                        include: [],
                        exclude: [
                            "servicepricepackmapId",
                            "businessId",
                            "serviceDuration",
                            "createDateTime",
                            "updateDateTime",
                            "status"
                        ]
                    },
                    where: {
                        pricepackId: pricepacks
                    },
                    include: [{
                            model: dbservices.services,
                            attributes: {
                                include: [],
                                exclude: [
                                    "serviceId",
                                    "serviceName",
                                    "businessId",
                                    "serviceDetails",
                                    "createDateTime",
                                    "updateDateTime",
                                    "status"
                                ]
                            }
                        },
                        {
                            model: dbPricepack.pricepacks,
                            attributes: [
                                ["amount", "pricepack_amount"]
                            ]
                        }
                    ],
                    raw: true
                });
                dbservices.sequelize.close();
                dbservicepricepackmap.sequelize.close();
                let priceCalculation = await getPaymentCalculation(
                    information,
                    serviceAll[0]["pricepack.pricepack_amount"],
                    serviceAll[0].serviceId,
                    serviceAll[0].pricepackId
                );

                var clients = [];
                clients = await commaSeparatedValues(clientId);
                var info = [];



                for (var c = 0; c < clients.length; c++) {
                    var clientId = clients[c];
                    var subscriptionsUpdate = await dbSub.subscriptions.update({
                        subscriptionDateTime: subcriptiondate,
                        installment_frequency: null,
                        numberOfInstallments: null,
                        totalSessionsAttended: (totalSessionsAttended) ? totalSessionsAttended : null,
                        amountPaidSoFar: (amountPaidSoFar) ? amountPaidSoFar : null,
                        status_amountPaidSoFar: (amountPaidSoFar) ? 1 : null,
                        payment_type: 1,
                        updateDateTime: Date.now(),
                        clientJoiningDate: isclientjoin
                    }, {
                        where: {
                            businessId: businessId,
                            subscriptionId: subscriptionId,
                            status: 1
                        }
                    });
                    var subscriptions = await dbSub.subscriptions.findAll({
                        where: {
                            businessId: businessId,
                            subscriptionId: subscriptionId,
                            status: 1
                        }
                    });

                    if (installment_due_on == 1) {
                        //specific date of month
                        var now = new Date(payment_due_date);
                    } else if (installment_due_on == 3) {
                        //joining date
                        var now = new Date();
                    }
                    var current_date = now; //in the second iteration it is gonna hold a date value. so we have taken temp_payment_due_date(int)

                    if (listclientPaymentsTotal.length == 0) {
                        var clientpayments = await dbPayment.clientpayments.create({
                            invoice_number,
                            businessId,
                            payment_type,
                            subscriptionsId: subscriptions[0].subscriptionId,
                            due_amount: priceCalculation.payableAmount,
                            payment_due_date: current_date,
                            payment_end_date: current_date,
                            is_last_payment: 1,
                            curent_subcription_number: 1,
                            paymentReminder_date: current_date,
                        });
                    }
                    info = info.concat(subscriptions);
                }
            } else if (payment_type == 2) {
                /* One Time Payment Completed */

                /* Installment Payment */
                //If payment type installment(2) then required installment_frequency and numberOfInstallments
                if (
                    installment_frequency == null ||
                    numberOfInstallments == null ||
                    numberOfInstallments == "" ||
                    installment_frequency == ""
                ) {
                    throw new ApplicationError(
                        "Error in numberOfInstallments or installment_frequency",
                        401
                    );
                }

                var payment_due_date_array = [];
                var payment_end_date_array = [];

                if (installment_frequency == 1) {
                    //daily
                    if (installment_due_on == 1) {
                        //specific date of month
                        var now = new Date(payment_due_date);
                    } else if (installment_due_on == 3) {
                        //joining date
                        var now = new Date();
                    }
                    for (i = 0; i < numberOfInstallments; i++) {
                        payment_due_date_array[i] = addDays(now, i);
                        payment_end_date_array[i] = addDays(now, i);
                    }
                } else if (installment_frequency == 2) {
                    //weekly
                    if (installment_due_on == 1) {
                        //specific date of month
                        var now = new Date(payment_due_date);
                    } else if (installment_due_on == 2) {
                        //days after joining date
                        var today = new Date();
                        var now = addDays(today, payment_due_date);
                    } else if (installment_due_on == 3) {
                        //joining date
                        var now = new Date();
                    }
                    var last_update_date = "";
                    var inst_start_date = "";
                    for (var i = 0; i < numberOfInstallments; i++) {
                        let payment_due_date_temp;
                        let payment_end_date_temp;
                        var nowModifyformatDate = await formatDate(now);
                        if (i == 0) {
                            payment_due_date_array[i] = nowModifyformatDate;
                            payment_end_date_array[i] = nowModifyformatDate;
                            last_update_date = nowModifyformatDate;
                            inst_start_date = await getPlusDays(last_update_date, 7);
                        } else {
                            last_update_date = await getPlusDays(last_update_date, 7);
                            inst_start_date = await getPlusDays(payment_due_date_array[i - 1], 1);
                            payment_due_date_array[i] = last_update_date;
                            payment_end_date_array[i] = inst_start_date;
                        }
                    }
                } else if (installment_frequency == 3) {
                    //monthly
                    if (installment_due_on == 1) {
                        //specific date of month
                        var now = new Date(payment_due_date);
                    } else if (installment_due_on == 3) {
                        //joining date
                        var now = new Date();
                    }
                    var last_update_date = "";
                    var inst_start_date = "";
                    for (var i = 0; i < numberOfInstallments; i++) {
                        let payment_due_date_temp;
                        let payment_end_date_temp;
                        var nowModifyformatDate = await formatDate(now);
                        if (i == 0) {
                            payment_due_date_array[i] = nowModifyformatDate;
                            payment_end_date_array[i] = nowModifyformatDate;
                            last_update_date = nowModifyformatDate;
                            inst_start_date = await getPlusDays(last_update_date, 1);
                        } else {
                            last_update_date = await getPlusMonths(last_update_date, 1);
                            inst_start_date = await getPlusDays(payment_due_date_array[i - 1], 1);
                            payment_due_date_array[i] = last_update_date;
                            payment_end_date_array[i] = inst_start_date;
                        }
                    }
                } else if (installment_frequency == 4) {
                    //quarterly
                    if (installment_due_on == 1) {
                        //specific date of month
                        var now = new Date(payment_due_date);
                    } else if (installment_due_on == 3) {
                        //joining date
                        var now = new Date();
                    }
                    var last_update_date = "";
                    var inst_start_date = "";
                    for (var i = 0; i < numberOfInstallments; i++) {
                        let payment_due_date_temp;
                        let payment_end_date_temp;
                        var nowModifyformatDate = await formatDate(now);
                        if (i == 0) {
                            payment_due_date_array[i] = nowModifyformatDate;
                            payment_end_date_array[i] = nowModifyformatDate;
                            last_update_date = nowModifyformatDate;
                            inst_start_date = await getPlusDays(last_update_date, 3);
                        } else {
                            last_update_date = await getPlusMonths(last_update_date, 3);
                            inst_start_date = await getPlusDays(payment_due_date_array[i - 1], 1);
                            payment_due_date_array[i] = last_update_date;
                            payment_end_date_array[i] = inst_start_date;
                        }
                    }
                } else if (installment_frequency == 5) {
                    //yearly
                    if (installment_due_on == 1) {
                        //specific date of month
                        var now = new Date(payment_due_date);
                    } else if (installment_due_on == 3) {
                        //joining date
                        var now = new Date();
                    }
                    var last_update_date = "";
                    var inst_start_date = "";
                    for (var i = 0; i < numberOfInstallments; i++) {
                        let payment_due_date_temp;
                        let payment_end_date_temp;
                        var nowModifyformatDate = await formatDate(now);
                        if (i == 0) {
                            payment_due_date_array[i] = nowModifyformatDate;
                            payment_end_date_array[i] = nowModifyformatDate;
                            last_update_date = nowModifyformatDate;
                            inst_start_date = await getPlusDays(last_update_date, 12);
                        } else {
                            last_update_date = await getPlusMonths(last_update_date, 12);
                            inst_start_date = await getPlusDays(payment_due_date_array[i - 1], 1);
                            payment_due_date_array[i] = last_update_date;
                            payment_end_date_array[i] = inst_start_date;
                        }
                    }
                } else if (installment_frequency == 6) {
                    //others
                    var date = await commaSeparatedValues(payment_due_date);
                    if (date.length != numberOfInstallments) {
                        throw new ApplicationError(
                            "Error ! Mismatch in payment_due_date and numberOfInstallments",
                            401
                        );
                    }
                    var amtDue = await commaSeparatedValues(due_amount);
                    if (amtDue.length != date.length) {
                        throw new ApplicationError(
                            "Error ! Mismatch in due_amount and payment_due_date",
                            401
                        );
                    }

                    var insDueOn = await commaSeparatedValues(installment_due_on);
                    if (insDueOn.length != date.length) {
                        throw new ApplicationError(
                            "Error ! Mismatch in installment_due_on and payment_due_date",
                            401
                        );
                    }
                    var last_update_date = "";
                    var inst_start_date = "";
                    for (var pd = 0; pd < date.length; pd++) {
                        if (pd == 0) {
                            payment_due_date_array[pd] = new Date(date[pd]);
                            payment_end_date_array[pd] = new Date(date[pd]);
                            last_update_date = new Date(date[pd]);
                        } else {
                            payment_due_date_array[pd] = new Date(date[pd]);
                            last_update_date = await getPlusDays(payment_due_date_array[pd - 1], 1);
                            payment_end_date_array[pd] = last_update_date;
                        }
                    }
                } else {
                    throw new ApplicationError(
                        "error in installment_frequecy. 1: daily. 2: weekly, 3: monthly, 4: quarterly, 5: yearly, 6: others",
                        401
                    );
                }

                var info = [];
                var clients = [];
                clients = await commaSeparatedValues(clientId);
                for (var c = 0; c < clients.length; c++) {
                    let dbSub = createInstance(["clientSubscription"], information);
                    var clientId = clients[c];
                    // let subscriptions = await dbSub.subscriptions.create({
                    //     clientId,
                    //     businessId,
                    //     pricepacks,
                    //     payment_type,
                    //     installment_frequency,
                    //     // frequency_number,
                    //     numberOfInstallments,
                    //     subscriptionDateTime: subcriptiondate,
                    //     clientJoiningDate: isclientjoin
                    // });

                    // await dbSub.subscriptions.update({
                    //     parentSubscriptionId: subscriptions.subscriptionId
                    // }, {
                    //     where: {
                    //         status: 1,
                    //         subscriptionId: subscriptions.subscriptionId
                    //     }
                    // });


                    var subscriptionsUpdate = await dbSub.subscriptions.update({
                        subscriptionDateTime: subcriptiondate,
                        installment_frequency,
                        numberOfInstallments,
                        totalSessionsAttended: (totalSessionsAttended) ? totalSessionsAttended : null,
                        amountPaidSoFar: (amountPaidSoFar) ? amountPaidSoFar : 0,
                        status_amountPaidSoFar: (Number(amountPaidSoFar) > 0) ? 1 : null,
                        payment_type: 2,
                        updateDateTime: Date.now(),
                        clientJoiningDate: isclientjoin
                    }, {
                        where: {
                            businessId: businessId,
                            subscriptionId: subscriptionId,
                            status: 1
                        }
                    });

                    var subscriptions = await dbSub.subscriptions.findAll({
                        where: {
                            businessId: businessId,
                            subscriptionId: subscriptionId,
                            status: 1
                        }
                    });




                    info = info.concat(subscriptions);
                    dbSub.sequelize.close();

                    var dbPayment = createInstance(["clientPayments"], information);

                    for (var i = 0; i < numberOfInstallments; i++) {
                        var payment_due_date = payment_end_date_array[i];
                        var payment_end_date = payment_due_date_array[i];
                        if (installment_frequency == 6) {
                            var due_amount = amtDue[i];
                        } else {
                            var due_amount = due_amount;
                        }
                        let serviceAll = await dbservicepricepackmap.servicepricepackmap.findAll({
                            attributes: {
                                include: [],
                                exclude: [
                                    "servicepricepackmapId",
                                    "businessId",
                                    "serviceDuration",
                                    "createDateTime",
                                    "updateDateTime",
                                    "status"
                                ]
                            },
                            where: {
                                pricepackId: pricepacks
                            },
                            include: [{
                                    model: dbservices.services,
                                    attributes: {
                                        include: [],
                                        exclude: [
                                            "serviceId",
                                            "serviceName",
                                            "businessId",
                                            "serviceDetails",
                                            "createDateTime",
                                            "updateDateTime",
                                            "status"
                                        ]
                                    }
                                },
                                {
                                    model: dbPricepack.pricepacks,
                                    attributes: [
                                        ["amount", "pricepack_amount"]
                                    ]
                                }
                            ],
                            raw: true
                        });

                        let priceCalculation = await getPaymentCalculation(
                            information,
                            serviceAll[0]["pricepack.pricepack_amount"],
                            serviceAll[0].serviceId,
                            serviceAll[0].pricepackId
                        );
                        let discountPercentage = parseFloat(priceCalculation.discountPercentage);
                        let gstCal = (parseFloat(priceCalculation.sgstPercentage) + parseFloat(priceCalculation.cgstPercentage)) / 100;
                        let due_amount_cal =
                            due_amount - due_amount * discountPercentage / 100;
                        let due_amount_cal_with_gst =
                            due_amount_cal + due_amount_cal * gstCal;
                        let is_last_payment = i == numberOfInstallments - 1 ? 1 : 0;

                        if (listclientPaymentsTotal.length == 0) {
                            var clientpayments = await dbPayment.clientpayments.create({
                                invoice_number,
                                businessId,
                                subscriptionsId: subscriptions[0].subscriptionId,
                                due_amount: due_amount_cal_with_gst,
                                payment_due_date,
                                payment_end_date,
                                payment_type,
                                is_last_payment,
                                curent_subcription_number: Number(i) + 1,
                                paymentReminder_date: payment_due_date,
                                // payment_mode
                            });
                        }
                    }

                    if (installment_frequency != 6) {
                        var pdd;
                        if (installment_due_on == 3) {
                            pdd = new Date();
                        } else {
                            pdd = payment_due_date_array[0];
                        }
                        //adding clientJoiningDate in client table
                        let updateClient = await dbClient.clients.update({
                            clientJoiningDate: pdd, //clientJoiningDate is the first payment day for each and every clients
                            updateDateTime: Date.now()
                        }, {
                            where: {
                                clientId: subscriptions.clientId
                            }
                        });
                    }
                    dbPayment.sequelize.close();
                }
            } else {
                /* installment payment completed */
                throw new ApplicationError(
                    "Please provide correct value for payment type. 1 for one time payment. 2 for installment payment.",
                    401
                );
            }



            dbPayment.sequelize.close();
            dbClient.sequelize.close();
            dbPricepack.sequelize.close();
            dbservices.sequelize.close();
            return await info;


        } catch (error) {
            throw error;
        }
    };


    listAll = async(information, businessId, filterPrivateFields = true) => {
        try {
            let db = createInstance(["clientSubscription"], information);
            // db.subscriptions.belongsTo(db.clients, {
            //   foreignKey: "clientId",
            //   targetKey: "clientId"
            // });
            // db.subscriptions.belongsTo(db.centers, {
            //   foreignKey: "centerId",
            //   targetKey: "centerId"
            // });
            // db.subscriptions.belongsTo(db.pricepacks, {
            //   foreignKey: "pricepacks",
            //   targetKey: "pricepackId"
            // });
            let subscriptions = await db.subscriptions.findAll({
                where: {
                    status: 1,
                    businessId: businessId
                },
                // include: [
                //   {
                //     model: db.clients,
                //     attributes: ["clientName"]
                //   },
                //   {
                //     model: db.centers,
                //     attributes: ["centerName"]
                //   },
                //   {
                //     model: db.pricepacks,
                //     attributes: ["pricepackName", "serviceName"]
                //   }
                // ],
                raw: true
            });
            db.sequelize.close();

            var new_date = Array();
            let pricepack_db = createInstance(["clientPricepack"], information);
            for (var i = 0; i < subscriptions.length; i++) {
                let service_pack_price = await pricepack_db.pricepacks.findAll({
                    where: {
                        status: 1,
                        businessId: businessId,
                        pricepackId: subscriptions[i].pricepacks
                    },
                    raw: true
                });

                var pricepack_amount = service_pack_price[0].amount; //Pricepack Amount
                var pending_amount = parseFloat(
                    service_pack_price[0].amount - subscriptions[i].totalAmountPaid
                );
                var perinstallment = parseFloat(
                    pending_amount / (subscriptions[i].numberOfInstallments - 1)
                );
                var date = new Date(subscriptions[i].payment_date);
                date.setDate(
                    date.getDate() + parseInt(subscriptions[i].installment_frequency)
                );
                new_date[i] = {
                    subscriptionId: subscriptions[i].subscriptionId,
                    clientId: subscriptions[i].clientId,
                    businessId: subscriptions[i].businessId,
                    centerId: subscriptions[i].centerId,
                    services: subscriptions[i].services,
                    pricepacks: subscriptions[i].pricepacks,

                    totalSessionUnit: subscriptions[i].totalSessionUnit,
                    totalSessionType: subscriptions[i].totalSessionType,

                    paymentType: subscriptions[i].paymentType,

                    subscriptionDateTime: subscriptions[i].subscriptionDateTime,
                    totalSessionsAttended: subscriptions[i].totalSessionsAttended,

                    payment_mode: subscriptions[i].payment_mode,
                    pricePackAmount: pricepack_amount,
                    totalAmountPaid: subscriptions[i].totalAmountPaid,
                    numberOfInstallments: subscriptions[i].numberOfInstallments,
                    installmentFrequency: subscriptions[i].installment_frequency,
                    pendingAmount: pending_amount,
                    paymentDate: subscriptions[i].payment_date,
                    payementDueDate: date,
                    perInstallmentPayment: perinstallment
                };
            }

            pricepack_db.sequelize.close();

            return await new_date;
            // if (filterPrivateFields) {
            //   return await filterFields(subscriptions, PUBLIC_FIELDS);
            // }
        } catch (error) {
            throw error;
        }
    };

    list = async(information, centerId, filterPrivateFields = true) => {
        try {
            let db = createInstance(["clientSubscription"], information);
            let client_db = createInstance(["client"], information);
            db.subscriptions.belongsTo(client_db.clients, {
                foreignKey: "clientId",
                targetKey: "clientId"
            });
            let center_db = createInstance(["clientCenter"], information);
            db.subscriptions.belongsTo(center_db.centers, {
                foreignKey: "centerId",
                targetKey: "centerId"
            });
            let pricepack_db = createInstance(["clientPricepack"], information);
            db.subscriptions.belongsTo(pricepack_db.pricepacks, {
                foreignKey: "pricepacks",
                targetKey: "pricepackId"
            });
            let subscriptions = await db.subscriptions.findAll({
                where: {
                    status: 1,
                    centerId: centerId
                },
                include: [{
                        model: client_db.clients,
                        attributes: ["clientName"]
                    },
                    {
                        model: center_db.centers,
                        attributes: ["centerName"]
                    },
                    {
                        model: pricepack_db.pricepacks,
                        attributes: ["pricepackName", "serviceName"]
                    }
                ],
                raw: true
            });
            db.sequelize.close();
            if (filterPrivateFields) {
                return await filterFields(subscriptions, PUBLIC_FIELDS);
            }
        } catch (error) {
            throw error;
        }
    };

    clientSubscription = async(
        information,
        clientId,
        filterPrivateFields = true
    ) => {
        try {
            let db = createInstance(["clientSubscription"], information);
            db.subscriptions.belongsTo(db.clients, {
                foreignKey: "clientId",
                targetKey: "clientId"
            });
            db.subscriptions.belongsTo(db.centers, {
                foreignKey: "centerId",
                targetKey: "centerId"
            });
            db.subscriptions.belongsTo(db.pricepacks, {
                foreignKey: "pricepacks",
                targetKey: "pricepackId"
            });
            let subscriptions = await db.subscriptions.findAll({
                where: {
                    status: 1,
                    clientId: clientId
                },
                include: [{
                        model: db.clients,
                        attributes: ["clientName"]
                    },
                    {
                        model: db.centers,
                        attributes: ["centerName"]
                    },
                    {
                        model: db.pricepacks,
                        attributes: ["pricepackName", "serviceName"]
                    }
                ],
                raw: true
            });
            db.sequelize.close();
            if (filterPrivateFields) {
                return await filterFields(subscriptions, PUBLIC_FIELDS);
            }
        } catch (error) {
            throw error;
        }
    };

    details = async(information, subscriptionId, filterPrivateFields = true) => {
        try {
            let todayFormat = await date_ISO_Convert(new Date());
            let businessIdinformation = information.split(",")[1];
            let dbclientSubscription = createInstance(["clientSubscription"], information);
            let dbclient = createInstance(["client"], information);
            let dbPricepack = createInstance(["clientPricepack"], information);
            let dbclientCenter = createInstance(["clientCenter"], information);
            let dbservicepricepackmap = createInstance(["servicePricepackMap"], information);
            let dbpaymentTransactions = createInstance(["paymentTransactions"], information);
            let dbclientpayments = createInstance(["clientPayments"], information);
            let dbservices = createInstance(["clientService"], information);
            let dbbusinessInfo = createInstance(["clientBusiness"], information);

            dbclientSubscription.subscriptions.belongsTo(dbclient.clients, {
                foreignKey: "clientId",
                targetKey: "clientId"
            });
            dbclientSubscription.subscriptions.belongsTo(dbPricepack.pricepacks, {
                foreignKey: "pricepacks",
                targetKey: "pricepackId"
            });
            dbclientpayments.clientpayments.belongsTo(dbpaymentTransactions.paymentTransactions, {
                foreignKey: "clientpaymentId",
                targetKey: "clientpaymentId"
            });
            dbservicepricepackmap.servicepricepackmap.belongsTo(dbservices.services, {
                foreignKey: "serviceId",
                targetKey: "serviceId"
            });
            dbclientSubscription.subscriptions.belongsTo(dbclientpayments.clientpayments, {
                foreignKey: "subscriptionId",
                targetKey: "subscriptionsId"
            });
            let subscriptions = await dbclientSubscription.subscriptions.findAll({
                attributes: {
                    include: [
                        "status_amountPaidSoFar",
                        "clientJoiningDate",
                        "subscriptionDateTime",
                        "amountPaidSoFar", [
                            dbclientSubscription.sequelize.literal(
                                'Date_add( `subscriptions`.`subscriptiondatetime`,INTERVAL `pricepack`.`pricepackvalidity` day)'
                            ),
                            "subcription_end_Date"
                        ],
                        [
                            dbclientSubscription.sequelize.literal("( SELECT IFNULL( SUM(`payble_amount`), 0 ) FROM `clientpayments` INNER join `paymentTransactions` AS `paymentTransactions` ON `clientpayments`.`clientpaymentId` = `paymentTransactions`.`clientpaymentId` WHERE `clientpayments`.`subscriptionsId` = `subscriptions`.`subscriptionId` )"),
                            "total_subcription_payemt"
                        ]
                    ],
                    exclude: []
                },
                where: {
                    status: 1,
                    subscriptionId: subscriptionId,
                    // [dbclientSubscription.sequelize.Op.and]: [
                    //     Sequelize.literal("( ( `payment_due_date` = '" + todayFormat + "' AND `payment_end_date` = '" + todayFormat + "' ) OR ( `payment_due_date` <= '" + todayFormat + "' AND `payment_end_date` >= '" + todayFormat + "' ) )")
                    // ]
                },
                include: [{
                        model: dbclient.clients,
                        attributes: ["clientName", "contactNumber", "emailId"]
                    },
                    {
                        model: dbPricepack.pricepacks,
                        attributes: ["pricepackName", "amount", "pricepackvalidity", "durationType"]
                    }
                ],
                raw: true
            });
            // return subscriptions;
            if (subscriptions) {
                let serviceAll = await dbservicepricepackmap.servicepricepackmap.findAll({
                    attributes: {
                        include: [],
                        exclude: [`servicepricepackmapId`, `serviceDuration_number`, `pricepackId`, `serviceId`, `businessId`, `serviceDuration`, `createDateTime`, `updateDateTime`, `status`]
                    },
                    where: {
                        status: 1,
                        pricepackId: subscriptions[0].pricepacks
                    },
                    include: [{
                        model: dbservices.services,
                        attributes: [
                            [dbservicepricepackmap.sequelize.fn('GROUP_CONCAT', dbservicepricepackmap.sequelize.col('serviceName')), 'serviceNameList']
                        ],
                    }],
                    raw: true
                });
                let serviceAllID = await dbservicepricepackmap.servicepricepackmap.findAll({
                    attributes: {
                        include: [],
                        exclude: [`servicepricepackmapId`, `pricepackId`, `serviceId`, `businessId`, `serviceDuration`, `createDateTime`, `updateDateTime`, `status`]
                    },
                    where: {
                        status: 1,
                        pricepackId: subscriptions[0].pricepacks
                    },
                    include: [{
                        model: dbservices.services,
                        attributes: [
                            'serviceId'
                        ],
                    }],
                    raw: true
                });
                subscriptions[0]["serviceIDList"] = serviceAllID;
                subscriptions[0]["serviceNameList"] = serviceAll;
                if (serviceAllID) {
                    for (var x = 0; x < serviceAllID.length; x++) {
                        let payble_amount_perPackage = subscriptions[0]["pricepack.amount"];
                        let serviceId_perPackage = serviceAllID[x]["service.serviceId"];
                        let pricepackId_perPackage = subscriptions[0].pricepackId;
                        subscriptions[0]["getPaymentCalculation"] = await getPaymentCalculation(information, payble_amount_perPackage, serviceId_perPackage, pricepackId_perPackage);
                    }
                }
                subscriptions[0]["sessionInfo"] = await sessionInfo(information, {
                    status: 1,
                    subscriptionId: subscriptionId
                });

                subscriptions[0]["businessinfo"] = await dbbusinessInfo.businessinfo.findAll({
                    attributes: {
                        include: [
                            [
                                dbbusinessInfo.sequelize.fn('SUBSTRING',
                                    dbbusinessInfo.sequelize.fn('SOUNDEX',
                                        dbbusinessInfo.sequelize.col('businessName')), 1, 1
                                ), 'name_initials'
                            ]
                        ],
                        exclude: []
                    },
                    where: {
                        businessId: businessIdinformation
                    },
                    raw: true
                });

                let listclientPayments = await dbclientpayments.clientpayments.findAll({
                    where: {
                        businessId: businessIdinformation,
                        subscriptionsId: subscriptionId,
                        status: 1,
                        "$payble_amount$": {
                            ne: null
                        }
                    },
                    raw: true
                });
                subscriptions[0]["getSubcriptionPaymentFormat"] = await this.getSubcriptionPaymentFormat(information, subscriptionId, subscriptions[0]['clientpayment.clientpaymentId']);
                subscriptions[0]["subcription_payment_status"] = listclientPayments.length;

            }
            dbclientSubscription.sequelize.close();
            dbclient.sequelize.close();
            dbPricepack.sequelize.close();
            dbclientCenter.sequelize.close();
            dbservicepricepackmap.sequelize.close();
            dbpaymentTransactions.sequelize.close();
            dbclientpayments.sequelize.close();
            dbservices.sequelize.close();
            dbbusinessInfo.sequelize.close();
            return subscriptions;
        } catch (error) {
            throw error;
        }
    };


    getSubcriptionPaymentFormat = async(information, subscriptionId, clientpaymentId) => {

        try {
            let paymentList = [];
            let dbclientpayments = createInstance(["clientPayments"], information);
            let dbclientSubscription = createInstance(["clientSubscription"], information);

            dbclientpayments.clientpayments.belongsTo(dbclientSubscription.subscriptions, {
                foreignKey: "subscriptionsId",
                targetKey: "subscriptionId"
            });
            paymentList = await dbclientpayments.clientpayments.findAll({
                attributes: {
                    include: ['due_amount'],
                    exclude: []
                },
                where: {
                    subscriptionsId: subscriptionId,
                    status: 1,
                    // clientpaymentId: clientpaymentId
                },
                raw: true
            });
            return paymentList;
        } catch (error) {
            throw error;
        }
    }




    getSubcritionRenewalDetails = async(information, subscriptionId, params, filterPrivateFields = true) => {
        try {
            let businessIdinformation = information.split(",")[1];
            let dbclientSubscription = createInstance(["clientSubscription"], information);
            let dbclient = createInstance(["client"], information);
            let dbPricepack = createInstance(["clientPricepack"], information);
            let dbclientCenter = createInstance(["clientCenter"], information);
            let dbservicepricepackmap = createInstance(["servicePricepackMap"], information);
            let dbpaymentTransactions = createInstance(["paymentTransactions"], information);
            let dbclientpayments = createInstance(["clientPayments"], information);
            let dbservices = createInstance(["clientService"], information);
            let dbbusinessSettings = createInstance(["businessSettings"], information);

            dbclientSubscription.subscriptions.belongsTo(dbclient.clients, {
                foreignKey: "clientId",
                targetKey: "clientId"
            });
            dbclientSubscription.subscriptions.belongsTo(dbPricepack.pricepacks, {
                foreignKey: "pricepacks",
                targetKey: "pricepackId"
            });
            dbclientpayments.clientpayments.belongsTo(dbpaymentTransactions.paymentTransactions, {
                foreignKey: "clientpaymentId",
                targetKey: "clientpaymentId"
            });
            dbservicepricepackmap.servicepricepackmap.belongsTo(dbservices.services, {
                foreignKey: "serviceId",
                targetKey: "serviceId"
            });
            dbclientSubscription.subscriptions.belongsTo(this.businessModel, {
                foreignKey: "businessId",
                targetKey: "businessId"
            });
            this.businessModel.belongsTo(dbbusinessSettings.businessSettings, {
                foreignKey: "businessId",
                targetKey: "businessId"
            });
            let subscriptions = await dbclientSubscription.subscriptions.findAll({
                attributes: {
                    include: [
                        [
                            dbclientSubscription.sequelize.literal(
                                'Date_add( `subscriptions`.`subscriptiondatetime`,INTERVAL `pricepack`.`pricepackvalidity` day)'
                            ),
                            "subcription_end_Date"
                        ],
                        [
                            dbclientSubscription.sequelize.literal("( SELECT IFNULL( SUM(`payble_amount`), 0 ) FROM `clientpayments` INNER join `paymentTransactions` AS `paymentTransactions` ON `clientpayments`.`clientpaymentId` = `paymentTransactions`.`clientpaymentId` WHERE `clientpayments`.`subscriptionsId` = `subscriptions`.`subscriptionId` )"),
                            "total_subcription_payemt"
                        ]
                    ],
                    exclude: []
                },
                where: {
                    status: 1,
                    subscriptionId: subscriptionId
                },
                include: [{
                        model: dbclient.clients,
                        attributes: {
                            include: [],
                            exclude: []
                        }
                    },
                    {
                        model: dbPricepack.pricepacks,
                        attributes: ["pricepackName", "amount", "pricepackvalidity"]
                    },
                    {
                        model: this.businessModel,
                        attributes: [
                            "businessName", [
                                this.businessModel.sequelize.fn(
                                    "SUBSTRING",
                                    this.businessModel.sequelize.fn(
                                        "SOUNDEX",
                                        this.businessModel.sequelize.col("businessName")
                                    ),
                                    1,
                                    1
                                ),
                                "businessName_initials"
                            ]
                        ],
                        include: [{
                            model: dbbusinessSettings.businessSettings,
                            attributes: {
                                include: [],
                                exclude: []
                            }
                        }]
                    }
                ],
                raw: true
            });
            if (subscriptions) {
                let serviceAll = await dbservicepricepackmap.servicepricepackmap.findAll({
                    attributes: {
                        include: [],
                        exclude: [`servicepricepackmapId`, `serviceDuration_number`, `pricepackId`, `serviceId`, `businessId`, `serviceDuration`, `createDateTime`, `updateDateTime`, `status`]
                    },
                    where: {
                        status: 1,
                        pricepackId: subscriptions[0].pricepacks
                    },
                    include: [{
                        model: dbservices.services,
                        attributes: [
                            [dbservicepricepackmap.sequelize.fn('GROUP_CONCAT', dbservicepricepackmap.sequelize.col('serviceName')), 'serviceNameList']
                        ],
                    }],
                    raw: true
                });
                let serviceAllID = await dbservicepricepackmap.servicepricepackmap.findAll({
                    attributes: {
                        include: [],
                        exclude: [`servicepricepackmapId`, `pricepackId`, `serviceId`, `businessId`, `serviceDuration`, `createDateTime`, `updateDateTime`, `status`]
                    },
                    where: {
                        status: 1,
                        pricepackId: subscriptions[0].pricepacks
                    },
                    include: [{
                        model: dbservices.services,
                        attributes: [
                            'serviceId'
                        ],
                    }],
                    raw: true
                });
                subscriptions[0]["serviceIDList"] = serviceAllID;
                subscriptions[0]["serviceNameList"] = serviceAll;
                if (serviceAllID) {
                    for (var x = 0; x < serviceAllID.length; x++) {
                        let payble_amount_perPackage = subscriptions[0]["pricepack.amount"];
                        let serviceId_perPackage = serviceAllID[x]["service.serviceId"];
                        let pricepackId_perPackage = subscriptions[0].pricepackId;
                        subscriptions[0]["getPaymentCalculation"] = await getPaymentCalculation(information, payble_amount_perPackage, serviceId_perPackage, pricepackId_perPackage);
                    }
                }
            }
            dbclientSubscription.sequelize.close();
            dbclient.sequelize.close();
            dbPricepack.sequelize.close();
            dbclientCenter.sequelize.close();
            dbservicepricepackmap.sequelize.close();
            dbpaymentTransactions.sequelize.close();
            dbclientpayments.sequelize.close();
            dbservices.sequelize.close();
            dbbusinessSettings.sequelize.close();
            if (params[0] == "share") {
                if (subscriptions[0]) {
                    var content = makeEmailTemplate(
                        "src/emailTemplate/SubcriptionRenewalReport.js",
                        subscriptions
                    );
                    const filenameRender = subscriptions[0]["parentSubscriptionId"] + '__' + Number(new Date());
                    fs.writeFile(
                        "uploads/subcriptionRenewalReport/html/" + filenameRender + ".html",
                        content,
                        function(err) {
                            if (err) {
                                return console.log(err);
                            } else {
                                var html = fs.readFileSync(
                                    "uploads/subcriptionRenewalReport/html/" + filenameRender + ".html",
                                    "utf8"
                                );
                                pdf1.create(html, {
                                        format: "Letter"
                                    })
                                    .toFile(
                                        "uploads/subcriptionRenewalReport/content/" + filenameRender + ".pdf",
                                        function(err, res) {
                                            if (err) return console.log(err);
                                        }
                                    );
                            }
                        }
                    );
                    sendEmail(
                        subscriptions[0]["client.emailId"],
                        "PAYMENT INVOICE ",
                        "INVOICE", {
                            filename: filenameRender + ".pdf",
                            path: "uploads/subcriptionRenewalReport/content/" + filenameRender + ".pdf",
                            contentType: "application/pdf"
                        }
                    );
                }
            }
            return subscriptions;
        } catch (error) {
            throw error;
        }
    };

    update = async(
        information,
        subscriptionId,
        clientId,
        businessId,
        centerId,
        services,
        pricepacks,
        totalSessionUnit,
        totalSessionType,
        offerId,
        taxGST,
        taxSGST,
        totalAmount,
        paymentType,
        numberOfInstallments,
        paymentDueDate,
        subscriptionDateTime
    ) => {
        try {
            let db = createInstance(["clientSubscription"], information);
            let subscriptions = await db.subscriptions.update({
                clientId: clientId,
                businessId: businessId,
                centerId: centerId,
                services: services,
                pricepacks: pricepacks,
                totalSessionUnit: totalSessionUnit,
                totalSessionType: totalSessionType,
                offerId: offerId,
                taxGST: taxGST,
                taxSGST: taxSGST,
                totalAmount: totalAmount,
                paymentType: paymentType,
                numberOfInstallments: numberOfInstallments,
                paymentDueDate: paymentDueDate,
                subscriptionDateTime: subscriptionDateTime,
                updateDateTime: Date.now()
            }, {
                where: {
                    subscriptionId: subscriptionId,
                    status: 1
                }
            });
            db.sequelize.close();
            return subscriptions;
        } catch (error) {
            throw error;
        }
    };


    remove = async(information, subscriptionId) => {
        try {
            let db = createInstance(["clientPayments"], information);
            let clientTotalPayment = await db.clientpayments.find({
                attributes: ['subscriptionsId', [Sequelize.fn('SUM', Sequelize.col('payble_amount')), 'totalPayment']],
                group: ["subscriptionsId"],
                where: {
                    status: 1,
                    subscriptionsId: subscriptionId
                },
                raw: true
            });
            db.sequelize.close();
            if (clientTotalPayment) { //if subscriptionId exists
                if (clientTotalPayment.totalPayment === null) { //delete if no payment is found
                    let db = createInstance(["clientSubscription"], information);
                    let subscriptions = await db.subscriptions.update({
                        status: 0,
                        updateDateTime: Date.now()
                    }, {
                        where: {
                            subscriptionId: subscriptionId
                        }
                    });
                    db.sequelize.close();
                    return subscriptions;
                } else { //if there is atleast one payment
                    throw new ApplicationError(
                        "This subscription cannot be deleted. there is atleast one payment",
                        401
                    );
                }
            } else { //if subscriptionId doesn't exist
                throw new ApplicationError(
                    "SubscriptionId doesn't exist",
                    404
                );
            }
        } catch (error) {
            throw error;
        }
    };

    listAllSubscriptionSchedule = async(
        information,
        businessId,
        clientId,
        filterPrivateFields = true
    ) => {
        try {
            let db = createInstance(["clientSubscription"], information);
            let subscriptions = await db.subscriptions.findAll({
                where: {
                    status: 1,
                    businessId: businessId,
                    clientId: clientId
                },
                raw: true
            });

            var dbClient = createInstance(["client"], information);
            var clientDetails = await dbClient.clients.findAll({
                where: {
                    status: 1,
                    clientId: clientId
                },
                raw: true
            });

            var merged = [{
                    clientDetails: clientDetails
                },
                {
                    subscriptions: subscriptions
                }
            ];

            db.sequelize.close();
            return await merged;
            // if (filterPrivateFields) {
            //   return await filterFields(subscriptions, PUBLIC_FIELDS);
            // }
        } catch (error) {
            throw error;
        }
    };


    listrenewalHistorySubcrition = async(
        information,
        subscriptionId,
        filterPrivateFields = true
    ) => {
        try {
            let businessIdinformation = information.split(",")[1];
            let dbclientSubscription = createInstance(["clientSubscription"], information);
            let dbclient = createInstance(["client"], information);
            let dbPricepack = createInstance(["clientPricepack"], information);
            let dbclientCenter = createInstance(["clientCenter"], information);
            let dbservicepricepackmap = createInstance(["servicePricepackMap"], information);
            let dbpaymentTransactions = createInstance(["paymentTransactions"], information);
            let dbclientpayments = createInstance(["clientPayments"], information);
            let dbservices = createInstance(["clientService"], information);
            dbclientSubscription.subscriptions.belongsTo(dbclient.clients, {
                foreignKey: "clientId",
                targetKey: "clientId"
            });
            dbclientSubscription.subscriptions.belongsTo(dbPricepack.pricepacks, {
                foreignKey: "pricepacks",
                targetKey: "pricepackId"
            });
            dbclientpayments.clientpayments.belongsTo(dbpaymentTransactions.paymentTransactions, {
                foreignKey: "clientpaymentId",
                targetKey: "clientpaymentId"
            });
            dbservicepricepackmap.servicepricepackmap.belongsTo(dbservices.services, {
                foreignKey: "serviceId",
                targetKey: "serviceId"
            });
            let subscriptionsRenewalList = await dbclientSubscription.subscriptions.findAndCount({
                attributes: {
                    include: [
                        [
                            dbclientSubscription.sequelize.literal(
                                'Date_add( `subscriptions`.`subscriptiondatetime`,INTERVAL `pricepack`.`pricepackvalidity` day)'
                            ),
                            "subcription_end_Date"
                        ],
                        [
                            dbclientSubscription.sequelize.literal('CASE WHEN Date_add( `subscriptions`.`subscriptiondatetime`, INTERVAL `pricepack`.`pricepackvalidity` day ) > CURDATE() THEN 1 ELSE 0 END'), "subcription_type_filter"
                        ]
                    ],
                    exclude: []
                },
                where: {
                    renewl_status: 0,
                    parentSubscriptionId: subscriptionId
                },
                include: [{
                        model: dbclient.clients,
                        attributes: ["clientName"]
                    },
                    {
                        model: dbPricepack.pricepacks,
                        attributes: ["pricepackName", "amount", "pricepackvalidity"]
                    }
                ],
                raw: true
            });
            if (subscriptionsRenewalList.rows.length > 0) {
                subscriptionsRenewalList.rows[0]["sessionInfo"] = await sessionInfo(information, {
                    status: 1,
                    subscriptionId: subscriptionId
                });
            }
            dbclientSubscription.sequelize.close();
            dbclient.sequelize.close();
            dbPricepack.sequelize.close();
            dbclientCenter.sequelize.close();
            dbservicepricepackmap.sequelize.close();
            dbpaymentTransactions.sequelize.close();
            dbclientpayments.sequelize.close();
            dbservices.sequelize.close();
            return subscriptionsRenewalList;
        } catch (error) {
            throw error;
        }
    };
}
export default new SubscriptionDB();