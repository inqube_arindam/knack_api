import { filterFields, createInstance } from "../../lib/common";
import { ApplicationError } from "../../lib/errors";
import { start } from "repl";
import Sequelize from "sequelize";
import BaseModel from "../../db/BaseModel";

const PUBLIC_FIELDS = [
  "clientScheduleMapId",
  "businessId",
  "scheduleId",
  "clientId",
  "clientName"
];

export class ClientScheduleMapDB extends BaseModel {
  clientschedulesmap = [];

  //Adding new schedule details
  create = async (information, businessId, scheduleId, clientId) => {
    const newClientScheduleMap = {
      businessId,
      scheduleId,
      clientId
    };
    try {
      let db = createInstance(["clientScheduleMap"], information);
      let checkSchedule = await db.clientschedulemap.findAll({
        where: {
          clientId: clientId,
          status: 1
        },
        raw: true
      });
      if (!checkSchedule[0]) {
        let clientschedulesmap = await db.clientschedulemap.create(
          newClientScheduleMap
        );
        db.sequelize.close();
        return await filterFields(clientschedulesmap, PUBLIC_FIELDS, true);
      } else {
        db.sequelize.close();
        throw new ApplicationError(
          "Selected client is already scheduled for selected date and time",
          409
        );
      }
    } catch (error) {
      throw error;
    }
  };

  list = async (information, scheduleId, filterPrivateFields = true) => {
    try {
      //code for taking all clients involved in a schedule from client-schedule-map table
      let db = createInstance(["clientScheduleMap"], information);
      let clients = await db.clientschedulemap.findAll({
        where: {
          status: 1,
          scheduleId: scheduleId
        },
        raw: true
      });
      //code ends

      //code for getting the client details by the value of clients from client-schedule-map table
      var clientDetails1 = [];
      var db = createInstance(["client"], information);
      for (var i = 0; i < clients.length; i++) {
        var clientDetails = await db.clients.findAll({
          where: {
            status: 1,
            clientId: clients[i].clientId
          },
          raw: true
        });
        clientDetails1 = clientDetails.concat(clientDetails1);
      }
      //code ends

      //code for taking all team involved in a schedule from team-schedule-map table
      let dbteam = createInstance(["teamScheduleMap"], information);
      let team = await dbteam.teamschedulemap.findAll({
        where: {
          status: 1,
          scheduleId: scheduleId
        },
        raw: true
      });
      //code ends

      //code for getting the team details by the value of team from team-schedule-map table
      var teamDetails1 = [];
      var dbteam = createInstance(["clientEmployee"], information);
      for (var i = 0; i < team.length; i++) {
        var teamDetails = await dbteam.employees.findAll({
          where: {
            status: 1,
            employeeId: team[i].teamId
          },
          raw: true
        });
        teamDetails1 = teamDetails.concat(teamDetails1);
      }
      //code ends

      //code for showing schedule details along with clients and team
      let dbschedule = createInstance(["clientSchedule"], information);
      let schedules = await dbschedule.schedule.findAll({
        where: {
          status: 1,
          scheduleId: scheduleId
        },
        raw: true
      });
      //code ends

      //finding batch details by batch id
      var bid = schedules[0].batchId;
      let dbbatch = createInstance(["clientBatches"], information);
      let batches = await dbbatch.batches.findAll({
        attributes: ["batchName"],
        where: {
          status: 1,
          batchId: bid
        },
        raw: true
      });

      /*merging two json objects : npm install merge-json */
      // var mergeJSON = require("merge-json");
      // // var obj1 = { a: true, b: false };
      // // var obj2 = { b: true, c: 12345 };
      // // var result = mergeJSON.merge(obj1, obj2);
      // var result = mergeJSON.merge(schedules, batches);
      // console.log(result);

      var merged = [];
      Array.prototype.push.apply(schedules, batches);
      merged = Object.assign.apply(Object, schedules);

      return await [
        { schedule: [merged] },
        { client: clientDetails1 },
        { team: teamDetails1 }
      ];
      // if (filterPrivateFields) {
      //   return await filterFields(clientDetails1, PUBLIC_FIELDS);
      // }
    } catch (error) {
      throw error;
    }
  };
}
export default new ClientScheduleMapDB();
