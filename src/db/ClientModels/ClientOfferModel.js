import { filterFields, createInstance } from "../../lib/common";
import { remove } from "../../routes/ClientRoutes/clientOffer";
const PUBLIC_FIELDS = [
  "id",
  "businessId",
  "centerId",
  "offerName",
  "offerType",
  "offerValue"
];

export class ClientOfferDB {
  offers = [];

  //Adding new offer details
  create = async (
    information,
    businessId,
    centerId,
    offerName,
    offerType,
    offerValue
  ) => {
    const newOffer = {
      businessId,
      centerId,
      offerName,
      offerType,
      offerValue
    };
    try {
      let db = createInstance(["clientOffer"], information);
      let offers = await db.offers.create(newOffer);
      db.sequelize.close();
      return await filterFields(offers, PUBLIC_FIELDS, true);
    } catch (error) {
      throw error;
    }
  };

  //Listing all the offers of a business
  listAll = async (information, businessId, filterPrivateFields = true) => {
    try {
      let db = createInstance(["clientOffer"], information);
      let offers = await db.offers.findAll({
        where: {
          businessId: businessId,
          status: 1
        },
        raw: true
      });
      db.sequelize.close();
      if (filterPrivateFields) {
        return await filterFields(offers, PUBLIC_FIELDS);
      }
    } catch (error) {
      throw error;
    }
  };

  //Listing all the offers of a center
  list = async (information, centerId, filterPrivateFields = true) => {
    try {
      let db = createInstance(["clientOffer"], information);
      let offers = await db.offers.findAll({
        where: {
          centerId: centerId,
          status: 1
        },
        raw: true
      });
      db.sequelize.close();
      if (filterPrivateFields) {
        return await filterFields(offers, PUBLIC_FIELDS);
      }
    } catch (error) {
      throw error;
    }
  };

  //Returning offer information
  details = async (information, id, filterPrivateFields = true) => {
    try {
      let db = createInstance(["clientOffer"], information);
      let offers = await db.offers.findAll({
        where: {
          id: id,
          status: 1
        },
        raw: true
      });
      db.sequelize.close();
      if (filterPrivateFields) {
        return await filterFields(offers, PUBLIC_FIELDS);
      }
    } catch (error) {
      throw error;
    }
  };

  //Update specific offer's information
  update = async (
    information,
    id,
    businessId,
    centerId,
    offerName,
    offerType,
    offerValue
  ) => {
    try {
      let db = createInstance(["clientOffer"], information);
      let offers = await db.offers.update(
        {
          businessId: businessId,
          centerId: centerId,
          offerName: offerName,
          offerType: offerType,
          offerValue: offerValue,
          updateDateTime: Date.now()
        },
        {
          where: {
            id: id
          }
        }
      );
      db.sequelize.close();
      return await offers;
    } catch (error) {
      throw error;
    }
  };

  //Removing offer's information, i.e. setting status=0, multiple offers can be deleted by passing multiple ids using ,(comma) separator
  remove = async (information, id) => {
    try {
      let db = createInstance(["clientOffer"], information);
      let offers = await db.offers.update(
        {
          status: 0,
          updateDateTime: Date.now()
        },
        {
          where: {
            id: id
          }
        }
      );
      db.sequelize.close();
      return await offers;
    } catch (error) {
      throw error;
    }
  };
}
export default new ClientOfferDB();
