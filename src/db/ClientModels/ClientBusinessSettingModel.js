import { filterFields, createInstance } from "../../lib/common";
import Sequelize from "sequelize";
const PUBLIC_FIELDS = [
    "businessId",
    "businessSettingData"
];


export class BusinessSettingDB {
    businessSetting = [];

    create = async(
        information,
        businessId,
        businessSettingsData,
        businessSettingType
    ) => {
        try {
            let dataService = await this.middleWarebusinessSetting(information, businessId, businessSettingType, businessSettingsData);
            var businessSettingsData = dataService;
            let db = createInstance(["businessSettings"], information);
            var businessSettingsCheck = await db.businessSettings.find({
                where: {
                    businessId: businessId
                },
                raw: true
            });
            let newBusinessSetting = {
                businessId,
                businessSettingsData
            };
            if (businessSettingsCheck) {
                let businessSetting = await db.businessSettings.update({
                    businessId: businessId,
                    businessSettingsData: businessSettingsData,
                    updateDateTime: Date.now()
                }, {
                    where: {
                        businessId: businessId
                    }
                });
                db.sequelize.close();
                return await businessSettingsData;
            } else {
                let businessSetting = await db.businessSettings.create(
                    newBusinessSetting
                );
                db.sequelize.close();
                return await businessSettingsData;
            }
            return dataService;
        } catch (error) {
            throw error;
        }
    };
    middleWarebusinessSetting = async(
        information,
        businessId,
        businessSettingType,
        businessSettingsData
    ) => {
        let newrowSet = null;

        let rowSet = await getServiceList(information, businessId);
        if (rowSet) {
            newrowSet = JSON.parse(rowSet.businessSettingsData);
        }
        let businessSettingsGlobal = [{
                "notification": {
                    "attendance": 1,
                    "paymentReceipts": 1,
                    "dailyInternalUpdate": 1,
                    "reScheaduling": 1,
                    "cancellation": 1,
                    "renewalOfService": 1,
                    "paymentInvoceies": 1
                }
            },
            {
                "neftDetails": {
                    "bankName": null,
                    "accountNumber": null,
                    "accountHolderName": null,
                    "ifscCode": null,
                    "acceptOnlinePayment": 0
                }
            },
            {
                "gstDetails": {
                    "gstApplicable": 0,
                    "sgst": null,
                    "cgst": null,
                    "gstnNumber": null
                }
            },
            {
                "paymentReminder": {
                    "sessionBased": {
                        "70%sessionBased": 0,
                        "0%sessionBased": 0,
                        "5daysBeforeValidity": 0,
                        "1daysBeforeValidity": 0
                    },
                    "anothersessionBased": {
                        "1daysBeforeDueDate": 0,
                        "3daysBeforeDueDate": 0,
                        "5daysBeforeDueDate": 0
                    }
                }
            },
            {
                "discountsOffers": {
                    "allServices": 0,
                    "selectedServices": []
                }
            },
            {
                "termsAndConditionsDetails": {
                    "content": null
                }
            }
        ];
        if (businessSettingType == "notification") {
            if (newrowSet == null) {
                businessSettingsGlobal[0].notification = {};
            }
            businessSettingsGlobal[0].notification = {
                attendance: parseInt(businessSettingsData.attendance),
                paymentReceipts: parseInt(businessSettingsData.paymentReceipts),
                dailyInternalUpdate: parseInt(businessSettingsData.dailyInternalUpdate),
                reScheaduling: parseInt(businessSettingsData.reScheaduling),
                cancellation: parseInt(businessSettingsData.cancellation),
                renewalOfService: parseInt(businessSettingsData.renewalOfService),
                paymentInvoceies: parseInt(businessSettingsData.paymentInvoceies)
            };
            if (newrowSet) {
                businessSettingsGlobal[1].neftDetails = newrowSet[1].neftDetails;
                businessSettingsGlobal[2].gstDetails = newrowSet[2].gstDetails;
                businessSettingsGlobal[3].paymentReminder = newrowSet[3].paymentReminder;
                businessSettingsGlobal[4].discountsOffers = newrowSet[4].discountsOffers;
                businessSettingsGlobal[5].termsAndConditionsDetails = newrowSet[5].termsAndConditionsDetails;
            }
        }
        if (businessSettingType == "neftDetails") {
            if (newrowSet == null) {
                businessSettingsGlobal[1].neftDetails = {};
            }
            businessSettingsGlobal[1].neftDetails = {
                bankName: businessSettingsData.bankName,
                accountNumber: parseInt(businessSettingsData.accountNumber),
                accountHolderName: businessSettingsData.accountHolderName,
                ifscCode: businessSettingsData.ifscCode,
                acceptOnlinePayment: parseInt(businessSettingsData.acceptOnlinePayment)
            };
            if (newrowSet) {
                businessSettingsGlobal[0].notification = newrowSet[0].notification;
                businessSettingsGlobal[2].gstDetails = newrowSet[2].gstDetails;
                businessSettingsGlobal[3].paymentReminder = newrowSet[3].paymentReminder;
                businessSettingsGlobal[4].discountsOffers = newrowSet[4].discountsOffers;
                businessSettingsGlobal[5].termsAndConditionsDetails = newrowSet[5].termsAndConditionsDetails;
            }
        }
        if (businessSettingType == "gstDetails") {
            if (newrowSet == null) {
                businessSettingsGlobal[2].gstDetails = {};
            }
            businessSettingsGlobal[2].gstDetails = {
                gstApplicable: businessSettingsData.gstApplicable,
                sgst: parseInt(businessSettingsData.sgst),
                cgst: businessSettingsData.cgst,
                gstnNumber: businessSettingsData.gstnNumber
            };
            if (newrowSet) {
                businessSettingsGlobal[0].notification = newrowSet[0].notification;
                businessSettingsGlobal[1].neftDetails = newrowSet[1].neftDetails;
                businessSettingsGlobal[3].paymentReminder = newrowSet[3].paymentReminder;
                businessSettingsGlobal[4].discountsOffers = newrowSet[4].discountsOffers;
                businessSettingsGlobal[5].termsAndConditionsDetails = newrowSet[5].termsAndConditionsDetails;
            }
        }
        if (businessSettingType == "paymentReminder") {
            if (newrowSet == null) {
                businessSettingsGlobal[3].paymentReminder = {};
            }
            if (businessSettingsData.sessionBased) {
                businessSettingsGlobal[3].paymentReminder.sessionBased = {
                    "70%sessionBased": businessSettingsData.sessionBased["70%sessionBased"],
                    "0%sessionBased": businessSettingsData.sessionBased["0%sessionBased"],
                    "5daysBeforeValidity": businessSettingsData.sessionBased["5daysBeforeValidity"],
                    "1daysBeforeValidity": businessSettingsData.sessionBased["1daysBeforeValidity"]
                };
                if (newrowSet) {
                    businessSettingsGlobal[3].paymentReminder.anothersessionBased = newrowSet[3].paymentReminder.anothersessionBased;
                }
            } else {
                businessSettingsGlobal[3].paymentReminder.anothersessionBased = {
                    "1daysBeforeDueDate": businessSettingsData.anothersessionBased["1daysBeforeDueDate"],
                    "3daysBeforeDueDate": businessSettingsData.anothersessionBased["3daysBeforeDueDate"],
                    "5daysBeforeDueDate": businessSettingsData.anothersessionBased["5daysBeforeDueDate"]
                };
                if (newrowSet) {
                    businessSettingsGlobal[3].paymentReminder.sessionBased = newrowSet[3].paymentReminder.sessionBased;
                }
            }
            if (newrowSet) {
                businessSettingsGlobal[0].notification = newrowSet[0].notification;
                businessSettingsGlobal[1].neftDetails = newrowSet[1].neftDetails;
                businessSettingsGlobal[2].gstDetails = newrowSet[2].gstDetails;
                businessSettingsGlobal[4].discountsOffers = newrowSet[4].discountsOffers;
                businessSettingsGlobal[5].termsAndConditionsDetails = newrowSet[5].termsAndConditionsDetails;
            }
        }
        if (businessSettingType == "discountsOffers") {
            if (newrowSet == null) {
                businessSettingsGlobal[4].discountsOffers = {
                    "allServices": 0,
                    "selectedServices": []
                };
            }
            let row = await getServiceList(information, businessId);
            var tempArr = {
                allServices: parseInt(businessSettingsData.allServices),
                selectedServices: [{
                    offerName: businessSettingsData.selectedServices[0].offerName,
                    serviceId: businessSettingsData.selectedServices[0].serviceId,
                    pricePackId: businessSettingsData.selectedServices[0].pricePackId,
                    amount: (businessSettingsData.selectedServices[0].amount) ? Math.round(businessSettingsData.selectedServices[0].amount).toFixed(2) : "0",
                    percentage: (businessSettingsData.selectedServices[0].percentage) ? parseInt(businessSettingsData.selectedServices[0].percentage) : "0",
                    status: 1
                }]
            };
            if (businessSettingsData.allServices == "1") {
                var tempArr = {
                    allServices: parseInt(businessSettingsData.allServices),
                    selectedServices: []
                };
                businessSettingsGlobal[4].discountsOffers = tempArr;
            } else {
                let rowbusinessSettingsData = JSON.parse(row.businessSettingsData);
                if (
                    rowbusinessSettingsData[4].discountsOffers.selectedServices.length > 0
                ) {
                    let manageData = [];
                    var Dataarray = tempArr.selectedServices[0].serviceId.split(",");
                    if (Dataarray.length > 0) {
                        for (var x = 0; x < Dataarray.length; x++) {
                            let singleData = {
                                offerName: tempArr.selectedServices[0].offerName,
                                serviceId: Dataarray[x],
                                pricePackId: tempArr.selectedServices[0].pricePackId,
                                amount: (Math.round(tempArr.selectedServices[0].amount * 100) / 100).toFixed(2),
                                percentage: parseInt(tempArr.selectedServices[0].percentage),
                                status: 1
                            };
                            manageData.push(singleData);
                        }
                    }
                    tempArr.selectedServices = manageData.concat(
                        rowbusinessSettingsData[4].discountsOffers.selectedServices
                    );
                    businessSettingsGlobal[4].discountsOffers = tempArr;
                } else {
                    let manageData = [];
                    var Dataarray = tempArr.selectedServices[0].serviceId.split(",");
                    if (Dataarray.length > 0) {
                        for (var x = 0; x < Dataarray.length; x++) {
                            let singleData = {
                                offerName: tempArr.selectedServices[0].offerName,
                                serviceId: Dataarray[x],
                                pricePackId: tempArr.selectedServices[0].pricePackId,
                                amount: (Math.round(tempArr.selectedServices[0].amount * 100) / 100).toFixed(2),
                                percentage: parseInt(tempArr.selectedServices[0].percentage),
                                status: 1
                            };
                            manageData.push(singleData);
                        }
                    }
                    tempArr.selectedServices = manageData;
                    businessSettingsGlobal[4].discountsOffers = tempArr;
                }
            }
            if (newrowSet) {
                businessSettingsGlobal[0].notification = newrowSet[0].notification;
                businessSettingsGlobal[1].neftDetails = newrowSet[1].neftDetails;
                businessSettingsGlobal[2].gstDetails = newrowSet[2].gstDetails;
                businessSettingsGlobal[3].paymentReminder = newrowSet[3].paymentReminder;
                businessSettingsGlobal[5].termsAndConditionsDetails = newrowSet[5].termsAndConditionsDetails;
            }
        }
        if (businessSettingType == "termsAndConditions") {
            if (newrowSet == null) {
                businessSettingsGlobal[5].termsAndConditionsDetails = {};
            }
            businessSettingsGlobal[5].termsAndConditionsDetails = {
                content: businessSettingsData.content
            };
            if (newrowSet) {
                businessSettingsGlobal[0].notification = newrowSet[0].notification;
                businessSettingsGlobal[1].neftDetails = newrowSet[1].neftDetails;
                businessSettingsGlobal[2].gstDetails = newrowSet[2].gstDetails;
                businessSettingsGlobal[3].paymentReminder = newrowSet[3].paymentReminder;
                businessSettingsGlobal[4].discountsOffers = newrowSet[4].discountsOffers;
            }
        }

        return businessSettingsGlobal;
    };


    businessSettingDetails = async(
        information,
        filterPrivateFields = true
    ) => {
        try {
            let db = createInstance(["businessSettings"], information);
            let businessIdinformation = information.split(",")[1];
            let businessSettingData = await db.businessSettings.findAll({
                where: {
                    status: 1,
                    businessId: businessIdinformation
                },
                raw: true
            });
            db.sequelize.close();
            if (filterPrivateFields) {
                if (businessSettingData[0].businessSettingsData) {
                    businessSettingData[0].businessSettingsData = JSON.parse(businessSettingData[0].businessSettingsData);
                }
                return businessSettingData;
            }
        } catch (error) {
            throw error;
        }
    };


    listservicepricepack = async(information, serviceId) => {
        try {

            let modifypricePackIdList;
            let businessIdinformation = information.split(",")[1];
            let db = createInstance(["businessSettings"], information);
            let dbservicepricepackmap = createInstance(
                ["servicePricepackMap"],
                information
            );
            let dbpricepacks = createInstance(["clientPricepack"], information);
            dbservicepricepackmap.servicepricepackmap.belongsTo(
                dbpricepacks.pricepacks, {
                    foreignKey: "pricepackId",
                    targetKey: "pricepackId"
                }
            );

            let businessSettingData = await db.businessSettings.findAll({
                where: {
                    status: 1,
                    businessId: businessIdinformation
                },
                raw: true
            });
            db.sequelize.close();

            if (businessSettingData) {
                businessSettingData[0].businessSettingsData = JSON.parse(
                    businessSettingData[0].businessSettingsData
                );
                let discountList =
                    businessSettingData[0].businessSettingsData[4].discountsOffers
                    .selectedServices;
                let pricePackIdList = [];
                if (discountList) {
                    // console.log(discountList.length);
                    for (let i = 0; i < discountList.length; i++) {
                        if (discountList[i].serviceId == serviceId) {
                            if (discountList[i].status == 1) {
                                let newpricePackId = '"' + discountList[i].pricePackId + '"';
                                pricePackIdList.push(newpricePackId);
                            }
                        }
                    }
                }
                modifypricePackIdList = pricePackIdList.join();
            }
            let whereCondition = {
                status: 1,
                serviceId: serviceId,
                businessId: businessIdinformation,
                // pricepackId: {
                //     [dbservicepricepackmap.sequelize.Op.not]: {
                //         [modifypricePackIdList]
                //     }
                // }
            };
            let pricepacklist = await dbservicepricepackmap.servicepricepackmap.findAll({
                attributes: {
                    include: [],
                    exclude: [
                        `servicepricepackmapId`,
                        `pricepackId`,
                        `businessId`,
                        `serviceDuration`,
                        `createDateTime`,
                        `updateDateTime`,
                        `status`
                    ]
                },
                where: whereCondition,
                include: [{
                    model: dbpricepacks.pricepacks,
                    attributes: ["pricepackId", "pricepackName"]
                }],
                raw: true
            });

            return pricepacklist;
        } catch (error) {
            throw error;
        }
    };


}

export async function getServiceList(information, businessId) {
    let db = createInstance(["businessSettings"], information);
    var businessSettingsRow = await db.businessSettings.find({
        where: {
            businessId: businessId
        },
        raw: true
    });
    return businessSettingsRow;
}

export default new BusinessSettingDB();