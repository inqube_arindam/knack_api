import { filterFields, createInstance } from "../../lib/common";
import { ApplicationError } from "../../lib/errors";
const PUBLIC_FIELDS = [
  "batchId",
  "centerId",
  "businessId",
  "serviceId",
  "batchName",
  "batchSize",
  "startTime",
  "endTime"
];

export class BatchDB {
  batch = [];
  create = async (
    information,
    businessId,
    centerId,
    serviceId,
    batchName,
    batchSize,
    startTime,
    endTime
  ) => {
    const newEmployee = {
      businessId,
      centerId,
      serviceId,
      batchName,
      batchSize,
      startTime,
      endTime
    };
    try {
      let db = createInstance(["clientBatches"], information);
      let batch = await db.batches.create(newEmployee);
      db.sequelize.close();
      return await filterFields(batch, PUBLIC_FIELDS, true);
    } catch (error) {
      throw error;
    }
  };

  listAll = async (
    information,
    centerId,
    serviceId,
    filterPrivateFields = true
  ) => {
    try {
      let db = createInstance(["clientBatches"], information);
      let batch = await db.batches.findAll({
        where: {
          status: 1,
          centerId: centerId,
          serviceId: serviceId
        },
        raw: true
      });
      db.sequelize.close();
      if (filterPrivateFields) {
        return await filterFields(batch, PUBLIC_FIELDS);
      }
    } catch (error) {
      throw error;
    }
  };

  batchDetails = async (information, batchId, filterPrivateFields = true) => {
    try {
      let db = createInstance(["clientBatches"], information);
      let batch = await db.batches.findAll({
        where: {
          status: 1,
          batchId: batchId
        },
        raw: true
      });
      db.sequelize.close();
      if (filterPrivateFields) {
        return await filterFields(batch, PUBLIC_FIELDS);
      }
    } catch (error) {
      throw error;
    }
  };

  update = async (
    information,
    batchId,
    businessId,
    serviceId,
    centerId,
    batchName,
    batchSize,
    startTime,
    endTime
  ) => {
    try {
      let db = createInstance(["clientBatches"], information);
      let batch = await db.batches.update(
        {
          centerId: centerId,
          businessId: businessId,
          serviceId: serviceId,
          batchName: batchName,
          batchSize: batchSize,
          startTime: startTime,
          endTime: endTime,
          updateDateTime: Date.now()
        },
        {
          where: {
            batchId: batchId
          }
        }
      );
      db.sequelize.close();
      return batch;
    } catch (error) {
      throw error;
    }
  };

  remove = async (information, batchId) => {
    try {
      let db = createInstance(["clientBatches"], information);
      let batch = await db.batches.update(
        {
          status: 0,
          updateDateTime: Date.now()
        },
        {
          where: {
            batchId: batchId
          }
        }
      );
      db.sequelize.close();
      return batch;
    } catch (error) {
      throw error;
    }
  };
}
export default new BatchDB();
