import { filterFields, createInstance, clientInfo } from "../../lib/common";
import { textNotification, mailNotification } from "../../lib/notification";
import {
    KNACKDB,
    KNACK_DBSETTINGS,
    KNACK_UPLOAD_URL,
    PEGINATION_LIMIT
} from "../../lib/constants.js";
import Sequelize from "sequelize";
const Op = Sequelize.Op;


const PUBLIC_FIELDS = [
    "messageId",
    "businessId",
    "serviceId",
    "senderId",
    "messageTypeId",
    "messageBody",
    "message_date_time"
];

var inArray = function (needle, haystack) {
    var length = haystack.length;
    for (var i = 0; i < length; i++) {
        if (haystack[i] == needle) return true;
    }
    return false;
}

export class MessagesDB {

    //===================Add New Message=============================
    add = async (information, serviceId, businessId, senderId, recipients, messageTypeId, messageBody, message_date_time) => {

        const newMessage = {
            businessId,
            senderId,
            recipients,
            messageTypeId,
            messageBody,
            message_date_time
        };

        try {

            let db = createInstance(["messages"], information);
            let mesageRecipientsdb = createInstance(['messageRecipients'], information);
            let subscriptionskmapdb = createInstance(['clientSubscription'], information);
            let mesageServicesdb = createInstance(['messageServices'], information);

            //insert message
            let message = await db.messages.create(newMessage);

            let messageId = message.dataValues.messageId;

            if (serviceId.indexOf(',') > -1) {
                var noOfServices = serviceId.split(",");
            } else {
                var noOfServices = [serviceId];
            }

            for (let i = 0; i < noOfServices.length; i++) {

                //--------------insert service to messageServices table---------------

                let serviceId = noOfServices[i];
                let recipientData = await mesageServicesdb.messageServices.create({
                    messageId,
                    businessId,
                    serviceId
                });

            }

            //===========IF RECIPIENT IS BLANK==================
            if (!recipients) {
                //-----------------fetch all clients who are subscribed to this service-----------------------------
                let servicepricepackmapdb = createInstance(['servicePricepackMap'], information);
                //Step 1 [Get all pricepacks of that serive from servicepricepackmap table]

                let pricepackArr = [];

                for (let i = 0; i < noOfServices.length; i++) {


                    let pricepacks = await servicepricepackmapdb.servicepricepackmap.findAll({
                        where: {
                            serviceId: noOfServices[i],
                            status: 1
                        },
                        raw: true
                    });



                    for (let i = 0; i < pricepacks.length; i++) {
                        pricepackArr.push(pricepacks[i].pricepackId);
                    }

                }


                //Step 2 [Get all clients from subscriptions table by pricepackId]

                let clients = await subscriptionskmapdb.subscriptions.findAll({
                    where: {
                        pricepacks: {
                            [Op.in]: pricepackArr
                        },
                        status: 1
                    },
                    raw: true
                });

                if (clients.length <= 0) {
                    throw new ApplicationError(
                        "No Clients found",
                        401
                    );
                }

                //console.log("Clients : ",clients);
                for (let i = 0; i < clients.length; i++) {
                    // insert recipient
                    let recipientId = clients[i].clientId;
                    let recipientData = await mesageRecipientsdb.messageRecipients.create({
                        messageId,
                        businessId,
                        senderId,
                        recipientId
                    });
                }

            } else {
                //===========IF RECIPIENT SELECTED==================

                if (recipients.indexOf(',') > -1) {
                    var noOfRecipients = recipients.split(",");
                } else {
                    var noOfRecipients = [recipients];
                }

                console.log("noOfRecipients : ", noOfRecipients);

                // insert recipient
                for (let i = 0; i < noOfRecipients.length; i++) {

                    let recipientId = noOfRecipients[i];
                    let recipientData = await mesageRecipientsdb.messageRecipients.create({
                        messageId,
                        businessId,
                        senderId,
                        recipientId
                    });

                }
            }

            //console.log(messageRecipient);

            db.sequelize.close();
            return await filterFields(message, PUBLIC_FIELDS, true);
        } catch (error) {
            throw error;
        }
    };


    listAllMessages = async (information, senderId, searchQuery, page, filterPrivateFields = true) => {
        try {
            var pageNo = (page) ? page : 1;
            var limitPerPage = 20;
            var offsetLimit = (pageNo - 1) * limitPerPage;
            let dbmessages = createInstance(["messages"], information);
            let dbmessageRecipients = createInstance(["messageRecipients"], information);
            let dbclient = createInstance(["client"], information);
            let dbmessageType = createInstance(["messageTypes"], information);
            let dbmessageService = createInstance(["messageServices"], information);
            let dbservice = createInstance(["clientService"], information);


            dbmessages.messages.belongsTo(dbmessageType.messageTypes, {
                foreignKey: "messageTypeId",
                targetKey: "messageTypeId"
            });

            dbmessageRecipients.messageRecipients.belongsTo(dbclient.clients, {
                foreignKey: "recipientId",
                targetKey: "clientId"
            });

            dbmessageRecipients.messageRecipients.belongsTo(dbclient.clients, {
                foreignKey: "recipientId",
                targetKey: "clientId"
            });

            dbmessageService.messageServices.belongsTo(dbservice.services, {
                foreignKey: "serviceId",
                targetKey: "serviceId"
            });



            let cond = {
                senderId: senderId,
                status: 1
            };

            if (searchQuery.key) {
                cond.messageBody = { [Op.like]: '%' + searchQuery.key + '%' };
            }

            let messagesList = await dbmessages.messages.findAndCountAll({
                attributes: ["messageId", "messageBody", "message_date_time"],
                where: cond,
                order: [
                    ['message_date_time', 'DESC']
                ],
                include: [{
                    model: dbmessageType.messageTypes,
                    attributes: [['messageTypeName', 'messageType']],
                }],
                offset: offsetLimit,
                limit: limitPerPage,
                raw: true
            });

            if (messagesList.rows.length > 0) {
                for (let x = 0; x < messagesList.rows.length; x++) {
                    messagesList.rows[x]["recipientList"] = await dbmessageRecipients.messageRecipients.findAll({
                        attributes: [],
                        where: { messageId: messagesList.rows[x]['messageId'] },
                        include: [{
                            model: dbclient.clients,
                            attributes: {
                                include: [
                                    [dbclient.sequelize.literal('CASE WHEN photoUrl is NULL THEN CONCAT("' + KNACK_UPLOAD_URL.localUrl + '" , "noimage.jpg" ) ELSE CONCAT("' + KNACK_UPLOAD_URL.localUrl + '" , photoUrl ) END'), 'photoUrl']
                                ],
                                exclude: ['createDateTime', 'updateDateTime', 'businessId', 'dateOfBirth', 'centerId', 'area', 'pin', 'city', 'alternateNumber', 'emailId', 'contactNumber']
                            }
                        }],
                        raw: true
                    });

                    messagesList.rows[x]["services"] = await dbmessageService.messageServices.findAll({
                        where: { messageId: messagesList.rows[x]['messageId'] },
                        include: [{
                            model: dbservice.services,
                            attributes: ["serviceName"],
                        }],
                        raw: true
                    });

                }
            }
            dbmessages.sequelize.close();
            dbmessageRecipients.sequelize.close();
            dbclient.sequelize.close();
            return messagesList;
        } catch (error) {
            throw error;
        }
    };

    singleMessage = async (information, messageId, filterPrivateFields = true) => {
        try {
            let dbMessage = createInstance(["messages"], information);
            let dbmessageTypes = createInstance(["messageTypes"], information);

            dbMessage.messages.belongsTo(dbmessageTypes.messageTypes, {
                foreignKey: "messageTypeId",
                targetKey: "messageTypeId"
            });

            let message = await dbMessage.messages.findAll({
                include: [{
                    model: dbmessageTypes.messageTypes,
                    attributes: ["messageTypeName"],
                }],
                where: {
                    messageId: messageId,
                    status: 1
                },
                raw: true
            });

            //======Find Clients==================

            let dbmessageRecipients = createInstance(["messageRecipients"], information);
            let dbclient = createInstance(["client"], information);

            dbmessageRecipients.messageRecipients.belongsTo(dbclient.clients, {
                foreignKey: "recipientId",
                targetKey: "clientId"
            });

            let clientData = [];

            let recipients = await dbmessageRecipients.messageRecipients.findAll({
                where: { messageId: messageId },
                include: [{
                    model: dbclient.clients,
                    attributes: {
                        include: [
                            [dbclient.sequelize.literal('CASE WHEN photoUrl is NULL THEN CONCAT("' + KNACK_UPLOAD_URL.localUrl + '" , "noimage.jpg" ) ELSE CONCAT("' + KNACK_UPLOAD_URL.localUrl + '" , photoUrl ) END'), 'photoUrl']
                        ],
                        exclude: ['createDateTime', 'updateDateTime', 'businessId', 'centerId']
                    }
                }],
                raw: true
            });

            //========Find Services=================

            let dbmessageService = createInstance(["messageServices"], information);
            let dbclientService = createInstance(["clientService"], information);

            dbmessageService.messageServices.belongsTo(dbclientService.services, {
                foreignKey: "serviceId",
                targetKey: "serviceId"
            });

            let services = await dbmessageService.messageServices.findAll({
                include: [{
                    model: dbclientService.services,
                    attributes: ["serviceId", 'serviceName'],
                }],
                where: {
                    messageId: messageId
                },
                raw: true
            });

            dbMessage.sequelize.close();
            dbmessageTypes.sequelize.close();
            dbmessageRecipients.sequelize.close();
            dbclient.sequelize.close();
            dbmessageService.sequelize.close();
            dbclientService.sequelize.close();

            return {
                messageDetails: message,
                recipients: recipients,
                services: services
            };

        } catch (error) {
            throw error;
        }
    };

    getClientsByServiceIds = async (information, serviceIds, page, searchQuery) => {

        try {

            var pageNo = (page) ? page : 1;
            var limitPerPage = 20;
            var offsetLimit = (pageNo - 1) * limitPerPage;
            let whereCond = '';
            if (searchQuery.key) {
                whereCond = "AND clients.clientName like '" + searchQuery.key + "%'";
            }

            let clientDb = createInstance(['client'], information);
            let finalResultArr = [];
            let serviceArr = [];

            if (serviceIds.indexOf(',') > -1) {
                serviceArr = serviceIds.split(",");
            } else {
                serviceArr = [serviceIds];
            }

            let AllClients = await clientDb.sequelize.query("SELECT clients.clientId,clients.clientName,clients.photoUrl, subscriptions.subscriptionId,subscriptions.pricepacks,subscriptions.subscriptionDateTime,pricepacks.pricepackName,pricepacks.pricepackValidity,servicepricepackmap.serviceId,services.serviceName FROM clients LEFT OUTER JOIN subscriptions ON clients.clientId=subscriptions.clientId LEFT OUTER JOIN `pricepacks` ON pricepacks.pricepackId = subscriptions.pricepacks LEFT OUTER JOIN `servicepricepackmap` ON servicepricepackmap.pricepackId = pricepacks.pricepackId LEFT OUTER JOIN services ON services.serviceId = servicepricepackmap.serviceId WHERE clients.status='1' " + whereCond + " ORDER BY clients.clientName LIMIT " + offsetLimit + " , " + limitPerPage, {
                type: clientDb.sequelize.QueryTypes.SELECT
            });

            AllClients.forEach(function (client, index, arr) {

                var d = new Date();
                var newdate = new Date(client.subscriptionDateTime);
                newdate.setDate(newdate.getDate() + client.pricepackValidity);

                var subscriptionStatus = (d <= newdate) ? 'active' : 'inactive';

                var dd = newdate.getDate();
                var mm = newdate.getMonth() + 1;
                var y = newdate.getFullYear();

                dd = (dd.toString().length === 1) ? '0' + dd : dd;
                mm = (mm.toString().length === 1) ? '0' + mm : mm;

                var subscriptionExpireDate = (client.subscriptionDateTime) ? y + '-' + mm + '-' + dd : null;

                var isphoto = (client.photoUrl === null) ? 'noimage.jpg' : client.photoUrl;

                if (client.serviceId) {
                    if (inArray(client.serviceId, serviceArr)) {
                        finalResultArr.push({
                            clientId: client.clientId,
                            clientName: client.clientName,
                            photoUrl: KNACK_UPLOAD_URL.localUrl + isphoto,
                            subscriptionId: client.subscriptionId,
                            pricepackName: client.pricepackName,
                            pricepackId: client.pricepacks,
                            subscriptionDate: client.subscriptionDateTime,
                            subscriptionExpireDate: subscriptionExpireDate,
                            subscriptinStatus: subscriptionStatus,
                            serviceId: client.serviceId,
                            isLead: false
                        });
                    }
                }

            })

            //return finalResultArr;
            var uniqueNames = [];
            let dataArray = [];
            for (let i = 0; i < finalResultArr.length; i++) {
                if (!inArray(finalResultArr[i].clientId, uniqueNames)) {
                    uniqueNames.push(finalResultArr[i].clientId);
                    dataArray.push(finalResultArr[i]);
                }
            }

            return dataArray;

        } catch (error) {
            throw error;
        }

    };
    //unscheduled client
    subscribedClientList = async (information, searchkey, page) => {
        try {

            var pageNo = (page) ? page : 1;
            var limitPerPage = PEGINATION_LIMIT;
            var offsetLimit = (pageNo - 1) * limitPerPage;
            let key = (searchkey.key) ? "WHERE clientName LIKE '" + searchkey.key + "%'" : "";

            let db = createInstance(['clientSubscription'], information);
            let clientArr = [];

            let clients = await db.sequelize.query("SELECT clients.clientId,clientName,contactNumber,photoUrl,servicepricepackmap.pricepackId,pricepackName,pricepackValidity,subscriptions.subscriptionId,subscriptionDateTime,clientschedulemap.scheduleId,servicepricepackmap.serviceId,services.serviceName FROM `clients` LEFT OUTER JOIN `subscriptions` ON subscriptions.clientId = clients.clientId LEFT OUTER JOIN `clientschedulemap` ON clientschedulemap.clientId = clients.clientId LEFT OUTER JOIN `schedule` ON schedule.scheduleId = clientschedulemap.scheduleId LEFT OUTER JOIN `pricepacks` ON pricepacks.pricepackId = subscriptions.pricepacks LEFT OUTER JOIN `servicepricepackmap` ON servicepricepackmap.pricepackId = pricepacks.pricepackId LEFT OUTER JOIN services ON servicepricepackmap.serviceId = services.serviceId " + key + " order by clients.clientName asc LIMIT " + offsetLimit + "," + limitPerPage, {
                type: db.sequelize.QueryTypes.SELECT
            });
            let finalResultArr = [];

            clients.forEach(function (client, index, arr) {

                var d = new Date();
                var newdate = new Date(client.subscriptionDateTime);
                newdate.setDate(newdate.getDate() + client.pricepackValidity);

                var subscriptionStatus = (d <= newdate) ? 'active' : 'inactive';

                var dd = newdate.getDate();
                var mm = newdate.getMonth() + 1;
                var y = newdate.getFullYear();

                dd = (dd.toString().length === 1) ? '0' + dd : dd;
                mm = (mm.toString().length === 1) ? '0' + mm : mm;

                var subscriptionExpireDate = y + '-' + mm + '-' + dd;

                var isphoto = (client.photoUrl === null) ? 'noimage.jpg' : client.photoUrl;


                if (client.subscriptionId && !client.scheduleId) {
                    finalResultArr.push({
                        initial: client.clientName.charAt(0),
                        clientId: client.clientId,
                        clientName: client.clientName,
                        photoUrl: KNACK_UPLOAD_URL.localUrl + isphoto,
                        serviceId: client.serviceId,
                        serviceName: client.serviceName,
                        subscriptionId: client.subscriptionId,
                        pricepackName: client.pricepackName,
                        pricepackId: client.pricepackId,
                        subscriptionDate: client.subscriptionDateTime,
                        subscriptionExpireDate: subscriptionExpireDate,
                        subscriptinStatus: subscriptionStatus
                    });
                }
            })

            // var uniqueNames = [];
            // let dataArray = [];
            // for (let i = 0; i < finalResultArr.length; i++) {
            // if (!inArray(finalResultArr[i].clientId, uniqueNames)) {
            // uniqueNames.push(finalResultArr[i].clientId);
            // dataArray.push(finalResultArr[i]);
            // }
            // }

            let setfinval = { rows: finalResultArr }
            return setfinval;

        } catch (error) {
            throw error;
        }
    };

    //unscheduled client
    subscribedClientList__ = async (information, searchkey, page) => {
        try {
            var pageNo = (page) ? page : 1;
            var limitPerPage = PEGINATION_LIMIT;
            var offsetLimit = (pageNo - 1) * limitPerPage;

            let businessIdinformation = information.split(",")[1];
            let cond = {
                status: 1,
                businessId: businessIdinformation,
            };
            if (searchkey.key) {
                cond.clientName = {
                    like: '%' + searchkey.key + '%'
                }
            }
            let dbClients = createInstance(["client"], information);
            let dbsubscriptions = createInstance(["clientSubscription"], information);
            let dbpricepack = createInstance(["clientPricepack"], information);
            //let dbservicepricepackmap = createInstance(["servicePricepackMap"], information);
            //let dbservices = createInstance(["clientService"], information);

            dbClients.clients.belongsTo(dbsubscriptions.subscriptions, {
                foreignKey: "clientId",
                targetKey: "clientId"
            });

            dbsubscriptions.subscriptions.belongsTo(dbpricepack.pricepacks, {
                foreignKey: "pricepacks",
                targetKey: "pricepackId"
            });


            let clientsListTotalList = await dbClients.clients.findAndCount({
                attributes: {
                    include: [
                        [dbClients.clients.sequelize.literal('CASE WHEN photoUrl is NULL THEN CONCAT("' + KNACK_UPLOAD_URL.localUrl + '" , "noimage.jpg" ) ELSE CONCAT("' + KNACK_UPLOAD_URL.localUrl + '" , photoUrl ) END'), 'photoFullUrl'],
                        [dbClients.sequelize.fn('SUBSTRING', dbClients.sequelize.fn('SOUNDEX', dbClients.sequelize.col('clientName')), 1, 1), 'initial']
                    ],
                    exclude: []
                },
                where: cond,
                order: [
                    ['clientName', 'ASC']
                ],
                offset: offsetLimit,
                limit: limitPerPage,
                raw: true
            });


            if (clientsListTotalList.rows.length > 0) {
                for (var i = 0; i < clientsListTotalList.rows.length; i++) {
                    clientsListTotalList.rows[i]["services"] = await dbsubscriptions.sequelize.query("SELECT services.serviceId,serviceName,`servicepricepackmap`.pricepackId,pricepackName,pricepackValidity,pricepacks.serviceDuration,clientschedulemap.scheduleId,sessionCount,scheduleName,scheduleDate,startTime,endTime,subscriptions.subscriptionId,subscriptions.subscriptionDateTime,(SELECT DATE_ADD(subscriptionDateTime, INTERVAL pricepackValidity DAY)) as validity FROM `services` LEFT OUTER JOIN `servicepricepackmap` ON services.serviceId = servicepricepackmap.serviceId LEFT OUTER JOIN `subscriptions` ON `servicepricepackmap`.pricepackId = `subscriptions`.pricepacks LEFT OUTER JOIN `pricepacks` ON servicepricepackmap.pricepackId = pricepacks.pricepackId LEFT OUTER JOIN `clientschedulemap` ON clientschedulemap.clientId = `subscriptions`.clientId LEFT OUTER JOIN `schedule` ON clientschedulemap.scheduleId = schedule.scheduleId WHERE `subscriptions`.clientId='" + clientsListTotalList.rows[i]["clientId"] + "'", {
                        type: dbsubscriptions.sequelize.QueryTypes.SELECT
                    })
                    clientsListTotalList.rows[i]["pricepacks"] = await dbsubscriptions.subscriptions.findAll({
                        attributes: ['subscriptionId', 'subscriptionDateTime'],
                        where: { clientId: clientsListTotalList.rows[i]["clientId"] },
                        include: [{
                            model: dbpricepack.pricepacks,
                            attributes: ['pricepackId', 'pricepackName', 'pricepackValidity', 'serviceDuration'],
                        }],
                        raw: true
                    });
                    clientsListTotalList.rows[i]["schedule"] = await dbsubscriptions.sequelize.query("SELECT clientschedulemap.scheduleId,scheduleName,scheduleDate,startTime,endTime FROM `schedule` LEFT OUTER JOIN `clientschedulemap` ON schedule.scheduleId = clientschedulemap.scheduleId WHERE `clientschedulemap`.clientId='" + clientsListTotalList.rows[i]["clientId"] + "'", {
                        type: dbsubscriptions.sequelize.QueryTypes.SELECT
                    });
                }
            }

            for (var x = 0; x < clientsListTotalList.rows.length; x++) {
                if (clientsListTotalList.rows[x]["services"].length > 0) {
                    for (var y = 0; y < clientsListTotalList.rows[x]["services"].length; y++) {

                        clientsListTotalList.rows[x]["services"][y]["paidAmount"] = await dbsubscriptions.sequelize.query("SELECT SUM(payble_amount) as paidAmount FROM `clientpayments` WHERE `subscriptionsId` = '" + clientsListTotalList.rows[x]["services"][y]["subscriptionId"] + "'GROUP BY `subscriptionsId`", {
                            type: dbsubscriptions.sequelize.QueryTypes.SELECT
                        });

                        clientsListTotalList.rows[x]["services"][y]["dueAmount"] = await dbsubscriptions.sequelize.query("SELECT distinct(due_amount) FROM `clientpayments` WHERE `subscriptionsId` = '" + clientsListTotalList.rows[x]["services"][y]["subscriptionId"] + "'", {
                            type: dbsubscriptions.sequelize.QueryTypes.SELECT
                        });
                    }
                }
            }

            dbsubscriptions.sequelize.close();
            dbClients.sequelize.close();
            dbpricepack.sequelize.close();
            //dbservicepricepackmap.close();


            return clientsListTotalList;


        } catch (error) {
            throw error;
        }
    };

    adminClientList = async (information, searchkey, page) => {
        try {
            var pageNo = (page) ? page : 1;
            var limitPerPage = PEGINATION_LIMIT;
            var offsetLimit = (pageNo - 1) * limitPerPage;

            let businessIdinformation = information.split(",")[1];
            let cond = {
                status: 1,
                businessId: businessIdinformation,
            };
            if (searchkey.key) {
                cond.clientName = {
                    like: searchkey.key + '%'
                }
            }
            let dbClients = createInstance(["client"], information);
            let dbsubscriptions = createInstance(["clientSubscription"], information);
            let dbpricepack = createInstance(["clientPricepack"], information);
            //let dbservicepricepackmap = createInstance(["servicePricepackMap"], information);
            //let dbservices = createInstance(["clientService"], information);

            dbClients.clients.belongsTo(dbsubscriptions.subscriptions, {
                foreignKey: "clientId",
                targetKey: "clientId"
            });

            dbsubscriptions.subscriptions.belongsTo(dbpricepack.pricepacks, {
                foreignKey: "pricepacks",
                targetKey: "pricepackId"
            });


            let clientsListTotalList = await dbClients.clients.findAndCount({
                attributes: {
                    include: [
                        [dbClients.clients.sequelize.literal('CASE WHEN photoUrl  is NULL THEN CONCAT("' + KNACK_UPLOAD_URL.localUrl + '" , "noimage.jpg" ) ELSE CONCAT("' + KNACK_UPLOAD_URL.localUrl + '" , photoUrl ) END'), 'photoFullUrl'],
                        [dbClients.sequelize.fn('SUBSTRING', dbClients.sequelize.fn('SOUNDEX', dbClients.sequelize.col('clientName')), 1, 1), 'initial']
                    ],
                    exclude: []
                },
                where: cond,
                order: [
                    ['clientName', 'ASC']
                ],
                offset: offsetLimit,
                limit: limitPerPage,
                raw: true
            });


            if (clientsListTotalList.rows.length > 0) {
                for (var i = 0; i < clientsListTotalList.rows.length; i++) {
                    clientsListTotalList.rows[i]["services"] = await dbsubscriptions.sequelize.query("SELECT services.serviceId,serviceName,`servicepricepackmap`.pricepackId,pricepackName,pricepackValidity,pricepacks.serviceDuration,clientschedulemap.scheduleId,sessionCount,scheduleName,scheduleDate,startTime,endTime,subscriptions.subscriptionId,subscriptions.subscriptionDateTime,(SELECT DATE_ADD(subscriptionDateTime, INTERVAL pricepackValidity DAY)) as validity FROM `services` LEFT OUTER JOIN `servicepricepackmap` ON services.serviceId = servicepricepackmap.serviceId LEFT OUTER JOIN `subscriptions` ON `servicepricepackmap`.pricepackId = `subscriptions`.pricepacks LEFT OUTER JOIN `pricepacks` ON servicepricepackmap.pricepackId = pricepacks.pricepackId LEFT OUTER JOIN `clientschedulemap` ON clientschedulemap.clientId = `subscriptions`.clientId LEFT OUTER JOIN `schedule` ON clientschedulemap.scheduleId = schedule.scheduleId WHERE `subscriptions`.clientId='" + clientsListTotalList.rows[i]["clientId"] + "'", {
                        type: dbsubscriptions.sequelize.QueryTypes.SELECT
                    })
                    clientsListTotalList.rows[i]["pricepacks"] = await dbsubscriptions.subscriptions.findAll({
                        attributes: ['subscriptionId', 'subscriptionDateTime'],
                        where: { clientId: clientsListTotalList.rows[i]["clientId"] },
                        include: [{
                            model: dbpricepack.pricepacks,
                            attributes: ['pricepackId', 'pricepackName', 'pricepackValidity', 'serviceDuration'],
                        }],
                        raw: true
                    });
                    clientsListTotalList.rows[i]["schedule"] = await dbsubscriptions.sequelize.query("SELECT clientschedulemap.scheduleId,scheduleName,scheduleDate,startTime,endTime FROM `schedule` LEFT OUTER JOIN `clientschedulemap` ON schedule.scheduleId = clientschedulemap.scheduleId  WHERE `clientschedulemap`.clientId='" + clientsListTotalList.rows[i]["clientId"] + "'", {
                        type: dbsubscriptions.sequelize.QueryTypes.SELECT
                    });
                }
            }

            for (var x = 0; x < clientsListTotalList.rows.length; x++) {
                if (clientsListTotalList.rows[x]["services"].length > 0) {
                    for (var y = 0; y < clientsListTotalList.rows[x]["services"].length; y++) {

                        clientsListTotalList.rows[x]["services"][y]["paidAmount"] = await dbsubscriptions.sequelize.query("SELECT SUM(payble_amount) as paidAmount FROM `clientpayments` WHERE `subscriptionsId` = '" + clientsListTotalList.rows[x]["services"][y]["subscriptionId"] + "'GROUP BY `subscriptionsId`", {
                            type: dbsubscriptions.sequelize.QueryTypes.SELECT
                        });

                        clientsListTotalList.rows[x]["services"][y]["dueAmount"] = await dbsubscriptions.sequelize.query("SELECT distinct(due_amount) FROM `clientpayments` WHERE `subscriptionsId` = '" + clientsListTotalList.rows[x]["services"][y]["subscriptionId"] + "'", {
                            type: dbsubscriptions.sequelize.QueryTypes.SELECT
                        });
                    }
                }
            }

            dbsubscriptions.sequelize.close();
            dbClients.sequelize.close();
            dbpricepack.sequelize.close();
            //dbservicepricepackmap.close();


            return clientsListTotalList;


        } catch (error) {
            throw error;
        }
    };

    deleteMessage = async (information, messageId) => {
        try {
            let db = createInstance(["messages"], information);
            let note = await db.messages.update(
                {
                    status: 0,
                    updateDateTime: Sequelize.fn('NOW')
                },
                {
                    where: {
                        messageId: messageId
                    }
                }
            );
            db.sequelize.close();
            return note;

        } catch (error) {
            throw error;
        }
    };

    messageTypes = async (information) => {

        let messageTypesdb = createInstance(["messageTypes"], information);

        let messageTypes = await messageTypesdb.messageTypes.findAll({
            raw: true
        });

        //let messageTypes = ['Birthday','Holiday','Workshop','Special offer','Registration'];
        return messageTypes;

    }

    update = async (information, messageId, senderId, serviceId, recipients, messageTypeId, messageBody, message_date_time) => {
        try {
            let dbmessages = createInstance(["messages"], information);
            let dbmessageRecipients = createInstance(["messageRecipients"], information);
            let dbmessageService = createInstance(["messageServices"], information);

            let inform = information.split(',');
            let businessId = inform[1];

            let message = await dbmessages.messages.update({
                messageTypeId: messageTypeId,
                messageBody: messageBody,
                message_date_time: message_date_time
            }, {
                    where: {
                        messageId: messageId
                    }
                });

            await dbmessageService.messageServices.destroy({
                where: {
                    messageId: messageId
                }
            })

            if (serviceId.indexOf(',') > -1) {
                var noOfServices = serviceId.split(",");
            } else {
                var noOfServices = [serviceId];
            }

            for (let i = 0; i < noOfServices.length; i++) {

                //--------------insert service to messageServices table---------------

                let serviceId = noOfServices[i];
                let recipientData = await dbmessageService.messageServices.create({
                    messageId,
                    businessId,
                    serviceId
                });

            }

            await dbmessageRecipients.messageRecipients.destroy({
                where: {
                    messageId: messageId
                }
            })

            //===========IF RECIPIENT IS BLANK==================
            if (!recipients) {
                //-----------------fetch all clients who are subscribed to this service-----------------------------
                let servicepricepackmapdb = createInstance(['servicePricepackMap'], information);
                //Step 1 [Get all pricepacks of that serive from servicepricepackmap table]

                let pricepackArr = [];

                for (let i = 0; i < noOfServices.length; i++) {


                    let pricepacks = await servicepricepackmapdb.servicepricepackmap.findAll({
                        where: {
                            serviceId: noOfServices[i],
                            status: 1
                        },
                        raw: true
                    });



                    for (let i = 0; i < pricepacks.length; i++) {
                        pricepackArr.push(pricepacks[i].pricepackId);
                    }

                }


                //Step 2 [Get all clients from subscriptions table by pricepackId]

                let clients = await subscriptionskmapdb.subscriptions.findAll({
                    where: {
                        pricepacks: {
                            [Op.in]: pricepackArr
                        },
                        status: 1
                    },
                    raw: true
                });

                //console.log("Clients : ",clients);
                for (let i = 0; i < clients.length; i++) {
                    // insert recipient
                    let recipientId = clients[i].clientId;
                    let recipientData = await dbmessageRecipients.messageRecipients.create({
                        messageId,
                        businessId,
                        senderId,
                        recipientId
                    });
                }

            } else {
                //===========IF RECIPIENT SELECTED==================

                if (recipients.indexOf(',') > -1) {
                    var noOfRecipients = recipients.split(",");
                } else {
                    var noOfRecipients = [recipients];
                }

                console.log("noOfRecipients : ", noOfRecipients);

                // insert recipient
                for (let i = 0; i < noOfRecipients.length; i++) {

                    let recipientId = noOfRecipients[i];
                    let recipientData = await dbmessageRecipients.messageRecipients.create({
                        messageId,
                        businessId,
                        senderId,
                        recipientId
                    });

                }
            }

            dbmessageService.sequelize.close();
            return await message;
        } catch (error) {
            throw error;
        }
    };

    sendMessage = async (information) => {
        try {
            var businessIdinformation = information.split(",")[1];
            let dbClients = createInstance(["client"], information);
            let dbMessage = createInstance(["messages"], information);
            let dbmessageRecipients = createInstance(["messageRecipients"], information);

            dbMessage.messages.hasMany(dbmessageRecipients.messageRecipients, {
                foreignKey: "messageId",
                targetKey: "messageId"
            });

            dbmessageRecipients.messageRecipients.belongsTo(dbClients.clients, {
                foreignKey: "recipientId",
                targetKey: "clientId"
            });

            let messageList = await dbMessage.messages.findAndCount({
                where: {
                    businessId: businessIdinformation
                },
                include: [{
                    model: dbmessageRecipients.messageRecipients,
                    attributes: ['recipientId'],
                    include: [{
                        model: dbClients.clients,
                        attributes: ["contactNumber", "clientName"]
                    }]
                }],
                raw: true
            })

            for (let i = 0; i < messageList.length; i++) {

                if (messageList.rows[i]['messageRecipients.client.contactNumber'] && messageList.rows[i].messageBody) {
                    let toDay = new Date();
                    let messageScheduleDate = new Date(messageList.rows[i].message_date_time);

                    let newToday = toDay.getDate() + '-' + toDay.getMonth() + '-' + toDay.getFullYear();
                    let newMessageScheduleDate = messageScheduleDate.getDate() + '-' + messageScheduleDate.getMonth() + '-' + messageScheduleDate.getFullYear();

                    if (newToday === newMessageScheduleDate) {
                        textNotification(messageList.rows[i].messageBody, messageList.rows[i]['messageRecipients.client.contactNumber']);
                    }
                }

            }
            dbMessage.sequelize.close();
            dbClients.sequelize.close();
            dbmessageRecipients.sequelize.close();

            return messageList;

        } catch (error) {
            throw error;
        }
    }

}


export default new MessagesDB();