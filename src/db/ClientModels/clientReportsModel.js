import {
    filterFields,
    createInstance,
    getScheduleInfo,
    getClientScheduleMapInfo,
    getClientInfo,
    makeEmailTemplate,
    sendEmail,
    sendSmsMobile,
    rsPaginate,
    sessionInfo
} from "../../lib/common";
import {
    ApplicationError
} from "../../lib/errors";
import BaseModel from "../BaseModel";
var fs = require("fs");
var pdf = require("pdfkit");
var pdf1 = require('html-pdf');
import BusinessSchema from "../../schemas/business.schema.js";
import nodemailer from "nodemailer";
import {
    KNACKDB,
    KNACK_UPLOAD_URL,
    PEGINATION_LIMIT,
    UPLOAD_DIR
} from "../../lib/constants.js";
import AttendanceDB from "../ClientModels/clientAttendanceModel.js";
import SubscriptionDB from "../ClientModels/ClientSubscriptionModel.js";

export class ReportsDB extends BaseModel {

    constructor(connection) {
        //Get Business Model Data
        super("businessinfo", connection);
        this.businessSchema = BusinessSchema.Business();
        this.businessName = "businessinfo";
        this.businessModel = this.connection.model(this.businessName, this.businessSchema);

        let payble_amount;
        let serviceId;
        let pricepackId;
    }

    showClientPricePackList = async (information,
        clientId,
        filterPrivateFields = true) => {
        try {
            let dbclientSubscription = createInstance(["clientSubscription"], information);
            let dbpricepacks = createInstance(["clientPricepack"], information);
            let dbservicePricepackMap = createInstance(["servicePricepackMap"], information);
            let dbclientService = createInstance(["clientService"], information);
            let dbclientScheduleMap = createInstance(["clientScheduleMap"], information);
            let dbclientSchedule = createInstance(["clientSchedule"], information);
            dbclientSubscription.subscriptions.belongsTo(dbpricepacks.pricepacks, {
                foreignKey: "pricepacks",
                targetKey: "pricepackId",

            });
            dbservicePricepackMap.servicepricepackmap.belongsTo(dbclientService.services, {
                foreignKey: "serviceId",
                targetKey: "serviceId",
            });
            let subscriptionsData = await dbclientSubscription.subscriptions.findAll({
                where: {
                    clientId: clientId,
                    status: 1
                },
                include: [{
                    model: dbpricepacks.pricepacks,
                    attributes: ["pricepackId", "pricepackName"]
                }],
                raw: true
            });
            dbclientSubscription.sequelize.close();
            if (subscriptionsData) {
                if (subscriptionsData.length >= 1) {
                    for (var i = 0; i < subscriptionsData.length; i++) {
                        let serviceNameList = await dbservicePricepackMap.servicepricepackmap.findAll({
                            where: {
                                pricepackId: subscriptionsData[i].pricepacks,
                                status: 1
                            },
                            include: [{
                                model: dbclientService.services,
                                attributes: ["serviceName"]
                            }],
                            raw: true
                        });
                        subscriptionsData[i].serviceNameList = serviceNameList;
                        for (var n = 0; n < subscriptionsData.length; n++) {
                            var scheduleIdList = await dbclientScheduleMap.clientschedulemap.findAll({
                                where: {
                                    subscriptionId: subscriptionsData[i].subscriptionId,
                                    status: 1
                                },
                                raw: true
                            });
                        }
                        for (var m = 0; m < scheduleIdList.length; m++) {
                            var scheduleIdTimeList = await dbclientSchedule.schedule.findAll({
                                where: {
                                    scheduleId: scheduleIdList[m].scheduleId,
                                    status: 1
                                },
                                attributes: ["startTime", "endTime"],
                                raw: true
                            });
                            subscriptionsData[i].scheduleIdTimeList = scheduleIdTimeList;
                        }
                    }
                    dbclientSchedule.sequelize.close();
                    dbservicePricepackMap.sequelize.close();
                }
            }
            return subscriptionsData;

        } catch (error) {
            throw error;
        }
    };




    showClientAttendance = async (information,
        scheduleId,
        clientId,
        reportType,
        filterPrivateFields = true) => {
        try {
            var report = [];
            var header = [];
            var businessIdinformation = information.split(",")[1];
            let businessinfoData = await this.businessModel.findAll({
                where: {
                    businessId: businessIdinformation,
                    status: 1
                },
                raw: true
            });
            let dbClients = createInstance(["client"], information);
            let clients = await dbClients.clients.findAll({
                where: {
                    clientId: clientId,
                    status: 1
                },
                raw: true
            });
            dbClients.sequelize.close();
            //end here

            //pricepack details (can be multiple)
            let dbCSM = createInstance(["clientScheduleMap"], information);
            let CSM = await dbCSM.clientschedulemap.findAll({
                where: {
                    businessId: businessIdinformation,
                    scheduleId: scheduleId,
                    clientId: clientId
                },
                raw: true
            });
            dbCSM.sequelize.close();
            let dbSub = createInstance(["clientSubscription"], information);
            let subscriptions = await dbSub.subscriptions.findAll({
                where: {
                    status: 1,
                    subscriptionId: CSM[0].subscriptionId
                },
                raw: true
            });
            let dbPP = createInstance(["clientPricepack"], information);
            let pricepacks = await dbPP.pricepacks.findAll({
                where: {
                    status: 1,
                    pricepackId: subscriptions[0].pricepacks
                },
                raw: true
            });
            //later calculate sessions that are left
            //end of pricepack details

            let dbservicePricepackMap = createInstance(["servicePricepackMap"], information);
            let dbclientService = createInstance(["clientService"], information);
            dbservicePricepackMap.servicepricepackmap.belongsTo(dbclientService.services, {
                foreignKey: "serviceId",
                targetKey: "serviceId",
            });
            let serviceNameList = await dbservicePricepackMap.servicepricepackmap.findAll({
                where: {
                    pricepackId: subscriptions[0].pricepacks,
                    status: 1
                },
                include: [{
                    model: dbclientService.services,
                    attributes: ["serviceName"]
                }],
                raw: true
            });

            //calculating attendance report with service details
            let dbServices = createInstance(["clientService"], information);
            let services = await dbServices.services.findAll({
                where: {
                    status: 1,
                    serviceId: subscriptions[0].services
                },
                raw: true
            });

            var attendanceKeys = [];
            var attendanceValues = [];
            Object.keys(CSM[0].attendanceType).forEach(function (key) {
                attendanceKeys.push(key);
                attendanceValues.push(CSM[0].attendanceType[key]);
            });

            if (reportType == 1) {
                var d = new Date();
                var month = d.getMonth() + 1;
                for (var i = 0; i < attendanceKeys.length; i++) {
                    var idate = attendanceKeys[i].split("-");
                    var myMonth = parseInt(idate[1]);
                    if (month == myMonth) {
                        var newObj = {
                            Date: attendanceKeys[i],
                            Service: services[0].serviceName,
                            Attendance: attendanceValues[i]
                        };
                        report = report.concat(newObj);
                    }
                }
            } else if (reportType == 2) {
                for (var i = 0; i < attendanceKeys.length; i++) {
                    var idate = attendanceKeys[i].split("-");
                    var myMonth = parseInt(idate[1]);
                    var newObj = {
                        Date: attendanceKeys[i],
                        Service: services[0].serviceName,
                        Attendance: attendanceValues[i]
                    };
                    report = report.concat(newObj);
                }
            } else {
                throw new ApplicationError(
                    "Error in input. 1 for this month. 2 for full schedule."
                );
            }
            //end of attendance

            var arr = [];
            Array.prototype.push.apply(arr, [{
                header: businessinfoData
            }]);
            Array.prototype.push.apply(arr, [{
                client: clients
            }]);
            Array.prototype.push.apply(arr, [{
                pricepack: pricepacks
            }]);
            Array.prototype.push.apply(arr, [{
                attendanceReport: report
            }]);
            Array.prototype.push.apply(arr, [{
                serviceNameList: serviceNameList
            }]);
            var finalReport = [];
            finalReport = Object.assign.apply(Object, arr);

            return await finalReport;
        } catch (error) {
            throw error;
        }
    };

    showClientPayment = async (information,
        scheduleId,
        clientId,
        reportType,
        filterPrivateFields = true) => {
        try {
            // console.log('reportType'+reportType);
            var report = [];
            //getting the header (batchName/scheduleName/businessName : confused !! :P)
            var header = [];
            //end of header

            var businessIdinformation = information.split(",")[1];
            let businessinfoData = await this.businessModel.findAll({
                where: {
                    businessId: businessIdinformation,
                    status: 1
                },
                raw: true
            });

            //getting the client's personal information
            let dbClients = createInstance(["client"], information);
            let clients = await dbClients.clients.findAll({
                where: {
                    clientId: clientId,
                    status: 1
                },
                raw: true
            });
            //end here

            //pricepack details (can be multiple)
            let dbCSM = createInstance(["clientScheduleMap"], information);
            let CSM = await dbCSM.clientschedulemap.findAll({
                where: {
                    businessId: businessIdinformation,
                    scheduleId: scheduleId,
                    clientId: clientId
                },
                raw: true
            });
            let dbSub = createInstance(["clientSubscription"], information);
            let subscriptions = await dbSub.subscriptions.findAll({
                where: {
                    status: 1,
                    subscriptionId: CSM[0].subscriptionId
                },
                raw: true
            });
            let dbPP = createInstance(["clientPricepack"], information);
            var pricepacks = await dbPP.pricepacks.findAll({
                where: {
                    status: 1,
                    pricepackId: subscriptions[0].pricepacks
                },
                raw: true
            });
            //later calculate sessions that are left
            //end of pricepack details

            let dbservicePricepackMap = createInstance(["servicePricepackMap"], information);
            let dbclientService = createInstance(["clientService"], information);
            dbservicePricepackMap.servicepricepackmap.belongsTo(dbclientService.services, {
                foreignKey: "serviceId",
                targetKey: "serviceId",
            });
            let serviceNameList = await dbservicePricepackMap.servicepricepackmap.findAll({
                where: {
                    pricepackId: subscriptions[0].pricepacks,
                    status: 1
                },
                include: [{
                    model: dbclientService.services,
                    attributes: ["serviceName"]
                }],
                raw: true
            });

            //calculating payments report with service details
            let clientTotalPaymentdb = createInstance(["clientPayments"], information);
            if (reportType == 1) {
                var today = new Date();
                var month = today.getMonth() + 1;
                var sequelize = require("sequelize");
                let clientTotalPayment = await clientTotalPaymentdb.clientpayments.findAll({
                    where: {
                        where: sequelize.where(sequelize.fn('MONTH', sequelize.col('payment_date')), month),
                        businessId: businessIdinformation,
                        subscriptionsId: subscriptions[0].subscriptionId,
                        status: 1
                    },
                    order: [
                        ['payment_due_date', 'ASC'],
                    ],
                    raw: true
                });

                var totalDue = 0;
                for (var i = 0; i < clientTotalPayment.length; i++) {

                    //status text
                    var paymentStatusText;
                    if (clientTotalPayment[i].paymentStatus == 1) paymentStatusText = 'pending';
                    else if (clientTotalPayment[i].paymentStatus == 2) paymentStatusText = 'paid';
                    else if (clientTotalPayment[i].paymentStatus == 3) paymentStatusText = 'untracked';
                    else paymentStatusText = 'undefined';


                    //calculating due amount depending on paid amount
                    var paidAmt = clientTotalPayment[i].paid_amount == null ?
                        0 : clientTotalPayment[i].paid_amount;

                    var currentDue = (clientTotalPayment[i].due_amount - paidAmt);

                    totalDue = (i != clientTotalPayment.length - 1) ?
                        totalDue + currentDue :
                        pricepacks[0].amount - totalDue;

                    var arr = [];
                    Array.prototype.push.apply(arr, [{
                        paymentDueDate: clientTotalPayment[i].payment_due_date
                    }]);
                    Array.prototype.push.apply(arr, [{
                        paymentDate: clientTotalPayment[i].payment_date
                    }]);
                    Array.prototype.push.apply(arr, [{
                        paymentDueSaid: clientTotalPayment[i].due_amount
                    }]);
                    Array.prototype.push.apply(arr, [{
                        paymentAmount: paidAmt
                    }]);
                    Array.prototype.push.apply(arr, [{
                        paymentDue: currentDue
                    }]);
                    // Array.prototype.push.apply(arr, [{ carryForwardAmount: totalDue }]);
                    Array.prototype.push.apply(arr, [{
                        status: paymentStatusText
                    }]);
                    var finalReport = [];
                    finalReport = Object.assign.apply(Object, arr);
                    report = report.concat(finalReport);
                }
            } else if (reportType == 2) {
                let clientTotalPayment = await clientTotalPaymentdb.clientpayments.findAll({
                    where: {
                        businessId: businessIdinformation,
                        subscriptionsId: subscriptions[0].subscriptionId,
                        status: 1
                    },
                    order: [
                        ['payment_due_date', 'ASC'],
                    ],
                    raw: true
                });

                var totalDue = 0;
                for (var i = 0; i < clientTotalPayment.length; i++) {

                    //status text
                    var paymentStatusText;
                    if (clientTotalPayment[i].paymentStatus == 1) paymentStatusText = 'pending';
                    else if (clientTotalPayment[i].paymentStatus == 2) paymentStatusText = 'paid';
                    else if (clientTotalPayment[i].paymentStatus == 3) paymentStatusText = 'untracked';
                    else paymentStatusText = 'undefined';


                    //calculating due amount depending on paid amount
                    var paidAmt = clientTotalPayment[i].paid_amount == null ?
                        0 : clientTotalPayment[i].paid_amount;

                    var currentDue = (clientTotalPayment[i].due_amount - paidAmt);

                    totalDue = (i != clientTotalPayment.length - 1) ?
                        totalDue + currentDue :
                        pricepacks[0].amount - totalDue;

                    var arr = [];
                    Array.prototype.push.apply(arr, [{
                        paymentDueDate: clientTotalPayment[i].payment_due_date
                    }]);
                    Array.prototype.push.apply(arr, [{
                        paymentDate: clientTotalPayment[i].payment_date
                    }]);
                    Array.prototype.push.apply(arr, [{
                        paymentDueSaid: clientTotalPayment[i].due_amount
                    }]);
                    Array.prototype.push.apply(arr, [{
                        paymentAmount: paidAmt
                    }]);
                    Array.prototype.push.apply(arr, [{
                        paymentDue: currentDue
                    }]);
                    Array.prototype.push.apply(arr, [{
                        carryForwardAmount: totalDue
                    }]);
                    Array.prototype.push.apply(arr, [{
                        status: paymentStatusText
                    }]);
                    var finalReport = [];
                    finalReport = Object.assign.apply(Object, arr);
                    report = report.concat(finalReport);
                }
            } else {
                throw new ApplicationError(
                    "Error in input. 1 for this month. 2 for full schedule."
                );
            }
            //end of payments report

            var arr = [];
            Array.prototype.push.apply(arr, [{
                header: businessinfoData
            }]);
            Array.prototype.push.apply(arr, [{
                client: clients
            }]);
            Array.prototype.push.apply(arr, [{
                pricepack: pricepacks
            }]);
            Array.prototype.push.apply(arr, [{
                paymentsReport: report
            }]);
            Array.prototype.push.apply(arr, [{
                serviceNameList: serviceNameList
            }]);
            var finalReport = [];
            finalReport = Object.assign.apply(Object, arr);

            return await finalReport;
        } catch (error) {
            throw error;
        }
    };


    showClientPaymentShare = async (information,
        scheduleId,
        clientId,
        reportType,
        filterPrivateFields = true) => {
        try {
            var report = [];
            var header = [];
            var businessIdinformation = information.split(",")[1];
            let businessinfoData = await this.businessModel.findAll({
                where: {
                    businessId: businessIdinformation,
                    status: 1
                },
                raw: true
            });

            let dbClients = createInstance(["client"], information);
            let clients = await dbClients.clients.findAll({
                where: {
                    clientId: clientId,
                    status: 1
                },
                raw: true
            });
            //end here

            //pricepack details (can be multiple)
            let dbCSM = createInstance(["clientScheduleMap"], information);
            let CSM = await dbCSM.clientschedulemap.findAll({
                where: {
                    businessId: businessIdinformation,
                    scheduleId: scheduleId,
                    clientId: clientId
                },
                raw: true
            });
            let dbSub = createInstance(["clientSubscription"], information);
            let subscriptions = await dbSub.subscriptions.findAll({
                where: {
                    status: 1,
                    subscriptionId: CSM[0].subscriptionId
                },
                raw: true
            });
            let dbPP = createInstance(["clientPricepack"], information);
            var pricepacks = await dbPP.pricepacks.findAll({
                where: {
                    status: 1,
                    pricepackId: subscriptions[0].pricepacks
                },
                raw: true
            });
            //later calculate sessions that are left
            //end of pricepack details

            let dbservicePricepackMap = createInstance(["servicePricepackMap"], information);
            let dbclientService = createInstance(["clientService"], information);
            dbservicePricepackMap.servicepricepackmap.belongsTo(dbclientService.services, {
                foreignKey: "serviceId",
                targetKey: "serviceId",
            });
            let serviceNameList = await dbservicePricepackMap.servicepricepackmap.findAll({
                where: {
                    pricepackId: subscriptions[0].pricepacks,
                    status: 1
                },
                include: [{
                    model: dbclientService.services,
                    attributes: ["serviceName"]
                }],
                raw: true
            });
            var total_name_array = [];
            var serviceNameListWithComma = "";
            if (serviceNameList) {
                var total_name_array = serviceNameList.map((x) => {
                    return x['service.serviceName']
                });
                serviceNameListWithComma = total_name_array.join();
            }
            //calculating payments report with service details
            let clientTotalPaymentdb = createInstance(["clientPayments"], information);
            if (reportType == 1) {
                var today = new Date();
                var month = today.getMonth() + 1;
                var sequelize = require("sequelize");
                let clientTotalPayment = await clientTotalPaymentdb.clientpayments.findAll({
                    where: {
                        where: sequelize.where(sequelize.fn('MONTH', sequelize.col('payment_date')), month),
                        businessId: businessIdinformation,
                        subscriptionsId: subscriptions[0].subscriptionId,
                        status: 1
                    },
                    order: [
                        ['payment_due_date', 'ASC'],
                    ],
                    raw: true
                });

                var totalDue = 0;
                for (var i = 0; i < clientTotalPayment.length; i++) {
                    //status text
                    var paymentStatusText;
                    if (clientTotalPayment[i].paymentStatus == 1) paymentStatusText = 'pending';
                    else if (clientTotalPayment[i].paymentStatus == 2) paymentStatusText = 'paid';
                    else if (clientTotalPayment[i].paymentStatus == 3) paymentStatusText = 'untracked';
                    else paymentStatusText = 'undefined';
                    //calculating due amount depending on paid amount
                    var paidAmt = clientTotalPayment[i].payble_amount == null ?
                        0 : clientTotalPayment[i].payble_amount;
                    var currentDue = (clientTotalPayment[i].due_amount - paidAmt);
                    totalDue = (i != clientTotalPayment.length - 1) ?
                        totalDue + currentDue :
                        pricepacks[0].amount - totalDue;
                    var arr = [];
                    Array.prototype.push.apply(arr, [{
                        paymentDueDate: clientTotalPayment[i].payment_due_date
                    }]);
                    Array.prototype.push.apply(arr, [{
                        paymentDate: clientTotalPayment[i].payment_date
                    }]);
                    Array.prototype.push.apply(arr, [{
                        paymentDueSaid: clientTotalPayment[i].due_amount
                    }]);
                    Array.prototype.push.apply(arr, [{
                        paymentAmount: paidAmt
                    }]);
                    Array.prototype.push.apply(arr, [{
                        paymentDue: currentDue
                    }]);
                    // Array.prototype.push.apply(arr, [{ carryForwardAmount: totalDue }]);
                    Array.prototype.push.apply(arr, [{
                        status: paymentStatusText
                    }]);
                    var finalReport = [];
                    finalReport = Object.assign.apply(Object, arr);
                    report = report.concat(finalReport);
                }
            } else if (reportType == 2) {
                let clientTotalPayment = await clientTotalPaymentdb.clientpayments.findAll({
                    where: {
                        businessId: businessIdinformation,
                        subscriptionsId: subscriptions[0].subscriptionId,
                        status: 1
                    },
                    order: [
                        ['payment_due_date', 'ASC'],
                    ],
                    raw: true
                });

                var totalDue = 0;
                for (var i = 0; i < clientTotalPayment.length; i++) {

                    //status text
                    var paymentStatusText;
                    if (clientTotalPayment[i].paymentStatus == 1) paymentStatusText = 'pending';
                    else if (clientTotalPayment[i].paymentStatus == 2) paymentStatusText = 'paid';
                    else if (clientTotalPayment[i].paymentStatus == 3) paymentStatusText = 'untracked';
                    else paymentStatusText = 'undefined';
                    //calculating due amount depending on paid amount
                    var paidAmt = clientTotalPayment[i].payble_amount == null ?
                        0 : clientTotalPayment[i].payble_amount;
                    var currentDue = (clientTotalPayment[i].due_amount - paidAmt);
                    totalDue = (i != clientTotalPayment.length - 1) ?
                        totalDue + currentDue :
                        pricepacks[0].amount - totalDue;
                    var arr = [];
                    Array.prototype.push.apply(arr, [{
                        paymentDueDate: clientTotalPayment[i].payment_due_date
                    }]);
                    Array.prototype.push.apply(arr, [{
                        paymentDate: clientTotalPayment[i].payment_date
                    }]);
                    Array.prototype.push.apply(arr, [{
                        paymentDueSaid: clientTotalPayment[i].due_amount
                    }]);
                    Array.prototype.push.apply(arr, [{
                        paymentAmount: paidAmt
                    }]);
                    Array.prototype.push.apply(arr, [{
                        paymentDue: currentDue
                    }]);
                    Array.prototype.push.apply(arr, [{
                        carryForwardAmount: totalDue
                    }]);
                    Array.prototype.push.apply(arr, [{
                        status: paymentStatusText
                    }]);
                    var finalReport = [];
                    finalReport = Object.assign.apply(Object, arr);
                    report = report.concat(finalReport);
                }
            } else {
                throw new ApplicationError(
                    "Error in input. 1 for this month. 2 for full schedule."
                );
            }
            //end of payments report

            var arr = [];
            Array.prototype.push.apply(arr, [{
                header: businessinfoData
            }]);
            Array.prototype.push.apply(arr, [{
                client: clients
            }]);
            Array.prototype.push.apply(arr, [{
                pricepack: pricepacks
            }]);
            Array.prototype.push.apply(arr, [{
                paymentsReport: report
            }]);
            Array.prototype.push.apply(arr, [{
                serviceNameList: serviceNameList
            }]);
            Array.prototype.push.apply(arr, [{
                serviceNameListWithComma: serviceNameListWithComma
            }]);
            var finalReport = [];
            finalReport = Object.assign.apply(Object, arr);
            var content = makeEmailTemplate("src/emailTemplate/reportTemplate.js", finalReport);
            const filenameRender = finalReport.client[0].clientId + new Date();
            fs.writeFile("uploads/paymentReceived/html/" + filenameRender + ".html", content, function (err) {
                if (err) {
                    return console.log(err);
                } else {
                    var html = fs.readFileSync('uploads/paymentReceived/html/' + filenameRender + '.html', 'utf8');
                    pdf1.create(html, {
                        format: 'Letter'
                    }).toFile('uploads/paymentReceived/content/' + filenameRender + '.pdf', function (err, res) {
                        if (err) return console.log(err);
                        //console.log(res); // { filename: '/app/businesscard.pdf' }
                    });
                }
            })
            sendEmail(finalReport.client[0].emailId, "PAYMENT INVOICE ", "INVOICE", {
                filename: filenameRender + '.pdf',
                path: 'uploads/paymentReceived/content/' + filenameRender + '.pdf',
                contentType: "application/pdf"
            });
            sendSmsMobile(finalReport.client[0].contactNumber, "PAYMENT INVOICE");
            return await finalReport;
        } catch (error) {
            throw error;
        }
    };

    // clientReportPaymentAndAttendance = async (information,
    //     clientList,
    //     reportType) => {
    //     try {
    //         let newClientList = [];
    //         let paymentData;
    //         let totalpaymentData = [];
    //         let businessIdinformation = information.split(",")[1];
    //         if (reportType) {
    //             if (clientList) {
    //                 let dbclientpayments = createInstance(["clientPayments"], information);
    //                 let dbClients = createInstance(["client"], information);
    //                 let dbsubscriptions = createInstance(["clientSubscription"], information);
    //                 let dbpricepack = createInstance(["clientPricepack"], information);
    //                 let dbservicepricepackmap = createInstance(["servicePricepackMap"], information);
    //                 let dbservices = createInstance(["clientService"], information);
    //                 let dbbusinessSettings = createInstance(["businessSettings"], information);
    //                 dbclientpayments.clientpayments.belongsTo(this.businessModel, {
    //                     foreignKey: "businessId",
    //                     targetKey: "businessId"
    //                 });
    //                 dbclientpayments.clientpayments.belongsTo(dbsubscriptions.subscriptions, {
    //                     foreignKey: "subscriptionsId",
    //                     targetKey: "subscriptionId"
    //                 });
    //                 dbsubscriptions.subscriptions.belongsTo(dbClients.clients, {
    //                     foreignKey: "clientId",
    //                     targetKey: "clientId"
    //                 });
    //                 dbsubscriptions.subscriptions.belongsTo(dbpricepack.pricepacks, {
    //                     foreignKey: "pricepacks",
    //                     targetKey: "pricepackId"
    //                 });
    //                 dbsubscriptions.subscriptions.belongsTo(dbservicepricepackmap.servicepricepackmap, {
    //                     foreignKey: "pricepacks",
    //                     targetKey: "pricepackId"
    //                 });
    //                 dbservicepricepackmap.servicepricepackmap.belongsTo(dbpricepack.pricepacks, {
    //                     foreignKey: "pricepackId",
    //                     targetKey: "pricepackId"
    //                 });
    //                 dbservicepricepackmap.servicepricepackmap.belongsTo(dbservices.services, {
    //                     foreignKey: "serviceId",
    //                     targetKey: "serviceId"
    //                 });
    //                 for (var i = 0; i < clientList.length; i++) {
    //                     if (clientList[i].payment_report == 1) {
    //                         newClientList.push(clientList[i].clientId);
    //                     }
    //                 }
    //                 if (newClientList) {
    //                     for (var m = 0; m < newClientList.length; m++) {
    //                         let clientid = newClientList[m];
    //                         let itemConditionsclientpayments = {
    //                             status: 1,
    //                             paymentStatus: {
    //                                 [dbclientpayments.sequelize.Op.not]: [3],
    //                             },
    //                             businessId: businessIdinformation,
    //                             subscriptionsId: [dbclientpayments.sequelize.literal('(SELECT `subscriptionId` FROM `subscriptions` WHERE `subscription->client`.`clientid` = "' + clientid + '")')]
    //                         };
    //                         let itemConditions = {};
    //                         let itemGroupConditions = {
    //                             status: 1,
    //                             businessId: businessIdinformation,
    //                         };
    //                         var itemGroupConditionsPricePack = {};
    //                         paymentData = await dbclientpayments.clientpayments.findAll({
    //                             attributes: ['clientpaymentId', 'CreateDateTime', 'subscriptionsId', 'due_amount', 'paid_amount', 'paymentStatus', ['UpdateDateTime', 'updated_at'],
    //                                 [dbclientpayments.sequelize.fn('sum', dbclientpayments.sequelize.col('paid_amount')), 'totalPayment']
    //                             ],
    //                             group: ['clientpaymentId', 'CreateDateTime'],
    //                             include: [{
    //                                 model: dbsubscriptions.subscriptions,
    //                                 attributes: ["clientId"],
    //                                 include: [{
    //                                     model: dbClients.clients,
    //                                     attributes: {
    //                                         include: [
    //                                             [dbClients.clients.sequelize.literal('CASE WHEN `subscription->client`.`photourl`  is NULL THEN CONCAT("' + KNACK_UPLOAD_URL.localUrl + '" , "noimage.jpg" ) ELSE CONCAT("' + KNACK_UPLOAD_URL.localUrl + '" , `subscription->client`.`photourl` ) END'), 'photoUrl']
    //                                         ],
    //                                         exclude: []
    //                                     },
    //                                     where: itemGroupConditions,
    //                                 },
    //                                 {
    //                                     model: dbpricepack.pricepacks,
    //                                     attributes: ["pricepackName"],
    //                                     where: itemGroupConditionsPricePack,
    //                                 }
    //                                 ],
    //                                 where: itemConditions,
    //                             },
    //                             {
    //                                 model: this.businessModel,
    //                                 attributes: {
    //                                     include: [
    //                                         "businessName", [this.businessModel.sequelize.fn('SUBSTRING', this.businessModel.sequelize.fn('SOUNDEX', this.businessModel.sequelize.col('businessName')), 1, 1), 'businessName_initials']
    //                                     ],
    //                                     exclude: ["photoUrl"]
    //                                 },
    //                             }
    //                             ],
    //                             order: [
    //                                 ['CreateDateTime', 'DESC']
    //                             ],
    //                             where: itemConditionsclientpayments,
    //                             subQuery: false,
    //                             raw: true
    //                         });
    //                         if (paymentData) {
    //                             for (var i = 0; i < paymentData.length; i++) {
    //                                 let serviceAll = await dbservicepricepackmap.servicepricepackmap.findAll({
    //                                     attributes: {
    //                                         include: [],
    //                                         exclude: [`servicepricepackmapId`, `pricepackId`, `serviceId`, `businessId`, `serviceDuration`, `createDateTime`, `updateDateTime`, `status`]
    //                                     },
    //                                     where: {
    //                                         status: 1,
    //                                         pricepackId: paymentData[i]["subscription.pricepack.pricepackId"]
    //                                     },
    //                                     include: [{
    //                                         model: dbservices.services,
    //                                         attributes: [
    //                                             [dbservicepricepackmap.sequelize.fn('GROUP_CONCAT', dbservicepricepackmap.sequelize.col('serviceName')), 'serviceNameList']
    //                                         ],
    //                                     }],
    //                                     raw: true
    //                                 });
    //                                 let serviceAllID = await dbservicepricepackmap.servicepricepackmap.findAll({
    //                                     attributes: {
    //                                         include: [],
    //                                         exclude: [`servicepricepackmapId`, `pricepackId`, `serviceId`, `businessId`, `serviceDuration`, `createDateTime`, `updateDateTime`, `status`]
    //                                     },
    //                                     where: {
    //                                         status: 1,
    //                                         pricepackId: paymentData[i]["subscription.pricepack.pricepackId"]
    //                                     },
    //                                     include: [{
    //                                         model: dbservices.services,
    //                                         attributes: [
    //                                             'serviceId'
    //                                         ],
    //                                     }],
    //                                     raw: true
    //                                 });
    //                                 paymentData[i]["serviceIDList"] = serviceAllID;
    //                                 paymentData[i]["serviceNameList"] = serviceAll;
    //                                 if (serviceAllID) {
    //                                     for (var x = 0; x < serviceAllID.length; x++) {

    //                                         this.payble_amount = paymentData[i]["paid_amount"];
    //                                         this.serviceId = serviceAllID[x]["serviceAllID"];
    //                                         this.pricepackId = serviceAllID[x]["subscription.pricepack.pricepackId"];
    //                                         paymentData[i]["getPaymentCalculation"] = await this.getPaymentCalculation(information, this.payble_amount, this.serviceId);
    //                                     }
    //                                 }
    //                             }
    //                             totalpaymentData.push(paymentData);
    //                         }
    //                     }
    //                     dbclientpayments.sequelize.close();
    //                     dbClients.sequelize.close();
    //                     dbsubscriptions.sequelize.close();
    //                     dbpricepack.sequelize.close();
    //                     dbservicepricepackmap.sequelize.close();
    //                     dbservices.sequelize.close();
    //                     dbbusinessSettings.sequelize.close();

    //                 }
    //             }
    //         }

    //         if (totalpaymentData) {
    //             for (var i = 0; i < totalpaymentData.length; i++) {
    //                 let clientpaymentsDetails = totalpaymentData[i][0];

    //                 var content = makeEmailTemplate("src/emailTemplate/multiple_reportTemplate.js", clientpaymentsDetails);

    //                 //return content;
    //                 const filenameRender = clientpaymentsDetails.clientpaymentId;
    //                 fs.writeFile("uploads/paymentReceived/html/" + filenameRender + ".html", content, function (err) {
    //                     if (err) {
    //                         return console.log(err);
    //                     } else {
    //                         var html = fs.readFileSync('uploads/paymentReceived/html/' + filenameRender + '.html', 'utf8');
    //                         pdf1.create(html, { format: 'Letter' }).toFile('uploads/paymentReceived/content/' + filenameRender + '.pdf', function (err, res) {
    //                             if (err) return console.log(err);
    //                         });
    //                     }
    //                 })
    //                 sendEmail(clientpaymentsDetails["subscription.client.emailId"], "PAYMENT INVOICE ", "INVOICE", {
    //                     filename: filenameRender + '.pdf',
    //                     path: 'uploads/paymentReceived/content/' + filenameRender + '.pdf',
    //                     contentType: "application/pdf"
    //                 });


    //             }
    //         }
    //         return totalpaymentData;
    //     } catch (error) {
    //         throw error;
    //     }

    // };

    clientReportPaymentAndAttendance = async (information,
        clientList,
        reportType) => {
        try {

            //report type 1 for full schedule, 2 for present date...

            let newClientList = [];
            let newScheduleList = [];
            let AttendanceStatus = [];
            let paymentData = [];
            let totalpaymentData = [];
            let businessIdinformation = information.split(",")[1];
            let listData = [];
            if (reportType) {
                if (clientList) {
                    newClientList = clientList;

                    let dbclientpayments = createInstance(["clientPayments"], information);
                    let dbClients = createInstance(["client"], information);
                    let dbsubscriptions = createInstance(["clientSubscription"], information);
                    let dbpricepack = createInstance(["clientPricepack"], information);
                    let dbservicepricepackmap = createInstance(["servicePricepackMap"], information);
                    let dbservices = createInstance(["clientService"], information);
                    let dbbusinessSettings = createInstance(["businessSettings"], information);
                    let dbpaymentTransactions = createInstance(["paymentTransactions"], information);
                    dbclientpayments.clientpayments.belongsTo(this.businessModel, {
                        foreignKey: "businessId",
                        targetKey: "businessId"
                    });
                    dbclientpayments.clientpayments.belongsTo(dbsubscriptions.subscriptions, {
                        foreignKey: "subscriptionsId",
                        targetKey: "subscriptionId"
                    });
                    dbsubscriptions.subscriptions.belongsTo(dbClients.clients, {
                        foreignKey: "clientId",
                        targetKey: "clientId"
                    });
                    dbsubscriptions.subscriptions.belongsTo(dbpricepack.pricepacks, {
                        foreignKey: "pricepacks",
                        targetKey: "pricepackId"
                    });
                    dbsubscriptions.subscriptions.belongsTo(dbservicepricepackmap.servicepricepackmap, {
                        foreignKey: "pricepacks",
                        targetKey: "pricepackId"
                    });
                    dbservicepricepackmap.servicepricepackmap.belongsTo(dbpricepack.pricepacks, {
                        foreignKey: "pricepackId",
                        targetKey: "pricepackId"
                    });
                    dbservicepricepackmap.servicepricepackmap.belongsTo(dbservices.services, {
                        foreignKey: "serviceId",
                        targetKey: "serviceId"
                    });
                    dbpaymentTransactions.paymentTransactions.belongsTo(dbsubscriptions.subscriptions, {
                        foreignKey: "subscriptionsId",
                        targetKey: "subscriptionId"
                    });

                    if (newClientList) {
                        for (var m = 0; m < newClientList.length; m++) {
                            let clientid = newClientList[m].clientId;
                            let subscriptionId = newClientList[m].subscriptionId;

                            // pricepack data

                            let pricepack_Amount = await dbsubscriptions.subscriptions.findAll({

                                where: {
                                    clientId: clientid,
                                    subscriptionId: subscriptionId
                                },

                                include: [{ model: dbpricepack.pricepacks }],

                                raw: true

                            });

                            //end of prcepack Data

                            if (newClientList[m].payment_report == 1) {
                                let paymentData = await SubscriptionDB.details(information, subscriptionId);

                                if (reportType == '1') {

                                    listData = await dbpaymentTransactions.paymentTransactions.findAll({
                                        attributes: {
                                            include: [],
                                            exclude: ["payment_content"]
                                        },
                                        order: [
                                            ["CreateDateTime", "DESC"]
                                        ],
                                        where: {
                                            status: 1,
                                            subscriptionsId: subscriptionId
                                        },
                                        raw: true
                                    });
                                } else {
                                    listData = await dbpaymentTransactions.paymentTransactions.findAll({
                                        attributes: {
                                            include: [],
                                            exclude: ["payment_content"]
                                        },
                                        order: [
                                            ["CreateDateTime", "DESC"]
                                        ],
                                        where: dbpaymentTransactions.sequelize.literal("YEAR(`payment_date`) = YEAR(CURDATE()) AND MONTH(`payment_date`) = MONTH(CURDATE()) AND `paymentTransactions`.`subscriptionsId` = '" + subscriptionId + "' AND `paymentTransactions`.`status` = 1"),
                                        raw: true
                                    });

                                }
                                paymentData[0].allPaymentListData = listData;
                                let clientpaymentsDetails = paymentData[0];
                                var content = makeEmailTemplate("src/emailTemplate/multiple_reportTemplate.js", clientpaymentsDetails);
                                const filenameRender = clientpaymentsDetails.subscriptionId + '__' + Date.now();
                                fs.writeFile("uploads/paymentReceived/html/" + filenameRender + ".html", content, function (err) {
                                    if (err) {
                                        return console.log(err);
                                    } else {
                                        var html = fs.readFileSync('uploads/paymentReceived/html/' + filenameRender + '.html', 'utf8');
                                        pdf1.create(html, {
                                            format: 'Letter'
                                        }).toFile('uploads/paymentReceived/content/' + filenameRender + '.pdf', function (err, res) {
                                            if (err) return console.log(err);
                                        });
                                    }
                                });
                                sendEmail(clientpaymentsDetails["client.emailId"], "PAYMENT INVOICE ", "INVOICE", {
                                    filename: filenameRender + '.pdf',
                                    path: 'uploads/paymentReceived/content/' + filenameRender + '.pdf',
                                    contentType: "application/pdf"
                                });
                                paymentData[0].paymentPdfLink = UPLOAD_DIR.localUrl + "paymentReceived/content/" + filenameRender + '.pdf';
                                newClientList[m]["payment_list"] = paymentData;
                            }
                            newClientList[m]["showClientAttendance"] = await AttendanceDB.showClientAttendanceReport(information, Array(newClientList[m].clientId), Array(newClientList[m].scheduleId), Array(newClientList[m].attendance_report), reportType);
                            newClientList[m]["PricePackAmount"]=pricepack_Amount[0]["pricepack.amount"];
                        }
                        dbclientpayments.sequelize.close();
                        dbClients.sequelize.close();
                        dbsubscriptions.sequelize.close();
                        dbpricepack.sequelize.close();
                        dbservicepricepackmap.sequelize.close();
                        dbservices.sequelize.close();
                        dbbusinessSettings.sequelize.close();
                        dbpaymentTransactions.sequelize.close();

                    }
                }
            }
            return newClientList;
        } catch (error) {
            throw error;
        }

    };


    findObjectByKey(array) {
        for (var i = 0; i < array.length; i++) {
            if (array[i][key] === value) {
                return array[i];
            }
        }
        return null;
    }



    getPaymentCalculation = async (
        information,
        payble_amount,
        serviceId,
        pricepackId,
    ) => {
        try {
            let payble_amount = this.payble_amount;
            let serviceId = this.serviceId;
            let pricepackId = this.pricepackId;
            // console.log("payble_amount======================== " + payble_amount);
            // console.log("serviceId======================== " + serviceId);
            // console.log("pricepackId======================== " + pricepackId);
            let sgstData = 0.0;
            let cgstData = 0.0;
            let taxableAmount = 0;
            let payCalculation = {
                basicAmout: 0,
                sgstPercentage: 0,
                cgstPercentage: 0,
                discountAmount: 0,
                discountPercentage: 0,
                taxableAmount: 0,
                payableAmount: 0
            };
            let dbbusinessSettings = createInstance(
                ["businessSettings"],
                information
            );
            let businessIdinformation = information.split(",")[1];
            let businessSettingData = await dbbusinessSettings.businessSettings.findAll({
                attributes: ["businessSettingsData"],
                where: {
                    status: 1,
                    businessId: businessIdinformation
                },
                raw: true
            });
            businessSettingData = JSON.parse(
                businessSettingData[0].businessSettingsData
            );
            let gstDetails = businessSettingData[2].gstDetails;
            if (gstDetails.gstApplicable) {
                sgstData = Number((Math.round(gstDetails.sgst * 100) / 100).toFixed(2));
                cgstData = Number((Math.round(gstDetails.cgst * 100) / 100).toFixed(2));
            }
            taxableAmount = (Math.round((Number(sgstData) + Number(cgstData)) * 100) / 100).toFixed(2);
            let discountsOffersDetails = businessSettingData[4].discountsOffers;
            if (Number(discountsOffersDetails.allServices) == 0) {
                if (discountsOffersDetails.selectedServices.length > 0) {
                    for (var x = 0; x < discountsOffersDetails.selectedServices.length; x++) {
                        if (
                            (discountsOffersDetails.selectedServices[x].serviceId == serviceId) &&
                            (discountsOffersDetails.selectedServices[x].pricePackId == pricepackId) && (discountsOffersDetails.selectedServices[x].status == 1)
                        ) {
                            let payble_amount_format = Number(
                                (Math.round(payble_amount * 100) / 100).toFixed(2)
                            );
                            let persectageRow = Number(
                                (discountsOffersDetails.selectedServices.length > 0) ? (discountsOffersDetails.selectedServices[x].percentage) : 0
                            );
                            let amountRow = Number(
                                (discountsOffersDetails.selectedServices.length > 0) ? (discountsOffersDetails.selectedServices[x].amount) : 0
                            );
                            if (persectageRow > 0) {
                                payCalculation.taxableAmount = payble_amount_format - persectageRow * payble_amount_format / 100;
                            } else {
                                payCalculation.taxableAmount = payble_amount_format - amountRow;
                            }
                            payCalculation.payableAmount = (Math.round(payCalculation.taxableAmount + payCalculation.taxableAmount * (sgstData + cgstData) / 100)).toFixed(2);
                            payCalculation.taxableAmount = (Math.round(payCalculation.taxableAmount)).toFixed(2);
                            payCalculation.basicAmout = payble_amount;
                            payCalculation.sgstPercentage = sgstData;
                            payCalculation.cgstPercentage = cgstData;
                            payCalculation.discountAmount = amountRow;
                            payCalculation.discountPercentage = persectageRow;
                        } else {
                            let payble_amount_format = Number(
                                (Math.round(payble_amount * 100) / 100).toFixed(2)
                            );
                            let persectageRow = Number(
                                (discountsOffersDetails.selectedServices.length > 0) ? (discountsOffersDetails.selectedServices[x].percentage) : 0
                            );
                            let amountRow = Number(
                                (discountsOffersDetails.selectedServices.length > 0) ? (discountsOffersDetails.selectedServices[x].amount) : 0
                            );

                            if (persectageRow > 0) {
                                payCalculation.taxableAmount = payble_amount_format - persectageRow * payble_amount_format / 100;
                            } else {
                                payCalculation.taxableAmount = payble_amount_format - amountRow;
                            }
                            payCalculation.payableAmount = (Math.round(payCalculation.taxableAmount + payCalculation.taxableAmount * (sgstData + cgstData) / 100)).toFixed(2);
                            payCalculation.taxableAmount = (Math.round(payCalculation.taxableAmount)).toFixed(2);
                            payCalculation.basicAmout = payble_amount;
                            payCalculation.sgstPercentage = sgstData;
                            payCalculation.cgstPercentage = cgstData;
                            payCalculation.discountAmount = amountRow;
                            payCalculation.discountPercentage = persectageRow;
                        }
                    }
                } else {
                    let payble_amount_format = Number(
                        (Math.round(payble_amount * 100) / 100).toFixed(2)
                    );
                    let persectageRow = Number(
                        (discountsOffersDetails.selectedServices.length > 0) ? (discountsOffersDetails.selectedServices[x].percentage) : 0
                    );
                    let amountRow = Number(
                        (discountsOffersDetails.selectedServices.length > 0) ? (discountsOffersDetails.selectedServices[x].amount) : 0
                    );
                    if (persectageRow > 0) {
                        payCalculation.taxableAmount = payble_amount_format - persectageRow * payble_amount_format / 100;
                    } else {
                        payCalculation.taxableAmount = payble_amount_format - amountRow;
                    }
                    payCalculation.payableAmount = (Math.round(payCalculation.taxableAmount + payCalculation.taxableAmount * (sgstData + cgstData) / 100)).toFixed(2);
                    payCalculation.taxableAmount = (Math.round(payCalculation.taxableAmount)).toFixed(2);
                    payCalculation.basicAmout = payble_amount;
                    payCalculation.sgstPercentage = sgstData;
                    payCalculation.cgstPercentage = cgstData;
                    payCalculation.discountAmount = amountRow;
                    payCalculation.discountPercentage = persectageRow;
                }
            } else {
                let payble_amount_format = Number(
                    (Math.round(payble_amount * 100) / 100).toFixed(2)
                );
                let persectageRow = Number(
                    (discountsOffersDetails.selectedServices.length > 0) ? (discountsOffersDetails.selectedServices[x].percentage) : 0
                );
                let amountRow = Number(
                    (discountsOffersDetails.selectedServices.length > 0) ? (discountsOffersDetails.selectedServices[x].amount) : 0
                );
                if (persectageRow > 0) {
                    payCalculation.taxableAmount = payble_amount_format - persectageRow * payble_amount_format / 100;
                } else {
                    payCalculation.taxableAmount = payble_amount_format - amountRow;
                }
                payCalculation.payableAmount = (Math.round(payCalculation.taxableAmount + payCalculation.taxableAmount * (sgstData + cgstData) / 100)).toFixed(2);
                payCalculation.taxableAmount = (Math.round(payCalculation.taxableAmount)).toFixed(2);
                payCalculation.basicAmout = payble_amount;
                payCalculation.sgstPercentage = sgstData;
                payCalculation.cgstPercentage = cgstData;
                payCalculation.discountAmount = amountRow;
                payCalculation.discountPercentage = persectageRow;
            }
            return payCalculation;
        } catch (error) {
            throw error;
        }
    };


    reportclientlist__ = async (
        information,
        params,
        filterPrivateFields = true
    ) => {
        try {
            var businessIdinformation = information.split(",")[1];
            let dbClientSchedule = createInstance(["clientSchedule"], information);
            let dbCSMap = createInstance(["clientScheduleMap"], information);
            let dbClients = createInstance(["client"], information);

            dbClientSchedule.schedule.belongsTo(dbCSMap.clientschedulemap, {
                foreignKey: "scheduleId",
                targetKey: "scheduleId"
            });

            dbCSMap.clientschedulemap.belongsTo(dbClients.clients, {
                foreignKey: "clientId",
                targetKey: "clientId"
            });

            let filter = (params[0]) ? params[0] : '';
            var pageNo = (params['page']) ? params['page'] : 1;
            var limitPerPage = PEGINATION_LIMIT;
            var offsetLimit = (pageNo - 1) * limitPerPage;
            let orderBy;
            if ((params["order"] == 1)) {
                orderBy = [
                    ["CreateDateTime", "DESC"]
                ];
            } else {
                orderBy = [
                    ["CreateDateTime", "ASC"]
                ];
            }
            let itemConditionsclientpayments = {
                status: 1,
                businessId: businessIdinformation
            };

            let scheduleClients = await dbClientSchedule.schedule.findAndCount({
                attributes: ['scheduleId'],
                include: [{
                    model: dbCSMap.clientschedulemap,
                    attributes: ["clientScheduleMapId", "clientId"],
                    required: true,
                    include: [{
                        model: dbClients.clients,
                        attributes: {
                            include: ['clientId', 'clientName', 'contactNumber', 'emailId', [dbClients.clients.sequelize.literal('CASE WHEN photoUrl  is NULL THEN CONCAT("' + KNACK_UPLOAD_URL.localUrl + '" , "noimage.jpg" ) ELSE CONCAT("' + KNACK_UPLOAD_URL.localUrl + '" , photoUrl ) END'), 'photoUrl'],]
                        },
                        where: {
                            clientName: {
                                like: '%' + filter + '%'
                            }
                        },

                    }],

                }],

                order: orderBy,
                where: itemConditionsclientpayments,
                subQuery: false,
                offset: offsetLimit,
                limit: limitPerPage,
                raw: true

            });
            return scheduleClients;

        } catch (error) {
            throw error;
        }
    };

    reportclientlist = async (
        information,
        params,
        filterPrivateFields = true
    ) => {
        try {
            var businessIdinformation = information.split(",")[1];
            let dbClientSchedule = createInstance(["clientSchedule"], information);
            let dbCSMap = createInstance(["clientScheduleMap"], information);
            let dbClients = createInstance(["client"], information);
            let dbsubscriptions = createInstance(["clientSubscription"], information);
            let dbpricepack = createInstance(["clientPricepack"], information);

            dbClientSchedule.schedule.belongsTo(dbCSMap.clientschedulemap, {
                foreignKey: "scheduleId",
                targetKey: "scheduleId"
            });

            dbCSMap.clientschedulemap.belongsTo(dbClients.clients, {
                foreignKey: "clientId",
                targetKey: "clientId"
            });

            dbCSMap.clientschedulemap.belongsTo(dbsubscriptions.subscriptions, {
                foreignKey: "subscriptionId",
                targetKey: "subscriptionId"
            });

            dbsubscriptions.subscriptions.belongsTo(dbpricepack.pricepacks, {
                foreignKey: "pricepacks",
                targetKey: "pricepackId"
            });

            let filter = (params[0]) ? params[0] : '';
            var pageNo = (params['page']) ? params['page'] : 1;
            var limitPerPage = PEGINATION_LIMIT;
            var offsetLimit = (pageNo - 1) * limitPerPage;
            let orderBy;
            if ((params["order"] == 1)) {
                orderBy = [
                    ["CreateDateTime", "DESC"]
                ];
            } else {
                orderBy = [
                    ["CreateDateTime", "ASC"]
                ];
            }
            let itemConditionsclientpayments = {
                status: 1,
                businessId: businessIdinformation
            };
            let scheduleClients = await dbClientSchedule.schedule.findAll({
                attributes: ['scheduleId'],
                include: [{
                    model: dbCSMap.clientschedulemap,
                    attributes: ["clientScheduleMapId", "clientId", 'subscriptionId', 'sessionCount'],
                    include: [{
                        model: dbClients.clients,
                        attributes: {
                            include: ['clientId', 'clientName', 'contactNumber', 'emailId', [dbClients.clients.sequelize.literal('CASE WHEN photoUrl  is NULL THEN CONCAT("' + KNACK_UPLOAD_URL.localUrl + '" , "noimage.jpg" ) ELSE CONCAT("' + KNACK_UPLOAD_URL.localUrl + '" , photoUrl ) END'), 'photoUrl'],]
                        }
                    }],

                }],

                order: orderBy,
                where: itemConditionsclientpayments,
                subQuery: false,
                offset: offsetLimit,
                limit: limitPerPage,
                raw: true

            });

            for (let i = 0; i < scheduleClients.length; i++) {
                scheduleClients[i]['pricepacks'] = await dbsubscriptions.subscriptions.findAll({
                    attributes: ['subscriptionId', 'subscriptionDateTime'],
                    where: {
                        subscriptionId: scheduleClients[i]["clientschedulemap.subscriptionId"]
                    },
                    include: [{
                        model: dbpricepack.pricepacks,
                        attributes: ['pricepackId', 'pricepackName', 'pricepackValidity', 'serviceDuration'],
                    }],
                    raw: true
                });
            }

            for (let y = 0; y < scheduleClients.length; y++) {
                //console.log("==============",scheduleClients[y]['pricepacks'][0]['pricepack.serviceDuration']);
                scheduleClients[y]['session'] = scheduleClients[y]['pricepacks'][0]['pricepack.serviceDuration'] + '/' + scheduleClients[y]['clientschedulemap.sessionCount']
            }

            return scheduleClients;

        } catch (error) {
            throw error;
        }
    };

    // Client Reports
    ClientReports = async (
        information,
        clientId,
        page,
        searchKey,
        filterPrivateFields = true
    ) => {
        try {
            let arr = information.split(",");
            let businessId = arr[1];
            let attendance, session;
            let td = new Date().toISOString().split('T')[0];
            let today = new Date(td);
            let dbSchedule = createInstance(["clientSchedule"], information);
            let dbCSM = createInstance(["clientScheduleMap"], information);
            let dbServices = createInstance(["clientService"], information);
            let dbPricepack = createInstance(["clientPricepack"], information);
            let dbSub = createInstance(["clientSubscription"], information);

            dbSchedule.schedule.hasOne(dbCSM.clientschedulemap, {
                foreignKey: "scheduleId",
                targetKey: "scheduleId"
            });

            dbSub.subscriptions.belongsTo(dbPricepack.pricepacks, {
                foreignKey: "pricepacks",
                targetKey: "pricepackId"
            })

            dbCSM.clientschedulemap.belongsTo(dbSub.subscriptions, {
                foreignKey: "subscriptionId",
                targetKey: "subscriptionId"
            })
            dbSchedule.schedule.belongsTo(dbServices.services, {
                foreignKey: "serviceId",
                targetKey: "serviceId"
            });


            let pageNo = parseInt(page);
            let showLimit;
            if (pageNo > 0) {
                pageNo = parseInt(page);
                showLimit = PEGINATION_LIMIT; //show 2 records per page
            } else {
                pageNo = 1;
                showLimit = 1000; //show first 1000 records if no page number is given
            }



            let schedules = await dbSchedule.schedule.findAll({
                where: {
                    status: 1,
                    businessId: businessId,
                },
                include: [{
                    model: dbServices.services,
                    attributes: ['serviceName'],
                }],
                raw: true
            });

            //showing the necessary data
            let pendingSchedules = [];
            for (let schedule of schedules) { //loop through all the bookings



                let clients = await dbCSM.clientschedulemap.findAll({
                    where: {
                        scheduleId: schedule.scheduleId,
                        status: 1,
                        clientId: clientId
                    },
                    include: [{
                        model: dbSub.subscriptions,
                        include: [{
                            model: dbPricepack.pricepacks
                        }]
                    }],
                    raw: true
                });



                let scheduleArr = [];
                if (clients.length > 0) {

                    Array.prototype.push.apply(scheduleArr, [{
                        scheduleId: schedule.scheduleId
                    }]);
                    schedule['bookingName'] = schedule.scheduleName === null ? schedule['service.serviceName'] : schedule.scheduleName;
                    Array.prototype.push.apply(scheduleArr, [{
                        SubscriptionID: clients[0]["subscription.subscriptionId"]
                    }]);
                    Array.prototype.push.apply(scheduleArr, [{
                        PricepackID: clients[0]["subscription.pricepack.pricepackId"]
                    }]);
                    Array.prototype.push.apply(scheduleArr, [{
                        PricepackName: clients[0]["subscription.pricepack.pricepackName"]
                    }]);
                    Array.prototype.push.apply(scheduleArr, [{
                        PricepackAmount: clients[0]["subscription.pricepack.amount"]
                    }]);
                    Array.prototype.push.apply(scheduleArr, [{
                        scheduleName: schedule.bookingName
                    }]);
                    Array.prototype.push.apply(scheduleArr, [{
                        serviceName: schedule['service.serviceName']
                    }]);
                    Array.prototype.push.apply(scheduleArr, [{
                        scheduleStartTime: schedule.startTime
                    }]);
                    Array.prototype.push.apply(scheduleArr, [{
                        scheduleEndTime: schedule.endTime
                    }]);
                    let scheduleObj = [];
                    scheduleObj = Object.assign.apply(Object, scheduleArr);
                    pendingSchedules = pendingSchedules.concat(scheduleObj);
                }
            }

            //pagination & search
            let data = await rsPaginate(pendingSchedules, pageNo, showLimit);
            let resultSet = [];
            if (searchKey[0].trim() == '') {
                resultSet = data;
            } else {
                if (data.length > 0) {
                    for (let i = 0; i < data.length; i++) {
                        let schedule_name = data[i].scheduleName.toLowerCase();
                        let service_name = data[i].serviceName.toLowerCase();
                        if (schedule_name.includes(searchKey[0]) || service_name.includes(searchKey[0])) {
                            resultSet = resultSet.concat(data[i]);
                        }
                    }
                } else resultSet = [];
            }
            dbServices.sequelize.close();
            dbCSM.sequelize.close();
            dbSchedule.sequelize.close();
            dbPricepack.sequelize.close();
            return await resultSet;
        } catch (error) {
            throw error;
        }
    };

}
export default new ReportsDB();