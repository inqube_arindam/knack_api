import {
    ApplicationError
} from "../../lib/errors";
import BaseModel from "../../db/BaseModel";
import {
    filterFields,
    createInstance,
    makeid, formatDate
} from "../../lib/common";
import {
    hashPassword,
    comparePassword
} from "../../lib/crypto";
import {
    textNotification,
    mailNotification
} from "../../lib/notification";
import Sequelize from "sequelize";
import {
    KNACKDB,
    KNACK_DBSETTINGS,
    KNACK_UPLOAD_URL,
    KNACK_TEAM_UPLOAD_URL,
    PEGINATION_LIMIT
} from "../../lib/constants.js";
import {
    addEmployee,
    viewEmployee,
    updateEmployee,
    deleteEmployee,
    countEmployee
} from "../ClientHelperModels/ClientEmployeeHelperModel";
import PaymentHelperDB from "../HelperModels/PaymentHelperModel";
import PackageHelperDB from "../HelperModels/PackageHelperModel";
import loginSignUpHelperDB from "../HelperModels/LoginSignUpHelperModel";
import {
    viewTeamScheduleMap,
    deleteTeamScheduleMap
} from "../ClientHelperModels/TeamScheduleMapHelperModel";
import ClientSchema from "../../schemas/role.schema.js";
import BusinessDB from "../BusinessModel";

const PUBLIC_FIELDS = [
    "centerId",
    "employeeId",
    "businessId",
    "employeeName",
    "emailId",
    "contactNumber",
    "area",
    "pin",
    "city"
];

export class EmployeeDB {
    employees = [];

    constructor(connection) {
        //Get Role Data
        // super("role", connection);
        // this.roleSchema = ClientSchema.role();
        // this.roleName = "role";
        // this.roleModel = this.connection.model(this.roleName, this.roleSchema);   
    }

    create = async (
        information,
        centerId,
        employeeName,
        contactNumber,
        emailId,
        alternateNumber,
        alternateEmail,
        dateOfBirth,
        area,
        pin,
        city
    ) => {
        try {
            let arrayOfStrings = information.split(",");
            let userId = arrayOfStrings.length > 2 ? arrayOfStrings[2] : null;

            let dbEmployee = createInstance(["clientEmployee"], information);
            var businessIdinformation = information.split(",")[1];
            let setCond = {
                status: 1,
                businessId: businessIdinformation
            };
            let userNumber = await countEmployee(information, dbEmployee, setCond);
            dbEmployee.sequelize.close();

            let setUserPaymentCond = {
                status: 1,
                isRenewl: 1,
                businessId: businessIdinformation
            };

            let paymentInfo = await PaymentHelperDB.viewPayment(setUserPaymentCond);
            if (paymentInfo.length > 0) {
                let setCondPayment = {
                    status: 1,
                    packageId: paymentInfo[0].packageId
                };
                let packageInfo = await PackageHelperDB.viewPackage(setCondPayment);
                let userNumberInPackage = packageInfo[0].numberOfTeamMembers;

                let dob;
                if (dateOfBirth) {
                    dob = (dateOfBirth == '') ? null : await formatDate(dateOfBirth);
                } else {
                    dob = null;
                }

                // Employee info
                const newEmployee = {
                    businessId: businessIdinformation,
                    centerId,
                    employeeName,
                    contactNumber,
                    emailId,
                    alternateNumber,
                    alternateEmail,
                    dateOfBirth: dob,
                    area,
                    pin,
                    city,
                    createdBy: userId
                };


                if (userNumber < userNumberInPackage) {
                    let dbEmployee = createInstance(["clientEmployee"], information);
                    let db = createInstance(["client"], information);
                    let employees;
                    try {
                        employees = await dbEmployee.employees.create(newEmployee);
                    } catch (err) {
                        throw new ApplicationError(err.parent.sqlMessage, 400);
                    }

                    let setCondViewLogin = {
                        businessId: businessIdinformation,
                        userType: 1
                    };
                    let loginInfo = await loginSignUpHelperDB.viewLogin(setCondViewLogin);
                    let businessInfo = await BusinessDB.details(information);

                    if (loginInfo.length > 0) {

                        let dbBusinessSettings = createInstance(["businessSettings"], information);
                        let permission = await dbBusinessSettings.businessSettings.findAll({
                            where: {
                                businessId: businessIdinformation
                            },
                            raw: true
                        });


                        let x = JSON.parse(permission[0].businessSettingsData);

                        if (x[0].notification["dailyInternalUpdate"] == 1) {
                            //text and mail for employee/team member
                            let phno = contactNumber;
                            let text = "Hi " + employeeName + ", you have been successfully added as a team member for " + loginInfo[0].name + "'s " + businessInfo[0].businessType + " on Knack. For any help or assistance, reach out to us anytime at +91 9820789803. ";
                            textNotification(text, phno);

                            let email_emp = emailId;

                            let subject_emp = "Congratulations! You have been successfully added as a team member to " + businessInfo[0].businessType;

                            let body_emp = "Hi " + employeeName + ",<br><br>Congratulations! You have been successfully added as a team member for " + businessInfo[0].businessType + ".<br><br> For any help or assistance, reach out to us anytime at support@justknackit.com";

                            mailNotification(subject_emp, body_emp, email_emp);

                            //text and mail for main admins
                            loginInfo.forEach((user) => {
                                let phno2 = user.contactNumber;
                                let emp_name = employeeName.toUpperCase();
                                let text2 = "Hi " + loginInfo[0].name + ", " + emp_name + " has been successfully added as a team member on Knack. For any help or assistance, reach out to us anytime at +91 9820789803. ";
                                let email = user.emailId;
                                let subject = "Congratulations! Team member successfully added";
                                let body = "Hi " + loginInfo[0].name + ",<br><br>Congratulations! " + emp_name + " has been successfully added as a team member to the platform.<br><br>For any help or assistance, reach out to us anytime at support@justknackit.com";
                                textNotification(text2, phno2);
                                mailNotification(subject, body, email);
                            });
                        }


                    }

                    dbEmployee.sequelize.close();
                    return await employees;
                } else {
                    throw new ApplicationError(
                        "Unable to add new user, please check package in details",
                        401
                    );
                }
            } else {
                throw new ApplicationError(
                    "Payment is not found !",
                    401
                );
            }
        } catch (error) {
            throw error;
        }
    };

    listAll = async (information, businessId, scheduleId, filterPrivateFields = true) => {
        try {
            let db = createInstance(["clientEmployee"], information);
            let dbteam_schedule = createInstance(["teamScheduleMap"], information);
            let dbSchedule = createInstance(["clientSchedule"], information);

            var filterData = [];
            var arr = [];
            var c = 0;
            var count = 0;
            for (var i = 65; i <= 90; i++) {
                arr[c] = String.fromCharCode(i).toLowerCase();
                let filterClients = await db.employees.findAll({
                    order: [
                        ["employeeName", "asc"]
                    ],
                    where: {
                        businessId: businessId,
                        status: 1,
                        employeeName: {
                            like: [arr[c] + "%"]
                        }
                    },
                    raw: true
                });

                if (filterClients.length) {
                    var merged = {};
                    var m = [];
                    var con = [];
                    for (var k = 0; k < filterClients.length; k++) {
                        var noschedule = [];
                        let teamschedulemap = await dbteam_schedule.teamschedulemap.findAll({
                            where: {
                                teamId: filterClients[k].employeeId
                            },
                            raw: true
                        });
                        var clientphoto = (filterClients[k].photoUrl === null) ? KNACK_UPLOAD_URL.localUrl + 'noimage.jpg' : KNACK_UPLOAD_URL.localUrl + filterClients[k].photoUrl;
                        filterClients[k]['clientPhoto'] = clientphoto;

                        // checking schedule id
                        let setschedules = [];
                        let issetsch = [];
                        if (teamschedulemap.length > 0) {

                            for (let tms of teamschedulemap) {
                                var arrnew = [];

                                let schedules = await dbSchedule.schedule.findAll({
                                    where: {
                                        scheduleId: tms.scheduleId,
                                        status: 1
                                    },
                                    raw: true
                                });
                                if (schedules.length > 0) {
                                    var isschedued = (scheduleId[0] === tms.scheduleId) ? '1' : '0';
                                    setschedules = setschedules.concat([{
                                        scheduleId: schedules[0].scheduleId,
                                        schedueName: schedules[0].scheduleName,
                                        isscheduled: isschedued
                                    }]);
                                } else {
                                    setschedules = [];
                                }
                            }
                            Array.prototype.push.apply(arrnew, [{
                                schedules: setschedules
                            }]);
                            Array.prototype.push.apply(arrnew, [filterClients[k]]);
                            Array.prototype.push.apply(arrnew, [{
                                count: count++
                            }]);
                            m = Object.assign.apply(Object, arrnew);
                            con = con.concat(m);
                        } else {
                            Array.prototype.push.apply(noschedule, [{
                                schedules: [{}]
                            }]);
                            Array.prototype.push.apply(noschedule, [filterClients[k]]);
                            Array.prototype.push.apply(noschedule, [{
                                count: count++
                            }]);
                            m = Object.assign.apply(Object, noschedule);
                            con = con.concat(m);
                        }
                    }
                    merged = Object.assign.apply(Object, [
                        //{ [arr[c].toUpperCase()]: con }
                        {
                            title: arr[c].toUpperCase(),
                            data: con
                        }
                    ]);
                    filterData = filterData.concat(merged);
                }
                c++;
            }
            db.sequelize.close();
            return await filterData;
        } catch (error) {
            throw error;
        }
    };

    teamManagement = async (information, params, filterPrivateFields = true) => {
        try {
            var businessIdinformation = information.split(",")[1];
            var pageNo = (params['page']) ? params['page'] : 1;
            var limitPerPage = PEGINATION_LIMIT;
            var offsetLimit = (pageNo - 1) * limitPerPage;
            let dbteam = createInstance(["clientEmployee"], information);
            let teamlist = await dbteam.employees.findAll({
                attributes: ['*', [dbteam.sequelize.literal('CASE WHEN photoUrl  is NULL THEN CONCAT("' + KNACK_TEAM_UPLOAD_URL.localUrl + '" , "noimage.jpg" ) ELSE CONCAT("' + KNACK_TEAM_UPLOAD_URL.localUrl + '" , photoUrl ) END'), 'photoUrl'],

                    [dbteam.sequelize.literal('CASE WHEN roleId IS NULL OR roleId = 0 or roleId = 1  THEN "Main Admin" ELSE "Center Admin" END'), 'roleName']
                ],
                order: [
                    ["updateDateTime", "desc"]
                ],
                where: {
                    businessId: businessIdinformation,
                    status: 1
                },
                subQuery: false,
                offset: offsetLimit,
                limit: limitPerPage,
                raw: true
            });
            dbteam.sequelize.close();
            return await teamlist;
        } catch (error) {
            throw error;
        }
    }

    teamManagementAndSchedule_old = async (information, scheduleId, params, filterPrivateFields = true) => {
        try {
            var businessIdinformation = information.split(",")[1];
            var pageNo = (params['page']) ? params['page'] : 1;
            var limitPerPage = PEGINATION_LIMIT;
            var offsetLimit = (pageNo - 1) * limitPerPage;
            let dbteam = createInstance(["clientEmployee"], information);
            let dbteam_schedule = createInstance(["teamScheduleMap"], information);
            let dbSchedule = createInstance(["clientSchedule"], information);

            dbteam.employees.hasOne(dbteam_schedule.teamschedulemap, {
                foreignKey: "teamId",
                targetKey: "employeeId",
            });

            dbteam_schedule.teamschedulemap.belongsTo(dbSchedule.schedule, {
                foreignKey: "scheduleId",
                targetKey: "scheduleId",
            });


            let teamlist = await dbteam.employees.findAll({
                attributes: ['*', [dbteam.sequelize.literal('CASE WHEN photoUrl  is NULL THEN CONCAT("' + KNACK_TEAM_UPLOAD_URL.localUrl + '" , "noimage.jpg" ) ELSE CONCAT("' + KNACK_TEAM_UPLOAD_URL.localUrl + '" , photoUrl ) END'), 'photoUrl'],

                    [dbteam.sequelize.literal('CASE WHEN roleId IS NULL OR roleId = 0 or roleId = 1  THEN "Main Admin" ELSE "Center Admin" END'), 'roleName']
                ],
                order: [
                    ["updateDateTime", "desc"]
                ],
                where: {
                    businessId: businessIdinformation,
                    status: 1
                },
                include: [{
                    model: dbteam_schedule.teamschedulemap,
                    required: true,

                    attributes: ['scheduleId'],

                    include: [

                        {
                            model: dbSchedule.schedule,
                            attributes: ['scheduleId', 'scheduleName'],
                        }

                    ]
                }],

                where: {
                    employeeName: {

                        like: ["%" + params[0] + "%"]

                    }
                },
                subQuery: false,
                offset: offsetLimit,
                limit: limitPerPage,
                raw: true
            });
            for (let i = 0; i < teamlist.length; i++) {

                let IsPresent = 0;
                let setschedules = [];
                if (teamlist[i]["teamschedulemap.scheduleId"] == scheduleId) {

                    IsPresent = 1;
                    setschedules = setschedules.concat([{
                        scheduleId: teamlist[i]["teamschedulemap.scheduleId"],
                        schedueName: teamlist[i]["teamschedulemap.schedule.scheduleName"],
                        isscheduled: IsPresent
                    }]);
                } else {
                    IsPresent = 0;
                    setschedules = setschedules.concat([{
                        scheduleId: teamlist[i]["teamschedulemap.scheduleId"],
                        schedueName: teamlist[i]["teamschedulemap.schedule.scheduleName"],
                        isscheduled: IsPresent
                    }]);
                }

                teamlist[i]["schedules"] = setschedules;
            }


            dbteam.sequelize.close();
            return await teamlist;
        } catch (error) {
            throw error;
        }
    }

    teamManagementAndSchedule = async (information, scheduleId, params, filterPrivateFields = true) => {
        try {
            let businessIdinformation = information.split(",")[1];
            let dbTeam = createInstance(["clientEmployee"], information);
            let dbTSM = createInstance(["teamScheduleMap"], information);
            let dbSchedule = createInstance(["clientSchedule"], information);

            let pageNo = (params['page']) ? params['page'] : 1;
            let limitPerPage = PEGINATION_LIMIT;
            let offsetLimit = (pageNo - 1) * limitPerPage;

            let teamlist = await dbTeam.employees.findAll({
                attributes: ['*', [dbTeam.sequelize.literal('CASE WHEN photoUrl  is NULL THEN CONCAT("' + KNACK_TEAM_UPLOAD_URL.localUrl + '" , "noimage.jpg" ) ELSE CONCAT("' + KNACK_TEAM_UPLOAD_URL.localUrl + '" , photoUrl ) END'), 'photoUrl'],
                    [dbTeam.sequelize.literal('CASE WHEN roleId IS NULL OR roleId = 0 or roleId = 1  THEN "Main Admin" ELSE "Center Admin" END'), 'roleName']
                ],
                order: [
                    ["updateDateTime", "desc"]
                ],
                where: {
                    businessId: businessIdinformation,
                    status: 1,
                    employeeName: {
                        like: ['%' + params[0] + '%']
                    }
                },
                subQuery: false,
                offset: offsetLimit,
                limit: limitPerPage,
                raw: true
            });

            var mergedTeamScheduleObj = [];
            var teamScheduleInfo = [];
            for (var k = 0; k < teamlist.length; k++) {
                let scheduleInfo = [];
                let teamschedulemap = await dbTSM.teamschedulemap.findAll({
                    where: {
                        teamId: teamlist[k].employeeId
                    },
                    raw: true
                });
                let setschedules = [];
                if (teamschedulemap.length > 0) {
                    for (let tsm of teamschedulemap) {
                        var mergedTeamScheduleArr = [];
                        let schedules = await dbSchedule.schedule.findAll({
                            where: {
                                scheduleId: tsm.scheduleId,
                                status: 1
                            },
                            raw: true
                        });
                        if (schedules.length > 0) {
                            var isschedued = (scheduleId === tsm.scheduleId) ? '1' : '0';
                            setschedules = setschedules.concat([{
                                scheduleId: schedules[0].scheduleId,
                                schedueName: schedules[0].scheduleName,
                                isscheduled: isschedued
                            }]);
                        } else {
                            setschedules = [];
                        }
                    }
                    Array.prototype.push.apply(mergedTeamScheduleArr, [teamlist[k]]);
                    Array.prototype.push.apply(mergedTeamScheduleArr, [{
                        schedules: setschedules
                    }]);
                    mergedTeamScheduleObj = Object.assign.apply(Object, mergedTeamScheduleArr);
                    teamScheduleInfo = teamScheduleInfo.concat(mergedTeamScheduleObj);
                } else {
                    Array.prototype.push.apply(scheduleInfo, [teamlist[k]]);
                    Array.prototype.push.apply(scheduleInfo, [{
                        schedules: [{}]
                    }]);
                    mergedTeamScheduleObj = Object.assign.apply(Object, scheduleInfo);
                    teamScheduleInfo = teamScheduleInfo.concat(mergedTeamScheduleObj);
                }
            }

            dbTeam.sequelize.close();
            return await teamScheduleInfo;
        } catch (error) {
            throw error;
        }
    }

    listAllcenter = async (information, centerId, filterPrivateFields = true) => {
        try {
            let db = createInstance(["clientEmployee"], information);
            let employees = await db.employees.findAll({
                where: {
                    status: 1,
                    centerId: centerId
                },
                raw: true
            });
            db.sequelize.close();
            if (filterPrivateFields) {
                return await filterFields(employees, PUBLIC_FIELDS);
            }
        } catch (error) {
            throw error;
        }
    };

    employeeDetails = async (
        information,
        employeeId,
        filterPrivateFields = true
    ) => {
        try {
            let dbteam = createInstance(["clientEmployee"], information);
            let employees = await dbteam.employees.findAll({
                attributes: ['*', [dbteam.sequelize.literal('CASE WHEN photoUrl  is NULL THEN CONCAT("' + KNACK_TEAM_UPLOAD_URL.localUrl + '" , "noimage.jpg" ) ELSE CONCAT("' + KNACK_TEAM_UPLOAD_URL.localUrl + '" , photoUrl ) END'), 'photoUrl'],

                    [dbteam.sequelize.literal('CASE WHEN roleId IS NULL OR roleId = 0  THEN "Main Admin" ELSE "Center Admin" END'), 'roleName']
                ],
                where: {
                    status: 1,
                    employeeId: employeeId
                },
                raw: true
            });
            dbteam.sequelize.close();
            return await employees;
        } catch (error) {
            throw error;
        }
    };

    update = async (
        information,
        employeeId,
        userId,
        roleId,
        centerId,
        employeeName,
        contactNumber,
        emailId,
        alternateNumber,
        alternateEmail,
        dateOfBirth,
        area,
        pin,
        city,
        photoUrl
    ) => {
        try {
            let dbEmployee = createInstance(["clientEmployee"], information);
            var businessIdinformation = information.split(",")[1];

            if (photoUrl) {
                var filename = photoUrl.name;

                // Use the mv() method to place the file somewhere on your server
                await photoUrl.mv("./uploads/users/" + filename, function (err) {
                    console.log("File uploading error : ", err);
                });
                var photoUrl = filename;
            } else {
                var photoUrl = null;
            }

            let setCond = {
                employeeId: employeeId
            };

            let dob = '';
            if (dateOfBirth) {
                dob = (dateOfBirth == '') ? null : await formatDate(dateOfBirth);
            } else {
                dob = null;
            }

            let setData = {
                roleId: roleId,
                centerId: centerId,
                businessId: businessIdinformation,
                employeeName: employeeName,
                contactNumber: contactNumber,
                emailId: emailId,
                alternateNumber: alternateNumber,
                alternateEmail: alternateEmail,
                dateOfBirth: dob,
                area: area,
                pin: pin,
                city: city,
                photoUrl: photoUrl,
                updateDateTime: Date.now()
            };

            const Op = Sequelize.Op;
            let checkemployee = await dbEmployee.employees.findAll({
                where: {
                    status: 1,
                    employeeId: {
                        [Op.notIn]: [employeeId]
                    }
                },
                raw: true
            });

            let employees;
            if (checkemployee.length > 0) {
                try {
                    employees = await updateEmployee(information, dbEmployee, setCond, setData);
                    //update main db logininfo table
                    let cond = {
                        userId: employeeId
                    }
                    let empDetails = await loginSignUpHelperDB.getEmployeeDetails(cond);
                    if (empDetails > 0) {
                        let loginInfoUpdateData = {
                            name: employeeName,
                            contactNumber: contactNumber
                        };
                        let loginInfo = await loginSignUpHelperDB.updateLogin(cond, loginInfoUpdateData);
                    }

                    let setCondViewLogin = {
                        businessId: businessIdinformation,
                        userType: 1
                    };
                    let loginInfo = await loginSignUpHelperDB.viewLogin(setCondViewLogin);
                    if (loginInfo.length > 0) {
                        //text and mail for employee
                        let phno = contactNumber;
                        let text = "You have been successfully updated to knack";;
                        let email_emp = emailId;
                        let subject_emp = "Knack Confirmation";
                        let body_emp = "You have been successfully updated to knack";
                        textNotification(text, phno);
                        mailNotification(subject_emp, body_emp, email_emp);

                        //text and mail for main admins
                        loginInfo.forEach((user) => {
                            let phno2 = user.contactNumber;
                            let emp_name = employeeName;
                            let text2 = emp_name + " is successfully updated to knack";
                            let email = user.emailId;
                            let subject = "Knack Confirmation";
                            let body = employeeName + ' is successfully updated to knack';
                            textNotification(text2, phno2);
                            mailNotification(subject, body, email);
                        });
                    }

                } catch (err) {
                    throw new ApplicationError(err.parent.sqlMessage, 400);
                }
                dbEmployee.sequelize.close();
            }


            return employees;
        } catch (error) {
            throw error;
        }
    };

    updateTeamRoleDb_ = async (
        information,
        userId,
        employeeId,
        roleId,
        params
    ) => {
        try {
            let dbEmployee = createInstance(["clientEmployee"], information);
            var businessIdinformation = information.split(",")[1];

            let setCond = {
                employeeId: employeeId
            };
            let setData = {
                roleId: roleId,
                updateDateTime: Date.now()
            };
            const Op = Sequelize.Op;
            let checkemployee = await dbEmployee.employees.findAll({
                where: {
                    status: 1,
                    employeeId: {
                        [Op.notIn]: [employeeId]
                    }
                },
                raw: true
            });

            let getteamInfo = await dbEmployee.employees.findAll({
                where: {
                    status: 1,
                    employeeId: employeeId
                },
                raw: true
            });

            if (!getteamInfo[0].emailId) {
                throw new ApplicationError("this Client has no Email.Please add an email and try again", 400);
            }

            let employees;


            if (checkemployee.length > 0) {
                try {
                    employees = await updateEmployee(information, dbEmployee, setCond, setData);
                } catch (err) {
                    throw new ApplicationError(err.parent.sqlMessage, 400);
                }
                dbEmployee.sequelize.close();

                if (roleId != null || roleId != "") {
                    let setCondLogin = {
                        userId: employeeId
                    };
                    let login = await loginSignUpHelperDB.viewLogin(setCondLogin);
                    let password = await makeid();
                    let encryptPass = await hashPassword(password);

                    if (login.length == 0) { //checking if the email id doesn't exist in knack.logininfo
                        let setCondAddLogin = {
                            userId: employeeId,
                            businessId: businessIdinformation,
                            name: getteamInfo[0].employeeName,
                            emailId: getteamInfo[0].emailId,
                            contactNumber: getteamInfo[0].contactNumber,
                            password: encryptPass,
                            userType: roleId,
                            createdBy: userId,
                            FcmUserToken: '1234',
                            IMEINumber: '1234',
                            status: 1
                        };
                        await loginSignUpHelperDB.addLogin(setCondAddLogin);
                    } else {
                        let setDataUpdateLogin = {
                            businessId: businessIdinformation,
                            name: getteamInfo[0].employeeName,
                            emailId: getteamInfo[0].emailId,
                            contactNumber: getteamInfo[0].contactNumber,
                            userType: roleId,
                            password: encryptPass,
                            createdBy: userId
                        };
                        let setCondUpdateLogin = {
                            userId: employeeId
                        };
                        await loginSignUpHelperDB.updateLogin(setCondUpdateLogin, setDataUpdateLogin);
                    }
                    let text = "Password for Knack is " + password + ". Don't share it with anyone.";
                    let subject = 'Knack Login Details';
                    await textNotification(text, getteamInfo[0].contactNumber);
                    await mailNotification(subject, text, getteamInfo[0].emailId);
                }

            }

            let setCondViewLogin = {
                businessId: businessIdinformation,
                userType: 1
            };
            let loginInfo = await loginSignUpHelperDB.viewLogin(setCondViewLogin);
            if (loginInfo.length > 0) {
                //text and mail for employee
                let phno = getteamInfo[0].contactNumber;
                let text = "YOU HAVE BEEN SUCCESSFULLY ADDED TO KNACK";;
                let email_emp = getteamInfo[0].emailId;
                let subject_emp = "KNACK CONFIRMATION";
                let body_emp = "YOU HAVE BEEN SUCCESSFULLY ADDED TO KNACK";
                textNotification(text, phno);
                mailNotification(subject_emp, body_emp, email_emp);

                //text and mail for main admins
                loginInfo.forEach((user) => {
                    let phno2 = user.contactNumber;
                    let emp_name = getteamInfo[0].employeeName.toUpperCase();
                    let text2 = emp_name + " IS SUCCESSFULLY ADDED TO KNACK";
                    let email = user.emailId;
                    let subject = "KNACK CONFIRMATION";
                    let body = getteamInfo[0].employeeName + ' SUCCESSFULLY ADDED TO KNACK';
                    textNotification(text2, phno2);
                    mailNotification(subject, body, email);
                });
            }

            // show all data
            var pageNo = (params['page']) ? params['page'] : 1;
            var limitPerPage = PEGINATION_LIMIT;
            var offsetLimit = (pageNo - 1) * limitPerPage;
            let dbteam = createInstance(["clientEmployee"], information);
            let teamlist = await dbteam.employees.findAll({
                attributes: ['*', [dbteam.sequelize.literal('CASE WHEN photoUrl  is NULL THEN CONCAT("' + KNACK_TEAM_UPLOAD_URL.localUrl + '" , "noimage.jpg" ) ELSE CONCAT("' + KNACK_TEAM_UPLOAD_URL.localUrl + '" , photoUrl ) END'), 'photoUrl'],

                    [dbteam.sequelize.literal('CASE WHEN roleId IS NULL OR roleId = 0 or roleId = 1  THEN "Main Admin" ELSE "Center Admin" END'), 'roleName']
                ],
                order: [
                    ["updateDateTime", "desc"]
                ],
                where: {
                    businessId: businessIdinformation,
                    status: 1
                },
                subQuery: false,
                offset: offsetLimit,
                limit: limitPerPage,
                raw: true
            });
            dbteam.sequelize.close();

            return teamlist;
        } catch (error) {
            throw error;
        }
    };

    updateTeamRoleDb = async (
        information,
        userId,
        employeeId,
        roleId,
        params
    ) => {
        try {
            let dbEmployee = createInstance(["clientEmployee"], information);
            var businessIdinformation = information.split(",")[1];

            let setCond = {
                employeeId: employeeId
            };
            let setData = {
                roleId: roleId,
                updateDateTime: Date.now()
            };
            const Op = Sequelize.Op;
            let checkemployee = await dbEmployee.employees.findAll({
                where: {
                    status: 1,
                    employeeId: {
                        [Op.notIn]: [employeeId]
                    }
                },
                raw: true
            });


            let getteamInfo = await dbEmployee.employees.findAll({
                where: {
                    status: 1,
                    employeeId: employeeId
                },
                raw: true
            });
            //==============Check if the email is exist in knack.logininfo table=============
            let isEmailExist = await loginSignUpHelperDB.getEmployeeDetails({
                emailId: getteamInfo[0].emailId
            });
            console.log("isEmailExist : ", isEmailExist);
            if (isEmailExist > 0) {
                throw new ApplicationError("EmailID of this employee conflist with another emailId", 400);
                return;
            }

            if (!getteamInfo[0].emailId) {
                throw new ApplicationError("This Client has no Email.Please add an email and try again", 400);
            }

            let employees;


            if (checkemployee.length > 0) {
                try {
                    employees = await updateEmployee(information, dbEmployee, setCond, setData);
                } catch (err) {
                    throw new ApplicationError(err.parent.sqlMessage, 400);
                }
                dbEmployee.sequelize.close();

                if (roleId != null || roleId != "") {
                    let setCondLogin = {
                        userId: employeeId
                    };
                    let login = await loginSignUpHelperDB.viewLogin(setCondLogin);
                    let password = await makeid();
                    let encryptPass = await hashPassword(password);

                    if (login.length == 0) { //checking if the email id doesn't exist in knack.logininfo
                        let setCondAddLogin = {
                            userId: employeeId,
                            businessId: businessIdinformation,
                            name: getteamInfo[0].employeeName,
                            emailId: getteamInfo[0].emailId,
                            contactNumber: getteamInfo[0].contactNumber,
                            password: encryptPass,
                            userType: roleId,
                            createdBy: userId,
                            FcmUserToken: '1234',
                            IMEINumber: '1234',
                            status: 1
                        };
                        await loginSignUpHelperDB.addLogin(setCondAddLogin);
                    } else {
                        let setDataUpdateLogin = {
                            businessId: businessIdinformation,
                            name: getteamInfo[0].employeeName,
                            emailId: getteamInfo[0].emailId,
                            contactNumber: getteamInfo[0].contactNumber,
                            userType: roleId,
                            password: encryptPass,
                            createdBy: userId
                        };
                        let setCondUpdateLogin = {
                            userId: employeeId
                        };
                        await loginSignUpHelperDB.updateLogin(setCondUpdateLogin, setDataUpdateLogin);
                    }

                    let text = "Hi " + getteamInfo[0].employeeName + ", Congratulations! You have been successfully assigned a role,Password for Knack is " + password + ". Don't share it with anyone.";

                    let emailBody = "Hi " + getteamInfo[0].employeeName + ",<br><br>Congratulations! You have been successfully assigned a role.<br><br>To start using, please use the following credentials to login on Knack <br><br>Email ID : " + getteamInfo[0].emailId + " <br><br>Password : " + password + "<br><br>For any help or assistance, reach out to us anytime at support@justknackit.com";
                    let subject = 'Congratulations! You have been successfully assigned a role';
                    await textNotification(text, getteamInfo[0].contactNumber);
                    await mailNotification(subject, emailBody, getteamInfo[0].emailId);
                }

            }
            // show all data
            var pageNo = (params['page']) ? params['page'] : 1;
            var limitPerPage = PEGINATION_LIMIT;
            var offsetLimit = (pageNo - 1) * limitPerPage;
            let dbteam = createInstance(["clientEmployee"], information);
            let teamlist = await dbteam.employees.findAll({
                attributes: ['*', [dbteam.sequelize.literal('CASE WHEN photoUrl  is NULL THEN CONCAT("' + KNACK_TEAM_UPLOAD_URL.localUrl + '" , "noimage.jpg" ) ELSE CONCAT("' + KNACK_TEAM_UPLOAD_URL.localUrl + '" , photoUrl ) END'), 'photoUrl'],

                    [dbteam.sequelize.literal('CASE WHEN roleId IS NULL OR roleId = 0 or roleId = 1  THEN "Main Admin" ELSE "Center Admin" END'), 'roleName']
                ],
                order: [
                    ["updateDateTime", "desc"]
                ],
                where: {
                    businessId: businessIdinformation,
                    status: 1
                },
                subQuery: false,
                offset: offsetLimit,
                limit: limitPerPage,
                raw: true
            });
            dbteam.sequelize.close();

            return teamlist;
        } catch (error) {
            throw error;
        }
    };

    remove = async (information, employeeId) => {
        try {
            let db = createInstance(["clientEmployee"], information);
            let teamscheduledb = createInstance(["teamScheduleMap"], information);

            let employees, teamschedules;
            // check team member is assign into schedule
            teamschedules = await teamscheduledb.teamschedulemap.findAndCountAll({
                where: {
                    status: 1,
                    teamId: employeeId
                },
                raw: true
            });

            if (teamschedules.count > 0) {
                return ["Team Member is already assigned"];
            } else {
                employees = await db.employees.update({
                    status: 0,
                    updateDateTime: Date.now()
                }, {
                        where: {
                            employeeId: employeeId
                        }
                    });
            }

            teamscheduledb.sequelize.close();
            db.sequelize.close();
            return employees;
        } catch (error) {
            throw error;
        }
    };

    // Add Multiple Employee
    multipleEmployeeCreate__ = async (information, businessId, empData) => {
        try {
            let totalemployee = empData.length;
            let counter = 0;
            let dbEmployee = createInstance(["clientEmployee"], information);
            empData.forEach(function (emp, index, arr) {
                dbEmployee.employees.create({
                    businessId: businessId,
                    employeeName: emp.employeeName,
                    contactNumber: emp.contactNumber
                }).then(() => {
                    counter++;
                    if (totalemployee === counter) {
                        dbEmployee.sequelize.close();
                    }
                });
            })
        } catch (error) {
            throw error;
        }
    };

    multipleEmployeeCreate = async (information, businessId, empData) => {
        try {
            let totalemployee = empData.length;
            let counter = 0;

            let setCondViewLogin = {
                businessId: businessId,
                userType: 1
            };
            let loginInfo = await loginSignUpHelperDB.viewLogin(setCondViewLogin);

            let dbEmployee = createInstance(["clientEmployee"], information);
            empData.forEach(function (emp, index, arr) {
                dbEmployee.employees.create({
                    businessId: businessId,
                    employeeName: emp.employeeName,
                    contactNumber: emp.contactNumber
                }).then(() => {
                    counter++;

                    let text = "Hi " + emp.employeeName + ", you have been successfully added as a team member for " + loginInfo[0].name + "'s " + (loginInfo[0].businessName) ? loginInfo[0].businessName : 'business' + " on Knack. For any help or assistance, reach out to us anytime at +91 9820789803. ";
                    textNotification(text, emp.contactNumber);


                    if (totalemployee === counter) {

                        let text2 = "Hi " + loginInfo[0].name + ", " + counter + " employees has been successfully added as a team member on Knack. The team member can login using the credentials sent to his/her registered email ID. For any help or assistance, reach out to us anytime at +91 9820789803. ";
                        textNotification(text2, loginInfo[0].contactNumber);

                        dbEmployee.sequelize.close();

                    }
                });
            })
        } catch (error) {
            throw error;
        }
    };

}
export default new EmployeeDB();