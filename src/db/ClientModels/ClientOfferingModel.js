import { filterFields, createInstance, weeklyDates, getDayOfMonth } from "../../lib/common";
import { KNACKDB, KNACK_UPLOAD_URL } from "../../lib/constants.js";
import { ApplicationError } from "../../lib/errors";
const PUBLIC_FIELDS = [
    "serviceId",
    "businessId",
    "serviceName",
    "serviceDetails"
];
import Sequelize from "sequelize";
export class OfferingDB {
    services = [];

    create = async (information, businessId, serviceName, serviceDetails) => {
        let createdBy = information.split(',')[2];
        if (createdBy == null) {
            throw new ApplicationError("Please provide userId", 401);
        }
        const newOffering = {
            businessId,
            serviceName,
            serviceDetails,
            createdBy
        };
        try {
            let db = createInstance(["clientService"], information);
            let services = await db.services.create(newOffering);
            db.sequelize.close();
            return await filterFields(services, PUBLIC_FIELDS, true);
        } catch (error) {
            throw error;
        }
    };

    newclientlist = async (information, serviceId, scheduleId, subscriptionId) => {
        try {
            console.log(information);
            console.log(scheduleId);
            let dbSPM = createInstance(["servicePricepackMap"], information);
            let dbPP = createInstance(["clientPricepack"], information);
            let db = createInstance(["clientSubscription"], information);
            var dbClient = createInstance(["client"], information);

            let services = await dbSPM.servicepricepackmap.findAll({
                where: {
                    status: 1,
                    serviceId: serviceId
                },
                raw: true
            });
            dbSPM.sequelize.close();

            var info = [];
            for (var i = 0; i < services.length; i++) {
                let pricepack = await dbPP.pricepacks.findAll({
                    where: {
                        status: 1,
                        pricepackId: services[i].pricepackId
                    },
                    raw: true
                });

                for (var j = 0; j < pricepack.length; j++) {
                    let subscriptions = await db.subscriptions.findAll({
                        where: {
                            status: 1,
                            pricepacks: pricepack[j].pricepackId
                        },
                        raw: true
                    });

                    for (var k = 0; k < subscriptions.length; k++) {
                        var clientDetails = await dbClient.clients.findAll({
                            where: {
                                status: 1,
                                clientId: subscriptions[k].clientId
                            },
                            raw: true
                        });
                        //check the pricepack is expired or not
                        var isphoto = (clientDetails[0].photoUrl === null) ? 'noimage.jpg' : clientDetails[0].photoUrl;
                        var clientphoto = KNACK_UPLOAD_URL.localUrl + isphoto;
                        var today = new Date();
                        var expiryDate = new Date(subscriptions[k].subscriptionDateTime);
                        expiryDate.setDate(expiryDate.getDate() + parseInt(pricepack[j].pricepackValidity));

                        if (new Date(today) <= new Date(expiryDate)) {
                            var cl = [];
                            Array.prototype.push.apply(cl, [{ clientId: clientDetails[0].clientId }]);
                            Array.prototype.push.apply(cl, [{ clientName: clientDetails[0].clientName }]);
                            Array.prototype.push.apply(cl, [{ clientPhoto: clientphoto }]);
                            Array.prototype.push.apply(cl, [{ subscriptionId: subscriptions[k].subscriptionId }]);
                            Array.prototype.push.apply(cl, [{ pricepackId: pricepack[j].pricepackId }]);
                            Array.prototype.push.apply(cl, [{ pricepackName: pricepack[j].pricepackName }]);
                            var merged = [];

                            if (scheduleId != '' && subscriptionId != '') {

                                let db = createInstance(["clientScheduleMap"], information);
                                let clients = await db.clientschedulemap.findAll({
                                    where: {
                                        status: 1,
                                        scheduleId: scheduleId,
                                        clientId: clientDetails[0].clientId,
                                        subscriptionId: subscriptionId
                                    },
                                    raw: true
                                });

                                //console.log(clients);
                                if (clients.length) {

                                    if (subscriptions[k].subscriptionId == subscriptionId) {
                                        console.log(clients[0].subscriptionId + "---" + subscriptionId);
                                        Array.prototype.push.apply(cl, [{ ScheduleStatus: 1 }]);
                                    } else {
                                        Array.prototype.push.apply(cl, [{ ScheduleStatus: 0 }]);
                                    }
                                } else {
                                    Array.prototype.push.apply(cl, [{ ScheduleStatus: 0 }]);
                                }

                            }

                            merged = Object.assign.apply(Object, cl);
                            info = info.concat(merged);
                        }
                    }
                }
            }
            dbClient.sequelize.close();
            db.sequelize.close();
            dbPP.sequelize.close();

            //the code below is for showing the list alphabatically
            let alpha = [],
                c = 0;
            let alphabaticObj = [];
            var filterData = [];
            let infoObj = [];
            var cc = 0;
            for (let i = 65; i <= 90; i++) {
                var merged = {};
                alpha[c] = String.fromCharCode(i).toUpperCase();
                let newObj = [];
                for (let obj of info) {
                    let regEx = new RegExp(alpha[c], 'gi');
                    if (obj.clientName.substr(0, 1).match(regEx)) {
                        for (let m = 0; m <= info.length; m++) { //to show only count
                            obj['count'] = cc;
                        }
                        cc++;
                        newObj = newObj.concat(obj);
                    }
                }
                if (newObj.length > 0) {
                    merged = Object.assign.apply(Object, [{ title: alpha[c].toUpperCase(), data: newObj }]);
                    filterData = filterData.concat(merged);
                }
                c++;
            }

            return await filterData;
        } catch (error) {
            throw error;
        }
    };

    listAll = async (information, businessId, filterPrivateFields = true) => {
        try {
            let dbService = createInstance(["clientService"], information);
            let dbSPM = createInstance(["servicePricepackMap"], information);
            let dbPP = createInstance(["clientPricepack"], information);
            let db = createInstance(["clientSubscription"], information);
            var dbClient = createInstance(["client"], information);

            var sequelize = require("sequelize");
            var seqdb = new Sequelize(businessId, KNACKDB.user, KNACKDB.password, {
                host: KNACKDB.host,
                port: 3306,
                dialect: "mysql",
                pool: {
                    max: 100,
                    min: 0,
                    idle: 10000
                }
            });
            var getdata = await seqdb.query(
                "SELECT sub.clientId,COUNT(*),sub.pricepacks FROM subscriptions as sub inner join servicepricepackmap spp on sub.pricepacks = spp.pricepackId       inner join services s on spp.serviceId = s.serviceId group by sub.clientId,sub.pricepacks", {
                    type: Sequelize.QueryTypes.SELECT //this code will remove the duplicate data (meta data)
                }
            );
            // console.log(getdata);


            let getservices = await dbService.services.findAll({
                where: {
                    status: 1,
                    businessId: businessId
                },

                raw: true
            });
            var newcon = [];
            for (var i = 0; i < getservices.length; i++) {
                getservices[i].totalCount = 0;
                newcon = getservices;
            }

            let getpricepackrel = await dbSPM.servicepricepackmap.findAll({
                where: {
                    status: 1
                },
                raw: true
            });
            var con = [];
            let mk = [];
            let allcount = 0;
            for (var m = 0; m < getpricepackrel.length; m++) {

                var arrnew = [];
                let getclientscount = await seqdb.query(
                    "SELECT pricepacks,  count(*) AS `countClient` FROM `subscriptions` WHERE `pricepacks` = '" + getpricepackrel[m].pricepackId + "' group by pricepacks ", {
                        type: Sequelize.QueryTypes.SELECT
                    }
                );

                if (getclientscount.length > 0) {
                    Array.prototype.push.apply(arrnew, getclientscount);
                    Array.prototype.push.apply(arrnew, [{ count: allcount }]);
                    Array.prototype.push.apply(arrnew, [{ serviceId: getpricepackrel[m].serviceId }]);
                    Array.prototype.push.apply(arrnew, [{ pricepackId: getpricepackrel[m].pricepackId }]);
                    mk = Object.assign.apply(Object, arrnew);
                    con = con.concat(mk);
                } else { }
            }


            function compare_values(newcon, con) {
                for (var i = 0, len = newcon.length; i < len; i++) {
                    for (var j = 0, len2 = con.length; j < len2; j++) {
                        if (newcon[i].serviceId == con[j].serviceId) {
                            // console.log(con[j]['countClient']);
                            newcon[i].totalCount += parseInt(con[j]['countClient']);
                            con.splice(j, 1);
                            len2 = con.length;;
                        }
                    }
                }
            }

            compare_values(newcon, con);
            return newcon;

            dbSPM.sequelize.close();
            dbClient.sequelize.close();
            db.sequelize.close();
            dbPP.sequelize.close();
        } catch (error) {
            throw error;
        }
    };

    listAllcenter = async (information, centerId, filterPrivateFields = true) => {
        try {
            let db = createInstance(["clientService"], information);
            let services = await db.services.findAll({
                where: {
                    status: 1,
                    centerId: centerId
                },
                raw: true
            });
            db.sequelize.close();
            if (filterPrivateFields) {
                return await filterFields(services, PUBLIC_FIELDS);
            }
        } catch (error) {
            throw error;
        }
    };

    offeringDetails = async (
        information,
        serviceId,
        filterPrivateFields = true
    ) => {
        try {

            let clientList = [];
            let Count = 0;
            let db = createInstance(["clientService"], information);
            let dbPricePack = createInstance(["clientPricepack"], information);
            let dbSubscription = createInstance(["clientSubscription"], information);
            let dbClient = createInstance(["client"], information);
            let services = await db.services.findAll({
                where: {
                    status: 1,
                    serviceId: serviceId
                },
                raw: true
            });

            let dbSPM = createInstance(["servicePricepackMap"], information);

            let spm = await dbSPM.servicepricepackmap.findAll({
                where: {
                    status: 1,
                    serviceId: serviceId
                },
                raw: true
            });

            for (let i = 0; i < spm.length; i++) {
                let subscription_data = await dbSubscription.subscriptions.findAll({
                    where: {
                        status: 1,
                        pricepacks: spm[i].pricepackId
                    },
                    raw: true
                });

                for (let j = 0; j < subscription_data.length; j++) {
                    let Obj = {};
                    let active_clients = await dbClient.clients.findAll({
                        where: {
                            clientId: subscription_data[j].clientId,
                            status: 1
                        },
                        raw: true
                    });
                    Count++;

                    var isphoto = (active_clients[0].photoUrl === null) ? 'noimage.jpg' : active_clients[0].photoUrl;
                    var clientphoto = KNACK_UPLOAD_URL.localUrl + isphoto;

                    Obj["Name"] = active_clients[0].clientName;
                    Obj["Photo"] = clientphoto;
                    Obj["Phno"] = active_clients[0].contactNumber;


                    let pricepacks = await dbPricePack.pricepacks.find({
                        where: {
                            pricepackId: subscription_data[j].pricepacks,
                            status: 1,
                        },
                        raw: true
                    });

                    if (pricepacks.isExpired == 1) {
                        Obj["Status"] = "InActive";
                    } else {
                        Obj["Status"] = "Active";
                    }
                    clientList.push(Obj);

                }
            }

            db.sequelize.close();
            if (filterPrivateFields) {
                return await [{ services, clientList, Count }];
            }
        } catch (error) {
            throw error;
        }
    };

    update = async (
        information,
        serviceId,
        businessId,
        centerId,
        serviceName,
        serviceDetails
    ) => {
        try {
            let db = createInstance(["clientService"], information);
            let services = await db.services.update({
                centerId: centerId,
                businessId: businessId,
                serviceName: serviceName,
                serviceDetails: serviceDetails,
                updateDateTime: Date.now()
            }, {
                    where: {
                        serviceId: serviceId,
                        status: 1
                    }
                });
            db.sequelize.close();
            return services;
        } catch (error) {
            throw error;
        }
    };

    remove = async (information, serviceId) => {
        try {
            let dbClientSchedule = createInstance(["clientSchedule"], information);
            let checkSchedule = await dbClientSchedule.schedule.findAll({
                where: {
                    status: 1,
                    serviceId: serviceId,
                    status: 1
                },
                raw: true
            }); //code ends here
            var subCount = checkSchedule.length;
            if (subCount > 0) {
                throw new ApplicationError(
                    "Service is assigned to the schedule. Hence it can't be deleted.",
                    401
                );
            } else {
                let db = createInstance(["clientService"], information);
                let services = await db.services.update({
                    status: 0,
                    updateDateTime: Date.now()
                }, {
                        where: {
                            serviceId: serviceId
                        }
                    });
                db.sequelize.close();
                return services;
            }
        } catch (error) {
            throw error;
        }
    };

    clientList = async (
        information,
        serviceId,
        scheduleId,
        filterPrivateFields = true
    ) => {
        try {
            let dbSPM = createInstance(["servicePricepackMap"], information);
            let dbPP = createInstance(["clientPricepack"], information);
            let db = createInstance(["clientSubscription"], information);
            var dbClient = createInstance(["client"], information);

            let services = await dbSPM.servicepricepackmap.findAll({
                where: {
                    status: 1,
                    serviceId: serviceId
                },
                raw: true
            });
            dbSPM.sequelize.close();

            var info = [];
            for (var i = 0; i < services.length; i++) {
                let pricepack = await dbPP.pricepacks.findAll({
                    where: {
                        status: 1,
                        pricepackId: services[i].pricepackId
                    },
                    raw: true
                });

                for (var j = 0; j < pricepack.length; j++) {
                    let subscriptions = await db.subscriptions.findAll({
                        where: {
                            status: 1,
                            pricepacks: pricepack[j].pricepackId
                        },
                        raw: true
                    });

                    for (var k = 0; k < subscriptions.length; k++) {
                        var clientDetails = await dbClient.clients.findAll({
                            where: {
                                status: 1,
                                clientId: subscriptions[k].clientId
                            },
                            raw: true
                        });
                        //check the pricepack is expired or not
                        var isphoto = (clientDetails[0].photoUrl === null) ? 'noimage.jpg' : clientDetails[0].photoUrl;
                        var clientphoto = KNACK_UPLOAD_URL.localUrl + isphoto;
                        var today = new Date();
                        var expiryDate = new Date(subscriptions[k].subscriptionDateTime);
                        expiryDate.setDate(expiryDate.getDate() + parseInt(pricepack[j].pricepackValidity));

                        if (new Date(today) <= new Date(expiryDate)) {
                            var cl = [];
                            Array.prototype.push.apply(cl, [{ clientId: clientDetails[0].clientId }]);
                            Array.prototype.push.apply(cl, [{ clientName: clientDetails[0].clientName }]);
                            Array.prototype.push.apply(cl, [{ clientPhoto: clientphoto }]);
                            Array.prototype.push.apply(cl, [{ subscriptionId: subscriptions[k].subscriptionId }]);
                            Array.prototype.push.apply(cl, [{ pricepackId: pricepack[j].pricepackId }]);
                            Array.prototype.push.apply(cl, [{ pricepackName: pricepack[j].pricepackName }]);
                            var merged = [];
                            if (scheduleId[0] != '') {

                                let db = createInstance(["clientScheduleMap"], information);
                                let clients = await db.clientschedulemap.findAll({
                                    where: {
                                        status: 1,
                                        scheduleId: scheduleId[0],
                                        clientId: clientDetails[0].clientId,
                                        //subscriptionId:subscriptions[j].subscriptionId
                                    },
                                    raw: true
                                });
                                if (clients.length) {
                                    Array.prototype.push.apply(cl, [{ ScheduleStatus: 1 }]);
                                } else {
                                    Array.prototype.push.apply(cl, [{ ScheduleStatus: 0 }]);
                                }

                            }

                            merged = Object.assign.apply(Object, cl);
                            info = info.concat(merged);
                        }
                    }
                }
            }
            dbClient.sequelize.close();
            db.sequelize.close();
            dbPP.sequelize.close();

            let day_1 = new Date('2012-12-31');
            let day_2 = new Date('2021-08-31');
            let day = await getDayOfMonth(day_1, day_2);
            console.log(day);

            //the code below is for showing the list alphabatically
            let alpha = [],
                c = 0;
            let alphabaticObj = [];
            var filterData = [];
            let infoObj = [];
            var cc = 0;
            for (let i = 65; i <= 90; i++) {
                var merged = {};
                alpha[c] = String.fromCharCode(i).toUpperCase();
                let newObj = [];
                for (let obj of info) {
                    let regEx = new RegExp(alpha[c], 'gi');
                    if (obj.clientName.substr(0, 1).match(regEx)) {
                        for (let m = 0; m <= info.length; m++) { //to show only count
                            obj['count'] = cc;
                        }
                        cc++;
                        newObj = newObj.concat(obj);
                    }
                }
                if (newObj.length > 0) {
                    merged = Object.assign.apply(Object, [{ title: alpha[c].toUpperCase(), data: newObj }]);
                    filterData = filterData.concat(merged);
                }
                c++;
            }

            return await filterData;
        } catch (error) {
            throw error;
        }
    };

}
export default new OfferingDB();