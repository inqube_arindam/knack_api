import BaseModel from "./BaseModel";
import Sequelize from "sequelize";
import LeadsSchema from "../schemas/knackadmin.lead.schema.js";
import NotesSchema from "../schemas/knackadmin.note.schema.js";
import ActivitySchema from "../schemas/knackadmin.activity.schema.js";
import LoginSignupSchema from "../schemas/loginSignup.schema.js";
import { createInstance } from "../lib/common";
import { KNACKDB, KNACK_DBSETTINGS } from "../lib/constants.js";
import { ApplicationError } from "../lib/errors";

const PUBLIC_FIELDS = [
  "leadName",
  "leadEmailId",
  "contactNumber",
  "leadAddress",
  "pincode",
  "businessName",
  "businessExperience",
  "numberOfClients",
  "numberOfBranches",
  "numberOfTeamMembers",
  "alternateContactNumber",
  "alternateAddress",
  "businessWebsite",
  "leadType",
  "leadStatus",
  "assignedTo",
  "priority",
  "leadScore",
  "createDateTime",
  "updateDateTime",
  "isActive",
  "isDelete",
  "gender",
  "leadCategory",
  "centers",
  "managedBy",
  "referalDetails",
  "isServeyComplete"
];

const Op = Sequelize.Op;

const filterFields = (toFilter, allowedFields, showCreate = false) => {
  let data = [];
  if (showCreate) {
    toFilter = Array(toFilter);
  }
  toFilter.forEach(function(element) {
    data.push(
      allowedFields.reduce((memo, field) => {
        return {
          ...memo,
          [field]: element[field]
        };
      }, {})
    );
  });
  return data;
};

export class LeadsDB extends BaseModel {
  constructor(connection) {
    super("leadinfo", connection);
    this.schema = LeadsSchema.Leadinfo();
    this.name = "leadinfo";
    this.db = this.connection;
    this.model = this.connection.model(this.name, this.schema);
    //Creating notes model
    
    this.Notesschema  = NotesSchema.Notesinfo();
    this.Notesname    = "notesinfo";
    this.Notesmodel   = this.connection.model(this.Notesname,this.Notesschema);
    
    this.Activityschema       = ActivitySchema.Activityinfo();
    this.Activityname         = "activityinfo";
    this.Activitymodel        = this.connection.model(this.Activityname,this.ActivitySchema);

    //this.schema = LoginSignupSchema.LoginSignUp();
    //this.name = "logininfo";
    this.Loginschema          = LoginSignupSchema.LoginSignUp();
    this.Loginname            = "logininfo";
    this.Loginmodel           = this.connection.model(this.Loginname,this.Loginschema);
  }

  lead = [];

  //Return business's information based on passed businessId
  details = async (leadId, filterPrivateFields = true) => {
    try {
      let lead = await this.model.findAll({
        where: {
          leadId: leadId
        },
        raw: true
      });
      if (filterPrivateFields) {
        return await filterFields(business, PUBLIC_FIELDS);
      }
    } catch (error) {
      throw error;
    }
  };

  //list all leads
  listleads = async() => {
    var leads = {};
    try {     
      var leads = await this.model.findAll({
        attributes: ["leadId","leadName","leadEmailId","contactNumber","leadAddress","pincode","businessName","businessExperience","numberOfClients","numberOfBranches","numberOfTeamMembers","alternateContactNumber","alternateAddress","businessWebsite","leadType","leadStatus","assignedTo","priority","leadScore","createDateTime","updateDateTime","leadCategory","isServeyComplete"],  
        where: {
          isDelete:0
        },
        order:[['updateDateTime','DESC']],
        raw: true
      });
      if(leads){
        for (var i = 0, len = leads.length; i < len; i++) {
          var assignedToUser  = await this.Loginmodel.find({
            attributes: ["userId","name"],
            where: {
              userId: leads[i].assignedTo
            }
          });            

          if(assignedToUser){
            leads[i].assignedUserName = assignedToUser.name;
          }     
          
          var totActivity = await this.Activitymodel.findAndCountAll({
            where: {
              leadId: leads[i].leadId,
              isDelete:0
            }
          });            
          if(totActivity){
            leads[i].callActivity = totActivity.count;
          }
          
          var lastActivity = await this.Activitymodel.findOne({
            where: {
              leadId: leads[i].leadId,
              isDelete:0
            },
            order:[['activityStep','DESC']],
            raw: true
          });
          
          if(lastActivity) {
            console.log(lastActivity.activityFinalStatus);
            //leads[i].lastActivityName   = lastActivity.activityFinalStatus;
            leads[i].lastActivityName = lastActivity.activityFinalStatus==1 ? 'Survey' : (lastActivity.activityFinalStatus==2 ? 'Tele call' : (lastActivity.activityFinalStatus==3 ? 'Demo' : (lastActivity.activityFinalStatus==4 ? 'Agreed to join' : (lastActivity.activityFinalStatus==5 ? 'On free trial' : (lastActivity.activityFinalStatus==6 ? 'Set up completed' : (lastActivity.activityFinalStatus==7 ? 'Paid client' : '------'))))));
          } else {
            leads[i].lastActivityName = '--------';
          }        
        }
      }
    } catch (error) {
        throw error;
    }
    return await leads;
  };
  //list lead by filtering search
  searchKnackLead = async(searchUser=null,searchLocation=null,searchPriority=null,searchStatus=null,searchType=null,searchAssignedUser=null) => {
    var leads = {}; 
    try {
      var whereStatement = {};
      whereStatement.isDelete = 0;
      //console.log('here');
      if(searchUser!=null) {
        whereStatement.leadName   = {like: ["" + searchUser + "%"]};
      } 
      if(searchLocation!=null && searchLocation!='Location') {
        whereStatement.pincode   = searchLocation;
      }
      if(searchPriority!=null && searchPriority>0) {
        whereStatement.priority   = searchPriority;
      }
      /*if(searchStatus!=null) {

      }*/
      if(searchType!=null && searchType>0) {
        whereStatement.leadType   = searchType;
      }
      if(searchAssignedUser!=null && searchAssignedUser!=0) {
        whereStatement.assignedTo   = searchAssignedUser;
      }     
      let packages = await this.model.findAll({
        where: whereStatement,
        raw: true
      });            
      var leads = await this.model.findAll({
        where: whereStatement,
        order:[['updateDateTime','DESC']],
        raw: true
      });
      //console.log(whereStatement);
      if(leads){
        for (var i = 0, len = leads.length; i < len; i++) {
          var assignedToUser  = await this.Loginmodel.find({
            attributes: ["userId","name"],
            where: {
              userId: leads[i].assignedTo
            }
          });            

          if(assignedToUser){
            leads[i].assignedUserName = assignedToUser.name;
          }     
          
          var totActivity = await this.Activitymodel.findAndCountAll({
            where: {
              leadId: leads[i].leadId,
              isDelete:0
            }
          });            
          if(totActivity){
            leads[i].callActivity = totActivity.count;
          }
          
          var lastActivity = await this.Activitymodel.findOne({
            where: {
              leadId: leads[i].leadId,
              isDelete:0
            },
            order:[['activityStep','DESC']],
            raw: true
          });
          
          if(lastActivity) {
            console.log(lastActivity.activityFinalStatus);
            leads[i].activityFinalStatus   = lastActivity.activityFinalStatus;
            leads[i].lastActivityName = lastActivity.activityFinalStatus==1 ? 'Survey' : (lastActivity.activityFinalStatus==2 ? 'Tele call' : (lastActivity.activityFinalStatus==3 ? 'Demo' : (lastActivity.activityFinalStatus==4 ? 'Agreed to join' : (lastActivity.activityFinalStatus==5 ? 'On free trial' : (lastActivity.activityFinalStatus==6 ? 'Set up completed' : (lastActivity.activityFinalStatus==7 ? 'Paid client' : '------'))))));
          } else {
            leads[i].lastActivityName = '--------';
            leads[i].activityFinalStatus   = '0';
          }  
        }
        if(searchStatus!=null && searchStatus!='') {            
          //leads = leads.filter(function(value){ if(value.activityFinalStatus==searchStatus) { console.log(value.activityFinalStatus); } })
          leads = leads.filter(function(value){ return value.activityFinalStatus==searchStatus;})
        }
      }
      return await leads;
    } catch (error) {
      throw error;
    }   
  }
  /*
  userId,
      leadName,
      leadContactNumber,
      leadEmailId,
      leadAddress,
      leadPinNumber,
      leadBusinessName,
      leadBusinessExperience,
      leadBusinessBranches,
      leadBusinessNumOfClients,
      leadBusinessNumOfCenters,
      leadBusinessNumOfTeamMembers,
      leadBusinessAlternateNum,
      leadBusinessAlternateAddress,
      leadBusinessWebsite,
      leadType,
      leadStatus,
      leadAssignedTo,
      leadPriority,
      leadManager,
      leadReferalDetails
  */

  //Add New Leads
  addNewLead = async(
    userId,
    leadName,
    leadContactNumber,
    leadEmailId,
    leadAddress,
    leadPinNumber,
    leadBusinessName,
    leadBusinessExperience,
    leadBusinessBranches,
    leadBusinessNumOfClients,
    leadBusinessCenters,
    leadBusinessNumOfTeamMembers,
    leadBusinessAlternateNum,
    leadBusinessAlternateAddress,
    leadBusinessWebsite,
    leadType,
    leadStatus,
    leadAssignedTo,
    leadPriority,
    leadManager,
    leadReferalDetails
  ) => {
    let verifylead = await this.model.find({
      where: {
        leadEmailId: leadEmailId
      },
      raw: true
    });
    if(verifylead) {
      throw new ApplicationError("Email ID Already Exist", 401);
    } else {
      const leadinfodetails = {
        createdBy:userId,
        leadName:leadName,
        contactNumber:leadContactNumber,
        leadEmailId:leadEmailId,        
        leadAddress:leadAddress,
        pincode:leadPinNumber,
        businessName:leadBusinessName,
        businessExperience:leadBusinessExperience,
        numberOfBranches: leadBusinessBranches,
        numberOfClients: leadBusinessNumOfClients,
        numberOfTeamMembers: leadBusinessNumOfTeamMembers,
        alternateContactNumber: leadBusinessAlternateNum,
        alternateAddress: leadBusinessAlternateAddress,
        businessWebsite: leadBusinessWebsite,
        centers:leadBusinessCenters,
        leadType: leadType,
        leadStatus: leadStatus,
        assignedTo: leadAssignedTo,
        managedBy:leadManager,
        priority: leadPriority,
        referalDetails: leadReferalDetails,
        createDateTime: Sequelize.literal('CURRENT_TIMESTAMP'),
      };
      await this.model.create(leadinfodetails);   
      var leads = await this.model.findAll({
        attributes: ["leadId","leadName","leadEmailId","contactNumber","leadAddress","pincode","businessName","businessExperience","numberOfClients","numberOfBranches","numberOfTeamMembers","alternateContactNumber","alternateAddress","businessWebsite","leadType","leadStatus","assignedTo","priority","leadScore","createDateTime","updateDateTime","leadCategory","isServeyComplete"],  
        where: {
          isDelete:0
        },
        order:[['updateDateTime','DESC']],
        raw: true
      });
      if(leads){
        for (var i = 0, len = leads.length; i < len; i++) {
          var assignedToUser  = await this.Loginmodel.find({
            attributes: ["userId","name"],
            where: {
              userId: leads[i].assignedTo
            }
          });            

          if(assignedToUser){
            leads[i].assignedUserName = assignedToUser.name;
          }     
          
          var totActivity = await this.Activitymodel.findAndCountAll({
            where: {
              leadId: leads[i].leadId,
              isDelete:0
            }
          });            
          if(totActivity){
            leads[i].callActivity = totActivity.count;
          }
          
          var lastActivity = await this.Activitymodel.findOne({
            where: {
              leadId: leads[i].leadId,
              isDelete:0
            },
            order:[['activityStep','DESC']],
            raw: true
          });
          
          if(lastActivity) {
            console.log(lastActivity.activityFinalStatus);
            //leads[i].lastActivityName   = lastActivity.activityFinalStatus;
            leads[i].lastActivityName = lastActivity.activityFinalStatus==1 ? 'Survey' : (lastActivity.activityFinalStatus==2 ? 'Tele call' : (lastActivity.activityFinalStatus==3 ? 'Demo' : (lastActivity.activityFinalStatus==4 ? 'Agreed to join' : (lastActivity.activityFinalStatus==5 ? 'On free trial' : (lastActivity.activityFinalStatus==6 ? 'Set up completed' : (lastActivity.activityFinalStatus==7 ? 'Paid client' : '------'))))));
          } else {
            leads[i].lastActivityName = '--------';
          }        
        }
      }
      return await leads;
      }
    //if()
  }
  //Update specific business's information
  updateLead = async (
    leadId,
    leadName,
    leadEmailId,
    leadContactNumber,
    leadAddress,
    leadPinNumber,
    leadBusinessName,
    leadBusinessExperience,
    leadBusinessBranches,
    leadBusinessNumOfClients,
    leadBusinessCenters,
    leadBusinessNumOfTeamMembers,
    leadBusinessAlternateNum,
    leadBusinessAlternateAddress,
    leadBusinessWebsite,
    leadType,
    leadStatus,
    leadAssignedTo,
    leadPriority,
    leadManager,
    leadReferalDetails
  ) => {
    try {
      let verifylead = await this.model.find({
        where: {
          leadId: {
            [Op.ne]: leadId
          },
          leadEmailId: leadEmailId          
        },
        raw: true
      });
      if(verifylead) {
        //console.log(leadId);console.log(leadEmailId);
        throw new ApplicationError("Email ID Already Exist", 401);
      } else {
        try {
          let leads = await this.model.update(
            {
              leadName:leadName,
              leadEmailId:leadEmailId,
              contactNumber:leadContactNumber,
              leadAddress:leadAddress,
              pincode:leadPinNumber,
              businessName:leadBusinessName,
              businessExperience:leadBusinessExperience,
              numberOfClients:leadBusinessNumOfClients,
              numberOfBranches:leadBusinessBranches,
              numberOfTeamMembers:leadBusinessNumOfTeamMembers,
              alternateContactNumber:leadBusinessAlternateNum,
              alternateAddress:leadBusinessAlternateAddress,
              businessWebsite:leadBusinessWebsite,
              centers:leadBusinessCenters,
              leadType:leadType,
              leadStatus:leadStatus,
              assignedTo:leadAssignedTo,
              priority:leadPriority,
              managedBy:leadManager,
              referalDetails:leadReferalDetails,
              centers:leadBusinessCenters,
              updateDateTime: Date.now()
            },
            {
              where: {
                leadId: leadId
              }
            }
          );
          return leads;
        } catch (error) {
          throw error;
        }
        throw new ApplicationError("Update", 401);
      }
    } catch (error) {
      throw error;
    }
  };

  removeLeads = async(leadId) => {    
     try {   
      let notes = await this.Notesmodel.update(
        {
          isDelete:1
        },
        {
          where: {
            leadId: leadId
          }
        }
      );
      let activity = await this.Activitymodel.update(
        {
          isDelete:1
        },
        {
          where: {
            leadId: leadId
          }
        }
      );  
      let leads = await this.model.update(
        {
          isDelete:1
        },
        {
          where: {
            leadId: leadId
          }
        }
      );      
      var leads = {};
      try {     
        var leads = await this.model.findAll({
          attributes: ["leadId","leadName","leadEmailId","contactNumber","leadAddress","pincode","businessName","businessExperience","numberOfClients","numberOfBranches","numberOfTeamMembers","alternateContactNumber","alternateAddress","businessWebsite","leadType","leadStatus","assignedTo","priority","leadScore","createDateTime","updateDateTime","leadCategory","isServeyComplete"],  
          where: {
            isDelete:0
          },
          order:[['updateDateTime','DESC']],
          raw: true
        });
        if(leads){
          for (var i = 0, len = leads.length; i < len; i++) {
            var assignedToUser  = await this.Loginmodel.find({
              attributes: ["userId","name"],
              where: {
                userId: leads[i].assignedTo
              }
            });            
  
            if(assignedToUser){
              leads[i].assignedUserName = assignedToUser.name;
            }     
            
            var totActivity = await this.Activitymodel.findAndCountAll({
              where: {
                leadId: leads[i].leadId,
                isDelete:0
              }
            });            
            if(totActivity){
              leads[i].callActivity = totActivity.count;
            }
            
            var lastActivity = await this.Activitymodel.findOne({
              where: {
                leadId: leads[i].leadId,
                isDelete:0
              },
              order:[['activityStep','DESC']],
              raw: true
            });
            
            if(lastActivity) {
              console.log(lastActivity.activityFinalStatus);
              //leads[i].lastActivityName   = lastActivity.activityFinalStatus;
              leads[i].lastActivityName = lastActivity.activityFinalStatus==1 ? 'Survey' : (lastActivity.activityFinalStatus==2 ? 'Tele call' : (lastActivity.activityFinalStatus==3 ? 'Demo' : (lastActivity.activityFinalStatus==4 ? 'Agreed to join' : (lastActivity.activityFinalStatus==5 ? 'On free trial' : (lastActivity.activityFinalStatus==6 ? 'Set up completed' : (lastActivity.activityFinalStatus==7 ? 'Paid client' : '------'))))));
            } else {
              leads[i].lastActivityName = '--------';
            }        
          }
        }
      } catch (error) {
          throw error;
      }
      return await leads;
    } catch (error) {
      throw error;
    }
  };
  //view single leads all data
  viewLead =  async(leadId, filterPrivateFields = true) => {

    // let leadDetails = await this.model.findAll({
    //   where: {
    //     leadId: leadId
    //   }
    // });

    // if(leadDetails){
    //   leadDetails[0].noteList = await this.Notesmodel.findAll({
    //     where: {
    //       leadId: leadId
    //     }
    //   });
    // }

    // leadDetails.noteList = [];

  

    // return leadDetails;

    // this.model.hasMany(this.Notesmodel, {
    //      foreignKey: "leadId"
    //   });
    //this.Notesmodel.hasMany(this.model,{ foreignKey: 'id'});
    var leads = await this.model.find({
      //attributes: ['leadId', 'leadName', 'leadEmailId', 'contactNumber',,'leadAddress','pincode','createDateTime','updateDateTime','businessName','priority'],  
      order:[['createDateTime','DESC']],
      where: {
        leadId:leadId,
        isDelete:0
      },
      raw: true
    });
    if(leads) {
      var getAssigedByName  = await this.Loginmodel.find({
        where: {
          userId:leads.assignedTo
        }
      });

      if(getAssigedByName){
        leads.assignedToName  = getAssigedByName.name;
      }

      var getManageByName     = await this.Loginmodel.find({
        where: {
          userId:leads.managedBy
        }
      });
      if(getManageByName){
        leads.managedByName  = getManageByName.name;
      }
      //console.log(leads.leadId);
      var notes   = await this.Notesmodel.findAll({
        where: {
          leadId: leads.leadId,
          isDelete:0
        },
        raw: true
      });
      if(notes) {
        for (var i = 0; i < notes.length; i++) {
          //console.log(notes[i].created_by);
          var notesAddedBy = await this.Loginmodel.find({
            where: {
              userId: notes[i].created_by
            }
          });//console.log(activities.length);

          //console.log(notesAddedBy);
          if(notesAddedBy) {
            //console.log(notesAddedBy.name);
            notes[i].created_by_name  = notesAddedBy.name;
            //console.log('here '+notes[i].created_by_name);
          }
          //console.log(notes[i].noteinfo);
        }
        //console.log(notes);        
      }
      leads.notesarr  = notes;
      var activities  = await this.Activitymodel.findAll({
        attributes: ['activityId', 'leadId', 'activityTypeLead', 'activityStatus','activityReason','otherReason','activityComments','activityDate','activityTime','activityFinalStatus','activityStep','activityAssignedTo','isCompleted','activityCreatedDateTime','activityModifiedDateTime','activityLastDateTime'],  
        order:[['activityStep','DESC']],
        where: {
         leadId: leads.leadId,
         isDelete:0
        },
        raw: true        
      });  
      //console.log(activities.length);
      leads.totalCallMeeting  = activities.length;
      if(activities.length>0) {
        //console.log('Activity');
        for (var i = 0; i < activities.length; i++) {
          var assignedToName = await this.Loginmodel.find({
          attributes: ['name'],
          where: {
            userId: activities[i].activityAssignedTo
          }
          });
          if(assignedToName){
            activities[i].assignedToName = assignedToName.name;
          }
        }
      }
      leads.activityarr  = activities;
    }
    return await leads;
    //console.log(leads.notesarr);
  }

  //get servey data
  getLeadData = async(leadId) => {
    try {
      let serveyResultSave = await this.model.findOne({
        attributes: ["leadName","contactNumber"]
      },
      {
        where: {
          leadId:leadId
        }
      }
    );
      return await serveyResultSave;
    } catch (error) {
      throw error;
    }
  };
  //save servey answer 
  serveyResult = async(leadId, profession, gender, ans1, ans2, ans3, ans4, ans5, ans6, ans7, ans8, ans9, ans10, ans11, ans12, ans13, ans14, ans15, ans16, filterPrivateFields = true) => {
    console.log(leadId+' '+gender);
    var totalServeyPoint  = parseFloat(ans1)+parseFloat(ans2)+parseFloat(ans3)+parseFloat(ans4)+parseFloat(ans5)+parseFloat(ans6)+parseFloat(ans7)+parseFloat(ans8)+parseFloat(ans9)+parseFloat(ans10)+parseFloat(ans11)+parseFloat(ans12)+parseFloat(ans13)+parseFloat(ans14)+parseFloat(ans15)+parseFloat(ans16);
    console.log(totalServeyPoint);
    try {
      let serveyResultSave = await this.model.update(
        {
          leadCategory: profession,
          gender:gender,
          leadScore: totalServeyPoint,
          isServeyComplete: 1,
          updateDateTime:Date.now()
        },
        {
          where: {
            leadId: leadId
          }
        }
      );
      return await serveyResultSave;
    } catch (error) {
      throw error;
    }
  };
}
export default new LeadsDB();
