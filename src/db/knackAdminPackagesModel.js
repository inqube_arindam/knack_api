import BaseModel from "./BaseModel";
import Sequelize from "sequelize";
import PackagesSchema from "../schemas/knackadmin.packages.schema.js";
import { ApplicationError } from "../lib/errors";
const PUBLIC_FIELDS = [
  "packageId",
  "name",
  "details",
  "duration",
  "amount",
  "referralDiscount",
  "isGstApplicable",
  "cgst",
  "sgst",
  "totalAmount",
  "numberOfCenters",
  "numberOfTeamMembers",
  "status",
  "isDelete"
];
const filterFields = (toFilter, allowedFields, showCreate = false) => {
  let data = [];
  if (showCreate) {
    toFilter = Array(toFilter);
  }
  toFilter.forEach(function(element) {
    data.push(
      allowedFields.reduce((memo, field) => {
        return {
          ...memo,
          [field]: element[field]
        };
      }, {})
    );
  });
  return data;
};
export class PackagesDB extends BaseModel {
  constructor(connection) {
    super("packages", connection);
    this.schema = PackagesSchema.Packages();
    this.name = "packages";
    this.db = this.connection;
    this.model = this.connection.model(this.name, this.schema);
  }
  packages = [];

  create = async (name, details, duration, amount, referralDiscount, isGstApplicable, cgst, sgst, numberOfCenters, numberOfTeamMembers, filterPrivateFields = true) => {
   var totalPayableAmount  = 0;
   //console.log(isGstApplicable);
    if(isGstApplicable>0){
      totalPayableAmount  = (amount-referralDiscount)+(amount*cgst)/100+(amount*sgst)/100;      
    } else {
      totalPayableAmount  = (amount-referralDiscount);
    }
    
    const packagedetail = {
      name,
      details,
      duration,
      amount,
      referralDiscount,
      isGstApplicable,
      cgst,
      sgst,
      totalAmount: totalPayableAmount,
      numberOfCenters,
      numberOfTeamMembers
    };
    //console.log(isGstApplicable);
    if(parseInt(duration)> 15){
      try {
        let packages = await this.model.create(packagedetail);
        let allpackages = await this.model.findAll({
          where: {
            isDelete: 0
          },
          raw: true
        });
        if (filterPrivateFields) {
          return await filterFields(allpackages, PUBLIC_FIELDS);
        }
        //return await filterFields(packages, PUBLIC_FIELDS, true);
      } catch (error) {
        throw error;
      }
    } else {
      throw new ApplicationError(
        "Duration should be more than 15 days.",
        401
      );
    }
  };
  listAll = async (packageId, filterPrivateFields = true) => {
    try {
      let packages = await this.model.findAll({
        where: {
          isDelete: 0
        },
        raw: true
      });
      if (filterPrivateFields) {
        return await filterFields(packages, PUBLIC_FIELDS);
      }
    } catch (error) {
      throw error;
    }
  };
  packageDetail = async (packageId, filterPrivateFields = true) => {
    try {
      let packages = await this.model.findAll({
        where: {
          status: 1,
          packageId: packageId
        },
        raw: true
      });
      if (filterPrivateFields) {
        return await filterFields(packages, PUBLIC_FIELDS);
      }
    } catch (error) {
      throw error;
    }
  };

  searchPackages = async (packageName=null, centerNumber=null, filterPrivateFields = true) => {
    try {
      var whereStatement = {};
      //whereStatement.status = 1;
      whereStatement.isDelete = 0;
      whereStatement.status = 1;
      //console.log(packageName);
      if(packageName!=null) {
        whereStatement.name   = {like: ["" + packageName + "%"]};
        console.log(whereStatement);
      } 
      
      if(centerNumber!=null && centerNumber!="Number of Centers") {
        whereStatement.numberOfCenters   = centerNumber;
      }
      
      let packages = await this.model.findAll({
        where: whereStatement,
        raw: true
      });
      if (filterPrivateFields) {
        return await filterFields(packages, PUBLIC_FIELDS);
      }
    } catch (error) {
      throw error;
    }
  };
  updatePackageStatus = async (
    status,
    packageId,
    filterPrivateFields = true
  ) => {
    try {
      let packages = await this.model.update(
        {
          status: status==1 ? 0 : 1,
          updateDateTime: Date.now()
        },
        {
          where: {
            packageId: packageId,
            isDelete:0
          }
        }
      );
      let allpackages = await this.model.findAll({
        where: {
          isDelete: 0
        },
        raw: true
      });
      if (filterPrivateFields) {
        return await filterFields(allpackages, PUBLIC_FIELDS);
      }
    } catch (error) {
      throw error;
    }
  };

  update = async (
    packageId,
    name,
    details,
    duration,
    amount,
    numberOfCenters
  ) => {
    try {
      let packages = await this.model.update(
        {
          name: name,
          details: details,
          duration: duration,
          amount: amount,
          numberOfCenters: numberOfCenters,
          updateDateTime: Date.now()
        },
        {
          where: {
            packageId: packageId
          }
        }
      );
      return packages;
    } catch (error) {
      throw error;
    }
  };

  remove = async packageId => {
    try {
      let packages = await this.model.update(
        {
          status: 0,
          updateDateTime: Date.now()
        },
        {
          where: {
            packageId: packageId
          }
        }
      );
      return packages;
    } catch (error) {
      throw error;
    }
  };

  removePackage = async (packageId, filterPrivateFields = true) => {
    console.log(packageId);
    try {
      let packages = await this.model.update(
        {
          isDelete: 1,
          status: 0,
          updateDateTime: Date.now()
        },
        {
          where: {
            packageId: packageId
          }
        }
      );
      let allpackages = await this.model.findAll({
        where: {
          isDelete: 0
        },
        raw: true
      });
      if (filterPrivateFields) {
        return await filterFields(allpackages, PUBLIC_FIELDS);
      }
      //return packages;
    } catch (error) {
      throw error;
    }
  };
  
}
export default new PackagesDB();
