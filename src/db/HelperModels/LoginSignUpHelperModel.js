import BaseModel from "../BaseModel";
import Sequelize from "sequelize";
import LoginSignupSchema from "../../schemas/loginSignup.schema.js";

const PUBLIC_FIELDS = [
    "userId",
    "name",
    "businessId",
    "emailId",
    "contactNumber",
    "userType",
    "modulePermissions",
    "lastLogin",
    "relatedDatabase",
    "relatedIP",
    "IMEINumber",
    "FcmUserToken",
    "status",
    "token"
];

const filterFields = (toFilter, allowedFields, showCreate = false) => {
    let data = [];
    if (showCreate) {
        toFilter = Array(toFilter);
    }
    toFilter.forEach(function (element) {
        data.push(
            allowedFields.reduce((memo, field) => {
                return {
                    ...memo,
                    [field]: element[field]
                };
            }, {})
        );
    });
    return data;
};

export class loginSignUpHelperDB extends BaseModel {
    constructor(connection) {
        super("logininfo", connection);
        this.schema = LoginSignupSchema.LoginSignUp();
        this.name = "logininfo";
        this.db = this.connection;
        this.model = this.connection.model(this.name, this.schema);
    }

    addLogin = async (getCond) => {
        let setCond = getCond;
        try {
            let loginInfo = await this.model
                .create(setCond)
                .catch(this.db.ValidationError, function (error) {
                    throw error["errors"];
                });
            return await loginInfo;
        } catch (error) {
            throw error;
        }
    }

    viewLogin = async (getCond) => {
        let setCond = getCond;
        try {
            let loginInfo = await this.model.findAll({
                where: setCond,
                raw: true
            });
            return await loginInfo;
        } catch (error) {
            throw error;
        }
    }

    updateLogin = async (getCond, getData) => {
        let setCond = getCond;
        let setData = getData;
        try {
            let loginInfo = await this.model.update(setData, {
                where: setCond
            });
            return await loginInfo;
        } catch (error) {
            throw error;
        }
    }

    deleteLogin = async (getCond) => {
        let setCond = getCond;
        try {
            let loginInfo = await this.model.update(
                {
                    status: 0,
                    updateDateTime: Date.now()
                }, {
                    where: setCond
                });
            return await loginInfo;
        } catch (error) {
            throw error;
        }
    }
    getEmployeeDetails = async (cond) => {
        try {
            let userDetails = await this.model.findAndCount(
                {
                    where: cond
                });
            return await userDetails.count;
        } catch (error) {
            throw error;
        }
    }
    
    getBusinessDetails = async(businessId) => {
        try {
            let getData = this.model.findAll({
                where: {
                    status: 1,
                    businessId: businessId
                },
                raw: true
            });
            return getData;
        } catch (error) {
            throw new ApplicationError(
                "Please provide businessId",
                409
            )
        }
    }
}
export default new loginSignUpHelperDB();