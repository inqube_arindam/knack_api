import BaseModel from "../BaseModel";
import Sequelize from "sequelize";
import PaymentSchema from "../../schemas/payment.schema.js";
const PUBLIC_FIELDS = [
    "paymentId",
    "packageId",
    "paymentMode",
    "userId",
    "businessId",
    "details"
];

const filterFields = (toFilter, allowedFields, showCreate = false) => {
    let data = [];
    if (showCreate) {
        toFilter = Array(toFilter);
    }
    toFilter.forEach(function (element) {
        data.push(
            allowedFields.reduce((memo, field) => {
                return {
                    ...memo,
                    [field]: element[field]
                };
            }, {})
        );
    });
    return data;
};


export class PaymentHelperDB extends BaseModel {
    constructor(connection) {
        super("payment", connection);
        this.schema = PaymentSchema.Payment();
        this.name = "payment";
        this.db = this.connection;
        this.model = this.connection.model(this.name, this.schema);
    }

    addPayment = async (getCond) => {
        let setCond = getCond;
        try {
            let payments = await this.model.create(setCond);
            return await payments;
        } catch (error) {
            throw error;
        }
    };

    viewPayment = async (getCond) => {
        let setCond = getCond;
        try {
            let payments = await this.model.findAll({
                where: setCond,
                raw: true
            });
            return await payments;
        } catch (error) {
            throw error;
        }
    };

    updatePayment = async (getCond, getData) => {
        let setCond = getCond;
        let setData = getData;
        try {
            let payments = await this.model.update(setData, {
                where: setCond
            });
            return await payments;
        } catch (error) {
            throw error;
        }
    };

    deletePayment = async (getCond) => {
        let setCond = getCond;
        try {
            let payments = await this.model.update(
                {
                    status: 0,
                    updateDateTime: Date.now()
                }, {
                    where: setCond
                });
            return await payments;
        } catch (error) {
            throw error;
        }
    };
}
export default new PaymentHelperDB();
