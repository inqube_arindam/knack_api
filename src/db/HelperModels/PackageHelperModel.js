import BaseModel from "../BaseModel";
import Sequelize from "sequelize";
import PackagesSchema from "../../schemas/packages.schema.js";
const PUBLIC_FIELDS = [
    "packageId",
    "name",
    "details",
    "duration",
    "amount",
    "numberOfCenters",
    "numberOfUser"
];
const filterFields = (toFilter, allowedFields, showCreate = false) => {
    let data = [];
    if (showCreate) {
        toFilter = Array(toFilter);
    }
    toFilter.forEach(function (element) {
        data.push(
            allowedFields.reduce((memo, field) => {
                return {
                    ...memo,
                    [field]: element[field]
                };
            }, {})
        );
    });
    return data;
};

export class PackageHelperDB extends BaseModel {
    constructor(connection) {
        super("packages", connection);
        this.schema = PackagesSchema.Packages();
        this.name = "packages";
        this.db = this.connection;
        this.model = this.connection.model(this.name, this.schema);
    }

    packages = [];
    addPackage = async (getCond) => {
        let setCond = getCond;
        try {
            let packages = await this.model.create(setCond);
            return await packages;
        } catch (error) {
            throw error;
        }
    };

    viewPackage = async (getCond) => {
        let setCond = getCond;
        try {
            let packages = await this.model.findAll({
                where: setCond,
                raw: true
            });
            return await packages;
        } catch (error) {
            throw error;
        }
    };

    updatePackage = async (getCond, getData) => {
        let setCond = getCond;
        let setData = getData;
        try {
            let packages = await this.model.update(setData, {
                where: setCond
            });
            return await packages;
        } catch (error) {
            throw error;
        }
    };

    deletePackage = async (getCond) => {
        let setCond = getCond;
        try {
            let packages = await this.model.update(
                {
                    status: 0,
                    updateDateTime: Date.now()
                }, {
                    where: setCond
                });
            return await packages;
        } catch (error) {
            throw error;
        }
    };
}
export default new PackageHelperDB();
