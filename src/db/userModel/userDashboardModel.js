import BaseModel from "./../BaseModel";
import Sequelize from "sequelize";
import BusinessSchema from "../../schemas/business.schema.js";
import {
    KNACKDB,
    KNACK_DBSETTINGS,
    TRIAL_PERIOD,
    HELPLINE,
    SUPPORT_EMAIL,
    KNACK_UPLOAD_URL
} from "../../lib/constants.js";
import {
    filterFields,
    createInstance,
    bookingInfo,
    subscriptionInfo,
    attendanceInfo,
    paymentInfo,
    scheduleInfo,
    attendanceInfoCalculate,
    bookingCountInfo,
    RemainingClientsOfService,
    hyphenDateFormat
} from "../../lib/common";
import {
    ApplicationError
} from "../../lib/errors";
import ScheduleDB from "../ClientModels/clientScheduleModel.js";
import loginSignUpHelperDB from "../HelperModels/LoginSignUpHelperModel";

export class UserDB extends BaseModel {
    constructor(connection) {
        super("businessinfo", connection);
        this.schema = BusinessSchema.Business();
        this.name = "businessinfo";
        this.db = this.connection;
        this.model = this.connection.model(this.name, this.schema);
    }

    business = [];

    userDashboard = async(information, businessId, filterPrivateFields = true) => {
        try {
            var date = new Date(); //specific date
            // var date = new Date(); //today
            var setObj = {
                businessId: businessId,
                status: 1
            };
            var getInfo = await bookingCountInfo(information, setObj, date);
            var todaysBooking = [{
                todaysBooking: getInfo
            }];
            var businessIdinformation = information.split(",")[1];
            let dbpaymentTransactions = createInstance(["paymentTransactions"], information);
            let dbclientpayments = createInstance(["clientPayments"], information);
            let dbClients = createInstance(["client"], information);
            let dbsubscriptions = createInstance(["clientSubscription"], information);
            let dbpricepack = createInstance(["clientPricepack"], information);
            let dbservicepricepackmap = createInstance(["servicePricepackMap"], information);
            let dbservices = createInstance(["clientService"], information);
            let dbclientScheduleMap = createInstance(["clientScheduleMap"], information);
            let todayDate = await hyphenDateFormat(new Date);
            dbclientpayments.clientpayments.belongsTo(dbsubscriptions.subscriptions, {
                foreignKey: "subscriptionsId",
                targetKey: "subscriptionId"
            });
            dbsubscriptions.subscriptions.belongsTo(dbClients.clients, {
                foreignKey: "clientId",
                targetKey: "clientId"
            });
            dbsubscriptions.subscriptions.belongsTo(dbpricepack.pricepacks, {
                foreignKey: "pricepacks",
                targetKey: "pricepackId"
            });
            dbclientpayments.clientpayments.belongsTo(dbpaymentTransactions.paymentTransactions, {
                foreignKey: "clientpaymentId",
                targetKey: "clientpaymentId"
            });
            dbservicepricepackmap.servicepricepackmap.belongsTo(dbservices.services, {
                foreignKey: "serviceId",
                targetKey: "serviceId"
            });
            dbsubscriptions.subscriptions.belongsTo(dbclientScheduleMap.clientschedulemap, {
                foreignKey: "subscriptionId",
                targetKey: "subscriptionId"
            });
            let itemConditionsclientpayments = {
                status: 1,
                // paymentStatus: 1,
                is_last_payment: 1,
                businessId: businessIdinformation,
                // [dbClients.sequelize.Op.and]: [Sequelize.literal("( ( `subscription->clientschedulemap`.`clientScheduleMapId` IS NOT NULL OR `subscription->clientschedulemap`.`sessionCount` IS NOT NULL ) OR ( `subscription->clientschedulemap`.`sessionCount` = `subscription->pricepack`.`serviceDuration` ) )")],
                // [dbClients.sequelize.Op.and]: [Sequelize.literal("(`subscription->clientschedulemap`.`sessionCount` = `subscription->pricepack`.`serviceDuration` )")],
                [dbClients.sequelize.Op.and]: [Sequelize.literal("(DATE( Date_add( `subscription`.`subscriptiondatetime`, INTERVAL `subscription->pricepack`.`pricepackvalidity` day )) < '" + todayDate + "') OR (`subscription->clientschedulemap`.`sessionCount` = `subscription->pricepack`.`serviceDuration` )")],
            };
            let itemGroupConditions = {
                status: 1,
                businessId: businessIdinformation,
            };
            let itemGroupConditionsPricePack = {};
            let itemConditions = {
                renewl_status: 1
            };
            var getSubInfo = await dbclientpayments.clientpayments.findAndCount({
                attributes: [
                    "clientpaymentId",
                    "subscriptionsId",
                    "due_amount",
                    "payble_amount",
                    "payment_due_date",
                    "payment_end_date", ["UpdateDateTime", "updated_at"],
                    [
                        dbClients.clients.sequelize.literal(
                            'DATE(Date_add( `subscription`.`subscriptiondatetime`,INTERVAL `subscription->pricepack`.`pricepackvalidity` day))'
                        ),
                        "subcription_end_Date"
                    ],
                    [
                        dbclientpayments.sequelize.literal("( SELECT IFNULL( SUM(`payble_amount`), 0 ) FROM `clientpayments` INNER join `paymentTransactions` AS `paymentTransactions` ON `clientpayments`.`clientpaymentId` = `paymentTransactions`.`clientpaymentId` WHERE `clientpayments`.`subscriptionsId` = `subscription`.`subscriptionId` )"),
                        "total_subcription_payemt"
                    ]
                ],
                include: [{
                    model: dbsubscriptions.subscriptions,
                    attributes: ["clientId", "status_amountPaidSoFar", "amountPaidSoFar"],
                    include: [{
                            model: dbClients.clients,
                            attributes: {
                                include: [
                                    "clientName", [
                                        dbClients.clients.sequelize.literal(
                                            'CASE WHEN photoUrl  is NULL THEN CONCAT("' +
                                            KNACK_UPLOAD_URL.localUrl +
                                            '" , "noimage.jpg" ) ELSE CONCAT("' +
                                            KNACK_UPLOAD_URL.localUrl +
                                            '" , photoUrl ) END'
                                        ),
                                        "photoUrl"
                                    ],
                                ],
                                exclude: []
                            },
                            where: itemGroupConditions
                        },
                        {
                            model: dbpricepack.pricepacks,
                            attributes: ["pricepackName", "amount", "serviceDuration"],
                            where: itemGroupConditionsPricePack,
                        },
                        {
                            model: dbclientScheduleMap.clientschedulemap,
                            attributes: ["scheduleId", "sessionCount"],
                            require: false,
                            where: {
                                // [dbclientScheduleMap.sequelize.Op.and]: [Sequelize.literal("`subscription->clientschedulemap`.`scheduleId`= `subscription->clientschedulemap`.`parentScheduleId`")],
                                [dbclientScheduleMap.sequelize.Op.and]: [Sequelize.literal("( `subscription->clientschedulemap`.`scheduleId` = `subscription->clientschedulemap`.`parentScheduleId` ) AND ( `subscription->clientschedulemap`.`scheduleId` = (select min(`scheduleId`) from `clientschedulemap` `inSched` where `inSched`.`subscriptionId` = `subscription`.`subscriptionId` ) )")],
                            }
                        }
                    ],
                    where: itemConditions
                }],
                where: itemConditionsclientpayments,
                subQuery: false,
                offset: 0,
                limit: 1,
                raw: true
            });
            dbpaymentTransactions.sequelize.close();
            dbclientpayments.sequelize.close();
            dbClients.sequelize.close();
            dbsubscriptions.sequelize.close();
            dbpricepack.sequelize.close();
            dbservicepricepackmap.sequelize.close();
            dbservices.sequelize.close();
            dbclientScheduleMap.sequelize.close();
            var getAttendance = await attendanceInfoCalculate(information, setObj, date);
            var unmarkAttendance = [{
                unmarkAttendance: getAttendance.unmarked
            }];

            // var getPaymentInfo = await paymentInfo(information, businessId, date);
            // var paymentReceived = [];
            // for (var i = 0; i < getPaymentInfo.length; i++) {
            //     var paymentMsg = 'Payment of Rs ' +
            //         getPaymentInfo[i].totalAmountPaid +
            //         ' has been received from ' +
            //         getPaymentInfo[i].clientName;

            //     paymentReceived[i] = {
            //         paymentDate: getPaymentInfo[i].paymentDate,
            //         paymentAmount: getPaymentInfo[i].totalAmountPaid,
            //         paymentMessage: paymentMsg
            //     };
            // }

            var getScheduleInfo = await scheduleInfo(information, businessId, date);
            // var latestActivity = [{
            //     latestActivity:
            //         [{ paymentReceived: paymentReceived, todaysSchedules: getScheduleInfo }]
            // }];

            var latestActivity = [{
                latestActivity: [{
                    paymentReceived: '',
                    todaysSchedules: getScheduleInfo
                }]
            }];

            //fetching data from log table
            var today = new Date();
            var numberOfDays = 5;
            var dayBeforeDays = new Date(today.getTime() - (numberOfDays * 24 * 60 * 60 * 1000));
            let dbLog = createInstance(["log"], information);
            let log = await dbLog.log.findAll({
                where: {
                    UpdateDateTime: {
                        between: [dayBeforeDays, today]
                    }
                },
                raw: true
            });
            let result = [];
            console.log(log.length);
            for (let i = 0; i < log.length; i++) {
                if (log[i]["activity"]["Status"] == 1) {
                    result.push(log[i]);
                }
            }
            //end of fetching data from log table

            result.sort(function(a, b) {
                return b.createDateTime - a.createDateTime;
            });
            var sub = [];
            Array.prototype.push.apply(sub, todaysBooking);
            // Array.prototype.push.apply(sub, Number(0));
            Array.prototype.push.apply(sub, unmarkAttendance);
            // Array.prototype.push.apply(sub, latestActivity);
            Array.prototype.push.apply(sub, [{
                latestActivity: result
            }]);
            sub.push({ upcomingRenewals: getSubInfo.count });
            var info = Object.assign.apply(Object, sub);
            return await info;
        } catch (error) {
            throw error;
        }
    }


    userProfile = async(information, businessId, filterPrivateFields = true) => {
        try {
            let business = await this.model.findAll({
                where: {
                    businessId: businessId,
                    status: 1
                },
                raw: true
            });

            var sequelize = require("sequelize");
            let dbClients = createInstance(["client"], information);
            let clients = await dbClients.clients.findAll({
                attributes: ['businessId', [sequelize.fn('count', sequelize.col('clientId')), 'totalClients']],
                group: ["businessId"],
                where: {
                    businessId: businessId,
                    status: 1
                },
                raw: true
            });

            let dbCenter = createInstance(["clientCenter"], information);
            let centers = await dbCenter.centers.findAll({
                attributes: ['businessId', [sequelize.fn('count', sequelize.col('centerId')), 'totalCenters']],
                group: ["businessId"],
                where: {
                    status: 1,
                    businessId: businessId
                },
                raw: true
            });

            let dbPricepack = createInstance(["clientPricepack"], information);
            let pricepacks = await dbPricepack.pricepacks.findAll({
                attributes: ['businessId', [sequelize.fn('count', sequelize.col('pricepackId')), 'totalPricepacks']],
                group: ["businessId"],
                where: {
                    status: 1,
                    businessId: businessId
                },
                raw: true
            });

            var sub = [];
            Array.prototype.push.apply(sub, [{
                businessId: business[0].businessId
            }]);
            Array.prototype.push.apply(sub, [{
                businessName: business[0].businessName
            }]);
            Array.prototype.push.apply(sub, [{
                contactNumber: business[0].contactNumber
            }]);
            Array.prototype.push.apply(sub, [{
                emailId: business[0].emailId
            }]);
            Array.prototype.push.apply(sub, [{
                clients: clients[0].totalClients
            }]);
            Array.prototype.push.apply(sub, [{
                centers: centers[0].totalCenters
            }]);
            Array.prototype.push.apply(sub, [{
                pricepacks: pricepacks[0].totalPricepacks
            }]);

            var final = [];
            final = Object.assign.apply(Object, sub);

            return await final;
        } catch (error) {
            throw error;
        }
    };

    notification = async(information, filterPrivateFields = true) => {
        try {

            let businessId = information.split(',')[1];

            let result = [];
            //Subscription expiry notification

            let paymentData = await this.model.sequelize.query("SELECT subscriptiondate, packages.name as PackageName, duration as PackageDuration, packages.amount as PricePackAmount, ( SELECT Date_add( subscriptiondate, INTERVAL duration day ) ) as expiryDate, ( SELECT Datediff( Date(( SELECT Date_add( subscriptiondate, INTERVAL duration day ) )), Date(Now()) ) ) as Diff, ( SELECT Date_add( Date(Now()), INTERVAL " + TRIAL_PERIOD + " day ) ) AS trialExpireDate, ( SELECT Datediff( Date(( SELECT Date_add( Date(Now()), INTERVAL " + TRIAL_PERIOD + " day ) )), Date(Now()) ) ) as trialEndsIn FROM `payment` JOIN `packages` ON payment.packageid = packages.packageid WHERE payment.businessId='" + businessId + "' AND payment.isRenewl='1'", {
                type: this.model.sequelize.QueryTypes.SELECT
            });


            if (paymentData[0].Diff == 1 || paymentData[0].Diff == 3 || paymentData[0].Diff == 5) {
                result.push({
                    "businessId": businessId,
                    "activity": {
                        "Status": 2,
                        "header": "Knack subscription due",
                        "NotificationText": "Your Knack subscription is going to end in" + paymentData[0].Diff + " days.Re-subscribe to enjoy uninterrupted service"
                    },
                    "referenceTable": "Knack subscription",
                    "createDateTime": paymentData[0].createDateTime,
                });
            }

            //For Notes
            let Notedb = createInstance(["notes"], information);

            let note = await Notedb.sequelize.query("SELECT notes.*,clients.clientName FROM `notes` JOIN `clients` ON notes.clientId = clients.clientId WHERE notes.reminder_date = CURDATE()", {
                type: Notedb.sequelize.QueryTypes.SELECT
            });

            if (note.length > 0) {
                result.push({
                    "businessId": businessId,
                    "activity": {
                        "Status": 2,
                        "noteId": note[0].noteId,
                        "header": "Client notes",
                        "reminder_date": note[0].reminder_date,
                        "reminder_time": note[0].reminder_time,
                        "NotificationText": "Reminder for " + note[0].clientName + "'s note : " + note[0].heading,
                    },
                    "referenceTable": "notes",
                    "createDateTime": note[0].createDateTime,
                });
            }

            // let Unscheduled_Clients = await ScheduleDB.unscheduledClientList(information);

            // Pending Attendance
            let connection = 'localhost,' + businessId;
            let x = await ScheduleDB.scheduleDashboard(connection);
            if (x[0]["booking"][0]["pendingAttendance"] > 0) {
                let NotificationText = 'You have ' + x[0]["booking"][0]["pendingAttendance"] + ' attendance records pending. Mark them before it gets too late!';
                const newLog = {
                    businessId: businessId,
                    activity: {
                        header: 'Pending Attendance Record',
                        Status: 2,
                        UserStatus: 0,
                        NotificationText: NotificationText,
                        activityDate: new Date(),
                        activityTime: new Date(),
                        message: 'Client Rescheduled',
                    },
                    referenceTable: 'clientScheduleMap',
                    CreateDateTime: new Date(),
                    UpdateDateTime: new Date(),
                    "status": 1
                };
                result.push(newLog);
            }
            // 

            // unschedule Clients
            let unScheduledClients = await RemainingClientsOfService(connection);
            for (let i = 0; i < unScheduledClients.length; i++) {
                if (unScheduledClients[i] != "null") {
                    result.push(unScheduledClients[i]);
                }
            }
            //end of Unscheduled Clients...

            let dbLog = createInstance(["log"], information);
            let log = await dbLog.log.findAll({
                where: {
                    status: 1
                },
                order: [
                    ['CreateDateTime', 'DESC'],
                    ['UpdateDateTime', 'DESC']
                ],
                raw: true
            });

            for (let i = 0; i < log.length; i++) {
                //if (log[i]["activity"]["Status"] == 2 && log[i]["activity"]["UserStatus"] != 1) {
                if (log[i]["activity"]["Status"] == 2) {
                    // 2 = Notification; 1 = Latest Activity
                    result.push(log[i]);
                }
            }
            result.sort(function(a, b) {
                return b.createDateTime - a.createDateTime;
            })



            return result;
        } catch (error) {
            throw error;
        }
    };



    // View Center
    viewCenter = async(information, filterPrivateFields = true) => {
        try {
            //======================connections==========================
            let dbCenter = createInstance(["clientCenter"], information);
            let dbSchedule = createInstance(["clientSchedule"], information);
            let dbTSM = createInstance(["teamScheduleMap"], information);
            dbTSM.teamschedulemap.belongsTo(dbSchedule.schedule, {
                foreignKey: "scheduleId",
                targetKey: "scheduleId"
            });
            dbSchedule.schedule.belongsTo(dbCenter.centers, {
                foreignKey: "centerId",
                targetKey: "centerId"
            });

            //=================necessary information=====================
            let getInfo = information.split(",");
            let businessId = getInfo[1];
            let userId = getInfo[2];
            if (!userId) throw new ApplicationError("No UserId", 401);


            //=======================user type===========================
            let checkUser = await loginSignUpHelperDB.viewLogin({
                businessId: businessId,
                userId: userId
            });
            if (checkUser.length == 0) throw new ApplicationError("Invalid User", 401);
            let userStatus = checkUser[0].userType == 1 ? 1 : checkUser[0].userType == 2 ? 2 : 0;


            //================conditional checking=======================
            let showCenter = [];
            if (userStatus == 1) { //main admin
                showCenter = await dbCenter.centers.findAll({
                    attributes: ['centerId', 'centerName'],
                    where: {
                        businessId: businessId,
                        status: 1
                    },
                    raw: true
                });
            } else if (userStatus == 2) { //centre admin
                let checkSchedule = await dbTSM.teamschedulemap.findAll({
                    attributes: ['teamId'],
                    where: {
                        businessId: businessId,
                        teamId: userId,
                        status: 1
                    },
                    include: [{
                        model: dbSchedule.schedule,
                        attributes: ['scheduleId', 'centerId'],
                        include: [{
                            model: dbCenter.centers,
                            attributes: ['centerId', 'centerName']
                        }]
                    }],
                    raw: true
                });
                for (let schedule of checkSchedule) {
                    showCenter.push({
                        centerId: schedule['schedule.center.centerId'],
                        centerName: schedule['schedule.center.centerName']
                    });
                }
            } else { //other cases
                return new ApplicationError(
                    "user is neither a centre admin nor a main admin !",
                    401
                );
            }

            //========removes the duplicate objects==============
            let centers = showCenter.filter((center, index, self) =>
                index === self.findIndex((t) => (
                    t.centerId == center.centerId &&
                    t.centerName == center.centerName
                ))
            )
            return await centers;

            //disconnect
            dbCenter.sequelize.close();
            dbSchedule.sequelize.close();
            dbTSM.sequelize.close();
        } catch (error) {
            throw error;
        }
    };

}
export default new UserDB();