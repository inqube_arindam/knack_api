import { filterFields, formatDate, dumpTables, sendEmail, sendSms, forgotPasswordSms, createInstance } from "../lib/common";
import { hashPassword, comparePassword } from "../lib/crypto";
import { ApplicationError } from "../lib/errors";
import { decodeToken, generateToken } from "../lib/token";
import {
    KNACKDB,
    KNACK_DBSETTINGS,
    KNACK_UPLOAD_URL
} from "../lib/constants.js";
import Sequelize from "sequelize";
import BaseModel from "./BaseModel";
import LoginSignupSchema from "../schemas/loginSignup.schema.js";
import LoginMappingSchema from "../schemas/loginMapping.schema.js";
import PaymentSchema from "../schemas/payment.schema.js";
import PackagesSchema from "../schemas/knackadmin.packages.schema.js";
import BusinessSchema from "../schemas/business.schema.js";
const PUBLIC_FIELDS = [
    "userId",
    "businessId",
    "centerId",
    "name",
    "businessName",
    "emailId",
    "contactNumber",
    "userType",
    "modulePermissions",
    "role",
    "isSuperAdminTeam"
];

const Op = Sequelize.Op;

export class KnackUserDB extends BaseModel {
    constructor(connection) {
        super("logininfo", connection);
        //Login model for updating user's status
        this.Loginschema = LoginSignupSchema.LoginSignUp();
        this.LoginName = "logininfo";
        this.Loginmodel = this.connection.model(this.LoginName, this.Loginschema);

        //Get Package Model Data
        this.packageSchema = PackagesSchema.Packages();
        this.packageName = "packages";
        this.packageModel = this.connection.model(this.packageName, this.packageSchema);

        //Get Payment Model Data
        this.PaymentSchema = PaymentSchema.Payment();
        this.paymentName = "payment";
        this.paymentModel = this.connection.model(this.paymentName, this.PaymentSchema);

        //Creating Login Mapping model
        this.Mappingschema = LoginMappingSchema.LoginMapping();
        this.Mappingname = "loginmapping";
        this.Mappingmodel = this.connection.model(
            this.Mappingname,
            this.Mappingschema
        );

        //Get Business Model Data
        this.businessSchema = BusinessSchema.Business();
        this.businessName = "businessinfo";
        this.businessModel = this.connection.model(this.businessName, this.businessSchema);
    }

    users = [];

    listAllKnackUser = async(businessId) => {
        console.log('hello ' + businessId);
        var users = {};
        var newUser = {};
        var paymentArr = {};
        var packageArr = {};
        var businessArr = {};
        try {
            //console.log(businessId);
            // this.Loginmodel.hasMany(this.paymentModel, { as: 'payments', foreignKey: 'userId',targetKey: 'userId'});
            // paymentModel.belongsTo(packageModel);
            //LoginModel.hasMany(paymentModel, {foreignKey: 'userId', targetKey: 'userId'});
            //paymentModel.belongsTo(packageModel, {foreignKey: 'packageId', targetKey: 'packageId'});
            var users = await this.Loginmodel.findAll({
                attributes: ['userId', 'businessId', 'name', 'businessName', 'contactNumber', 'userType', 'dateOfBirth', 'lastLogin', 'status', 'isSuperAdminTeam'],
                where: {
                    status: {
                        [Op.or]: [0, 1]
                    },
                    isSuperAdmin: 0,
                    isSuperAdminTeam: 0
                },
                raw: true
            });
            // console.log(JSON.stringify(users));
            if (users != null) {
                for (var i = 0, len = users.length; i < len; i++) {
                    // var business  = await this.businessModel.findAll({
                    //   where: {
                    //     businessId: users[i].businessId
                    //   }
                    // });
                    // console.log("=========================="+business);

                    // if(business){
                    //   users[i].businessName = business[0].businessName;
                    // }
                    var payment = await this.paymentModel.findAll({
                        where: {
                            userId: users[i].userId
                        },
                        order: [
                            ['createDateTime', 'DESC']
                        ],
                        raw: true
                    });
                    if (payment != null) {
                        for (var j = 0, plen = payment.length; j < plen; j++) {
                            if (j == 0) {
                                users[i].paymentAmount = payment[0].amount;
                                users[i].totalNumberOfPayment = payment.length;
                                users[i].dueDate = payment[0].amountDate;
                                users[i].paymentMode = payment[0].paymentMode;
                                users[i].paymentStatus = payment[0].paymentStatus;
                                var packageplan = await this.packageModel.findOne({
                                    where: {
                                        packageId: payment[j].packageId
                                    },
                                    raw: true
                                });
                                if (packageplan != null) {
                                    users[i].packageName = packageplan.name;
                                    users[i].planName = packageplan.duration + ' days / ' + packageplan.amount;
                                }
                                //users[i].packageArr = packageplan;
                            }
                        }
                    }

                }
            }
            console.log(users);
        } catch (error) {
            throw error;
        }
        //console.log(newUser);
        //return await filterFields(users, PUBLIC_FIELDS, true);
        return await users;
        //console.log('error2');
    };

    activateAdminUser = async(information, userId) => {
        var users = {};
        try {
            let usersUpdate = await this.Loginmodel.update({
                status: 1
            }, {
                where: {
                    userId: userId
                }
            });
            var users = await this.Loginmodel.findAll({
                where: {
                    status: {
                        [Op.or]: [0, 1]
                    },
                    isSuperAdmin: 0,
                    isSuperAdminTeam: 0
                },
                raw: true
            });

            if (users != null) {
                for (var i = 0, len = users.length; i < len; i++) {
                    //console.log(users[i].userId);
                    //newUser[i]['userId'] = users[i].userId;
                    /*var payment = await this.paymentModel.findAll({
                        where: {
                          userId: users[i].userId
                        },
                        raw: true
                    });
                    console.log(payment);*/
                    // var business  = await this.businessModel.findAll({
                    //   where: {
                    //     businessId: users[i].businessId
                    //   }
                    // });
                    // if(business!=null){
                    //   users[i].businessName = business[0].businessName;
                    // }
                    var payment = await this.paymentModel.findAll({
                        where: {
                            userId: users[i].userId
                        },
                        order: [
                            ['createDateTime', 'DESC']
                        ],
                        raw: true
                    });
                    if (payment != null) {
                        for (var j = 0, plen = payment.length; j < plen; j++) {
                            if (j == 0) {
                                users[i].paymentAmount = payment[0].amount;
                                users[i].totalNumberOfPayment = payment.length;
                                users[i].dueDate = payment[0].amountDate;
                                users[i].paymentMode = payment[0].paymentMode;
                                users[i].paymentStatus = payment[0].paymentStatus;
                                var packageplan = await this.packageModel.findOne({
                                    where: {
                                        packageId: payment[j].packageId
                                    },
                                    raw: true
                                });
                                if (packageplan != null) {
                                    users[i].packageName = packageplan.name;
                                    users[i].planName = packageplan.duration + ' days / ' + packageplan.amount;
                                }
                                //users[i].packageArr = packageplan;
                            }
                        }
                    }
                    //users[i].paymentArr = payment;
                    //users[i].businessArr= business;
                    //console.log(payment);
                }
            } else {
                users[0].businessName = '';
                users[0].paymentAmount = '';
                users[0].totalNumberOfPayment = '';
                users[0].dueDate = '';
                users[0].paymentMode = '';
                users[0].paymentStatus = '';
                users[0].packageName = '';
            }
        } catch (error) {
            throw error;
        }
        return await users;
    }

    getAllPrevSubs = async(userId) => {
        //console.log('userID 123 '+userId);
        var users = {};
        var xyz = [];
        var users = await this.Loginmodel.find({
            attributes: ['userId', 'businessId', 'name', 'businessName', 'contactNumber', 'userType', 'dateOfBirth', 'lastLogin', 'status', 'isSuperAdminTeam'],
            where: {
                status: {
                    [Op.or]: [0, 1]
                },
                userId: userId
            },
            raw: true
        });

        if (users != null) {
            //console.log(users.userId);
            // var business  = await this.businessModel.find({
            //   where: {
            //     businessId: users.businessId
            //   }
            // });
            // //console.log(users);
            // var userBusinessName = '';
            // if(business!=null){

            // }
            //userBusinessName = users.businessName;
            var payment = await this.paymentModel.findAll({
                where: {
                    userId: userId
                },
                order: [
                    ['createDateTime', 'DESC']
                ],
                offset: 1,
                raw: true
            });


            //return;
            if (payment != null) {
                for (var j = 0, plen = payment.length; j < plen; j++) {
                    //if(j==0){
                    // xyz[j].userId                 = userId;
                    // xyz[j].businessId             = users.businessId;
                    // xyz[j].businessName           = userBusinessName;
                    // xyz[j].name                   = users.name;
                    // xyz[j].contactNumber          = users.contactNumber;
                    // xyz[j].lastLogin              = users.lastLogin;
                    // xyz[j].paymentAmount          = payment[j].amount;
                    // xyz[j].totalNumberOfPayment   = payment.length;
                    // xyz[j].dueDate                = payment[j].amountDate;
                    // xyz[j].paymentMode            = payment[j].paymentMode;
                    // xyz[j].paymentStatus          = payment[j].paymentStatus;
                    var packageplan = await this.packageModel.findOne({
                        where: {
                            packageId: payment[j].packageId
                        },
                        raw: true
                    });
                    var packageName = '';
                    var planName = '';
                    if (packageplan != null) {
                        var packageName = packageplan.name;
                        var planName = packageplan.duration + ' days / ' + packageplan.amount;
                    }

                    let data = {
                        "userId": userId,
                        "userName": users.name,
                        "userBusinessName": users.businessName,
                        "businessId": users.businessId,
                        "contactNumber": users.contactNumber,
                        "lastLogin": users.lastLogin,
                        "paymentAmount": payment[j].amount,
                        "totalNumberOfPayment": payment[j].amount,
                        "dueDate": payment[j].amountDate,
                        "paymentMode": payment[j].paymentMode,
                        "paymentStatus": payment[j].paymentStatus,
                        "packageName": packageName,
                        "planName": planName
                    }
                    console.log(data);
                    xyz.push(data);
                }
            }
        } else {
            xyz[0].userId = '';
            xyz[0].businessId = '';
            xyz[0].businessName = '';
            xyz[0].name = '';
            xyz[0].contactNumber = '';
            xyz[0].lastLogin = '';
            xyz[0].paymentAmount = '';
            xyz[0].totalNumberOfPayment = '';
            xyz[0].dueDate = '';
            xyz[0].paymentMode = '';
            xyz[0].paymentStatus = '';
        }
        return await xyz;
    }

    blockAdminUser = async(userId, status) => {
        var users = {};

        var status = status == 1 ? 0 : 1;
        try {
            let usersUpdate = await this.Loginmodel.update({
                status: status
            }, {
                where: {
                    userId: userId
                }
            });

            var users = await this.Loginmodel.findAll({
                where: {
                    status: {
                        [Op.or]: [0, 1]
                    },
                    isSuperAdmin: 0,
                    isSuperAdminTeam: 0
                },
                raw: true
            });

            if (users != null) {
                for (var i = 0, len = users.length; i < len; i++) {
                    var payment = await this.paymentModel.findAll({
                        where: {
                            userId: users[i].userId
                        },
                        order: [
                            ['createDateTime', 'DESC']
                        ],
                        raw: true
                    });

                    if (payment != null) {
                        for (var j = 0, plen = payment.length; j < plen; j++) {
                            if (j == 0) {
                                users[i].paymentAmount = payment[0].amount;
                                users[i].totalNumberOfPayment = payment.length;
                                users[i].dueDate = payment[0].amountDate;
                                users[i].paymentMode = payment[0].paymentMode;
                                users[i].paymentStatus = payment[0].paymentStatus;
                                var packageplan = await this.packageModel.findOne({
                                    where: {
                                        packageId: payment[j].packageId
                                    },
                                    raw: true
                                });
                                if (packageplan != null) {
                                    users[i].packageName = packageplan.name;
                                    users[i].planName = packageplan.duration + ' days / ' + packageplan.amount;
                                }
                                //users[i].packageArr = packageplan;
                            }
                        }
                    }
                }
            } else {
                users[0].businessName = '';
                users[0].paymentAmount = '';
                users[0].totalNumberOfPayment = '';
                users[0].dueDate = '';
                users[0].paymentMode = '';
                users[0].paymentStatus = '';
                users[0].packageName = '';
            }
        } catch (error) {
            throw error;
        }
        return await users;
    };


    searchKnackAdminUser = async(reinfomationq, searchUser = null, searchUserStatus = null, searchMOP = null, searchUserDate = null) => {
        try {
            var users = {};
            var searchUserText = searchUser;
            var whereStatement = {};
            whereStatement.isSuperAdminTeam = 0;
            whereStatement.isSuperAdmin = 0;

            if (searchUser != null) {
                whereStatement.name = { like: ["" + searchUser + "%"] };
            }
            if (searchUserStatus != null && searchUserStatus != 'status') {
                whereStatement.status = searchUserStatus;
            } else {
                whereStatement.status = {
                    [Op.or]: [0, 1] };
            }

            var users = await this.Loginmodel.findAll({
                // attributes: ['userId', 'businessId', 'name', 'contactNumber','userType','dateOfBirth','lastLogin','status'],
                where: whereStatement,
                raw: true
            });
            if (users != null) {
                for (var i = 0, len = users.length; i < len; i++) {
                    var payment = await this.paymentModel.findAll({
                        where: {
                            userId: users[i].userId
                        },
                        order: [
                            ['createDateTime', 'DESC']
                        ],
                        raw: true
                    });
                    if (payment != null) {
                        for (var j = 0, plen = payment.length; j < plen; j++) {
                            if (j == 0) {
                                users[i].paymentAmount = payment[0].amount;
                                users[i].totalNumberOfPayment = payment.length;
                                users[i].dueDate = payment[0].amountDate;
                                users[i].paymentMode = payment[0].paymentMode;
                                users[i].paymentStatus = payment[0].paymentStatus;
                                var packageplan = await this.packageModel.findOne({
                                    where: {
                                        packageId: payment[j].packageId
                                    },
                                    raw: true
                                });
                                if (packageplan != null) {
                                    users[i].packageName = packageplan.name;
                                    users[i].planName = packageplan.duration + ' days / ' + packageplan.amount;
                                }
                                //users[i].packageArr = packageplan;
                            }
                        }
                    }
                }
                //console.log('here');
                if (searchMOP != null && searchMOP != 'mop') {

                    users = users.filter(function(value) { return value.paymentMode == searchMOP; })
                }

                if (searchUserDate != null) {
                    var getDueDate = await formatDate(searchUserDate);
                    users = users.filter(function(value) {
                        if (value.dueDate != null) {
                            var dueDateStr = new Date(value.dueDate);
                            var year = dueDateStr.getUTCFullYear();
                            var month = dueDateStr.getUTCMonth() + 1;
                            var day = dueDateStr.getUTCDate();
                            month = ('0' + month).slice(-2);
                            day = ('0' + day).slice(-2);
                            var dueDateValue = year + '-' + month + '-' + day;
                            return dueDateValue === getDueDate;
                        }
                    })
                }
            } else {
                users[0].businessName = '';
                users[0].paymentAmount = '';
                users[0].totalNumberOfPayment = '';
                users[0].dueDate = '';
                users[0].paymentMode = '';
                users[0].paymentStatus = '';
                users[0].packageName = '';
            }
        } catch (error) {
            throw error;
        }
        return await users;
    };

    editTeamMemberDetails = async(userId, teamMemberName, teamMemberRole, teamMemberContactNumber) => {
        try {
            let usersUpdate = await this.Loginmodel.update({
                name: teamMemberName,
                role: teamMemberRole,
                contactNumber: teamMemberContactNumber,
                lastLogin: Date.now()
            }, {
                where: {
                    userId: userId
                }
            });
        } catch (error) {
            throw error;
        }
        var users = {};
        try {
            var users = await this.Loginmodel.findAll({
                attributes: ['userId', 'businessId', 'name', 'businessName', 'emailId', 'contactNumber', 'userType', 'dateOfBirth', 'lastLogin', 'status', 'role', 'isSuperAdminTeam'],
                where: {
                    status: {
                        [Op.or]: [0, 1]
                    },
                    isSuperAdmin: 0,
                    isSuperAdminTeam: 1
                },
                order: [
                    ['lastLogin', 'DESC']
                ],
                raw: true
            });
        } catch (error) {
            throw error;
        }
        return await users;
    };



    getTeamMemberDetails = async(userId) => {
        var users = {};
        try {
            // this.Loginmodel.hasMany(this.paymentModel, { as: 'payments', foreignKey: 'userId',targetKey: 'userId'});
            // paymentModel.belongsTo(packageModel);
            //LoginModel.hasMany(paymentModel, {foreignKey: 'userId', targetKey: 'userId'});
            //paymentModel.belongsTo(packageModel, {foreignKey: 'packageId', targetKey: 'packageId'});        

            var users = await this.Loginmodel.findOne({
                attributes: ['userId', 'businessId', 'name', 'businessName', 'emailId', 'contactNumber', 'userType', 'dateOfBirth', 'lastLogin', 'status', 'role', 'isSuperAdminTeam'],
                where: {
                    userId: userId,
                    isSuperAdminTeam: 1
                }
            });
            //console.log('Monitor');
        } catch (error) {
            throw error;
        }
        return await users;
    };
    //editTeamMemberDetails = async(userId,memberName,memberRole,memberContactNumber) => {
    //var users = {};
    //try {
    // this.Loginmodel.hasMany(this.paymentModel, { as: 'payments', foreignKey: 'userId',targetKey: 'userId'});
    // paymentModel.belongsTo(packageModel);
    //LoginModel.hasMany(paymentModel, {foreignKey: 'userId', targetKey: 'userId'});
    //paymentModel.belongsTo(packageModel, {foreignKey: 'packageId', targetKey: 'packageId'});        

    // var users = await this.Loginmodel.findOne({
    //   attributes: ['userId', 'businessId', 'name', 'businessName','emailId','contactNumber','userType','dateOfBirth','lastLogin','status','role','isSuperAdminTeam'],  
    //   where: {
    //       userId: userId,
    //       isSuperAdminTeam: 1              
    //     }
    // }); 
    //console.log('Monitor');
    //} catch (error) {
    // throw error;
    // }
    //return await users;
    //};

    listAllKnackTeam = async() => {
        var users = {};
        try {
            // this.Loginmodel.hasMany(this.paymentModel, { as: 'payments', foreignKey: 'userId',targetKey: 'userId'});
            // paymentModel.belongsTo(packageModel);
            //LoginModel.hasMany(paymentModel, {foreignKey: 'userId', targetKey: 'userId'});
            //paymentModel.belongsTo(packageModel, {foreignKey: 'packageId', targetKey: 'packageId'});        
            var users = await this.Loginmodel.findAll({
                attributes: ['userId', 'businessId', 'name', 'businessName', 'emailId', 'contactNumber', 'userType', 'dateOfBirth', 'lastLogin', 'status', 'role', 'isSuperAdminTeam'],
                where: {
                    status: {
                        [Op.or]: [0, 1]
                    },
                    isSuperAdmin: 0,
                    isSuperAdminTeam: 1
                },
                order: [
                    ['lastLogin', 'DESC']
                ],
                raw: true
            });
            //console.log('Monitor');
        } catch (error) {
            throw error;
        }
        return await users;
    };
    //create new team member
    createNewTeam = async(name, emailId, contactNumber, password, role) => {
        //console.log(role);
        let verifyuser = await this.Loginmodel.find({
            where: {
                status: 1,
                isSuperAdminTeam: 1,
                emailId: emailId
            },
            raw: true
        });

        if (verifyuser) {
            throw new ApplicationError("Email ID Already Exist", 401);
        } else {
            const newLoginUser = {
                name,
                emailId,
                contactNumber,
                password: await hashPassword(password),
                role,
                FcmUserToken: '123456',
                IMEINumber: '654789',
                userType: 0,
                status: 1,
                isSuperAdmin: 0,
                isSuperAdminTeam: 1
            };
            await this.Loginmodel.create(newLoginUser);
            var users = {};
            try {
                var users = await this.Loginmodel.findAll({
                    attributes: ['userId', 'businessId', 'name', 'businessName', 'emailId', 'contactNumber', 'userType', 'dateOfBirth', 'lastLogin', 'status', 'role', 'isSuperAdminTeam'],
                    where: {
                        status: {
                            [Op.or]: [0, 1]
                        },
                        isSuperAdmin: 0,
                        isSuperAdminTeam: 1
                    },
                    order: [
                        ['lastLogin', 'DESC']
                    ],
                    raw: true
                });
            } catch (error) {
                throw error;
            }
            return await users;
        }
    };

    editTeamMember = async(name, contactNumber, role, teamId) => {
        var users = {};
        try {
            let users = await this.Loginmodel.update({
                name: name,
                contactNumber: contactNumber,
                role: role
            }, {
                where: {
                    userId: teamId
                }
            });
        } catch (error) {
            throw error;
        }

        try {
            var users = await this.Loginmodel.findAll({
                attributes: ['userId', 'businessId', 'name', 'businessName', 'emailId', 'contactNumber', 'userType', 'dateOfBirth', 'lastLogin', 'status', 'role', 'isSuperAdminTeam'],
                where: {
                    status: {
                        [Op.or]: [0, 1]
                    },
                    isSuperAdmin: 0,
                    isSuperAdminTeam: 1
                },
                order: [
                    ['lastLogin', 'DESC']
                ],
                raw: true
            });
        } catch (error) {
            throw error;
        }
        return await users;
    };

    changeMemberStatus = async( //change team member active/Inactive
        status,
        userId,
        filterPrivateFields = true
    ) => {
        //console.log(status);
        try {
            let users = await this.Loginmodel.update({
                status: status == 1 ? 0 : 1
            }, {
                where: {
                    userId: userId
                }
            });
        } catch (error) {
            throw error;
        }
        //console.log(users);
        var users = {};
        try {
            var users = await this.Loginmodel.findAll({
                attributes: ['userId', 'businessId', 'name', 'businessName', 'emailId', 'contactNumber', 'userType', 'dateOfBirth', 'lastLogin', 'status', 'role', 'isSuperAdminTeam'],
                where: {
                    status: {
                        [Op.or]: [0, 1]
                    },
                    isSuperAdmin: 0,
                    isSuperAdminTeam: 1
                },
                order: [
                    ['lastLogin', 'DESC']
                ],
                raw: true
            });
        } catch (error) {
            throw error;
        }
        return await users;
    };

    searchAllKnackTeamMember = async(reinfomationq, searchUser = null, searchLastLoginDate = null) => {
        var users = {};
        var searchUserText = searchUser;
        var whereStatement = {};
        whereStatement.status = {
            [Op.or]: [0, 1] };
        whereStatement.isSuperAdminTeam = 1;

        if (searchUser != null) {
            whereStatement.name = { like: ["" + searchUser + "%"] };
        }

        if (searchLastLoginDate != null) {
            //var date_diff     = searchLastLoginDate;
            //var date_format   = new Date(searchLastLoginDate); 
            var date_start = await formatDate(searchLastLoginDate);
            //var currDate      = new Date(date_start);
            //console.log(currDate);
            var day = new Date(date_start);
            //console.log(day); 
            var nextDay = new Date(day);
            nextDay.setDate(day.getDate() + 1);
            //console.log(nextDay); 
            var date_end = await formatDate(nextDay);
            whereStatement.lastLogin = {
                    [Op.gt]: date_start,
                    [Op.lt]: date_end
                }
                //whereStatement.lastLogin = {sequelize.fn('date_format', searchLastLoginDate, '%Y-%m-%d')}
                // var date          = new Date();
                // var currentYear   = date.getFullYear();
                // var currentMonth  = date.getMonth()+1;
                // var currentDay    = date.getDate();
                // var date_end      = currentYear+'-'+(currentMonth<=9 ? '0'+currentMonth : currentMonth)+'-'+(currentDay<=9 ? '0'+currentDay : currentDay);
                // //console.log(date_end);
                // date.setDate(date.getDate() - date_diff);
                // var previousYear  = date.getFullYear();
                // var previousMonth = date.getMonth()+1;
                // var previousDay   = date.getDate();
                // var date_start      = previousYear+'-'+(previousMonth<=9 ? '0'+previousMonth : previousMonth)+'-'+(previousDay<=9 ? '0'+previousDay : previousDay);
                // //console.log(date_start);
                // whereStatement.lastLogin = {
                //   [Op.gt]: date_start,
                //   [Op.lt]: date_end
                // }
        }
        try {
            var users = await this.Loginmodel.findAll({
                attributes: ['userId', 'businessId', 'name', 'businessName', 'emailId', 'contactNumber', 'userType', 'dateOfBirth', 'lastLogin', 'status', 'role', 'isSuperAdminTeam'],
                where: whereStatement,
                order: [
                    ['lastLogin', 'DESC']
                ],
                raw: true
            });
        } catch (error) {
            throw error;
        }
        return await users;
    };

    //check knack admin login credentials
    loginKnackAdmin = async(userName, passWord, FcmUserToken, IMEINumber) => {
        var users = {};
        //console.log('==========================='+userName+'=============================');
        const login = await this.Loginmodel.find({
            where: {
                emailId: userName,
                isSuperAdmin: 1
            },
            raw: true
        });
        //console.log(login);
        if (!login) {
            throw new ApplicationError("Email ID not Present", 401);
        } else {
            //console.log(login.password);
            if (await comparePassword(passWord, login.password)) {
                let chechkLogin = await this.Mappingmodel.find({
                    where: {
                        IMEINumber: IMEINumber
                    },
                    raw: true
                });
                if (!chechkLogin) {
                    const newLogin = {
                        userId: login.userId,
                        businessId: login.businessId,
                        centerId: login.centerId,
                        FcmUserToken: FcmUserToken,
                        IMEINumber: IMEINumber
                    };
                    await this.Mappingmodel.create(newLogin);
                } else {
                    await this.Mappingmodel.update({
                        userId: login.userId,
                        businessId: login.businessId,
                        centerId: login.centerId,
                        lastLogin: Date.now(),
                        FcmUserToken: FcmUserToken
                    }, { where: { IMEINumber: IMEINumber } });
                }

                let finduser = await this.Loginmodel.find({
                    where: {
                        emailId: userName
                    },
                    raw: true
                });

                var seqdb = new Sequelize(finduser.businessId, KNACKDB.user, KNACKDB.password, {
                    host: KNACKDB.host,
                    port: 3306,
                    dialect: "mysql",
                    pool: {
                        max: 100,
                        min: 0,
                        idle: 10000
                    }
                });
                //for centre admin
                if (finduser.userType == 2 || finduser.userType == 1) {

                    var emp = await seqdb.query("select * from employees where employeeId ='" + finduser.userId + "'", {
                        type: Sequelize.QueryTypes.SELECT //this code will remove the duplicate data (meta data)
                    });

                    // var centre = await seqdb.query("select * from centers where centerId ='" + emp[0].centerId + "'", {
                    //   type: Sequelize.QueryTypes.SELECT //this code will remove the duplicate data (meta data)
                    // });
                    var info = [];
                    Array.prototype.push.apply(info, login);
                    if (finduser.userType == 1) {
                        Array.prototype.push.apply(info, [{ User: "Main Admin" }]);
                    } else {
                        Array.prototype.push.apply(info, [{ User: "Center Admin" }]);
                    }
                    //Array.prototype.push.apply(info, [{ centerDetails: centre }]);
                    Object.assign.apply(Object, info);
                    // send the response, along with the token to be used in the Bearer header
                    // (see verify below)
                    //res.send(await successRoute(user));
                    //return login;
                }
                //end of centre admin section
                const token = await generateToken(login.businessId);
                login.token = await token;
                return login;
            } else {
                throw new ApplicationError("Invalid Password", 401);
            }
        }
    }
}
export default new KnackUserDB();