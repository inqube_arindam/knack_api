export async function addPricepack(information, db, getCond) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let pricepacks = await db.pricepacks.create(setCond);
        return pricepacks;
    } catch (error) {
        throw error;
    }
}

export async function viewPricepack(information, db, getCond) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let pricepacks = await db.pricepacks.findAll({
            where: setCond,
            raw: true
        });
        return pricepacks;
    } catch (error) {
        throw error;
    }
}

export async function updatePricepack(information, db, getCond, getData) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let pricepacks = await db.pricepacks.update(
            getData, {
                where: setCond
            }
        );
        return pricepacks;
    } catch (error) {
        throw error;
    }
}

export async function deletePricepack(information, db, getCond) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let pricepacks = await db.pricepacks.update({
            status: 0,
            updateDateTime: Date.now()
        }, {
                where: setCond
            }
        );
        return pricepacks;
    } catch (error) {
        throw error;
    }
}