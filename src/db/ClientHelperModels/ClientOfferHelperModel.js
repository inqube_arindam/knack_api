export async function addOffer(information, db, getCond) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let offers = await db.offers.create(setCond);
        return offers;
    } catch (error) {
        throw error;
    }
}

export async function viewOffer(information, db, getCond) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let offers = await db.offers.findAll({
            where: setCond,
            raw: true
        });
        return offers;
    } catch (error) {
        throw error;
    }
}

export async function updateOffer(information, db, getCond, getData) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let offers = await db.offers.update(
            getData, {
                where: setCond
            }
        );
        return offers;
    } catch (error) {
        throw error;
    }
}

export async function deleteOffer(information, db, getCond) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let offers = await db.offers.update({
            status: 0,
            updateDateTime: Date.now()
        }, {
                where: setCond
            }
        );
        return offers;
    } catch (error) {
        throw error;
    }
}