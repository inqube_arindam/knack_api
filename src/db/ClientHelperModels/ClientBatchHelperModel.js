export async function addBatch(information, db, getCond) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let batches = await db.batches.create(setCond);
        return batches;
    } catch (error) {
        throw error;
    }
}

export async function viewBatch(information, db, getCond) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let batches = await db.batches.findAll({
            where: setCond,
            raw: true
        });
        return batches;
    } catch (error) {
        throw error;
    }
}

export async function updateBatch(information, db, getCond, getData) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let batches = await db.batches.update(
            getData, {
                where: setCond
            }
        );
        return batches;
    } catch (error) {
        throw error;
    }
}

export async function deleteBatch(information, db, getCond) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let batches = await db.batches.update({
            status: 0,
            updateDateTime: Date.now()
        }, {
                where: setCond
            }
        );
        return batches;
    } catch (error) {
        throw error;
    }
}