export async function addEmployee(information, db, getCond) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let employees = await db.employees.create(setCond);
        return employees;
    } catch (error) {
        throw error;
    }
}

export async function viewEmployee(information, db, getCond) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let employees = await db.employees.findAll({
            where: setCond,
            raw: true
        });
        return employees;
    } catch (error) {
        throw error;
    }
}

export async function updateEmployee(information, db, getCond, getData) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let employees = await db.employees.update(
            getData, {
                where: setCond
            }
        );
        return employees;
    } catch (error) {
        throw error;
    }
}

export async function deleteEmployee(information, db, getCond) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let employees = await db.employees.update({
            status: 0,
            updateDateTime: Date.now()
        }, {
                where: setCond
            }
        );
        return employees;
    } catch (error) {
        throw error;
    }
}

export async function countEmployee(information, db, getCond) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let employees = await db.employees.findAndCountAll({
            where: setCond
          });
        return employees.count;
    } catch (error) {
        throw error;
    }
}