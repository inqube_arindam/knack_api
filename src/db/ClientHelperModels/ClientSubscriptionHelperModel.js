export async function addSubscription(information, db, getCond) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let subscriptions = await db.subscriptions.create(setCond);
        return subscriptions;
    } catch (error) {
        throw error;
    }
}

export async function viewSubscription(information, db, getCond) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let subscriptions = await db.subscriptions.findAll({
            where: setCond,
            raw: true
        });
        return subscriptions;
    } catch (error) {
        throw error;
    }
}

export async function updateSubscription(information, db, getCond, getData) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let subscriptions = await db.subscriptions.update(
            getData, {
                where: setCond
            }
        );
        return subscriptions;
    } catch (error) {
        throw error;
    }
}

export async function deleteSubscription(information, db, getCond) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let subscriptions = await db.subscriptions.update({
            status: 0,
            updateDateTime: Date.now()
        }, {
                where: setCond
            }
        );
        return subscriptions;
    } catch (error) {
        throw error;
    }
}