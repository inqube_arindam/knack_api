export async function addCenter(information, db, getCond) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let centers = await db.centers.create(setCond);
        return centers;
    } catch (error) {
        throw error;
    }
}

export async function viewCenter(information, db, getCond) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let centers = await db.centers.findAll({
            where: setCond,
            raw: true
        });
        return centers;
    } catch (error) {
        throw error;
    }
}

export async function updateCenter(information, db, getCond, getData) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let centers = await db.centers.update(
            getData, {
                where: setCond
            }
        );
        return centers;
    } catch (error) {
        throw error;
    }
}

export async function deleteCenter(information, db, getCond) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let centers = await db.centers.update({
            status: 0,
            updateDateTime: Date.now()
        }, {
                where: setCond
            }
        );
        return centers;
    } catch (error) {
        throw error;
    }
}


export async function countCenter(information, db, getobj) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getobj, setDbid);
        let center = await db.centers.findAndCountAll({
            where: setCond
          });
        return center.count;
    } catch (error) {
        throw error;
    }
}