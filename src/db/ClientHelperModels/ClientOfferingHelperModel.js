export async function addService(information, db, getCond) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let services = await db.services.create(setCond);
        return services;
    } catch (error) {
        throw error;
    }
}

export async function viewService(information, db, getCond) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let services = await db.services.findAll({
            where: setCond,
            raw: true
        });
        return services;
    } catch (error) {
        throw error;
    }
}

export async function updateService(information, db, getCond, getData) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let services = await db.services.update(
            getData, {
                where: setCond
            }
        );
        return services;
    } catch (error) {
        throw error;
    }
}

export async function deleteService(information, db, getCond) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let services = await db.services.update({
            status: 0,
            updateDateTime: Date.now()
        }, {
                where: setCond
            }
        );
        return services;
    } catch (error) {
        throw error;
    }
}