import { filterFields, createInstance, commaSeparatedValues, getAttendance, getScheduleTimeTable, hyphenDateFormat, intersect_arrays, diff_arrays, attendanceInfo, tConv24, convartday, attendanceInfoOfClientAndDate, rsPaginate, sessionInfo } from "../../lib/common";
export async function addSchedule(information, db, getCond) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let schedule = await db.schedule.create(setCond);
        return schedule;
    } catch (error) {
        throw error;
    }
}

export async function viewSchedule(information, db, getCond) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let schedule = await db.schedule.findAll({
            where: setCond,
            raw: true
        });
        return schedule;
    } catch (error) {
        throw error;
    }
}

export async function updateSchedule(information, db, getCond, getData) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let schedule = await db.schedule.update(
            getData, {
                where: setCond
            }
        );
        return schedule;
    } catch (error) {
        throw error;
    }
}

export async function deleteSchedule(information, db, getCond) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let schedule = await db.schedule.update({
            status: 0,
            updateDateTime: Date.now()
        }, {
            where: setCond
        });
        return schedule;
    } catch (error) {
        throw error;
    }
}

export async function ongoingSubSchedule__3_07(information, getObj) {
    try {
        let dbCSM = createInstance(["clientScheduleMap"], information);
        let dbSchedule = createInstance(["clientSchedule"], information);
        let dbServices = createInstance(["clientService"], information);
        let dbSubscription = createInstance(["clientSubscription"], information);
        let dbPricepack = createInstance(["clientPricepack"], information);

        dbCSM.clientschedulemap.belongsTo(dbSchedule.schedule, {
            foreignKey: "scheduleId",
            targetKey: "scheduleId"
        });
        dbSchedule.schedule.belongsTo(dbServices.services, {
            foreignKey: "serviceId",
            targetKey: "serviceId"
        });
        dbSubscription.subscriptions.belongsTo(dbPricepack.pricepacks, {
            foreignKey: "pricepacks",
            targetKey: "pricepackId"
        });

        let bookings = await dbCSM.clientschedulemap.findAll({
            // attributes:
            //     ['scheduleId'],
            // group: ["scheduleId"],
            where: getObj,
            include: [{
                model: dbSchedule.schedule,
                include: [{
                    model: dbServices.services,
                    attributes: ['serviceName']
                }],
                // order: orderBy
            }],
            // order: orderBy
            order: [
                [
                    { model: dbSchedule.schedule },
                    'scheduleDate',
                    'DESC'
                ]
            ],
            raw: true
        });


        //showing the necessary data
        let bookingArrUpper = [],
            bookingArrLower = [],
            bookingArr = [];
        for (let booking of bookings) {
            booking['bookingName'] = booking['schedule.scheduleName'] === null ? booking['schedule.service.serviceName'] : booking['schedule.scheduleName'];
            let getCSM = await dbCSM.clientschedulemap.find({
                where: {
                    status: 1,
                    scheduleId: booking.scheduleId,
                    clientId: getObj.clientId
                },
                raw: true
            });

            //subscription & pricepack details of the booking of the client
            let subscriptions = await dbSubscription.subscriptions.find({
                where: {
                    subscriptionId: getCSM.subscriptionId,
                    status: 1,
                    renewl_status: 1
                },
                include: [{
                    model: dbPricepack.pricepacks
                }],
                raw: true
            });
            //session count
            let countAtd = 0;
            for (let attendance of getCSM.attendanceType) {
                if (attendance.atd == 1 || attendance.atd == 2) countAtd++;
            }

            let setObj = { status: 1, subscriptionId: getCSM.subscriptionId };
            let session = await sessionInfo(information, setObj);
            booking['sessionInfo'] = session[0].attendedSession + '/' + session[0].totalSession;

            // let countSession = subscriptions.totalSessionsAttended == null ? countAtd : subscriptions.totalSessionsAttended + countAtd;
            // booking['sessionInfo'] = booking.sessionCount + '/' + subscriptions['pricepack.serviceDuration'];

            //getting validity
            let pack_val = subscriptions['pricepack.pricepackValidity'];
            var sub_date = new Date(subscriptions.subscriptionDateTime);
            var valDate = new Date(sub_date.getTime() + (pack_val * 24 * 60 * 60 * 1000));

            var today = new Date(); //change the date to today

            // if (valDate > today || subscriptions['pricepack.serviceDuration'] > countSession) {//pricepack validity isn't expired or Ongoing sessions
            //     // booking['subscriptionExpired'] = 0;
            //     bookingArrUpper.push(booking);
            // }
            if (valDate > today || subscriptions['pricepack.serviceDuration'] > session[0].attendedSession) { //pricepack validity isn't expired or Ongoing sessions
                // booking['subscriptionExpired'] = 0;
                bookingArrUpper.push(booking);
            } else {
                // booking['subscriptionExpired'] = 1;
                bookingArrLower.push(booking)
            }
            // console.log(booking);            
        }
        bookingArr = bookingArrUpper.concat(bookingArrLower);

        dbServices.sequelize.close();
        dbCSM.sequelize.close();
        dbSchedule.sequelize.close();
        dbSubscription.sequelize.close();
        dbPricepack.sequelize.close();
        return bookingArr;
    } catch (error) {
        throw error;
    }
}

export async function ongoingSubSchedule(information, getObj) {
    try {
        let dbCSM = createInstance(["clientScheduleMap"], information);
        let dbSchedule = createInstance(["clientSchedule"], information);
        let dbServices = createInstance(["clientService"], information);
        let dbSubscription = createInstance(["clientSubscription"], information);
        let dbPricepack = createInstance(["clientPricepack"], information);

        dbCSM.clientschedulemap.belongsTo(dbSchedule.schedule, {
            foreignKey: "scheduleId",
            targetKey: "scheduleId"
        });
        dbSchedule.schedule.belongsTo(dbServices.services, {
            foreignKey: "serviceId",
            targetKey: "serviceId"
        });
        dbSubscription.subscriptions.belongsTo(dbPricepack.pricepacks, {
            foreignKey: "pricepacks",
            targetKey: "pricepackId"
        });

        let bookings = await dbCSM.clientschedulemap.findAll({
            where: getObj,
            include: [{
                model: dbSchedule.schedule,
                include: [{
                    model: dbServices.services,
                    attributes: ['serviceName']
                }],
            }],
            order: [
                [
                    { model: dbSchedule.schedule },
                    'scheduleDate',
                    'DESC'
                ]
            ],
            raw: true
        });

        //showing the necessary data
        let bookingArrUpper = [],
            bookingArrLower = [],
            bookingArr = [];
        for (let booking of bookings) {
            booking['bookingName'] = booking['schedule.scheduleName'] === null ? booking['schedule.service.serviceName'] : booking['schedule.scheduleName'];
            let getCSM = await dbCSM.clientschedulemap.find({
                where: {
                    status: 1,
                    scheduleId: booking.scheduleId,
                    clientId: getObj.clientId
                },
                raw: true
            });

            //subscription & pricepack details of the booking of the client
            let subscriptions = await dbSubscription.subscriptions.find({
                where: {
                    subscriptionId: getCSM.subscriptionId,
                    status: 1,
                    renewl_status: 1
                },
                include: [{
                    model: dbPricepack.pricepacks
                }],
                raw: true
            });

            //session count
            let setObj = { status: 1, subscriptionId: getCSM.subscriptionId };
            let session = await sessionInfo(information, setObj);
            booking['sessionInfo'] = session[0].durationType == 1 ? session[0].attendedSession + '/' + session[0].totalSession : "" + session[0].attendedSession;
            booking['sessionMessage'] = session[0].message;
            //getting validity
            let pack_val = subscriptions['pricepack.pricepackValidity'];
            var sub_date = new Date(subscriptions.subscriptionDateTime);
            var valDate = new Date(sub_date.getTime() + (pack_val * 24 * 60 * 60 * 1000));

            var today = new Date(); //change the date to today
            if (valDate > today || subscriptions['pricepack.serviceDuration'] > session[0].attendedSession) { //pricepack validity isn't expired or Ongoing sessions
                bookingArrUpper.push(booking);
            } else {
                bookingArrLower.push(booking)
            }
        }
        bookingArr = bookingArrUpper.concat(bookingArrLower);

        dbServices.sequelize.close();
        dbCSM.sequelize.close();
        dbSchedule.sequelize.close();
        dbSubscription.sequelize.close();
        dbPricepack.sequelize.close();
        return bookingArr;
    } catch (error) {
        throw error;
    }
}

export async function pastSubSchedule__3_07(information, getObj) {
    try {
        let dbCSM = createInstance(["clientScheduleMap"], information);
        let dbSchedule = createInstance(["clientSchedule"], information);
        let dbServices = createInstance(["clientService"], information);
        let dbSubscription = createInstance(["clientSubscription"], information);
        let dbPricepack = createInstance(["clientPricepack"], information);

        dbCSM.clientschedulemap.belongsTo(dbSchedule.schedule, {
            foreignKey: "scheduleId",
            targetKey: "scheduleId"
        });
        dbSchedule.schedule.belongsTo(dbServices.services, {
            foreignKey: "serviceId",
            targetKey: "serviceId"
        });
        dbSubscription.subscriptions.belongsTo(dbPricepack.pricepacks, {
            foreignKey: "pricepacks",
            targetKey: "pricepackId"
        });

        let bookings = await dbCSM.clientschedulemap.findAll({
            // attributes:
            //     ['scheduleId'],
            // group: ["scheduleId"],
            where: getObj,
            include: [{
                model: dbSchedule.schedule,
                include: [{
                    model: dbServices.services,
                    attributes: ['serviceName']
                }],
                // order: orderBy
            }],
            // order: orderBy
            order: [
                [
                    { model: dbSchedule.schedule },
                    'scheduleDate',
                    'DESC'
                ]
            ],
            raw: true
        });


        //showing the necessary data
        let bookingArrUpper = [],
            bookingArrLower = [],
            bookingArr = [];
        for (let booking of bookings) {
            booking['bookingName'] = booking['schedule.scheduleName'] === null ? booking['schedule.service.serviceName'] : booking['schedule.scheduleName'];
            let getCSM = await dbCSM.clientschedulemap.find({
                where: {
                    status: 1,
                    scheduleId: booking.scheduleId,
                    clientId: getObj.clientId
                },
                raw: true
            });

            //subscription & pricepack details of the booking of the client
            let subscriptions = await dbSubscription.subscriptions.find({
                where: {
                    subscriptionId: getCSM.subscriptionId,
                    status: 1,
                    renewl_status: 1
                },
                include: [{
                    model: dbPricepack.pricepacks
                }],
                raw: true
            });
            //session count
            let countAtd = 0;
            for (let attendance of getCSM.attendanceType) {
                if (attendance.atd == 1 || attendance.atd == 2) countAtd++;
            }
            let countSession = subscriptions.totalSessionsAttended == null ? countAtd : subscriptions.totalSessionsAttended + countAtd;
            booking['sessionInfo'] = booking.sessionCount + '/' + subscriptions['pricepack.serviceDuration'];

            //getting validity
            let pack_val = subscriptions['pricepack.pricepackValidity'];
            var sub_date = new Date(subscriptions.subscriptionDateTime);
            var valDate = new Date(sub_date.getTime() + (pack_val * 24 * 60 * 60 * 1000));

            var today = new Date(); //change the date to today
            if (valDate <= today || subscriptions['pricepack.serviceDuration'] == countSession) { //pricepack validity is expired or completed sessions
                // booking['subscriptionExpired'] = 1;
                bookingArrUpper.push(booking);
            } else {
                // booking['subscriptionExpired'] = 0;
                bookingArrLower.push(booking);
            }
        }
        bookingArr = bookingArrUpper.concat(bookingArrLower);

        dbServices.sequelize.close();
        dbCSM.sequelize.close();
        dbSchedule.sequelize.close();
        dbSubscription.sequelize.close();
        dbPricepack.sequelize.close();
        return bookingArr;
    } catch (error) {
        throw error;
    }
}

export async function pastSubSchedule(information, getObj) {
    try {
        let dbCSM = createInstance(["clientScheduleMap"], information);
        let dbSchedule = createInstance(["clientSchedule"], information);
        let dbServices = createInstance(["clientService"], information);
        let dbSubscription = createInstance(["clientSubscription"], information);
        let dbPricepack = createInstance(["clientPricepack"], information);

        dbCSM.clientschedulemap.belongsTo(dbSchedule.schedule, {
            foreignKey: "scheduleId",
            targetKey: "scheduleId"
        });
        dbSchedule.schedule.belongsTo(dbServices.services, {
            foreignKey: "serviceId",
            targetKey: "serviceId"
        });
        dbSubscription.subscriptions.belongsTo(dbPricepack.pricepacks, {
            foreignKey: "pricepacks",
            targetKey: "pricepackId"
        });

        let bookings = await dbCSM.clientschedulemap.findAll({
            where: getObj,
            include: [{
                model: dbSchedule.schedule,
                include: [{
                    model: dbServices.services,
                    attributes: ['serviceName']
                }],
            }],
            order: [
                [
                    { model: dbSchedule.schedule },
                    'scheduleDate',
                    'DESC'
                ]
            ],
            raw: true
        });

        //showing the necessary data
        let bookingArrUpper = [],
            bookingArrLower = [],
            bookingArr = [];
        for (let booking of bookings) {
            booking['bookingName'] = booking['schedule.scheduleName'] === null ? booking['schedule.service.serviceName'] : booking['schedule.scheduleName'];
            let getCSM = await dbCSM.clientschedulemap.find({
                where: {
                    status: 1,
                    scheduleId: booking.scheduleId,
                    clientId: getObj.clientId
                },
                raw: true
            });

            //subscription & pricepack details of the booking of the client
            let subscriptions = await dbSubscription.subscriptions.find({
                where: {
                    subscriptionId: getCSM.subscriptionId,
                    status: 1,
                    renewl_status: 1
                },
                include: [{
                    model: dbPricepack.pricepacks
                }],
                raw: true
            });

            //session count
            let setObj = { status: 1, subscriptionId: getCSM.subscriptionId };
            let session = await sessionInfo(information, setObj);
            booking['sessionInfo'] = session[0].durationType == 1 ? session[0].attendedSession + '/' + session[0].totalSession : "" + session[0].attendedSession;
            booking['sessionMessage'] = session[0].message;
            //getting validity
            let pack_val = subscriptions['pricepack.pricepackValidity'];
            var sub_date = new Date(subscriptions.subscriptionDateTime);
            var valDate = new Date(sub_date.getTime() + (pack_val * 24 * 60 * 60 * 1000));

            var today = new Date(); //change the date to today
            if (valDate <= today || subscriptions['pricepack.serviceDuration'] == session[0].attendedSession) { //pricepack validity is expired or completed sessions
                bookingArrUpper.push(booking);
            } else {
                bookingArrLower.push(booking);
            }
        }
        bookingArr = bookingArrUpper.concat(bookingArrLower);

        dbServices.sequelize.close();
        dbCSM.sequelize.close();
        dbSchedule.sequelize.close();
        dbSubscription.sequelize.close();
        dbPricepack.sequelize.close();
        return bookingArr;
    } catch (error) {
        throw error;
    }
}



export async function currentWeekSchedule__3_07(information, getObj) {
    try {
        let dbCSM = createInstance(["clientScheduleMap"], information);
        let dbSchedule = createInstance(["clientSchedule"], information);
        let dbServices = createInstance(["clientService"], information);
        let dbSubscription = createInstance(["clientSubscription"], information);
        let dbPricepack = createInstance(["clientPricepack"], information);

        dbCSM.clientschedulemap.belongsTo(dbSchedule.schedule, {
            foreignKey: "scheduleId",
            targetKey: "scheduleId"
        });
        dbSchedule.schedule.belongsTo(dbServices.services, {
            foreignKey: "serviceId",
            targetKey: "serviceId"
        });
        dbSubscription.subscriptions.belongsTo(dbPricepack.pricepacks, {
            foreignKey: "pricepacks",
            targetKey: "pricepackId"
        });

        let bookings = await dbCSM.clientschedulemap.findAll({
            // attributes:
            //     ['scheduleId'],
            // group: ["scheduleId"],
            where: getObj,
            include: [{
                model: dbSchedule.schedule,
                include: [{
                    model: dbServices.services,
                    attributes: ['serviceName']
                }],
                // order: orderBy
            }],
            // order: orderBy
            order: [
                [
                    { model: dbSchedule.schedule },
                    'scheduleDate',
                    'DESC'
                ]
            ],
            raw: true
        });


        //week calculation
        let td = new Date();
        let currentday = td.getDay();
        let weekstartday = new Date(td.getTime() - currentday * 24 * 60 * 60 * 1000);
        let weekendday = new Date(td.getTime() + (7 - (currentday + 1)) * 24 * 60 * 60 * 1000);
        let wStart = await hyphenDateFormat(weekstartday);
        let wEnd = await hyphenDateFormat(weekendday);

        //showing the necessary data
        let bookingArr = [];
        for (let booking of bookings) {
            let subscriptions = await dbSubscription.subscriptions.find({
                where: {
                    subscriptionId: booking.subscriptionId
                },
                include: [{
                    model: dbPricepack.pricepacks
                }],
                raw: true
            });
            booking['bookingName'] = booking['schedule.scheduleName'] == null ? booking['schedule.service.serviceName'] : booking['schedule.scheduleName'];
            for (let schedule of booking.attendanceType) {
                if (new Date(schedule.scdate) >= new Date(wStart) && new Date(schedule.scdate) <= new Date(wEnd)) {
                    booking['sessionInfo'] = booking.sessionCount + '/' + subscriptions['pricepack.serviceDuration'];
                    bookingArr.push(booking);
                    break;
                }
            }
        }

        dbCSM.sequelize.close();
        dbServices.sequelize.close();
        dbSchedule.sequelize.close();
        dbSubscription.sequelize.close();
        dbPricepack.sequelize.close();
        return bookingArr;
    } catch (error) {
        throw error;
    }
}

export async function currentWeekSchedule(information, getObj) {
    try {
        let dbCSM = createInstance(["clientScheduleMap"], information);
        let dbSchedule = createInstance(["clientSchedule"], information);
        let dbServices = createInstance(["clientService"], information);

        dbCSM.clientschedulemap.belongsTo(dbSchedule.schedule, {
            foreignKey: "scheduleId",
            targetKey: "scheduleId"
        });
        dbSchedule.schedule.belongsTo(dbServices.services, {
            foreignKey: "serviceId",
            targetKey: "serviceId"
        });

        let bookings = await dbCSM.clientschedulemap.findAll({
            where: getObj,
            include: [{
                model: dbSchedule.schedule,
                include: [{
                    model: dbServices.services,
                    attributes: ['serviceName']
                }],
            }],
            order: [
                [
                    { model: dbSchedule.schedule },
                    'scheduleDate',
                    'DESC'
                ]
            ],
            raw: true
        });

        //week calculation
        let td = new Date();
        let currentday = td.getDay();
        let weekstartday = new Date(td.getTime() - currentday * 24 * 60 * 60 * 1000);
        let weekendday = new Date(td.getTime() + (7 - (currentday + 1)) * 24 * 60 * 60 * 1000);
        let wStart = await hyphenDateFormat(weekstartday);
        let wEnd = await hyphenDateFormat(weekendday);

        //showing the necessary data
        let bookingArr = [];
        for (let booking of bookings) {
            booking['bookingName'] = booking['schedule.scheduleName'] == null ? booking['schedule.service.serviceName'] : booking['schedule.scheduleName'];
            for (let schedule of booking.attendanceType) {
                if (new Date(schedule.scdate) >= new Date(wStart) && new Date(schedule.scdate) <= new Date(wEnd)) {
                    let setObj = { status: 1, subscriptionId: booking.subscriptionId };
                    let session = await sessionInfo(information, setObj);
                    booking['sessionInfo'] = session[0].durationType == 1 ? session[0].attendedSession + '/' + session[0].totalSession : "" + session[0].attendedSession;
                    booking['sessionMessage'] = session[0].message;
                    bookingArr.push(booking);
                    break;
                }
            }
        }

        dbCSM.sequelize.close();
        dbServices.sequelize.close();
        dbSchedule.sequelize.close();
        return bookingArr;
    } catch (error) {
        throw error;
    }
}

export async function currentMonthSchedule__3_07(information, getObj) {
    try {
        let dbCSM = createInstance(["clientScheduleMap"], information);
        let dbSchedule = createInstance(["clientSchedule"], information);
        let dbServices = createInstance(["clientService"], information);
        let dbSubscription = createInstance(["clientSubscription"], information);
        let dbPricepack = createInstance(["clientPricepack"], information);

        dbCSM.clientschedulemap.belongsTo(dbSchedule.schedule, {
            foreignKey: "scheduleId",
            targetKey: "scheduleId"
        });
        dbSchedule.schedule.belongsTo(dbServices.services, {
            foreignKey: "serviceId",
            targetKey: "serviceId"
        });
        dbSubscription.subscriptions.belongsTo(dbPricepack.pricepacks, {
            foreignKey: "pricepacks",
            targetKey: "pricepackId"
        });

        let bookings = await dbCSM.clientschedulemap.findAll({
            // attributes:
            //     ['scheduleId'],
            // group: ["scheduleId"],
            where: getObj,
            include: [{
                model: dbSchedule.schedule,
                include: [{
                    model: dbServices.services,
                    attributes: ['serviceName']
                }],
                // order: orderBy
            }],
            // order: orderBy
            order: [
                [
                    { model: dbSchedule.schedule },
                    'scheduleDate',
                    'DESC'
                ]
            ],
            raw: true
        });


        //month calculation
        let date = new Date();
        let firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
        let lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
        let fday = await hyphenDateFormat(firstDay);
        let lday = await hyphenDateFormat(lastDay);

        //showing the necessary data
        let bookingArr = [];
        for (let booking of bookings) {
            let subscriptions = await dbSubscription.subscriptions.find({
                where: {
                    subscriptionId: booking.subscriptionId
                },
                include: [{
                    model: dbPricepack.pricepacks
                }],
                raw: true
            });
            booking['bookingName'] = booking['schedule.scheduleName'] == null ? booking['schedule.service.serviceName'] : booking['schedule.scheduleName'];
            for (let schedule of booking.attendanceType) {
                if (new Date(schedule.scdate) >= new Date(fday) && new Date(schedule.scdate) <= new Date(lday)) {
                    booking['sessionInfo'] = booking.sessionCount + '/' + subscriptions['pricepack.serviceDuration'];
                    bookingArr.push(booking);
                    break;
                }
            }
        }

        dbCSM.sequelize.close();
        dbServices.sequelize.close();
        dbSchedule.sequelize.close();
        dbSubscription.sequelize.close();
        dbPricepack.sequelize.close();
        return bookingArr;
    } catch (error) {
        throw error;
    }
}

export async function currentMonthSchedule(information, getObj) {
    try {
        let dbCSM = createInstance(["clientScheduleMap"], information);
        let dbSchedule = createInstance(["clientSchedule"], information);
        let dbServices = createInstance(["clientService"], information);

        dbCSM.clientschedulemap.belongsTo(dbSchedule.schedule, {
            foreignKey: "scheduleId",
            targetKey: "scheduleId"
        });
        dbSchedule.schedule.belongsTo(dbServices.services, {
            foreignKey: "serviceId",
            targetKey: "serviceId"
        });

        let bookings = await dbCSM.clientschedulemap.findAll({
            where: getObj,
            include: [{
                model: dbSchedule.schedule,
                include: [{
                    model: dbServices.services,
                    attributes: ['serviceName']
                }],
            }],
            order: [
                [
                    { model: dbSchedule.schedule },
                    'scheduleDate',
                    'DESC'
                ]
            ],
            raw: true
        });


        //month calculation
        let date = new Date();
        let firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
        let lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
        let fday = await hyphenDateFormat(firstDay);
        let lday = await hyphenDateFormat(lastDay);

        //showing the necessary data
        let bookingArr = [];
        for (let booking of bookings) {
            booking['bookingName'] = booking['schedule.scheduleName'] == null ? booking['schedule.service.serviceName'] : booking['schedule.scheduleName'];
            for (let schedule of booking.attendanceType) {
                if (new Date(schedule.scdate) >= new Date(fday) && new Date(schedule.scdate) <= new Date(lday)) {
                    let setObj = { status: 1, subscriptionId: booking.subscriptionId };
                    let session = await sessionInfo(information, setObj);
                    booking['sessionInfo'] = session[0].durationType == 1 ? session[0].attendedSession + '/' + session[0].totalSession : "" + session[0].attendedSession;
                    booking['sessionMessage'] = session[0].message;
                    bookingArr.push(booking);
                    break;
                }
            }
        }

        dbCSM.sequelize.close();
        dbServices.sequelize.close();
        dbSchedule.sequelize.close();
        return bookingArr;
    } catch (error) {
        throw error;
    }
}