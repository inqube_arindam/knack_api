export async function addPayment(information, db, getCond) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let clientpayments = await db.clientpayments.create(setCond);
        return clientpayments;
    } catch (error) {
        throw error;
    }
}

export async function viewPayment(information, db, getCond) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let clientpayments = await db.clientpayments.findAll({
            where: setCond,
            raw: true
        });
        return clientpayments;
    } catch (error) {
        throw error;
    }
}

export async function updatePayment(information, db, getCond, getData) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let clientpayments = await db.clientpayments.update(
            getData, {
                where: setCond
            }
        );
        return clientpayments;
    } catch (error) {
        throw error;
    }
}

export async function deletePayment(information, db, getCond) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let clientpayments = await db.clientpayments.update({
            status: 0,
            updateDateTime: Date.now()
        }, {
                where: setCond
            }
        );
        return clientpayments;
    } catch (error) {
        throw error;
    }
}