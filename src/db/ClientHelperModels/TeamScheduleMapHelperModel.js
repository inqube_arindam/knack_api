export async function addTeamScheduleMap(information, db, getCond) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let teamschedulemap = await db.teamschedulemap.create(setCond);
        return teamschedulemap;
    } catch (error) {
        throw error;
    }
}

export async function viewTeamScheduleMap(information, db, getCond) {
    console.log(information+','+db+','+getCond);
    try {
        
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let teamschedulemap = await db.teamschedulemap.findAll({
            where: setCond,
            raw: true
        });
        return teamschedulemap;
    } catch (error) {
        throw error;
    }
}

export async function updateTeamScheduleMap(information, db, getCond, getData) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let teamschedulemap = await db.teamschedulemap.update(
            getData, {
                where: setCond
            }
        );
        return teamschedulemap;
    } catch (error) {
        throw error;
    }
}

export async function deleteTeamScheduleMap(information, db, getCond) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let teamschedulemap = await db.teamschedulemap.update({
            status: 0,
            updateDateTime: Date.now()
        }, {
                where: setCond
            }
        );
        return teamschedulemap;
    } catch (error) {
        throw error;
    }
}

export async function countTeamScheduleMap(information, db, getCond) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let teamschedulemap = await db.teamschedulemap.findAndCountAll({
            where: setCond
          });
        return teamschedulemap.count;
    } catch (error) {
        throw error;
    }
}
