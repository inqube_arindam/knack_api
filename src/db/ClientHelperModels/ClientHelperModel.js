export async function addClient(information, db, getCond) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let clients = await db.clients.create(setCond);
        return clients;
    } catch (error) {
        throw error;
    }
}

export async function viewClient(information, db, getCond) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let clients = await db.clients.findAll({
            where: setCond,
            raw: true
        });
        return clients;
    } catch (error) {
        throw error;
    }
}

export async function updateClient(information, db, getCond, getData) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let clients = await db.clients.update(
            getData, {
                where: setCond
            }
        );
        return clients;
    } catch (error) {
        throw error;
    }
}

export async function deleteClient(information, db, getCond) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let clients = await db.clients.update({
            status: 0,
            updateDateTime: Date.now()
        }, {
                where: setCond
            }
        );
        return clients;
    } catch (error) {
        throw error;
    }
}