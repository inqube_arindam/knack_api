export async function addClientScheduleMap(information, db, getCond) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let clientschedulemap = await db.clientschedulemap.create(setCond);
        return clientschedulemap;
    } catch (error) {
        throw error;
    }
}

export async function viewClientScheduleMap(information, db, getCond) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let clientschedulemap = await db.clientschedulemap.findAll({
            where: setCond,
            raw: true
        });
        return clientschedulemap;
    } catch (error) {
        throw error;
    }
}

export async function updateClientScheduleMap(information, db, getCond, getData) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let clientschedulemap = await db.clientschedulemap.update(
            getData, {
                where: setCond
            }
        );
        return clientschedulemap;
    } catch (error) {
        throw error;
    }
}

export async function deleteClientScheduleMap(information, db, getCond) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let clientschedulemap = await db.clientschedulemap.update({
            status: 0,
            updateDateTime: Date.now()
        }, {
                where: setCond
            }
        );
        return clientschedulemap;
    } catch (error) {
        throw error;
    }
}

export async function countClientScheduleMap(information, db, getCond) {
    try {
        let getInfo = information.split(",");
        let dbBusinessId = getInfo[1]; //get db or business id from information
        let setDbid = { businessId: dbBusinessId };
        const setCond = Object.assign(getCond, setDbid);
        let clientschedulemap = await db.clientschedulemap.findAndCountAll({
            where: setCond
          });
        return clientschedulemap.count;
    } catch (error) {
        throw error;
    }
}
