import { route, successRoute } from "./";
import NotesDB from "../db/knackAdminNoteModel.js";

//Create New Lead
export const addNewNote = route(
  async (req, res) => {
    const {
      leadId,
      comment,
      created_by
    } = req.body;

    const notes = await NotesDB.createNewNote(
      leadId,
      comment,
      created_by
    );
    res.send(await successRoute(notes));
  }
);
//Delete notes
export const removeNote = route(
  async (req, res) => {
    const {
      notesId,
      leadId
    } = req.body;

    const notes = await NotesDB.deleteNote(
      notesId,
      leadId
    );
    res.send(await successRoute(notes));
  }
);

//List All Notes
export const listNotes = route(
  async (req,res) => {
    const{
      leadId
    } = req.body;

    const notes = await NotesDB.listNotes(leadId);
    res.send(await successRoute(notes));
  }
);
