import { route, successRoute } from "./";
import ActivityDB from "../db/knackAdminActivityModel.js";

//Create New Lead
export const addNewActivity = route(
  async (req, res) => {
    const {
      leadId,
      comment,
      created_by,
      creationDateTime
    } = req.body;

    const notes = await ActivityDB.createNewActivity(
      leadId,
      comment,
      created_by,
      creationDateTime
    );
    res.send(await successRoute(notes));
  }
);

//View all activity
export const viewActivity = route(
  async (req, res) => {
    const {
      leadId
    } = req.body;

    const activity = await ActivityDB.viewActivity(
      leadId
    );
    res.send(await successRoute(activity));
  }
);

export const getAllAssignee = route(async (req, res) => {
  //console.log('here');
  const allAssignee = await ActivityDB.getAllAssignee();
  //console.log('here');
  res.send(await successRoute(allAssignee));
});

export const saveActivity = route(
  async (req, res) => {
    const {
      activityId,
      leadId,
      activityType, 
      activityStatus, 
      activityReason, 
      otherReason, 
      activityComments, 
      activityAssignedTo,
      activityDate, 
      activityTime, 
      activityStep, 
      nextActivityType, 
      nextActivityReason, 
      nextActivityDate,
      nextActivityTime, 
      nextActivityReferal,
      nextActivityAssignedTo, 
      nextActivityComment, 
      nextActivityStatus
    } = req.body;
    //console.log(req.body);
    const activity = await ActivityDB.saveActivity(
      activityId,
      leadId,
      activityType, 
      activityStatus, 
      activityReason, 
      otherReason, 
      activityComments, 
      activityAssignedTo,
      activityDate, 
      activityTime, 
      activityStep, 
      nextActivityType, 
      nextActivityReason, 
      nextActivityDate,
      nextActivityTime,
      nextActivityReferal, 
      nextActivityAssignedTo, 
      nextActivityComment, 
      nextActivityStatus
    );
    //console.log(nextActivityStatus);
    res.send(await successRoute(activity));
  }
);

export const deleteActivity = route(
  async(req,res) => {
    const {activityId,leadId} = req.body;
    const activity = await ActivityDB.deleActivity(activityId,leadId);
    res.send(await successRoute(activity));
  }
);
