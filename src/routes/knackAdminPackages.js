import { route, successRoute } from "./";
import PackagesDB from "../db/knackAdminPackagesModel.js";

//Adding new Packages Details
export const create = route(
  async (req, res) => {
    const { name, details, duration, amount, referralDiscount, isGstApplicable, cgst, sgst, numberOfCenters, numberOfTeamMembers } = req.body;
    //console.log(duration);
    const newPackage = await PackagesDB.create(
      name,
      details,
      duration,
      amount,
      referralDiscount,
      isGstApplicable,
      cgst,
      sgst,
      numberOfCenters,
      numberOfTeamMembers
    );
    res.send(await successRoute(newPackage));
  },
  {
    requiredFields: ["name", "details", "duration", "amount", "numberOfCenters", "numberOfTeamMembers"]
  }
);

//Listing all Packages
export const listAll = route(async (req, res) => {
  const Packages = await PackagesDB.listAll();
  res.send(await successRoute(Packages));
});

//Returning detail of one Package
export const packageDetail = route(async (req, res) => {
  const Packages = await PackagesDB.packageDetail(req.params.packageId);
  res.send(await successRoute(Packages));
});

//Update Package's information
export const update = route(
  async (req, res) => {
    const { name, details, duration, amount, numberOfCenters } = req.body;
    const Packages = await PackagesDB.update(
      req.params.packageId,
      name,
      details,
      duration,
      amount,
      numberOfCenters
    );
    res.send(await successRoute(Packages));
  },
  {
    requiredFields: ["name", "details", "duration", "amount", "numberOfCenters"]
  }
);
//change package status
export const chagePackageStatus = route(async(req, res) => {
  const { status,packageId } = req.body;
  const Packages = await PackagesDB.updatePackageStatus(
    status,
    packageId
  );
  res.send(await successRoute(Packages));
});
//Search packages by matching word
export const searchPackages = route(async (req, res) => {
  //console.log('js file');
  /*if(req.params.packageName) {
    var searchPackageName = req.params.packageName;
  } else {
    var searchPackageName = null;
  }*/
  //console.log(req.params.packageName);
  const { packageName, centerNumber } = req.body;
  const packages = await PackagesDB.searchPackages(packageName,centerNumber);
  res.send(await successRoute(packages));
});
//Delete Package's information
export const remove = route(async (req, res) => {
  const Packages = await PackagesDB.remove(
    req.params.packageId.split(/\s*,\s*/)
  );
  res.send(await successRoute(Packages));
});
//Delete Package's information
export const removePackage = route(async (req, res) => {
  const { packageId } = req.body;
  const Packages = await PackagesDB.removePackage(packageId);
  res.send(await successRoute(Packages));
});
