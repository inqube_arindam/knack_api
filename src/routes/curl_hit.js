var mysql = require('mysql');
import { route, successRoute } from "./";
import loginSignUpDB from "../db/LoginSignupModel";
import { createInstance } from "../lib/common.js";
import request from "request";
import { KNACK_ALL_DB } from "../lib/constants.js";

export const getDatabase = route(
        async(req, res) => {
            const getDatabase = await loginSignUpDB.findAll();
            var data = JSON.parse(getDatabase);
            res.send(await successRoute(data));


            data.forEach((data, index) => {

                console.log("in for each");
                console.log(data.relatedDatabase);

                var con = mysql.createConnection({
                    host: KNACK_ALL_DB.host,
                    user: KNACK_ALL_DB.user,
                    password: KNACK_ALL_DB.password,
                    database: data.relatedDatabase
                });
                console.log("connected");

                // .... pricepack update/expire....
                con.query("update pricepacks set isExpired=1,status=0 where  DATE(expiryDate)=DATE(NOW())", function(err, result) {

                    if (err) throw err;
                    console.log(result);

                });

                // .....subscription expiry and notification.....
                con.query("select DATEDIFF(DATE(subscriptionDateTime),DATE(NOW())) as Diff,contactNumber,subscriptionId from subscriptions,clients where subscriptions.clientId=clients.clientId", async function(err, result) {
                    if (err) { console.log(err); } else {
                        var data = JSON.stringify(result);
                        var dataJSON = JSON.parse(data);


                        console.log(dataJSON);

                        dataJSON.forEach((dataJSON, index) => {

                            // console.log(dataJSON.Diff);
                            var days = dataJSON.Diff;
                            console.log(days);
                            console.log(dataJSON.contactNumber);
                            var id = dataJSON.subscriptionId;
                            console.log(dataJSON.subscriptionId);
                            console.log("---------");
                            if (days === 1) {
                                console.log("===========");

                                con.query("update subscriptions set Notification=1 where subscriptionId=" + id, async function(err, result) {
                                    if (err) { console.log(err) } else {
                                        // Send SMS
                                        console.log("updated 1");

                                        var sms_api = " http://api-alerts.solutionsinfini.com/v3/?method=sms&api_key=A31cf85c3cc3b65100bf9bd7fbe30cd90&to=+91_MOBILE&sender=SIDEMO&message=_MESSAGE";
                                        var url = "";
                                        var text = "Your subscription is going to end in days  " + days;
                                        url = sms_api.replace("_MOBILE", dataJSON.contactNumber);
                                        url = url.replace("_MESSAGE", encodeURIComponent(text));
                                        request(url, function(err) {
                                            if (err) {
                                                throw err;
                                            } else { console.log('message sent') }
                                        });
                                    }
                                });



                            } else if (days === 5) {
                                console.log("#######");
                                con.query("update subscriptions set Notification=5 where subscriptionId=" + id, async function(err, result) {
                                    if (err) { console.log(err) } else {
                                        //send sms
                                        console.log("updated 5");

                                        var sms_api = " http://api-alerts.solutionsinfini.com/v3/?method=sms&api_key=A31cf85c3cc3b65100bf9bd7fbe30cd90&to=+91_MOBILE&sender=SIDEMO&message=_MESSAGE";
                                        var url = "";
                                        var text = "Your subscription is going to end in days  " + days;
                                        url = sms_api.replace("_MOBILE", dataJSON.contactNumber);
                                        url = url.replace("_MESSAGE", encodeURIComponent(text));
                                        request(url, function(err) {
                                            if (err) {
                                                throw err;
                                            } else { console.log('message sent') }
                                        });
                                    }

                                });

                            }

                        }); // end of dataJSON


                    } // end of else of main select query
                    con.end();
                });

            }); // end of data.ForEach

        }

    ) //end of route.......