import { ApplicationError } from "../lib/errors";
import { decodeToken, generateToken } from "../lib/token";
import loginSignUpDB from "../db/LoginSignupModel";
import { route, successRoute } from "./";

export const authenticate = route(
  async (req, res, next) => {
    // TODO: this is a stub implementation that just checks if the password
    // is the same as the username. Implement real authentication, then return
    // user metadata in the JSON web token as you desire.
    const { emailId, password, FcmUserToken, IMEINumber } = req.body;

    const user = await loginSignUpDB.getLoginByCredentials(
      emailId,
      password,
      FcmUserToken,
      IMEINumber
    );
    if (!user[0]) {
      throw new ApplicationError("Invalid credentials", 401);
    }
    const token = await generateToken(user[0].businessId);
    user[0].token = await token;
    // send the response, along with the token to be used in the Bearer header
    // (see verify below)
    res.send(await successRoute(user));
  },
  {
    requiredFields: ["emailId", "password"]
  }
);

export const testpost = route(async (req, res, next) => {
  res.send(req.body);
});

// middleware that verifies that a token is present and is legitimate.
export const verify = async (req, res, next) => {
  const authHeader = req.headers.authorization;
  if (!authHeader) {
    next(
      new ApplicationError(
        "Missing Authorization header with Bearer token",
        401
      )
    );
    return;
  }

  // strip the leading "Bearer " part from the rest of the token string
  // const token = authHeader.substring("Bearer ".length);
  try {
    const decoded = await decodeToken(authHeader);
    const user = await loginSignUpDB.getUserById(decoded.id);
    if (!user[0].businessId) {
      // just triggers the catch block below, contents of error are
      // ignored
      throw new ApplicationError("Not found");
    }
    // res.locals.authData = decoded;
    next();
  } catch (err) {
    // assume failed decoding means bad token string
    next(new ApplicationError("Could not verify token", 401));
  }
};
