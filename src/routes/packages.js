import { route, successRoute } from "./";
import PackagesDB from "../db/PackagesModel.js";

//Adding new Packages Details
export const create = route(
  async (req, res) => {
    const { name, details, duration, amount, numberOfCenters, numberOfTeamMembers } = req.body;
    const newPackage = await PackagesDB.create(
      name,
      details,
      duration,
      amount,
      numberOfCenters,
      numberOfTeamMembers
    );
    res.send(await successRoute(newPackage));
  },
  {
    requiredFields: ["name", "details", "duration", "amount", "numberOfCenters","numberOfTeamMembers"]
  }
);

//Listing all Packages
export const listAll = route(async (req, res) => {
  const Packages = await PackagesDB.listAll();
  res.send(await successRoute(Packages));
});

//Returning detail of one Package
export const packageDetail = route(async (req, res) => {
  const Packages = await PackagesDB.packageDetail(req.params.packageId);
  res.send(await successRoute(Packages));
});

//Update Package's information
export const update = route(
  async (req, res) => {
    const { name, details, duration, amount, numberOfCenters } = req.body;
    const Packages = await PackagesDB.update(
      req.params.packageId,
      name,
      details,
      duration,
      amount,
      numberOfCenters
    );
    res.send(await successRoute(Packages));
  },
  {
    requiredFields: ["name", "details", "duration", "amount", "numberOfCenters"]
  }
);

//Delete Package's information
export const remove = route(async (req, res) => {
  const Packages = await PackagesDB.remove(
    req.params.packageId.split(/\s*,\s*/)
  );
  res.send(await successRoute(Packages));
});
