import { route, successRoute } from "./";
import LeadsDB from "../db/knackAdminLeadModel.js";

//Create New Lead
export const addNewLead = route(
  /*
  userId:user_Id, leadName:str.leadName, leadEmailId:str.leadEmail, leadContactNumber: str.leadContactNumber, leadAddress:str.leadAddress, leadPinNumber: str.leadPinNumber, leadBusinessName:str.leadBusinessName, leadBusinessExperience: str.leadBusinessExperience, leadBusinessBranches: str.leadBusinessBranches, leadBusinessNumOfClients: str.leadBusinessNumOfClients, leadBusinessNumOfTeamMembers: str.leadBusinessNumOfTeamMembers, leadBusinessAlternateNum: str.leadBusinessAlternateNum, leadBusinessAlternateAddress: str.leadBusinessAlternateAddress, leadBusinessWebsite: str.leadBusinessWebsite, leadType: str.leadType, leadStatus: str.leadStatus, leadAssignedTo: str.leadAssignedTo, leadPriority: str.leadPriority, leadManager: str.leadManager, leadBusinessNumOfCenters: str.leadBusinessNumOfCenters, leadReferalDetails: str.leadReferalDetails
  */
  async (req, res) => {
    const {
      userId,
      leadName,
      leadEmailId,
      leadContactNumber,
      leadAddress,
      leadPinNumber,
      leadBusinessName,
      leadBusinessExperience,
      leadBusinessBranches,
      leadBusinessNumOfClients,
      leadBusinessNumOfCenters,
      leadBusinessNumOfTeamMembers,
      leadBusinessAlternateNum,
      leadBusinessAlternateAddress,
      leadBusinessWebsite,
      leadType,
      leadStatus,
      leadAssignedTo,
      leadPriority,
      leadManager,
      leadReferalDetails
    } = req.body;
  
    const leads = await LeadsDB.addNewLead(
      userId,
      leadName,
      leadContactNumber,
      leadEmailId,
      leadAddress,
      leadPinNumber,
      leadBusinessName,
      leadBusinessExperience,
      leadBusinessBranches,
      leadBusinessNumOfClients,
      leadBusinessNumOfCenters,
      leadBusinessNumOfTeamMembers,
      leadBusinessAlternateNum,
      leadBusinessAlternateAddress,
      leadBusinessWebsite,
      leadType,
      leadStatus,
      leadAssignedTo,
      leadPriority,
      leadManager,
      leadReferalDetails
    );
    res.send(await successRoute(leads));
  }
);

//search leads by filtering
export const searchKnackLeads = route(
  async (req, res) => {
    const {
      searchUser,
      searchLocation,
      searchPriority,
      searchStatus,
      searchType,
      searchAssignedUser
    } = req.body;

    const leads = await LeadsDB.searchKnackLead(
      searchUser,
      searchLocation,
      searchPriority,
      searchStatus,
      searchType,
      searchAssignedUser
    );
    res.send(await successRoute(leads));
  }
);

//Update Lead information
export const updateLead = route(
  async (req, res) => {
    const {
      leadId,
      leadName,
      leadEmailId,
      leadContactNumber,
      leadAddress,
      leadPinNumber,
      leadBusinessName,
      leadBusinessExperience,
      leadBusinessBranches,
      leadBusinessNumOfClients,
      leadBusinessCenters,
      leadBusinessNumOfTeamMembers,
      leadBusinessAlternateNum,
      leadBusinessAlternateAddress,
      leadBusinessWebsite,
      leadType,
      leadStatus,
      leadAssignedTo,
      leadPriority,
      leadManager,
      leadReferalDetails
    } = req.body;
    //console.log(req.body);
    const leads = await LeadsDB.updateLead(
      leadId,
      leadName,
      leadEmailId,
      leadContactNumber,
      leadAddress,
      leadPinNumber,
      leadBusinessName,
      leadBusinessExperience,
      leadBusinessBranches,
      leadBusinessNumOfClients,
      leadBusinessCenters,
      leadBusinessNumOfTeamMembers,
      leadBusinessAlternateNum,
      leadBusinessAlternateAddress,
      leadBusinessWebsite,
      leadType,
      leadStatus,
      leadAssignedTo,
      leadPriority,
      leadManager,
      leadReferalDetails
    );
    res.send(await successRoute(leads));
  }
);

//Get all leads
export const listleads = route(
  async (req, res) => {
    const leads = await LeadsDB.listleads();
    res.send(await successRoute(leads));
  }
);

//view lead
export const viewLead = route(
  async (req, res) => {
    const { leadId } = req.body;
    //console.log(leadId);
    const leads = await LeadsDB.viewLead(leadId);
    res.send(await successRoute(leads));
  });
//get lead data
export const getLeadServey = route(
  async(req,res)=>{
    const {leadId} = req.body;
    const getLeadServey  = await LeadsDB.getLeadData(leadId);
    res.send(await successRoute(getLeadServey));
});
//save servey data
export const saveServeyData = route(
  async(req,res)=>{
    const {leadId,profession,gender,ans1,ans2,ans3,ans4,ans5,ans6,ans7,ans8,ans9,ans10,ans11,ans12,ans13,ans14,ans15,ans16} = req.body;
    const serveyAnswer  = await LeadsDB.serveyResult(leadId,profession,gender,ans1,ans2,ans3,ans4,ans5,ans6,ans7,ans8,ans9,ans10,ans11,ans12,ans13,ans14,ans15,ans16);
    res.send(await successRoute(serveyAnswer));
});
//Delete business information
export const deleteLeads = route(async (req, res) => {
  const { leadId }  = req.body;
  const leads     = await LeadsDB.removeLeads(leadId);
  res.send(await successRoute(leads));
});
