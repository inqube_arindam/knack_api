import { route, successRoute } from "./";
import BusinessDB from "../db/BusinessModel";

//Listing business details based on passed ID
export const details = route(async(req, res) => {
    const business = await BusinessDB.details(
        req.headers.information
    );
    res.send(await successRoute(business));
});

//Update business information
export const update = route(
    async(req, res) => {
        const {
            businessName,
            businessType,
            numberOfClients,
            numberOfCenters,
            numberOfTeamMembers,
            contactNumber,
            emailId,
            area,
            pin,
            city,
            landmark,
            business_days
        } = req.body;
        if (req.files) var photoUrl = req.files.user_profile;
        else var photoUrl = null;

        const business = await BusinessDB.update(
            req.headers.information,
            req.params.businessId,
            businessName,
            businessType,
            numberOfClients,
            numberOfCenters,
            numberOfTeamMembers,
            contactNumber,
            emailId,
            area,
            pin,
            city,
            landmark,
            photoUrl,
            business_days
        );

        res.send(await successRoute(business));
    }, {
        requiredFields: [
            "businessName",
            "businessType",
            "numberOfCenters",
            "numberOfTeamMembers",
            "contactNumber",
            "emailId",
            "area",
            "pin",
            "city",
            "landmark",
            "business_days"
        ]
    }
);

//Delete business information
export const remove = route(async(req, res) => {
    const business = await BusinessDB.remove(
        req.params.businessId.split(/\s*,\s*/)
    );
    res.send(await successRoute(business));
});