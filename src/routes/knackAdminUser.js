import { route, successRoute } from "./";
import KnackUserDB from "../db/knackAdminUserModel";

//login check for knack admin
export const loginKnackAdmin = route(async (req, res) => {
    const { emailId,password,FcmUserToken,IMEINumber } = req.body;
    const Users = await KnackUserDB.loginKnackAdmin(emailId,password,FcmUserToken,IMEINumber);
    res.send(await successRoute(Users));
});

//Listing all Knack User
export const listAllKnackUser = route(async (req, res) => {
    //console.log("businessId "+businessId);
    const Users = await KnackUserDB.listAllKnackUser(req.params.businessId);    
    res.send(await successRoute(Users));
});
//Block Knack User
export const blockKnackAdminUser = route(async (req, res) => {
    const { userId,status } = req.body;
    const Users = await KnackUserDB.blockAdminUser(userId,status);
    res.send(await successRoute(Users));
});

//Activate Knack User
export const activateKnackAdminUser = route(async (req, res) => {
    const Users = await KnackUserDB.activateAdminUser(req.headers.information,req.params.userId);
    //console.log(Users);
    res.send(await successRoute(Users));
});

//Search Admin User by Name
export const searchKnackAdminUser = route(async (req, res) => {
    const Users = await KnackUserDB.searchKnackAdminUser(
        req.headers.information,
        req.body.searchUser,
        req.body.searchUserStatus,
        req.body.searchMOP,
        req.body.searchDueDate
    );
    //console.log(Users);
    res.send(await successRoute(Users));
});

//get knack user previous subscription
export const getPrevSubs = route(async (req, res) => {
    const { userId } = req.body;
    //console.log('UserID = '+userId);
    const Users = await KnackUserDB.getAllPrevSubs(userId);
    //console.log(Users);
    res.send(await successRoute(Users));
});


