import { route, successRoute } from "./";
import { hashPassword, comparePassword } from "../lib/crypto";
import KnackUserDB from "../db/knackAdminUserModel";

//Listing all Knack User
export const listAllKnackTeam = route(async (req, res) => {
    const knackTeamUsers = await KnackUserDB.listAllKnackTeam();
    //console.log(knackTeamUsers);
    res.send(await successRoute(knackTeamUsers));
});

export const addNewTeamMember = route(async (req, res) => {
    const { name, emailId, contactNumber, password, role } = req.body;
    const newLoginUser = await KnackUserDB.createNewTeam(
        name, emailId, contactNumber, password, role
    );
    res.send(await successRoute(newLoginUser));

    //console.log(emailId);
});

//Add New Team Member
/*export const addNewTeamMember = route(async (req, res) => {
    const { name, emailId, contactNumber, password, role } = req.body;
    console.log(emailId);
    const newLoginUser = {
        name,
        emailId,
        contactNumber,
        password,
        role
    };
    try {
    let LoginUser = await this.model
        .create(newLoginUser)
        .catch(this.db.ValidationError, function(error) {
        var showError = error["errors"];
        //throw error["errors"];
        throw new ApplicationError(showError[0].message, 409);
    });
    /*sendEmail(
        emailId,
        "Verification code from Knack",
        "Your verification code is " + evcode
    );*/
    //sendSms(contactNumber, vcode);
   /* return await filterFields(LoginUser, PUBLIC_FIELDS, true);
    } catch (error) {
    // throw new ApplicationError(error.name, error.parent.errno);
    throw error;
    }
    //res.send(await successRoute(newPackage));
});*/

//Block Knack User
export const changeMemberStatus = route(async (req, res) => {
    //console.log(req.params);    
    //console.log('here');
    console.log(req.body);
    const { status, userId } = req.body;
    const Users = await KnackUserDB.changeMemberStatus(status,userId);    
    res.send(await successRoute(Users));
});

//Edit Knack Team Member
export const editTeamMember = route(async (req, res) => {
    const { userId,teamMemberName,teamMemberRole,teamMemberContactNumber } = req.body;
    //console.log(teamId);
    const editAdminUser = await KnackUserDB.editTeamMemberDetails(userId,teamMemberName,teamMemberRole,teamMemberContactNumber);
    res.send(await successRoute(editAdminUser));
});

//Activate Knack User
export const activateKnackAdminUser = route(async (req, res) => {
    const Users = await KnackUserDB.activateAdminUser(req.headers.information,req.params.userId);
    res.send(await successRoute(Users));
});
//get team records
export const getTeamRecords =  route(async(req, res) => {
    const TeamMembers = await KnackUserDB.getTeamRecords(req.params.userId);
    res.send(await successRoute(TeamMembers));
});
//Search Admin User by Name
export const getKnackTeamMember = route(async (req, res) => {
    const { name, lastLoginDateSearch } = req.body;
    //console.log('here');console.log(req.body);
    const teamMembers = await KnackUserDB.searchAllKnackTeamMember(
        req.headers.information,
        req.body.name,
        req.body.lastLoginDateSearch
    );
    //console.log(teamMembers);
    res.send(await successRoute(teamMembers));
});
export const getTeamMember = route(async (req,res) => {
    const { userId } = req.body;
    const teamMembers = await KnackUserDB.getTeamMemberDetails(
        userId
    );
    //console.log(teamMembers);
    res.send(await successRoute(teamMembers));
});

// export const editTeamMember = route(async (req,res)=>{
//     const { userId,teamMemberName,teamMemberRole,teamMemberContactNumber } = req.body;
//     const teamMembers = await KnackUserDB.editTeamMemberDetails(userId,teamMemberName,teamMemberRole,teamMemberContactNumber);
//     //console.log(teamMembers);
//     res.send(await successRoute(teamMembers));
// });


