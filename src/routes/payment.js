import { route, successRoute } from "./";
import PaymentDB from "../db/PaymentModel.js";

var async = require('async');
var cron = require('node-cron');

//Adding new Payment Details
export const create = route(
  async (req, res) => {
    const {
      packageId,
      userId,
      businessId,
      invoiceId,
      paymentMode,
      details,
      centerNumber,
      amountDate,
      amount
    } = req.body;
    const newpayment = await PaymentDB.create(
      packageId,
      userId,
      businessId,
      invoiceId,
      paymentMode,
      details,
      centerNumber,
      amountDate,
      amount
    );
    res.send(await successRoute(newpayment));
  },
  {
    requiredFields: [
      "packageId",
      "paymentMode",
      "userId",
      "businessId",
      "centerNumber",
      "amountDate"
    ]
  }
);

//User Invoide Data
export const userInvoice = route(async (req, res) => {
  const {
    packageId,
    userId,
    businessId,
    centerNumber,
  } = req.body;
  const Invoice = await PaymentDB.userInvoice(
    packageId,
    userId,
    businessId,
    centerNumber,
  );
  res.send(successRoute(Invoice));
});

// Renew subscription
export const renewSubscriptionPayment = route(
  async (req, res) => {
    const {
      paymentId,
      packageId,
      userId,
      businessId,
      invoiceId,
      paymentMode,
      amount,
      centerNumber,
      amountDate,
      details
    } = req.body;
    const Payment = await PaymentDB.renewSubscriptionPayment(
      req.params.paymentId,
      packageId,
      userId,
      businessId,
      invoiceId,
      paymentMode,
      amount,
      centerNumber,
      amountDate,
      details
    );
    res.send(successRoute(Payment));
  },
  {
    requiredFields: [
      "packageId",
      "paymentMode",
      "userId",
      "businessId",
      "centerNumber",
      "amountDate"
    ]
  }
);

//Listing all Payment
export const listAll = route(async (req, res) => {
  const Payments = await PaymentDB.listAll(req.params.businessId);
  res.send(successRoute(Payments));
});

//Returning payment detail of Bussiness Id
export const listAllBussiness = route(async (req, res) => {
  const Payments = await PaymentDB.listAllBussiness(req.params.businessId);
  res.send(successRoute(Payments));
});
//Returning detail of one Payment
export const paymentDetail = route(async (req, res) => {
  const Payments = await PaymentDB.paymentDetail(req.params.paymentId);
  res.send(successRoute(Payments));
});

//Update Payment's information
export const update = route(
  async (req, res) => {
    const {
      packageId,
      userId,
      businessId,
      paymentMode,
      amount,
      details
    } = req.body;
    const Payment = await PaymentDB.update(
      req.params.paymentId,
      packageId,
      userId,
      businessId,
      paymentMode,
      amount,
      details
    );
    res.send(successRoute(Payment));
  },
  {
    requiredFields: [
      "packageId",
      "paymentMode",
      "amount",
      "userId",
      "businessId"
    ]
  }
);

//Delete Payment's information
export const remove = route(async (req, res) => {
  const Payment = await PaymentDB.remove(req.params.paymentId.split(/\s*,\s*/));
  res.send(successRoute(Payment));
});

export const membershipDetails = route(async (req, res) => {
  const {
    userId,
    businessId
  } = req.body;
  const membership = await PaymentDB.membershipDetails(
    userId,
    businessId
  );
  res.send(successRoute(membership));

})

//every 12 hours
cron.schedule('0 */12 * * *', function () {
  PaymentDB.allPayments().then((payments) => {
    console.log("Payments : ", payments);
    async.each(payments, function (payment, cb) {
      if (payment.Diff == 5  || payment.Diff == 3 ||  payment.Diff == 1) {
        // send notification
        PaymentDB.sendMessage(payment, 'PLAN').then((res) => {
          cb();
        })
      }

      //TRIAL PERIOD CHECKING
      if (payment.trialEndsIn == 5 || payment.trialEndsIn == 1) {
        PaymentDB.sendMessage(payment, 'TRIAL').then((res) => {
          cb();
        })
      }

    }, function (err) {
      if (err) {
        console.log("Error : ", err);
      } else {
        console.log("Cron executed successfully");
      }
    })
  })

});