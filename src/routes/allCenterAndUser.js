import { route, successRoute } from "./";
import AllCenterAndUserDB from "../db/allCenterAndUserModel";

//Listing all the Center of a business
export const listAll = route(async (req, res) => {
    const centers = await AllCenterAndUserDB.listAll(
      req.headers.information,
      req.params.businessId
    );
    res.send(await successRoute(centers));
  });