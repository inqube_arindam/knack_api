import { route, successRoute } from "../";
import UserDB from "../../db/userModel/userDashboardModel";

//Listing details of a business
export const userDashboard = route(async (req, res) => {
  const user = await UserDB.userDashboard(
    req.headers.information,
    req.params.businessId
  );
  res.send(await successRoute(user));
});

export const userProfile = route(async (req, res) => {
  const user = await UserDB.userProfile(
    req.headers.information,
    req.params.businessId
  );
  res.send(await successRoute(user));
});

//UserDashboard.js
export const notification = route(async (req, res) => {
  const user = await UserDB.notification(
    req.headers.information,
  );
  res.send(await successRoute(user));
});

//view the centres depending on users
export const viewCenter = route(async (req, res) => {
  const user = await UserDB.viewCenter(
    req.headers.information,
  );
  res.send(await successRoute(user));
});