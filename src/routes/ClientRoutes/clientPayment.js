import {
    route,
    successRoute
} from "../";
import PendingpaymentDB from "../../db/ClientModels/ClientPaymentModel";
var cron = require('node-cron');
var async = require('async');
import BusinessDB from "../../db/BusinessModel";

//Adding Client Pending Payment Details
// export const create = route(
//   async (req, res) => {
//     const {
//       businessId,
//       subscriptionId,
//       pricepacks,
//       totalAmountPaid,
//       payment_mode,
//       cheque_number,
//       payment_date
//     } = req.body;
//     const pendingPayment = await PendingpaymentDB.create(
//       req.headers.information,
//       businessId,
//       subscriptionId,
//       pricepacks,
//       totalAmountPaid,
//       payment_mode,
//       cheque_number,
//       payment_date
//     );
//     res.send(await successRoute(pendingPayment));
//   },
//   {
//     requiredFields: [
//       "businessId",
//       "subscriptionId",
//       "pricepacks",
//       "totalAmountPaid",
//       "payment_mode",
//       "payment_date"
//     ]
//   }
// );

//Adding Client Pending Payment Details
export const create = route(
    async(req, res) => {
        const {
            clientpaymentId,
            businessId,
            subscriptionId,
            pricepacks,
            totalAmountPaid,
            payment_mode,
            cheque_number,
            payment_date
        } = req.body;
        const pendingPayment = await PendingpaymentDB.create(
            req.headers.information,
            clientpaymentId,
            businessId,
            subscriptionId,
            pricepacks,
            totalAmountPaid,
            payment_mode,
            cheque_number,
            payment_date
        );
        res.send(await successRoute(pendingPayment));
    }, {
        requiredFields: [
            "clientpaymentId",
            "businessId",
            "subscriptionId",
            "pricepacks",
            "totalAmountPaid",
            "payment_mode",
            "payment_date"
        ]
    }
);

//Listing all the Subscribed Pending Payments of a business
export const listAll = route(async(req, res) => {
    const pendingpayments = await PendingpaymentDB.listAll(
        req.headers.information,
        req.params.businessId,
        req.params.typeid
    );
    res.send(await successRoute(pendingpayments));
});

// Payment Reminders
export const listPaymentReminders = route(async(req, res) => {
    const paymentsreminders = await PendingpaymentDB.listPaymentReminders(
        req.headers.information,
        req.params
    );
    res.send(await successRoute(paymentsreminders));
});


export const clientPaymentRemindersList = route(async(req, res) => {
    const paymentsreminders = await PendingpaymentDB.clientPaymentRemindersList(
        req.headers.information,
        req.params.clientId,
        req.params
    );
    res.send(await successRoute(paymentsreminders));
});


// Payment History
export const listClientpaymentHistory = route(async(req, res) => {
    const clientpaymenthistory = await PendingpaymentDB.listClientpaymentHistory(
        req.headers.information,
        req.params
    );
    res.send(await successRoute(clientpaymenthistory));
});

// Client Payment History
export const clientPaymenthistoryList = route(async(req, res) => {
    const clientpaymenthistory = await PendingpaymentDB.clientPaymenthistoryList(
        req.headers.information,
        req.params.clientId,
        req.params
    );
    res.send(await successRoute(clientpaymenthistory));
});

export const listRecentIncome = route(async(req, res) => {
    const pendingpayments = await PendingpaymentDB.listRecentIncome(
        req.headers.information,
        req.params
    );
    res.send(await successRoute(pendingpayments));
});

//Listing all the Pending Payments of a Client
// export const clientPendingpayments = route(async (req, res) => {
//   const pendingpayments = await PendingpaymentDB.clientPendingpayments(
//     req.headers.information,
//     req.params.clientId
//   );
//   res.send(await successRoute(pendingpayments));
// });

//Returning details Payment info of one Subscription
export const details = route(async(req, res) => {
    const pendingpayments = await PendingpaymentDB.details(
        req.headers.information,
        req.params.subscriptionId
    );
    res.send(await successRoute(pendingpayments));
});

//Update Client Payment's information
export const update = route(
    async(req, res) => {
        const {
            clientId,
            businessId,
            centerId,
            services,
            pricepacks,
            totalSessionUnit,
            totalSessionType,
            offerId,
            taxGST,
            taxSGST,
            totalAmount,
            paymentType,
            numberOfInstallments,
            paymentDueDate,
            subscriptionDateTime
        } = req.body;
        const pendingpayments = await PendingpaymentDB.update(
            req.headers.information,
            req.params.subscriptionId,
            clientId,
            businessId,
            centerId,
            services,
            pricepacks,
            totalSessionUnit,
            totalSessionType,
            offerId,
            taxGST,
            taxSGST,
            totalAmount,
            paymentType,
            numberOfInstallments,
            paymentDueDate,
            subscriptionDateTime
        );
        res.send(await successRoute(pendingpayments));
    }, {
        requiredFields: [
            "clientId",
            "businessId",
            "centerId",
            "services",
            "pricepacks",
            "totalSessionUnit",
            "totalSessionType",
            "totalAmount",
            "paymentType",
            "subscriptionDateTime"
        ]
    }
);

//Delete Client Payment's information
export const remove = route(async(req, res) => {
    const pendingpayments = await PendingpaymentDB.remove(
        req.headers.information,
        req.params.subscriptionId.split(/\s*,\s*/)
    );
    res.send(await successRoute(pendingpayments));
});

// Payment Dashboard
export const expenseIncomeDashboard = route(async(req, res) => {
    const dashBoard = await PendingpaymentDB.expenseIncomeDashboard(
        req.headers.information
    );
    res.send(await successRoute(dashBoard));
});

export const clientPaymentdetails = route(async(req, res) => {
    const pendingpayments = await PendingpaymentDB.clientPaymentdetails(
        req.headers.information,
        req.params.clientpaymentId,
        req.params
    );
    res.send(await successRoute(pendingpayments));
});


export const paymentReminderAction = route(
    async(req, res) => {
        const {
            clientpaymentId,
            reminderType,
        } = req.body;
        const paymentReminderAction = await PendingpaymentDB.paymentReminderAction(
            req.headers.information,
            clientpaymentId,
            reminderType,
        );
        res.send(await successRoute(paymentReminderAction));
    }
);

cron.schedule('10 0 * * *', async function() {
    // cron.schedule('*/10 * * * *', async function() {
    console.log('Cron run......');
    BusinessDB.getAllBusinessIds().then((businessids) => {
        async.each(businessids.rows, function(business, cb) {
            let information = 'localhost,' + business.dataValues.businessId;
            PendingpaymentDB.paymentReminderActionDb(
                information
            ).then((res) => {
                if (res.length > 0) {
                    if (res[0].clientpaymentId) {
                        let postData = {
                            "clientpaymentId": res[0].clientpaymentId,
                            "reminderType": "0"
                        };
                        PendingpaymentDB.paymentReminderAction(
                            information,
                            postData.clientpaymentId,
                            postData.reminderType,
                        ).then((res) => {
                            console.log(res);
                            cb();
                        });

                    }
                }
            });
        }, function(err) {
            if (err) {
                console.log("Error : ", err);
            } else {
                console.log("Cron executed successfully");
            }
        })
    })
});