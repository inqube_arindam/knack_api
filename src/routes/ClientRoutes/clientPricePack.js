import { route, successRoute } from "../";
import PricepackDB from "../../db/ClientModels/ClientPricePackModel";
import LoginSignupModel from "../../db/LoginSignupModel";
import ScheduleDB from "../../db/ClientModels/clientScheduleModel";
import { createInstance, RemainingClientsOfService } from "../../lib/common";
var cron = require('node-cron');
//----------------Cron----------------------------
var requiredFields = [
    "businessId",
    "serviceId",
    "pricepackName",
    "amount",
    "serviceDuration",
    "durationType",
    "pricepackType"
];
//Adding new PricePack Details
export const create = route(
    async(req, res) => {
        const {
            businessId,
            serviceId,
            pricepackName,
            amount,
            serviceDuration,
            durationType,
            pricepackValidity,
            pricepackType,
            details
        } = req.body;

        const newService = await PricepackDB.create(
            req.headers.information,
            businessId,
            serviceId,
            pricepackName,
            amount,
            serviceDuration,
            durationType,
            pricepackValidity,
            pricepackType,
            details
        );
        res.send(await successRoute(newService));
    }, {
        requiredFields: requiredFields
    }
);

//Listing all the Service of a business
export const listAll = route(async(req, res) => {
    const pricepacks = await PricepackDB.listAll(
        req.headers.information,
        req.params
    );
    res.send(await successRoute(pricepacks));
});

//Listing all the pricepacks of a Center
export const listAllcenter = route(async(req, res) => {
    const pricepacks = await PricepackDB.listAllcenter(
        req.headers.information,
        req.params.centerId
    );
    res.send(await successRoute(pricepacks));
});

//Listing all the pricepacks of an offer
export const listAlloffer = route(async(req, res) => {
    const pricepacks = await PricepackDB.listAlloffer(
        req.headers.information,
        req.params.serviceId
    );
    res.send(await successRoute(Service));
});

//Returning detail of one pricepacks
export const PricePackDetails = route(async(req, res) => {
    const pricepacks = await PricepackDB.PricePackDetails(
        req.headers.information,
        req.params.pricepackId
    );
    res.send(await successRoute(pricepacks));
});

//Update Pricepack's information
export const update = route(async(req, res) => {
    const { pricepackId, businessId, pricepackName, details } = req.body;
    const pricepacks = await PricepackDB.update(
        req.headers.information,
        req.params.pricepackId,
        businessId,
        pricepackName,
        details
    );
    res.send(await successRoute(pricepacks));
});

//Expire Pricepack's information
export const expirePricepack = route(async(req, res) => {
    const pricepacks = await PricepackDB.expirePricepack(
        req.headers.information,
        req.params.pricepackId.split(/\s*,\s*/),
        req.params.expiryDate
    );
    res.send(await successRoute(pricepacks));
});

//Delete Pricepack's information
export const remove = route(async(req, res) => {
    const pricepacks = await PricepackDB.remove(
        req.headers.information,
        req.params.pricepackId.split(/\s*,\s*/)
    );
    res.send(await successRoute(pricepacks));
});

//Listing all the pricepacks of a service
export const pricepackListByServiceId = route(async(req, res) => {

    const pricepacks = await PricepackDB.pricepackListByServiceId(
        req.headers.information,
        req.params
    );
    res.send(await successRoute(pricepacks));
});

// cron setup (At 8:00:00 [0 8 * * *])
cron.schedule('0 * * * *', async() => {
    let RelatedDatabase = await LoginSignupModel.findAll();
    var data = JSON.parse(RelatedDatabase);
    data.forEach(async(data, index) => {
        let connection = 'localhost,' + data.businessId;
        let x = await ScheduleDB.scheduleDashboard(connection);
        let NotificationText = 'You have ' + x[0]["booking"][0]["pendingAttendance"] + ' attendance records pending. Mark them before it gets too late!';
        //Log Entry......
        // const newLog = {
        //     businessId: data.businessId,
        //     activity: {
        //         header: 'Pending Attendance Record',
        //         Status: 2,
        //         UserStatus: 0,
        //         NotificationText: NotificationText,
        //         activityDate: new Date(),
        //         activityTime: new Date(),
        //         message: 'Client Rescheduled',
        //     },
        //     referenceTable: 'clientScheduleMap',
        // };
        let dbLog = createInstance(["log"], connection);
        //let log = await dbLog.log.create(newLog);
        await RemainingClientsOfService(connection); //unscheduled client
        PricepackDB.cronFunction(connection);
    });
})