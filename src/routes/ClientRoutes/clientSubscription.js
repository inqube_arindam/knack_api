import { route, successRoute } from "../";
import SubscriptionDB from "../../db/ClientModels/ClientSubscriptionModel";

//Adding new Subscription Details
export const create = route(
    async(req, res) => {
        const {
            clientId,
            businessId,
            pricepacks,
            payment_type,
            numberOfInstallments,
            installment_frequency,
            due_amount,
            installment_due_on,
            payment_due_date
        } = req.body;
        const newSubscription = await SubscriptionDB.create(
            req.headers.information,
            clientId,
            businessId,
            pricepacks,
            payment_type,
            numberOfInstallments,
            installment_frequency,
            due_amount,
            installment_due_on,
            payment_due_date
        );
        res.send(await successRoute(newSubscription));
    }, {
        requiredFields: [
            "clientId",
            "businessId",
            "pricepacks",
            "payment_type",
            "payment_due_date",
            //"installment_frequency",
            //"numberOfInstallments"
        ]
    }
);

//Listing all the current subscriptions
export const currentSubscription = route(async(req, res) => {
    const subscriptions = await SubscriptionDB.currentSubscription(
        req.headers.information,
        req.params
    );
    res.send(await successRoute(subscriptions));
});

//Listing all the past subscriptions
export const pastSubscription = route(async(req, res) => {
    const subscriptions = await SubscriptionDB.pastSubscription(
        req.headers.information,
        req.params
    );
    res.send(await successRoute(subscriptions));
});


//Update Subscription's information, Subcription Settings
export const settings1 = route(
    async(req, res) => {
        const {
            subscriptionId,
            businessId,

            payment_type,
            installment_frequency,
            numberOfInstallments,
            due_amount,
            installment_due_on,
            payment_due_date,

            dateOfJoining,
            totalSessionsAttended,
            amountPaidSoFar
        } = req.body;

        const subscriptions = await SubscriptionDB.settings1(
            req.headers.information,
            req.params.subscriptionId,
            businessId,

            payment_type,
            installment_frequency,
            numberOfInstallments,
            due_amount,
            installment_due_on,
            payment_due_date,

            dateOfJoining,
            totalSessionsAttended,
            amountPaidSoFar
        );
        res.send(await successRoute(subscriptions));
    }
);



//Update Subscription's information, Subcription Settings
export const settings = route(
    async(req, res) => {
        const {
            subscriptionId,
            clientId,
            businessId,
            pricepacks,
            payment_type,
            numberOfInstallments,
            installment_frequency,
            due_amount,
            installment_due_on,
            payment_due_date,
            dateOfJoining,
            totalSessionsAttended,
            amountPaidSoFar
        } = req.body;

        const subscriptions = await SubscriptionDB.settings(
            req.headers.information,
            req.params.subscriptionId,
            clientId,
            businessId,
            pricepacks,
            payment_type,
            numberOfInstallments,
            installment_frequency,
            due_amount,
            installment_due_on,
            payment_due_date,
            dateOfJoining,
            totalSessionsAttended,
            amountPaidSoFar
        );
        res.send(await successRoute(subscriptions));
    }
);

export const renewlSubscription = route(
    async(req, res) => {
        const {
            businessId,
            subscriptionId,
            pricepacks,
            services,
            renewl_date,
            amount_paid,
        } = req.body;
        const subscriptions = await SubscriptionDB.renewlSubscription(
            req.headers.information,
            req.params.businessId,
            subscriptionId,
            pricepacks,
            services,
            renewl_date,
            amount_paid,
        );
        res.send(await successRoute(subscriptions));
    }
);

//Listing all the Subscriptions with corresponding schedule details of a Client
export const listOfPricepacksUnderService = route(async(req, res) => {
    const subscriptions = await SubscriptionDB.listOfPricepacksUnderService(
        req.headers.information,
        req.params.serviceId
    );
    res.send(await successRoute(subscriptions));
});

//Search subscriptions based on clients' namee, Pricepack and services
export const searchSubscription = route(async(req, res) => {
    const subscriptions = await SubscriptionDB.searchSubscription(
        req.headers.information,
        req.params.businessId,
        req.params.searchKeyword,
    );
    res.send(await successRoute(subscriptions));
});

//Listing all the Subscriptions of a business
export const listAll = route(async(req, res) => {
    const subscriptions = await SubscriptionDB.listAll(
        req.headers.information,
        req.params.businessId
    );
    res.send(await successRoute(subscriptions));
});
//Listing all the Subscriptions of a Center
export const list = route(async(req, res) => {
    const subscriptions = await SubscriptionDB.list(
        req.headers.information,
        req.params.centerId
    );
    res.send(await successRoute(subscriptions));
});

//Listing all the Subscriptions of a Client
export const clientSubscription = route(async(req, res) => {
    const subscriptions = await SubscriptionDB.clientSubscription(
        req.headers.information,
        req.params.clientId
    );
    res.send(await successRoute(subscriptions));
});

//Returning detail of one Subscription
export const details = route(async(req, res) => {
    const subscriptions = await SubscriptionDB.details(
        req.headers.information,
        req.params.subscriptionId
    );
    res.send(await successRoute(subscriptions));
});

//Update Subscription's information
export const update = route(
    async(req, res) => {
        const {
            clientId,
            businessId,
            centerId,
            services,
            pricepacks,
            totalSessionUnit,
            totalSessionType,
            offerId,
            taxGST,
            taxSGST,
            totalAmount,
            paymentType,
            numberOfInstallments,
            paymentDueDate,
            subscriptionDateTime
        } = req.body;
        const subscriptions = await SubscriptionDB.update(
            req.headers.information,
            req.params.subscriptionId,
            clientId,
            businessId,
            centerId,
            services,
            pricepacks,
            totalSessionUnit,
            totalSessionType,
            offerId,
            taxGST,
            taxSGST,
            totalAmount,
            paymentType,
            numberOfInstallments,
            paymentDueDate,
            subscriptionDateTime
        );
        res.send(await successRoute(subscriptions));
    }, {
        requiredFields: [
            "clientId",
            "businessId",
            "centerId",
            "services",
            "pricepacks",
            "totalSessionUnit",
            "totalSessionType",
            "paymentType",
            "subscriptionDateTime"
        ]
    }
);

//Delete Subscription's information
export const remove = route(async(req, res) => {
    const subscriptions = await SubscriptionDB.remove(
        req.headers.information,
        req.params.subscriptionId.split(/\s*,\s*/)
    );
    res.send(await successRoute(subscriptions));
});

//Listing all the Subscriptions with corresponding schedule details of a Client
export const listAllSubscriptionSchedule = route(async(req, res) => {
    const subscriptions = await SubscriptionDB.listAllSubscriptionSchedule(
        req.headers.information,
        req.params.businessId,
        req.params.clientId
    );
    res.send(await successRoute(subscriptions));
});

export const listrenewalHistorySubcrition = route(async(req, res) => {
    const subscriptions = await SubscriptionDB.listrenewalHistorySubcrition(
        req.headers.information,
        req.params.serviceId,
        req.params
    );
    res.send(await successRoute(subscriptions));
});

export const getSubcritionRenewalDetails = route(async(req, res) => {
    const subscriptions = await SubscriptionDB.getSubcritionRenewalDetails(
        req.headers.information,
        req.params.serviceId,
        req.params
    );
    res.send(await successRoute(subscriptions));
});