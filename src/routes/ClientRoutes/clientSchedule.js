import { route, successRoute } from "../";
import ScheduleDB from "../../db/ClientModels/clientScheduleModel";
var requiredFields = [
  "businessId",
  "centerId",
  "clientId",
  "serviceId",
  "teamId",
  "scheduleDate",
  "startTime",
  "endTime",
  "centerId",
];

//Adding new schedule Details Old Code
export const create = route(
  async (req, res) => {
    const {
      businessId,
      centerId,
      clientId,
      serviceId,
      scheduleName,
      teamId,
      scheduleDate,
      startTime,
      endTime,
      batchId,
      is_repeat,
      repeatation_type,
      repeat_number,
      repeat_days,
      repeat_ends,
      repeat_ends_date,
      parent_scheduleId,
      subscriptionId,
      numberOfClients
    } = req.body;
    const newSchedule = await ScheduleDB.create(
      req.headers.information,
      businessId,
      centerId,
      clientId,
      serviceId,
      scheduleName,
      teamId,
      scheduleDate,
      startTime,
      endTime,
      batchId,
      is_repeat,
      repeatation_type,
      repeat_number,
      repeat_days,
      repeat_ends,
      repeat_ends_date,
      parent_scheduleId,
      subscriptionId,
      numberOfClients
    );
    res.send(await successRoute(newSchedule));
  },
  {
    requiredFields: requiredFields
  }
);

//Adding new schedule Details new code
export const newcreate = route(
  async (req, res) => {
    const {
      businessId,
      centerId,
      clientId,
      serviceId,
      scheduleName,
      teamId,
      scheduleDate,
      startTime,
      endTime,
      batchId,
      is_repeat,
      repeatation_type,
      monthly_type,
      repeat_number,
      repeat_days,
      repeat_ends,
      repeat_ends_date,
      parent_scheduleId,
      subscriptionId,
      numberOfClients
    } = req.body;
    const newSchedule = await ScheduleDB.createSchedule(
      req.headers.information,
      businessId,
      centerId,
      clientId,
      serviceId,
      scheduleName,
      teamId,
      scheduleDate,
      startTime,
      endTime,
      batchId,
      is_repeat,
      repeatation_type,
      monthly_type,
      repeat_number,
      repeat_days,
      repeat_ends,
      repeat_ends_date,
      parent_scheduleId,
      subscriptionId,
      numberOfClients
    );
    res.send(await successRoute(newSchedule));
  },
  {
    requiredFields: requiredFields
  }
);

//Listing all Schedules
export const listAll = route(async (req, res) => {
  const Schedules = await ScheduleDB.listAll(req.headers.information);
  res.send(await successRoute(Schedules));
});

//Returning detail of one Schedule
export const scheduleDetail = route(async (req, res) => {
  const Schedules = await ScheduleDB.scheduleDetail(
    req.headers.information,
    req.params.scheduleId
  );
  res.send(await successRoute(Schedules));
});

//Update schedule's information new code
export const update = route(
  async (req, res) => {
    const {
      scheduleId,
      businessId,
      teamId,
      scheduleName,
      scheduleDate,
      startTime,
      endTime,
      centerId,
      batchId,
      is_repeat,
      repeatation_type,
      monthly_type,
      repeat_number,
      repeat_days,
      repeat_ends,
      repeat_ends_date,
      numberOfClients,
      comments
    } = req.body;
    const schedule = await ScheduleDB.updateSchedule(
      req.headers.information,
      req.params.scheduleId,
      businessId,
      teamId,
      scheduleName,
      scheduleDate,
      startTime,
      endTime,
      centerId,
      batchId,
      is_repeat,
      repeatation_type,
      monthly_type,
      repeat_number,
      repeat_days,
      repeat_ends,
      repeat_ends_date,
      numberOfClients,
      comments
    );
    res.send(await successRoute(schedule));
  },
  {
    requiredFields: [
      "businessId",
      "scheduleDate",
      "startTime",
      "endTime",
      "centerId",
    ]
  }
);

//Schedule Dashboard
export const scheduleDashboard = route(async (req, res) => {
  const Schedules = await ScheduleDB.scheduleDashboard(
    req.headers.information,
    req.params.date
  );
  res.send(await successRoute(Schedules));
});

//Delete Schedule's information
export const remove = route(async (req, res) => {
  const schedule = await ScheduleDB.remove(
    req.headers.information,
    req.params.scheduleId.split(/\s*,\s*/)
  );
  res.send(successRoute(schedule));
});

//Adding new clients into schedule
export const addNewClient = route(async (req, res) => {
  const { businessId, clientId, scheduleId, subscriptionId } = req.body;
  const newSchedule = await ScheduleDB.addNewClient(
    req.headers.information,
    businessId,
    clientId,
    scheduleId,
    subscriptionId
  );
  res.send(await successRoute(newSchedule));
});

//Listing all Schedules on specific date
export const showScheduleOnDate = route(async (req, res) => {
  const Schedules = await ScheduleDB.showScheduleOnDate(
    req.headers.information,
    req.params.businessId,
    req.params.date
  );
  res.send(await successRoute(Schedules));
});

//cancel a schedule
export const cancelSchedule = route(async (req, res) => {
  const {
    flag,
    businessId,
    scheduleId,
    scheduleDate,
    scheduleStartTime,
    comment
  } = req.body;
  const schedule = await ScheduleDB.cancelSchedule(
    req.headers.information,
    req.params.flag,
    businessId,
    scheduleId,
    scheduleDate,
    scheduleStartTime,
    comment
  );
  res.send(await successRoute(schedule));
});

export const showUserSchedules = route(async (req, res) => {
  const Schedules = await ScheduleDB.showUserSchedules(
    req.headers.information,
    req.params.scheduleId,
  );
  res.send(await successRoute(Schedules));
});


//User Reschedule
export const userReschedule = route(
  async (req, res) => {
    const {
      scheduleId,
      rescheduleFrom,
      rescheduleFromTime,
      rescheduleTo,
      startTime,
      endTime,
      centerId,
      comment
    } = req.body;
    const reschedule = await ScheduleDB.userReschedule(
      req.headers.information,
      req.params.scheduleId,
      rescheduleFrom,
      rescheduleFromTime,
      rescheduleTo,
      startTime,
      endTime,
      centerId,
      comment
    );
    res.send(await successRoute(reschedule));
  }
);


//Reschedule
export const createReschedule = route(async (req, res) => {
  const {
    businessId,
    rescheduleId,
    centerId,
    scheduleDate,
    startTime,
    endTime,
    scheduleType,
    parent_scheduleId,
    comments
  } = req.body;
  const newReschedule = await ScheduleDB.createReschedule(
    req.headers.information,
    businessId,
    rescheduleId,
    centerId,
    scheduleDate,
    startTime,
    endTime,
    scheduleType,
    parent_scheduleId,
    comments
  );
  res.send(await successRoute(newReschedule));
});

//Attendance History
export const AttendanceHistory = route(async (req, res) => {
  const Schedules = await ScheduleDB.AttendanceHistory(
    req.headers.information,
    req.params.filter,
    req.params
  );
  res.send(await successRoute(Schedules));
});

//Booking Attendance History
export const BookingAttendanceHistory = route(async (req, res) => {
  const Schedules = await ScheduleDB.BookingAttendanceHistory(
    req.headers.information,
    req.params.scheduleID,
    req.params
  );
  res.send(await successRoute(Schedules));
});

//Attendance History New
export const newattendanceHistory = route(async (req, res) => {
  const Schedules = await ScheduleDB.newattendanceHistory(
    req.headers.information,
    req.params.booking_date,
    req.params.schedule_time,
    req.params
  );
  res.send(await successRoute(Schedules));
});

//Booking Attendance History New
export const newBookingAttendanceHistory = route(async (req, res) => {
  const Schedules = await ScheduleDB.newBookingAttendanceHistory(
    req.headers.information,
    req.params.scheduleId,
    req.params.booking_date,
    req.params.schedule_time,
    req.params
  );
  res.send(await successRoute(Schedules));
});

//Listing all the schedules under the business along with booking name and pricepack name
export const recordAttendanceBooking = route(async (req, res) => {
  const Schedules = await ScheduleDB.recordAttendanceBooking(
    req.headers.information,
    req.params.businessId
  );
  res.send(await successRoute(Schedules));
});

// Record Attendance New
export const newrecordAttendanceBooking = route(async (req, res) => {
  const Schedules = await ScheduleDB.newrecordAttendanceBooking(
    req.headers.information,
    req.params.businessId,
    req.params.booking_date,
    req.params.schedule_time,
    req.params
  );
  res.send(await successRoute(Schedules));
});

export const showSchedulesOfClient = route(async (req, res) => {
  const Schedules = await ScheduleDB.showSchedulesOfClient(
    req.headers.information,
    req.params.scheduleId,
    req.params.clientId,
    req.params.date
  );
  res.send(await successRoute(Schedules));
});

export const schedulesWithSameService = route(async (req, res) => {
  const Schedules = await ScheduleDB.schedulesWithSameService(
    req.headers.information,
    req.params.scheduleId,
    req.params.centerId,
    req.params.date
  );
  res.send(await successRoute(Schedules));
});

export const clientReschedule = route(
  async (req, res) => {
    const {
      scheduleId,
      clientId,
      rescheduleFrom,
      rescheduleFromTime,
      rescheduleTo,
      newScheduleId,
      centerId,
      comment,
      scheduleName,
      startTime,
      endTime,
      scheduleTime,
      numberOfClients,
      teamId
    } = req.body;
    const reschedule = await ScheduleDB.clientReschedule(
      req.headers.information,
      req.params.scheduleId,
      req.params.clientId,
      rescheduleFrom,
      rescheduleFromTime,
      rescheduleTo,
      newScheduleId,
      centerId,
      comment,
      scheduleName,
      startTime,
      endTime,
      scheduleTime,
      numberOfClients,
      teamId
    );
    res.send(await successRoute(reschedule));
  }
);

export const getAttendanceHistory = route(async (req, res) => {
  const Schedules = await ScheduleDB.getAttendanceHistory(
    req.headers.information,
    req.params
  );
  res.send(await successRoute(Schedules));
});

// Client Bookings
export const clientBooking = route(
  async (req, res) => {
    const {
      clientId,
    } = req.body;
    const reschedule = await ScheduleDB.clientBooking(
      req.headers.information,
      req.params.clientId,
      req.params
    );
    res.send(await successRoute(reschedule));
  }
);

export const clientBookingDetails = route(
  async (req, res) => {
    const {
      clientId,
      scheduleId
    } = req.body;
    const reschedule = await ScheduleDB.clientBookingDetails(
      req.headers.information,
      req.params.clientId,
      req.params.scheduleId,
      req.params
    );
    res.send(await successRoute(reschedule));
  }
);

//currently scheduled timetable
export const showClientSchedules = route(async (req, res) => {
  const Schedules = await ScheduleDB.showClientSchedules(
    req.headers.information,
    req.params.scheduleId,
    req.params.clientId,
    req.params.date
  );
  res.send(await successRoute(Schedules));
});

//Update schedule's information
export const Client_Edit_booking = route(
  async (req, res) => {
    const {
      scheduleId,
      clientId,
      businessId,
      teamId,
      scheduleName,
      scheduleDate,
      startTime,
      endTime,
      scheduleFromDate,
      centerId,
      is_repeat,
      repeatation_type,
      monthly_type,
      repeat_number,
      repeat_days,
      repeat_ends,
      repeat_ends_date,
      numberOfClients,
      comments,
      scheduleTo
    } = req.body;
    const schedule = await ScheduleDB.Client_Edit_booking(
      req.headers.information,
      req.params.scheduleId,
      req.params.clientId,
      businessId,
      teamId,
      scheduleName,
      scheduleDate,
      startTime,
      endTime,
      scheduleFromDate,
      centerId,
      // batchId,
      is_repeat,
      repeatation_type,
      monthly_type,
      repeat_number,
      repeat_days,
      repeat_ends,
      repeat_ends_date,
      numberOfClients,
      comments,
      scheduleTo
    );
    res.send(await successRoute(schedule));
  },
  {
    requiredFields: [
      "businessId",
      "scheduleDate",
      "startTime",
      "endTime",
      "centerId",
      "scheduleFromDate"
    ]
  }
);

//ClientAttendance History
export const ClientAttendanceHistory = route(async (req, res) => {
  const Schedules = await ScheduleDB.ClientAttendanceHistory(
    req.headers.information,
    req.params.businessId,
    req.params.clientId,
    req.params.booking_date,
    req.params.schedule_time,
    req.params
  );
  res.send(await successRoute(Schedules));
});

//get Schedule on Date
export const ScheduleOnSameServiceandCenter = route(async (req, res) => {
  const Schedules = await ScheduleDB.ScheduleOnSameServiceandCenter(
    req.headers.information,
    req.params.scheduleId,
    req.params
  );
  res.send(await successRoute(Schedules));
});


export const schedulesWithSameServicebutClient = route(async (req, res) => {
  const Schedules = await ScheduleDB.schedulesWithSameServicebutClient(
    req.headers.information,
    req.params.scheduleId,
    req.params.centerId,
    req.params.date,
    req.params.clientId
  );
  res.send(await successRoute(Schedules));
});
