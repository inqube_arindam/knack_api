import { route, successRoute } from "../";
import ClientScheduleMapDB from "../../db/ClientModels/ClientScheduleMapModel";
var requiredFields = [
    "businessId",
    "scheduleId",
    "clientId"
];

//Adding new schedule Details
export const create = route(
  async (req, res) => {
    const {
      businessId,
      scheduleId,
      clientId
    } = req.body;
    const newClientScheduleMap = await ClientScheduleMapDB.create(
      req.headers.information,
      businessId,
      scheduleId,
      clientId
    );
    res.send(await successRoute(newClientScheduleMap));
  },
  {
    requiredFields: requiredFields
  }
);

//Listing all clients of a Schedules
export const list = route(async (req, res) => {
  const clients = await ClientScheduleMapDB.list( req.headers.information, req.params.scheduleId);
  res.send(await successRoute(clients));
});