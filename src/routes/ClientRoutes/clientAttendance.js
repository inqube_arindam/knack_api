import { route, successRoute } from "../";
import AttendanceDB from "../../db/ClientModels/clientAttendanceModel";

export const clientAttendance = route(
  async (req, res) => {
    const {
      businessId,
      scheduleId,
      clientId,
      attendanceType,
      attendanceDate
    } = req.body;
    const attendance = await AttendanceDB.clientAttendance(
      req.headers.information,
      req.params.scheduleId,
      businessId,
      clientId,
      attendanceType,
      attendanceDate
    );
    res.send(await successRoute(attendance));
  },
  {
    requiredFields: [
      "scheduleId",
      "businessId",
      "clientId",
      "attendanceType",
      "attendanceDate"
    ]
  }
);

export const updateAttendanceMulitipleClient = route(
  async (req, res) => {
    const {
      businessId,
      scheduleId,
      clientId,
      attendanceType,
      attendanceDate
    } = req.body;
    //console.log(req.params.attendanceDate);
    const attendance = await AttendanceDB.updateAttendanceMulitipleClient(
      req.headers.information,
      req.params.scheduleId,
      businessId,
      clientId,
      attendanceType,
      req.params.attendanceDate
    );
    res.send(await successRoute(attendance));
  },
  {
    requiredFields: [
      "scheduleId",
      "businessId",
      "clientId",
      "attendanceType",
      //"attendanceDate"     
    ]
  }
);

// Booking Attendance Record New
export const newupdateAttendanceMulitipleClient = route(
  async (req, res) => {
    const {
      businessId,
      scheduleId,
      clientId,
      attendanceType,
      attendanceDate,
      time
    } = req.body;
    //console.log(req.params.attendanceDate);
    const attendance = await AttendanceDB.newupdateAttendanceMulitipleClient(
      req.headers.information,
      req.params.scheduleId,
      businessId,
      clientId,
      attendanceType,
      req.params.attendanceDate,
      req.params.time
    );
    res.send(await successRoute(attendance));
  },
  {
    requiredFields: [
      "scheduleId",
      "businessId",
      "clientId",
      "attendanceType",
      //"attendanceDate"     
    ]
  }
);

export const listAttendance = route(async (req, res) => {
  const attendance = await AttendanceDB.listAttendance(
    req.headers.information,
    req.params.scheduleId,
    req.params.businessId,
    req.params.attendanceDate,
    req.params
    //attendanceType
  );
  res.send(await successRoute(attendance));
});

// Attendance Record New
export const newlistAttendance = route(
  async (req, res) => {
    const attendance = await AttendanceDB.newlistAttendance(
      req.headers.information,
      req.params.scheduleId,
      req.params.attendanceDate,
      req.params.time,
      req.params
    );
    res.send(await successRoute(attendance));
  }
);

export const showAttendance = route(async (req, res) => {
  const attendance = await AttendanceDB.showAttendance(
    req.headers.information,
    req.params.businessId,
    req.params.scheduleId,
    req.params.attendanceDate,
    //attendanceType
  );
  res.send(await successRoute(attendance));
});

//Creating new Attendance Details
export const createAttendance = route(
  async (req, res) => {
    const { businessId, scheduleId, clientId, attendanceType } = req.body;
    const newAttendance = await AttendanceDB.createAttendance(
      req.headers.information,
      businessId,
      scheduleId,
      clientId,
      attendanceType
    );
    res.send(await successRoute(newAttendance));
  },
  {
    requiredFields: ["businessId", "scheduleId", "clientId", "attendanceType"]
  }
);

//client Booking Attendence Record
export const listClientAttendanceRoute = route(
  async (req, res) => {
    const attendance = await AttendanceDB.ClientBookingAttendence(
      req.headers.information,
      req.params.businessId,
      req.params.scheduleId,
      req.params.clientId,
      req.params
      //attendanceType
    );
    res.send(await successRoute(attendance));
  }
);
