import { route, successRoute } from "../";
import BatchDB from "../../db/ClientModels/ClientBatchModel";

//Adding new Batch Details
export const create = route(
  async (req, res) => {
    const {
      businessId,
      centerId,
      serviceId,
      batchName,
      batchSize,
      startTime,
      endTime
    } = req.body;
    const newBatch = await BatchDB.create(
      req.headers.information,
      businessId,
      centerId,
      serviceId,
      batchName,
      batchSize,
      startTime,
      endTime
    );
    res.send(await successRoute(newBatch));
  },
  {
    requiredFields: [
      "businessId",
      "centerId",
      "serviceId",
      "batchName",
      "batchSize",
      "startTime",
      "endTime"
    ]
  }
);

//Listing all the Batches
export const listAll = route(async (req, res) => {
  const batch = await BatchDB.listAll(
    req.headers.information,
    req.params.centerId,
    req.params.serviceId
  );
  res.send(await successRoute(batch));
});
//Returning detail of one Batch
export const batchDetails = route(async (req, res) => {
  const batch = await BatchDB.batchDetails(
    req.headers.information,
    req.params.batchId
  );
  res.send(await successRoute(batch));
});

//Update batch's information
export const update = route(
  async (req, res) => {
    const {
      businessId,
      serviceId,
      centerId,
      batchName,
      batchSize,
      startTime,
      endTime
    } = req.body;
    const batch = await BatchDB.update(
      req.headers.information,
      req.params.batchId,
      businessId,
      serviceId,
      centerId,
      batchName,
      batchSize,
      startTime,
      endTime
    );
    res.send(await successRoute(batch));
  },
  {
    requiredFields: [
      "businessId",
      "serviceId",
      "centerId",
      "batchName",
      "batchSize",
      "startTime",
      "endTime"
    ]
  }
);

//Delete batch's information
export const remove = route(async (req, res) => {
  const batch = await BatchDB.remove(
    req.headers.information,
    req.params.batchId.split(/\s*,\s*/)
  );
  res.send(await successRoute(batch));
});
