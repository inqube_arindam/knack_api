import { route, successRoute } from "../";
import EmployeeDB from "../../db/ClientModels/ClientEmployeeModel";

//Adding new Employee Details
export const create = route(
  async (req, res) => {
    const {
      centerId,
      employeeName,
      contactNumber,
      emailId,
      alternateNumber,
      alternateEmail,
      dateOfBirth,
      area,
      pin,
      city
    } = req.body;
    const newEmployee = await EmployeeDB.create(
      req.headers.information,
      centerId,
      employeeName,
      contactNumber,
      emailId,
      alternateNumber,
      alternateEmail,
      dateOfBirth,
      area,
      pin,
      city
    );
    res.send(await successRoute(newEmployee));
  },
  {
    requiredFields: [
      "employeeName",
      "contactNumber",
    ]
  }
);

// Team Management
export const teamManagement = route(async (req, res) => {
  const employee = await EmployeeDB.teamManagement(
    req.headers.information,
    req.params
  );
  res.send(await successRoute(employee));
});

export const teamManagementAndSchedule = route(async (req, res) => {
  const employee = await EmployeeDB.teamManagementAndSchedule(
    req.headers.information,
    req.params.scheduleId,
    req.params,

  );
  res.send(await successRoute(employee));
});

//Listing all the Employee of a business
export const listAll = route(async (req, res) => {
  const employee = await EmployeeDB.listAll(
    req.headers.information,
    req.params.businessId,
    req.params
  );
  res.send(await successRoute(employee));
});
//Listing all the Employee of a Center
export const listAllcenter = route(async (req, res) => {
  const employee = await EmployeeDB.listAllcenter(
    req.headers.information,
    req.params.centerId
  );
  res.send(await successRoute(employee));
});
//Returning detail of one Employee
export const employeeDetails = route(async (req, res) => {
  const employee = await EmployeeDB.employeeDetails(
    req.headers.information,
    req.params.employeeId
  );
  res.send(await successRoute(employee));
});

//Update Employee's information
export const update = route(
  async (req, res) => {
    const {
      userId,
      roleId,
      centerId,
      employeeName,
      contactNumber,
      emailId,
      alternateNumber,
      alternateEmail,
      dateOfBirth,
      area,
      pin,
      city
    } = req.body;
    var photoUrl;
    if (req.files) {
      var photoUrl = req.files.photo;
    }
    const employee = await EmployeeDB.update(
      req.headers.information,
      req.params.employeeId,
      userId,
      roleId,
      centerId,
      employeeName,
      contactNumber,
      emailId,
      alternateNumber,
      alternateEmail,
      dateOfBirth,
      area,
      pin,
      city,
      photoUrl
    );
    res.send(await successRoute(employee));
  },
  {
    requiredFields: [
      "contactNumber",
    ]
  }
);

//Update Employee's Role
export const updateTeamRole = route(
  async (req, res) => {
    const {
      userId,
      employeeId,
      roleId,
    } = req.body;
    const employee = await EmployeeDB.updateTeamRoleDb(
      req.headers.information,
      userId,
      employeeId,
      roleId,
      req.params
    );
    res.send(await successRoute(employee));
  },
  {
    requiredFields: [
      "employeeId",
    ]
  }
);

//Delete Employee's information
export const remove = route(async (req, res) => {
  const employee = await EmployeeDB.remove(
    req.headers.information,
    req.params.employeeId.split(/\s*,\s*/)
  );
  res.send(await successRoute(employee));
});

//Multiple new employee create
export const multipleEmployeeCreate = route(
  async (req, res) => {
    let headerInformation = req.headers.information;
    let information = headerInformation.split(',');
    const newClient = await EmployeeDB.multipleEmployeeCreate(
      req.headers.information,
      information[1],
      req.body
    );
    res.send(await successRoute(req.body));
  }, {
    requiredFields: []
  }
);
