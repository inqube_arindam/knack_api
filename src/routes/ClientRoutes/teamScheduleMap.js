import { route, successRoute } from "../";
import TeamScheduleMapDB from "../../db/ClientModels/TeamScheduleMapModel";
var requiredFields = [
    "businessId",
    "scheduleId",
    "teamId"
];

//Adding new schedule Details
export const create = route(
  async (req, res) => {
    const {
      businessId,
      scheduleId,
      teamId
    } = req.body;
    const newTeamScheduleMap = await TeamScheduleMapDB.create(
      req.headers.information,
      businessId,
      scheduleId,
      teamId
    );
    res.send(await successRoute(newTeamScheduleMap));
  },
  {
    requiredFields: requiredFields
  }
);