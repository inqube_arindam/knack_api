import { route, successRoute } from "../";
import ClientOfferDB from "../../db/ClientModels/ClientOfferModel";

//Adding new Offer Details
export const create = route(
  async (req, res) => {
    const { businessId, centerId, offerName, offerType, offerValue } = req.body;
    const newOffer = await ClientOfferDB.create(
      req.headers.information,
      businessId,
      centerId,
      offerName,
      offerType,
      offerValue
    );
    res.send(await successRoute(newOffer));
  },
  {
    requiredFields: ["businessId", "offerName", "offerType", "offerValue"]
  }
);

//Listing all the offers of a business
export const listAll = route(async (req, res) => {
  const offers = await ClientOfferDB.listAll(
    req.headers.information,
    req.params.businessId
  );
  res.send(await successRoute(offers));
});

//Listing all the offers of a center
export const list = route(async (req, res) => {
  const offers = await ClientOfferDB.list(
    req.headers.information,
    req.params.centerId
  );
  res.send(await successRoute(offers));
});

//Returning offer details
export const details = route(async (req, res) => {
  const offers = await ClientOfferDB.details(
    req.headers.information,
    req.params.id
  );
  res.send(await successRoute(offers));
});

//Update offer's information
export const update = route(
  async (req, res) => {
    const { businessId, centerId, offerName, offerType, offerValue } = req.body;
    const offers = await ClientOfferDB.update(
      req.headers.information,
      req.params.id,
      businessId,
      centerId,
      offerName,
      offerType,
      offerValue
    );
    res.send(await successRoute(offers));
  },
  {
    requiredFields: ["businessId", "offerName", "offerType", "offerValue"]
  }
);

//Delete offer's information
export const remove = route(async (req, res) => {
  const offers = await ClientOfferDB.remove(
    req.headers.information,
    req.params.id.split(/\s*,\s*/)
  );
  res.send(await successRoute(offers));
});
