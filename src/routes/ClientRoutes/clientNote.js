import { route, successRoute } from "../";
import NoteDB from "../../db/NoteModel";

//===============Add Note===============

export const addNote = route(
    async (req, res) => {
      const {
        businessId,
        clientId,
        heading,
        description,
        reminder_date,
        reminder_time
      } = req.body;
      const newNote = await NoteDB.create(
        req.headers.information,
        businessId,
        clientId,
        heading,
        description,
        reminder_date,
        reminder_time
      );
      res.send(await successRoute(newNote));
    },
    {
      requiredFields: [
        "businessId",
        "clientId",
        "heading",
        "description",
        "reminder_date",
        "reminder_time"
      ]
    }
  );

 //==========Listing all Notes by clientID===========

export const allNotes = route(async (req, res) => {

    const note = await NoteDB.listAllNotes(
      req.headers.information,
      req.params.page,
      req.params.clientId,
    );
    res.send(await successRoute(note));
});

//============get single Note=======================

export const singleNote = route(async(req, res) => {
  const note = await NoteDB.singleNote(
    req.headers.information,
    req.params.noteId,
  );
  res.send(await successRoute(note));
});

//============update Note============================

export const updateNote = route(
  async(req, res) => {
    let noteId = req.body.noteId;
    let heading = req.body.heading;
    let description = req.body.description;
    let reminder_date = req.body.reminder_date;
    let reminder_time = req.body.reminder_time;
    const note = await NoteDB.updateNote(
      req.headers.information,
      noteId,
      heading,
      description,
      reminder_date,
      reminder_time
    );
   res.send(await successRoute(note));
 },
 {
  requiredFields: [
    "noteId",
    "heading",
    "description",
    "reminder_date",
    "reminder_time"
  ]
 })

 //================search Note==================

export const searchNote = route(async (req, res) => {

    const note = await NoteDB.searchNote(
      req.headers.information,
      req.body
    );
    res.send(await successRoute(note));
},{
  requiredFields: [
    "clientId",
    "searchKey",
  ]
});

//==================Delete Note===================

export const deleteNote = route(async(req, res) => {
  const note = await NoteDB.deleteNote(
    req.headers.information,
    req.params.noteId,
  );
  res.send(await successRoute(note));
});