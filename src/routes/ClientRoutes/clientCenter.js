import { route, successRoute } from "../";
import CenterDB from "../../db/ClientModels/ClientCenterModel.js";

//Adding new center Details
export const create = route(
  async (req, res) => {
    const {
      businessId,
      centerName,
      area,
      landmark,
      pin,
      city,
      contactNumber
    } = req.body;
    const newCenter = await CenterDB.create(
      req.headers.information,
      businessId,
      centerName,
      area,
      landmark,
      pin,
      city,
      contactNumber
    );
    res.send(await successRoute(newCenter));
  },
  {
    requiredFields: [
      "businessId",
      "centerName",
      "area",
      "pin",
    ]
  }
);

//Listing all the Center of a business
export const listAll = route(async (req, res) => {
  const centers = await CenterDB.listAll(
    req.headers.information,
    req.params.businessId
  );
  res.send(await successRoute(centers));
});

// List of Team or Service
export const listTeamOrService = route(async (req, res) => {
  const centers = await CenterDB.listTeamOrService(
    req.headers.information,
    req.params.centerId,
    req.params.typeId
  );
  res.send(await successRoute(centers));
});

//Returning detail of one Center
export const centerDetails = route(async (req, res) => {
  const centers = await CenterDB.centerDetails(
    req.headers.information,
    req.params.centerId
  );
  res.send(await successRoute(centers));
});

//Update Center's information
export const update = route(
  async (req, res) => {
    const {
      businessId,
      centerName,
      area,
      landmark,
      pin,
      city,
      contactNumber
    } = req.body;
    const centers = await CenterDB.update(
      req.headers.information,
      req.params.centerId,
      businessId,
      centerName,
      area,
      landmark,
      pin,
      city,
      contactNumber
    );
    res.send(await successRoute(centers));
  },
  {
    requiredFields: [
      "businessId",
      "centerName",
      "contactNumber",
      "area",
      "pin",
      "city"
    ]
  }
);

//Delete Center's information
export const remove = route(async (req, res) => {
  const centers = await CenterDB.remove(
    req.headers.information,
    req.params.centerId.split(/\s*,\s*/)
  );
  res.send(await successRoute(centers));
});
