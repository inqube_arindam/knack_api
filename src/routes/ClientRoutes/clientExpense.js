import { route, successRoute } from "../";
import ExpenseDB from "../../db/ClientModels/clientExpenseModel";

//Adding new Offer Details
export const addExpense = route(
    async(req, res) => {
        const {
            businessId,
            name,
            amount,
            expenseType,
            centerId,
            dateOfPayment,
            modeOfPayment,
            chequeNumber
        } = req.body;
        const newExpense = await ExpenseDB.addExpense(
            req.headers.information,
            businessId,
            name,
            amount,
            expenseType,
            centerId,
            dateOfPayment,
            modeOfPayment,
            chequeNumber
        );
        res.send(await successRoute(newExpense));
    }, {
        requiredFields: [
            "businessId",
            "centerId",
            "name",
            "amount",
            "expenseType",
            "dateOfPayment",
            "modeOfPayment"
        ]
    }
);

//Listing all the Offerings of a business
export const listExpense = route(async(req, res) => {
    const expense = await ExpenseDB.listExpense(
        req.headers.information,
        req.params.businessId
    );
    res.send(await successRoute(expense));
});

//Listing recent Expenses of a business
export const listRecentExpense = route(async(req, res) => {
    const expense = await ExpenseDB.listRecentExpense(
        req.headers.information,
        req.params
    );
    res.send(await successRoute(expense));
});

//Listing recent Expenses of a business
export const expenseMonth = route(async(req, res) => {
    const expense = await ExpenseDB.expenseMonth(
        req.headers.information,
        req.params.businessId,
        req.params.dateOfPayment,
        req.params.YM,
        req.params.IE,
        req.params
    );
    res.send(await successRoute(expense));
});

export const getExpenseType = route(async(req, res) => {
    const expense = await ExpenseDB.getExpenseType(
        req.headers.information,
        req.params
    );
    res.send(await successRoute(expense));
});


export const remove = route(async(req, res) => {
    const expense = await ExpenseDB.remove(
        req.headers.information,
        req.params.clientexpenseId.split(/\s*,\s*/)
    );
    res.send(await successRoute(expense));
});