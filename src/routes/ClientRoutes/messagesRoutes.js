import { route, successRoute } from "../";
import MessagesDB from "../../db/ClientModels/messagesModel.js";
import BusinessDB from "../../db/BusinessModel";
var async = require('async');
var cron = require('node-cron');


export const add = route(async (req, res) => {

    const { businessId, serviceId, senderId, recipients, messageTypeId, messageBody, message_date, message_time } = req.body;

    const newMessage = await MessagesDB.add(
        req.headers.information,
        serviceId,
        businessId,
        senderId,
        recipients,
        messageTypeId,
        messageBody,
        message_date + ' ' + message_time
    );

    res.send(await successRoute(newMessage));

},

    {
        requiredFields: ["businessId", "serviceId", "senderId", "messageTypeId", "messageBody", "message_date", "message_time"]
    }

);

export const list = route(async (req, res) => {

    const message = await MessagesDB.listAllMessages(
        req.headers.information,
        req.params.senderId,
        req.query,
        req.params.page
    );
    res.send(await successRoute(message));

});

export const subscribedClientList = route(async (req, res) => {

    const message = await MessagesDB.subscribedClientList(
        req.headers.information,
        req.query,
        req.params.page
    );
    res.send(await successRoute(message));

});

export const getClientsByServiceIds = route(async (req, res) => {

    const message = await MessagesDB.getClientsByServiceIds(
        req.headers.information,
        req.params.serviceId,
        req.params.page,
        req.query
    );
    res.send(await successRoute(message));

});

export const messageDetails = route(async (req, res) => {

    const message = await MessagesDB.singleMessage(
        req.headers.information,
        req.params.messageId,
    );
    res.send(await successRoute(message));

});

export const deleteMessage = route(async (req, res) => {
    const note = await MessagesDB.deleteMessage(
        req.headers.information,
        req.params.messageId,
    );
    res.send(await successRoute(note));
});

export const messageTypes = route(async (req, res) => {
    const note = await MessagesDB.messageTypes(
        req.headers.information
    );
    res.send(await successRoute(note));
});

export const updateMessage = route(async (req, res) => {
    const { messageTypeId, businessId, senderId, serviceId, recipients, messageBody, message_date, message_time } = req.body;
    const newMessage = await MessagesDB.update(
        req.headers.information,
        req.params.messageId,
        senderId,
        serviceId,
        recipients,
        messageTypeId,
        messageBody,
        message_date + ' ' + message_time
    );
    res.send(await successRoute(newMessage));
});

export const adminClientList = route(async (req, res) => {
    const message = await MessagesDB.adminClientList(
        req.headers.information,
        req.query,
        req.params.page
    );
    res.send(await successRoute(message));

});

cron.schedule('10 0 * * *', function () {
    BusinessDB.getAllBusinessIds().then((businessids) => {
        async.each(businessids.rows, function (business, cb) {
            //console.log("==========",business.dataValues.businessId,"================");
            MessagesDB.sendMessage('localhost,' + business.dataValues.businessId).then((res) => {
                cb();
            })

        }, function (err) {
            if (err) {
                console.log("Error : ", err);
            } else {
                console.log("Cron executed successfully");
            }
        })
    })
});