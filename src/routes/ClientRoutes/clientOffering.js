import { route, successRoute } from "../";
import OfferingsDB from "../../db/ClientModels/ClientOfferingModel";

//Adding new Offer Details
export const create = route(
  async (req, res) => {
    const { businessId, centerId, serviceName, serviceDetails } = req.body;
    const newOffering = await OfferingsDB.create(
      req.headers.information,
      businessId,
      serviceName,
      serviceDetails
    );
    res.send(await successRoute(newOffering));
  },
  {
    requiredFields: ["businessId", "serviceName"]
  }
);

//Listing all the Offerings of a business
export const listAll = route(async (req, res) => {
  const Offerings = await OfferingsDB.listAll(
    req.headers.information,
    req.params.businessId
  );
  res.send(await successRoute(Offerings));
});
//Listing all the Offerings of a Center
export const listAllcenter = route(async (req, res) => {
  const Offerings = await OfferingsDB.listAllcenter(
    req.headers.information,
    req.params.centerId
  );
  res.send(await successRoute(Offerings));
});
//Returning detail of one Offering
export const offeringDetails = route(async (req, res) => {
  const Offerings = await OfferingsDB.offeringDetails(
    req.headers.information,
    req.params.serviceId
  );
  res.send(await successRoute(Offerings));
});

export const clientList = route(async (req, res) => {
  const Offerings = await OfferingsDB.clientList(
    req.headers.information,
    req.params.serviceId,
    req.params
  );
  res.send(await successRoute(Offerings));
});

export const newclientList = route(
  async (req, res) => {
    const {  scheduleId, subscriptionId } = req.body;
    const newclientlistdata = await OfferingsDB.newclientlist(
      req.headers.information,
      req.params.serviceId,
      scheduleId,
      subscriptionId
    );
    res.send(await successRoute(newclientlistdata));
  }
  
);

//Update Offering's information
export const update = route(
  async (req, res) => {
    const { businessId, centerId, serviceName, serviceDetails } = req.body;
    const Offerings = await OfferingsDB.update(
      req.headers.information,
      req.params.serviceId,
      businessId,
      centerId,
      serviceName,
      serviceDetails
    );
    res.send(await successRoute(Offerings));
  },
  {
    requiredFields: ["businessId", "serviceName", "serviceDetails"]
  }
);

//Delete Offering's information
export const remove = route(async (req, res) => {
  const Offerings = await OfferingsDB.remove(
    req.headers.information,
    req.params.serviceId.split(/\s*,\s*/)
  );
  res.send(await successRoute(Offerings));
});
