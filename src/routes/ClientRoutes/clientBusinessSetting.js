import { route, successRoute } from "../";
import BusinessSettingDB from "../../db/ClientModels/ClientBusinessSettingModel";

//Adding new Employee Details
export const create = route(
    async(req, res) => {
        const {
            businessId,
            businessSettingsData,
            businessSettingType
        } = req.body;
        const newBusinessSetting = await BusinessSettingDB.create(
            req.headers.information,
            businessId,
            businessSettingsData,
            businessSettingType
        );
        res.send(await successRoute(newBusinessSetting));
    }, {
        requiredFields: [
            "businessId",
            "businessSettingsData",
            "businessSettingType",
        ]
    }
);


export const update = route(
    async(req, res) => {
        const {
            businessId,
            businessSettingsData
        } = req.body;
        const BusinessSetting = await BusinessSettingDB.update(
            req.headers.information,
            req.params.businessId,
            businessSettingsData
        );
        res.send(await successRoute(BusinessSetting));
    }, {
        requiredFields: [
            "businessId",
            "businessSettingsData"
        ]
    }
);


//Returning detail of one Employee
export const details = route(async(req, res) => {
    const BusinessSetting = await BusinessSettingDB.businessSettingDetails(
        req.headers.information
    );
    res.send(await successRoute(BusinessSetting));
});


export const listservicepricepack = route(
    async(req, res) => {
        const { serviceId } = req.body;
        const BusinessSetting = await BusinessSettingDB.listservicepricepack(
            req.headers.information,
            req.body.serviceId
        );
        res.send(await successRoute(BusinessSetting));
    }, {
        requiredFields: ["serviceId"]
    }
);