import { route, successRoute } from "../";
import ReportsDB from "../../db/ClientModels/clientReportsModel";


//Listing all the Offerings of a business


export const showClientPricePackList = route(async(req, res) => {
    const report = await ReportsDB.showClientPricePackList(
        req.headers.information,
        req.params.clientId
    );
    res.send(await successRoute(report));
});

export const showClientAttendance = route(async(req, res) => {
    const report = await ReportsDB.showClientAttendance(
        req.headers.information,
        req.params.scheduleId,
        req.params.clientId,
        req.params.reportType
    );
    res.send(await successRoute(report));
});

export const showClientPayment = route(async(req, res) => {
    const report = await ReportsDB.showClientPayment(
        req.headers.information,
        req.params.scheduleId,
        req.params.clientId,
        req.params.reportType
    );
    res.send(await successRoute(report));
});
export const showClientPaymentShare = route(async(req, res) => {
    const report = await ReportsDB.showClientPaymentShare(
        req.headers.information,
        req.params.scheduleId,
        req.params.clientId,
        req.params.reportType
    );
    res.send(await successRoute(report));
});

export const clientReportPaymentAndAttendance = route(
    async(req, res) => {
        const { clientList, reportType } = req.body;
        const newclientlistdata = await ReportsDB.clientReportPaymentAndAttendance(
            req.headers.information,
            clientList,
            reportType
        );
        res.send(await successRoute(newclientlistdata));
    }

);

// Report client List
export const reportclientlist = route(async(req, res) => {
    const report = await ReportsDB.reportclientlist(
        req.headers.information,
        req.params
    );
    res.send(await successRoute(report));
});

//Client Report
export const ClientReports = route(async (req, res) => {
    const Schedules = await ReportsDB.ClientReports(
      req.headers.information,
      req.params.clientId,
      req.params.page,
      req.params
    );
    res.send(await successRoute(Schedules));
  });