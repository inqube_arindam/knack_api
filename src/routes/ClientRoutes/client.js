import {
    route,
    successRoute
} from "../";
import ClientDB from "../../db/ClientModels/ClientModel";

//Adding new Client Details
export const create = route(
    async(req, res) => {
        const {
            businessId,
            //centerId,
            clientName,
            contactNumber,
            emailId,
            alternateEmail,
            area,
            pin,
            city,
            dateOfBirth
        } = req.body;
        var photoUrl;
        if (req.files) {
            var photoUrl = req.files.client_profile;
        }
        const newClient = await ClientDB.create(
            req.headers.information,
            businessId,
            // centerId,
            clientName,
            contactNumber,
            emailId,
            alternateEmail,
            area,
            pin,
            city,
            dateOfBirth,
            photoUrl
        );
        res.send(await successRoute(newClient));
    }, {
        requiredFields: [
            "businessId",
            //"centerId",
            "contactNumber",
            "clientName",
            // "emailId",
            // "area",
            // "pin",
            // "city"
        ]
    }
);

//Client Management Dashboard
export const userClientManagementDashboard = route(async(req, res) => {
    const client = await ClientDB.userClientManagementDashboard(
        req.headers.information
    );
    res.send(await successRoute(client));
});

//Listing all the Client of a business
export const listAll = route(async(req, res) => {
    const client = await ClientDB.listAll(
        req.headers.information,
        req.params.businessId,
        req.params
    );
    res.send(await successRoute(client));
});
//Listing all the Client of a business
export const listAllClient = route(async(req, res) => {
    const client = await ClientDB.listAllClient(
        req.headers.information,
        req.params
    );
    res.send(await successRoute(client));
});


export const listAllClientNew = route(
    async(req, res) => {
        const {
            status,
            search_data,
        } = req.body;
        const newClient = await ClientDB.listAllClientNew(
            req.headers.information,
            status,
            search_data,
            req.params
        );
        res.send(await successRoute(newClient));
    }
);


//Listing all the Client of a Center
export const listAllcenter = route(async(req, res) => {
    const client = await ClientDB.listAllcenter(
        req.headers.information,
        req.params.centerId
    );
    res.send(await successRoute(client));
});
//Returning details of one Client
export const clientDetails = route(async(req, res) => {
    const client = await ClientDB.clientDetails(
        req.headers.information,
        req.params.clientId
    );
    res.send(await successRoute(client));
});

//Update Client's information
export const update = route(
    async(req, res) => {
        const {
            centerId,
            clientName,
            contactNumber,
            emailId,
            alternateEmail,
            area,
            pin,
            city,
            dateOfBirth,
            alternateNumber
        } = req.body;
        var photoUrl;
        if (req.files) {
            var photoUrl = req.files.client_profile;
        }
        const client = await ClientDB.update(
            req.headers.information,
            req.params.clientId,
            clientName,
            contactNumber,
            emailId,
            alternateEmail,
            area,
            pin,
            city,
            dateOfBirth,
            photoUrl,
            alternateNumber
        );
        res.send(await successRoute(client));
    }, {
        requiredFields: [
            "contactNumber",
            "clientName"
        ]
    }
);

//Delete client's information
export const remove = route(async(req, res) => {
    const client = await ClientDB.remove(
        req.headers.information,
        req.params.clientId.split(/\s*,\s*/)
    );
    res.send(await successRoute(client));
});

//Search clients based on clients' names
export const searchClient = route(async(req, res) => {
    const client = await ClientDB.searchClient(
        req.headers.information,
        req.params.businessId,
        req.params.clientName
    );
    res.send(await successRoute(client));
});

//Listing all the Client of a Center
export const listingClients = route(async(req, res) => {
    const client = await ClientDB.listingClients(
        req.headers.information,
        req.params.businessId
    );
    res.send(await successRoute(client));
});

//Listing all the Client of a Center
export const clientNoOfSubscription = route(async(req, res) => {
    const client = await ClientDB.clientNoOfSubscription(
        req.headers.information,
        req.params.businessId,
        req.params.status
    );
    res.send(await successRoute(client));
});

export const clientDetailsDashboard = route(async(req, res) => {
    const client = await ClientDB.clientDetailsDashboard(
        req.headers.information,
        req.params.businessId,
        req.params.clientId
    );
    res.send(await successRoute(client));
});

export const clientRecordAttendance = route(async(req, res) => {
    const client = await ClientDB.clientRecordAttendance(
        req.headers.information,
        req.params.businessId,
        req.params.clientId
    );
    res.send(await successRoute(client));
});
export const showClients = route(async(req, res) => {
    const client = await ClientDB.showClients(
        req.headers.information,
        req.params.businessId,
        req.params.option
    );
    res.send(await successRoute(client));
});


// Active Client List
export const activeClientListRoute = route(async(req, res) => {
    const activeClientList = await ClientDB.activeClientListRoute(
        req.headers.information,
        req.params
    );
    res.send(await successRoute(activeClientList));
});


// Active Client List
export const inactiveClientListRoute = route(async(req, res) => {
    const inactiveClientListRouteList = await ClientDB.inactiveClientListRoute(
        req.headers.information,
        req.params
    );
    res.send(await successRoute(inactiveClientListRouteList));
});

//List Current Past Subcription Per Client
export const listCurrentPastSubcriptionPerClient = route(async(req, res) => {
    const listCurrentPastSubcriptionPerClientRoute = await ClientDB.listCurrentPastSubcriptionPerClient(
        req.headers.information,
        req.params
    );
    res.send(await successRoute(listCurrentPastSubcriptionPerClientRoute));
});

export const topClients = route(async(req, res) => {
    const client = await ClientDB.topClients(
        req.headers.information,
        req.params
    );
    res.send(await successRoute(client));
});

//Listing all the Client of a business filter using service id or pricepack id
export const listClientServicePricepack = route(async(req, res) => {
    const client = await ClientDB.listClientServicePricepack(
        req.headers.information,
        req.params
    );
    res.send(await successRoute(client));
});

//Listing only lead clients
export const leadClientList = route(async(req, res) => {
    const client = await ClientDB.leadClientList(
        req.headers.information,
        req.params
    );
    res.send(await successRoute(client));
});

//Adding Multiple Client
export const createMultiple = route(
    async(req, res) => {
        let headerInformation = req.headers.information;
        let information = headerInformation.split(',');
        const newClient = await ClientDB.createClientMultiple(
            req.headers.information,
            information[1],
            req.body
        );
        res.send(await successRoute(req.body));
    }, {
        requiredFields: [

        ]
    }
);

export const listClientOnServiceAndSchedule = route(async(req, res) => {
    const client = await ClientDB.listClientOnServiceAndSchedule(
        req.headers.information,
        req.params.serviceId,
        req.params.scheduleId,
        req.params.page,
        req.params
    );
    res.send(await successRoute(client));
});


export const listClientOnService = route(async(req, res) => {
    const client = await ClientDB.listClientOnService(
        req.headers.information,
        req.params.serviceId,
        req.params.page,
        req.params
    );
    res.send(await successRoute(client));
});

//csv upload

export const csvUpload = route(
    async(req, res) => {
        let headerInformation = req.headers.information;
        let information = headerInformation.split(',');
        if (req.files) {
            var csvUrl = req.files.client;
        } else {
            var csvUrl = null;
        }

        const newClient = await ClientDB.csvUpload(
            req.headers.information,
            information[1],
            csvUrl
        );

        res.send(await successRoute(newClient));
    }
);