import { route, successRoute } from "./";
import loginSignUpDB from "../db/LoginSignupModel";

//For signup process
export const signup = route(
  async (req, res) => {
    const {
      name,
      emailId,
      contactNumber,
      password,
      FcmUserToken,
      IMEINumber
    } = req.body;
    const newLoginUser = await loginSignUpDB.create(
      name,
      emailId,
      contactNumber,
      password,
      FcmUserToken,
      IMEINumber
    );
    res.send(await successRoute(newLoginUser));
  },
  {
    requiredFields: [
      "name",
      "emailId",
      "contactNumber",
      "password",
      "FcmUserToken",
      "IMEINumber"
    ]
  }
);

//For Verification
export const verify = route(
  async (req, res) => {
    const {
      businessId,
      verificationCode,
      emailverificationCode,
      FcmUserToken,
      IMEINumber
    } = req.body;
    const verifylogin = await loginSignUpDB.verify(
      req.headers.information,
      businessId,
      verificationCode,
      emailverificationCode,
      FcmUserToken,
      IMEINumber
    );
    res.send(await successRoute(verifylogin));
  },
  {
    requiredFields: ["businessId", "verificationCode", "emailverificationCode"]
  }
);

//For resending verification code
export const resend = route(async (req, res) => {
  const { userId, businessId } = req.params;
  const verifylogin = await loginSignUpDB.resend(userId, businessId);
  res.send(await successRoute(verifylogin));
});

//For Login
export const login = route(
  async (req, res) => {
    const { emailId, password, FcmUserToken, IMEINumber } = req.body;
    const login = await loginSignUpDB.getLoginByCredentials(
      emailId,
      password,
      FcmUserToken,
      IMEINumber
    );
    res.send(await successRoute(verifylogin));
  },
  {
    //requiredFields: ["emailId", "password"]
  }
);

//For Logout
export const logout = route(
  async (req, res) => {
    const { userId, businessId, FcmUserToken, IMEINumber } = req.body;
    const logout = await loginSignUpDB.logout(
      userId,
      businessId,
      FcmUserToken,
      IMEINumber
    );
    res.send(await successRoute(logout));
  },
  {
    requiredFields: ["userId", "businessId", "FcmUserToken", "IMEINumber"]
  }
);

//For Forgot Password
export const forgotPassword = route(
  async (req, res) => {
    const { emailId } = req.body;
    const forgotpassword = await loginSignUpDB.forgotPassword(emailId);
    res.send(await successRoute(forgotpassword));
  },
  {
    requiredFields: ["emailId"]
  }
);

//For verifying reset Password OTP
export const verifyResetPassword = route(
  async (req, res) => {
    //const { emailId, verificationCode, emailverificationCode } = req.body;
    const { emailId, emailverificationCode } = req.body;
    const forgotpassword = await loginSignUpDB.verifyResetPassword(
      emailId,
      //verificationCode,
      emailverificationCode
    );
    res.send(await successRoute(forgotpassword));
  },
  {
    requiredFields: ["emailId", "emailverificationCode"]
  }
);

//For Reseting Password
export const resetPassword = route(
  async (req, res) => {
    const { emailId, password, FcmUserToken, IMEINumber } = req.body;
    const forgotpassword = await loginSignUpDB.resetPassword(
      emailId,
      password,
      FcmUserToken,
      IMEINumber
    );
    res.send(await successRoute(forgotpassword));
  },
  {
    requiredFields: ["emailId", "password", "FcmUserToken", "IMEINumber"]
  }
);
