
module.exports = {
    apps: [{
      name: "knack",
      script: "src/index.js"
    }],
    deploy: {
      production: {
        user: "your user",
        host: "Your Host Name",
        key: "your .pem file location",
        ref: "origin/master",
        repo: "your project github repository",
       "ssh_options": ["StrictHostKeyChecking=no", "PasswordAuthentication=no"],
        path:"path of your directory on server", 
       "post-deploy": "npm install && pm2 startOrRestart ecosystem.config.js"
      }
    }
  }